
/* Std do lokacji laki w poblizu Rinde
   Made by Avard, 17.07.06            

   Przer�bki i system losowania opis�w lokacji - Vera, 
                                                 Sat Mar 10 2007
*/

inherit "/std/room";

#include <mudtime.h>
#include <language.h>
#include <macros.h>
#include <stdproperties.h>
#include <pogoda.h>
#include "../dir.h"

string losuj_kurwa_long();
string opis_polmroku();

string dlugi;

void
create_laka()
{
}

nomask void
create_room()
{
    set_long("@@dlugi_opis@@");
    set_polmrok_long("@@opis_polmroku@@");

    add_event("@@event_laki:"+file_name(TO)+"@@");
	
    set_event_time(300.0);

    add_sit(({"na trawie","na ziemi"}),"na trawie","z trawy",0);
	add_prop(ROOM_I_TYPE, ROOM_FIELD);
	
    dodaj_ziolo("babka_lancetowata.c");
    dodaj_ziolo("chaber.c");
	dodaj_ziolo(({"czarny_bez-lisc.c",
				"czarny_bez-kwiaty.c",
				"czarny_bez-owoce.c"}));
    dodaj_ziolo(({"krwawnik_ziele.c",
                "krwawnik_kwiatostan.c"}));
	dodaj_ziolo("piolun.c");
	
    create_laka();
}

string
event_laki()
{
    switch (pora_dnia())
    {
	case MT_RANEK:
	case MT_POLUDNIE:
	case MT_POPOLUDNIE:
	    return process_string(({"@@pora_roku_dzien@@"})[random(1)]);
	default: // wieczor, noc
	    return process_string(({"@@pora_roku_noc@@",
            "Nietoperz muska mi�kkim skrzyd�em tw�j policzek po czym "+
            "przera�ony odlatuje czym pr�dzej.\n",
            "Cicho pohukuj�ca ma�a s�wka przelatuje tu� obok twojej g�owy.\n",
            "S�ycha� nieprzyjemne wysokie brz�czenie i nim orientujesz si� "+
            "co to jest kilka komar�w gryzie ci� bole�nie w kark.\n"})[random(4)]);
    }
}
string
pora_roku_dzien()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Motyl przysiada na kwiecie mlecza, kt^ory ko^lysze "+
             "si^e lekko pod jego ci^e^zarem.\n",
             "Motyl przysiada na kwiecie stokrotki, kt^ory ko^lysze si^e "+
             "lekko pod jego ci^e^zarem.\n",
             "Wysoki trel skowronka d^xwi^ecz^ac wibruje w powietrzu.\n",
             "Drapie�ny ptak nagle pikuje w d� atakuj�c upatrzon� "+
             "zwierzyn�.\n",
             "Ma�a mr�wka pracowicie wspina si� po �d�ble trawy, ale nag�y "+
             "podmuch wiatru str�ca j� z powrotem na ziemi�.\n",
             "W powietrzu unosz� si� chmary drobnych muszek, kt�re "+
             "zaczynaj� k�sa� bardzo bole�nie.\n",
             "Wielka mucha przelecia^la ci tu^z ko^lo nosa.\n",
             "Polna myszka przebieg^la niedaleko twoich st^op.\n",
             "Dochodzi ci^e ciche popiskiwanie. Czy^zby jaka^s mysz?\n",
             "Ma^la pszcz^o^lka przeskakuje rado^snie z kwiatka na kwiatek.\n",
             "Niewielki je^z przetupta^l niedaleko ciebie i znikn^a^l "+
             "pomi^edzy trawami.\n",
             "Du^zy b^ak niebezpiecznie kr^eci si^e ko^lo twojego ucha.\n",
             "W powietrzu rozchodzi si^e przyjemny zapach kwiat^ow i trawy.\n",
             "Bogatokolorowy motyl beztrosko przelecia^l nad trawami.\n",
             "W^sr^od kwiat^ow dostrzegasz krz^ataj^ace si^e pszczo^ly.\n"})[random(15)];
         case MT_JESIEN:
             return ({"Drapie�ny ptak nagle pikuje w d� atakuj�c upatrzon� "+
             "zwierzyn�.\n",
             "Ma�a mr�wka pracowicie wspina si� po �d�ble trawy, ale nag�y "+
             "podmuch wiatru str�ca j� z powrotem na ziemi�.\n",
             "W powietrzu unosz� si� chmary drobnych muszek, kt�re "+
             "zaczynaj� k�sa� bardzo bole�nie.\n",
             "Klucz dzikich g�si osiada na ��kach aby odpocz�� przed "+
             "dalszym lotem na po�udnie.\n",
             "Stado dzikich ptak�w zbiera si� do odlotu na po�udnie "+
             "nawo�uj�c si� kr�tkim pokrzykiwaniem.\n",
             "Wielka mucha przelecia^la ci tu^z ko^lo nosa.\n",
             "Polna myszka przebieg^la niedaleko twoich st^op.\n",
             "Dochodzi ci^e ciche popiskiwanie. Czy^zby jaka^s mysz?\n",
             "Niewielki je^z przetupta^l niedaleko ciebie i znikn^a^l "+
             "pomi^edzy trawami.\n"})[random(9)];
         case MT_ZIMA:
             return ({"Stado kracz�cych wron siada na �niegu i przygl�da "+
             "ci si� �akomym wzrokiem.\n",
             "S�ycha� skrzypienie i trzaskanie marzn�cych drzew.\n"})[random(2)];
    }
}
string
pora_roku_noc()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Wielka mucha przelecia^la ci tu^z ko^lo nosa.\n",
             "Polna myszka przebieg^la niedaleko twoich st^op.\n",
             "Dochodzi ci^e ciche popiskiwanie. Czy^zby jaka^s mysz?\n"})[random(3)];
         case MT_JESIEN:
             return ({"Wielka mucha przelecia^la ci tu^z ko^lo nosa.\n",
             "Polna myszka przebieg^la niedaleko twoich st^op.\n",
             "Dochodzi ci^e ciche popiskiwanie. Czy^zby jaka^s mysz?\n"})[random(3)];
         case MT_ZIMA:
             return ({"S�ycha� skrzypienie i trzaskanie marzn�cych drzew.\n"})[random(1)];
    }
}








/*----------Opis�w by�o chyba z pi��. W tym opis Faeve i Tinardan,
 * wi�cej grzech�w nie pami�tam,
 * Vera
 */
string
opis_polmroku()
{
    string str;

    if(pora_roku() == MT_ZIMA || pora_roku() == MT_JESIEN)
    {
        if(CZY_JEST_SNIEG(this_object()))
            str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
                "le��cego na ziemi.\n";
        else
            str="Nad ��k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
                "zbyt wiele.\n";
    }
    else
        str=losuj_kurwa_long();

    return str;
}



//UNIWERSALNE DLA NOCY PR�CZ ZIMY
string str1noc_ogolny = "Ciemno�� tylko pot�guje "+
            "monotoni� tego miejsca. Jest tak "+
            "ciemno, �e nic praktycznie nie wida�, jedynie w odleg�o�ci nie "+
            "wi�kszej ni� wyci�gni�cie r�ki - jakie� ro�liny i pozamykane "+
            "na noc p�czki kwiat�w. Nietrudno wdepn�� w dziur�, czy te� "+
            "odchody dzikich zwierz�t. O tym, �e jeste� na ��ce "+
            "przypominaj� jedynie pl�cz�ce si� wok� n�g �odygi ro�lin, "+
            "jednak w ciemno�ciach nie spos�b rozpozna� ich gatunk�w. "+
            "Gdzie� na wschodzie majacz� mury miejskie, od "+
            "kt�rych bije niedu�a �una pochodni i latar� ulicznych. ";

string
losuj_kurwa_long()
{
    string str;
    string *str1, *str2, *str3;

    //W �rodku s� warunki czy dzie� czy noc
    if(pora_roku() == MT_ZIMA)
    {

        str1=({"Monotonia ��ki zim� jest beznami�tna. ", //5+0pustych
           "��ka zim� jest beznami�tna. ",
           "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz pokryte puchow^a "+
           "pierzyn^a po^lacie bezkresnych ^l^ak. ",
           "Dooko^la s^lycha^c ciche popiskiwanie dzikich zwierz^atek, "+
           "kt^ore zwinnie poruszaj^a si^e pomi^edzy wysokimi zaro^slami, "+
           "sprawnie omijaj^ac przer^o^zne przeszkody. ",
           "Dooko^la s^lycha^c ciche popiskiwanie dzikich zwierz^atek, kt^ore "+
           "zwinnie poruszaj^a si^e pomi^edzy wysokimi zaro^slami, sprawnie "+
           "omijaj^ac przer^o^zne przeszkody. ",
           });
        if(jest_dzien())
        {
           str1+=({"W chwilach, gdy na niebie pojawia si� s�o�ce, jego promienie "+
           "dodaj� temu miejscu nieco rado�ci i �ycia - odbijaj� si� weso�o "+
           "na �niegu, skrz�c niczym ma�e brylanciki. "});
           str1+=({"Dooko^la s^lycha^c ciche popiskiwanie dzikich "+
           "zwierz^atek, kt^ore zwinnie poruszaj^a si^e pomi^edzy wysokimi "+
           "zaro^slami, sprawnie omijaj^ac przer^o^zne przeszkody. W chwilach, "+
           "gdy na niebie pojawia si� s�o�ce, jego promienie dodaj� temu "+
           "miejscu nieco rado�ci i �ycia - odbijaj� si� weso�o na �niegu, "+
           "skrz�c niczym ma�e brylanciki. "});
        }


        //6+1puste
        str2=({"Gruba warstwa zlodowacia^lego ^sniegu pokrywa ^l^aki poczynaj^ac "+
           "od muru Rinde na wschodzie, a^z do lasu ciemniej^acego na "+
           "po^ludniowym-zachodzie. ",
           "Wszystko dooko�a jest takie same, dopiero na zachodzie wida� "+
           "�cian� lasu. ",
           "Na po^ludniowym-wschodzie ten monotonny krajobraz przecina ciemna "+
           "kresa traktu okolonego rz^edami dostojnych, pot^e^znych drzew. ",
           "Pomijaj�c mury miasta biel�ce si� na wschodzie, okolica wygl�da "+
           "na wymar��. ",
           ""});
        if(jest_dzien())
        {
            str2+=({"Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy od "+
           "przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
           "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. "});
           str2+=({"Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy od "+
           "przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
           "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W przeciwnym "+
           "kierunku, ponad trawami i pojedynczymi krzakami, wy�aniaj^a "+
           "si^e mury miasta. "});
        }

        //4+2puste
        str3=({"Wok� roztacza si� ogromna �nie�na pustynia. ",
           "Wok� roztacza si� ogromna bia�a pustynia. Gdzieniegdzie spod "+
           "�nie�nej pierzyny wystaj� jakie� suche �odygi traw i kwiat�w. ",
           "Pokryte ^sniegiem ro^sliny i wysokie trawy, kt^orych suche ko^nce "+
           "wystaj^a ponad bia^ly ko�uch, z niecierpliwo^sci^a oczekuj^ac na "+
           "pierwsze oznaki wiosny. ",
           "Nieliczne tropy zwierz^at, kieruj^ace si^e g^l^ownie w stron^e "+
           "niewielkiej zaspy le^z^acej na skraju lasu tworz^a na tej "+
           "ol^sniewaj^acej powierzchni asymetryczny wz^or. ",
           "",""});

        if(!jest_dzien())
        {
            str3+=({"Pomijaj�c mury miasta w dali na "+
            "wschodzie, od kt�rych bije niedu�a �una pochodni i "+
            "latar� ulicznych, okolica wygl�da na wymar��."});
            str3+=({"Mroczny zarys pobliskiego "+
            "pobliskiego lasu maluje si^e gdzie^s daleko na zachodzie. "+
            "W przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
            "wy�aniaj^a si^e mury miasta."});
        }

        str=str1[random(sizeof(str1))]+
            str2[random(sizeof(str2))]+
            str3[random(sizeof(str3))];

    }
    if(pora_roku() == MT_WIOSNA)
    {

        str1=({"To miejsce jest paradoksalne - przenikaj� si� tu "+
            "monotonia i r�norodno��. Nie jest w �aden spos�b "+
            "charakterystyczne, wr�cz przeciwnie - wszystko jest takie same "+
            "- wok� rozpo�ciera si� ogromna ��ka, a dopiero gdzie� na "+
            "po�udniowo-zachodniej cz�ci horyzontu widnieje las. "+
            "R�norodno�� za� zawiera si� w niezwyk�ej rozmaito�ci ro�lin "+
            "i barw przeplataj�cych si� nawzajem. Jest tu ca�e mn�stwo "+
            "mak�w i b�awatk�w, poprzetykanych ��t� naw�oci� i "+
            "zielonkawymi k�osami traw. ",
            "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone "+
            "po^lacie bezkresnych ^l^ak. ",
            "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone "+
            "po^lacie bezkresnych ^l^ak. Bezwiednie ko^lysz^ace si^e na "+
            "wietrze ro^sliny u^spione i skulone spokojnie czekaj^a na "+
            "powr^ot s^lo^nca wraz z ^zyciodajnym ^swiat^lem. ",
            });

        if(jest_dzien())
        {
            str1+=({
                "Niska, jasnozielona murawa pokrywaj^aca delikatnym "+
            "kobiercem ziemi^e, przetykana gdzieniegdzie plamami "+
            "ciemniejszej zieleni k^ep jakiego^s zielska skrzy si^e "+
            "ca^la od rosy pokrywaj^acej ka^zd^a trawk^e i barwne "+
            "g^l^owki kwitn^acych kwiat^ow. W tej soczystej obfito^sci "+
            "ro^slin chroni^a si^e liczne owady, kt^orych jednostajne "+
            "brz^eczenie i cykanie wraz ze ^spiewem bujaj^acych wysoko "+
            "na niebie skowronk^ow tworzy niepowtarzaln^a muzyk^e "+
            "bezkresnych, zielonych ^l^ak rozci^agaj^acych si^e na "+
            "p^o^lnocy a^z po horyzont. ",
            "Niska, jasnozielona murawa pokrywaj^aca delikatnym "+
            "kobiercem ziemi^e, przetykana gdzieniegdzie plamami "+
            "ciemniejszej zieleni k^ep jakiego^s zielska skrzy si^e "+
            "ca^la od rosy pokrywaj^acej ka^zd^a trawk^e i barwne "+
            "g^l^owki kwitn^acych kwiat^ow. W tej soczystej obfito^sci "+
            "ro^slin chroni^a si^e liczne owady, kt^orych jednostajne "+
            "brz^eczenie i cykanie wraz ze ^spiewem bujaj^acych wysoko "+
            "na niebie skowronk^ow tworzy niepowtarzaln^a muzyk^e "+
            "bezkresnych, zielonych ^l^ak rozci^agaj^acych si^e na "+
            "p^o^lnocy a^z po horyzont. Bezkres tej r^owniny m^aci "+
            "tylko majacz^acy na wschodzie ciemny mur "+
            "Rinde. "});
        }
        else
        {
            str1+=({"Niska, ciemna murawa pokrywaj^aca delikatnym kobiercem "+
                "ziemi^e, przetykana gdzieniegdzie plamami ciemniejszej "+
                "zieleni k^ep jakiego^s zielska l^sni w blasku ksi^e^zyca "+
                "od rosy pokrywaj^acej ka^zd^a trawk^e i barwne g^l^owki "+
                "kwitn^acych kwiat^ow. W tej soczystej obfito^sci ro^slin "+
                "chroni^a si^e liczne owady, kt^orych jednostajne cykanie "+
                "wraz z pohukiwaniem przelatuj^acych bezszelestnie s^ow "+
                "tworzy niepowtarzaln^a muzyk^e bezkresnych, ciemnych ^l^ak "+
                "rozci^agaj^acych si^e na p^o^lnocy a^z po horyzont. "+
                "Bezkres tej r^owniny m^aci tylko majacz^acy na "+
                "wschodzie ciemny mur Rinde. ",
                "Niska, ciemna murawa pokrywaj^aca delikatnym kobiercem "+
                "ziemi^e, przetykana gdzieniegdzie plamami ciemniejszej "+
                "zieleni k^ep jakiego^s zielska l^sni w blasku ksi^e^zyca "+
                "od rosy pokrywaj^acej ka^zd^a trawk^e i barwne g^l^owki "+
                "kwitn^acych kwiat^ow. W tej soczystej obfito^sci ro^slin "+
                "chroni^a si^e liczne owady, kt^orych jednostajne cykanie "+
                "wraz z pohukiwaniem przelatuj^acych bezszelestnie s^ow "+
                "tworzy niepowtarzaln^a muzyk^e bezkresnych, ciemnych ^l^ak "+
                "rozci^agaj^acych si^e na p^o^lnocy a^z po horyzont. "+
                "Bezkres tej r^owniny m^aci tylko majacz^acy na "+
                "wschodzie ciemny mur Rinde. Niewysokie, "+
                "spl^atane zaro^sla majacz^ace ciemn^a plam^a na tle lasu "+
                "na po^ludniowym-zachodzie daj^a schronienie wielu ptakom i "+
                "drobnej "+
                "zwierzynie. Doskonale widoczny w blasku ksi^e^zyca rz^ad "+
                "dostojnych, grubych drzew wyznacza ju^z z daleka tras^e "+
                "traktu majacz^acego w oddali na po^ludniowym-"+
                "wschodzie. "});
        }


        //3+2puste
        str2=({"Dooko^la s^lycha^c ciche popiskiwanie "+
          "dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e pomi^edzy "+
          "wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne przeszkody. ",
          "Mroczny zarys pobliskiego lasu maluje si^e gdzie^s "+
          "daleko na zachodzie. W przeciwnym kierunku, ponad trawami i "+
          "pojedynczymi krzakami, wy�aniaj^a si^e mury miasta. ",
          "Niewysokie, spl^atane zaro^sla pokryte zaledwie "+
          "mgie^lk^a zieleni tworz^a na po^ludniowym-zachodzie, na "+
          "skraju por^eby "+
          "zwarty mur daj^acy schronienie wielu ptakom i drobnej "+
          "zwierzynie. ","",""});


        //1+3puste
        str3=({"","",""});
        if(jest_dzien())
        {
            str3+=({"Rz^ad dostojnych, grubych drzew okrytych "+
               "drobnymi, zielonymi listeczkami wyznacza ju^z z daleka "+
               "tras^e traktu majacz^acego w oddali na po^ludniowym-"+
               "wschodzie."});


            str=str1[random(sizeof(str1))]+
                str2[random(sizeof(str2))]+
                str3[random(sizeof(str3))];

        }
        else
            str=random(2)?str1[random(sizeof(str1))]:str1noc_ogolny+
            str2[random(sizeof(str2))]+
            str3[random(sizeof(str3))];
    }

    if(pora_roku() == MT_LATO)
    {

        str1 = ({
            "To miejsce jest paradoksalne - przenikaj� si� tu "+
            "monotonia i r�norodno��. Nie jest w �aden spos�b "+
            "charakterystyczne, wr�cz przeciwnie - wszystko jest takie same "+
            "- wok� rozpo�ciera si� ogromna ��ka, a dopiero gdzie� na "+
            "po�udniowo-zachodniej cz�ci horyzontu widnieje las. "+
            "R�norodno�� za� zawiera si� w niezwyk�ej rozmaito�ci ro�lin "+
            "i barw przeplataj�cych si� nawzajem. Jest tu ca�e mn�stwo "+
            "mak�w i b�awatk�w, poprzetykanych ��t� naw�oci� i "+
            "zielonkawymi k�osami traw. ",
            "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone po^lacie "+
            "bezkresnych ^l^ak. ",
            "Zapach siana, zi^o^l i kwiat^ow przesyca lekko, "+
            "rozgrzane powietrze pe^lne brz^eczenia i cykania "+
            "wsz^edobylskich owad^ow i ^swiergotliwego jazgotu "+
            "uganiaj^acych si^e za nimi ptak^ow. ",
            "Rozci^agaj^ace si^e a^z po p^o^lnocny horyzont morze coraz "+
            "wy^zszych traw faluje w rytm powiew^ow wiatru, a z jego "+
            "trawiastych fal wy^lania si^e na wschodzie jak "+
            "okr^et mur Rinde. ",
            "Rozci^agaj^ace si^e a^z po p^o^lnocny horyzont morze coraz "+
            "wy^zszych traw faluje w rytm powiew^ow wiatru, a z jego "+
            "trawiastych fal wy^lania si^e na wschodzie jak "+
            "okr^et mur Rinde. "});

        if(jest_dzien())
            str1+=({
                "Zapach siana, zi^o^l i kwiat^ow przesyca lekko, "+
                "rozgrzane powietrze pe^lne brz^eczenia i cykania "+
                "wsz^edobylskich owad^ow i ^swiergotliwego jazgotu "+
                "uganiaj^acych si^e za nimi ptak^ow. ",
                "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone po^lacie "+
                "bezkresnych ^l^ak. Kolorowe kwiaty, szumi^a cicho poruszone "+
                "podmuchem ciep^lego wiatru, kt^ory delikatnie muska twoj^a twarz, "+
                "przypominaj^ac o nieustaj^acym upale. "});
        else
           str1+=({
                "Kwiaty, szumi^a cicho ,poruszone podmuchem "+
                "ch^lodnego wiatru, kt^ory delikatnie muska twoj^a twarz, "+
                "och^ladzaj^ac przy tym cia^lo po nieustaj^acym upale. ",
                "Zapach siana, zi^o^l i kwiat^ow przesyca lekko, "+
                "drgaj^ace ponad ^l^akami, rozgrzane powietrze pe^lne "+
                "cykania wsz^edobylskich owad^ow i pohukiwania s^ow. "});



        //3+1puste
        str2 =({"Dooko^la s^lycha^c ciche "+
          "popiskiwanie dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e "+
          "pomi^edzy wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne "+
          "przeszkody. ",
          "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
          "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
          "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. ",
          ""       });
        if(jest_dzien())
            str2+=({
                   "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
                   "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
                   "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W "+
                   "przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
                   "wy�aniaj^a si^e mury miasta. "});
        else
           str2+=({
                   "Mroczny zarys pobliskiego pobliskiego "+
                   "lasu maluje si^e gdzie^s daleko na zachodzie. W przeciwnym "+
                   "kierunku, ponad trawami i pojedynczymi krzakami, wy�aniaj^a "+
                   "si^e mury miasta. "});

         //2+2puste
         str3 =({"Drzewa rosn^ace wzd^lu^z widocznego "+
                "na po^ludniowym-wschodzie traktu pokryte g^estym listowiem "+
                "i kwiatami wydzielaj^a s^lodki intensywny zapach "+
                "docieraj^acy chwilami z podmuchami wiatru a^z tu. ",
                "",""});
         if(jest_dzien())
            str3+=({"Kolczaste zaro^sla na po^ludniowym-"+
                "zachodzie, "+
                "obsypane bia^lymi kwiatkami i czerwonymi owocami ca^le a^z "+
                "dr^z^a od bezustannego ptasiego ^swiergotu milkn^acego "+
                "tylko wtedy gdy na niebie pojawia si^e majestatyczna "+
                "sylwetka myszo^lowa. "});
         else
            str3+=({
                "Kolczaste zaro^sla na "+
                "wschodzie, majacz^ace ciemn^a plam^a na tle ponurego lasu "+
                "na po^ludniowym-zachodzie ^spi^a teraz spokojnie. ",
                "Drzewa rosn^ace "+
                "wzd^lu^z widocznego na po^ludniowym-wschodzie traktu "+
                "b^lyszcz^ace jak srebrne kolumny w blasku ksi^e^zyca "+
                "wydzielaj^a s^lodki intensywny zapach docieraj^acy "+
                "chwilami z podmuchami wiatru a^z tu. ",
                "Kolczaste zaro^sla na "+
                "wschodzie, majacz^ace ciemn^a plam^a na tle ponurego lasu "+
                "na po^ludniowym-zachodzie ^spi^a teraz spokojnie. Drzewa "+
                "rosn^ace "+
                "wzd^lu^z widocznego na po^ludniowym-wschodzie traktu "+
                "b^lyszcz^ace jak srebrne kolumny w blasku ksi^e^zyca "+
                "wydzielaj^a s^lodki intensywny zapach docieraj^acy "+
                "chwilami z podmuchami wiatru a^z tu. "});

        if(jest_dzien())
            str=str1[random(sizeof(str1))]+
                str2[random(sizeof(str2))]+
                str3[random(sizeof(str3))];
        else
            str=random(2)?str1[random(sizeof(str1))]:str1noc_ogolny+
                          str2[random(sizeof(str2))]+
                          str3[random(sizeof(str3))];
    }

    if(pora_roku() == MT_LATO)
    {

        str1=({
            "To miejsce jest paradoksalne - przenikaj� si� tu "+
            "monotonia i r�norodno��. Nie jest w �aden spos�b "+
            "charakterystyczne, wr�cz przeciwnie - wszystko jest takie same "+
            "- wok� rozpo�ciera si� ogromna ��ka, a dopiero gdzie� na "+
            "po�udniowo-zachodniej cz�ci horyzontu widnieje las. "+
            "R�norodno�� za� zawiera si� w niezwyk�ej rozmaito�ci ro�lin i "+
            "barw przeplataj�cych si� nawzajem. Jest tu ca�e mn�stwo "+
            "��tawych i fioletowawych drobniutkich fio�k�w, "+
            "fioletowo-niebieskich b�awatk�w, ��tych naw�oci i wrotyczy, "+
            "a gdzieniegdzie biel� si� i r�owi� zebrane w grona male�kie "+
            "kwiatki krwawnika. ",
            "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz ^z^o^lte po^lacie "+
            "bezkresnych ^l^ak. ",
            "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz ^z^o^lte po^lacie "+
          "bezkresnych ^l^ak. Zwi^edni^ete i po^z�^lk^le ju^z ro^sliny, "+
          "szumi^a cicho poruszone ch^lodnym wiatrem, kt^ory przypomina o "+
          "zbli^zaj^acej si^e zimie. ",
          "Rozci^agaj^aca si^e a^z po horyzont, bezkresna r^ownina "+
            "pokryta namok^lymi trawami wydziela wo^n butwiej^acych "+
            "ro^slin, a wyp^lowia^le od wiatru i deszczu k^epy suchych "+
            "badyli, kt^ore opar^ly si^e apetytowi pas^acych si^e tutaj "+
            "zwierz^at chwiej^a si^e sm^etnie w podmuchach wiatru. "});

        str2=({"Jak olbrzymie zwierz^e przyczajone w ciemno^sci majaczy po "+
                "wschodniej stronie r^owniny ciemny, zamglony "+
                "mur Rinde. ",
               "Jak olbrzymie zwierz^e przyczajone w ciemno^sci majaczy po "+
                "wschodniej stronie r^owniny ciemny, zamglony "+
                "mur Rinde. Rozmi^ek^l^a, b^lotnist^a ziemi^e mlaszcz^ac^a "+
                "pod stopami przy najdrobniejszym poruszeniu pokrywa jaka^s "+
                "o^slizg^la mi^ekka masa. ",
               "Dooko^la s^lycha^c ciche popiskiwanie "+
               "dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e pomi^edzy "+
               "wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne przeszkody. ",
               "",""});
        str3=({
               "Ja^sniejsz^a plam^a majacz^a na "+
               "tle ciemnego lasu kolczaste zaro^sla le^z^ace na skraju "+
               "por^eby. ",
               "Ja^sniejsz^a plam^a majacz^a na "+
               "tle ciemnego lasu kolczaste zaro^sla le^z^ace na skraju "+
               "por^eby. Dostojne drzewa chroni^ace drapie^znymi, "+
               "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
               "butwiej^acymi ^l^akami na po�udniu. ",
               "Dostojne drzewa chroni^ace drapie^znymi, "+
               "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
               "butwiej^acymi ^l^akami gdzie^s na po^ludniu. ",
               ""});
        if(jest_dzien())
            str3+=({
               "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
               "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
               "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W "+
               "przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
               "wy�aniaj^a si^e mury miasta. ",
               "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
               "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
               "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. "
               });
        else
            str3+=({""});



        if(jest_dzien())
            str=str1[random(sizeof(str1))]+
                str2[random(sizeof(str2))]+
                str3[random(sizeof(str3))];
        else
            str=random(2)?str1[random(sizeof(str1))]:str1noc_ogolny+
                          str2[random(sizeof(str2))]+
                          str3[random(sizeof(str3))];
    }

    str+="\n";
    return str;

}
