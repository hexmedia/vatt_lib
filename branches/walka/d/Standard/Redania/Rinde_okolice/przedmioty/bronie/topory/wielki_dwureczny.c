inherit "/std/weapon";

#include <materialy.h>
#include <object_types.h>
#include <stdproperties.h>
#include <wa_types.h>

void create_weapon()
{
    ustaw_nazwe("topor");
    
    dodaj_przym("wielki", "wielcy");
    dodaj_przym("dwur�czny", "dwur�czni");
    
    set_long("Top�r ten jest zaopatrzony w niezwyk�ej wielko�ci"
	    +" ostrze. Mniej wi�cej na jego �rodku zauwa�asz"
	    +" kilka dziur o sporej �rednicy - dzi�ki nim bro�"
	    +" ma nieco mniejsza wag�. Top�r z pewno�ci� nie nale�y"
	    +" do najpor�cznieszych, jednak przy odpowiedniej sile"
	    +" i umiej�tno�ciach mo�e stanowi� gro�ny or�.\n");
    
    set_type(O_BRON_TOPORY_2H);
    ustaw_material(MATERIALY_STAL, 85);
    ustaw_material(MATERIALY_DR_DAB, 15);
    
    add_prop(OBJ_I_WEIGHT, 9500);
    add_prop(OBJ_I_VOLUME, 3500);
    add_prop(OBJ_I_VALUE, 3345);
    
    set_hit(20);
    set_pen(36);
    set_wt(W_AXE);
    set_dt(W_SLASH);
    set_hands(W_BOTH);
    
}