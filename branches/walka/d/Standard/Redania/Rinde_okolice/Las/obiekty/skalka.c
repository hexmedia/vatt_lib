/* Autor: Avard
   Opis : Tinardan
   Data : 29.11.06 */
inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
#include <ss_types.h>
#include <sit.h>
#include "dir.h"

int wespnij(string str);

create_object()
{
    ustaw_nazwe("skalka");
    dodaj_nazwy("skala");
    dodaj_nazwy("glaz");
    dodaj_przym("szary","szarzy");
    dodaj_przym("powyginany","powyginani");

    set_long("@@dlugasny@@");
    
    add_prop(OBJ_I_WEIGHT, 500000);
    add_prop(OBJ_I_VOLUME, 500000);
    add_prop(OBJ_I_VALUE, 50);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_I_NO_GET, "Nie dasz rady jej wzi^a^c.\n");

    make_me_sitable("przy","przy ska�ce","spod ska�ki",4);
    //FIXME ustaw_material(MATERIALY_DR_BUK);
    set_type(O_INNE);
    seteuid(getuid());
set_no_show_composite(1);
}
string
dlugasny()
{
    string str;
    object *kto;
    str = "A^z dziw bierze jak niezwyk^le formy potrafi przybiera^c natura. "+
        "Ta ska^lka wygl^ada, jakby by^la dzie^lem jakiego^s szalonego "+
        "rze^xbiarza. Powyginana w ^luki i kopu^ly, przez d^lugie lata "+
        "kszta^ltowa^la j^a zbieraj^aca si^e w zag^l^ebieniach woda. Z "+
        "jednej strony utworzy^lo si^e co^s w rodzaju skalnego komina, "+
        "pustej dziury, przez kt^or^a mo^zna si^e przecisn^a^c, by dosta^c "+
        "si^e na g^or^e. Ska^lka nie jest co prawda zbyt wysoka � nie "+
        "si^ega nawet koron wy^zszych drzew, ale po stopniu wy^slizgania "+
        "ska^ly mo^zna pozna^c, ^ze nie jeden amator ^latwej wspinaczki "+
        "pozostawi^l sw^oj ^slad na szczycie. ";
kto = FILTER_LIVE(all_inventory(find_object(LAS_RINDE+"lokacje/na_skalce")));
    if (sizeof(kto) > 0)
    {
	    str +="Na ska^lce widzisz " + COMPOSITE_LIVE(kto, PL_BIE) + ".";
    }
    str +="\n";
    return str;
}
public void
init()
{
    ::init();                                                                  
    add_action(wespnij,"wespnij");
    add_action(wespnij,"wejd�");
}

int
wespnij(string str)
{
    object *kto, *skalka;

    if(query_verb() == "wespnij")
        notify_fail("Wespnij si� gdzie?\n");
    else
        notify_fail("Wejd� gdzie?\n");

    
    if(!strlen(str))
       return 0;

    if(!parse_command(str, environment(TP), "'si^e' 'na' %i:" +PL_BIE,skalka) &&
        !parse_command(str, environment(TP), "'na' %i:" +PL_BIE,skalka))
        return 0;
        
    if(!HAS_FREE_HANDS(TP))
    {
       write("Musisz mie^c obie d^lonie wolne, aby m^oc to zrobi^c.\n");
       return 1;
    }

    if (!skalka) return 0;
    if (!str) return 0;
    if (skalka[0][1] != TO)
        return 0;

	if(TP->query_prop(SIT_LEZACY) || TP->query_prop(SIT_SIEDZACY))
	{
		write("Najpierw wsta�!\n");
		return 1;
	}
		
	if(TP->query_fatigue() <= 25)
	{
	   write("Nie masz ju� na to si�y.\n");
       return 1;
	}

        write("Szukasz dogodnego uchwytu i rozpoczynasz wspinaczk^e.\n");
        saybb(QCIMIE(TP, PL_MIA) + " zaczyna wspina^c si^e "+
            "na ska^lk^e.\n");
        if(environment(TO)->pora_roku() == MT_ZIMA &&
           TP->query_skill(SS_CLIMB) <= 6)
	    {
            clone_object(LAS_RINDE+"obiekty/paraliz_wspina_sie_nie")->move(TP);
	        return 1;
	    }
        if(environment(TO)->pora_roku() != MT_ZIMA &&
           TP->query_skill(SS_CLIMB) <= 3)
        {
            clone_object(LAS_RINDE+"obiekty/paraliz_wspina_sie_nie")->move(TP);
            return 1;
        }
	    if(environment(TO)->pora_roku() == MT_ZIMA && 
           TP->query_skill(SS_CLIMB) >= 7 && 
           TP->query_skill(SS_CLIMB) <= 10)
	    { 
	        clone_object(LAS_RINDE+"obiekty/paraliz_wspina_sie_prawie")->move(TP);
	        return 1;
	    }
        if(environment(TO)->pora_roku() != MT_ZIMA && 
           TP->query_skill(SS_CLIMB) >= 4 && 
           TP->query_skill(SS_CLIMB) <= 6)
        {
            clone_object(LAS_RINDE+"obiekty/paraliz_wspina_sie_prawie")->move(TP);
	        return 1;
	    }
        kto = FILTER_LIVE(all_inventory(find_object(LAS_RINDE+"lokacje/na_skalce")));
        if (sizeof(kto) == 2)
        {
            write("Po chwili wspinaczki zdajesz sobie spraw^e, ^ze ju^z "+
                "si^e tam nie zmie^scisz i rezygnujesz.\n");
            saybb(QCIMIE(TP, PL_MIA) + "po chwili wspinaczki "+
                "zeskakuje na ziemi^e.\n");
            return 1;
        }

    clone_object(LAS_RINDE+"obiekty/paraliz_wspina_sie")->move(TP);
    return 1;      
    //}
}
