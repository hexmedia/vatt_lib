inherit "/std/container";

#include <macros.h>
#include <materialy.h>
#include <pl.h>
#include <object_types.h>
#include <stdproperties.h>

#include "dir.h"

void create_container()
{
	ustaw_nazwe("kosz");
	
	dodaj_przym("wysoki", "wysocy");
	dodaj_przym("metalowy", "metalowi");
	
	set_long("Kilka metalowych obr�czy po��czono razem za pomoc� pr�t�w,"
			+" tworz�c w ten spos�b konstrukcj� jednocze�nie prost� i dobrze"
			+" spe�niaj�c� swoj� rol�.\n");
	
	add_prop(CONT_I_WEIGHT, 6300);
	add_prop(CONT_I_VOLUME, 24000);
	
	ustaw_material(MATERIALY_ZELAZO, 100);
	set_type(O_MEBLE);
	
	setuid();
	seteuid(getuid());
	
	add_object(RINDE+"przedmioty/narzedzia/zwykla_stalowa_siekiera");
	add_object(RINDE+"przedmioty/narzedzia/zwykla_stalowa_siekiera");
	add_object(RINDE+"przedmioty/narzedzia/zwykla_stalowa_siekiera");
	add_object(RINDE+"przedmioty/narzedzia/zwykla_stalowa_siekiera");
	add_object(RINDE+"przedmioty/narzedzia/zwykla_stalowa_siekiera");
	add_object(RINDE+"przedmioty/narzedzia/zwykla_stalowa_siekiera");
	add_object(RINDE+"przedmioty/narzedzia/zwykla_stalowa_siekiera");
	add_object(RINDE+"przedmioty/narzedzia/zwykla_stalowa_siekiera");
	
	add_object(RINDE+"przedmioty/narzedzia/dluga_zebata_pila");
	add_object(RINDE+"przedmioty/narzedzia/dluga_zebata_pila");
	add_object(RINDE+"przedmioty/narzedzia/dluga_zebata_pila");
	add_object(RINDE+"przedmioty/narzedzia/dluga_zebata_pila");
	add_object(RINDE+"przedmioty/narzedzia/dluga_zebata_pila");
	add_object(RINDE+"przedmioty/narzedzia/dluga_zebata_pila");
	
	set_alarm_every_hour("odnow_zawartosc");
}

int  prevent_leave(object ob)
{
    object tartak = find_object("/d/Standard/Redania/Rinde_okolice/Las/lokacje/tartak");
	object npc = present("raghol", ENV(TO));
	
	if(!npc)
		return 0;
	
	if(tartak->query_zabral(this_player()->query_real_name()) > 1)
	{
		npc->command("powiedz do "+OB_NAME(this_player())+" Ty� ju� swoje"
					+" st�d zabra�"+this_player()->koncowka("", "a")+"!");
		return 1;
	}
	else
	{
		tartak->set_zabral(this_player()->query_real_name(),
                tartak->query_zabral(this_player()->query_real_name()) + 1);
		return 0;
	}
}

int
prevent_enter(object ob)
{
    object tartak = find_object("/d/Standard/Redania/Rinde_okolice/Las/lokacje/tartak");
    object npc = present("raghol", ENV(TO));

    if(!npc)
        return 0;

    if ((ob->query_nazwa() ~= "siekiera") || (ob->query_nazwa() ~= "pi�a"))
	{
		tartak->decrease_zabral(this_player()->query_real_name());
        return 0;
	}

    npc->command("powiedz do "+OB_NAME(this_player())+" Tw�j kosz, �e jakie� �mieci tam pchasz?");
    tartak->set_zabral(this_player()->query_real_name(), 2);
    return 1;
}

int
prevent_leave_env(object ob)
{
    object tartak = find_object("/d/Standard/Redania/Rinde_okolice/Las/lokacje/tartak");
    object npc = present("raghol", ENV(TO));

	if(!npc)
		return 0;

    npc->command("powiedz do "+OB_NAME(this_player())+" �apy wara, bo je ur�n�!");
    tartak->set_zabral(this_player()->query_real_name(), 2);
    return 1;
}

void odnow_zawartosc()
{
    if(!objectp(ENV(TO)))
            return;

    object tartak = find_object("/d/Standard/Redania/Rinde_okolice/Las/lokacje/tartak");
    object npc = present("raghol", ENV(TO));
	object *inv = all_inventory(this_object());
	int inv_size = sizeof(inv);
	int ile_siekier, ile_pil;

	if((!npc) || (ENV(TO) != tartak))
		return;
	
	for(int i=0; i < inv_size; i++)
	{
		if(inv[i]->query_nazwa() ~= "siekiera")
			ile_siekier++;
		if(inv[i]->query_nazwa() ~= "pi�a")
			ile_pil++;
	}

    if ((ile_siekier + ile_pil) >= 14) 
        return;

    npc->command("emote dok�ada narz�dzia do " + TO->short(PL_DOP) + ".");
	
	for(int i=0; i < (8-ile_siekier); i++)
		clone_object(RINDE+"przedmioty/narzedzia/zwykla_stalowa_siekiera")->move(this_object());
	
	for(int i=0; i < (6-ile_pil); i++)
		clone_object(RINDE+"przedmioty/narzedzia/dluga_zebata_pila")->move(this_object());
}




