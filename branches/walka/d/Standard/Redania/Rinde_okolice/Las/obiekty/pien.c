
/* Autor: Avard
   Opis : Tinardan
   Data : 18.11.06 */

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <mudtime.h>
#include <language.h>

create_object()
{
    ustaw_nazwe("pie^n");
    dodaj_przym("omsza^ly","omszali");
    dodaj_przym("d^ebowy","d^ebowi");

    set_long("Cho^c to drzewo upad^lo ju^z dawno pod ostrzami bezlitosnych "+
        "siekier, nikt nie pokwapi^l si^e by je st^ad zabra^c. Dok^ladnie "+
        "obciosany z ga^l^ezi, ^sci^ety r^owno nikomu na nic si^e nie "+
        "przyda^l. Teraz cenne d^ebowe drewno pr^ochnieje i niszczy si^e, "+
        "z^zerane przez korniki, w zimie rozsadzane przez zamarzaj^ac^a "+
        "wod^e, a w lecie butwiej^ace od wilgoci. Kora, niegdy^s mocna i "+
        "chropowata, zamieni^la si^e w delikatny br^azowy py^lek "+
        "kruszej^acy pod ka^zdym dotkni^eciem i naszpikowany r^o^znego "+
        "rodzaju robactwem.\n");

    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 0);
    add_prop(OBJ_I_NO_GET, "To ci si^e chyba nie uda.\n");
    add_prop(OBJ_M_NO_SELL, 1);
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_INNE);
    make_me_sitable("na","na pniu","z pnia", 4); 
}