#pragma strict_types

#include <exp.h>
#include <macros.h>
#include <ss_types.h>
#include "dir.h"

inherit "/std/paralyze";

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta^n");
    set_stop_message("Przestajesz wspina^c si^e na ska^lk^e.\n");
    set_fail_message("Jeste� teraz zaj^et"+TP->koncowka("y","a")+
                     " wspinaniem si^e na ska^lke. Musisz wpisa^c "+
                "'przesta^n' by m^oc zrobi^c, to co chcesz.\n");
    set_finish_object(this_object());
    set_finish_fun("koniec_wspinaczki");
    set_remove_time(6);
    setuid();
    seteuid(getuid());
}

void
koniec_wspinaczki(object player)
{
    player->catch_msg("Powoli wygrzebujesz si^e na g^or^e i rozgl^adasz "+
        "dooko^la.\n");
    saybb(QCIMIE(player,PL_MIA)+" ko^nczy wspinaczk^e.\n");
    player->add_fatigue(-25);
    player->increase_ss(SS_CLIMB,EXP_WSPINAM_UDANY_CLIMB);
    player->increase_ss(SS_STR,EXP_WSPINAM_UDANY_CLIMB_STR);
    player->increase_ss(SS_DEX,EXP_WSPINAM_UDANY_CLIMB_DEX);
    player->increase_ss(SS_CON,EXP_WSPINAM_UDANY_CLIMB_CON);
    player->move_living("M", LAS_RINDE+"lokacje/na_skalce.c");
    saybb(QCIMIE(player,PL_MIA)+" przybywa z do^lu.\n");
    remove_object();
    return;
}
