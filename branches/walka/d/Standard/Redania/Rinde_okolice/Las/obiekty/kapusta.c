
/* Gnijaca kapusta
   Wykonane przez Avarda dnia 07.07.06 
   Opis by Tinardan i Eria */

inherit "/std/food.c";

#include <pl.h>
#include <macros.h>
#include <object_types.h>
#include <stdproperties.h>

void
create_food()
{
    ustaw_nazwe("kapusta");  
    dodaj_przym("gnij^acy","gnij^acy");

    set_long("Nie jest to bynajmniej ozdoba okolicy i natchnienie poet^ow: "+
        "kapusta wydziela intensywny, mdl^acy zapach i k^l^ebi^a si^e "+
        "wok^o^l niej ca^le stada natr^etnych much. Trzeba przyzna^c, ^ze "+
        "g^l^owka kiedy^s by^la spora i m^og^lby z niej by^c ca^lkiem "+
        "niez^ly obiad, gdyby zachowa^la si^e w stanie nadaj^acym do "+
        "spo^zycia. Jednak w tej chwili szarozielona ma^x nie zach^eca do "+
        "bli^zszego si^e z ni^a zapoznania i raczej odstrasza ni^z "+
        "przyci^aga swoim wygl^adem i zapachem.\n");
    set_amount(100);
}
void
special_effect()
{
    this_player()->set_hp(this_player()->query_hp()-5);
    this_player()->set_fatigue(this_player()->query_fatigue()-50-random(48));
    
}
void
consume_text(object *arr, string vb)
{
    write("Prze^zuwaj^ac, czujesz obrzydliwy smak, a zapach przyprawia ci^e "+
        "o md^lo^sci. Zjedzenie tego chyba nie by^lo najlepszym "+
        "pomys^lem.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " zjada gnij^ac^a kapuste i robi "+
        "si^e zielony.\n");
}
