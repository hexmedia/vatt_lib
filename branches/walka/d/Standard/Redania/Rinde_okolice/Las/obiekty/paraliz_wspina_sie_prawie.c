#pragma strict_types

#include <macros.h>
#include <exp.h>
#include <ss_types.h>
#include "dir.h"

inherit "/std/paralyze";

static int alarm_id;

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta^n");
    set_stop_message("Przestajesz si^e wspina^c na ska^lk^e.\n");
    set_fail_message("Jeste� teraz zaj^et"+TP->koncowka("y","a")+
                     " wspinaniem si^e na ska^lke. Musisz wpisa^c "+
                "'przesta^n' by m^oc zrobi^c, to co chcesz.\n");
    set_finish_object(this_object());
    set_finish_fun("zatrzymanie");
    set_remove_time(4);
    setuid();
    seteuid(getuid());
}

int
zatrzymanie(string str)
{
    write("Jeden fa^lszywy krok i nagle tracisz oparcie, zje^zd^zasz na sam "+
        "d^o^l szoruj^ac plecami po szorstkiej skale. L^adujesz na ziemi z "+
        "hukiem. Przez chwil^e czujesz si^e, jakby tw^oj kr^egos^lup "+
        "pr^obowa^l wydosta^c si^e przez nos.\n");
    saybb(QCIMIE(this_player(), PL_MIA) +" traci oparcie i zje^zdza na d^o^l"+
        " szoruj^ac po skale.\n");
    this_player()->set_hp(this_player()->query_hp()-9);
    TP->add_fatigue(-40);
    TP->increase_ss(SS_CLIMB,EXP_WSPINAM_PRAWIE_UDANY_CLIMB);
    TP->increase_ss(SS_STR,EXP_WSPINAM_PRAWIE_UDANY_CLIMB_STR);
    TP->increase_ss(SS_DEX,EXP_WSPINAM_PRAWIE_UDANY_CLIMB_DEX);
    TP->increase_ss(SS_CON,EXP_WSPINAM_PRAWIE_UDANY_CLIMB_CON);
    remove_object();
    return 1;
}
