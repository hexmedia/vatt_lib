
/* But z odlepiona podeszwa
Wykonano dnia 02.07.06 przez Avarda. 
Opis by Tinardan */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{  
    ustaw_nazwe("but z odlepiona podeszwa");
    dodaj_nazwy("but");

    set_long("O tym bucie wiele mo^zna powiedzie^c, ale na pewno nie to, "+
        "^ze przechodzi w^la^snie sw^oj najlepszy okres. Na grubej sk^orze "+
        "cholewy czas wypisa^l prawdopodobnie wi^ekszo^s^c zdarze^n, kt^ore "+
        "by^ly udzia^lem tego spracowanego obuwia: wida^c wi^ec zielon^a "+
        "smug^e, zapewne po wodorostach, gdy w^la^sciciel brodzi^l w rzece, "+
        "gruby szew biegn^acy wzd^lu^z klamer ^swiadczy o niegdysiejszym "+
        "bliskim spotkaniu z nieprzyjaznym no^zem. Brud wgryz^l si^e w "+
        "niegdy^s g^ladk^a powierzchni^e niczym paso^zyt, tak ^ze nie^latwo "+
        "stwierdzi^c jaki by^l pierwotny kolor sk^ory buta. Teraz natomiast "+
        "jest zgni^loszary.\n");
		
    set_slots(A_L_FOOT);
    add_prop(OBJ_I_VOLUME, 500);
    add_prop(OBJ_I_VALUE, 1);
    add_prop(OBJ_I_WEIGHT, 250);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
    ustaw_material(MATERIALY_SK_SWINIA, 100);
    set_type(O_UBRANIA);
}