inherit "/std/armour";

#pragma no_clone

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>

string *rozwiazali = ({});

void
create_armour() {
    ustaw_nazwe("sygnet");
    dodaj_przym( "du�y", "duzi" );
    dodaj_przym( "srebrny", "srebrni" );

    set_long("Ten wykonany ze srebra sygnet nie jest zbyt wykwintnie wykonany. "+
            "Na jego g�adkiej powierzchni jedynym urozmaiceniem jest ko�lawo wyryta "+
            "litera 'R'.\n");

    set_slots(A_ANY_FINGER);
    set_type(O_BIZUTERIA);
    add_prop(OBJ_I_WEIGHT, 30);
    add_prop(OBJ_I_VOLUME, 30);
    add_prop(OBJ_I_VALUE, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
    ustaw_material(MATERIALY_SREBRO);
    add_prop(ARMOUR_S_DLA_RASY, "cz�owiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
    seteuid(getuid(this_object()));
    restore_object(MASTER);
}

string
stat_object()
{
    string str = ::stat_object();

    str += "\n<<< QUEST ITEM >>>\n";
    str += "quest: sygnet dla raghola\n";
    str += "rozwi�zali:\n";
    foreach (string who : rozwiazali) {
        str += "  " + who + "\n";
    }

    return str;
}

int
is_raghol_sygnet()
{
    return 1;
}

public string
query_auto_load()
{
    return 0;
}

int
check_rozwiazal(string who)
{
    return (member_array(who, rozwiazali) != -1);
}

void
add_rozwiazal(string who)
{
    rozwiazali += ({who});
    seteuid(getuid(this_object()));
    save_object(MASTER);
}
