
/* Autor: Valor
   Opis : Vortak
   Data : 24.03.07 */

inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include "dir.h"

void random_przym(string str, int n);
int rand;

void
create_zwierze()
{
    ustaw_odmiane_rasy("zaj^ac");
    set_gender(G_MALE);
    rand = random(2);
    set_long("@@dlugi@@");
    random_przym("ma^ly:mali drobny:drobni puchaty:puchaci "+
        "szary:szarzy futrzaty:futrzaci||d^lugouchy:d^lugousi "+
        "nerwowy:nerwowi czujny:czujni ospa^ly:ospali",2);
    set_act_time(30);
    add_act("emote nerwowo strzy^ze uszami.");
    add_act("emote skubie pojedy^ncze ^xd^xb^la trawy.");
    add_act("emote kica sobie spokojnie.");
    add_act("emote uwa^znie omiata otoczenie czujnym spojrzeniem.");
    add_act("emote patrzy na ciebie br^azowymi oczami.");
    set_stats (({1, 60, 5, 10, 10, 5}));
    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 5000);
    add_prop(CONT_I_HEIGHT, 30);
   /* set_attack_unarmed(0, 5, 5, W_SLASH,  30, "praw^a ^lap^a");
    set_attack_unarmed(1, 5, 5, W_SLASH,  30, "lew^a ^lap^a");
    set_attack_unarmed(2, 5, 5, W_IMPALE,  40, "z^ebami");*/
    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "g^low^e");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "lew^a �apk�");
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "praw^a �apk�");
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "tyln^a, lew^a �apk�");
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "tyln^a, praw^a �apk�");
    set_random_move(100);
    set_restrain_path(LAS_RINDE_LOKACJE);
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
        case "szturchnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;
    }
}
void
zly(object kto)
{
   TO->run_away();
}
void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
}
string
dlugi()
{
    string str1, str2;
    if(environment(TO)->pora_roku() != MT_ZIMA)
    {
    str1 ="D^lugie na ponad dwie stopy zwierz^atko nerwowo strzy^ze swymi "+
        "uszami nas^luchuj^ac dobiegaj^acych zewsz^ad niepokoj^acych "+
        "odg�os^ow. Zdaje si^e, ^ze starczy jeden nieostro^zny ruch i "+
        "szarak umknie w pop^lochu. Cia^lo pokryte ma szarym futrem, "+
        "dzi^eki czemu prawie idealnie wtapia si^e w otoczenie. Tylnie "+
        "^lapy, zdecydowanie d^lu^zsze ni^z przednie umo^zliwiaj^a "+
        "niepozornemu zwierz^atku ucieczk^e przed du^zo wi^ekszymi "+
        "drapie^znikami. \n";
    }
    else
    {
    str1 ="D^lugie na ponad dwie stopy zwierz^atko nerwowo strzy^ze swymi "+
        "uszami nas^luchuj^ac dobiegaj^acych zewsz^ad niepokoj^acych "+
        "odg�os^ow. Zdaje si^e, ^ze starczy jeden nieostro^zny ruch i "+
        "szarak umknie w pop^lochu. Cia^lo pokryte ma jasnym, niezwykle "+
        "g^estym futrem kt^ore zapewnia mu ciep^lo nawet przy siarczystym "+
        "mrozie. Tylnie lapy, zdecydowanie d^lu^zsze ni^z przednie "+
        "umo^zliwiaj^a niepozornemu zwierz^atku ucieczk^e przed du^zo "+
        "wi^ekszymi drapie^znikami. \n";
    }

    if(environment(TO)->pora_roku() != MT_ZIMA)
    {
        str2 ="Pierwsze co rzuca si^e w oczy to d^lugie uszy, du^ze, "+
            "br^azowe, osadzone na bokach g^lowy oczy oraz kr^oki, czarny "+
            "omyk. Futro pokrywaj^ace cia^lo zwierz^atka jest "+
            "szarobr^azowe, jedynie na zadzie nieco bardziej popielate co "+
            "upodabnia szaraka do otoczenia. ^Lapy zaj^aca, twarde i "+
            "w^askie umo^zliwiaj^a mu sprawne poruszanie si^e po twardym "+
            "pod^lo^zu, a d^lugie skoki b^lyskawiczn^a ucieczk^e w razie "+
            "zagro^zenia. \n";
	}
	else
	{
    str2 ="Pierwsze co rzuca si^e w oczy to d^lugie uszy, du^ze, br^azowe, "+
        "osadzone na bokach g^lowy oczy oraz kr^oki, czarny omyk. Jasne "+
        "futro pokrywaj^ace cia^lo zwierz^atka jest niezwykle g^este "+
        "dzi^eki czemu mo^ze prze^zy^c nawet najci^e^zsze mrozy. ^Lapy "+
        "zaj^aca, twarde i w^askie umo^zliwiaj^a mu sprawne poruszanie si^e "+
        "po twardym pod^lo^zu, a d^lugie skoki b^lyskawiczn^a ucieczk^e w "+
        "razie zagro^zenia. \n";
    }
    return (rand ? str1 : str2);

}

void random_przym(string str, int n)
{
        mixed* groups = ({ });
        groups = explode(str, "||");
        int groups_size = sizeof(groups);
        if(n > groups_size)
                return;
        for(int i =0; i < groups_size; i++)
                groups[i] = explode(groups[i], " ");
        
        for(int a = 0; a < n; a++)
        {
                int group_index = random(sizeof(groups));
                int adj_index = random(sizeof(groups[group_index]));
                string* tmp = explode(groups[group_index][adj_index], ":");
                dodaj_przym(tmp[0], tmp[1]);
                groups = groups-({groups[group_index]});
        }
}
