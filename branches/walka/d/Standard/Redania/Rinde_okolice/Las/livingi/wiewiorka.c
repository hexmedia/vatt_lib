/* Autor: Avard
   Opis : Eria
   Data : 24.11.06 */

inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include <object_types.h>
#include "dir.h"

void random_przym(string str, int n);

void
create_zwierze()
{
    ustaw_odmiane_rasy("wiewiorka");
    set_gender(G_MALE);

    set_long("To niewielkich rozmiar^ow zwierz^atko, kt^orego pyszczek i "+
        "w^asiki nieustannie si^e poruszaj^a jakby stara^ly si^e co^s "+
        "wyw^acha^c, bez w^atpienia jest najzwyklejsz^a w ^swiecie "+
        "wiewi^ork^a. Krotkie ^lapki zako^nczone s^a ostrymi pazurkami, "+
        "kt^orymi nerwowo przebiera. Ruda sier^s^c wiewi^orki jest "+
        "szorstka, jedynie puszysty, poka^xny ogonek stanowi wyj^atek od "+
        "tej regu^ly. Brzuszek gryzionia jest bia^ly, a reszta rudej "+
        "sier^sci przeplata si^e miejscami z tak^a o czarnej lub szarej "+
        "barwie. W^lochate uszy stworzenia, strzy^z^a lekko i nas^luchuj^a "+
        "zewsz^ad dobiegaj^acych odg^los^ow.");

    random_przym("puszysty:puszy^sci niewielki:niewielcy drobny:drobni "+
        "puchaty:puchaci mi�kki:mi^ekcy ma^ly:mali lekki:lekcy||rudy:rudzi "+
        "szorstkow^losy:szorstkow^losi||czarnooki:czarnoocy "+
        "p^lochliwy:p^lochliwi ruchliwy:ruchliwi",2);

    set_act_time(30);
    add_act("emote unosi ogon do g�ry.");
    add_act("emote umyka w podskokach par� krok�w po czym przystaje.");
    add_act("emote szybko obraca g�ow� w twoim kierunku.");

    set_stats (({1, 40, 1, 20, 20, 1}));

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 800);
    add_prop(CONT_I_HEIGHT, 10);

    //set_attack_unarmed(0, 15, 15, W_SLASH,  50, "praw^a ^lapk^a");
    //set_attack_unarmed(1, 15, 15, W_SLASH,  50, "lew^a ^lapk^a");
    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "g^low^e");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "lew^a �apk�");
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "praw^a �apk�");
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "tyln^a, lew^a �apk�");
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "tyln^a, praw^a �apk�");

    set_random_move(1);
    //set_restrain_path(({LAS_RINDE_LOKACJE+"las33",
    //    LAS_RINDE_LOKACJE+"las22",LAS_RINDE_LOKACJE+"las32"}));
    set_restrain_path(LAS_RINDE_LOKACJE);
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
        case "szturchnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
   TO->run_away();
}

void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
    TO->run_away();
}
void random_przym(string str, int n)
{
        mixed* groups = ({ });
        groups = explode(str, "||");
        int groups_size = sizeof(groups);
        if(n > groups_size)
                return;
        for(int i =0; i < groups_size; i++)
                groups[i] = explode(groups[i], " ");
        
        for(int a = 0; a < n; a++)
        {
                int group_index = random(sizeof(groups));
                int adj_index = random(sizeof(groups[group_index]));
                string* tmp = explode(groups[group_index][adj_index], ":");
                dodaj_przym(tmp[0], tmp[1]);
                groups = groups-({groups[group_index]});
        }
}
