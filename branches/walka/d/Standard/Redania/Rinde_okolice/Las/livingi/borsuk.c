/* Autor: Avard
   Opis : Faeve
   Data : 11.03.07 */

inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include <object_types.h>
#include "dir.h"

void random_przym(string str, int n);

void
create_zwierze()
{
    ustaw_odmiane_rasy("borsuk");
    set_gender(G_MALE);

    set_long("Zwierze to ma do^s^c charakterystyczn^a kr^ep^a budow^e oraz "+
        "r^ownie charakterystyczne ubrawienie - wierzch cia^la jest szary, "+
        "a po bokach bia^lej g^lowy, rozpoczynaj^ac od nosa, biegn^a "+
        "szerokie pasy czarnej sier^sci. Jego ko^nczyny zaopatrzone sa w "+
        "ostre pazury, kt^ore zdaj^a si^e by^c tak mocne, ^ze przy ich "+
        "pomocy mo^zna by rozora^c nielich^a po^la^c ziemi. Jednak "+
        "sympatyczny wyraz jego pyszczka i m^adre oczy zdradzaj^a, ^ze "+
        "borsuk nie jest zwierz^eciem drapie^znym.\n");

    random_przym("okr^aglutki:okr^aglutcy wychudzony:wychudzeni "+
        "drobny:drobni grubiutki:grubiutcy||szary:szarzy "+
        "wylinia^ly:wyliniali||m^lody:m^lodzi stary:starzy||"+
        "ruchliwy:ruchliwi spokojny:spokojni",2);

    set_act_time(30);
    add_act("emote w�szy w poszukiwaniu po�ywienia.");
    add_act("emote grzebie �ap� w ziemi.");
    add_act("emote unosi ryjek i czego� wypatruje.");
    add_act("emote podskakuje gwa�townie.");

    set_stats (({10, 10, 10, 20, 20, 15}));

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 15000 + random(4000));
    add_prop(CONT_I_HEIGHT, 30);

    set_attack_unarmed(0, 15, 15, W_SLASH,  30, "praw^a ^lap^a");
    set_attack_unarmed(1, 15, 15, W_SLASH,  30, "lew^a ^lap^a");
    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "g^low^e");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "lew^a ^lap^e");
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "praw^a ^lap^e");
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "tyln^a, lew^a ^lap^e");
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "tyln^a, praw^a ^lap^e");

    set_random_move(100);
    set_restrain_path(LAS_RINDE_LOKACJE);
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
        case "szturchnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
   TO->run_away();
}

void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
}

void random_przym(string str, int n)
{
        mixed* groups = ({ });
        groups = explode(str, "||");
        int groups_size = sizeof(groups);
        if(n > groups_size)
                return;
        for(int i =0; i < groups_size; i++)
                groups[i] = explode(groups[i], " ");
        
        for(int a = 0; a < n; a++)
        {
                int group_index = random(sizeof(groups));
                int adj_index = random(sizeof(groups[group_index]));
                string* tmp = explode(groups[group_index][adj_index], ":");
                dodaj_przym(tmp[0], tmp[1]);
                groups = groups-({groups[group_index]});
        }
}
