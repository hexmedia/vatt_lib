/* Autor: Avard
   Opis : Tinardan
   Data : 24.11.06 */

inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include <object_types.h>
#include "dir.h"

void random_przym(string str, int n);

void
create_zwierze()
{
    ustaw_odmiane_rasy("sarna");
    set_gender(G_MALE);

    set_long("@@dlugasny@@");

    random_przym("smuk^ly:smukli niewielki:niewielcy "+
        "drobny:drobni||br^azowy:br^azowi||m^lody:m^lodzi stary:starzy||"+
        "ruchliwy:ruchliwi spokojny:spokojni nerwowy:nerwowi",2);

    set_act_time(30);
    add_act("emote skubie spokojnie mech i traw^e.");
    add_act("emote rozgl^ada si^e nerwowo.");
    add_act("emote podnosi g^low^e i w^eszy uwa^znie.");

    set_stats (({15, 40, 10, 20, 20, 10}));

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 23000);
    add_prop(CONT_I_HEIGHT, 75);

    set_attack_unarmed(0, 15, 15, W_SLASH,  30, "prawym kopytem");
    set_attack_unarmed(1, 15, 15, W_SLASH,  30, "lewym kopytem");
    set_attack_unarmed(2, 15, 15, W_IMPALE,  40, "z^ebami");
    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "g^low^e");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "lew^a nog^e");
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "praw^a nog^e");
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "tyln^a, lew^a nog^e");
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "tyln^a, praw^a nog^e");

    set_random_move(100);
    set_restrain_path(LAS_RINDE_LOKACJE);
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
        case "szturchnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
   TO->run_away();
}

void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
}
string
dlugasny()
{
    string str;
    str ="W jej mi^e^sniach drga wigor po^l^aczony z delikatno^sci^a, "+
        "zgrabna g^lowa czujnie przechyla si^e raz w jedn^a, raz w "+
        "drug^a stron^e wypatruj^ac niebezpiecze^nstwa czy nawet zbyt "+
        "gwa^ltownego ruchu w^sr^od ga^l^ezi i listowia. Stoj^aca w plamie ";
    if(environment(this_object())->jest_dzien() == 1)
    {
        str +="s^lonecznego ";
    }
    else
    {
        str +="ksi^e^zycowego ";
    }
    str +="^swiat^la zdaje si^e by^c przez chwil^e le^snym duchem, albo "+
        "zakl^et^a w zwierz^e bogink^a. N^o^zki ma drobne, cieniutkie, "+
        "korpus szczup^ly i zgrabny � niby rze^xbiona r^ek^a mistrza. "+
        "Czarne pere^lki oczu patrz^a uwa^znie, sarna w ka^zdej chwili "+
        "gotowa jest do ucieczki. Uszy postawione s^a wysoko, ^lapi^a "+
        "ka^zdy najmniejszy szelest. Na br^azowej sier^sci wida^c kilka "+
        "ciemniejszych plamek, a ruchliwy nos jest niemal czarny i "+
        "wilgotny.\n";
    return str;
}
void random_przym(string str, int n)
{
        mixed* groups = ({ });
        groups = explode(str, "||");
        int groups_size = sizeof(groups);
        if(n > groups_size)
                return;
        for(int i =0; i < groups_size; i++)
                groups[i] = explode(groups[i], " ");
        
        for(int a = 0; a < n; a++)
        {
                int group_index = random(sizeof(groups));
                int adj_index = random(sizeof(groups[group_index]));
                string* tmp = explode(groups[group_index][adj_index], ":");
                dodaj_przym(tmp[0], tmp[1]);
                groups = groups-({groups[group_index]});
        }
}
