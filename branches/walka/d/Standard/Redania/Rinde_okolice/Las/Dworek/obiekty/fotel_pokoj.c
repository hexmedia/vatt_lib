
/* Autor: Avard
   Opis : Rantaur
   Data : 02.09.06*/

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("fotel");
    dodaj_przym("jasny","ja^sni");
    dodaj_przym("elegancki","eleganccy");

    set_long("Dziesi^atki niewielkich, bordowych kwiat^ow zamkni^etych w "+
        "nieregularne otoczki tworz^a jednolity wz^or pokrywaj^acy jasne "+
        "obicie fotela. Dobrze wypolerowane drewno niby rama ujmuje mebel "+
        "w swoje objecia, nadaj^ac mu elegancki i stylowy wygl^ad. Zdaje "+
        "si^e, ^ze fotel jest ca^lkiem wygodny - ^swiadczy o tym zar^owno "+
        "g^l^ebokie, grubo podbite siedzisko, jak i wy^scielone materia^lem "+
        "oparcia na r^ece.\n");
    make_me_sitable("w","w fotelu","z fotela",1);
    add_prop(OBJ_I_WEIGHT, 7000);
    add_prop(OBJ_I_VOLUME, 7000);
    add_prop(OBJ_I_VALUE, 260);
    add_prop(OBJ_M_NO_SELL, 1);
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
}
public int
jestem_fotelem_workrooma_avarda()
{
        return 1;
}

