/* Autor: Avard
   Opis : Ivrin
   Data : 02.09.06 */
inherit "/std/receptacle";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_receptacle()
{
    ustaw_nazwe("szafa");
    dodaj_przym("du^zy", "duzi");
    dodaj_przym("jasny", "ja^sni");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Du^za szafa z jasnego d^ebowego drewna ma drzwi ^z^lobione w "+
        "delikatny wz^or, poza tym jedynym jej ornamentem s^a kunsztowne "+
        "br^azowe zawiasy i takie^z okucie dziurki od klucza.\n");
	     
    add_prop(OBJ_I_VALUE, 1000);
    add_prop(CONT_I_RIGID, 1);
    add_prop(CONT_I_MAX_VOLUME, 250000);
    add_prop(CONT_I_MAX_WEIGHT, 500000);
    add_prop(CONT_I_VOLUME, 10000);
    add_prop(CONT_I_WEIGHT, 20000);

}

