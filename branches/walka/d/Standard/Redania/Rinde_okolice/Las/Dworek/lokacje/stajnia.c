
/* Autor: Avard
   Opis : Ivrin
   Data : 03.09.06 */

inherit "/std/room";

#include <stdproperties.h>
#include <macros.h>
#include "dir.h"

void
create_room() 
{
    set_short("Stajnia");
    set_long("@@dlugi_opis@@");
    add_prop(ROOM_I_INSIDE, 1);
    add_object(DWOREK_DRZWI + "wrota_ze_stajni.c");
    dodaj_rzecz_niewyswietlana("dwudzielne zdobione wrota",1);
    add_prop(ROOM_I_INSIDE,1);
    
}
public string
exits_description() 
{
    return "\n";
}

string
dlugi_opis()
{
    string str;
    str = "Niewielki korytarz stajni wysypany jest bia^lym piaskiem. W rogu "+
        "stoj^a oparte o wybielon^a ^scian^e wid^ly i szczotka na d^lugim "+
        "trzonku, dwa wiadra i beczka na wode, na gwo^xdziu zawieszono "+
        "skrzynk^e na rzeczy potrzebne przy oporz^adzaniu koni. Obok "+
        "ustawiono dwa drewniane kozio^lki na siod^la, nad nimi na hakach "+
        "mo^zna powiesi^c uzdy. Z prawej strony znajduj^a si^e dwa do^s^c "+
        "obszerne boksy dla wierzchowc^ow, wy^scie^lane s^lom^a, z "+
        "murowanymi z^lobami i poid^lami. Stajnia utrzymana jest w "+
        "czysto^sci.";
    return str;
}
