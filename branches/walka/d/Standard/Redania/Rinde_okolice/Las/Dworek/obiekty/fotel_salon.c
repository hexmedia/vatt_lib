/* Autor: Valor
   Data : 02.09.06 */

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("fotel"),
                 
    dodaj_przym("du^zy","duzi");
    dodaj_przym("sk^orzany","sk^orzani");
   
    make_me_sitable("w","w fotelu","z fotela",1);

    set_long("Sporych rozmiar^ow fotel obity zosta^l ciemnobr^azow^a "+
        "sk^or^a. Wyrze^xbione z bukowego drewna przednie cz^e^sci "+
        "pod^lokietnik^ow przedstawiaj^a wij^ace si^e w^e^ze, "+
        "dooko^la kt^orych umieszczono miedziane nity maj^ace za "+
        "zadanie utrzyma^c sk^or^e napi^et^a, a jednocze^snie traktowane "+
        "s^a jako ozdoby. Po obu stronach oparcia znajduj^a si^e r^o^znej "+
        "wielko^sci wybrzuszenia, stopniowo zmniejszaj^ace swoj^a "+
        "wielko^s^c tak, aby fotel by^l wygodny dla ka^zdego, kto na nim "+
        "usi^adzie.\n");
        
    add_prop(OBJ_I_WEIGHT, 20000);
    add_prop(OBJ_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 3000);
    ustaw_material(MATERIALY_DR_BUK, 50);
    ustaw_material(MATERIALY_MIEDZ, 20);
    ustaw_material(MATERIALY_SK_BYDLE, 30);
    set_type(O_MEBLE);
}
public int
jestem_fotelem_salonu_avarda()
{
        return 1;
}
