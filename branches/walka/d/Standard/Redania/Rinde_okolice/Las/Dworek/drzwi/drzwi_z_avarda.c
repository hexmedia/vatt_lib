
/* Autor: Avard
   Data : 03.09.06
   Info : Drzwi z pokoju Avarda */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
    dodaj_przym("stary","starzy");
    dodaj_przym("machoniowy", "machoniowi");
    
    set_other_room(DWOREK_LOKACJE + "salon.c");
    set_door_id("DRZWI_DO_POKOJU_AVARDA");
    set_door_desc("Stare, ale mimo to dobrze zachowane drzwi wykonane z "+
        "machoniu.\n");

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({({"drzwi","mahoniowe drzwi","stare drzwi",
        "stare mahoniowe drzwi","salon","wyj^scie"}),
        "przez machoniowe drzwi","z pokoju"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_key("KLUCZ_DRZWI_DO_DOMU_AVARDA_I_FAEVE");
    set_lock_name("zamek");
}