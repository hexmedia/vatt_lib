
/* Autor: Avard
   Data : 18.03.07
   Info : Drzwi z pokoju Faeve */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
    dodaj_przym("pot^e^zny","pot^e^zni");
    dodaj_przym("d^ebowy", "d^ebowi");
    
    set_other_room(DWOREK_LOKACJE + "salon.c");
    set_door_id("DRZWI_DO_POKOJU_FAEVE");
    set_door_desc("S^a do^s^c pot^e^zne, ale zarazem maj^a w sobie co^s "+
        "delikatnego, jakby nieco kobiecego ciep^la. Pi^ekny kolor "+
        "d^ebowego drewna doskonale wsp^o^lgra z mosi^e^znymi okuciami, "+
        "zawiasami oraz klamk^a o do^s^c prostym, ale trudnym do opisania "+
        "kszta^lcie.\n");

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({({"drzwi","mahoniowe drzwi","stare drzwi",
        "stare mahoniowe drzwi","salon","wyj^scie"}),
        "przez machoniowe drzwi","z pokoju"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_key("KLUCZ_DRZWI_DO_DOMU_AVARDA_I_FAEVE");
    set_lock_name("zamek");
}