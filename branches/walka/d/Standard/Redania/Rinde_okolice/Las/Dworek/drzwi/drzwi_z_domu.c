
/* Autor: Avard
   Data : 03.09.06
   Info : Drzwi do domu Avarda */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
    dodaj_przym("masywny","masywni");
    dodaj_przym("ciemnobr^azowy", "ciemnobr^azowi");
    
    set_other_room(DWOREK_LOKACJE + "ogrod.c");
    set_door_id("DRZWI_DO_DOMU_AVARDA");
    set_door_desc("Masywne drzwi wykonano z mocnego drewna i polakierowano "+
        "na ciemny br^az. Pod okr^ag^l^a ^zeliwn^a klamk^a znajduje sie "+
        "ma^la dziurka od klucza.\n");

    set_open_desc("");
    set_closed_desc("");
    set_open(0);
    set_locked(1);
        
    set_pass_command(({({"drzwi","masywne drzwi","ciemnobr^azowe drzwi",
        "masywne ciemnobr^azowe drzwi","wyj^scie"}),"przez masywne drzwi",
        "wychodz^ac z dworku"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_key("KLUCZ_DRZWI_DO_DOMU_AVARDA_I_FAEVE");
    set_lock_name("zamek");
}