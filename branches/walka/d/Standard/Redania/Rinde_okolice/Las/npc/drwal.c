/*
 * mega pakerny ochroniarz
 * kolega Raghola
 * coby elfy nam drwala nie bi^ly
 * opis by Barteusz pono^c
 * jakie^s poprawki i og^olne zepsucie by faeve
 * a buziaki i inne zboki by Jeremian ^^
 * dn. 14 maja 2007, pami^etny dzie^n, w kt^orym matematyk^e zdawa^lam
 * zapiski me potomnym zostawiam
 *
 */

inherit "/std/monster";

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
//Takich includow raczej nie robcie, potem cos sie przeniesie i sie wszystko pokaszani
//POzdrawiam Krun
//#include "/d/Standard/Redania/Rinde_okolice/Las/dir.h"
#include "dir.h"

mapping zboki = ([]);
int czy_walka();
int walka = 0;

void
create_monster()
{
    set_living_name(DRWAL_WSPIERACZ);
    ustaw_odmiane_rasy("m^e^zczyzna");
    set_gender(G_MALE);
    dodaj_nazwy("drwal");

    set_long("@@dlugasny@@");

	dodaj_przym("pot^e^zny","pot^e^zni");
	dodaj_przym("umi^e^sniony","umi^e^snieni");

    set_stats (({ 140, 90, 130, 70, 90, 150}));
    set_skill(SS_PARRY, 50);
	set_skill(SS_DEFENCE, 50);
	set_skill(12, 70);

    add_prop(CONT_I_WEIGHT, 120000);
    add_prop(CONT_I_HEIGHT, 190);
  
    add_prop(LIVE_I_NEVERKNOWN, 1);


    set_act_time(30);
    add_act("emote drapie si^e wielk^a d^loni^a po brodzie.");
    add_act("emote poprawia chwyt topora.");
    add_act("emote rozgl^ada si^e czujnie po okolicy. ");
    add_act("emote mierzy ci^e zimnym wzrokiem.");
    add_act("emote mruczy cicho pod nosem.");

    set_cact_time(30);
    add_cact("Zaraz!");
    add_cact("Ostatni raz w tych r^ekach trzymasz bro^n!");
    
	add_armour(OK_RINDE_PRZEDMIOTY+"ubrania/buty/podniszczone_ciemne_M.c");
	add_armour(OK_RINDE_PRZEDMIOTY+"ubrania/koszule/pozolkla_lniana_M.c");
	add_armour(OK_RINDE_PRZEDMIOTY+"ubrania/spodnie/przybrudzone_lniane_M.c");
	add_weapon(OK_RINDE_PRZEDMIOTY+"bronie/topory/wielki_dwureczny.c");

    add_ask(({"las"}), VBFC_ME("pyt_o_las"));
    add_ask(({"prac^e","pomoc","tartak"}), VBFC_ME("pyt_o_prace"));
    add_ask(({"raghola"}), VBFC_ME("pyt_o_raghola"));

    set_default_answer(VBFC_ME("default_answer"));

    set_alarm_every_hour("co_godzine");
    set_wzywanie_strazy(0);
	set_reakcja_na_walke(0);
}

string
dlugasny()
{
    string str;
    str = "Wysoki na przesz^lo siedem st^op m^e^zczyzna wyra^xnie "+
		"g^oruje postaw^a nad otoczeniem i bez problemu mo^ze uchodzi^c "+
		"za istnego olbrzyma w^sr^od ludzi. Wielki niczym wie^za, "+
		"demonstracyjnie uwydatnia muskulatur^e, ostrzegaj^ac tym wszelkich "+
		"rozrabiak^ow, aby zachowywali si^e grzecznie w okolicy jego miejsca "+
		"pracy je^sli chc^a ^zeby nie pogruchota^c im ko^sci. S^adz^ac po "+
		"ubraniu i dzier^zonym przez niego toporem, mo^zna wywnioskowa^c, "+
		"^ze jest zwyczajnym drwalem, lecz zimne spojrzenie stalowych oczu "+
		"zdradza, i^z walka w  z pewno^sci^a nie jest mu obca. Nie ma "+
		"w^atpliwo^sci, ^ze nie jest to osoba, kt^or^a chcia^loby si^e "+
		"spotka^c w ciemnym zau^lku wracaj^ac do domu lub id^ac samotnie "+
		"traktem. ";

	return str;
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_las()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Tutaj pracujemy.");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Zmykaj lepiej!");
    }
    return "";
    
}
string
pyt_o_prace()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Szukasz pracy? Zg^lo^s si^e do Raghola, on tam rz^adzi. ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Zmykaj lepiej!");
    }
    return "";
    
}

string
pyt_o_raghola()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Ta, nasz szef. Znajdziesz go w tartaku. ");
    }
else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Zmykaj lepiej!");
    }
    return "";
    
}

string
default_answer()
{
    if(!query_attack())
    {
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Id^x m^eczy^c durnymi pytaniami kogo innego. "); 
	}
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Won mi st^ad!");
    }
    return "";
}

void
emote_hook(string emote, object wykonujacy)
{
	switch (emote)
		{
		case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
		break;
		case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
		break;
		case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
		break;
		case "poca^luj":
			case "pog^laszcz":
			case "przytul":
			{
			if(wykonujacy->query_gender() == G_FEMALE) 
				{
				if (zboki[wykonujacy->query_real_name()] == 0) 
					{
					command("emote m^owi ch^lodno: Niewy^zyta jaka^s "+
						"jeste^s? Zapisz si^e w takim razie do burdelu.");
					}
					else if (zboki[wykonujacy->query_real_name()] == 1) 
						{
						command("emote m^owi spogl^adaj^ac wymownie na sw^oj "+
							"top^or: Ostrzegam...");
						}
						else 
							{
							command("zabij " + OB_NAME(wykonujacy));
							}
				}
				else 
					{
					if (zboki[wykonujacy->query_real_name()] == 0) 
						{
						command("emote m^owi spogl^adaj^ac wymownie na "+
							"sw^oj top^or: Jeszcze raz to zrobisz, a "+
							"po^za^lujesz...");
						}
						else
							{
							command("zabij " + OB_NAME(wykonujacy));
							}
					}
					zboki[wykonujacy->query_real_name()] += 1;

			}
			break;
		}
}


void
nienajlepszy(object wykonujacy)
{
	if(wykonujacy->query_gender() == G_FEMALE)
              switch(random(1))
              {
                case 0: set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
					"powiedz Ty g^lupia szmato!"); 
				set_alarm(2.0, 0.0, "command_present", this_player(),
					"zabij "+OB_NAME(wykonujacy)); break;

                        break;
              }
      else
              {
                switch(random(1))
                {
             case 0: set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
				 "powiedz Ty skurwielu!"); 
			 set_alarm(2.0, 0.0, "command_present", this_player(),
				 "zabij "+OB_NAME(wykonujacy)); break;
                }
              } 
}

void
wesprzyj_mnie()
{
    find_living(RAGHOL_SZEF)->command("wesprzyj " + OB_NAME(TO));
}

int czy_walka();

void attacked_by(object wrog)
{
    if(walka==0)
    {
        walka = 1;
        set_alarm(0.5,0.0,"command","krzyknij Ha! Wreszcie jaka� bitka!");
        set_alarm(0.5,0.0, wesprzyj_mnie);
        set_alarm(15.0, 0.0, "czy_walka", 1);
        return ::attacked_by(wrog);
    }
    else
    {
        return ::attacked_by(wrog);
    }
}
int
czy_walka()
{
    if(!query_attack())
    {
        set_alarm(0.5,0.0,"command","krzyknij Ha! Kto^s jeszcze chce "+
            "dosta^c?!");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}
