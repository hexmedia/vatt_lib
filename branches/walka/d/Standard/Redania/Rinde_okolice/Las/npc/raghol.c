inherit "/std/monster";
inherit "/std/act/action";
inherit "/std/act/chat";
inherit "/std/act/asking";
inherit "/lib/sklepikarz";

#include <object_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <money.h>
#include <pl.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <exp.h>

//Ehhh tak nie includujcie.. Przy przenoszeniu mog� by� kiedy� problemy
//#include "/d/Standard/Redania/Rinde_okolice/Las/dir.h"
#include "dir.h"

#define DEBUG(x) find_player("rantaur")->catch_msg(x+"\n");

int odbierz(string str);
void zly(object kto);
void dobry(object kto);
int czy_walka();
int walka = 0;

void
create_monster()
{
	set_living_name(RAGHOL_SZEF);
	ustaw_odmiane_rasy("m�czyzna");
	set_gender(G_MALE);
	ustaw_imie(({"raghol","raghola","ragholowi","raghola","ragholem",
		"ragholu"}), PL_MESKI_OS);
		
	dodaj_przym("�ysy", "�ysi");
	dodaj_przym("barczysty", "barczy�ci");
		
	set_long("M�czyzna z garncowatym, zwisaj�cym z ponad paska brzuchem posiada bardzo umi�nione ramiona i"
		+" barki. Wygl�da na wieloletniego pracownika tutejszego tartaku. Krz�ta si� nieustannie po"
		+" rozleg�ym pomieszczeniu, bacznie przygl�daj�c si� ka�demu drzewu, jakie tu trafia, oceniaj�c"
		+" je swoimi bystrymi oczyma. Na jego �ysej g�owie �wiec� si� liczne krople potu, kt�re co"
		+" jaki� czas ociera r�kawem wybrudzonej, poszarza�ej ze staro�ci koszuli.\n");
	
	set_stats (({ 100, 60, 100, 25, 38, 150}));
	set_skill(SS_PARRY, 50);
	set_skill(SS_DEFENCE, 50);
	set_skill(12, 70);
	
	config_default_sklepikarz();

	set_co_skupujemy(O_DREWNO);
	set_co_sprzedajemy(0);
	set_store_room("/d/Standard/Redania/Rinde_okolice/Las/lokacje/tartak_magazyn.c");
	set_skarbonka(this_object());
	set_money_greed_buy(100);
	set_money_greed_sell(100);
	
	add_prop(CONT_I_WEIGHT, 68000);
	add_prop(CONT_I_HEIGHT, 170);
	
	set_act_time(60);
	add_act("emote ociera pot z czo�a r�kawem brudnej koszuli.");
	add_act("emote odk�ada poci�te na kawa�ki drewno do jednej ze skrzy�.");
	add_act("emote m�wi mru��c oczy jak kot: To pewnie te cholerne sroki...");
	add_act("emote nerwowo krz�ta si� po pomieszczeniu jakby czego� szuka�."); 
	add_act("emote mruczy pod nosem jak�� ra�n� piosenk�.");
	add_act("emote m�wi drapi�c si� po g�owie: Gdzie jest ten cholerny sygnet?");
	
	set_cchat_time(15);
	add_cchat("W�asna matka ci� nie pozna!");
	add_cchat("Nooo, jak ci zaraz pieprzne...");
	add_cchat("Ha! Wreszcie co� co lubie!");

    add_ask(({"srok�", "sroki", "ptaka", "ptaki"}), VBFC_ME("pyt_o_sroki"));
    add_ask(({"pomoc"}), VBFC_ME("pyt_o_pomoc"));
    add_ask(({"sygnet"}), VBFC_ME("pyt_o_sygnet"));
	
	add_ask(({"zap�at�", "wyp�at�"}), VBFC_ME("pyt_o_zaplate"));
	add_ask(({"skup", "sprzeda�"}), VBFC_ME("pyt_o_skup"));
	add_ask(({"las", "drzewa"}), VBFC_ME("pyt_o_las"));
	add_ask(({"prac�", "zadanie"}), VBFC_ME("pyt_o_prace"));
	add_ask(({"transport", "ch�opak�w", "drwali"}), VBFC_ME("pyt_o_transport"));
	add_ask(({"siekier�", "pi��"}), VBFC_ME("pyt_o_siekiery"));
	set_default_answer(VBFC_ME("default_answer"));
	
	add_armour(OK_RINDE_PRZEDMIOTY+"ubrania/buty/podniszczone_ciemne_M.c");
	add_armour(OK_RINDE_PRZEDMIOTY+"ubrania/koszule/pozolkla_lniana_M.c");
	add_armour(OK_RINDE_PRZEDMIOTY+"ubrania/spodnie/przybrudzone_lniane_M.c");
	add_weapon(OK_RINDE_PRZEDMIOTY+"bronie/topory/wielki_dwureczny.c");

        remove_prop(NPC_M_NO_ACCEPT_GIVE);
	
	set_wzywanie_strazy(0);
	set_reakcja_na_walke(0);
}

string
pyt_o_sroki()
{
    set_alarm(0.5, 0.0, "command_present", this_player(),
            "emote m�wi mru��c oczy: Taaa... Pe�no tego badziewia tutaj...");
    set_alarm(2.5, 0.0, "command_present", this_player(),
            "emote m�wi: Niby nie wida� ich, ale nim cz�owiek si� obejrzy ju� mu co� b�yszcz�cego zajumaj�...");
    set_alarm(3.5, 0.0, "command_present", this_player(),
            "emote spluwa z niesmakiem na ziemi�.");
    return "";
}

string
pyt_o_pomoc()
{
    set_alarm(0.5, 0.0, "command_present", this_player(),
            "emote m�wi: Cholera, akurat mam pewn� spraw�..");
    set_alarm(2.5, 0.0, "command_present", this_player(),
            "emote m�wi u�miechaj�c si� lekko: Wierz czy nie, ale jaka� cholerna sroka mi sygnet rodowy zajuma�a...");
    set_alarm(4.5, 0.0, "command_present", this_player(),
            "emote m�wi: A te cholerstwo na drzewach pewnie chowa takie b�yskotki...");
    set_alarm(6.5, 0.0, "command_present", this_player(),
            "emote m�wi: Oczywi�cie b�d� bardzo wdzi�czny, gdyby m�j sygnet sie odnalaz�...");
    set_alarm(7.5, 0.0, "command_present", this_player(),
            "emote mruga porozumiewawczo.");
    return "";
}

string
pyt_o_sygnet()
{
    set_alarm(0.5, 0.0, "command_present", this_player(),
            "emote m�wi: W sumie to nic szczeg�lnego...");
    set_alarm(2.5, 0.0, "command_present", this_player(),
            "emote m�wi: Taka ot rodzinna pami�tka...");
}

init()
{
	::init();
	add_action(&odbierz(), "odbierz");
    init_sklepikarz();
}

void
powiedz_gdy_jest(object player, string tekst)
{
       if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_zaplate()
{
	set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m�wi chrapliwym g�osem: No, jak ch�opy zwie�li jakie drewno od ciebie, to pewnikiem"
	+" mo�esz u mnie odebra� swoj� zap�at�.");
        return "";
}

string
pyt_o_skup()
{
	set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m�wi energicznie: Pewnie, �e skupuj�! Zawsze mo�esz u mnie sprzeda� jakie drewno.");
        return "";
}

string
pyt_o_las()
{
	set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m�wi: Ano, las jak las, drzewo jak drzewo. Wa�ne, �e jaka praca i zarobek z tego jest.");
        return "";
}

string
pyt_o_prace()
{
	set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m�wi: A pewnie! Jak chcesz popracowa� to bierz toporzec i w las �cina� drzewa!"
	+" Jak ju� jakie zetniesz i ociosasz porz�dnie to zwo�aj transport - ch�opaki ju� si� tym zajm�.");
	set_alarm(0.8, 0.0, "powiedz_gdy_jest", this_player(), "emote mruga.");
        return "";
}

string
pyt_o_transport()
{
	set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m�wi: Tak, tak. Jak tylko ociosasz drzewo to zwo�aj ch�opak�w.");
        return "";
}

string
pyt_o_siekiery()
{
	set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m�wi: A moja to rzecz? Znajd� sobie jak�� jak ci trza.");
        return "";
}

string default_answer()
{
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "emote mruczy: Daj spok�j, robot� mam.");
        return "";
}

int odbierz(string str)
{
    NF("");
    //Pozmienia�em notify_faile na command, lepiej to b�dzie wygl�da�, Krun

    if(!((str ~= "zap�at�") || (str ~= "wyp�at�") || (str ~= "monety") || (str ~= "pieni�dze")))
    {
        command("powiedz do " + OB_NAME(TP) + " Co chcesz odebra�?\n");
        return 0;
    }

    int zaplata = environment()->query_zaplata(TP->query_real_name());
    if(!zaplata)
    {
        command("powiedz do " + OB_NAME(TP) + " Niestety, nie czeka na ciebie �adna zap�ata.\n");
        return 0;
    }

    MONEY_ADD(TP, zaplata);
    environment()->usun_zaplate(TP->query_real_name());
    TP->catch_msg(QCIMIE(TO, PL_MIA)+" wyp�aca ci "
                 +MONEY_TEXT_SPLIT(zaplata, PL_BIE)+".\n");
    saybb(QCIMIE(TO, PL_MIA)+" wyp�aca "+QIMIE(TP, PL_CEL)+" jakie� monety.\n");

    return 1;
}

void emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij": set_alarm(2.0, 0.0, "zly", wykonujacy);
                    break;
        case "spoliczkuj" : set_alarm(2.0, 0.0, "zly", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy);
                    break;
        case "zasmiej": set_alarm(2.0, 0.0, "dobry", wykonujacy);
                    break;
        case "pocaluj": set_alarm(2.0, 0.0, "dobry", wykonujacy);
                    break;
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
                   break;
        case "poglaszcz": set_alarm(2.0, 0.0, "dobry", wykonujacy);
                   break;
    }
}

void dobry(object kto)
{
	if(!kto->query_gender())
		zly(kto);
	else
	{
		switch(random(3))
		{
			case 0: command("usmiech chutliwie do "+OB_NAME(kto));
				break;
			case 1: command("mrugnij zawadiacko do "+OB_NAME(kto));
				break;
			case 2: set_alarm(2.0, 0.0, "powiedz_gdy_jest", kto, 
					"powiedz do "+OB_NAME(kto)+" No, no...dla takiej dziewki to"	
					+" i bym si� ch�tnie od roboty oderwa�...");
				break;
		}
	}
}

void zly(object kto)
{
	switch(random(3))
	{
		case 0: command("splun pod nogi "+OB_NAME(kto));
			break;
		case 1: command("warknij ze zloscia");
			break;
		case 2: set_alarm(2.0, 0.0, "powiedz_gdy_jest", kto, 
				"powiedz do "+OB_NAME(kto)+" Po mordzie chyba� dawno nie"
				+" dosta�"+kto->koncowka("", "a")+"!");
			break;
	}
}

void
oddajemy(object ob, object old)
{
    if (function_exists("create_heap", ob) == "/std/coins") {
        // kasy nie oddajemy
        return;
    }
    if (interactive(old) && (ENV(old) == ENV(TO))) {
        command_present(old, "emote m�wi: A po choler� mi to!");
        command_present(old, "daj " + OB_NAME(ob) + " " + OB_NAME(old));
        return;
    }
}

void
kasa_dla(object old)
{
    if (interactive(old) && (ENV(old) == ENV(TO))) {
        old->catch_msg(QCIMIE(TO, PL_MIA)+" daje ci gar�� monet.\n");
        tell_room(ENV(old), QCIMIE(TO, PL_MIA)+" daje "+QIMIE(old, PL_CEL)+" jakie� monety.\n", old);
        MONEY_ADD(old, 400);
    }
}

void
exp_dla(object old)
{
    if (interactive(old) && (ENV(old) == ENV(TO))) {
        old->catch_msg(QCIMIE(TO, PL_MIA)+" gestykuluj�c �ywo t�umaczy, jak powinno si� �cina� drzewa.\n");
        tell_room(ENV(old), QCIMIE(TO, PL_MIA)+" t�umaczy co� "+QIMIE(old, PL_CEL)+".\n", old);
        old->increase_ss(SS_WOODCUTTING, EXP_QUEST_SYGNET_DLA_RAGHOLA_WOODCUTING);
    }
}

void
sroka()
{
    tell_room(ENV(TO), "Nagle niewielki ptak spada z nieba i wytr�ca trzymany przez " +
            QCIMIE(TO, PL_DOP) + " sygnet, �api�c go dziobem w powietrzu. Ptak znika w�r�d " +
            "drzew jeszcze szybciej ni� si� pojawi�.\n", 0);
    command("'");
}

void
sygnecik(object ob, object old)
{
    if (interactive(old) && (ENV(old) == ENV(TO))) {
        if (ob->check_rozwiazal(old->query_real_name())) {
            set_alarm(0.5, 0.0, "command_present", old,
                    "emote m�wi u�miechaj�c si� szeroko: O! Dzi�ki wielkie! Tym bardziej, �e to kt�ry� raz z kolei...");
            set_alarm(1.5, 0.0, "command_present", this_player(),
                    "emote mruga porozumiewawczo.");

        }
        else {
            ob->add_rozwiazal(old->query_real_name());
            set_alarm(0.5, 0.0, "command_present", old,
                    "emote m�wi u�miechaj�c si� szeroko: O! Dzi�ki wielkie! Zaraz co� ci dam w nagrod�!");
            set_alarm(2.5, 0.0, &kasa_dla(old));
            set_alarm(4.5, 0.0, "command_present", old,
                    "emote m�wi: No i jeszcze popatrz, jak prawid�owo powinno si� siekier� trzyma� podczas �cinania drzewa.");
            set_alarm(7.0, 0.0, &exp_dla(old));
        }
        set_alarm(11.0, 0.0, &sroka());
    }
    ob->remove_object();
}

void
enter_inv(object ob, object old)
{
    ::enter_inv(ob, old);
    if (ob->is_raghol_sygnet()) {
        set_alarm(0.1, 0.0, &sygnecik(ob, old));
    }
    else {
        set_alarm(0.1, 0.0, &oddajemy(ob, old));
    }
}

int czy_walka();

void
wesprzyj_mnie()
{
    find_living(DRWAL_WSPIERACZ)->command("wesprzyj " + OB_NAME(TO));
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {
        walka = 1;
        set_alarm(15.0, 0.0, "czy_walka", 1);
        set_alarm(0.5, 0.0, wesprzyj_mnie); 
        return ::attacked_by(wrog);
    }
    else
    {
        return ::attacked_by(wrog);
    }
}
int
czy_walka()
{
    if(!query_attack())
    {
        set_alarm(0.5,0.0,"command","zarechocz");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}
