/* Wykonane przez Valora dnia 03.07.06
   opis made by myself
   */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("G^esty ciemny las");
    add_exit(LAS_RINDE_LOKACJE + "las18.c","sw",0,10,1);
    add_npc(LAS_RINDE_LIVINGI + "sarna");
    
    add_prop(ROOM_I_INSIDE,0);

}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Gdzie tylko nie spojrzysz, zauwa^zasz grub^a warstwe "+
            "^sci^o^lki le^snej pokrywaj^ac^a ka^zdy skrawek pod^lo^za. "+
            "Wysoko nad tob^a dostrzegasz roz^lo^zyste korony drzew, "+
            "kt^ore zakrywaj^a ca^le niebo, tylko gdzieniegdzie pojedynczy "+
            "promyk ^swiat^la przedziera si^e przez g^este li^scie. "+
            "Otaczaj^ace ci^e liczne pnie drzew, skutecznie "+
            "uniemo^zliwiaj^a dalsze zag^l^ebianie si^e w dzicz, a "+
            "bujne krzaki porastaj^ace ka^zd woln^a przestrze^n oraz "+
            "zwisaj^ace konary, nie pozwalaj^a dostrzec czegokolwiek "+
            "znajduj^acego si^e za nimi."; 
        }
        else
        {
            str = "Spod ^snie^znobia^lego dywanu przebijaj^a si^e fragmenty "+
            "^sci^o^lki le^snej le^z^acej pod ^sniegiem. Wysoko nad tob^a "+
            "dostrzegasz roz^lo^zyste korony drzew, kt^ore zakrywaj^a ca^le "+
            "niebo, tylko gdzieniegdzie pojedy^nczy promyk ^swiat^la "+
            "przedziera si^e przez g^este ga^l^ezie drzew pokryte ^sniegiem. "+
            "Otaczaj^ace ci^e liczne pnie drzew, uniemo^zliwiaj^a dalsze "+
            "zag^l^ebianie si^e w dzicz, a bujne krzaki w ca^lo^sci pokryte "+
            "^snie^zn^a ko^ldr^a, porastaj^a ka^zd^a woln^a przestrze^n. "+
            "Zwisaj^ace konary oblepione szronem, nie pozwalaj^a dostrzec "+
            "czegokolwiek znajdujacego sie za nimi.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Gdzie tylko nie spojrzysz, zauwa^zasz grub^a warstwe "+
            "^sci^o^lki le^snej pokrywaj^ac^a ka^zdy skrawek pod^lo^za. "+
            "Wysoko nad tob^a dostrzegasz roz^lo^zyste korony drzew, "+
            "kt^ore zakrywaj^a ca^le niebo, tylko gdzieniegdzie pojedynczy "+
            "promyk ^swiat^la gwiazd przedziera si^e przez g^este li^scie. "+
            "Otaczaj^ace ci^e liczne pnie drzew, skutecznie "+
            "uniemo^zliwiaj^a dalsze zag^l^ebianie si^e w dzicz, a bujne "+
            "krzaki porastaj^ace ka^zd woln^a przestrze^n oraz zwisaj^ace "+
            "konary, nie pozwalaj^a dostrzec czegokolwiek znajduj^acego "+
            "^si^e za nimi."; 
        }
        else
        {
            str = "Spod ^snie^znobia^lego dywanu przebijaj^a si^e fragmenty "+
            "^sci^o^lki le^snej le^z^acej pod ^sniegiem. Wysoko nad tob^a "+
            "dostrzegasz roz^lo^zyste korony drzew, kt^ore zakrywaj^a ca^le "+
            "niebo, tylko gdzieniegdzie pojedy^nczy promyk ^swiat^la gwiazd "+
            "przedziera si^e przez g^este ga^l^ezie drzew pokryte "+
            "^sniegiem. Otaczaj^ace ci^e liczne pnie drzew, "+
            "uniemo^zliwiaj^a dalsze zag^l^ebianie si^e w dzicz, a bujne "+
            "krzaki w ca^lo^sci pokryte ^snie^zn^a ko^ldr^a, porastaj^a "+
            "ka^zd^a woln^a przestrze^n. Zwisaj^ace konary oblepione "+
            "szronem, nie pozwalaj^a dostrzec "+
            "czegokolwiek znajdujacego sie za nimi.";
        }
    }
   str += "\n";
   return str;
}

string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
    str = "Gdzie tylko nie spojrzysz, zauwa^zasz grub^a warstwe ^sci^o^lki "+
        "le^snej pokrywaj^ac^a ka^zdy skrawek pod^lo^za. Wysoko nad tob^a "+
        "dostrzegasz roz^lo^zyste korony drzew, kt^ore zakrywaj^a ca^le "+
        "niebo, tylko gdzieniegdzie pojedynczy promyk ^swiat^la gwiazd "+
        "przedziera si^e przez g^este li^scie. Otaczaj^ace ci^e liczne "+
        "pnie drzew, skutecznie uniemo^zliwiaj^a dalsze zag^l^ebianie si^e "+
        "w dzicz, a bujne krzaki porastaj^ace ka^zd woln^a przestrze^n oraz "+
        "zwisaj^ace konary, nie pozwalaj^a dostrzec czegokolwiek "+
        "znajduj^acego si^e za nimi."; 
      }
      else
      {
    str = "Spod ^snie^znobia^lego dywanu przebijaj^a si^e fragmenty "+
        "^sci^o^lki le^snej le^z^acej pod ^sniegiem. Wysoko nad tob^a "+
        "dostrzegasz roz^lo^zyste korony drzew, kt^ore zakrywaj^a ca^le "+
        "niebo, tylko gdzieniegdzie pojedy^nczy promyk ^swiat^la gwiazd "+
        "przedziera si^e przez g^este ga^l^ezie drzew pokryte ^sniegiem. "+
        "Otaczaj^ace ci^e liczne pnie drzew, uniemo^zliwiaj^a dalsze "+
        "zag^l^ebianie si^e w dzicz, a bujne krzaki w ca^lo^sci pokryte "+
        "^snie^zn^a ko^ldr^a, porastaj^a ka^zd^a woln^a przestrze^n. "+
        "Zwisaj^ace konary oblepione szronem, nie pozwalaj^a dostrzec "+
        "czegokolwiek znajdujacego sie za nimi.";
      }
    str += "\n";
    return str;
}
