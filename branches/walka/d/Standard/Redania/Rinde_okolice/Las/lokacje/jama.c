
inherit "/std/room";

#include "dir.h"
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <pl.h>
#include <object_types.h>

string dlugi_opis();

void
create_room() 
{
    set_short("Ciemna jama");
    set_long("@@dlugi_opis@@\n");


    add_item("legowisko",
                "Konstrukcja legowiska sk�ada si� na chaotycznie "+
                "u�o�one ga��zie i grubsze konary drzew.\n");
    add_item("korzenie",
		"Liczne cieniutkie korzenie opadaj� kaskadowo ze stropu "+
		"znacznie ograniczaj�c widoczno��, natomiast te, kt�re "+
		"dostrzegasz w ziemistych �ciankach tej jamy s� ju� znacznie "+
		"grubsze i mocniejsze.\n");

	add_npc(LAS_RINDE_LIVINGI+"niedzwiedz_z_jamy.c");

	set_event_time(250.0+itof(random(100)));
    add_event("Co� ma�ego przemkn�o ci pod nogami.\n");
    add_event("Co� zapiszcza�o w k�cie.\n");
    add_event("Z zewn�trz dobiegaj� twych uszu le�ne odg�osy.\n");
    add_event("Ch��d wiej�cy po nogach zaczyna przyprawia� ci� o g�si� sk�rk�.\n");

    add_prop(ROOM_I_INSIDE,1);
    remove_prop(ROOM_I_LIGHT);
    add_prop(ROOM_S_DARK_LONG,"W ciemnej jamie.\n");
    
    add_sit("na legowisku","na legowisku","z legowiska",1);

}
/*public string
exits_description() 
{
    return "Wyj^scie st^ad prowadzi na plac.\n"; 
}




public void 
enter_inv(object ob, object from)
{
        ::enter_inv(ob, from);
        if (!objectp(ob)) {
                return;
        }
        if (ob->query_type() == O_MEBLE) {
                if ((ob->short() ~= "krysztalowy stol")) {
                        ++ile_foteli;
                        if (ile_foteli == 1) {
                                return;
                        }
                }
                dodaj_rzecz_niewyswietlana(ob->short());
        }
}

public void
leave_inv(object ob, object to)
{
        ::leave_inv(ob, to);
        if (!objectp(ob)) {
                return;
        }
        if (ob->query_type() == O_MEBLE) {
                if ((ob->short() ~= "krysztalowy stol")) {
                        --ile_foteli;
                        if (ile_foteli <= 0) {
                                ile_foteli = 0;
                                return;
                        }
                }
                usun_rzecz_niewyswietlana(ob->short());
        }
}*/

string
dlugi_opis()
{
    string str="";
    
    str="Ch��d jaki powiewa z g��bi tej jamy jest niemal�e "+
    "s�yszalny. Z niewysokiego stropu zwisaj� pojedyncze cienkie i "+
    "poskr�cane korzenie, kt�re oplataj� tak�e i ziemiste �ciany. Tak, "+
    "to chyba w�a�nie one tworz� t� niesamowit� atmosfer� dziko�ci tego "+
    "miejsca. Dopiero teraz dostrzegasz w oddali u�o�one z ga��zi legowisko";
    
    if(!dzien_noc())
    	str+="do kt�rego nie docieraj� ju� nik�e promienie �wiat�a "+
    	"pochodz�ce z niewielkiego otworu w g�rze.";
   	else
   		str+=".";
    
    return str;
}

void
wyjdz(object player)
{
	player->move(LAS_RINDE_LOKACJE+"las7");
	player->do_glance(2);
	saybb(QCIMIE(TP,PL_MIA)+" wychodzi z niewielkiej jamy spod wzg�rza.\n");
	return;
}

int
wespnij(string str)
{
	if(query_verb() ~= "wespnij" || query_verb() ~= "wejd�")
	{
        if(query_verb() == "wespnij")
            notify_fail("Wespnij si� gdzie?\n");
        else
            notify_fail("Wejd� gdzie?\n");

		if(str ~= "si� na g�r�" || str~="si� po korzeniach" ||
			str ~="si� po korzeniach na g�r�" || str ~="si� na g�r� po korzeniach" ||
            str ~= "na g�r�" || str~="po korzeniach" ||
            str ~="po korzeniach na g�r�" || str ~="na g�r� po korzeniach")
		{
			object paraliz=clone_object("/std/paralyze");
			paraliz->set_fail_message("");
    		paraliz->set_finish_object(this_object());
    		paraliz->set_finish_fun("wyjdz");
    		paraliz->set_remove_time(4);
    		paraliz->setuid();
    		paraliz->seteuid(getuid());
    		paraliz->move(TP);
		
			write("Zaczynasz wspina� si� po korzeniach na g�r�.\n");
			saybb(QCIMIE(TP,PL_MIA)+" zaczyna wspina� si� po korzeniach "+
					"na g�r�.\n");
			return 1;
		}
	}
	else if(query_verb() ~="wyjd�")
	{
		notify_fail("Wyjd� gdzie?\n");

		if(str ~= "na zewn�trz" || str ~= "z jamy")
		{
			object paraliz=clone_object("/std/paralyze");
			paraliz->set_fail_message("");
    		paraliz->set_finish_object(this_object());
    		paraliz->set_finish_fun("wyjdz");
    		paraliz->set_remove_time(4);
    		paraliz->setuid();
    		paraliz->seteuid(getuid());
    		paraliz->move(TP);
		
			write("Zaczynasz wspina� si� po korzeniach na g�r�.\n");
			saybb(QCIMIE(TP,PL_MIA)+" zaczyna wspina� si� po korzeniach "+
					"na g�r�.\n");
			return 1;
		}
	}
	
	return 0;
}

void
init()
{
        ::init();
        add_action(wespnij,"wespnij");
        add_action(wespnij,"wejd�");
        add_action(wespnij,"wyjd�");
}

/*

L�ni�ce, br�zowe futro mieni si� refleksami przy ka�dym ruchu
zwierz�cia. Mimo, i� sprawiaj� one wra�enie powolnych
i leniwych to z pewno�ci� nied�wied� jest te� zdolny do
sprawnego ataku na upatrzon� ofiar�, b�dz co b�dz wielu
ju� zgin�o rozszarpanych w�a�nie nied�wiedzimi pazurami.
Jego czarne oczy wodz� niespokojnie po okolicy, a z masywnego
pyska od czasu do czasu wydobywa si� cichy pomruk.



Masywne cielsko porusza si� miarowo w rytm oddechu zwierz�cia.
�pi�cy nied�wied� przypomina, z pozoru niegro�n�, kup� furta,
jednak wolisz nie my�le� jak zachowa�by si� gdyby kto� raczy�
przeszkodzi� mu w wypoczynku. Jego kr�tkie, p�okr�g�e uszy 
od czasu do czasu poruszaj� si� zupe�nie jakby nas�uchiwa�y
przybycia owego wichrzyciela.
*/

