/* Autor: Avard
   Data : 01.12.06
   Opis : Tinardan */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("Las");
    add_exit(LAS_RINDE_LOKACJE + "las32.c","se",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las33.c","s",0,10,1);
    add_prop(ROOM_I_INSIDE,0);
    add_npc(LAS_RINDE_LIVINGI+"wiewiorka");
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Buki, brzozy i d^eby przemiesza^ly si^e tu ze sob^a, "+
                "tworz^ac krajobraz nader malowniczy. Ich pnie s^a smuk^le "+
                "i mocne, korony szumi^a zawzi^ecie w g^orze od czasu do "+
                "czasu sypi^ac w d^o^l ";
            if(pora_roku() == MT_JESIEN)
            {
                str +="usychaj^acymi ";
            }
            str += "li^s^cmi. Gruba ich warstwa pokrywa ju^z le^sne "+
                "pod^lo^ze, ale poza nimi nic tu nie wida^c � poszycie nie "+
                "sta^lo si^e tutaj polem bitwy pomi^edzy krzewami i "+
                "m^lodymi drzewkami. Gdzieniegdzie mo^zna dostrzec "+
                "ja^sniejsze k^epki zawilc^ow i ciemnozielone grupki "+
                "ostrej, le^snej trawy. W koronach drzew ptaki "+
                "urz^adzi^ly ^spiewacze zawody i ca^ly las rozbrzmiewa "+
                "ich weso^lym trelem. Woko^lo unosi si^e "+
                "charakterystyczny le^sny zapach.";
        }
        else
        {
            str = "Buki, brzozy i d^eby przemiesza^ly si^e tu ze sob^a, "+
                "tworz^ac krajobraz nader malowniczy. Ich pnie s^a smuk^le "+
                "i mocne, korony szumi^a zawzi^ecie w g^orze od czasu do "+
                "czasu sypi^ac w d^o^l ostatnimi, zeschni^etymi li^s^cmi. "+
                "^Snieg zalega na ziemi grub^a warstw^a, a jego "+
                "powierzchnia jest r^owna i p^laska, niemal jak st^o^l. "+
                "Znacz^a go jednak do^s^c liczne tropy ptak^ow i zwierz^at, "+
                "kt^ore nie zaszy^ly si^e w swoich norkach w oczekiwaniu na "+
                "wiosenn^a por^e. Gdzie^s w g^orze przysiad^lo na ga^l^ezi "+
                "stadko wron i gawron^ow, racz^ac teraz ca^l^a okolic^e "+
                "swym w^atpliwej jako^sci ^spiewem.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Buki, brzozy i d^eby przemiesza^ly si^e tu ze sob^a, "+
                "tworz^ac krajobraz nader malowniczy. Ich pnie s^a smuk^le "+
                "i mocne, korony szumi^a zawzi^ecie w g^orze od czasu do "+
                "czasu sypi^ac w d^o^l ";
            if(pora_roku() == MT_JESIEN)
            {
                str +="usychaj^acymi ";
            }
            str += "li^s^cmi. Gruba ich warstwa pokrywa ju^z le^sne "+
                "pod^lo^ze, ale poza nimi nic tu nie wida^c � poszycie nie "+
                "sta^lo si^e tutaj polem bitwy pomi^edzy krzewami i "+
                "m^lodymi drzewkami. Gdzieniegdzie mo^zna dostrzec "+
                "ja^sniejsze k^epki zawilc^ow i ciemnozielone grupki "+
                "ostrej, le^snej trawy. Woko^lo unosi si^e "+
                "charakterystyczny le^sny zapach.";
        }
        else
        {
            str = "Buki, brzozy i d^eby przemiesza^ly si^e tu ze sob^a, "+
                "tworz^ac krajobraz nader malowniczy. Ich pnie s^a smuk^le "+
                "i mocne, korony szumi^a zawzi^ecie w g^orze od czasu do "+
                "czasu sypi^ac w d^o^l ostatnimi, zeschni^etymi li^s^cmi. "+
                "^Snieg zalega na ziemi grub^a warstw^a, a jego "+
                "powierzchnia jest r^owna i p^laska, niemal jak st^o^l. "+
                "Znacz^a go jednak do^s^c liczne tropy ptak^ow i zwierz^at, "+
                "kt^ore nie zaszy^ly si^e w swoich norkach w oczekiwaniu na "+
                "wiosenn^a por^e. ";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Buki, brzozy i d^eby przemiesza^ly si^e tu ze sob^a, "+
                "tworz^ac krajobraz nader malowniczy. Ich pnie s^a smuk^le "+
                "i mocne, korony szumi^a zawzi^ecie w g^orze od czasu do "+
                "czasu sypi^ac w d^o^l ";
            if(pora_roku() == MT_JESIEN)
            {
                str +="usychaj^acymi ";
            }
            str += "li^s^cmi. Gruba ich warstwa pokrywa ju^z le^sne "+
                "pod^lo^ze, ale poza nimi nic tu nie wida^c � poszycie nie "+
                "sta^lo si^e tutaj polem bitwy pomi^edzy krzewami i "+
                "m^lodymi drzewkami. Gdzieniegdzie mo^zna dostrzec "+
                "ja^sniejsze k^epki zawilc^ow i ciemnozielone grupki "+
                "ostrej, le^snej trawy. Woko^lo unosi si^e "+
                "charakterystyczny le^sny zapach.";
        }
        else
        {
            str = "Buki, brzozy i d^eby przemiesza^ly si^e tu ze sob^a, "+
                "tworz^ac krajobraz nader malowniczy. Ich pnie s^a smuk^le "+
                "i mocne, korony szumi^a zawzi^ecie w g^orze od czasu do "+
                "czasu sypi^ac w d^o^l ostatnimi, zeschni^etymi li^s^cmi. "+
                "^Snieg zalega na ziemi grub^a warstw^a, a jego "+
                "powierzchnia jest r^owna i p^laska, niemal jak st^o^l. "+
                "Znacz^a go jednak do^s^c liczne tropy ptak^ow i zwierz^at, "+
                "kt^ore nie zaszy^ly si^e w swoich norkach w oczekiwaniu na "+
                "wiosenn^a por^e. ";
        }
        str += "\n";
    return str;
}

