/* Autor: Avard
   Data : 21.11.06
   Opis : Tinardan */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("Przerzedzony las");
    add_exit(LAS_RINDE_LOKACJE + "las36.c","ne",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las48.c","e",0,10,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt23.c","se",0,10,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt24.c","s",0,10,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt25.c","w",0,10,1);
    add_npc(LAS_RINDE_LIVINGI + "sarna");

    add_prop(ROOM_I_INSIDE,0);

}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Las nie jest tu g^esty i strzelisty, drzewa rosn^a w "+
                "do^s^c sporych odleg^lo^sciach od siebie daj^ac tym samym "+
                "szans^e p^edom zupe^lnie m^lodym. Buki, klony, srebrzyste "+
                "brzozy, leszczynowe krzewy mieszaj^a si^e tu ze sob^a i "+
                "przeplataj^a, walcz^a o byt i przestrze^n. Z poszycia "+
                "bij^a w g^or^e m^lodziutkie drzewka, niekt^ore pogi^ete i "+
                "po^lamane, inne dzielnie pn^ace si^e w g^or^e, mocne i "+
                "zdeterminowane by wywalczy^c dla siebie odrobin^e ziemi i "+
                "cho^c troch^e promieni s^lonecznych. Pomi^edzy ich "+
                "drobnymi pniami rozgrywa si^e kolejna walka � mech i "+
                "paprocie tocz^a ze sob^a zaci^ety b^oj. ^Sci^o^lka jest "+
                "wilgotna, pokryta warstw^a butwiej^acych li^sci, "+
                "po^lamanych ga^l^ezi i resztek ro^slin, kt^ore przegra^ly "+
                "bitw^e o przetrwanie.";
        }
        else
        {
            str = "Las nie jest tu g^esty i strzelisty, drzewa rosn^a w "+
                "do^s^c sporych odleg^lo^sciach od siebie daj^ac tym samym "+
                "szans^e p^edom zupe^lnie m^lodym. Buki, klony, srebrzyste "+
                "brzozy, leszczynowe krzewy mieszaj^a si^e tu ze sob^a i "+
                "przeplataj^a, walcz^a o byt i przestrze^n. ^Snieg zalega "+
                "na ziemi grub^a warstw^a, ale jego powierzchnia jest "+
                "nier^owna, po^lamana pod wieloma kontami, usiana "+
                "niewielkimi kopczykami. Spod niekt^orych kopczyk^ow "+
                "wychylaj^a si^e nagie ga^l^ezie m^lodziutkich drzewek, "+
                "walcz^acych o byt i o przetrwanie.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Las nie jest tu g^esty i strzelisty, drzewa rosn^a w "+
                "do^s^c sporych odleg^lo^sciach od siebie daj^ac tym samym "+
                "szans^e p^edom zupe^lnie m^lodym. Buki, klony, srebrzyste "+
                "brzozy, leszczynowe krzewy mieszaj^a si^e tu ze sob^a i "+
                "przeplataj^a, walcz^a o byt i przestrze^n. Z poszycia "+
                "bij^a w g^or^e m^lodziutkie drzewka, niekt^ore pogi^ete i "+
                "po^lamane, inne dzielnie pn^ace si^e w g^or^e, mocne i "+
                "zdeterminowane by wywalczy^c dla siebie odrobin^e ziemi i "+
                "cho^c troch^e promieni s^lonecznych. Pomi^edzy ich "+
                "drobnymi pniami rozgrywa si^e kolejna walka � mech i "+
                "paprocie tocz^a ze sob^a zaci^ety b^oj. ^Sci^o^lka jest "+
                "wilgotna, pokryta warstw^a butwiej^acych li^sci, "+
                "po^lamanych ga^l^ezi i resztek ro^slin, kt^ore przegra^ly "+
                "bitw^e o przetrwanie.";
        }
        else
        {
            str = "Las nie jest tu g^esty i strzelisty, drzewa rosn^a w "+
                "do^s^c sporych odleg^lo^sciach od siebie daj^ac tym samym "+
                "szans^e p^edom zupe^lnie m^lodym. Buki, klony, srebrzyste "+
                "brzozy, leszczynowe krzewy mieszaj^a si^e tu ze sob^a i "+
                "przeplataj^a, walcz^a o byt i przestrze^n. ^Snieg zalega "+
                "na ziemi grub^a warstw^a, ale jego powierzchnia jest "+
                "nier^owna, po^lamana pod wieloma kontami, usiana "+
                "niewielkimi kopczykami. Spod niekt^orych kopczyk^ow "+
                "wychylaj^a si^e nagie ga^l^ezie m^lodziutkich drzewek, "+
                "walcz^acych o byt i o przetrwanie.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Las nie jest tu g^esty i strzelisty, drzewa rosn^a w "+
                "do^s^c sporych odleg^lo^sciach od siebie daj^ac tym samym "+
                "szans^e p^edom zupe^lnie m^lodym. Buki, klony, srebrzyste "+
                "brzozy, leszczynowe krzewy mieszaj^a si^e tu ze sob^a i "+
                "przeplataj^a, walcz^a o byt i przestrze^n. Z poszycia "+
                "bij^a w g^or^e m^lodziutkie drzewka, niekt^ore pogi^ete i "+
                "po^lamane, inne dzielnie pn^ace si^e w g^or^e, mocne i "+
                "zdeterminowane by wywalczy^c dla siebie odrobin^e ziemi i "+
                "cho^c troch^e promieni s^lonecznych. Pomi^edzy ich "+
                "drobnymi pniami rozgrywa si^e kolejna walka � mech i "+
                "paprocie tocz^a ze sob^a zaci^ety b^oj. ^Sci^o^lka jest "+
                "wilgotna, pokryta warstw^a butwiej^acych li^sci, "+
                "po^lamanych ga^l^ezi i resztek ro^slin, kt^ore przegra^ly "+
                "bitw^e o przetrwanie.";
        }
        else
        {
            str = "Las nie jest tu g^esty i strzelisty, drzewa rosn^a w "+
                "do^s^c sporych odleg^lo^sciach od siebie daj^ac tym samym "+
                "szans^e p^edom zupe^lnie m^lodym. Buki, klony, srebrzyste "+
                "brzozy, leszczynowe krzewy mieszaj^a si^e tu ze sob^a i "+
                "przeplataj^a, walcz^a o byt i przestrze^n. ^Snieg zalega "+
                "na ziemi grub^a warstw^a, ale jego powierzchnia jest "+
                "nier^owna, po^lamana pod wieloma kontami, usiana "+
                "niewielkimi kopczykami. Spod niekt^orych kopczyk^ow "+
                "wychylaj^a si^e nagie ga^l^ezie m^lodziutkich drzewek, "+
                "walcz^acych o byt i o przetrwanie.";
        }
    str +="\n";
    return str;
}

