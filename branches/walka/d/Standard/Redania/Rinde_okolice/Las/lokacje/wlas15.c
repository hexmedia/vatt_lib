
/* Autor: Avard
   Data : 26.09.06
   Opis : vortak */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit WLAS_RINDE_STD;

void create_las() 
{
    set_short("Na skraju wykarczowanego lasu");
    add_exit(LAS_RINDE_LOKACJE + "las8.c","nw",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "las15.c","w",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "las23.c","s",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "tartak.c","se",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas14.c","e",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas11.c","ne",0,6,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Wykarczowany las wida^c na p^o^lnocnym-wschodzie i wschodzie. "+
        "Wej^scia do lasu znajduj^a sie na p^o^lnocnym-zachodzie, zachodzie "+
        "i po^ludniu, za^s tartak jest na po^ludniowym-wschodzie.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Na po^ludnie st^ad, za przerzedzonymi drzewami wida^c "+
            "wiod^ac^a do tartaku drog^e. W^lasnie t^edy gotowe produkty "+
            "opuszczaj^a zak^lad za^ladowane na kupieckie wozy. Otacza ci^e "+
            "zielona ^sciana lasu, tylko na wschodzie otwiera si^e przed "+
            "tob^a otwarta przesrze^n - efekt wieloletniej, wyt^e^zonej "+
            "pracy drwali. Jednak to miejsce wydaje si^e by^c oszcz^edniej "+
            "eksploatowane - tylko gdzieniegdzie wida^c wystaj^ace z "+
            "^sci^o^lki pniaki, kt^ore pozosta^ly po rosn^acym tu niegdy^s "+
            "g^estym lesie. Za g^aszczem krzak^ow wida^c tartak, za^s "+
            "dobiegaj^acy z niego ha^las skutecznie zag^lusza szum "+
            "bliskiego przecie^z lasu. Charakterystyczny zapach, podobnie "+
            "jak lataj^ace wko^lo muchy ^swiadcz^a o tym, ^ze cz^esto "+
            "zagl^adaj^a tu kupcy zmuszeni do zbyt d^lugiego oczekiwania na "+
            "za^ladowanie wozu.";
        }
        else
        {
            str = "Na po^ludnie st^ad, za przysypanymi ^sniegiem, rzadkimi "+
            "drzewami wida^c wiod^ac^a do tartaku drog^e. W^la^snie t^edy "+
            "gotowe produkty opuszczaj^a zak^lad za^ladowane na kupieckie "+
            "wozy. Otacza ci^e ^sciana o^snie^zonego lasu, tylko na "+
            "wschodzie otwiera si^e przed tob^a otwarta przestrze^n - efekt "+
            "wieloletniej, wyt^e^zonej pracy drwali. Jednak to miejsce "+
            "wydaje si^e by^c oszcz^edniej eksploatowane - tylko "+
            "gdzieniegdzie wida^c wystaj^ace z bia^lego puchu pniaki, "+
            "kt^ore pozosta^ly po rosn^acym tu niegdy^s g^estym lesie. "+
            "Za g^aszczem krzak^ow wida^c tartak, za^s dobiegaj^acy z "+
            "niego ha^las zag^lusza szum bliskiego przecie^z lasu. ^Slady "+
            "wiod^ace od tartaku w stron^e zapewniaj^acych odrobin^e "+
            "prywatno^sci krzak^ow niezbicie ^swiadcz^a o tym, ^ze cz^esto "+
            "zagl^adaj^a tu kupcy zmuszeni do zbyt d^lugiego oczekiwania na "+
            "za^ladowanie wozu.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Na po^ludnie st^ad, za przerzedzonymi drzewami wida^c "+
            "wiod^ac^a do tartaku drog^e. W^lasnie t^edy gotowe produkty "+
            "opuszczaj^a zak^lad za^ladowane na kupieckie wozy. Otacza ci^e "+
            "zielona ^sciana lasu, tylko na wschodzie otwiera si^e przed "+
            "tob^a otwarta przesrze^n - efekt wieloletniej, wyt^e^zonej "+
            "pracy drwali. Jednak to miejsce wydaje si^e by^c oszcz^edniej "+
            "eksploatowane - tylko gdzieniegdzie wida^c wystaj^ace z "+
            "^sci^o^lki pniaki, kt^ore pozosta^ly po rosn^acym tu niegdy^s "+
            "g^estym lesie. Za g^aszczem krzak^ow wida^c tartak. "+
            "Charakterystyczny zapach, podobnie "+
            "jak lataj^ace wko^lo muchy ^swiadcz^a o tym, ^ze cz^esto "+
            "zagl^adaj^a tu kupcy zmuszeni do zbyt d^lugiego oczekiwania na "+
            "za^ladowanie wozu.";
        }
        else
        {
            str = "Na po^ludnie st^ad, za przysypanymi ^sniegiem, rzadkimi "+
            "drzewami wida^c wiod^ac^a do tartaku drog^e. W^la^snie t^edy "+
            "gotowe produkty opuszczaj^a zak^lad za^ladowane na kupieckie "+
            "wozy. Otacza ci^e ^sciana o^snie^zonego lasu, tylko na "+
            "wschodzie otwiera si^e przed tob^a otwarta przestrze^n - efekt "+
            "wieloletniej, wyt^e^zonej pracy drwali. Jednak to miejsce "+
            "wydaje si^e by^c oszcz^edniej eksploatowane - tylko "+
            "gdzieniegdzie wida^c wystaj^ace z bia^lego puchu pniaki, "+
            "kt^ore pozosta^ly po rosn^acym tu niegdy^s g^estym lesie. "+
            "Za g^aszczem krzak^ow wida^c tartak. ^Slady "+
            "wiod^ace od tartaku w stron^e zapewniaj^acych odrobin^e "+
            "prywatno^sci krzak^ow niezbicie ^swiadcz^a o tym, ^ze cz^esto "+
            "zagl^adaj^a tu kupcy zmuszeni do zbyt d^lugiego oczekiwania na "+
            "za^ladowanie wozu.";
        }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Na po^ludnie st^ad, za przerzedzonymi drzewami wida^c "+
            "wiod^ac^a do tartaku drog^e. W^lasnie t^edy gotowe produkty "+
            "opuszczaj^a zak^lad za^ladowane na kupieckie wozy. Otacza ci^e "+
            "zielona ^sciana lasu, tylko na wschodzie otwiera si^e przed "+
            "tob^a otwarta przesrze^n - efekt wieloletniej, wyt^e^zonej "+
            "pracy drwali. Jednak to miejsce wydaje si^e by^c oszcz^edniej "+
            "eksploatowane - tylko gdzieniegdzie wida^c wystaj^ace z "+
            "^sci^o^lki pniaki, kt^ore pozosta^ly po rosn^acym tu niegdy^s "+
            "g^estym lesie. Za g^aszczem krzak^ow wida^c tartak. "+
            "Charakterystyczny zapach, podobnie "+
            "jak lataj^ace wko^lo muchy ^swiadcz^a o tym, ^ze cz^esto "+
            "zagl^adaj^a tu kupcy zmuszeni do zbyt d^lugiego oczekiwania na "+
            "za^ladowanie wozu.";
    }
    else
    {
        str = "Na po^ludnie st^ad, za przysypanymi ^sniegiem, rzadkimi "+
            "drzewami wida^c wiod^ac^a do tartaku drog^e. W^la^snie t^edy "+
            "gotowe produkty opuszczaj^a zak^lad za^ladowane na kupieckie "+
            "wozy. Otacza ci^e ^sciana o^snie^zonego lasu, tylko na "+
            "wschodzie otwiera si^e przed tob^a otwarta przestrze^n - efekt "+
            "wieloletniej, wyt^e^zonej pracy drwali. Jednak to miejsce "+
            "wydaje si^e by^c oszcz^edniej eksploatowane - tylko "+
            "gdzieniegdzie wida^c wystaj^ace z bia^lego puchu pniaki, "+
            "kt^ore pozosta^ly po rosn^acym tu niegdy^s g^estym lesie. "+
            "Za g^aszczem krzak^ow wida^c tartak. ^Slady "+
            "wiod^ace od tartaku w stron^e zapewniaj^acych odrobin^e "+
            "prywatno^sci krzak^ow niezbicie ^swiadcz^a o tym, ^ze cz^esto "+
            "zagl^adaj^a tu kupcy zmuszeni do zbyt d^lugiego oczekiwania na "+
            "za^ladowanie wozu.";
    }
    str += "\n";
    return str;
}
