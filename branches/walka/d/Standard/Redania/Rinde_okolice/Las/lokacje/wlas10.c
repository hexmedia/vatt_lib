
/* Autor: Avard
   Data : 09.03.07
   Opis : vortak */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit WLAS_RINDE_STD;

void create_las() 
{
    set_short("Wykarczowany las");
    add_exit(LAS_RINDE_LOKACJE + "wlas7.c","n",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas8.c","nw",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas11.c","w",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas14.c","sw",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas13.c","s",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas12.c","se",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas9.c","e",0,6,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka38.c","ne",0,6,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Wykarczowany las wida^c na p^o^lnocy, p^o^lnocnym-zachodzie, "+
        "zachodzie, po^ludniowym-zachodzie, po^ludniu, po^ludniowym-"+
        "wschodzie i wschodzie, za^s ^l^aki znajduj^a si^e "+
        "na po^ludniowym-wschodzie.\n";
}

string
dlugi_opis()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Wsz�dzie dookoo�a widzisz pnie pozosta�e po rosn�cych tu niegdy� pot�nych drzewach. Przez wiele lat w ich koronach schronienie znajdowa�y liczne gatunki ptak�w, kt�re teraz zosta�y wygnane dalej na zach�d - tam wida� ciemn� �cian� lasu, z tej odleg�o�ci wygl�daj�c� na trudn� do przebycia. W�a�nie z zachodu dociera do ciebie cichy szum drzew, a obiegaj�cy z po�udnia gwar, coraz wyra�niej wdzieraj�cy si� do twych uszu pozwala si� domy�la�, �e w�a�nie tam znajduje si� tartak. Na p�nocnym wschodzie rozci�ga si� jak okiem si�gn�� zielony dywan, gdzieniegdzie poprzetykany kolorowymi plamami kwiat�w, za� na horyzoncie dostrzegasz zarysy mur�w miejskich.";
    }
    else
    {
       str = "Niedaleko na po^ludniu widzisz g^esty, r^owny szpaler drzew - "+
           "zapewne w^la^snie tam przebiega ruchliwy i gwarny trakt "+
           "prowadz^acy z Rinde. Za^s zar^owno na p^o^lnocy jak i na "+
           "wschodzie, jak okiem si^egn^a^c, rozci^aga si^e bezkresna "+
           "^snie^zna pustynia. W^la^snie tam, gdzie^s na skraju ^l^aki "+
           "majacz^a, ledwo teraz widoczne mury obronne jakiego^s miasta. "+
           "W miejscu, w kt^orym si^e znalaz^le^s kiedy^s r^os^l g^esty "+
           "b^or - miejsce schronienia dla ptak^ow, zwierz^at. Teraz "+
           "pozosta^ly po nim liczne pot^e^zne pnie - wida^c, drwale byli "+
           "tu niedawno - m^lode drzewa jeszcze nie zdo^la^ly wyrosn^a^c "+
           "i rozpocz^a^c nier^ownej walki o przetrwanie. Ledwo widoczna "+
           "w ^sniegu ^scie^zka wiedzie na po^ludniowy-zach^od pewnie "+
           "w^la^snie tam znajduje si^e tartak.";
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Niedaleko na po^ludniu widzisz g^esty, r^owny szpaler drzew "+
            "- zapewne w^la^snie tam przebiega ruchliwy i gwarny trakt "+
            "prowadz^acy z Rinde. Za^s zar^owno na p^o^lnocy jak i na "+
            "wschodzie, jak okiem si^egn^a^c, rozci^aga si^e bezkresny "+
            "dywan kwiat^ow i g^estej, ";
        if(pora_roku() == MT_WIOSNA)
        {
            str += "soczy^scie zielonej ";
        }
        if(pora_roku() == MT_LATO)
        {
            str += "zielonej ";
        }
        if(pora_roku() == MT_JESIEN)
        {
            str += "ciemno zielonej ";
        }
        str += "trawy. W^la^snie tam, gdzie^s na skraju ^l^aki majacz^a, "+
            "ledwo teraz widoczne mury obronne jakiego^s miasta. W miejscu, "+
            "w kt^orym si^e znalaz^le^s kiedy^s r^os^l g^esty b^or - "+
            "miejsce schronienia dla ptak^ow, zwierz^at. Teraz pozosta^ly "+
            "po nim liczne pot^e^zne pnie - wida^c, drwale byli tu niedawno "+
            "- m^lode drzewa jeszcze nie zdo^la^ly wyrosn^a^c i rozpocz^a^c "+
            "nier^ownej walki o przetrwanie. Wyra^xnie wydeptana w poszyciu "+
            "^scie^zka wiedzie na po^ludniowy-zach^od pewnie w^la^snie tam "+
            "znajduje si^e tartak.";
    }
    else
    {
       str = "Niedaleko na po^ludniu widzisz g^esty, r^owny szpaler drzew - "+
           "zapewne w^la^snie tam przebiega ruchliwy i gwarny trakt "+
           "prowadz^acy z Rinde. Za^s zar^owno na p^o^lnocy jak i na "+
           "wschodzie, jak okiem si^egn^a^c, rozci^aga si^e bezkresna "+
           "^snie^zna pustynia. W^la^snie tam, gdzie^s na skraju ^l^aki "+
           "majacz^a, ledwo teraz widoczne mury obronne jakiego^s miasta. "+
           "W miejscu, w kt^orym si^e znalaz^le^s kiedy^s r^os^l g^esty "+
           "b^or - miejsce schronienia dla ptak^ow, zwierz^at. Teraz "+
           "pozosta^ly po nim liczne pot^e^zne pnie - wida^c, drwale byli "+
           "tu niedawno - m^lode drzewa jeszcze nie zdo^la^ly wyrosn^a^c "+
           "i rozpocz^a^c nier^ownej walki o przetrwanie. Ledwo widoczna "+
           "w ^sniegu ^scie^zka wiedzie na po^ludniowy-zach^od pewnie "+
           "w^la^snie tam znajduje si^e tartak.";
    }
    str += "\n";
    return str;
}
