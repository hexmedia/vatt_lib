/* Autor: Avard
   Data : 03.12.06
   Opis : Yran */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("G^esty las");
    add_exit(LAS_RINDE_LOKACJE + "las31.c","ne",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las32.c","nw",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las41.c","e",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las43.c","w",0,10,1);
    add_npc(LAS_RINDE_LIVINGI+"wiewiorka");

    add_prop(ROOM_I_INSIDE,0);
    add_npc(LAS_RINDE_LIVINGI + "sarna",1);
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Nisko zwisaj^ace ga^l^ezie m^lodych brz^oz o "+
                "snie^znobia^lych konarach znacznie utrudniaja "+
                "w^edr^owke. Wszelakie chaszcze i kolczaste krzewy "+
                "sprawiaj^a, ^ze przedarcie si^e przez g^estwine jest "+
                "nie lada wyczynem. Ponad brz^ozkami majestatycznie "+
                "unosz^a swe roz^lo^zyste li^sciaste korony buki odcinaj^ac "+
                "tym samym ni^zsze drzewka od promieni s^lonecznych. "+
                "Pod^lo^ze pokrywa gruba ^sci^o^lka sk^ladaj^aca si^e z "+
                "miekkiego mchu i le^zacych gdzieniegdzie ma^lych "+
                "ga^l^azek, kt^ore zosta^ly brutalnie wyrwane z po^scigu o "+
                "^zyciodajne ^swiat^lo przez porywisty wiatr ^lami^acy je "+
                "bez najmniejszego problemu.";
        }
        else
        {
            str = "Nisko zwisaj^ace ga^l^ezie m^lodych brz^oz o "+
                "snie^znobia^lych konarach znacznie utrudniaja w^edr^owke. "+
                "Wszelakie chaszcze i kolczaste krzewy sprawiaj^a, ^ze "+
                "przedarcie si^e przez g^estwine jest nie lada wyczynem. "+
                "Ponad brz^ozkami majestatycznie unosz^a swe roz^lo^zyste "+
                "bezlistne korony buki odcinaj^ac tym samym ni^zsze drzewka "+
                "od  promieni s^lonecznych. Pod^lo^ze pokrywa warstwa "+
                "bia^lego puchu, na kt^orej le^z^a liczne ma^le ga^l^azki, "+
                "kt^ore zosta^ly brutalnie wyrwane z po^scigu o ^zyciodajne "+
                "^swiat^lo przez porywisty wiatr ^lami^acy je bez "+
                "najmniejszego problemu.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Nisko zwisaj^ace ga^l^ezie m^lodych brz^oz o "+
                "snie^znobia^lych konarach znacznie utrudniaja "+
                "w^edr^owke. Wszelakie chaszcze i kolczaste krzewy "+
                "sprawiaj^a, ^ze przedarcie si^e przez g^estwine jest "+
                "nie lada wyczynem. Ponad brz^ozkami majestatycznie "+
                "unosz^a swe roz^lo^zyste li^sciaste korony buki odcinaj^ac "+
                "tym samym ni^zsze drzewka od promieni s^lonecznych. "+
                "Pod^lo^ze pokrywa gruba ^sci^o^lka sk^ladaj^aca si^e z "+
                "miekkiego mchu i le^zacych gdzieniegdzie ma^lych "+
                "ga^l^azek, kt^ore zosta^ly brutalnie wyrwane z po^scigu o "+
                "^zyciodajne ^swiat^lo przez porywisty wiatr ^lami^acy je "+
                "bez najmniejszego problemu.";
        }
        else
        {
            str = "Nisko zwisaj^ace ga^l^ezie m^lodych brz^oz o "+
                "snie^znobia^lych konarach znacznie utrudniaja w^edr^owke. "+
                "Wszelakie chaszcze i kolczaste krzewy sprawiaj^a, ^ze "+
                "przedarcie si^e przez g^estwine jest nie lada wyczynem. "+
                "Ponad brz^ozkami majestatycznie unosz^a swe roz^lo^zyste "+
                "bezlistne korony buki odcinaj^ac tym samym ni^zsze drzewka "+
                "od  promieni s^lonecznych. Pod^lo^ze pokrywa warstwa "+
                "bia^lego puchu, na kt^orej le^z^a liczne ma^le ga^l^azki, "+
                "kt^ore zosta^ly brutalnie wyrwane z po^scigu o ^zyciodajne "+
                "^swiat^lo przez porywisty wiatr ^lami^acy je bez "+
                "najmniejszego problemu.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Nisko zwisaj^ace ga^l^ezie m^lodych brz^oz o "+
                "snie^znobia^lych konarach znacznie utrudniaja "+
                "w^edr^owke. Wszelakie chaszcze i kolczaste krzewy "+
                "sprawiaj^a, ^ze przedarcie si^e przez g^estwine jest "+
                "nie lada wyczynem. Ponad brz^ozkami majestatycznie "+
                "unosz^a swe roz^lo^zyste li^sciaste korony buki odcinaj^ac "+
                "tym samym ni^zsze drzewka od promieni s^lonecznych. "+
                "Pod^lo^ze pokrywa gruba ^sci^o^lka sk^ladaj^aca si^e z "+
                "miekkiego mchu i le^zacych gdzieniegdzie ma^lych "+
                "ga^l^azek, kt^ore zosta^ly brutalnie wyrwane z po^scigu o "+
                "^zyciodajne ^swiat^lo przez porywisty wiatr ^lami^acy je "+
                "bez najmniejszego problemu.";
        }
        else
        {
            str = "Nisko zwisaj^ace ga^l^ezie m^lodych brz^oz o "+
                "snie^znobia^lych konarach znacznie utrudniaja w^edr^owke. "+
                "Wszelakie chaszcze i kolczaste krzewy sprawiaj^a, ^ze "+
                "przedarcie si^e przez g^estwine jest nie lada wyczynem. "+
                "Ponad brz^ozkami majestatycznie unosz^a swe roz^lo^zyste "+
                "bezlistne korony buki odcinaj^ac tym samym ni^zsze drzewka "+
                "od  promieni s^lonecznych. Pod^lo^ze pokrywa warstwa "+
                "bia^lego puchu, na kt^orej le^z^a liczne ma^le ga^l^azki, "+
                "kt^ore zosta^ly brutalnie wyrwane z po^scigu o ^zyciodajne "+
                "^swiat^lo przez porywisty wiatr ^lami^acy je bez "+
                "najmniejszego problemu.";
        }
    str +="\n";
    return str;
}

