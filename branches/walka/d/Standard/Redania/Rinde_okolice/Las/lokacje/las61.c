/* Autor: Avard
   Data : 28.11.06
   Opis : Yran */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("W lesie");
    add_exit(LAS_RINDE_LOKACJE + "las52.c","ne",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las53.c","nw",0,10,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt31.c","e",0,10,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt32.c","s",0,10,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt33.c","sw",0,10,1);
    add_npc(LAS_RINDE_LIVINGI+"wiewiorka");

    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Porywiste podmuchy wiatru nieustannie nawiedzaj^ace "+
                "okolice sprawiaj^a, ^ze rosn^ace tu dorodne, "+
                "majestatycznie pn^ace sie ku g^orze d^eby nie s^a w "+
                "stanie wytrzyma^c naporu i w dzikim ta�cu lekko ko^lysz^a "+
                "si^e na wietrze. Przez roz^lo^zyste konary rosn^acych tu "+
                "li^sciastych drzew przebija si^e ";
            if(jest_dzien() == 1)
            {
                str +="niewiele promieni ";
                if(pora_dnia() == MT_WCZESNY_RANEK || pora_dnia() == MT_RANEK)
                {
                    str +="wschodz^acego ";
                }
                if(pora_dnia() == MT_POLUDNIE || pora_dnia() == MT_POPOLUDNIE)
                {
                    str +="g^oruj^acego ";
                }
                if(pora_dnia() == MT_WIECZOR)
                {
                    str +="zachodz^acego "; 
                }
                str +="s^lo�ca rzucaj^ac niewiele ^swiat^la na najbli^zsz^a "+
                    "okolice.";
            }
            if(jest_dzien() == 0)
            {
                str+="nik^ly blask ksi^e^zyca rzucaj^acy niewiele ^swiat^la "+
                    "na najbli^zsz^a okolice.";
            }
            str +=" Pod^lo^ze tworzy mi^ekka ^sci^o^lka, na kt^orej pod "+
                "wp^lywem twojego ci^e^zaru pozostaje s^labo widoczny "+
                "^slad. Do twych uszu dochodzi cichy szum lewniwie "+
                "p^lyn^acej nieopodal rzeki ledwo widocznej pomi^edzy "+
                "grubymi konarami drzew";
        }
        else
        {
            str = "Porywiste podmuchy wiatru nieustannie nawiedzaj^ace "+
                "okolice sprawiaj^a, ^ze rosn^ace tu dorodne, "+
                "majestatycznie pn^ace sie ku g^orze d^eby nie s^a w "+
                "stanie wytrzyma^c naporu i w dzikim ta�cu lekko ko^lysz^a "+
                "si^e na wietrze. Roz^lo^zyste konary drzew pod wp^lywem "+
                "przymrozk^ow pozrzuca^ly li^scie ze swych ga^l^ezi "+
                "otwieraj^ac przestrze� na niebo i tym samym daj^ac dostep "+
                "nilicznym, przebijaj^acym sie poprzez cienk^a pokryw^e "+
                "^sniegu ro^slinkom do ^swiat^la. Na bia^lym puchu "+
                "wyra^xnie zaznaczone s^a ^slady zwierzyny ^zyj^acej na "+
                "tym obszarze, kt^ora zapewne opu^sci^la swoje zimowe "+
                "legowisko w poszukiwaniu po^zywienia. Do twych uszu "+
                "dochodzi cichy szum lewniwie p^lyn^acej nieopodal rzeki "+
                "pokrytej cz^esciowo kr^a, kt^ora jest ledwo widoczna "+
                "pomi^edzy grubymi konarami drzew.";
        }    
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Porywiste podmuchy wiatru nieustannie nawiedzaj^ace "+
                "okolice sprawiaj^a, ^ze rosn^ace tu dorodne, "+
                "majestatycznie pn^ace sie ku g^orze d^eby nie s^a w "+
                "stanie wytrzyma^c naporu i w dzikim ta�cu lekko ko^lysz^a "+
                "si^e na wietrze. Przez roz^lo^zyste konary rosn^acych tu "+
                "li^sciastych drzew przebija si^e ";
            if(jest_dzien() == 1)
            {
                str +="niewiele promienie ";
                if(pora_dnia() == MT_WCZESNY_RANEK || pora_dnia() == MT_RANEK)
                {
                    str +="wschodz^acego ";
                }
                if(pora_dnia() == MT_POLUDNIE || pora_dnia() == MT_POPOLUDNIE)
                {
                    str +="g^oruj^acego ";
                }
                if(pora_dnia() == MT_WIECZOR)
                {
                    str +="zachodz^acego "; 
                }
                str +="s^lo�ca rzucaj^acy niewiele ^swiat^la na najbli^zsz^a "+
                    "okolice.";
            }
            if(jest_dzien() == 0)
            {
                str+="nik^ly blask ksi^e^zyca rzucaj^acy niewiele ^swiat^la "+
                    "na najbli^zsz^a okolice.";
            }
            str +=" Pod^lo^ze tworzy mi^ekka ^sci^o^lka, na kt^orej pod "+
                "wp^lywem twojego ci^e^zaru pozostaje s^labo widoczny "+
                "^slad. Do twych uszu dochodzi cichy szum lewniwie "+
                "p^lyn^acej nieopodal rzeki ledwo widocznej pomi^edzy "+
                "grubymi konarami drzew";
        }
        else
        {
            str = "Porywiste podmuchy wiatru nieustannie nawiedzaj^ace "+
                "okolice sprawiaj^a, ^ze rosn^ace tu dorodne, "+
                "majestatycznie pn^ace sie ku g^orze d^eby nie s^a w "+
                "stanie wytrzyma^c naporu i w dzikim ta�cu lekko ko^lysz^a "+
                "si^e na wietrze. Roz^lo^zyste konary drzew pod wp^lywem "+
                "przymrozk^ow pozrzuca^ly li^scie ze swych ga^l^ezi "+
                "otwieraj^ac przestrze� na niebo i tym samym daj^ac dostep "+
                "nilicznym, przebijaj^acym sie poprzez cienk^a pokryw^e "+
                "^sniegu ro^slinkom do ^swiat^la. Na bia^lym puchu "+
                "wyra^xnie zaznaczone s^a ^slady zwierzyny ^zyj^acej na "+
                "tym obszarze, kt^ora zapewne opu^sci^la swoje zimowe "+
                "legowisko w poszukiwaniu po^zywienia. Do twych uszu "+
                "dochodzi cichy szum lewniwie p^lyn^acej nieopodal rzeki "+
                "pokrytej cz^esciowo kr^a, kt^ora jest ledwo widoczna "+
                "pomi^edzy grubymi konarami drzew.";
        }    
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
        if(pora_roku() != MT_ZIMA)
        {
            str = "Porywiste podmuchy wiatru nieustannie nawiedzaj^ace "+
                "okolice sprawiaj^a, ^ze rosn^ace tu dorodne, "+
                "majestatycznie pn^ace sie ku g^orze d^eby nie s^a w "+
                "stanie wytrzyma^c naporu i w dzikim ta�cu lekko ko^lysz^a "+
                "si^e na wietrze. Przez roz^lo^zyste konary rosn^acych tu "+
                "li^sciastych drzew przebija si^e ";
            if(jest_dzien() == 1)
            {
                str +="niewiele promienie ";
                if(pora_dnia() == MT_WCZESNY_RANEK || pora_dnia() == MT_RANEK)
                {
                    str +="wschodz^acego ";
                }
                if(pora_dnia() == MT_POLUDNIE || pora_dnia() == MT_POPOLUDNIE)
                {
                    str +="g^oruj^acego ";
                }
                if(pora_dnia() == MT_WIECZOR)
                {
                    str +="zachodz^acego "; 
                }
                str +="s^lo�ca rzucaj^acy niewiele ^swiat^la na najbli^zsz^a "+
                    "okolice.";
            }
            if(jest_dzien() == 0)
            {
                str+="nik^ly blask ksi^e^zyca rzucaj^acy niewiele ^swiat^la "+
                    "na najbli^zsz^a okolice.";
            }
            str +=" Pod^lo^ze tworzy mi^ekka ^sci^o^lka, na kt^orej pod "+
                "wp^lywem twojego ci^e^zaru pozostaje s^labo widoczny "+
                "^slad. Do twych uszu dochodzi cichy szum lewniwie "+
                "p^lyn^acej nieopodal rzeki ledwo widocznej pomi^edzy "+
                "grubymi konarami drzew";
        }
        else
        {
            str = "Porywiste podmuchy wiatru nieustannie nawiedzaj^ace "+
                "okolice sprawiaj^a, ^ze rosn^ace tu dorodne, "+
                "majestatycznie pn^ace sie ku g^orze d^eby nie s^a w "+
                "stanie wytrzyma^c naporu i w dzikim ta�cu lekko ko^lysz^a "+
                "si^e na wietrze. Roz^lo^zyste konary drzew pod wp^lywem "+
                "przymrozk^ow pozrzuca^ly li^scie ze swych ga^l^ezi "+
                "otwieraj^ac przestrze� na niebo i tym samym daj^ac dostep "+
                "nilicznym, przebijaj^acym sie poprzez cienk^a pokryw^e "+
                "^sniegu ro^slinkom do ^swiat^la. Na bia^lym puchu "+
                "wyra^xnie zaznaczone s^a ^slady zwierzyny ^zyj^acej na "+
                "tym obszarze, kt^ora zapewne opu^sci^la swoje zimowe "+
                "legowisko w poszukiwaniu po^zywienia. Do twych uszu "+
                "dochodzi cichy szum lewniwie p^lyn^acej nieopodal rzeki "+
                "pokrytej cz^esciowo kr^a, kt^ora jest ledwo widoczna "+
                "pomi^edzy grubymi konarami drzew.";
        }    
    
    str +="\n";
    return str;
}

