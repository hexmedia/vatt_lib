/* Autor: Avard
   Data : 29.11.06
   Opis : Yran */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit LAS_RINDE_STD;
void create_las() 
{
    set_short("W g^estym lesie");
    //add_exit(LAS_RINDE_LOKACJE + "las60.c","s",0,10,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt28.c","se",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las41.c","nw",0,10,1);
    //add_exit(LAS_RINDE_LOKACJE + "las40.c","ne",0,10,1);
    add_npc(LAS_RINDE_LIVINGI + "jelen");
    
    add_prop(ROOM_I_INSIDE,0);
    add_sit(({"na drzewie","na powalonym drzewie",
        "na przewr^oconym drzewie"}),"na powalonym drzewie",
        "z powalonego drzewa",3);
}
string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Znajdujesz si^e w g^estym lesie li^sciastym. Poskr^ecane "+
                "konary drzew tworz^a liczne pozwijane sklepienie tu^z nad "+
                "twoja g^low^a, niemal ca^lkowicie przes^laniajac niebo. "+
                "Dorodne drzewa bez w^atpienia s^a bardzo stare, jednak "+
                "ogromne pnie i majestatycznie pn^ace sie ku g^orze "+
                "roz^lo^zyste ga^l^ezie sprawiaja, ^ze wygl^adaj^a "+
                "niezwykle dostojnie, niczym stra^znicy spogl^adaj^acy na "+
                "ciebie z g^ory i ^sledz^acy ka^zdy, nawet najmniejszy "+
                "ruch. Pod^lo^ze pokrywaj^a ma^le ga^l^azki ^lami^ace sie "+
                "pod nad naporem ka^zdego, nawet najostro^zniejszego kroku "+
                "oraz rosn^aca gdzieniegdzie k^epami wy^zsza trawa b^ed^aca "+
                "znakomitym schronieniem dla ma^lych le^snych zwiarz^atek. "+
                "W pobli^zu dostrzegasz przewr^ocone drzewo b^ed^ace "+
                "zapewne ofiar^a silnych podmuch^ow wiatru jakie do^s^c "+
                "czesto nawiedzaj^a to miejsce. ";
        }
        else
        {
            str = "Znajdujesz si^e w lesie nieopodal Rinde. Tu^z nad twoj^a "+
                "g^low^a poskr^ecane konary drzew tworz^a liczne pozwijane "+
                "sklepienie. Dorodne drzewa bez w^atpienia s^a bardzo "+
                "stare, jednak ogromne pnie i majestatycznie pn^ace sie ku "+
                "g^orze roz^lo^zyste ga^l^ezie sprawiaja, ^ze wygl^adaj^a "+
                "niezwykle dostojnie, niczym stra^znicy spogl^adaj^acy na "+
                "ciebie z g^ory i ^sledz^acy ka^zdy, nawet najmniejszy "+
                "ruch. ^Snieg pokry^l ju^z niemal ca^le pod^lo^ze "+
                "przys^laniajac ^sci^o^lke, jedynie nielicznym k^epom traw "+
                "uda^lo przebi^c sie poprzez warstwe zalegaj^acego na ziemi "+
                "bia^lego puchu. W pobli^zu dostrzegasz przewr^ocone, "+
                "pokryte cie^nk^a warstw^a ^sniegu drzewo b^ed^ace zapewne "+
                "ofiar^a silnych podmuch^ow wiatru jakie do^s^c czesto "+
                "nawiedzaj^a to miejsce.";
       }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Znajdujesz si^e w g^estym lesie li^sciastym. Poskr^ecane "+
                "konary drzew tworz^a liczne pozwijane sklepienie tu^z nad "+
                "twoja g^low^a, niemal ca^lkowicie przes^laniajac niebo. "+
                "Dorodne drzewa bez w^atpienia s^a bardzo stare, jednak "+
                "ogromne pnie i majestatycznie pn^ace sie ku g^orze "+
                "roz^lo^zyste ga^l^ezie sprawiaja, ^ze wygl^adaj^a "+
                "niezwykle dostojnie, niczym stra^znicy spogl^adaj^acy na "+
                "ciebie z g^ory i ^sledz^acy ka^zdy, nawet najmniejszy "+
                "ruch. Pod^lo^ze pokrywaj^a ma^le ga^l^azki ^lami^ace sie "+
                "pod nad naporem ka^zdego, nawet najostro^zniejszego kroku "+
                "oraz rosn^aca gdzieniegdzie k^epami wy^zsza trawa b^ed^aca "+
                "znakomitym schronieniem dla ma^lych le^snych zwiarz^atek. "+
                "W pobli^zu dostrzegasz przewr^ocone drzewo b^ed^ace "+
                "zapewne ofiar^a silnych podmuch^ow wiatru jakie do^s^c "+
                "czesto nawiedzaj^a to miejsce. ";
        }
        else
        {
            str = "Znajdujesz si^e w lesie nieopodal Rinde. Tu^z nad twoj^a "+
                "g^low^a poskr^ecane konary drzew tworz^a liczne pozwijane "+
                "sklepienie. Dorodne drzewa bez w^atpienia s^a bardzo "+
                "stare, jednak ogromne pnie i majestatycznie pn^ace sie ku "+
                "g^orze roz^lo^zyste ga^l^ezie sprawiaja, ^ze wygl^adaj^a "+
                "niezwykle dostojnie, niczym stra^znicy spogl^adaj^acy na "+
                "ciebie z g^ory i ^sledz^acy ka^zdy, nawet najmniejszy "+
                "ruch. ^Snieg pokry^l ju^z niemal ca^le pod^lo^ze "+
                "przys^laniajac ^sci^o^lke, jedynie nielicznym k^epom traw "+
                "uda^lo przebi^c sie poprzez warstwe zalegaj^acego na ziemi "+
                "bia^lego puchu. W pobli^zu dostrzegasz przewr^ocone, "+
                "pokryte cie^nk^a warstw^a ^sniegu drzewo b^ed^ace zapewne "+
                "ofiar^a silnych podmuch^ow wiatru jakie do^s^c czesto "+
                "nawiedzaj^a to miejsce.";
       }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Nie jeste^s w stanie tu nic dojrze^c, panuje tutaj "+
                "kompletna ciemno^s^c.";
        }
        else
        {
            str = "Znajdujesz si^e w lesie nieopodal Rinde. Tu^z nad twoj^a "+
                "g^low^a poskr^ecane konary drzew tworz^a liczne pozwijane "+
                "sklepienie. Dorodne drzewa bez w^atpienia s^a bardzo "+
                "stare, jednak ogromne pnie i majestatycznie pn^ace sie ku "+
                "g^orze roz^lo^zyste ga^l^ezie sprawiaja, ^ze wygl^adaj^a "+
                "niezwykle dostojnie, niczym stra^znicy spogl^adaj^acy na "+
                "ciebie z g^ory i ^sledz^acy ka^zdy, nawet najmniejszy "+
                "ruch. ^Snieg pokry^l ju^z niemal ca^le pod^lo^ze "+
                "przys^laniajac ^sci^o^lke, jedynie nielicznym k^epom traw "+
                "uda^lo przebi^c sie poprzez warstwe zalegaj^acego na ziemi "+
                "bia^lego puchu. W pobli^zu dostrzegasz przewr^ocone, "+
                "pokryte cie^nk^a warstw^a ^sniegu drzewo b^ed^ace zapewne "+
                "ofiar^a silnych podmuch^ow wiatru jakie do^s^c czesto "+
                "nawiedzaj^a to miejsce.";
       }
    str += "\n";
    return str;
}