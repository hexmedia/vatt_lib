
/* Autor: Avard
   Data : 06.07.06
   Opis : Tinardan */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("^Scie^zka w lesie");
    add_exit(LAS_RINDE_LOKACJE + "las48.c","sw",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "las47.c","s",0,10,1);
    //add_exit(LAS_RINDE_LOKACJE + "las46.c","se",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las24.c","n",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las34.c","e",0,6,0);
    add_npc(LAS_RINDE_LIVINGI+"wiewiorka");

    add_prop(ROOM_I_INSIDE,0);
    add_object(LAS_RINDE_OBIEKTY+"mrowisko");
    dodaj_rzecz_niewyswietlana("pot^e^zne mrowisko",1);
    dodaj_rzecz_niewyswietlana("zniszczone mrowisko",1);

}
public string
exits_description() 
{
    return "^Scie^zka prowadzi na po^ludniowy-zach^od i wsch^od.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Ledwie widoczna w^sr^od zielonego lasu, scie^zka prowadzi "+
            "gdzie^s w g^l^ab roz^lo^zystych konar^ow drzew. Tu^z przy "+
            "skraju drogi k^l^ebi^a si^e spl^atane krzewy i krzaki obsypane "+
            "g^esto listowiem. ";
        if(jest_rzecz_w_sublokacji(0, "pot^e^zne mrowisko"))
        {
            str += "Po lewej stronie, nieco wg^l^ebi stoi pot^e^zne "+
                "mrowisko  a^z dziw, ^ze nie zosta^lo zniszczone jeszcze "+
                "przez niedba^lych podr^o^znych. ";
        }
        if(jest_rzecz_w_sublokacji(0, "zniszczone mrowisko"))
        {
            str += "Po lewej stronie, nieco wg^l^ebi stoi ca^lkowicie "+
                "zniszczone mrowisko. ";
        }
        str += "^Scie^zka nie jest szeroka, wi^ekszy w^oz mia^lby spory "+
            "problem, aby t^edy przejecha^c, cho^c l^zejsze powoziki "+
            "zmie^sci^lyby si^e, pod warunkiem, ^ze nie musia^lyby si^e "+
            "mija^c. Gdzie^s z po^ludnia wiatr przynosi gwar i ha^las - "+
            "zapewne biegnie tamt^edy jaki^s wi^ekszy trakt. S^lycha^c "+
            "pod^spiewywanie ptak^ow, ukrytych w^sr^od g^estego listowia.";
    }
    else
    {
        str = "Ledwie widoczna w^sr^od obsypanego ^sniegiem lasu, scie^zka "+
            "prowadzi gdzie^s w g^l^ab roz^lo^zystych konar^ow drzew. Tu^z "+
            "przy skraju drogi k^l^ebi^a si^e spl^atane krzewy i krzaki. ";
        if(jest_rzecz_w_sublokacji(0, "pot^e^zne mrowisko"))
        {
            str += "Po lewej stronie, nieco wg^l^ebi stoi pot^e^zne, "+
                "obsypane ^sniegiem mrowisko  a^z dziw, ^ze nie zosta^lo "+
                "zniszczone jeszcze przez niedba^lych podr^o^znych. ";
        }
        if(jest_rzecz_w_sublokacji(0, "zniszczone mrowisko"))
        {
            str += "Po lewej stronie, nieco wg^l^ebi stoi ca^lkowicie "+
                "zniszczone mrowisko. ";
        }
        str += "^Scie^zka nie jest szeroka, wi^ekszy w^oz mia^lby spory "+
            "problem, aby t^edy przejecha^c, cho^c l^zejsze powoziki "+
            "zmie^sci^lyby si^e, pod warunkiem, ^ze nie musia^lyby si^e "+
            "mija^c. Gdzie^s z po^ludnia wiatr przynosi gwar i ha^las - "+
            "zapewne biegnie tamt^edy jaki^s wi^ekszy trakt. S^lycha^c "+
            "ostre krakanie wron, siedz^acych wysoko na nagich ga^l^eziach.";
       }
       }
//Tu sie noc zaczyna
       if(jest_dzien() == 0)
       {
       if(pora_roku() != MT_ZIMA)
    {
        str = "Ledwie widoczna w^sr^od zielonego lasu, scie^zka prowadzi "+
            "gdzie^s w g^l^ab roz^lo^zystych konar^ow drzew. Tu^z przy "+
            "skraju drogi k^l^ebi^a si^e spl^atane krzewy i krzaki obsypane "+
            "g^esto listowiem. ";
        if(jest_rzecz_w_sublokacji(0, "pot^e^zne mrowisko"))
        {
            str += "Po lewej stronie, nieco wg^l^ebi stoi pot^e^zne "+
                "mrowisko  a^z dziw, ^ze nie zosta^lo zniszczone jeszcze "+
                "przez niedba^lych podr^o^znych. ";
        }
        if(jest_rzecz_w_sublokacji(0, "zniszczone mrowisko"))
        {
            str += "Po lewej stronie, nieco wg^l^ebi stoi ca^lkowicie "+
            "zniszczone mrowisko. ";
        }
        str += "^Scie^zka nie jest szeroka, wi^ekszy w^oz mia^lby spory "+
            "problem, aby t^edy przejecha^c, cho^c l^zejsze powoziki "+
            "zmie^sci^lyby si^e, pod warunkiem, ^ze nie musia^lyby "+
            "si^e mija^c.";
    }
    else
    {
        str = "Ledwie widoczna w^sr^od obsypanego ^sniegiem lasu, scie^zka "+
            "prowadzi gdzie^s w g^l^ab roz^lo^zystych konar^ow drzew. Tu^z "+
            "przy skraju drogi k^l^ebi^a si^e spl^atane krzewy i krzaki. ";
        if(jest_rzecz_w_sublokacji(0, "pot^e^zne mrowisko"))
        {
            str += "Po lewej stronie, nieco wg^l^ebi stoi pot^e^zne, "+
                "obsypane ^sniegiem mrowisko  a^z dziw, ^ze nie zosta^lo "+
                "zniszczone jeszcze przez niedba^lych podr^o^znych. ";
        }
        if(jest_rzecz_w_sublokacji(0, "zniszczone mrowisko"))
        {
            str += "Po lewej stronie, nieco wg^l^ebi stoi ca^lkowicie "+
                "zniszczone mrowisko. ";
        }
        str += "^Scie^zka nie jest szeroka, wi^ekszy w^oz mia^lby spory "+
            "problem, aby t^edy przejecha^c, cho^c l^zejsze powoziki "+
            "zmie^sci^lyby si^e, pod warunkiem, ^ze nie musia^lyby si^e "+
            "mija^c.";
        }
        }
       str += "\n";
       return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Ledwie widoczna w^sr^od zielonego lasu, scie^zka prowadzi "+
            "gdzie^s w g^l^ab roz^lo^zystych konar^ow drzew. Tu^z przy "+
            "skraju drogi k^l^ebi^a si^e spl^atane krzewy i krzaki obsypane "+
            "g^esto listowiem. ";
        if(jest_rzecz_w_sublokacji(0, "pot^e^zne mrowisko"))
        {
            str += "Po lewej stronie, nieco wg^l^ebi stoi pot^e^zne "+
                "mrowisko  a^z dziw, ^ze nie zosta^lo zniszczone jeszcze "+
                "przez niedba^lych podr^o^znych. ";
        }
        if(jest_rzecz_w_sublokacji(0, "zniszczone mrowisko"))
        {
            str += "Po lewej stronie, nieco wg^l^ebi stoi ca^lkowicie "+
            "zniszczone mrowisko. ";
        }
        str += "^Scie^zka nie jest szeroka, wi^ekszy w^oz mia^lby spory "+
            "problem, aby t^edy przejecha^c, cho^c l^zejsze powoziki "+
            "zmie^sci^lyby si^e, pod warunkiem, ^ze nie musia^lyby "+
            "si^e mija^c.";
    }
    else
    {
        str = "Ledwie widoczna w^sr^od obsypanego ^sniegiem lasu, scie^zka "+
            "prowadzi gdzie^s w g^l^ab roz^lo^zystych konar^ow drzew. Tu^z "+
            "przy skraju drogi k^l^ebi^a si^e spl^atane krzewy i krzaki. ";
        if(jest_rzecz_w_sublokacji(0, "pot^e^zne mrowisko"))
        {
            str += "Po lewej stronie, nieco wg^l^ebi stoi pot^e^zne, "+
                "obsypane ^sniegiem mrowisko  a^z dziw, ^ze nie zosta^lo "+
                "zniszczone jeszcze przez niedba^lych podr^o^znych. ";
        }
        if(jest_rzecz_w_sublokacji(0, "zniszczone mrowisko"))
        {
            str += "Po lewej stronie, nieco wg^l^ebi stoi ca^lkowicie "+
                "zniszczone mrowisko. ";
        }
        str += "^Scie^zka nie jest szeroka, wi^ekszy w^oz mia^lby spory "+
            "problem, aby t^edy przejecha^c, cho^c l^zejsze powoziki "+
            "zmie^sci^lyby si^e, pod warunkiem, ^ze nie musia^lyby si^e "+
            "mija^c.";
        }
    str += "\n";
    return str;
}
/*
Eventy: 
(je^sli masz konia) 
Tw^oj ko^n rozdyma chrapy, jakby co^s zwietrzy^l, ale po chwili uspokaja si^e. 
(wiosna, lato) Gdzie^s, ukryty w^sr^od listowia, weso^lo ^spiewa drozd. 
Mija ci^e jaki^s drwal, pod^spiewuj^ac dziarsko i melodyjnie. 
Mija ci^e pot^e^zny krasnolud, dosiadaj^acy kr^epego, g^orskiego kuca.
*/

/* if mrowisko TODO
Czujesz na sk^orze lekkie ^laskotanie. 
^Laskotanie nasila si^e. 
Au! Co^s ci^e ugryz^lo w szyj^e. 
Szukasz ^xr^od^la ^laskotania. Brrr. Oblaz^ly ci^e mr^owki!
*/