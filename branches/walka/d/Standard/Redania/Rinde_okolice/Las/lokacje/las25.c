/* Autor: Avard
   Data : 24.11.06
   Opis : Tinardan */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit LAS_RINDE_STD;
void create_las() 
{
    set_short("Skarpa w lesie");
    add_exit(LAS_RINDE_LOKACJE + "las16.c","n",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las24.c","e",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las37.c","sw",0,10,1);
    
    add_prop(ROOM_I_INSIDE,0);
}
string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Teren wznosi si^e tu ^lagodnie, przechodz^ac w "+
                "niewielkie wzg^orze. Drzewa rosn^a g^esto, buki i "+
                "brzozy ust^epuj^a miejsca pot^e^znym d^ebom o "+
                "poskr^ecanych konarach i roz^lo^zystych koronach. Nie "+
                "prowadzi t^edy ^zadna ^scie^zka, poza tymi wydeptanymi "+
                "przez zwierz^eta. Na zboczu wzg^orza, niczym naro^sle, "+
                "przyczepione rosn^a niskie krzewy szak^laku. ";
            /*Na jego ga^l^eziach b^lyszcz^a ciemnogranatowe, drobniutkie 
            owoce (maj, czerwiec; owoce s^a truj^ace). */
            str += "Na ziemi zalega gruba warstwa butwiej^acych li^sci i "+
                "po^lamanych ga^l^ezi.";
        }
        else
        {
            str = "Teren wznosi si^e tu ^lagodnie, przechodz^ac w "+
                "niewielkie wzg^orze. Drzewa rosn^a g^esto, buki i brzozy "+
                "ust^epuj^a miejsca pot^e^znym d^ebom o poskr^ecanych "+
                "konarach i roz^lo^zystych koronach. Nie prowadzi t^edy "+
                "^zadna ^scie^zka, poza tymi wydeptanymi przez zwierz^eta. "+
                "Na zboczu wzg^orza, niczym naro^sle poprzyczepiany s^a "+
                "spore kupki ^sniegu, spod kt^orych wygl^adaj^a nagie "+
                "ga^l^ezie szak^laku. W koronie kt^orego^s z wi^ekszych "+
                "drzew usadowi^lo si^e spore stadko wron i gawron^ow i "+
                "teraz ha^lasuje zawzi^ecie, jakby ka^zdy ptak chcia^l "+
                "przekrzycze^c swoich towarzyszy.";
       }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Teren wznosi si^e tu ^lagodnie, przechodz^ac w "+
                "niewielkie wzg^orze. Drzewa rosn^a g^esto, buki i "+
                "brzozy ust^epuj^a miejsca pot^e^znym d^ebom o "+
                "poskr^ecanych konarach i roz^lo^zystych koronach. Nie "+
                "prowadzi t^edy ^zadna ^scie^zka, poza tymi wydeptanymi "+
                "przez zwierz^eta. Na zboczu wzg^orza, niczym naro^sle, "+
                "przyczepione rosn^a niskie krzewy szak^laku. ";
            /*Na jego ga^l^eziach b^lyszcz^a ciemnogranatowe, drobniutkie 
            owoce (maj, czerwiec; owoce s^a truj^ace). */
            str += "Na ziemi zalega gruba warstwa butwiej^acych li^sci i "+
                "po^lamanych ga^l^ezi.";
        }
        else
        {
            str = "Teren wznosi si^e tu ^lagodnie, przechodz^ac w "+
                "niewielkie wzg^orze. Drzewa rosn^a g^esto, buki i brzozy "+
                "ust^epuj^a miejsca pot^e^znym d^ebom o poskr^ecanych "+
                "konarach i roz^lo^zystych koronach. Nie prowadzi t^edy "+
                "^zadna ^scie^zka, poza tymi wydeptanymi przez zwierz^eta. "+
                "Na zboczu wzg^orza, niczym naro^sle poprzyczepiany s^a "+
                "spore kupki ^sniegu, spod kt^orych wygl^adaj^a nagie "+
                "ga^l^ezie szak^laku. W koronie kt^orego^s z wi^ekszych "+
                "drzew usadowi^lo si^e spore stadko wron i gawron^ow i "+
                "teraz ha^lasuje zawzi^ecie, jakby ka^zdy ptak chcia^l "+
                "przekrzycze^c swoich towarzyszy.";
       }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Teren wznosi si^e tu ^lagodnie, przechodz^ac w "+
                "niewielkie wzg^orze. Drzewa rosn^a g^esto, buki i "+
                "brzozy ust^epuj^a miejsca pot^e^znym d^ebom o "+
                "poskr^ecanych konarach i roz^lo^zystych koronach. Nie "+
                "prowadzi t^edy ^zadna ^scie^zka, poza tymi wydeptanymi "+
                "przez zwierz^eta. Na zboczu wzg^orza, niczym naro^sle, "+
                "przyczepione rosn^a niskie krzewy szak^laku. ";
            /*Na jego ga^l^eziach b^lyszcz^a ciemnogranatowe, drobniutkie owoce (maj, czerwiec; owoce s^a truj^ace). TODO*/
            str += "Na ziemi zalega gruba warstwa butwiej^acych li^sci i "+
                "po^lamanych ga^l^ezi.";
        }
        else
        {
            str = "Teren wznosi si^e tu ^lagodnie, przechodz^ac w "+
                "niewielkie wzg^orze. Drzewa rosn^a g^esto, buki i brzozy "+
                "ust^epuj^a miejsca pot^e^znym d^ebom o poskr^ecanych "+
                "konarach i roz^lo^zystych koronach. Nie prowadzi t^edy "+
                "^zadna ^scie^zka, poza tymi wydeptanymi przez zwierz^eta. "+
                "Na zboczu wzg^orza, niczym naro^sle poprzyczepiany s^a "+
                "spore kupki ^sniegu, spod kt^orych wygl^adaj^a nagie "+
                "ga^l^ezie szak^laku. W koronie kt^orego^s z wi^ekszych "+
                "drzew usadowi^lo si^e spore stadko wron i gawron^ow i "+
                "teraz ha^lasuje zawzi^ecie, jakby ka^zdy ptak chcia^l "+
                "przekrzycze^c swoich towarzyszy.";
       }
    str += "\n";
    return str;
}