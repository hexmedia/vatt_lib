/* Autor: Avard
   Data : 21.11.06
   Opis : Tinardan */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("D^ebowy las");
    add_exit(LAS_RINDE_LOKACJE + "las2.c","nw",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las1.c","ne",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las15.c","s",0,10,1);

    add_prop(ROOM_I_INSIDE,0);
    add_object(LAS_RINDE_OBIEKTY+"pien");
    dodaj_rzecz_niewyswietlana("omsza^ly d^ebowy pie^n",1);

}

string
dlugi_opis()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "D^eby rosn^ace na skraju por^eby pn^a swe ga^l^ezie wysoko i "+
            "pr^e^znie. Ich pot^e^zne korzenie si^egaj^a w ^zyzn^a ziemi^e "+
            "g^l^eboko, ci^agn^ac z niej soki i wod^e. Jednak nie da si^e "+
            "nie zauwa^zy^c, ^ze cywilizacja upomina o si^e o ten, niegdy^s "+
            "cichy i zapomniany, zak^atek i wkracza tu z ca^lym swym "+
            "impetem i moc^a. Twarde d^ebowe drewno jest cennym nabytkiem "+
            "dla stolarzy, meble wykonane z takiego budulca s^a trwa^le i "+
            "eleganckie. Spokojna niegdy^s d^abrowa sta^la si^e ^latwym "+
            "celem drwali - na korze niekt^orych drzew widniej^a bia^le "+
            "znaki, Niekt^ore pnie s^a ju^z spruchnia^le i obro^sni^ete "+
            "grubym dywanem mchu, jakby ci, co je ^sci^eli, zapomnieli o "+
            "nich i pozostawili je w^lasnemu losowi. Trawa pokrywaj^aca "+
            "le^sne runo jest zadeptana, a co ni^zsze ga^l^ezie po^lamane "+
            "i usch^le.";
    }
    else
    {
        str = "D^eby rosn^ace na skraju por^eby pn^a swe ga^l^ezie "+
            "wysoko i pr^e^znie. Ich pot^e^zne korzenie si^egaj^a w "+
            "^zyzn^a ziemi^e g^l^eboko, ci^agn^ac z niej soki i wod^e. "+
            "Jednak nie da si^e nie zauwa^zy^c, ^ze cywilizacja upomina "+
            "o si^e o ten, niegdy^s cichy i zapomniany, zak^atek i "+
            "wkracza tu z ca^lym swym impetem i moc^a. Twarde d^ebowe "+
            "drewno jest cennym nabytkiem dla stolarzy, meble wykonane "+
            "z takiego budulca s^a trwa^le i eleganckie. Spokojna "+
            "niegdy^s d^abrowa sta^la si^e ^latwym celem drwali � na "+
            "korze niekt^orych drzew widniej^a bia^le znaki, a kilka "+
            "olbrzym^ow spoczywa ju^z na ziemi, poszarza^le i "+
            "bezw^ladne. ^Snieg le^z^acy pomi^edzy pniami jest szary i "+
            "zadeptany, a co ni^zsze ga^l^ezie po^lamane i usch^le.";
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "D^eby rosn^ace na skraju por^eby pn^a swe ga^l^ezie wysoko i "+
            "pr^e^znie. Ich pot^e^zne korzenie si^egaj^a w ^zyzn^a ziemi^e "+
            "g^l^eboko, ci^agn^ac z niej soki i wod^e. Jednak nie da si^e "+
            "nie zauwa^zy^c, ^ze cywilizacja upomina o si^e o ten, niegdy^s "+
            "cichy i zapomniany, zak^atek i wkracza tu z ca^lym swym "+
            "impetem i moc^a. Twarde d^ebowe drewno jest cennym nabytkiem "+
            "dla stolarzy, meble wykonane z takiego budulca s^a trwa^le i "+
            "eleganckie. Spokojna niegdy^s d^abrowa sta^la si^e ^latwym "+
            "celem drwali - na korze niekt^orych drzew widniej^a bia^le "+
            "znaki, Niekt^ore pnie s^a ju^z spruchnia^le i obro^sni^ete "+
            "grubym dywanem mchu, jakby ci, co je ^sci^eli, zapomnieli o "+
            "nich i pozostawili je w^lasnemu losowi. Trawa pokrywaj^aca "+
            "le^sne runo jest zadeptana, a co ni^zsze ga^l^ezie po^lamane "+
            "i usch^le.";
    }
    else
    {
        str = "D^eby rosn^ace na skraju por^eby pn^a swe ga^l^ezie "+
            "wysoko i pr^e^znie. Ich pot^e^zne korzenie si^egaj^a w "+
            "^zyzn^a ziemi^e g^l^eboko, ci^agn^ac z niej soki i wod^e. "+
            "Jednak nie da si^e nie zauwa^zy^c, ^ze cywilizacja upomina "+
            "o si^e o ten, niegdy^s cichy i zapomniany, zak^atek i "+
            "wkracza tu z ca^lym swym impetem i moc^a. Twarde d^ebowe "+
            "drewno jest cennym nabytkiem dla stolarzy, meble wykonane "+
            "z takiego budulca s^a trwa^le i eleganckie. Spokojna "+
            "niegdy^s d^abrowa sta^la si^e ^latwym celem drwali � na "+
            "korze niekt^orych drzew widniej^a bia^le znaki, a kilka "+
            "olbrzym^ow spoczywa ju^z na ziemi, poszarza^le i "+
            "bezw^ladne. ^Snieg le^z^acy pomi^edzy pniami jest szary i "+
            "zadeptany, a co ni^zsze ga^l^ezie po^lamane i usch^le.";
    }
    str +="\n";
    return str;
}

