/* Autor: Avard
   Data : 21.11.06
   Opis : Tinardan */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("Bukowina");
    add_exit(LAS_RINDE_LOKACJE + "las4.c","w",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las9.c","se",0,10,1);
    add_npc(LAS_RINDE_LIVINGI+"borsuk");

    add_prop(ROOM_I_INSIDE,0);

}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "W tym cichym zak^atku rosn^a jedynie stare buki, o "+
                "g^estych koronach i kompletnie go^lych, g^ladkich pniach. "+
                "Ga^l^ezie strzelaj^a w boki dopiero wysoko w g^orze, "+
                "przes^laniaj^a grubym kobiercem niebo. Panuje tu niemal "+
                "idealna cisza, brak te^z zbyt ^swie^zych ^slad^ow "+
                "bytowania cz^lowieka. Ludzie docieraj^a tu rzadko, drewno "+
                "bukowe nie jest tak cenne jak inne gatunki, u^zywane "+
                "jedynie od czasu do czasu. Poszycie le^sne jest tu "+
                "mi^ekkie, za^sciela je gruba warstwa niewielkich li^sci i "+
                "ca^le po^lacie mchu i niewielkich krzewinek. Gdzie^s "+
                "wysoko, w konarach ^spiewa uparty ptak.";
        }
        else
        {
            str = "W tym cichym zak^atku rosn^a jedynie stare buki, o "+
                "g^estych koronach i kompletnie go^lych, g^ladkich pniach. "+
                "Ga^l^ezie strzelaj^a w boki dopiero wysoko w g^orze, "+
                "przes^laniaj^a grubym kobiercem niebo. Panuje tu niemal "+
                "idealna cisza, brak te^z zbyt ^swie^zych ^slad^ow "+
                "bytowania cz^lowieka. Ludzie docieraj^a tu rzadko, "+
                "drewno bukowe nie jest tak cenne jak inne gatunki, "+
                "u^zywane jedynie od czasu do czasu. ^Snieg zalega na "+
                "ziemi warstw^a do^s^c grub^a, jest puchaty i nie "+
                "zadeptany. Gdzieniegdzie ciemniejsz^a smug^a odbijaj^a "+
                "si^e na nim tropy zwierz^at.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "W tym cichym zak^atku rosn^a jedynie stare buki, o "+
                "g^estych koronach i kompletnie go^lych, g^ladkich pniach. "+
                "Ga^l^ezie strzelaj^a w boki dopiero wysoko w g^orze, "+
                "przes^laniaj^a grubym kobiercem niebo. Panuje tu niemal "+
                "idealna cisza, brak te^z zbyt ^swie^zych ^slad^ow "+
                "bytowania cz^lowieka. Ludzie docieraj^a tu rzadko, drewno "+
                "bukowe nie jest tak cenne jak inne gatunki, u^zywane "+
                "jedynie od czasu do czasu. Poszycie le^sne jest tu "+
                "mi^ekkie, za^sciela je gruba warstwa niewielkich li^sci i "+
                "ca^le po^lacie mchu i niewielkich krzewinek. Gdzie^s "+
                "wysoko, w konarach ^spiewa uparty ptak.";
        }
        else
        {
            str = "W tym cichym zak^atku rosn^a jedynie stare buki, o "+
                "g^estych koronach i kompletnie go^lych, g^ladkich pniach. "+
                "Ga^l^ezie strzelaj^a w boki dopiero wysoko w g^orze, "+
                "przes^laniaj^a grubym kobiercem niebo. Panuje tu niemal "+
                "idealna cisza, brak te^z zbyt ^swie^zych ^slad^ow "+
                "bytowania cz^lowieka. Ludzie docieraj^a tu rzadko, "+
                "drewno bukowe nie jest tak cenne jak inne gatunki, "+
                "u^zywane jedynie od czasu do czasu. ^Snieg zalega na "+
                "ziemi warstw^a do^s^c grub^a, jest puchaty i nie "+
                "zadeptany. Gdzieniegdzie ciemniejsz^a smug^a odbijaj^a "+
                "si^e na nim tropy zwierz^at.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "W tym cichym zak^atku rosn^a jedynie stare buki, o "+
                "g^estych koronach i kompletnie go^lych, g^ladkich pniach. "+
                "Ga^l^ezie strzelaj^a w boki dopiero wysoko w g^orze, "+
                "przes^laniaj^a grubym kobiercem niebo. Panuje tu niemal "+
                "idealna cisza, brak te^z zbyt ^swie^zych ^slad^ow "+
                "bytowania cz^lowieka. Ludzie docieraj^a tu rzadko, drewno "+
                "bukowe nie jest tak cenne jak inne gatunki, u^zywane "+
                "jedynie od czasu do czasu. Poszycie le^sne jest tu "+
                "mi^ekkie, za^sciela je gruba warstwa niewielkich li^sci i "+
                "ca^le po^lacie mchu i niewielkich krzewinek. Gdzie^s "+
                "wysoko, w konarach ^spiewa uparty ptak.";
        }
        else
        {
            str = "W tym cichym zak^atku rosn^a jedynie stare buki, o "+
                "g^estych koronach i kompletnie go^lych, g^ladkich pniach. "+
                "Ga^l^ezie strzelaj^a w boki dopiero wysoko w g^orze, "+
                "przes^laniaj^a grubym kobiercem niebo. Panuje tu niemal "+
                "idealna cisza, brak te^z zbyt ^swie^zych ^slad^ow "+
                "bytowania cz^lowieka. Ludzie docieraj^a tu rzadko, "+
                "drewno bukowe nie jest tak cenne jak inne gatunki, "+
                "u^zywane jedynie od czasu do czasu. ^Snieg zalega na "+
                "ziemi warstw^a do^s^c grub^a, jest puchaty i nie "+
                "zadeptany. Gdzieniegdzie ciemniejsz^a smug^a odbijaj^a "+
                "si^e na nim tropy zwierz^at.";
        }
    str +="\n";
    return str;
}

