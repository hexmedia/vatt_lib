
/* Autor: Avard
   Data : 09.03.07
   Opis : vortak */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit WLAS_RINDE_STD;

void create_las() 
{
    set_short("W pobli^zu ^l^aki");
    add_exit(LAS_RINDE_LOKACJE + "wlas3.c","n",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas4.c","nw",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas8.c","w",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas11.c","sw",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas10.c","s",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas9.c","se",0,6,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka38.c","e",0,6,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka33.c","ne",0,6,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Wykarczowany las wida^c na p^o^lnocy, p^o^lnocnym-zachodzie, "+
        "zachodzie, po^ludniowym-zachodzie, po^ludniu i po^ludniowym-"+
        "wschodzie, za^s ^l^aki znajduj^a si^e na wschodzie i p^o^lnocnym"+
        "-wschodzie.\n";
}

string
dlugi_opis()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Rozci^agaj^aca si^e na wschodzie ^l^aka si^ega zdawa^c by "+
            "si^e mog^lo horyzontu. Faluj^aca ";
        if(pora_roku() == MT_WIOSNA)
        {
            str += "soczysta ziele^n ";
        }
        if(pora_roku() == MT_LATO)
        {
            str += "ziele^n ";
        }
        if(pora_roku() == MT_JESIEN)
        {
            str += "ciemna ziele^n ";
        }
        str += "traw jest tylko gdzieniegdzie poprzetykana ";
        if(pora_roku() != MT_JESIEN)
        {
            str += "ciemniejsz^a zieleni^a ";
        }
        else
        {
            str += "prawie czarnymi plamami ";
        }
        str +="kar^lowatych krzak^ow. Krajobraz m^ac^a jedynie widoczne "+
            "daleko st^ad, hen, na p^o^lnocnym-wschodzie mury du^zego "+
            "miasta. W miejscu kt^orym si^e znalaz^le^s kr^oluj^a niskie, "+
            "m^lode wci^a^z drzewka i pn^ace si^e jak najwy^zej, w stron^e "+
            "nieba niewielkie krzewy. Jednak gdy przyjrzysz si^e "+
            "dok^ladniej zauwa^zy^c mo^zesz ledwo widoczne w zielonej "+
            "g^estwinie pot^e^zne pnie, ^swiadcz^ace, ^ze kiedy^s szumia^l "+
            "w tym miejscu g^esty las, daj^acy schronienie ptakom i drobnej "+
            "zwierzynie. Kawa^lek dalej, na zachodzie widzisz ciemn^a "+
            "^scian^e lasu - tam jeszcze drwale wida^c nie dotarli.";
    }
    else
    {
       str = "Rozci^agaj^aca si^e na wschodzie ^l^aka si^ega zdawa^c by "+
           "si^e mog^lo horyzontu. Bia^l^a, ^snie^zn^a r^ownin^e tylko "+
           "gdzieniegdzie zaburzaj^a ciemne plamy kar^lowatych krzak^ow. "+
           "Krajobraz m^ac^a jedynie widoczne daleko st^ad, hen, na "+
           "p^o^lnocnym-wschodzie mury du^zego miasta. W miejscu kt^orym "+
           "si^e znalaz^le^s kr^oluj^a niskie, m^lode wci^a^z drzewka i "+
           "pn^ace si^e jak najwy^zej, w stron^e nieba niewielkie "+
           "krzewy. Jednak gdy przyjrzysz si^e dok^ladniej zauwa^zy^c "+
           "mo^zesz ledwo widoczne w g^estym puchu pot^e^zne pnie, "+
           "^swiadcz^ace, ^ze kiedy^s szumia^l w tym miejscu g^esty b^or, "+
           "daj^acy schronienie ptakom i drobnej zwierzynie. Kawa^lek "+
           "dalej, na zachodzie widzisz ciemn^a ^scian^e lasu - tam jeszcze "+
           "drwale wida^c nie dotarl.";
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Rozci^agaj^aca si^e na wschodzie ^l^aka si^ega zdawa^c by "+
            "si^e mog^lo horyzontu. Faluj^aca ";
        if(pora_roku() == MT_WIOSNA)
        {
            str += "soczysta ziele^n ";
        }
        if(pora_roku() == MT_LATO)
        {
            str += "ziele^n ";
        }
        if(pora_roku() == MT_JESIEN)
        {
            str += "ciemna ziele^n ";
        }
        str += "traw jest tylko gdzieniegdzie poprzetykana ";
        if(pora_roku() != MT_JESIEN)
        {
            str += "ciemniejsz^a zieleni^a ";
        }
        else
        {
            str += "prawie czarnymi plamami ";
        }
        str +="kar^lowatych krzak^ow. Krajobraz m^ac^a jedynie widoczne "+
            "daleko st^ad, hen, na p^o^lnocnym-wschodzie mury du^zego "+
            "miasta. W miejscu kt^orym si^e znalaz^le^s kr^oluj^a niskie, "+
            "m^lode wci^a^z drzewka i pn^ace si^e jak najwy^zej, w stron^e "+
            "nieba niewielkie krzewy. Jednak gdy przyjrzysz si^e "+
            "dok^ladniej zauwa^zy^c mo^zesz ledwo widoczne w zielonej "+
            "g^estwinie pot^e^zne pnie, ^swiadcz^ace, ^ze kiedy^s szumia^l "+
            "w tym miejscu g^esty las, daj^acy schronienie ptakom i drobnej "+
            "zwierzynie. Kawa^lek dalej, na zachodzie widzisz ciemn^a "+
            "^scian^e lasu - tam jeszcze drwale wida^c nie dotarli.";
    }
    else
    {
       str = "Rozci^agaj^aca si^e na wschodzie ^l^aka si^ega zdawa^c by "+
           "si^e mog^lo horyzontu. Bia^l^a, ^snie^zn^a r^ownin^e tylko "+
           "gdzieniegdzie zaburzaj^a ciemne plamy kar^lowatych krzak^ow. "+
           "Krajobraz m^ac^a jedynie widoczne daleko st^ad, hen, na "+
           "p^o^lnocnym-wschodzie mury du^zego miasta. W miejscu kt^orym "+
           "si^e znalaz^le^s kr^oluj^a niskie, m^lode wci^a^z drzewka i "+
           "pn^ace si^e jak najwy^zej, w stron^e nieba niewielkie "+
           "krzewy. Jednak gdy przyjrzysz si^e dok^ladniej zauwa^zy^c "+
           "mo^zesz ledwo widoczne w g^estym puchu pot^e^zne pnie, "+
           "^swiadcz^ace, ^ze kiedy^s szumia^l w tym miejscu g^esty b^or, "+
           "daj^acy schronienie ptakom i drobnej zwierzynie. Kawa^lek "+
           "dalej, na zachodzie widzisz ciemn^a ^scian^e lasu - tam jeszcze "+
           "drwale wida^c nie dotarl.";
    }
    str += "\n";
    return str;
}
