/* Autor: Avard
   Data : 30.11.06
   Opis : Yran */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit LAS_RINDE_STD;
void create_las() 
{
    set_short("Posr^od d^eb^ow i brz^oz");
    add_exit(LAS_RINDE_LOKACJE + "las43.c","ne",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las63.c","s",0,10,1);
    add_npc(LAS_RINDE_LIVINGI+"borsuk");
    add_prop(ROOM_I_INSIDE,0);
}
string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Wysokie d^eby wzrastaj^a wok^o^l majestatycznie pn^ac "+
                "swe grube konary ku niebu. Ciemna, chropowata kora jest "+
                "^swietnym oparciem dla porastaj^acego po^lnocn^a stron^e "+
                "drzewa mchu. Smuk^le, aczkolwiek wysokie brzozy, z jasnymi "+
                "niczym ^snieg pniami, tylko dzi^eki wytrwa^lo^sci i "+
                "uparciu dotrzymuj^a kroku d^ebom i nie daj^a si^e "+
                "zepchn^a^c na drugi tor, ^lapi^ac swoimi roz^lo^zystymi "+
                "li^s^cmi ka^zdy promie^n s^lo^nca. Teren os^loni^ety "+
                "jest ze wszystkich stron niewielkimi pag^orkami dzi^eki "+
                "czemu silny wiatr wiej^acy na tym obszarze jedynie "+
                "delikatnie muska szczyty drzew, a tu na dole w tym ma^lym "+
                "zag^l^ebieniu panuje b^loga cisza, przerywana jedynie "+
                "cichym brz^eczeniem owad^ow buszuj^acych po^sr^od "+
                "rosn^acych w pobli^zu traw. ";
        }
        else
        {
            str = "Wysokie d^eby wzrastaj^a wok^o^l majestatycznie pn^ac "+
                "swe grube konary ku niebu. Ciemna, chropowata kora jest "+
                "^swietnym oparciem dla porastaj^acego po^lnocn^a stron^e "+
                "drzewa mchu. Smuk^le, aczkolwiek wysokie brzozy, z jasnymi "+
                "niczym ^snieg pniami, tylko dzi^eki wytrwa^lo^sci i "+
                "uparciu dotrzymuj^a kroku d^ebom i nie daj^a si^e "+
                "zepchn^a^c na drugi tor. Teren os^loni^ety jest ze "+
                "wszystkich stron niewielkimi pag^orkami dzi^eki czemu "+
                "silny wiatr wiej^acy na tym obszarze jedynie delikatnie "+
                "muska nagie, bezlistne szczyty drzew, a tu na dole w tym "+
                "ma^lym zag^l^ebieniu pokrytym bia^lym puchem panuje b^loga "+
                "cisza, przerywana jedynie odg^losami wyg^lodzonych przez "+
                "brak po^zywienia i zim^e zwierz^eta.";
       }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Wysokie d^eby wzrastaj^a wok^o^l majestatycznie pn^ac "+
                "swe grube konary ku niebu. Ciemna, chropowata kora jest "+
                "^swietnym oparciem dla porastaj^acego po^lnocn^a stron^e "+
                "drzewa mchu. Smuk^le, aczkolwiek wysokie brzozy, z jasnymi "+
                "niczym ^snieg pniami, tylko dzi^eki wytrwa^lo^sci i "+
                "uparciu dotrzymuj^a kroku d^ebom i nie daj^a si^e "+
                "zepchn^a^c na drugi tor, ^lapi^ac swoimi roz^lo^zystymi "+
                "li^s^cmi ka^zdy promie^n s^lo^nca. Teren os^loni^ety "+
                "jest ze wszystkich stron niewielkimi pag^orkami dzi^eki "+
                "czemu silny wiatr wiej^acy na tym obszarze jedynie "+
                "delikatnie muska szczyty drzew, a tu na dole w tym ma^lym "+
                "zag^l^ebieniu panuje b^loga cisza, przerywana jedynie "+
                "cichym brz^eczeniem owad^ow buszuj^acych po^sr^od "+
                "rosn^acych w pobli^zu traw. ";
        }
        else
        {
            str = "Wysokie d^eby wzrastaj^a wok^o^l majestatycznie pn^ac "+
                "swe grube konary ku niebu. Ciemna, chropowata kora jest "+
                "^swietnym oparciem dla porastaj^acego po^lnocn^a stron^e "+
                "drzewa mchu. Smuk^le, aczkolwiek wysokie brzozy, z jasnymi "+
                "niczym ^snieg pniami, tylko dzi^eki wytrwa^lo^sci i "+
                "uparciu dotrzymuj^a kroku d^ebom i nie daj^a si^e "+
                "zepchn^a^c na drugi tor. Teren os^loni^ety jest ze "+
                "wszystkich stron niewielkimi pag^orkami dzi^eki czemu "+
                "silny wiatr wiej^acy na tym obszarze jedynie delikatnie "+
                "muska nagie, bezlistne szczyty drzew, a tu na dole w tym "+
                "ma^lym zag^l^ebieniu pokrytym bia^lym puchem panuje b^loga "+
                "cisza, przerywana jedynie odg^losami wyg^lodzonych przez "+
                "brak po^zywienia i zim^e zwierz^eta.";
       }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Wysokie d^eby wzrastaj^a wok^o^l majestatycznie pn^ac "+
                "swe grube konary ku niebu. Ciemna, chropowata kora jest "+
                "^swietnym oparciem dla porastaj^acego po^lnocn^a stron^e "+
                "drzewa mchu. Smuk^le, aczkolwiek wysokie brzozy, z jasnymi "+
                "niczym ^snieg pniami, tylko dzi^eki wytrwa^lo^sci i "+
                "uparciu dotrzymuj^a kroku d^ebom i nie daj^a si^e "+
                "zepchn^a^c na drugi tor, ^lapi^ac swoimi roz^lo^zystymi "+
                "li^s^cmi ka^zdy promie^n s^lo^nca. Teren os^loni^ety "+
                "jest ze wszystkich stron niewielkimi pag^orkami dzi^eki "+
                "czemu silny wiatr wiej^acy na tym obszarze jedynie "+
                "delikatnie muska szczyty drzew, a tu na dole w tym ma^lym "+
                "zag^l^ebieniu panuje b^loga cisza, przerywana jedynie "+
                "cichym brz^eczeniem owad^ow buszuj^acych po^sr^od "+
                "rosn^acych w pobli^zu traw. ";
        }
        else
        {
            str = "Wysokie d^eby wzrastaj^a wok^o^l majestatycznie pn^ac "+
                "swe grube konary ku niebu. Ciemna, chropowata kora jest "+
                "^swietnym oparciem dla porastaj^acego po^lnocn^a stron^e "+
                "drzewa mchu. Smuk^le, aczkolwiek wysokie brzozy, z jasnymi "+
                "niczym ^snieg pniami, tylko dzi^eki wytrwa^lo^sci i "+
                "uparciu dotrzymuj^a kroku d^ebom i nie daj^a si^e "+
                "zepchn^a^c na drugi tor. Teren os^loni^ety jest ze "+
                "wszystkich stron niewielkimi pag^orkami dzi^eki czemu "+
                "silny wiatr wiej^acy na tym obszarze jedynie delikatnie "+
                "muska nagie, bezlistne szczyty drzew, a tu na dole w tym "+
                "ma^lym zag^l^ebieniu pokrytym bia^lym puchem panuje b^loga "+
                "cisza, przerywana jedynie odg^losami wyg^lodzonych przez "+
                "brak po^zywienia i zim^e zwierz^eta.";
       }
    str += "\n";
    return str;
}