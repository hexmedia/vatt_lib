/* Autor: Keneth
   Data : 20.12.06
   Opis : Keneth */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit WLAS_RINDE_STD;
#define DEF_DIRS ({"po^ludniowy-wsch^od"})
void create_las() 
{
    set_short("Przy trakcie");
    add_exit(LAS_RINDE_LOKACJE + "las23.c","nw",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "tartak.c","n",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas16.c","ne",0,6,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt16.c","e",0,6,0);
    if(pora_roku() != MT_WIOSNA)
    {
        add_exit(TRAKT_RINDE_LOKACJE + "trakt17.c","se",0,4,0);
    }
    add_exit(LAS_RINDE_LOKACJE + "las45.c","s",0,6,0);
    add_npc(LAS_RINDE_LIVINGI + "lis");

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Na p^o^lnocnym-zachodzie i po^ludniu znajduj^a si^e wej^scia do "
    +"lasu, na po^lnocnym wschodzie jest wyr^ab, a na po^lnocy tartak. Na "
    +"wschodzie i po^ludniowym-wschodzie dostrzegasz trakt.\n";
}
int unq_no_move(string str)
{
    ::unq_no_move(str);

    if (member_array(query_verb(), DEF_DIRS) != -1 && 
        pora_roku() == MT_WIOSNA)
        notify_fail("Idziesz na "+ (query_verb() ~=
        "g^ora" ? "g^or^e" : query_verb()) +", jednak tam rozci^aga si^e "+
        "szerokie rozlewisko Pontaru. Po chwili zrezygnowany zawracasz.\n");
    return 0;
}


string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Drzewa i krzewy na zachodzie tworza zwart^a ^scian^e, "+
            "niemo^zliwa do przej^scia w rozs^adnym czasie. W miejscu "+
            "kt^orym  stoisz las zosta^l wyci^ety ju^z kilka lat temu "+
            "ust^epuj^ac miejsca mniejszym ro^slinom. Odg^losy dochodz^ace "+
            "ci^e ze znajduj^acego si^e na p^o^lnocy tartaku ^swiadcz^a, "+
            "^ze praca toczy si^e tam pe^lna par^a. O rosn^acym tu niegdy^s"+
            "lesie ^swiadcz^a jedynie wystaj^ace z trawy pniaki. Zaraz za "+
            "traktem biegn^acym na poludniowym-wschodzie dostrzegasz "+
            "b^l^ekitn^a wst^ege Pontaru.";
        }
        else
        {
            str = "Drzewa i krzewy na zachodzie tworza zwarta sciane, "+
            "niemo^zliwa do przejscia w rozs^adnym czasie. W miejscu kt^orym"+
            "stoisz las zosta^l wyciety juz kilka lat temu ust^epuj^ac "+
            "miejsca mniejszym roslinom. Odglosy dochodzace cie ze "+
            "znajduj^acego si^e na p^o^lnocy tartaku ^swiadcz^a, ^ze praca "+
            "toczy si^e tam pe^lna par^a. O rosn^acym tu niegdy^s lesie "+
            "^swiadcz^a jedynie wystaj^ace spod ^sniegu pniaki. Zaraz za "+
            "traktem biegn^acym na poludniowym-wschodzie dostrzegasz skute "+
            "lodem wody Pontaru.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Drzewa i krzewy na zachodzie tworz^a zwart^a sciane, "+
            "niemo^zliw^a do przejscia w rozs^adnym czasie. W miejscu "+
            "kt^orym stoisz las zosta^l wyci^ety ju^z kilka lat temu "+
            "ustepuj^ac miejsca mniejszym roslinom. W ciemnosciach ledwo "+
            "dostrzegasz stoj^acy na p^o^lnocy budynek, najprawdopodobniej "+
            "tartak. O rosn^acym tu niegdy^s lesie przypominaj^a jedynie "+
            "wystaj^ace z trawy pniaki. Zaraz za traktem biegn^acym na "+
            "poludniowym-wschodzie dostrzegasz b^l^ekitn^a wstege Pontaru.";
        }
        else
        {
            str = "Drzewa i krzewy na zachodzie tworz^a zwart^a sciane, "+
            "niemo^zliw^a do przejscia w rozs^adnym czasie. W miejscu "+
            "kt^orym stoisz las zosta^l wyci^ety ju^z kilka lat temu "+
            "ustepuj^ac miejsca mniejszym roslinom. W ciemnosciach ledwo "+
            "dostrzegasz stoj^acy na p^o^lnocy budynek, najprawdopodobniej "+
            "tartak. O rosn^acym tu niegdy^s lesie przypominaj^a jedynie "+
            "wystaj^ace z trawy pniaki.Zaraz za traktem biegn^acym na "+
            "poludniowym-wschodzie dostrzegasz skute lodem wody Pontaru.";
        }
    }
    str += "\n";
    return str;

}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Drzewa i krzewy na zachodzie tworz^a zwart^a sciane, "+
        "niemo^zliw^a do przejscia w rozs^adnym czasie. W miejscu "+
        "kt^orym stoisz las zosta^l wyci^ety ju^z kilka lat temu "+
        "ustepuj^ac miejsca mniejszym roslinom. W ciemnosciach ledwo "+
        "dostrzegasz stoj^acy na p^o^lnocy budynek, najprawdopodobniej "+
        "tartak. O rosn^acym tu niegdy^s lesie przypominaj^a jedynie "+
        "wystaj^ace z trawy pniaki. Zaraz za traktem biegn^acym na "+
        "poludniowym-wschodzie dostrzegasz b^l^ekitn^a wstege Pontaru.";
    }
    else
    {
        str = "Drzewa i krzewy na zachodzie tworz^a zwart^a sciane, "+
        "niemo^zliw^a do przejscia w rozs^adnym czasie. W miejscu "+
        "kt^orym stoisz las zosta^l wyci^ety ju^z kilka lat temu "+
        "ustepuj^ac miejsca mniejszym roslinom. W ciemnosciach ledwo "+
        "dostrzegasz stoj^acy na p^o^lnocy budynek, najprawdopodobniej "+
        "tartak. O rosn^acym tu niegdy^s lesie przypominaj^a jedynie "+
        "wystaj^ace z trawy pniaki.Zaraz za traktem biegn^acym na "+
        "poludniowym-wschodzie dostrzegasz skute lodem wody Pontaru.";
    }
    str += "\n";
    return str;
}
