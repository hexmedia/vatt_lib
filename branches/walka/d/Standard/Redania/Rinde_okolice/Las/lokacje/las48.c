/* Autor: Avard
   Data : 06.07.06
   Opis : Tinardan */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("^Scie^zka w lesie");
    add_exit(LAS_RINDE_LOKACJE + "las49.c","w",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las37.c","nw",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las36.c","n",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las35.c","ne",0,6,0);
    add_exit(LAS_RINDE_LOKACJE + "las57.c","se",0,10,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt23.c","s",0,8,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt24.c","sw",0,8,0);

    add_prop(ROOM_I_INSIDE,0);
    add_item(({"^smieci","^smiecie"}),"@@smiecie@@");

    add_object("/d/Standard/items/ubrania/onuce/stare_szarawe_Lc",3);
    add_object(LAS_RINDE_OBIEKTY+"but_z_odlepiona_podeszwa",3);
    add_object(LAS_RINDE_OBIEKTY+"but_bez_cholewki",3);
    //add_object(LAS_RINDE_OBIEKTY+"kapusta.c",3);
    dodaj_rzecz_niewyswietlana("dziurawy buk^lak", 3);
    dodaj_rzecz_niewyswietlana("but z odlepion^a podeszw^a", 3);
    dodaj_rzecz_niewyswietlana("but bez cholewki", 3);
    dodaj_rzecz_niewyswietlana("para starych szarawych onuc", 3);
    //dodaj_rzecz_niewyswietlana("gnij^aca kapusta", 3);

}
public string
exits_description() 
{
    return "^Scie^zka prowadzi na p^o^lnocny-wsch^od, za^s na po^ludniu d"+
        "ostrzegasz trakt.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Ledwie widoczna w^sr^od zielonego lasu scie^zka prowadzi "+
            "gdzie^s w g^l^ab roz^lorzystych konar^ow drzew. Wi^ekszy w^oz "+
            "mia^lby spory problem, aby t^edy przejecha^c, cho^c l^zejsze "+
            "powoziki zmie^sci^lyby si^e, pod warunkiem, ^ze nie musia^lyby "+
            "si^e mija^c. Drzewa po obu stronach rosn^a wysoko, cho^c s^a "+
            "do^s^c mocno przerzedzone  wida^c, ^ze podr^o^zni nie "+
            "stroni^a od obozowania w tych okolicach";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") ||
           jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
           jest_rzecz_w_sublokacji(0, "but bez cholewki") ||
           jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
           jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", a i nie bywaj^a tak^ze zbyt porz^adni.";
        }
        else
        {
            str +=".";
        }
        str += " Gdzie^s z po^ludnia wiatr przynosi gwar i ha^las - "+
            "zapewne biegnie tamt^edy jaki^s wi^ekszy trakt. S^lycha^c "+
            "pod^spiewywanie ptak^ow, ukrytych w^sr^od g^estego listowia.";
    }
    else
    {
        str = "Ledwie widoczna w^sr^od obsypanego ^sniegiem lasu, scie^zka "+
            "prowadzi gdzie^s w g^l^ab roz^lorzystych konar^ow drzew. "+
            "Wi^ekszy w^oz mia^lby spory problem, aby t^edy przejecha^c, "+
            "cho^c l^zejsze powoziki zmie^sci^lyby si^e, pod warunkiem, ^ze "+
            "nie musia^lyby si^e mija^c. Drzewa po obu stronach rosn^a "+
            "wysoko, cho^c s^a do^s^c mocno przerzedzone  wida^c, ^ze "+
            "podr^o^zni nie stroni^a od obozowania w tych okolicach";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
            jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
            jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
            jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
            jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", a i nie bywaj^a tak^ze zbyt porz^adni.";
        }
        else
        {
            str +=".";
        }
        str += " Gdzie^s z po^ludnia wiatr przynosi gwar i ha^las - "+
            "zapewne biegnie tamt^edy jaki^s wi^ekszy trakt. S^lycha^c ostre "+
            "krakanie wron, siedz^acych wysoko na nagich ga^l^eziach.";
        }
        }
//Tu sie noc zaczyna
    if(jest_dzien() == 0)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Ledwie widoczna w^sr^od zielonego lasu, scie^zka prowadzi "+
            "gdzie^s w g^l^ab roz^lorzystych konar^ow drzew. Wi^ekszy w^oz "+
            "mia^lby spory problem, aby t^edy przejecha^c, cho^c l^zejsze "+
            "powoziki zmie^sci^lyby si^e, pod warunkiem, ^ze nie musia^lyby "+
            "si^e mija^c. Drzewa po obu stronach rosn^a wysoko, cho^c s^a "+
            "do^s^c mocno przerzedzone  wida^c, ^ze podr^o^zni nie "+
            "stroni^a od obozowania w tych okolicach";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
           jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
           jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
           jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
           jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", a i nie bywaj^a tak^ze zbyt porz^adni.";
        }
        else
        {
            str +=".";
        }
    }
    else
    {
        str = "Ledwie widoczna w^sr^od obsypanego ^sniegiem lasu, scie^zka "+
            "prowadzi gdzie^s w g^l^ab roz^lorzystych konar^ow drzew. "+
            "Wi^ekszy w^oz mia^lby spory problem, aby t^edy przejecha^c, "+
            "cho^c l^zejsze powoziki zmie^sci^lyby si^e, pod warunkiem, "+
            "^ze nie musia^lyby si^e mija^c. Drzewa po obu stronach rosn^a "+
            "wysoko, cho^c s^a do^s^c mocno przerzedzone  wida^c, ^ze "+
            "podr^o^zni nie stroni^a od obozowania w tych okolicach";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
           jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
           jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
           jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
           jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", a i nie bywaj^a tak^ze zbyt porz^adni.";
        }
        else
        {
            str +=".";
        }
        str += " Gdzie^s z po^ludnia wiatr przynosi gwar i ha^las - "+
            "zapewne biegnie tamt^edy jaki^s wi^ekszy trakt.";
        }
        }
        str += "\n";
        return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Ledwie widoczna w^sr^od zielonego lasu, scie^zka prowadzi "+
            "gdzie^s w g^l^ab roz^lorzystych konar^ow drzew. Wi^ekszy w^oz "+
            "mia^lby spory problem, aby t^edy przejecha^c, cho^c l^zejsze "+
            "powoziki zmie^sci^lyby si^e, pod warunkiem, ^ze nie musia^lyby "+
            "si^e mija^c. Drzewa po obu stronach rosn^a wysoko, cho^c s^a "+
            "do^s^c mocno przerzedzone  wida^c, ^ze podr^o^zni nie "+
            "stroni^a od obozowania w tych okolicach";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
           jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
           jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
           jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
           jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", a i nie bywaj^a tak^ze zbyt porz^adni.";
        }
        else
        {
            str +=".";
        }
    }
    else
    {
        str = "Ledwie widoczna w^sr^od obsypanego ^sniegiem lasu, scie^zka "+
            "prowadzi gdzie^s w g^l^ab roz^lorzystych konar^ow drzew. "+
            "Wi^ekszy w^oz mia^lby spory problem, aby t^edy przejecha^c, "+
            "cho^c l^zejsze powoziki zmie^sci^lyby si^e, pod warunkiem, "+
            "^ze nie musia^lyby si^e mija^c. Drzewa po obu stronach rosn^a "+
            "wysoko, cho^c s^a do^s^c mocno przerzedzone  wida^c, ^ze "+
            "podr^o^zni nie stroni^a od obozowania w tych okolicach";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
           jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
           jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
           jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
           jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", a i nie bywaj^a tak^ze zbyt porz^adni.";
        }
        else
        {
            str +=".";
        }
        str += " Gdzie^s z po^ludnia wiatr przynosi gwar i ha^las - "+
            "zapewne biegnie tamt^edy jaki^s wi^ekszy trakt.";
        }
    str += "\n";
    return str;
}
string
smiecie()
{
    string str;
    if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
        jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
        jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
        jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
        jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
           {
        str = "Walaj^a si^e tu r^o^znego rodzaju ^smiecie";
           }
           else
           {
               str = "Nie zauwa^zasz niczego takiego";
           }
     
    if(jest_rzecz_w_sublokacji(0, "dziurawy buklak"))
           {
        str += ", dziurawe buk^laki";
           }
    if(jest_rzecz_w_sublokacji(0, "but z odlepion^a podeszw^a") || 
    jest_rzecz_w_sublokacji(0, "but bez cholewki"))  
           {
        str += ", zu^zyte buty";
           }
    if(jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
           {
        str +=", zgni^le kapusty";
           }
    if(jest_rzecz_w_sublokacji(0, "para starych szarawych onuc"))
           {
        str += " i przetarte onuce nie pierwszej ^swie^zo^sci, a niekt^orzy "+
            "nie zawahaliby si^e u^zy^c stwierdzenia, ^ze tak^ze i nie "+
            "dziesi^atej";
           }
    str += ".\n";

    return str;
}
/*
Eventy: 
(je^sli masz konia) 
Tw^oj ko^n rozdyma chrapy, jakby co^s zwietrzy^l, ale po chwili uspokaja si^e. 
(wiosna, lato) Gdzie^s, ukryty w^sr^od listowia, weso^lo ^spiewa drozd. 
Mija ci^e jaki^s drwal, pod^spiewuj^ac dziarsko i melodyjnie. 
Mija ci^e pot^e^zny krasnolud, dosiadaj^acy kr^epego, g^orskiego kuca.
*/