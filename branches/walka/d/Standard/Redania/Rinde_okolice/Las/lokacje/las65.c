/* Autor: Avard
   Data : 21.12.06
   Opis : Yran */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
inherit LAS_RINDE_STD;
void create_las() 
{
    set_short("W lesie");
    add_exit(LAS_RINDE_LOKACJE + "las55.c","ne",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las64.c","e",0,10,1);
    add_npc(LAS_RINDE_LIVINGI+"borsuk");
    add_prop(ROOM_I_INSIDE,0);
}
string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Li^sciaste, roz^lo^zyste d^eby pn^a si^e monumentalnie ku "+
				  "gorze lekko ko^lysz^ac ga^leziami pod naporem wiej^acego "+
				  "wiatru. Liczne krzewy rosn^a pomi^edzy g^esto "+
				  "rozmieszczonymi drzewami utrudniaj^ac poruszanie, co "+
				  "wi^ecej czasem i uniemo^zliwiaj^ac przej^scie w g^lab "+
				  "lasu. Niemal ca^le pod^lo^ze pokrywa zielony mech, tylko "+
				  "miejscami przebija si^e br^az ^s^c^o^lki le^snej "+
				  "z^lo^zonej g^lownie ze starych li^sci kt^ore ju^z dawno "+
				  "opad^ly na ziemi^e z powod^ow znanych tylko naturze.";
        }
        else
        {
            str = "Nagie, roz^lo^zyste d^eby pn^a si^e monumentalnie ku "+
				  "gorze lekko ko^lysz^ac ga^leziami pod naporem wiej^acego "+
				  "wiatru. Liczne krzewy rosn^a pomi^edzy g^esto "+
				  "rozmieszczonymi drzewami utrudniaj^ac poruszanie, co "+
				  "wi^ecej czasem i uniemo^zliwiaj^ac przej^scie w g^lab "+
				  "lasu. Niemal ca^le pod^lo^ze pokrywa bia^ly puch, tylko "+
				  "w niekt^orych miejscach dostrzec mo^zna ^sci^o^lk^e "+
				  "le^sn^a, ale tylko dlatemu, ^ze ogromna paj^eczyna "+
				  "ga^l^ezi zatrzyma^la padaj^acy ^snieg wysoko pod "+
				  "niebosk^lonem.";
        }
    }
    if(jest_dzien() == 0)
    {
     
   if(pora_roku() != MT_ZIMA)
        {
            str = "Li^sciaste, roz^lo^zyste d^eby pn^a si^e monumentalnie ku "+
				  "gorze lekko ko^lysz^ac ga^leziami pod naporem wiej^acego "+
				  "wiatru. Liczne krzewy rosn^a pomi^edzy g^esto "+
				  "rozmieszczonymi drzewami utrudniaj^ac poruszanie, co "+
				  "wi^ecej czasem i uniemo^zliwiaj^ac przej^scie w g^lab "+
				  "lasu. Niemal ca^le pod^lo^ze pokrywa zielony mech, tylko "+
				  "miejscami przebija si^e br^az ^s^c^o^lki le^snej "+
				  "z^lo^zonej g^lownie ze starych li^sci kt^ore ju^z dawno "+
				  "opad^ly na ziemi^e z powod^ow znanych tylko naturze.";
        }
        else
        {
            str = "Nagie, roz^lo^zyste d^eby pn^a si^e monumentalnie ku "+
				  "gorze lekko ko^lysz^ac ga^leziami pod naporem wiej^acego "+
				  "wiatru. Liczne krzewy rosn^a pomi^edzy g^esto "+
				  "rozmieszczonymi drzewami utrudniaj^ac poruszanie, co "+
				  "wi^ecej czasem i uniemo^zliwiaj^ac przej^scie w g^lab "+
				  "lasu. Niemal ca^le pod^lo^ze pokrywa bia^ly puch, tylko "+
				  "w niekt^orych miejscach dostrzec mo^zna ^sci^o^lk^e "+
				  "le^sn^a, ale tylko dlatemu, ^ze ogromna paj^eczyna "+
				  "ga^l^ezi zatrzyma^la padaj^acy ^snieg wysoko pod "+
				  "niebosk^lonem.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Li^sciaste, roz^lo^zyste d^eby pn^a si^e monumentalnie ku "+
				  "gorze lekko ko^lysz^ac ga^leziami pod naporem wiej^acego "+
				  "wiatru. Liczne krzewy rosn^a pomi^edzy g^esto "+
				  "rozmieszczonymi drzewami utrudniaj^ac poruszanie, co "+
				  "wi^ecej czasem i uniemo^zliwiaj^ac przej^scie w g^lab "+
				  "lasu. Niemal ca^le pod^lo^ze pokrywa zielony mech, tylko "+
				  "miejscami przebija si^e br^az ^s^c^o^lki le^snej "+
				  "z^lo^zonej g^lownie ze starych li^sci kt^ore ju^z dawno "+
				  "opad^ly na ziemi^e z powod^ow znanych tylko naturze.";
        }
        else
        {
            str = "Nagie, roz^lo^zyste d^eby pn^a si^e monumentalnie ku "+
				  "gorze lekko ko^lysz^ac ga^leziami pod naporem wiej^acego "+
				  "wiatru. Liczne krzewy rosn^a pomi^edzy g^esto "+
				  "rozmieszczonymi drzewami utrudniaj^ac poruszanie, co "+
				  "wi^ecej czasem i uniemo^zliwiaj^ac przej^scie w g^lab "+
				  "lasu. Niemal ca^le pod^lo^ze pokrywa bia^ly puch, tylko "+
				  "w niekt^orych miejscach dostrzec mo^zna ^sci^o^lk^e "+
				  "le^sn^a, ale tylko dlatemu, ^ze ogromna paj^eczyna "+
				  "ga^l^ezi zatrzyma^la padaj^acy ^snieg wysoko pod "+
				  "niebosk^lonem.";
        }
    str += "\n";
    return str;
}
