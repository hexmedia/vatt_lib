/* Autor: Avard
   Data : 02.06.06
   Opis : Yran */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("Polanka");
    add_exit(LAS_RINDE_LOKACJE + "las20.c","nw",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las30.c","e",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las41.c","s",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las42.c","sw",0,10,1);
    add_npc(LAS_RINDE_LIVINGI + "sarna");

    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Polanka poro^sni^eta jest licznymi krzewami, a woko^l "+
                "okalaj^a j^a wysokie, majestatycznie pn^ace sie ku g^orze "+
                "d^eby. Na ^srodku niewielkiego terenu jaki nie jest "+
                "poro^sni^ety drzewami u^lo^zono w k^o^lko kilka kamieni "+
                "tworz^ac tym samym znakomite miejsca na odpoczynek przy "+
                "ognisku po ci^e^zkiej przeprawie przez g^esty las. "+
                "Pod^lo^ze jest twarde poro^sni^ete zielonymi k^epami "+
                "traw i mchem, na kt^orym bez trudu dostrzesz ^slady "+
                "zaj^ac^ow ukrywaj^acych si^e w zaro^slach przed "+
                "czychaj^acymi w g^estwinie drapie^znikami gotowymi w "+
                "ka^zdej chwili rzuci^c si^e w szale^nczy po^scig za "+
                "swoj^a ofiar^a.";
        }
        else
        {
            str = "Polanka pokryta jest niemal w ca^lo^sci ^sniegiem, "+
                "jedynie pod kolczastymi krzewami mo^zna zauwa^zyc "+
                "fragmenty ^sci^o^lki. Okolice okalaj^a wysokie, "+
                "majestatycznie pn^ace sie ku g^orze bezlistne d^eby. Na "+
                "^srodku niewielkiego terenu u^lo^zono w k^o^lko kilka "+
                "kamieni tworz^ac tym samym znakomite miejsca na odpoczynek "+
                "przy ognisku po ci^e^zkiej przeprawie przez g^esty las. Na "+
                "pod^lo^zu zalega ubity, bia^ly puch, na kt^orym bez trudu "+
                "dostrzesz ^slady zaj^ac^ow ukrywaj^acych si^e w swoich "+
                "legowiskach przed czychaj^acymi drapie^znikami gotowymi w "+
                "ka^zdej chwili rzuci^c si^e w szale^nczy po^scig za swoj^a "+
                "ofiar^a.";
       }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Polanka poro^sni^eta jest licznymi krzewami, a woko^l "+
                "okalaj^a j^a wysokie, majestatycznie pn^ace sie ku g^orze "+
                "d^eby. Na ^srodku niewielkiego terenu jaki nie jest "+
                "poro^sni^ety drzewami u^lo^zono w k^o^lko kilka kamieni "+
                "tworz^ac tym samym znakomite miejsca na odpoczynek przy "+
                "ognisku po ci^e^zkiej przeprawie przez g^esty las. "+
                "Pod^lo^ze jest twarde poro^sni^ete zielonymi k^epami "+
                "traw i mchem, na kt^orym bez trudu dostrzesz ^slady "+
                "zaj^ac^ow ukrywaj^acych si^e w zaro^slach przed "+
                "czychaj^acymi w g^estwinie drapie^znikami gotowymi w "+
                "ka^zdej chwili rzuci^c si^e w szale^nczy po^scig za "+
                "swoj^a ofiar^a.";
        }
        else
        {
            str = "Polanka pokryta jest niemal w ca^lo^sci ^sniegiem, "+
                "jedynie pod kolczastymi krzewami mo^zna zauwa^zyc "+
                "fragmenty ^sci^o^lki. Okolice okalaj^a wysokie, "+
                "majestatycznie pn^ace sie ku g^orze bezlistne d^eby. Na "+
                "^srodku niewielkiego terenu u^lo^zono w k^o^lko kilka "+
                "kamieni tworz^ac tym samym znakomite miejsca na odpoczynek "+
                "przy ognisku po ci^e^zkiej przeprawie przez g^esty las. Na "+
                "pod^lo^zu zalega ubity, bia^ly puch, na kt^orym bez trudu "+
                "dostrzesz ^slady zaj^ac^ow ukrywaj^acych si^e w swoich "+
                "legowiskach przed czychaj^acymi drapie^znikami gotowymi w "+
                "ka^zdej chwili rzuci^c si^e w szale^nczy po^scig za swoj^a "+
                "ofiar^a.";
       }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Polanka poro^sni^eta jest licznymi krzewami, a woko^l "+
                "okalaj^a j^a wysokie, majestatycznie pn^ace sie ku g^orze "+
                "d^eby. Na ^srodku niewielkiego terenu jaki nie jest "+
                "poro^sni^ety drzewami u^lo^zono w k^o^lko kilka kamieni "+
                "tworz^ac tym samym znakomite miejsca na odpoczynek przy "+
                "ognisku po ci^e^zkiej przeprawie przez g^esty las. "+
                "Pod^lo^ze jest twarde poro^sni^ete zielonymi k^epami "+
                "traw i mchem, na kt^orym bez trudu dostrzesz ^slady "+
                "zaj^ac^ow ukrywaj^acych si^e w zaro^slach przed "+
                "czychaj^acymi w g^estwinie drapie^znikami gotowymi w "+
                "ka^zdej chwili rzuci^c si^e w szale^nczy po^scig za "+
                "swoj^a ofiar^a.";
        }
        else
        {
            str = "Polanka pokryta jest niemal w ca^lo^sci ^sniegiem, "+
                "jedynie pod kolczastymi krzewami mo^zna zauwa^zyc "+
                "fragmenty ^sci^o^lki. Okolice okalaj^a wysokie, "+
                "majestatycznie pn^ace sie ku g^orze bezlistne d^eby. Na "+
                "^srodku niewielkiego terenu u^lo^zono w k^o^lko kilka "+
                "kamieni tworz^ac tym samym znakomite miejsca na odpoczynek "+
                "przy ognisku po ci^e^zkiej przeprawie przez g^esty las. Na "+
                "pod^lo^zu zalega ubity, bia^ly puch, na kt^orym bez trudu "+
                "dostrzesz ^slady zaj^ac^ow ukrywaj^acych si^e w swoich "+
                "legowiskach przed czychaj^acymi drapie^znikami gotowymi w "+
                "ka^zdej chwili rzuci^c si^e w szale^nczy po^scig za swoj^a "+
                "ofiar^a.";
       }
    str += "\n";
    return str;
}