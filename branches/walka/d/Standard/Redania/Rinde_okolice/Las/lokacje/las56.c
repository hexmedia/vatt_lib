/* Autor: Avard
   Data : 30.06.06
   Opis : Tinardan */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("Las w pobli^zu traktu");
    add_exit(LAS_RINDE_LOKACJE + "las45.c","ne",0,10,1);
    //add_exit(LAS_RINDE_LOKACJE + "las46.c","nw",0,10,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt20.c","sw",0,8,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt19.c","s",0,8,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt18.c","e",0,8,1);
    add_npc(LAS_RINDE_LIVINGI + "sarna");

    add_prop(ROOM_I_INSIDE,0);

}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Drzewa, g^l^ownie buki, rosn^a rzadko, a ich pnie s^a "+
            "rachityczne i nie^ladne  wida^c, ^ze podr^o^znicy "+
            "korzystaj^acy z niedalekiego traktu nie stroni^a od chronienia "+
            "na brzegach lasu";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
        jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
        jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
        jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
        jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str += ", nie bardzo przy tym przejmuj^ac si^e "+
                "przyrod^a. Walaj^a si^e tu r^o^znego rodzaju ^smiecie";
        }
        else
        {
            str += ". ";
        }
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak"))
        {
            str += ", dziurawe buk^laki";
        }
        if(jest_rzecz_w_sublokacji(0, "but z odlepion^a podeszw^a") ||
            jest_rzecz_w_sublokacji(0, "but bez cholewki"))  
        {
            str += ", zu^zyte buty";
        }
        if(jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", zgni^le kapusty";
        }
        if(jest_rzecz_w_sublokacji(0, "para starych szarawych onuc"))
        {
            str += " i przetarte onuce nie pierwszej ^swie^zo^sci, a "+
                "niekt^orzy nie zawahaliby si^e u^zy^c stwierdzenia, ^ze "+
                "tak^ze i nie dziesi^atej";
        }
        str += "Trawa jest kr^ociutka i mocno wydeptana, a po^sr^od "+
            "jej nie^smia^lych k^los^ow mo^zna zobaczy^c pozosta^lo^sci po "+
            "niedawnych ogniskach i obozowiskach. ^Swiergot ptak^ow jest "+
            "s^laby i odleg^ly, zwierz^eta schroni^ly si^e raczej w miejscu "+
            "mniej dost^epnym dla niepo^z^adanych osobnik^ow. Na wschodzie "+
            "i po^ludniu wida^c przesiek^e, kt^or^a biegnie, do^s^c szeroki "+
            "ju^z w tym miejscu, trakt.";
    }
    else
    {
        str = "Drzewa, g^l^ownie buki, rosn^a rzadko, a ich pnie s^a "+
            "rachityczne i nie^ladne  wida^c, ^ze podr^o^znicy "+
            "korzystaj^acy z niedalekiego traktu nie stroni^a od "+
            "schronienia na brzegach lasu";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
        jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
        jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
        jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
        jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str += " i nie bardzo przejmuj si�przyrod. Walaj^a si^e tu "+
                "r^o^znego rodzaju ^smiecie";
        }
        else
        {
            str += ". ";
        }
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak"))
        {
            str += ", dziurawe buk^laki";
        }
        if(jest_rzecz_w_sublokacji(0, "but z odlepion^a podeszw^a") ||
            jest_rzecz_w_sublokacji(0, "but bez cholewki"))  
        {
            str += ", zu^zyte buty";
        }
        if(jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", zgni^le kapusty";
        }
        if(jest_rzecz_w_sublokacji(0, "para starych szarawych onuc"))
        {
            str += " i przetarte onuce nie pierwszej ^swie^zo^sci, a "+
                "niekt^orzy nie zawahaliby si^e u^zy^c stwierdzenia, ^ze "+
                "tak^ze i nie dziesi^atej.";
        }
        str += "^Sniegu w^la^sciwie nie ma, jedynie na wp^o^l zamarzni^ete, "+
            "brudne b^locko przypr^oszone kilkoma ^swie^zszymi p^latkami. "+
            "Nie trzeba si^e zbytnio przygl^ada^c, aby znale^c "+
            "pozosta^lo^sci po niedawnych ogniskach i obozowiskach. Szlak "+
            "na wschodzie i po^ludniu czernieje po^sr^od ^snieg^ow niczym "+
            "nie^ladna, zab^locona kreska jakiego^s niezbyt uzdolnionego "+
            "rysownika.";
        }
        }
//Tu sie noc zaczyna
        if(jest_dzien() == 0)
        {
        if(pora_roku() != MT_ZIMA)
    {
        str = "Drzewa, g^l^ownie buki, rosn^a rzadko, a ich pnie s^a "+
            "rachityczne i nie^ladne  wida^c, ^ze podr^o^znicy "+
            "korzystaj^acy z niedalekiego traktu nie stroni^a od chronienia "+
            "na brzegach lasu";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
        jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
        jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
        jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
        jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str += ", nie bardzo przy tym przejmuj^ac si^e "+
                "przyrod^a. Walaj^a si^e tu r^o^znego rodzaju ^smiecie";
        }
        else
        {
            str += ". ";
        }
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak"))
        {
            str += ", dziurawe buk^laki";
        }
        if(jest_rzecz_w_sublokacji(0, "but z odlepion^a podeszw^a") ||
            jest_rzecz_w_sublokacji(0, "but bez cholewki"))  
        {
            str += ", zu^zyte buty";
        }
        if(jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", zgni^le kapusty";
        }
        if(jest_rzecz_w_sublokacji(0, "para starych szarawych onuc"))
        {
            str += " i przetarte onuce nie pierwszej ^swie^zo^sci, a "+
                "niekt^orzy nie zawahaliby si^e u^zy^c stwierdzenia, ^ze "+
                "tak^ze i nie dziesi^atej";
        }
        str += "Trawa jest kr^ociutka i mocno wydeptana, a po^sr^od "+
            "jej nie^smia^lych k^los^ow mo^zna zobaczy^c pozosta^lo^sci po "+
            "niedawnych ogniskach i obozowiskach. ^Swiergot ptak^ow jest "+
            "s^laby i odleg^ly, zwierz^eta schroni^ly si^e raczej w miejscu "+
            "mniej dost^epnym dla niepo^z^adanych osobnik^ow. Na wschodzie "+
            "i po^ludniu wida^c przesiek^e, kt^or^a biegnie, do^s^c szeroki "+
            "ju^z w tym miejscu, trakt.";
    }
    else
    {
        str = "Drzewa, g^l^ownie buki, rosn^a rzadko, a ich pnie s^a "+
            "rachityczne i nie^ladne  wida^c, ^ze podr^o^znicy "+
            "korzystaj^acy z niedalekiego traktu nie stroni^a od "+
            "schronienia na brzegach lasu";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
        jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
        jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
        jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
        jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str += " i nie bardzo przejmuj si�przyrod. Walaj^a si^e tu "+
                "r^o^znego rodzaju ^smiecie";
        }
        else
        {
            str += ". ";
        }
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak"))
        {
            str += ", dziurawe buk^laki";
        }
        if(jest_rzecz_w_sublokacji(0, "but z odlepion^a podeszw^a") ||
            jest_rzecz_w_sublokacji(0, "but bez cholewki"))  
        {
            str += ", zu^zyte buty";
        }
        if(jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", zgni^le kapusty";
        }
        if(jest_rzecz_w_sublokacji(0, "para starych szarawych onuc"))
        {
            str += " i przetarte onuce nie pierwszej ^swie^zo^sci, a "+
                "niekt^orzy nie zawahaliby si^e u^zy^c stwierdzenia, ^ze "+
                "tak^ze i nie dziesi^atej.";
        }
        str += "^Sniegu w^la^sciwie nie ma, jedynie na wp^o^l zamarzni^ete, "+
            "brudne b^locko przypr^oszone kilkoma ^swie^zszymi p^latkami. "+
            "Nie trzeba si^e zbytnio przygl^ada^c, aby znale^c "+
            "pozosta^lo^sci po niedawnych ogniskach i obozowiskach. Szlak "+
            "na wschodzie i po^ludniu czernieje po^sr^od ^snieg^ow niczym "+
            "nie^ladna, zab^locona kreska jakiego^s niezbyt uzdolnionego "+
            "rysownika.";
        }
        }
        str += "\n";
        return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Drzewa, g^l^ownie buki, rosn^a rzadko, a ich pnie s^a "+
            "rachityczne i nie^ladne  wida^c, ^ze podr^o^znicy "+
            "korzystaj^acy z niedalekiego traktu nie stroni^a od chronienia "+
            "na brzegach lasu";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
        jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
        jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
        jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
        jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str += ", nie bardzo przy tym przejmuj^ac si^e "+
                "przyrod^a. Walaj^a si^e tu r^o^znego rodzaju ^smiecie";
        }
        else
        {
            str += ". ";
        }
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak"))
        {
            str += ", dziurawe buk^laki";
        }
        if(jest_rzecz_w_sublokacji(0, "but z odlepion^a podeszw^a") ||
            jest_rzecz_w_sublokacji(0, "but bez cholewki"))  
        {
            str += ", zu^zyte buty";
        }
        if(jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", zgni^le kapusty";
        }
        if(jest_rzecz_w_sublokacji(0, "para starych szarawych onuc"))
        {
            str += " i przetarte onuce nie pierwszej ^swie^zo^sci, a "+
                "niekt^orzy nie zawahaliby si^e u^zy^c stwierdzenia, ^ze "+
                "tak^ze i nie dziesi^atej";
        }
        str += "Trawa jest kr^ociutka i mocno wydeptana, a po^sr^od "+
            "jej nie^smia^lych k^los^ow mo^zna zobaczy^c pozosta^lo^sci po "+
            "niedawnych ogniskach i obozowiskach. ^Swiergot ptak^ow jest "+
            "s^laby i odleg^ly, zwierz^eta schroni^ly si^e raczej w miejscu "+
            "mniej dost^epnym dla niepo^z^adanych osobnik^ow. Na wschodzie "+
            "i po^ludniu wida^c przesiek^e, kt^or^a biegnie, do^s^c szeroki "+
            "ju^z w tym miejscu, trakt.";
    }
    else
    {
        str = "Drzewa, g^l^ownie buki, rosn^a rzadko, a ich pnie s^a "+
            "rachityczne i nie^ladne  wida^c, ^ze podr^o^znicy "+
            "korzystaj^acy z niedalekiego traktu nie stroni^a od "+
            "schronienia na brzegach lasu";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
        jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
        jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
        jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
        jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str += " i nie bardzo przejmuj si�przyrod. Walaj^a si^e tu "+
                "r^o^znego rodzaju ^smiecie";
        }
        else
        {
            str += ". ";
        }
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak"))
        {
            str += ", dziurawe buk^laki";
        }
        if(jest_rzecz_w_sublokacji(0, "but z odlepion^a podeszw^a") ||
            jest_rzecz_w_sublokacji(0, "but bez cholewki"))  
        {
            str += ", zu^zyte buty";
        }
        if(jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", zgni^le kapusty";
        }
        if(jest_rzecz_w_sublokacji(0, "para starych szarawych onuc"))
        {
            str += " i przetarte onuce nie pierwszej ^swie^zo^sci, a "+
                "niekt^orzy nie zawahaliby si^e u^zy^c stwierdzenia, ^ze "+
                "tak^ze i nie dziesi^atej.";
        }
        str += "^Sniegu w^la^sciwie nie ma, jedynie na wp^o^l zamarzni^ete, "+
            "brudne b^locko przypr^oszone kilkoma ^swie^zszymi p^latkami. "+
            "Nie trzeba si^e zbytnio przygl^ada^c, aby znale^c "+
            "pozosta^lo^sci po niedawnych ogniskach i obozowiskach. Szlak "+
            "na wschodzie i po^ludniu czernieje po^sr^od ^snieg^ow niczym "+
            "nie^ladna, zab^locona kreska jakiego^s niezbyt uzdolnionego "+
            "rysownika.";
        }
    str += "\n";
    return str;
}
