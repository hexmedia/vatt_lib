/* Autor: Avard
   Data : 21.12.06
   Opis : Yran */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
inherit LAS_RINDE_STD;
void create_las() 
{
    set_short("Zagajnik");
    add_exit(LAS_RINDE_LOKACJE + "las70.c","w",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las64.c","nw",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las63.c","n",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las62.c","ne",0,10,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt35.c","sw",0,10,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt34.c","s",0,10,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt33.c","s",0,10,1);
    add_prop(ROOM_I_INSIDE,0);
}
string
dlugi_opis()
{ 
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Zagajnik jest g^esto poro^sni^ety niskimi, m^lodymi "+
				"brzozami o ^snie^znobia^lych pniach. Roz^lo^zyste "+
				"li^sciaste gal^azki posplata^ly si^e ze sob^a tworz^ac "+
				"paj^eczyne koron zas^laniaj^acych niebo. Kolczaste krzaki, "+
				"liczne zaro^sla i drzewa sprawiaj^a, ^ze pomimo r^owninnego "+
				"terenu w^edr^owka poprzez g^estwin^e mo^ze by^c bardzo "+
				"ci^e^zka. Zielone k^epy wysokich traw porastaj^a ka^zda "+
				"mo^zliw^a przestrze^n utrudniaj^ac przej^scie mi^edzy nimi.";
        }
        else
        {
            str = "Zagajnik jest g^esto poro^sni^ety niskimi, m^lodymi "+
				"brzozami o ^snie^znobia^lych pniach. Roz^lo^zyste "+
				"nagie gal^azki posplata^ly si^e ze sob^a tworz^ac "+
				"paj^eczyne koron zas^laniaj^acych niebo. Kolczaste krzaki, "+
				"liczne zaro^sla i drzewa sprawiaj^a, ^ze pomimo r^owninnego "+
				"terenu w^edr^owka poprzez g^estwin^e mo^ze by^c bardzo "+
				"ci^e^zka. Zolte k^epki traw nie^smialo wystaj^a z wysokiego "+
				"^sniegu kt^ory w po^l^aczeniu z licznymi zaro^slami tworzy "+
				"przeszkod^e niemal nie do przej^scia.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Zagajnik jest g^esto poro^sni^ety niskimi, m^lodymi "+
				"brzozami o ^snie^znobia^lych pniach. Roz^lo^zyste "+
				"li^sciaste gal^azki posplata^ly si^e ze sob^a tworz^ac "+
				"paj^eczyne koron zas^laniaj^acych niebo. Kolczaste krzaki, "+
				"liczne zaro^sla i drzewa sprawiaj^a, ^ze pomimo r^owninnego "+
				"terenu w^edr^owka poprzez g^estwin^e mo^ze by^c bardzo "+
				"ci^e^zka. Zielone k^epy wysokich traw porastaj^a ka^zda "+
				"mo^zliw^a przestrze^n utrudniaj^ac przej^scie mi^edzy nimi.";
        }
        else
        {
            str = "Zagajnik jest g^esto poro^sni^ety niskimi, m^lodymi "+
				"brzozami o ^snie^znobia^lych pniach. Roz^lo^zyste "+
				"nagie gal^azki posplata^ly si^e ze sob^a tworz^ac "+
				"paj^eczyne koron zas^laniaj^acych niebo. Kolczaste krzaki, "+
				"liczne zaro^sla i drzewa sprawiaj^a, ^ze pomimo r^owninnego "+
				"terenu w^edr^owka poprzez g^estwin^e mo^ze by^c bardzo "+
				"ci^e^zka. Zolte k^epki traw nie^smialo wystaj^a z wysokiego "+
				"^sniegu kt^ory w po^l^aczeniu z licznymi zaro^slami tworzy "+
				"przeszkod^e niemal nie do przej^scia.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Zagajnik jest g^esto poro^sni^ety niskimi, m^lodymi "+
				"brzozami o ^snie^znobia^lych pniach. Roz^lo^zyste "+
				"li^sciaste gal^azki posplata^ly si^e ze sob^a tworz^ac "+
				"paj^eczyne koron zas^laniaj^acych niebo. Kolczaste krzaki, "+
				"liczne zaro^sla i drzewa sprawiaj^a, ^ze pomimo r^owninnego "+
				"terenu w^edr^owka poprzez g^estwin^e mo^ze by^c bardzo "+
				"ci^e^zka. Zielone k^epy wysokich traw porastaj^a ka^zda "+
				"mo^zliw^a przestrze^n utrudniaj^ac przej^scie mi^edzy nimi.";
        }
        else
        {
            str = "Zagajnik jest g^esto poro^sni^ety niskimi, m^lodymi "+
				"brzozami o ^snie^znobia^lych pniach. Roz^lo^zyste "+
				"nagie gal^azki posplata^ly si^e ze sob^a tworz^ac "+
				"paj^eczyne koron zas^laniaj^acych niebo. Kolczaste krzaki, "+
				"liczne zaro^sla i drzewa sprawiaj^a, ^ze pomimo r^owninnego "+
				"terenu w^edr^owka poprzez g^estwin^e mo^ze by^c bardzo "+
				"ci^e^zka. Zolte k^epki traw nie^smialo wystaj^a z wysokiego "+
				"^sniegu kt^ory w po^l^aczeniu z licznymi zaro^slami tworzy "+
				"przeszkod^e niemal nie do przej^scia.";
        }
    str += "\n";
    return str;
}
