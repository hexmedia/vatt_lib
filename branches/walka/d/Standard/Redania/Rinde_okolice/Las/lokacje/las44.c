
/* Autor: Keneth
   Data : 27.12.06
   Opis : Keneth */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit LAS_RINDE_STD;
void create_las() 
{
    set_short("W g^estwinie");
    add_exit(LAS_RINDE_LOKACJE + "las33.c","ne",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las55.c","s",0,10,1);
    add_npc(LAS_RINDE_LIVINGI + "lis");
    
    add_prop(ROOM_I_INSIDE,0);
}
string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Krzewy i m^lode drzewa walcz^a tu mi^edzy sob^a o "+
                "odrobin^e miejsca i dost^epdo ^swiat^la. Efektem ich walki "+
                "jest trudna do przebycia pl^atanina ga^l^ezi, w^sr^od kt^orej "+
                "z trudem dostrzegasz ledwo widoczn^a scie^zke ci^agn^ac^a "+
                "si^e z p^o^lnocnego wschodu na po^ludnie. Zosta^la "+
                "najpewniej wydeptana przez dzikie zwierz^eta i nielicznych "+
                "w tej cze^sci lasu drwali lub my^sliwych. Dominuj^a tu "+
                "d^eby, buki i brzozy kt^orych bia^le pnie wyra^znie "+
                "odcinaj^a si^e na tle ciemnego lasu. Cze^sc opadaj^acych z "+
                "g^ory li^sci zatrzymuje si^e na rozpi^etych mi^edzy "+
                "ga^l^eziami drzew i krzew^ow paj^eczynach, l^sni^acych "+
                "delikatnie w promieniach s^lo^nca. ";
        }
        else
        {
            str = "Krzewy i m^lode drzewa walcz^a tu mi^edzy sob^a o "+
                "odrobin^e miejsca i dost^epdo ^swiat^la. Efektem ich "+
                "walki jest trudna do przebycia pl^atanina ga^l^ezi, w^sr^od "+
                "kt^orej z trudem dostrzegasz ledwo widoczn^a scie^zke "+
                "ci^agn^ac^a si^e z p^o^lnocnego wschodu na po^ludnie. "+
                "Zosta^la najpewniej wydeptana przez dzikie zwierz^eta i "+
                "nielicznych w tej cze^sci lasu drwali lub my^sliwych. "+
                "Dominuj^a tu d^eby, buki i brzozy kt^orych bia^le pnie "+
                "wyra^znie odcinaj^a si^e na tle ciemnego lasu. Cze^sc "+
                "opadaj^acych z g^ory li^sci zatrzymuje si^e na rozpi^etych "+
                "mi^edzy ga^l^eziami drzew i krzew^ow paj^eczynach, "+
                "l^sni^acych delikatnie w promieniach s^lo^nca. Dodatkowo o "+
                "tej porze roku otaczaj^aca ci^e g^estwina przykryta jest "+
                "grub^a warstw^a ^sniegu.";
       }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Krzewy i m^lode drzewa walcz^a tu mi^edzy sob^a o "+
                "odrobin^e miejsca i dost^ep do ^swiat^la. Efektem ich "+
                "walki jest trudna do przebycia pl^atanina ga^l^ezi w^sr^od, "+
                "kt^orej z trudem dostrzegasz ledwo widoczn^a scie^zke "+
                "ci^agn^ac^a si^e z p^o^lnocnego wschodu na po^ludnie. "+
                "Zosta^la najpewniej wydeptana przez dzikie zwierz^eta i "+
                "nielicznych w tej cze^sci lasu drwali lub my^sliwych. "+
                "Dominuj^a tu d^eby, buki i brzozy kt^orych bia^le pnie "+
                "wyra^znie odcinaj^a si^e na tle ciemnego lasu. Cze^sc "+
                "opadaj^acych z g^ory li^sci zatrzymuje si^e na rozpi^etych "+
                "mi^edzy ga^l^eziami drzew i krzew^ow paj^eczynach, "+
                "l^sni^acych delikatnie w ^swietle gwiazd.";
        }
        else
        {
            str = "Krzewy i m^lode drzewa walcz^a tu mi^edzy sob^a o "+
                "odrobin^e miejsca i dost^ep do ^swiat^la. Efektem ich walki "+
                "jest trudna do przebycia pl^atanina ga^l^ezi, w^sr^od kt^orej "+
                "z trudem dostrzegasz ledwo widoczn^a scie^zke ci^agn^ac^a "+
                "si^e z p^o^lnocnego wschodu na po^ludnie. Zosta^la "+
                "najpewniej wydeptana przez dzikie zwierz^eta i nielicznych "+
                "w tej cze^sci lasu drwali lub my^sliwych. Dominuj^a tu "+
                "d^eby, buki i brzozy kt^orych bia^le pnie wyra^znie "+
                "odcinaj^a si^e na tle ciemnego lasu. Cze^sc opadaj^acych z "+
                "g^ory li^sci zatrzymuje si^e na rozpi^etych mi^edzy "+
                "ga^l^eziami drzew i krzew^ow paj^eczynach, l^sni^acych "+
                "delikatnie w ^swietle gwiazd. Dodatkowo o tej porze roku "+
                "otaczaj^aca ci^e g^estwina przykryta jest grub^a warstw^a "+
                "^sniegu.";
       }
    }
    str += "\n";

    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Krzewy i m^lode drzewa walcz^a tu mi^edzy sob^a o "+
                "odrobin^e miejsca i dost^ep do ^swiat^la. Efektem ich "+
                "walki jest trudna do przebycia pl^atanina ga^l^ezi w^sr^od "+
                "kt^orej z trudem dostrzegasz ledwo widoczn^a scie^zke "+
                "ci^agn^ac^a si^e z p^o^lnocnego wschodu na po^ludnie. "+
                "Zosta^la najpewniej wydeptana przez dzikie zwierz^eta i "+
                "nielicznych w tej cze^sci lasu drwali lub my^sliwych. "+
                "Dominuj^a tu d^eby, buki i brzozy kt^orych bia^le pnie "+
                "wyra^znie odcinaj^a si^e na tle ciemnego lasu. Co kilka "+
                "krok^ow do twojej twarzy przykleja si^e nieprzyjemna "+
                "paj^eczyna.";
        }
        else
        {
            str = "Krzewy i m^lode drzewa walcz^a tu mi^edzy sob^a o "+
                "odrobin^e miejsca i dost^ep do ^swiat^la. Efektem ich walki "+
                "jest trudna do przebycia pl^atanina ga^l^ezi w^sr^od kt^orej "+
                "z trudem dostrzegasz ledwo widoczn^a scie^zke ci^agn^ac^a "+
                "si^e z p^o^lnocnego wschodu na po^ludnie. Zosta^la "+
                "najpewniej wydeptana przez dzikie zwierz^eta i nielicznych "+
                "w tej cze^sci lasu drwali lub my^sliwych. Dominuj^a tu "+
                "d^eby, buki i brzozy kt^orych bia^le pnie wyra^znie "+
                "odcinaj^a si^e na tle ciemnego lasu.Co kilka krok^ow do "+
                "twojej twarzy przykleja si^e nieprzyjemna paj^eczyna. "+
                "Dodatkowo o tej porze roku otaczaj^aca ci^e g^estwina "+
                "przykryta jest grub^a warstw^a ^sniegu.";
       }
    str += "\n";
    return str;
}
