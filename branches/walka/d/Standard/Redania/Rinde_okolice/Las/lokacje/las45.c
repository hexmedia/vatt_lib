/* Autor: Avard
   Data : 14.10.06
   Opis : Tinardan */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;
#define DEF_DIRS ({"wsch^od"})
void create_las() 
{
    set_short("Bukowy lasek");
    add_exit(LAS_RINDE_LOKACJE + "las56.c","sw",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas17.c","n",0,10,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt16.c","ne",0,8,1);
    if(pora_roku() != MT_WIOSNA)
    {
        add_exit(TRAKT_RINDE_LOKACJE + "trakt17.c","e",0,4,0);
    }
    add_exit(TRAKT_RINDE_LOKACJE + "trakt18.c","s",0,8,1);
    add_npc(LAS_RINDE_LIVINGI+"borsuk");

    add_prop(ROOM_I_INSIDE,0);
}
int unq_no_move(string str)
{
    ::unq_no_move(str);

    if (member_array(query_verb(), DEF_DIRS) != -1 && 
        pora_roku() == MT_WIOSNA)
        notify_fail("Idziesz na "+ (query_verb() ~=
        "g^ora" ? "g^or^e" : query_verb()) +", jednak tam rozci^aga si^e "+
        "szerokie rozlewisko Pontaru. Po chwili zrezygnowany zawracasz.\n");
    return 0;
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Pomi^edzy rosn^acymi tutaj bukami wida^c prowadz^ac^a z "+
            "po^ludnia na p^o^lnocny wsch^od wydeptan^a w trawie "+
            "^scie^zk^e, na tyle szerok^a, ^ze bez wi^ekszych problem^ow "+
            "zmie^sci^c si^e na niej powinien nawet kupiecki w^oz. Wygl^ada "+
            "na to, ^ze w^la^snie t^edy podr^o^zni omijaj^a zalewany przez "+
            "szumi^acy na wschodzie Pontar odcinek traktu. Drzewa "+
            "otaczaj^ace ^scie^zk^e s^a powykrzywiane, rachityczne. Chyba "+
            "tylko temu zawdzi^eczaj^a to, ^ze nie trafi^ly jeszcze pod "+
            "ostrza drwalskich topor^ow. Szum rzeki miesza si^e z "+
            "pokrzykiwaniami dobiegaj^acymi z widocznego niedaleko tartaku "+
            "oraz ^spiewem ptak^ow ukrytych w zielonych koronach drzew.";
        }
        else
        {
            str = "Pomi^edzy rosn^acymi tutaj bukami wida^c prowadz^ac^a z "+
            "po^ludnia na p^o^lnocny wsch^od przysypan^a ^sniegiem "+
            "^scie^zk^e, na tyle szerok^a, ^ze bez wi^ekszych problem^ow "+
            "zmie^sci^c si^e na niej powinien nawet kupiecki w^oz. "+
            "Wygl^ada na to, ^ze w^la^snie t^edy podr^o^zni omijaj^a "+
            "zalewany przez widoczny na wschodzie Pontar odcinek traktu. "+
            "Drzewa otaczaj^ace ^scie^zk^e s^a powykrzywiane, rachityczne. "+
            "Chyba tylko temu zawdzi^eczaj^a to, ^ze nie trafi^ly jeszcze "+
            "pod ostrza drwalskich topor^ow. Pokrzykiwania dobiegaj^ace z "+
            "widocznego niedaleko tartaku mieszaj^a si^e z s^lyszalnym od "+
            "czasu do czasu trzeszczeniem grubej warstwy lodu pokrywaj^acej "+
            "rzek^e.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Pomi^edzy rosn^acymi tutaj bukami wida^c prowadz^ac^a z "+
            "po^ludnia na p^o^lnocny wsch^od wydeptan^a w trawie "+
            "^scie^zk^e, na tyle szerok^a, ^ze bez wi^ekszych problem^ow "+
            "zmie^sci^c si^e na niej powinien nawet kupiecki w^oz. Wygl^ada "+
            "na to, ^ze w^la^snie t^edy podr^o^zni omijaj^a zalewany przez "+
            "szumi^acy na wschodzie Pontar odcinek traktu. Drzewa "+
            "otaczaj^ace ^scie^zk^e s^a powykrzywiane, rachityczne. Chyba "+
            "tylko temu zawdzi^eczaj^a to, ^ze nie trafi^ly jeszcze pod "+
            "ostrza drwalskich topor^ow. Szum rzeki miesza si^e z "+
            "pokrzykiwaniami dobiegaj^acymi z widocznego niedaleko tartaku "+
            "oraz ^spiewem ptak^ow ukrytych w zielonych koronach drzew.";
        }
        else
        {
            str = "Pomi^edzy rosn^acymi tutaj bukami wida^c prowadz^ac^a z "+
            "po^ludnia na p^o^lnocny wsch^od przysypan^a ^sniegiem "+
            "^scie^zk^e, na tyle szerok^a, ^ze bez wi^ekszych problem^ow "+
            "zmie^sci^c si^e na niej powinien nawet kupiecki w^oz. "+
            "Wygl^ada na to, ^ze w^la^snie t^edy podr^o^zni omijaj^a "+
            "zalewany przez widoczny na wschodzie Pontar odcinek traktu. "+
            "Drzewa otaczaj^ace ^scie^zk^e s^a powykrzywiane, rachityczne. "+
            "Chyba tylko temu zawdzi^eczaj^a to, ^ze nie trafi^ly jeszcze "+
            "pod ostrza drwalskich topor^ow. Pokrzykiwania dobiegaj^ace z "+
            "widocznego niedaleko tartaku mieszaj^a si^e z s^lyszalnym od "+
            "czasu do czasu trzeszczeniem grubej warstwy lodu pokrywaj^acej "+
            "rzek^e.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Pomi^edzy rosn^acymi tutaj bukami wida^c prowadz^ac^a z "+
            "po^ludnia na p^o^lnocny wsch^od wydeptan^a w trawie "+
            "^scie^zk^e, na tyle szerok^a, ^ze bez wi^ekszych problem^ow "+
            "zmie^sci^c si^e na niej powinien nawet kupiecki w^oz. Wygl^ada "+
            "na to, ^ze w^la^snie t^edy podr^o^zni omijaj^a zalewany przez "+
            "szumi^acy na wschodzie Pontar odcinek traktu. Drzewa "+
            "otaczaj^ace ^scie^zk^e s^a powykrzywiane, rachityczne. Chyba "+
            "tylko temu zawdzi^eczaj^a to, ^ze nie trafi^ly jeszcze pod "+
            "ostrza drwalskich topor^ow. Szum rzeki miesza si^e z "+
            "pokrzykiwaniami dobiegaj^acymi z widocznego niedaleko tartaku "+
            "oraz ^spiewem ptak^ow ukrytych w zielonych koronach drzew.";
    }
    else
    {
        str = "Pomi^edzy rosn^acymi tutaj bukami wida^c prowadz^ac^a z "+
            "po^ludnia na p^o^lnocny wsch^od przysypan^a ^sniegiem "+
            "^scie^zk^e, na tyle szerok^a, ^ze bez wi^ekszych problem^ow "+
            "zmie^sci^c si^e na niej powinien nawet kupiecki w^oz. "+
            "Wygl^ada na to, ^ze w^la^snie t^edy podr^o^zni omijaj^a "+
            "zalewany przez widoczny na wschodzie Pontar odcinek traktu. "+
            "Drzewa otaczaj^ace ^scie^zk^e s^a powykrzywiane, rachityczne. "+
            "Chyba tylko temu zawdzi^eczaj^a to, ^ze nie trafi^ly jeszcze "+
            "pod ostrza drwalskich topor^ow. Pokrzykiwania dobiegaj^ace z "+
            "widocznego niedaleko tartaku mieszaj^a si^e z s^lyszalnym od "+
            "czasu do czasu trzeszczeniem grubej warstwy lodu pokrywaj^acej "+
            "rzek^e.";
    }
    str +="\n";
    return str;
}
