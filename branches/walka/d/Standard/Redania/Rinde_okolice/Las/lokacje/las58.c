/* Autor: Avard
   Data : 06.10.06
   Opis : Tinardan */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit LAS_RINDE_STD;
inherit "/lib/drink_water";
void create_las() 
{
    set_short("Pag^orek nad skrajem rzeki");
    add_exit(LAS_RINDE_LOKACJE + "las59.c","w",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las66.c","se",0,10,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt24.c","e",0,8,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt25.c","n",0,8,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt26.c","nw",0,8,1);
    
	set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_prop(ROOM_I_INSIDE,0);
    add_event("@@pora_rok:"+file_name(TO)+"@@");

    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
}

void
init()
{
    ::init();
    init_drink_water(); 
} 

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Ziemia, wyd^eta niczym ^zagiel na wietrze, tworzy "+
            "niewielki pag^orek obro^sni^ety g^ladk^a traw^a. Drzewa "+
            "porastaj^a go z rzadka i s^a dosy^c cienkie, natomiast "+
            "pe^lno tu r^o^znego rodzaju krzaczk^ow, kwiat^ow, k^ep "+
            "trawy. Ze szczytu pag^orka wida^c po jednej stronie las i "+
            "ci^agn^acy si^e jego brzegiem trakt. Za nim zielenieje las, "+
            "g^esty i jeszcze dziki, cho^c co raz bardziej zadeptywany "+
            "przez podr^o^znych. Na po^ludniu natomiast, niczym wielka "+
            "b^l^ekitna wst^ega, przelewa swe wody pot^e^zny Pontar. Jego "+
            "powierzchnia, g^ladka, niemal aksamitna, daje wra^zenie "+
            "spokoju i ^lagodno^sci, ale tu^z pod ni^a nurt pr^e^zny i "+
            "silny, porywa wszystko ze sob^a. Wida^c to tak^ze po "+
            "brzegu, nier^ownym i oberwanym, gdy niegdy^s z^eby Pontaru "+
            "wbi^ly si^e w ziemi^e zabieraj^ac j^a wraz z nurtem.";
        }
        else
        {
            str = "Ziemia, wyd^eta niczym ^zagiel na wietrze, tworzy "+
            "niewielki pag^orek obsypany teraz grub^a ko^ldr^a ^sniegu. "+
            "Drzewa porastaj^a go z rzadka i s^a dosy^c cienkie, natomiast "+
            "pe^lno tu r^o^znego rodzaju krzaczk^ow wychylaj^acych swe "+
            "nagie ga^l^azki spod puchowej pokrywy. Ze szczytu pag^orka "+
            "wida^c po jednej stronie las i ci^agn^acy si^e jego brzegiem "+
            "trakt. Za nim bieleje i zieleni si^e las, g^esty i jeszcze "+
            "dziki, cho^c co raz bardziej zadeptywany przez podr^o^znych. "+
            "Na po^ludniu natomiast, niczym wielka granatowa wst^ega, "+
            "przelewa swe wody pot^e^zny Pontar. Jego powierzchnia, pokryta "+
            "bia^lymi strz^epami kry, pieni^aca si^e bystra, ^swiadczy o "+
            "sile, jak^a posiada ta rzeka. Tu^z pod lodem nurt pr^e^zny i "+
            "silny, porywa wszystko ze sob^a. Wida^c to tak^ze po brzegu, "+
            "nier^ownym i oberwanym, gdy niegdy^s z^eby Pontaru wbi^ly si^e "+
            "w ziemi^e zabieraj^ac j^a wraz z nurtem.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Ziemia, wyd^eta niczym ^zagiel na wietrze, tworzy "+
            "niewielki pag^orek obro^sni^ety g^ladk^a traw^a. Drzewa "+
            "porastaj^a go z rzadka i s^a dosy^c cienkie, natomiast "+
            "pe^lno tu r^o^znego rodzaju krzaczk^ow, kwiat^ow, k^ep "+
            "trawy. Ze szczytu pag^orka wida^c po jednej stronie las i "+
            "ci^agn^acy si^e jego brzegiem trakt. Za nim zielenieje "+
            "las, g^esty i jeszcze dziki, cho^c co raz bardziej zadeptywany "+
            "przez podr^o^znych. Na po^ludniu natomiast, niczym wielka "+
            "granatowa wst^ega, przelewa swe wody pot^e^zny Pontar. "+
            "Jego powierzchnia, g^ladka, niemal aksamitna, daje wra^zenie "+
            "spokoju i ^lagodno^sci, ale tu^z pod ni^a nurt pr^e^zny i "+
            "silny, porywa wszystko ze sob^a. Wida^c to tak^ze po "+
            "brzegu, nier^ownym i oberwanym, gdy niegdy^s z^eby Pontaru "+
            "wbi^ly si^e w ziemi^e zabieraj^ac j^a wraz z nurtem.";
        }
        else
        {
            str = "Ziemia, wyd^eta niczym ^zagiel na wietrze, tworzy "+
            "niewielki pag^orek obsypany teraz grub^a ko^ldr^a ^sniegu. "+
            "Drzewa porastaj^a go z rzadka i s^a dosy^c cienkie, natomiast "+
            "pe^lno tu r^o^znego rodzaju krzaczk^ow wychylaj^acych swe "+
            "nagie ga^l^azki spod puchowej pokrywy. Ze szczytu pag^orka "+
            "wida^c po jednej stronie las i ci^agn^acy si^e jego brzegiem "+
            "trakt. Za nim bieleje i zieleni si^e las, g^esty i jeszcze "+
            "dziki, cho^c co raz bardziej zadeptywany przez podr^o^znych. "+
            "Na po^ludniu natomiast, niczym wielka granatowa wst^ega, "+
            "przelewa swe wody pot^e^zny Pontar. Jego powierzchnia, pokryta "+
            "bia^lymi strz^epami kry, pieni^aca si^e bystra, ^swiadczy o "+
            "sile, jak^a posiada ta rzeka. Tu^z pod lodem nurt pr^e^zny i "+
            "silny, porywa wszystko ze sob^a. Wida^c to tak^ze po brzegu, "+
            "nier^ownym i oberwanym, gdy niegdy^s z^eby Pontaru wbi^ly si^e "+
            "w ziemi^e zabieraj^ac j^a wraz z nurtem.";
        }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
    str = "Ziemia, wyd^eta niczym ^zagiel na wietrze, tworzy niewielki "+
        "pag^orek obro^sni^ety g^ladk^a traw^a. Drzewa porastaj^a go z "+
        "rzadka i s^a dosy^c cienkie, natomiast pe^lno tu r^o^znego "+
        "rodzaju krzaczk^ow, kwiat^ow, k^ep trawy. Ze szczytu pag^orka "+
        "wida^c po jednej stronie las i ci^agn^acy si^e jego brzegiem "+
        "trakt. Za nim zielenieje las, g^esty i jeszcze dziki, cho^c co "+
        "raz bardziej zadeptywany przez podr^o^znych. Na po^ludniu "+
        "natomiast, niczym wielka granatowa wst^ega, przelewa swe wody "+
        "pot^e^zny Pontar. Jego powierzchnia, g^ladka, niemal aksamitna, "+
        "daje wra^zenie spokoju i ^lagodno^sci, ale tu^z pod ni^a nurt "+
        "pr^e^zny i silny, porywa wszystko ze sob^a. Wida^c to tak^ze po "+
        "brzegu, nier^ownym i oberwanym, gdy niegdy^s z^eby Pontaru wbi^ly "+
        "si^e w ziemi^e zabieraj^ac j^a wraz z nurtem.";
    }
    else
    {
    str = "Ziemia, wyd^eta niczym ^zagiel na wietrze, tworzy niewielki "+
        "pag^orek obsypany teraz grub^a ko^ldr^a ^sniegu. Drzewa "+
        "porastaj^a go z rzadka i s^a dosy^c cienkie, natomiast pe^lno "+
        "tu r^o^znego rodzaju krzaczk^ow wychylaj^acych swe nagie "+
        "ga^l^azki spod puchowej pokrywy. Ze szczytu pag^orka wida^c po "+
        "jednej stronie las i ci^agn^acy si^e jego brzegiem trakt. Za nim "+
        "bieleje i zieleni si^e las, g^esty i jeszcze dziki, cho^c co raz "+
        "bardziej zadeptywany przez podr^o^znych. Na po^ludniu natomiast, "+
        "niczym wielka granatowa wst^ega, przelewa swe wody pot^e^zny "+
        "Pontar. Jego powierzchnia, pokryta bia^lymi strz^epami kry, "+
        "pieni^aca si^e bystra, ^swiadczy o sile, jak^a posiada ta rzeka. "+
        "Tu^z pod lodem nurt pr^e^zny i silny, porywa wszystko ze sob^a. "+
        "Wida^c to tak^ze po brzegu, nier^ownym i oberwanym, gdy niegdy^s "+
        "z^eby Pontaru wbi^ly si^e w ziemi^e zabieraj^ac j^a wraz z nurtem.";
    }
    str += "\n";
    return str;
}
string
pora_rok()
{
    switch (pora_roku())
    {
        case MT_LATO:
        case MT_WIOSNA:
            return ({"Wa^zka przelecia^la tu^z ko^lo twojego nosa.\n",
            "W wodzie trzepocze przez chwil^e jaka^s ryba, po czym niknie w "+
            "g^l^ebinach.\n",
            "Fale Pontaru szumi^a cicho.\n",
            "Gdzie^s od strony traktu dochodz^a ci^e nawo^lywania.\n",
            "Powierzchnia rzeki burzy si^e, jakby co^s poruszy^lo si^e "+
            "gwa^ltownie w g^l^ebinach.\n"})
            [random(5)];
         case MT_JESIEN:
             return ({"Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
             "Gdzie^s od strony traktu dochodz^a ci^e nawo^lywania.\n",
             "Powierzchnia rzeki burzy si^e, jakby co^s poruszy^lo si^e "+
             "gwa^ltownie w g^l^ebinach.\n"})
             [random(3)];
         case MT_ZIMA:
             return ({"Fale Pontaru hucz^a gro^xnie.\n",
             "Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
             "Kra z trzaskiem od^lamuje si^e od brzegu i odp^lywa niesiona "+
             "silnym nurtem.\n","Gdzie^s od strony traktu dochodz^a ci^e "+
             "nawo^lywania.\n","Granatowe fale Pontaru podchodz^a wysoko "+
             "pod wystrz^epiony brzeg.\n","Pontar huczy gro^xnie.\n",
             "Powierzchnia rzeki burzy si^e, jakby co^s poruszy^lo si^e "+
             "gwa^ltownie w g^l^ebinach.\n"})
             [random(7)];
    }
}
