/* Autor: Avard
   Data : 19.03.07
   Opis : Yran */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("Brzozowy zagajnik");
    add_exit(LAS_RINDE_LOKACJE + "las28.c","n",0,6,0);
    //add_exit(LAS_RINDE_LOKACJE + "las50.c","sw",0,6,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt26.c","s",0,6,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt25.c","se",0,6,0);
    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{ 
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Znajdujesz si^e w niewielkim brzozowym zagajniku. M^lode "+
                "drzewa o snie^znobia^lych konarach maj^a spl^atane ze "+
                "sob^a nisko rosn^ace ga^l^azki, kt^ore mimo r^owninnego "+
                "terenu znacznie utrudniaj^a w^edr^owk^e. Wiej^acy z "+
                "zachodu lekki wiatr delikatnie ko^lysze czubkami "+
                "rosn^acych doko^la brz^oz. Z wysokiej, g^estej trawy "+
                "rosn^acej k^epami wystaj^a nastroszone uszka le^snych "+
                "zwierz^atek uwa^znie nas^luchuj^acych niebezpiecze^nstwa, "+
                "bowiem taki zagajnik jest znakomitym miejscem dla "+
                "czaj^acego si^e w g^estwinie drapie^znika czekaj^acego "+
                "jedynie na okazje by w dzikim szale rzuci^c sie w "+
                "krwio^zerczy po^scig za upatrzon^a ofiara. Przez smuk^le "+
                "pnie z trudem dostrzegasz na po^ludniu ospale p^lyn^ace "+
                "wody Pontaru.";
        }
        else
        {
            str = "Znajdujesz si^e w niewielkim brzozowym zagajniku. "+
                "M^lode, bezlistne drzewa o snie^znobia^lych konarach "+
                "maj^a spl^atane ze sob^a nisko rosn^ace ga^l^azki, kt^ore "+
                "mimo r^owninnego terenu znacznie utrudniaj^a w^edr^owk^e. "+
                "Wiej^acy z zachodu lekki wiatr delikatnie ko^lysze "+
                "czubkami rosn^acych doko^la brz^oz. Poprzez zalegaj^acy "+
                "na ziemi ^snieg zdo^la^ly przebi^c si^e nieliczne k^epy "+
                "traw, z kt^orych wystaj^a nastroszone uszka le^snych "+
                "zwierz^atek uwa^znie nas^luchuj^acych niebezpiecze^nstwa, "+
                "bowiem taki zagajnik jest znakomitym miejscem dla "+
                "czaj^acego si^e w g^estwinie drapie^znika czekaj^acego "+
                "jedynie na okazje by w dzikim szale rzuci^c sie w "+
                "krwio^zerczy po^scig za upatrzon^a ofiara. Przez smuk^le "+
                "pnie z trudem dostrzegasz na po^ludniu ospale p^lyn^ace "+
                "wody Pontaru oraz dryfuj^ac^a na rzece bezw^ladnie kr^e "+
                "obijaj^ac^a si^e o wystaj^a^ce ponad tafle wody kamienie.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Znajdujesz si^e w niewielkim brzozowym zagajniku. M^lode "+
                "drzewa o snie^znobia^lych konarach maj^a spl^atane ze "+
                "sob^a nisko rosn^ace ga^l^azki, kt^ore mimo r^owninnego "+
                "terenu znacznie utrudniaj^a w^edr^owk^e. Wiej^acy z "+
                "zachodu lekki wiatr delikatnie ko^lysze czubkami "+
                "rosn^acych doko^la brz^oz. Z wysokiej, g^estej trawy "+
                "rosn^acej k^epami wystaj^a nastroszone uszka le^snych "+
                "zwierz^atek uwa^znie nas^luchuj^acych niebezpiecze^nstwa, "+
                "bowiem taki zagajnik jest znakomitym miejscem dla "+
                "czaj^acego si^e w g^estwinie drapie^znika czekaj^acego "+
                "jedynie na okazje by w dzikim szale rzuci^c sie w "+
                "krwio^zerczy po^scig za upatrzon^a ofiara. Przez smuk^le "+
                "pnie z trudem dostrzegasz na po^ludniu ospale p^lyn^ace "+
                "wody Pontaru.";
        }
        else
        {
            str = "Znajdujesz si^e w niewielkim brzozowym zagajniku. "+
                "M^lode, bezlistne drzewa o snie^znobia^lych konarach "+
                "maj^a spl^atane ze sob^a nisko rosn^ace ga^l^azki, kt^ore "+
                "mimo r^owninnego terenu znacznie utrudniaj^a w^edr^owk^e. "+
                "Wiej^acy z zachodu lekki wiatr delikatnie ko^lysze "+
                "czubkami rosn^acych doko^la brz^oz. Poprzez zalegaj^acy "+
                "na ziemi ^snieg zdo^la^ly przebi^c si^e nieliczne k^epy "+
                "traw, z kt^orych wystaj^a nastroszone uszka le^snych "+
                "zwierz^atek uwa^znie nas^luchuj^acych niebezpiecze^nstwa, "+
                "bowiem taki zagajnik jest znakomitym miejscem dla "+
                "czaj^acego si^e w g^estwinie drapie^znika czekaj^acego "+
                "jedynie na okazje by w dzikim szale rzuci^c sie w "+
                "krwio^zerczy po^scig za upatrzon^a ofiara. Przez smuk^le "+
                "pnie z trudem dostrzegasz na po^ludniu ospale p^lyn^ace "+
                "wody Pontaru oraz dryfuj^ac^a na rzece bezw^ladnie kr^e "+
                "obijaj^ac^a si^e o wystaj^a^ce ponad tafle wody kamienie.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Znajdujesz si^e w niewielkim brzozowym zagajniku. M^lode "+
                "drzewa o snie^znobia^lych konarach maj^a spl^atane ze "+
                "sob^a nisko rosn^ace ga^l^azki, kt^ore mimo r^owninnego "+
                "terenu znacznie utrudniaj^a w^edr^owk^e. Wiej^acy z "+
                "zachodu lekki wiatr delikatnie ko^lysze czubkami "+
                "rosn^acych doko^la brz^oz. Z wysokiej, g^estej trawy "+
                "rosn^acej k^epami wystaj^a nastroszone uszka le^snych "+
                "zwierz^atek uwa^znie nas^luchuj^acych niebezpiecze^nstwa, "+
                "bowiem taki zagajnik jest znakomitym miejscem dla "+
                "czaj^acego si^e w g^estwinie drapie^znika czekaj^acego "+
                "jedynie na okazje by w dzikim szale rzuci^c sie w "+
                "krwio^zerczy po^scig za upatrzon^a ofiara. Przez smuk^le "+
                "pnie z trudem dostrzegasz na po^ludniu ospale p^lyn^ace "+
                "wody Pontaru.";
    }
    else
    {
        str = "Znajdujesz si^e w niewielkim brzozowym zagajniku. "+
                "M^lode, bezlistne drzewa o snie^znobia^lych konarach "+
                "maj^a spl^atane ze sob^a nisko rosn^ace ga^l^azki, kt^ore "+
                "mimo r^owninnego terenu znacznie utrudniaj^a w^edr^owk^e. "+
                "Wiej^acy z zachodu lekki wiatr delikatnie ko^lysze "+
                "czubkami rosn^acych doko^la brz^oz. Poprzez zalegaj^acy "+
                "na ziemi ^snieg zdo^la^ly przebi^c si^e nieliczne k^epy "+
                "traw, z kt^orych wystaj^a nastroszone uszka le^snych "+
                "zwierz^atek uwa^znie nas^luchuj^acych niebezpiecze^nstwa, "+
                "bowiem taki zagajnik jest znakomitym miejscem dla "+
                "czaj^acego si^e w g^estwinie drapie^znika czekaj^acego "+
                "jedynie na okazje by w dzikim szale rzuci^c sie w "+
                "krwio^zerczy po^scig za upatrzon^a ofiara. Przez smuk^le "+
                "pnie z trudem dostrzegasz na po^ludniu ospale p^lyn^ace "+
                "wody Pontaru oraz dryfuj^ac^a na rzece bezw^ladnie kr^e "+
                "obijaj^ac^a si^e o wystaj^a^ce ponad tafle wody kamienie.";
    }
    str += "\n";
    return str;
}

