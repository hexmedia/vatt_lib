/* Autor: Daeru
   Data : 7.11.06
   Opis : Bajoro 
   Info : Opis jest do poprawki */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("Li^sciasty las");
    add_exit(LAS_RINDE_LOKACJE + "las56.c","se",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las35.c","nw",0,10,1); 

    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str; 
    if(jest_dzien() == 1) // Czyli jesli na lokacji jest dzien
    {
        if(pora_roku() != MT_ZIMA) // Jesli na lokacji nie ma zimy
        {
            str = "Stoisz po^srodku brzozowego gaju, kt^orego korony drzew, "+
            "skrywaj^a niebo nad tob^a. Panuje tu tajemniczy p^o^lmrok  "+
            "kt^ory dodaje tajemniczo^sci temu miejscu. Wok^o^l panuje "+
            "pozorny spok^oj zak^l^ocany co jaki^s czas skrzypieniem starych "+
            "konar^ow oraz woz^ow pd^a^zaj^acych pobliskim traktem "+
            "w kierunku Rinde. Charakterystycznym elementem tego krajobrazu, "+
            "jest p^laski, wapienny g^laz, ca^lkowicie niepasuj^acy do tego "+
            "otoczenia.Panuj^acy tu spok^oj i cisza  sprawiaj^a ^ze "+ 
            "jest to dobre miejsce na odpoczynek po trudach "+
            "podr^o^zy. A^z dziw bierze i^z mimo tego ^ze miejsce to le^zy "+
            "blisko ucz^eszczanego traktu, nie nosi ^zadnych slad^ow "+ 
            "cywilizacji. Runo le^sne us^lane jest krzaczkami borowiny "+
            "faluj^acej na wietrze niczym wielki dywan. Wok^ol wyczuwa si^e "+
            "charakterystyczny zapach gnij^acyh li^sci. Za^s w g^orze "+
            "s^lycha^c ^spiew ptak^ow ";
        }
        if(pora_roku() == MT_ZIMA) // Czyli jesli na lokacji jest zima
        {
            str ="Stoisz po^srodku brzozowego gaju, kt^orego korony drzew, "+
            "skrywaj^a niebo nad tob^a. Panuje tu tajemniczy p^o^lmrok  "+
            "kt^ory dodaje tajemniczo^sci temu miejscu. Wok^o^l panuje "+
            "pozorny spok^oj zak^l^ocany co jaki^s czas skrzypieniem starych "+
            "konar^ow oraz woz^ow pd^a^zaj^acych pobliskim traktem "+
            "w kierunku Rinde. Charakterystycznym elementem tego krajobrazu, "+
            "jest p^laski, wapienny g^laz, ca^lkowicie niepasuj^acy do tego "+
            "otoczenia.Panuj^acy tu spok^oj i cisza  sprawiaj^a ^ze "+ 
            "jest to dobre miejsce na odpoczynek po trudach "+
            "podr^o^zy . A^z dziw bierze i^z mimo tego ^ze miejsce to le^zy "+
            "blisko ucz^eszczanego traktu, nie nosi ^zadnych slad^ow "+ 
            "cywilizacji. Runo le^sne w ca^lo^sci przykryte jest grub^a "+
            "warstw^a puszystego ^sniegu kt^ory musia^l spa^s^c ca^lkiem "+
            "niedawno. Jedynym ^swiadectwem ^zycia w tej g^luszy, s^a "+
            "r^o^znego rodzaju ^slady na ^sniegu, zapewne pozostawione, "+
            "przez stworzenia zamieszkuj^ace te le^sne ost^epy. Oraz "+
            "krakanie okupuj^acyh pobliskie ga^l^ezie, gawron^ow. Nie "+
            "odczuwasz tutaj ch^lodu, pomimo tego ze koronami drzew "+
            "targa mro^xny, wschodni wiatr.";
        }
    }
    if(jest_dzien() == 0) // Jesli na lokacji jest noc 
                          // i nie ma innego zrodla swiatla
    {
        if(pora_roku() != MT_ZIMA) // Jesli nie ma zimy, ale teraz pod
                                  // warunkiem nocy, czyli noc, nie zima
        {
             str = "Stoisz po^srodku brzozowego gaju, kt^orego korony "+
             "drzew, skrywaj^a niebo nad tob^a. Panuje tu tajemniczy "+
             "p^o^lmrok kt^ory dodaje tajemniczo^sci temu miejscu. "+
             "Wok^o^l panuje pozorny spok^oj zak^l^ocany co jakis czas "+
             "skrzypieniem starych konar^ow oraz odg^losami nocnych "+
             "zwierz^at budzacych si^e do ^zycia. Charakterystycznym "+
             "elementem tego krajobrazu, jest p^laski, wapienny g^laz, "+
             "ca^lkowicie niepasuj^acy do tego otoczenia. Jest to dobre "+
             "miejsce na odpoczynek po trudach podr^ozy. A^z dziw bierze i^z "+
             "mimo tego ^ze miejsce to le^zy blisko ucz^eszczanego traktu, "+
             "nie nosi ^zadnych slad^ow cywilizacji. Ca^l^a okolice skrywa "+
             "ca^lun nocy. Wygl^adaj^acy raz po raz, zza chmur, ksi^e^zyc, "+
             "pozwala ci dostrzec jedynie zarysy pobliskich drzew. ";
        } 
        else // przeciwienstwo rownorzednego warunku, czyli jesli jest zima
        {
            str= "Stoisz po^srodku brzozowego gaju, kt^orego korony drzew, "+
            "skrywaj^a niebo nad tob^a. Panuje tu tajemniczy p^o^lmrok  "+
            "kt^ory dodaje tajemniczo^sci temu miejscu. Wok^o^l panuje "+
            "pozorny spok^oj zak^l^ocany co jaki^s czas skrzypieniem starych "+
            "konar^ow uginajacych si^e pod ci^e^zarem ^sniegu."+
            "Charakterystycznym elementem tego krajobrazu, "+
            "jest p^laski, wapienny g^laz, ca^lkowicie niepasuj^acy do tego "+
            "otoczenia. Jest to dobre miejsce na odpoczynek po trudach "+
            "podr^ozy. A^z dziw bierze i^z mimo tego ^ze miejsce to le^zy "+
            "blisko ucz^eszczanego traktu, nie nosi ^zadnych slad^ow "+ 
            "cywilizacji. Wok^o^l panuje mrok kt^orego za^s urozmaiceniem, "+
            "jest biel le^z^acego wok^o^l ^sniegu. "; 
        }
    }
      str += "\n";
      return str;
}

string
opis_nocy() // Czyli jesli na lokacji jest noc i nie ma sztucznego zrodla 
            // swiatla
{
    string str;
    if(pora_roku() != MT_ZIMA) 
    {
        str = "Stoisz po^srodku brzozowego gaju, kt^orego korony drzew, "+
            "skrywaj^a niebo nad tob^a. Panuje tu tajemniczy p^o^lmrok  "+
            "kt^ory dodaje tajemniczo^sci temu miejscu. Wok^o^l panuje "+
            "pozorny spok^oj zak^l^ocany co jakis czas skrzypieniem starych "+
            "konar^ow. Charakterystycznym elementem tego "+
            "krajobrazu, jest p^laski, wapienny g^laz, ca^lkowicie "+
            "niepasuj^acy do tego otoczenia. Jest to dobre miejsce na "+
            "odpoczynek po trudach podr^ozy. A^z dziw bierze iz mimo tego "+
            "^ze miejsce to le^zy blisko ucz^eszczanego traktu, nie nosi "+
            "^zadnych slad^ow cywilizacji. Runo le^sne us^lane jest "+
            "krzaczkami borowiny faluj^acej na wietrze niczym wielki "+
            "dywan. Wok^o^l wyczuwa si^e charakterystyczny zapach gnij^acyh "+
            "li^sci. Wszystko powoli zaczyna ogarnia^c p^o^lmrok. Trel "+
            "ptak^ow z kazd^a minut^a cichnie coraz bardziej. Las powoli "+
            "uk^lada si^e do snu. ";
    } 
    else
    {
        str = "Stoisz po^srodku brzozowego gaju, kt^orego korony drzew, "+
            "skrywaj^a niebo nad tob^a. Panuje tu tajemniczy p^o^lmrok  "+
            "kt^ory dodaje tajemniczo^sci temu miejscu. Wok^o^l panuje "+
            "pozorny spok^oj zak^l^ocany co jakis czas skrzypieniem starych "+
            "konar^ow oraz. Charakterystycznym elementem tego krajobrazu, "+
            "jest p^laski, wapienny g^laz, ca^lkowicie niepasuj^acy do tego "+
            "otoczenia. Jest to dobre miejsce na odpoczynek po trudach "+
            "podr^ozy. Az dziw bierze iz mimo tego ^ze miejsce to le^zy "+
            "blisko ucz^eszczanego traktu, nie nosi ^zadnych slad^ow "+ 
            "cywilizacji. Runo le^sne w ca^lo^sci przykryte jest grub^a "+
            "warstw^a puszystego ^sniegu ktory musia^l spa^s^c ca^lkiem "+
            "niedawno. Las powoli niknie w nocnej szaro^sci. Ucia^zliwe, "+
            "jak do tej pory, krakanie gawron^ow s^labnie z ka^zd^a minut^a, "+
            "u^swiadamiaj^ac ci^e o zapadaj^acej nocy. ";
    }
    str += "\n";
    return str;
}