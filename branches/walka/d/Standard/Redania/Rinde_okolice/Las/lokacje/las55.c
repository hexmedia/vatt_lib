/* Autor: Avard
   Data : 30.11.06
   Opis : Yran & Valor*/
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
inherit LAS_RINDE_STD;
void create_las() 
{
    set_short("W^sr^od drzew");
    add_exit(LAS_RINDE_LOKACJE + "las44.c","n",0,10,1);
    add_exit(LAS_RINDE_LOKACJE + "las65.c","sw",0,10,1);
    add_npc(LAS_RINDE_LIVINGI+"wiewiorka");
    
    add_prop(ROOM_I_INSIDE,0);
}
string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Liczne gatunki drzew nieustannie walcz^a o ^zyciodajne "+
              "promienie s^loneczne. Smuk^le brzozy pn^a swe cienkie, "+
              "^snie^znobia^le konary ku g^orze wystawiaj^ac zielone listki "+
              "do s^lo^nca, jednak skutecznie utradniaj^a im to wysokie, "+
              "wynios^le d^eby zbieraj^ac swoimi roz^lo^zystymi "+
              "li^sciastymi ga^l^eziami wi^ekszo^s^c ^swiat^la. Ogromne "+
              "korony drzew zas^laniaj^a niemal ca^le niebo przez co panuje "+
              "tutaj p^o^lmrok. Pod^lo^ze pokrywa mi^eciutki mech i "+
              "le^z^ace gdzieniegdzie ma^le ga^l^azki, kt^ore pospada^ly z "+
              "drzew podczas silniejszych podmuch^ow wiatru. ";
        }
        else
        {
            str = "Liczne gatunki drzew pogr^a^zone w ^snie zimowym "+
              "bezwiednie ko^lysz^a si^e przy ka^zdym, nawet najmniejszym "+
 	          "podmuchu wiatru. Smuk^le brzozy przeplataj^a swe cienkie "+
              "ga^l^azki z ogromnymi koronami d^eb^ow, tworz^ac paj^eczyn^e "+
              "suchych konar^ow zas^laniaj^ac niemal ca^le niebo, tylko "+
              "pojedyncze promienie ^swiat^la docieraj^a do ziemi "+
              "pozostawiaj^ac okolic^e skryt^a w p^o^lmroku. Pod^lo^ze "+
              "pokrywa bia^ly puch i le^z^ace gdzieniegdzie ma^le "+
              "ga^l^azki, kt^ore pospada^ly z drzew podczas silniejszych "+
              "podmuch^ow wiatru.";
       }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Liczne gatunki drzew nieustannie walcz^a o ^zyciodajne "+
              "promienie s^loneczne. Smuk^le brzozy pn^a swe cienkie, "+
              "^snie^znobia^le pnie ku g^orze wystawiaj^ac zielone listki "+
              "ku s^lo^ncu, jednak skutecznie utradniaj^a im to wysokie, "+
              "wynios^le d^eby zbieraj^ac swoimi roz^lo^zystymi "+
              "li^sciastymi ga^l^eziami wi^ekszo^s^c ^swiat^la. Ogromne "+
              "korony drzew zas^laniaj^a niemal ca^le niebo przez co panuje "+
              "tutaj p^o^lmrok. Pod^lo^ze pokrywa mi^eciutki mech i "+
              "le^z^ace gdzieniegdzie ma^le ga^l^azki, kt^ore pospada^ly z "+
              "drzew podczas silniejszych podmuch^ow wiatru. ";
        }
        else
        {
            str = "Liczne gatunki drzew pogr^a^zone w ^snie zimowym "+
              "bezwiednie ko^lysz^a si^e przy ka^zdym, nawet najmniejszym "+
              "podmuchu wiatru. Smuk^le brzozy przeplataj^a swe cienkie "+
              "ga^l^azki z ogromnymi koronami d^eb^ow, tworz^ac paj^eczyn^e "+
              "suchych konar^ow zas^laniaj^ac niemal ca^le niebo, tylko "+
              "pojedyncze promienie ^swiat^la docieraj^a do ziemi "+
              "pozostawiaj^ac okolic^e skryt^a w p^o^lmroku. Pod^lo^ze "+
              "pokrywa bia^ly puch i le^z^ace gdzieniegdzie ma^le "+
              "ga^l^azki, kt^ore pospada^ly z drzew podczas silniejszych "+
              "podmuch^ow wiatru.";
       }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Liczne gatunki drzew nieustannie walcz^a o ^zyciodajne "+
              "promienie s^loneczne. Smuk^le brzozy pn^a swe cienkie, "+
              "^snie^znobia^le konary ku g^orze wystawiaj^ac zielone listki "+
              "do s^lo^nca, jednak skutecznie utradniaj^a im to wysokie, "+
              "wynios^le d^eby zbieraj^ac swoimi roz^lo^zystymi "+
              "li^sciastymi ga^l^eziami wi^ekszo^s^c ^swiat^la. Ogromne "+
              "korony drzew zas^laniaj^a niemal ca^le niebo przez co panuje "+
              "tutaj p^o^lmrok. Pod^lo^ze pokrywa mi^eciutki mech i "+
              "le^z^ace gdzieniegdzie ma^le ga^l^azki, kt^ore pospada^ly z "+
              "drzew podczas silniejszych podmuch^ow wiatru.";
        }
        else
        {
            str = "Liczne gatunki drzew pogr^a^zone w ^snie zimowym "+
              "bezwiednie ko^lysz^a si^e przy ka^zdym, nawet najmniejszym "+
              "podmuchu wiatru. Smuk^le brzozy przeplataj^a swe cienkie "+
              "ga^l^azki z ogromnymi koronami d^eb^ow, tworz^ac paj^eczyn^e "+
              "suchych konar^ow zas^laniaj^ac niemal ca^le niebo, tylko "+
              "pojedyncze promienie ^swiat^la docieraj^a do ziemi "+
              "pozostawiaj^ac okolic^e skryt^a w p^o^lmroku. Pod^lo^ze "+
              "pokrywa bia^ly puch i le^z^ace gdzieniegdzie ma^le "+
              "ga^l^azki, kt^ore pospada^ly z drzew podczas silniejszych "+
              "podmuch^ow wiatru.";
       }
    str += "\n";
    return str;
}
