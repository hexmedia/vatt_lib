#include "../dir.h"
#define LAS_RINDE           (REDANIA + "Rinde_okolice/Las/")
#define LAKA_RINDE          (REDANIA + "Rinde_okolice/Laka/")

#define LAS_RINDE_STD       (LAS_RINDE + "std/las.c")
#define WLAS_RINDE_STD      (LAS_RINDE + "std/wlas.c")
#define TRAKT_RINDE_STD     (TRAKT_RINDE +"std/trakt.c")
#define LAKA_RINDE_STD      (LAKA_RINDE +"std/laka.c")

#define LAS_RINDE_LIVINGI   (LAS_RINDE + "livingi/")
#define LAKA_RINDE_LIVINGI  (LAKA_RINDE + "livingi/")

#define LAS_RINDE_OBIEKTY   (LAS_RINDE + "obiekty/")
#define TRAKT_RINDE_OBIEKTY (TRAKT_RINDE + "obiekty/")

#define LAS_RINDE_LOKACJE   (LAS_RINDE + "lokacje/")
#define LAKA_RINDE_LOKACJE  (LAKA_RINDE + "lokacje/")

#define TRAKT_RINDE_DRZWI   (TRAKT_RINDE + "drzwi/")

#define DWOREK              (LAS_RINDE + "Dworek/")
#define DWOREK_LOKACJE      (DWOREK + "lokacje/")
#define DWOREK_DRZWI        (DWOREK + "drzwi/")
#define DWOREK_OBIEKTY      (DWOREK + "obiekty/")

#define OK_RINDE_PRZEDMIOTY (REDANIA + "Rinde_okolice/przedmioty/")
