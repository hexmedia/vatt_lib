inherit "/std/creature";
inherit "/std/combat/unarmed";
inherit "/std/act/attack";
inherit "/std/act/action";
inherit "/std/act/ask_gen";
inherit "/std/act/domove";
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

void random_przym(string str, int n);

int
czy_atakowac()
{
    if(TP->query_humanoid() && TP->query_rasa() != "utopiec")
        return 1;
    else
        return 0;
}


void
create_creature()
{
    ustaw_odmiane_rasy("utopiec");
    set_gender(G_MALE);
    random_przym("gnij^acy:gnij^ace zgni^ly:zgnili "+
        "^smierdz^acy:^smierdz^ace paskudny:paskudni "+
        "zakrwawiony:zakrwawieni || "+
        "agresywny:agresywni nerwowy:nerwowi wysoki:wysocy "+
        "chudy:chudzi wychudzony:wychudzeni",2);

    set_long("Humanoidalna sylwetka i cia^lo nap^ecznia^le od wody "+
        "wskazuje na to ^ze przygl^adasz si^e utopcowi. Poziom "+
        "zgnicia tkanki i deformacja cia^la prawie uniemo^zliwiaj^a "+
        "rozpoznanie w nim cz^lowieka, kt^orym by^l w przesz^lo^sci. "+
        "D^lugie r^ece zako^nczone brudnymi i ostrymi pazurami, ostre "+
        "zaostrzone z^eby oraz bezmy^slny i krwio^zerczy wzrok, mog^a "+
        "nap^edzi^c strachu niejednemu wojownikowi. Chude cia^lo "+
        "przys^loni^ete jest gdzieniegdzie resztkami przegni^lego "+
        "ubrania, kt^ore nie wiadomo jakim cudem utrzymuje si^e na "+
        "w^la^scicielu. \n");

    set_stats (({40+random(30),30+random(20),75+random(50),30+random(10),
				30+random(10),100+random(10)}));
    add_prop(CONT_I_WEIGHT, 65000+random(15000));
    add_prop(CONT_I_HEIGHT, 150+random(30));
    add_prop(LIVE_I_NEVERKNOWN, 1);
    set_aggressive(&czy_atakowac());
    set_attack_chance(91+random(10));
    set_npc_react(1);

   /* set_cact_time(10);
    add_cact("sapnij cicho");
    add_cact("emote bulgocze przeci^agle.");*/
	set_act_time(20+random(4));
	add_act("emote rozgl�da sie po okolicy.");

	set_default_answer(VBFC_ME("default_answer"));

	set_skill(SS_2H_COMBAT,60+random(24));
	set_skill(SS_SWIM,80);
    set_skill(SS_UNARM_COMBAT, 50+random(14));
    set_skill(SS_PARRY, 66);
    set_skill(SS_DEFENCE,44);
    set_skill(SS_AWARENESS,64+random(10));
    set_skill(SS_BLIND_COMBAT,30+random(50));
	set_skill(SS_HIDE,30+random(40));
	set_skill(SS_SNEAK,30+random(30));

	set_alarm_every_hour("ukryj");

    set_attack_unarmed(0, 20, 22, W_SLASH,  50, "pazurami prawej d^loni");
    set_attack_unarmed(1, 20, 22, W_SLASH,  50, "pazurami lewej d^loni");
    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "g^low^e");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "lew^a r^eke");
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "praw^a r^eke");
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 20, "nogi");

    add_prop(LIVE_I_SEE_DARK, 1);

	set_random_move(10);
	set_restrain_path(JASKINIA_LOKACJE);
}

int
ukryj()
{
	if(random(2))
		command("ukryj sie");
	return 1;
}

string default_answer()
{
        command("emote bulgocze.");
        return "";
}

void random_przym(string str, int n)
{
        mixed* groups = ({ });
        groups = explode(str, "||");
        int groups_size = sizeof(groups);
        if(n > groups_size)
                return;
        for(int i =0; i < groups_size; i++)
                groups[i] = explode(groups[i], " ");

        for(int a = 0; a < n; a++)
        {
                int group_index = random(sizeof(groups));
                int adj_index = random(sizeof(groups[group_index]));
                string* tmp = explode(groups[group_index][adj_index], ":");
                dodaj_przym(tmp[0], tmp[1]);
                groups = groups-({groups[group_index]});
        }
}
