inherit "/std/room";
#include <stdproperties.h>
#include <macros.h>
#include "dir.h"

int
query_jaskinia_zakon_okolice()
{
	return 2;
}

int
check(string dokad, string skad, int odwroc = 0)
{
	object last;
    if(TP->query_leader()) //w teamie robimy inaczej
    {
		//jesli leader prowadzi:
		if(file_name(ENV(TP->query_leader())) == JASKINIA_LOKACJE+"jaskinia_"+dokad)
			last = (TP->query_leader())->query_prop(LIVE_O_LAST_LAST_ROOM);
		else
			last = TP->query_prop(LIVE_O_LAST_ROOM);

    }
    else
    {
		last = TP->query_prop(LIVE_O_LAST_ROOM);
	}
	if(!objectp(last))
		last = TO; //???
	/*
write("sk�d "+ skad+" dok�d: "+dokad);
	string gdzie = explode(file_name(ENV(TP)),"_")[sizeof(explode(file_name(ENV(TP)),"_"))-1];
write(" gdzie: "+gdzie+"\n");
	//zawa�y gruzu
	foreach(string *tmptab : query_zawaly())
	{
//		dump_array(tmptab);
		if(member_array(gdzie,tmptab) != -1)
			if(member_array(dokad,tmptab) != -1)
			{
				write("Pot�ny zawa� gruzu i innych odpadk�w toruj�cych drog� "+
						"uniemo�liwia ci pod��anie w tym kierunku.\n");
				return 1;
			}
	}
*/
	if(odwroc)
	{
		string *zakazane = ({JASKINIA_LOKACJE+"jaskinia_02"});
		if(member_array(file_name(last),zakazane) == -1)
			return 0;
		else
			return 2;
	}
	else
	{
		if(file_name(last) == JASKINIA_LOKACJE+"jaskinia_"+skad)
			return 0;
		else
			return 2;
	}
}


