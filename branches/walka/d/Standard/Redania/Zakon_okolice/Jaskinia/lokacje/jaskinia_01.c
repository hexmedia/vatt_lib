//inherit "/std/room";
#include "dir.h"
inherit JASKINIA_U_STD;
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <pl.h>
#include <object_types.h>
string dlugi_opis();

void
create_room()
{
    set_short("Zalane wej^scie");
    set_long("@@dlugi_opis@@");

    add_exit("jaskinia_02.c", ({"prz�d","do przodu","z jednego z korytarzy"}),&check("02","xxx",1),
		    			J_FA,&check("02","xxx",1));
    add_exit("jaskinia_02.c",({"ty�","do ty�u","z jednego z korytarzy"}),&check("02","02"),
		    			J_FA,&check("02","02"));

    add_item("otw^or",
        "Ciemna dziura w suficie jest do^s^c w^aska, lecz "+
		"przy sporym wysi^lku by^lby^s najprawdopodobniej "+
        "w stanie si^e przez ni^a przecisn^a^c. Zdaje si^e, "+
		"^ze z jej wn^etrza dolatuj^a powiewy ^swie^zego "+
                "powietrza .\n");
    add_item("^sciany",
                "^Sciany wy^z^lobione przez wod^e pokryte s^a dziwn^a "+
                "ro^slinno^sci^a przypominaj^aca glony. Niemi^le "+
                "i o^slizg^le w dotyku dodatkowo sp^lywaj^a wod^a "+
                "kapi^ac^a z sufitu .\n");

    add_item("szcz^atki",
                "Szcz^atki unosz^ace si^e na wodzie s^a bardzo trudne do "+
                "rozpoznania, mo^zliwe ^ze s^a to jakie^s kawa^lki ubrania "+
                "i drewna.\n");


        set_event_time(250.0+itof(random(100)));
    add_event("Powiew ^swie^zego powietrza dochodzi ci^e od "+
			"strony stropu.\n");
    add_event("Jaki^s obrzydliwy zapach zdaje si^e dobiega^c z g^l^ebi "+
			"jaskini.\n");
    add_event("Kilka kropel wody spad^lo z sufitu. \n");
    add_event("Powierzchnia wody zafalowa^la lekko. \n");

    add_prop(ROOM_I_INSIDE,1);
    remove_prop(ROOM_I_LIGHT);
    add_prop(ROOM_S_DARK_LONG,"W ciemnym korytarzu. \n");

}

string
dlugi_opis()
{
    string str;

    str="Naturalna jaskinia wytworzona w wapiennej skale, okazuje si^e "+
        "ci^agn^a^c dalej w jednym tylko kierunku. Zalana do po^lowy wod^a na "+
        "mniej wi^ecej trzy stopy zdaje si^e by^c w tym miejscu "+
		"jedynie korytarzem. "+
        "Do^s^c szeroka by min^e^ly si^e w niej trzy osoby, a "+
		"wysoka na pi^e^c ^lokci "+
        "pozwala na do^s^c du^z^a swobod^e ruch^ow. Woda zalewaj^aca "+
		"korytarz jest "+
        "brudna i mulista, a po powierzchni p^lywaj^a jakie^s "+
		"szcz^atki, kt^orych "+
        "nie mo^zesz rozpozna^c. ^Sciany pokryte ^sluzowatymi "+
		"ro^slinami s^a niemi^le "+
        "w dotyku. W suficie mo^zesz dostrzec jaki^s ciemny otw^or. \n";
    return str;
}

void
wyjdz(object player)
{
        player->move("/d/Aretuza/faeve/workroom.c");
        player->do_glance(2);
        saybb(QCIMIE(TP,PL_MIA)+" pojawia si^e spo^sr^od trzciny. \n");
        return;
}

int
wespnij(string str)
{
	if(query_verb() ~= "przeci^snij")
    {
        if(query_verb() == "przeci^snij")
            notify_fail("Przeci^snij sie przez co?\n");
        else
            notify_fail("Wejd^x gdzie?\n");

        if(str ~= "si^e przez otw^or" || str~="si^e przez dziur^e" ||
           str ~="si^e przez otw^or na g^or^e" || 
			str ~="si^e przez dziur^e na g^or^e" ||
        	str ~= "sie na g^or^e")
        {
            object paraliz=clone_object("/std/paralyze");
            paraliz->set_fail_message("");
            paraliz->set_finish_object(this_object());
            paraliz->set_finish_fun("wyjdz");
            paraliz->set_remove_time(4);
            paraliz->setuid();
            paraliz->seteuid(getuid());
            paraliz->move(TP);
            write("Zaczynasz przeciska^c si^e przez otw^or.\n");
            saybb(QCIMIE(TP,PL_MIA)+" Zaczyna przeciska^c si^e przez otw^or.\n");
            return 1;
       }
    }
    else if(query_verb() ~="przejd^x")
    {
        notify_fail("Przejd^x przez co?\n");

        if(str ~= "przez otw^or" || str ~= "przez dziur^e")
        {
            object paraliz=clone_object("/std/paralyze");
            paraliz->set_fail_message("");
            paraliz->set_finish_object(this_object());
            paraliz->set_finish_fun("wyjdz");
            paraliz->set_remove_time(4);
            paraliz->setuid();
            paraliz->seteuid(getuid());
            paraliz->move(TP);
            write("Zaczynasz przeciska^c si^e przez otw^or.\n");
            saybb(QCIMIE(TP,PL_MIA)+" Zaczyna przeciska^c si^e przez otw^or.\n");
            return 1;
        }
    }

    return 0;
}

void
init()
{
        ::init();
        add_action(wespnij,"przeci^snij");
        add_action(wespnij,"przejd^x");
}
