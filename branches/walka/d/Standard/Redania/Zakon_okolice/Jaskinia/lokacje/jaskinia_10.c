//inherit "/std/room";
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit JASKINIA_U_STD;


void
create_room()
{
    set_short("Du^za grota");
    add_prop(ROOM_I_INSIDE,1);
    //add_exit(JASKINIA_LOKACJE + "jaskinia_02.c","n",0,1,0);
	add_exit(JASKINIA_LOKACJE+"jaskinia_06.c",({"ty�","do ty�u",
		   "z jednego z korytarzy" 	}), &check("06","06"),J_FA,&check("06","06"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_06.c",({"lewo","w lewo",
		   "z jednego z korytarzy" 	}), &check("06","09"),J_FA,&check("06","09"));  

	add_exit(JASKINIA_LOKACJE+"jaskinia_09.c",({"ty�","do ty�u",
		   "z jednego z korytarzy" 	}), &check("09","09"),J_FA,&check("09","09"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_09.c",({"prawo","w prawo",
		   "z jednego z korytarzy" 	}), &check("09","06"),J_FA,&check("09","06"));  


    add_prop(ROOM_I_LIGHT, 0);
    add_item("^sciany",
                "^Sciany wy^z^lobione przez wod^e pokryte s^a dziwn^a "+
                "ro^slinno^sci^a przypominaj^aca glony. Niemi^le "+
                "i o^slizg^le w dotyku dodatkowo sp^lywaj^a wod^a "+
                "kapi^ac^a z sufitu .\n");

    add_item("szcz^atki",
                "Szcz^atki unosz^ace si^e na wodzie s^a bardzo trudne do "+
                "rozpoznania, mo^zliwe ^ze s^a to jakie^s kawa^lki ubrania "+
                "i drewna.\n");


        set_event_time(250.0+itof(random(100)));
    add_event("Jaki^s obrzydliwy zapach zdaje si^e dobiega^c z g^l^ebi "+
			"jaskini.\n");
    add_event("Kilka kropel wody spad^lo z sufitu. \n");
    add_event("Powierzchnia wody zafalowa^la lekko. \n");

}

string
dlugi_opis()
{
    string str;
    str = "Korytarz ko^nczy si^e w tym miejscu ^scian^a "+
        "pokryt^a jakimi^s glonami. Woda zalewaj^aca przej^scie "+
        "jest brudna i mulista, a po powierzchni p�ywaj^a jakie^s "+
        "szcz^atki, kt^orych nie mo^zesz rozpozna^c. ^Sciany pokryte "+
        "^sluzowatymi ro^slinami s^a niemi^le w dotyku i dodatkowo "+
        "ociekaj^a wod^a, ^z^lobi^ac^a w nich przedziwne formacje skalne. \n";

    return str;
}
