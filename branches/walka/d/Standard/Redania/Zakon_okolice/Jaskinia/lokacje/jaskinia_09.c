//inherit "/std/room";
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit JASKINIA_U_STD;

void
dodaj_wyjscia_na_05()
{
//	remove_exit("ty�");
//	remove_exit("lewo");
//	remove_exit("prawo");
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"ty�","do ty�u",
		   "z jednego z korytarzy" 	}), &check("05","05"),J_FA,&check("05","05"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"lewo","w lewo",
		   "z jednego z korytarzy" 	}), &check("05","11"),J_FA,&check("05","11"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"prawo","w prawo",
		   "z jednego z korytarzy" 	}), &check("05","10"),J_FA,&check("05","10"));  

	ugly_update_action("ty�",1);
	ugly_update_action("lewo",1);
	ugly_update_action("prawo",1);
}




void
create_room()
{
    set_short("Du^za grota");
    add_prop(ROOM_I_INSIDE,1);
    //add_exit(JASKINIA_LOKACJE + "jaskinia_02.c","n",0,1,0);
	add_exit(JASKINIA_LOKACJE+"jaskinia_10.c",({"ty�","do ty�u",
		   "z jednego z korytarzy" 	}), &check("10","10"),J_FA,&check("10","10"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_10.c",({"lewo","w lewo",
		   "z jednego z korytarzy" 	}), &check("10","05"),J_FA,&check("10","05")); 
	add_exit(JASKINIA_LOKACJE+"jaskinia_10.c",({"prz�d","do przodu",
		   "z jednego z korytarzy" 	}), &check("10","11"),J_FA,&check("10","11"));  

	add_exit(JASKINIA_LOKACJE+"jaskinia_11.c",({"ty�","do ty�u",
		   "z jednego z korytarzy" 	}), &check("11","11"),J_FA,&check("11","11"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_11.c",({"prawo","w prawo",
		   "z jednego z korytarzy" 	}), &check("11","05"),J_FA,&check("11","05"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_11.c",({"prz�d","do przodu",
		   "z jednego z korytarzy" 	}), &check("11","10"),J_FA,&check("11","10"));  
    
	
	add_prop(ROOM_I_LIGHT, 0);
    add_item("^sciany",
                "^Sciany wy^z^lobione przez wod^e pokryte s^a dziwn^a "+
                "ro^slinno^sci^a przypominaj^aca glony. Niemi^le "+
                "i o^slizg^le w dotyku dodatkowo sp^lywaj^a wod^a "+
                "kapi^ac^a z sufitu .\n");

    add_item("szcz^atki",
                "Szcz^atki unosz^ace si^e na wodzie s^a bardzo trudne do "+
                "rozpoznania, mo^zliwe ^ze s^a to jakie^s kawa^lki ubrania "+
                "i drewna.\n");


        set_event_time(250.0+itof(random(100)));
    add_event("Jaki^s obrzydliwy zapach zdaje si^e dobiega^c z g^l^ebi "+
			"jaskini.\n");
    add_event("Kilka kropel wody spad^lo z sufitu. \n");
    add_event("Powierzchnia wody zafalowa^la lekko. \n");

}

string
dlugi_opis()
{
    string str;
    str = "Korytarz ko^nczy si^e w tym miejscu ^scian^a "+
        "pokryt^a jakimi^s glonami. Woda zalewaj^aca przej^scie "+
        "jest brudna i mulista, a po powierzchni p�ywaj^a jakie^s "+
        "szcz^atki, kt^orych nie mo^zesz rozpozna^c. ^Sciany pokryte "+
        "^sluzowatymi ro^slinami s^a niemi^le w dotyku i dodatkowo "+
        "ociekaj^a wod^a, ^z^lobi^ac^a w nich przedziwne formacje skalne. \n";

    return str;
}
