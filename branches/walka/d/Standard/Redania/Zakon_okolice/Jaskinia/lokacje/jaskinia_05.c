//inherit "/std/room";
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit JASKINIA_U_STD;

void
create_room()
{
    set_short("Skrzy�owanie korytarzy");
    add_prop(ROOM_I_INSIDE,1);

	add_exit(JASKINIA_LOKACJE+"jaskinia_02.c",({"prawo","w prawo",
			   "z jednego z korytarzy" 	}), &check("02","06"),J_FA,&check("02","06"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_02.c",({"lewo","w lewo",
			   "z jednego z korytarzy" 	}), &check("02","07"),J_FA,&check("02","07"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_02.c",({"prz�d","do przodu",
			   "z jednego z korytarzy" 	}), &check("02","09"),J_FA,&check("02","09"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_02.c",({"ty�","do ty�u",
			   "z jednego z korytarzy" 	}), &check("02","02"),J_FA,&check("02","02"));  

	if(random(2))
	{
	add_exit(JASKINIA_LOKACJE+"jaskinia_07.c",({"prawo","w prawo",
			   "z jednego z korytarzy" 	}), &check("07","02"),J_FA,&check("07","02"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_07.c",({"lewo","w lewo",
			   "z jednego z korytarzy" 	}), &check("07","09"),J_FA,&check("07","09"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_07.c",({"prz�d","do przodu",
			   "z jednego z korytarzy" 	}), &check("07","06"),J_FA,&check("07","06"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_07.c",({"ty�","do ty�u",
			   "z jednego z korytarzy" 	}), &check("07","07"),J_FA,&check("07","07")); 
	LOAD_ERR(JASKINIA_LOKACJE+"jaskinia_07");
	find_object(JASKINIA_LOKACJE+"jaskinia_07.c")->dodaj_wyjscia_na_05();	
	}
	else
	{
	add_exit(JASKINIA_LOKACJE+"jaskinia_09.c",({"prawo","w prawo",
			   "z jednego z korytarzy" 	}), &check("09","07"),J_FA,&check("09","07"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_09.c",({"lewo","w lewo",
			   "z jednego z korytarzy" 	}), &check("09","06"),J_FA,&check("09","06"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_09.c",({"prz�d","do przodu",
			   "z jednego z korytarzy" 	}), &check("09","02"),J_FA,&check("09","02"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_09.c",({"ty�","do ty�u",
			   "z jednego z korytarzy" 	}), &check("09","09"),J_FA,&check("09","09")); 
	LOAD_ERR(JASKINIA_LOKACJE+"jaskinia_09");
	find_object(JASKINIA_LOKACJE+"jaskinia_09.c")->dodaj_wyjscia_na_05();	

	}
	

	add_exit(JASKINIA_LOKACJE+"jaskinia_06.c",({"prawo","w prawo",
			   "z jednego z korytarzy" 	}), &check("06","09"),J_FA,&check("06","09"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_06.c",({"lewo","w lewo",
			   "z jednego z korytarzy" 	}), &check("06","02"),J_FA,&check("06","02"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_06.c",({"prz�d","do przodu",
			   "z jednego z korytarzy" 	}), &check("06","07"),J_FA,&check("06","07"));  
	add_exit(JASKINIA_LOKACJE+"jaskinia_06.c",({"ty�","do ty�u",
			   "z jednego z korytarzy" 	}), &check("06","06"),J_FA,&check("06","06"));  





    add_npc(JASKINIA_LIVINGI+"utopiec.c");
    add_prop(ROOM_I_LIGHT, 0);
	add_prop(ROOM_I_TYPE,ROOM_CAVE);
    add_item("^sciany",
                "^Sciany wy^z^lobione przez wod^e pokryte s^a dziwn^a "+
                "ro^slinno^sci^a przypominaj^aca glony. Niemi^le "+
                "i o^slizg^le w dotyku dodatkowo sp^lywaj^a wod^a "+
                "kapi^ac^a z sufitu .\n");

    add_item("szcz^atki",
                "Szcz^atki unosz^ace si^e na wodzie s^a bardzo trudne do "+
                "rozpoznania, mo^zliwe ^ze s^a to jakie^s kawa^lki ubrania "+
                "i drewna.\n");

        set_event_time(250.0+itof(random(100)));
    add_event("Intensywniejszy smr^od dochodzi ci^e "+
			"najprawdopodobniej z lewej strony. \n");
    add_event("Intensywniejszy smr^od dochodzi ci^e "+
			"najprawdopodobniej z prawej strony. \n");
	add_event("Kilka kropel wody spad^lo z sufitu. \n");
    add_event("Ka^zdy odg^los, nawet ten najcichszy rozbrzmiewa tu echem. \n");
    add_event("Powierzchnia wody zafalowa^la lekko. \n");
	add_event("�mierdzi tu czym� zgni�ym.\n");
}
/*exits_description()
{
    return "Do jaskini dochodz^a cztery korytarze: "+
			"p^o^lnocny, po^ludniowy, wschodni i zachodni.\n";
}*/

string
dlugi_opis()
{
    string str;
    str = "Sporych rozmiar^ow grota wy^z^lobiona przez wod^e w "+
        "wapiennej skale rozdziela si^e na cztery korytarze. "+
        "Jedne z przej^s^c s^a mniejsze i ni^zsze od pozosta�ych. "+
        "Woda zalewaj^aca przej^scie jest brudna i mulista, a po "+
        "powierzchni p^lywaj^a jakie^s szcz^atki, kt^orych nie mo^zesz "+
        "rozpozna^c. ^Sciany pokryte ^sluzowatymi ro^slinami s^a niemi^le "+
        "w dotyku i dodatkowo ociekaj^a wod^a, ^z^lobi^ac^a w nich "+
		"przedziwne formacje skalne.\n";
    return str;
}
