/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Le�ny trakt");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t22.c","n",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t20.c","se",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description() 
{
    return "Trakt prowadzi z po�udniowego-wschodu na p�noc.\n";
}

string
dlugi_opis()
{
    string str="";

	if(CZY_JEST_SNIEG(TO))
		str+="Trakt rozjechany przez ko�a pojazd�w, pokryty jest "+
		"warstw� �niegu, kt�ra bardzo utrudnia podr� tym szlakiem. "+
		"Gdzieniegdzie w grz�skim b�ocie wida� tak�e �lady ko�skich "+
		"kopyt i ludzkich st�p. W niekt�rych miejscach koleiny s� tak "+
		"g��bokie, i� w�z jad�cy t�dy musi bardzo zwolni� by nie "+
		"z�ama� zawieszenia.";
	else
		str+="Trakt jest rozjechany przez ko�a pojazd�w, wida� na nim "+
		"gdzieniegdzie tak�e �lady ko�skich kopyt i ludzkich st�p. W "+
		"niekt�rych miejscach koleiny s� tak g��bokie, i� w�z jad�cy t�dy "+
		"musi bardzo zwolni� by nie z�ama� zawieszenia.";

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}