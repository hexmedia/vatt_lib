/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("przy jeziorze");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj14.c","s",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj16.c","nw",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z p�nocnego-zachodu na po�udnie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Zwarta �ciana drzew li�ci"+
             "asto iglastych przeszkadza podr�nemu w zboczeniu ze �cie"+
             "�ki, a masywne i g�ste korony przys�aniaj� widok nieba. "+
			"Na p�nocy, gdy podniesiemy wzrok ponad martwe i zmarz�e "+
			"konary i krzewy dostrze�emy oblodzon� tafl� rozleg�ego "+
			"jeziora, za� na po�udniu swe miejsce znalaz�y g�sto rosn�ce "+
			"drzewa, teraz przykryte �niegiem.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Zwarta �ciana drzew li�ci"+
             "asto iglastych przeszkadza podr�nemu w zboczeniu ze �cie"+
             "�ki, a masywne i g�ste korony przys�aniaj� widok nieba. "+
			"Na p�nocy, za rzadk� �cian� ro�lin i krzew�w rozpo�ciera "+
			"si� widok na b��kitn� to� jeziora, za� na po�udniu okazale "+
			"prezentuje si� zielona �ciana mieszanych drzew.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Zwarta �ciana drzew li�ci"+
             "asto iglastych przeszkadza podr�nemu w zboczeniu ze �cie"+
             "�ki, a masywne i g�ste korony przys�aniaj� widok nieba. "+
			"Na p�nocy, za rzadk� �cian� ro�lin i krzew�w rozpo�ciera "+
			"si� widok na b��kitn� to� jeziora, za� na po�udniu okazale "+
			"prezentuje si� zielona �ciana mieszanych drzew.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Zwarta �ciana drzew li�ci"+
             "asto iglastych przeszkadza podr�nemu w zboczeniu ze �cie"+
             "�ki, a masywne i g�ste korony przys�aniaj� widok nieba. "+
			"Na p�nocy, za rzadk� �cian� ro�lin i krzew�w rozpo�ciera "+
			"si� widok na b��kitn� to� jeziora, za� na po�udniu okazale "+
			"prezentuje si� zielona �ciana mieszanych drzew.";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}