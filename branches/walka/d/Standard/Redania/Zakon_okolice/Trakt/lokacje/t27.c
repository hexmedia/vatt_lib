/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Trakt w�r�d p�l");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t28.c","n",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t26.c","se",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description() 
{
    return "Trakt prowadzi z po�udniowego-wschodu na p�noc.\n";
}

string
dlugi_opis()
{
    string str="";

	if(CZY_JEST_SNIEG(TO))
		str+="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
		"Trakt i okoliczne drzewa pokryte s� "+
		"�niegiem, kt�rego ilo�� mo�e sprawia� problemy podr�nym. ";
	else
		str+="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
		"Trakt jest wy��obiony ko�ami pojazd�w, "+
		"wida� na nim tak�e �lady kopyt i podk�w. ";




	if(CZY_JEST_SNIEG(TO))
		str+="Prawie wszystko przykryte jest spor� warstw� bielutkiego "+
		"�niegu, kt�ry przysypa� okolic^e. ";
	else if(pora_roku() == MT_WIOSNA)
		str+="Otaczaj�ca ci� przyroda budzi si^e do �ycia po zimowym "+
		"�nie, a lekki wietrzyk przynosi do ciebie �wie�y zapach ro�lin. ";
	else if(pora_roku() == MT_LATO)
		str+="Ca�a przyroda w pe�nym rozkwicie, zieleni otaczaj�cy "+
		"ci� krajobraz, a w powietrzu mo�esz wyczu� intensywny zapach ro�lin. ";
	else if(pora_roku() == MT_JESIEN)
		str+="Kolory powoli zaczynaj� blakn��, przyroda zaczyna "+
		"przygotowywa� si� do zimowego snu. ";

	str+="Na p�nocy majacz� wysokie mury otaczaj�ce miasto, z kt�rego "+
	"bij� w g�r� wysokie wie�e.";

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}