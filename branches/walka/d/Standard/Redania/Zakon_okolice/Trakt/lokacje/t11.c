/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Le�ny trakt");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t12.c","nw",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t10.c","e",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description() 
{
    return "Trakt prowadzi ze wschodu na p�nocny-zach�d.\n";
}

string
dlugi_opis()
{
    string str="";

	str+="Znajdujesz si� w g��bi g�stego, starego i tajemniczego "+
	"lasu. Powietrze przesycone jest zapachem �ywicy i wilgotnego "+
	"igliwia, a wiatr przynosi do twych uszu ciche odg�osy "+
	"zamieszkuj�cych t� pot�n� puszcz� zwierz�t. Trakt jest do�� "+
	"szeroki, a gdzieniegdzie utworzy�y si� g��bokie koleiny "+
	"wy��obione przez przeje�d�aj�ce wozy.";

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}