/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Trakt po�r�d ��k");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t02.c","nw",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt37.c","s",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi z po�udnia na p�nocny-zach�d.\n";
}

string
dlugi_opis()
{
    string str="";

	if(CZY_JEST_SNIEG(TO))
		str+="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
		"Trakt oraz okoliczne pola pokryte s� "+
		"�niegiem, kt�rego ilo�� mo�e sprawia� problemy podr�nym. "+
		"Wy��obiony ko�ami pojazd�w, i pe�en b�ota, na kt�rym wida� "+
		"tak�e �lady kopyt, podk�w i st�p wygl�da na do�� u�ywany.";
	else
		str+="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
		"Trakt jest wy��obiony ko�ami pojazd�w, "+
		"wida� na nim tak�e �lady kopyt i podk�w. W niekt�rych miejscach "+
		"koleiny s� tak g��bokie, i� w�z jad�cy t�dy musi bardzo zwolni� "+
		"by nie z�ama� zawieszenia.";

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}