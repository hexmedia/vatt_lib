/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("kr�ta �cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj02.c","ne",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj04.c","nw",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z p�nocnego-zachodu na p�nocny-wsch�d.\n";
}
string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="G�sto rosn�ce praktycznie ze wszystkich stron wysokie drze"+
             "wa przejmuj� groz�, a powykr�cane krzewy i pnie dodaj� n"+
             "a niesamowito�ci otoczenia. Oblodzona �cie�ka wije si� f"+
             "alist� wst�g� ze wschodu na zach�d, a gdzie nie spojrze�"+
             " dooko�a zalega bia�y puch. Natura u�piona w czasie zimy"+
             ", jest cicha i spokojna, nie s�ycha� tutaj prawie �adnych"+
             " d�wi�k�w, a prawie wszystko przykryte jest grub� warstw"+
             "a czystego �niegu. Drzewa i krzewy pokryte �niegiem uginaj"+
             "� si� pod jego ci�arem, a gdzieniegdzie pod krzakiem mo"+
             "�na zauwa�y� na tym bia�ym puchu ma�e �lady zaj�cy. W"+
             "ydeptana pomi�dzy drzewami �cie�ka jest bardzo kr�ta i w"+
             "�ska. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="G�sto rosn�ce praktycznie ze wszystkich stron wysokie drze"+
             "wa przejmuj� groz�, a powykr�cane krzewy i pnie dodaj� n"+
             "a niesamowito�ci otoczenia. Le�na �cie�ka wije si� fali"+
             "st� wst�g� ze wschodu na zach�d, otoczona zewsz�d natur"+
             "�. W powietrzu unosz� sie stada ma�ych muszek, ptaki �pi"+
             "ewaj� swoje piosenki, a flora rozkwita na ka�dym kroku. La"+
             "s ze swoj� r�norodn� zwierzyn�, wype�niony jest docho"+
             "dz�cymi z niego pomrukami, �wierkaniami i szelestami. Wyde"+
             "ptana pomi�dzy drzewami �cie�ka jest bardzo kr�ta i w�s"+
             "ka. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="G�sto rosn�ce praktycznie ze wszystkich stron wysokie drze"+
             "wa przejmuj� groz�, a powykr�cane krzewy i pnie dodaj� n"+
             "a niesamowito�ci otoczenia. Le�na �cie�ka wije si� fali"+
             "st� wst�g� ze wschodu na zach�d, otoczona zewsz�d natur"+
             "�. W powietrzu unosz� sie stada ma�ych muszek, ptaki �pi"+
             "ewaj� swoje piosenki, a flora rozkwita na ka�dym kroku. La"+
             "s ze swoj� r�norodn� zwierzyn�, wype�niony jest docho"+
             "dz�cymi z niego pomrukami, �wierkaniami i szelestami. Wyde"+
             "ptana pomi�dzy drzewami �cie�ka jest bardzo kr�ta i w�s"+
             "ka. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="G�sto rosn�ce praktycznie ze wszystkich stron wysokie drze"+
             "wa przejmuj� groz�, a powykr�cane krzewy i pnie dodaj� n"+
             "a niesamowito�ci otoczenia. Le�na �cie�ka wije si� fali"+
             "st� wst�g� ze wschodu na zach�d, otoczona zewsz�d natur"+
             "�. Pod stopami s�yszysz chrz�st patyk�w i szum suchych o"+
             "padni�tych li�ci. Od czasu do czasu na tw� g�ow� spada "+
             "jaki� zrzucony li��. Wydeptana pomi�dzy drzewami �cie�"+
             "ka jest bardzo kr�ta i w�ska. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}