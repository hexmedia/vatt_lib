/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("�cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj13.c","se",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj15.c","n",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z p�nocy na po�udniowy-wsch�d.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="G�sty las przez kt�ry prowadzi w�ska �cie�ka, po kt�rej"+
             " z du�ymi problemami porusza�by si� nawet ma�ych rozmiar"+
             "�w w�z, wygl�da na bardzo stary. �cie�ka wydeptana prze"+
             "z stopy ludzkie jest nie szersza ni� na naprawd� malutki w"+
             "�z. Bia�y puch przykrywaj�cy ro�liny jest do�� gruby i"+
             " skrzypi pod krokami, a gdzieniegdzie mo�na zauwa�y� na n"+
             "im drobne �lady ma�ych zwierz�t. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="G�sty las przez kt�ry prowadzi w�ska �cie�ka, po kt�rej"+
             " z du�ymi problemami porusza�by si� nawet ma�ych rozmiar"+
             "�w w�z, wygl�da na bardzo stary. �cie�ka wydeptana prze"+
             "z stopy ludzkie jest nie szersza ni� na naprawd� malutki w"+
             "�z. Soczysta ziele�, i wszechobecne odg�osy zamieszkuj�c"+
             "ych ten las zwierz�t, powoduj� i� w�dr�wka t� �cie�y"+
             "n� jest naprawd� przyjemna. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="G�sty las przez kt�ry prowadzi w�ska �cie�ka, po kt�rej"+
             " z du�ymi problemami porusza�by si� nawet ma�ych rozmiar"+
             "�w w�z, wygl�da na bardzo stary. �cie�ka wydeptana prze"+
             "z stopy ludzkie jest nie szersza ni� na naprawd� malutki w"+
             "�z. Soczysta ziele�, i wszechobecne odg�osy zamieszkuj�c"+
             "ych ten las zwierz�t, powoduj� i� w�dr�wka t� �cie�y"+
             "n� jest naprawd� przyjemna. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="G�sty las przez kt�ry prowadzi w�ska �cie�ka, po kt�rej"+
             " z du�ymi problemami porusza�by si� nawet ma�ych rozmiar"+
             "�w w�z, wygl�da na bardzo stary. �cie�ka wydeptana prze"+
             "z stopy ludzkie jest nie szersza ni� na naprawd� malutki w"+
             "�z. Opad�e z drzew li�cie powoduj� i� na pod�o�u utwo"+
             "rzy� sie dywan szumi�cy i szeleszcz�cy pod ka�dym krokie"+
             "m podr�nego. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
