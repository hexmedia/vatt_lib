/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

string
czapa()
{
	if(CZY_JEST_SNIEG(TO) && random(2))
		return "Du�a czapa �niegu spada z pobliskiej ga��zi.\n";
	else if(random(2))
		return "S�yszysz szum wiatru w koronach drzew.\n";
	else if(random(3))
		return "Mocniejszy podmuch wiatru poderwa� li�cie "+
				"do powietrznego ta�ca.\n";
	else if(random(2))
		return "S�yszysz wiatr wiej�cy w koronach drzew.\n";
	else
		return "Do twych uszu dochodz� ciche pomruki i wycie gdzie� z oddali.\n";

	return "";
}

void create_trakt() 
{
    set_short("Le�ny trakt");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t11.c","w",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t09.c","se",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
	add_event("@@czapa:"+file_name(TO)+"@@");
}

public string
exits_description() 
{
    return "Trakt prowadzi z po�udniowego-wschodu na zach�d.\n";
}

string
dlugi_opis()
{
    string str="";

	if(CZY_JEST_SNIEG(TO))
		str+="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
		"Trakt i okoliczne drzewa pokryte s� "+
		"�niegiem, kt�rego ilo�� mo�e sprawia� problemy podr�nym. "+
		"Wy��obiony ko�ami pojazd�w, i pe�en b�ota na, kt�rym wida� "+
		"tak�e �lady kopyt, podk�w i st�p wygl�da na do�� u�ywany. "+
		"W niekt�rych miejscach koleiny s� tak g��bokie, i� w�z jad�cy "+
		"t�dy musi bardzo zwolni� by nie z�ama� zawieszenia.";
	else
		str+="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
		"Trakt jest wy��obiony ko�ami pojazd�w, "+
		"wida� na nim tak�e �lady kopyt i podk�w. W niekt�rych miejscach "+
		"koleiny s� tak g��bokie, i� w�z jad�cy t�dy musi bardzo zwolni� "+
		"by nie z�ama� zawieszenia.";

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}
