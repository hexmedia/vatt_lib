/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>


inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("le�na �cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj01.c","e",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj03.c","sw",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z po�udniowego-zachodu na wsch�d.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Natura u�piona w czasie zimy, jest cicha i spokojna, nie s�"+
             "ycha� tutaj prawie �adnych d�wi�k�w, a prawie wszystko"+
             " przykryte jest grub� warstwa czystego �niegu. Na po�udni"+
             "u rozci�gaj� si� niezmierzone po�acie p�l, kt�re przyk"+
             "ryte bia�ym �niegiem wygl�daj� jak tafla ogromnego jezio"+
             "ra, na kt�rym panuje ca�kowita cisza i spok�j. Drzewa i k"+
             "rzewy pokryte �niegiem uginaj� si� pod jego ci�arem, a"+
             " gdzieniegdzie pod krzakiem mo�na zauwa�y� na tym bia�ym"+
             " puchu ma�e �lady zaj�cy. Bia�y puch przykrywaj�cy ro�"+
             "liny jest do�� gruby i skrzypi pod krokami, a gdzieniegdzi"+
             "e mo�na zauwa�y� na nim drobne �lady ma�ych zwierz�t. "+
             "Oblodzona �cie�ka wije si� falist� wst�g� ze wschodu n"+
             "a zach�d, a gdzie nie spojrze� dooko�a zalega bia�y puch"+
             ". ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="W powietrzu unosz� sie stada ma�ych muszek, ptaki �piewaj"+
             "� swoje piosenki, a flora rozkwita na ka�dym kroku. Na po�"+
             "udniu rozci�gaj� si� niezmierzone po�acie p�l, kt�re "+
             "b�d�c pi�knie zazielenione, s� prawdziwym rajem dla okol"+
             "icznych ptak�w i zwierz�t, kt�rych piski, skrzeki i mrucz"+
             "enia s�ycha� nawet z takiej odleg�o�ci. Las ze swoj� r�"+
             "�norodn� zwierzyn�, wype�niony jest dochodz�cymi z nie"+
             "go pomrukami, �wierkaniami i szelestami. Soczysta ziele�, "+
             "i wszechobecne odg�osy zamieszkuj�cych ten las zwierz�t, "+
             "powoduj� i� w�dr�wka t� �cie�yn� jest naprawd� przy"+
             "jemna. Le�na �cie�ka wije si� falist� wst�g� ze wscho"+
             "du na zach�d, otoczona zewsz�d natur�. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="W powietrzu unosz� sie stada ma�ych muszek, ptaki �piewaj"+
             "� swoje piosenki, a flora rozkwita na ka�dym kroku. Na po�"+
             "udniu rozci�gaj� si� niezmierzone po�acie p�l, kt�re "+
             "b�d�c pi�knie zazielenione, s� prawdziwym rajem dla okol"+
             "icznych ptak�w i zwierz�t, kt�rych piski, skrzeki i mrucz"+
             "enia s�ycha� nawet z takiej odleg�o�ci. Las ze swoj� r�"+
             "�norodn� zwierzyn�, wype�niony jest dochodz�cymi z nie"+
             "go pomrukami, �wierkaniami i szelestami. Soczysta ziele�, "+
             "i wszechobecne odg�osy zamieszkuj�cych ten las zwierz�t, "+
             "powoduj� i� w�dr�wka t� �cie�yn� jest naprawd� przy"+
             "jemna. Le�na �cie�ka wije si� falist� wst�g� ze wscho"+
             "du na zach�d, otoczona zewsz�d natur�. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Pod stopami s�yszysz chrz�st patyk�w i szum suchych opadn"+
             "i�tych li�ci. Na po�udniu rozci�gaj� si� niezmierzone "+
             "po�acie p�l, nad kt�rymi snuje si� sie lekka mg�a nadaj"+
             "�ca im do�� niepokoj�cy wygl�d, a z otaczaj�cych ci� "+
             "drzew powoli spadaj� r�nokolorowe li�cie, kt�re po swo"+
             "bodnym locie, delikatnie opadaj� na ziemi�. Od czasu do cz"+
             "asu na tw� g�ow� spada jaki� zrzucony li��. Opad�e z "+
             "drzew li�cie powoduj� i� na pod�o�u utworzy� sie dywan"+
             " szumi�cy i szeleszcz�cy pod ka�dym krokiem podr�nego."+
             " Le�na �cie�ka wije si� falist� wst�g� ze wschodu na "+
             "zach�d, otoczona zewsz�d natur�. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}