/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Trakt w lesie");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t23.c","nw",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t21.c","s",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description() 
{
    return "Trakt prowadzi z po�udnia na p�nocny-zach�d.\n";
}

string
dlugi_opis()
{
    string str="";

	switch(pora_roku())
	{
		case MT_JESIEN:
		case MT_WIOSNA:
			str="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
			"G��wny trakt jest rozjechany przez ko�a pojazd�w, wida� "+
			"na nim gdzieniegdzie tak�e �lady ko�skich kopyt i "+
			"ludzkich st�p. W niekt�rych miejscach koleiny s� tak "+
			"g��bokie, i� w�z jad�cy t�dy musi bardzo zwolni� by nie "+
			"z�ama� zawieszenia.";
			break;
		case MT_ZIMA:
			if(CZY_JEST_SNIEG(TO))
			str="Na oblodzonej, a gdzieniegdzie grz�skiej powierzchni drogi "+
			"trudno jest stawia� nogi, aby si� nie zapa�� w b�ocie, "+
			"czy nie po�lizgn��, a zapewne niejeden wo�nica, czy "+
			"je�dziec przekona� si� tak�e, �e r�wnie trudno jest "+
			"sterowa� w takich warunkach wozem lub wierzchowcem.";
			else
			str="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
			"G��wny trakt jest rozjechany przez ko�a pojazd�w, wida� "+
			"na nim gdzieniegdzie tak�e �lady ko�skich kopyt i "+
			"ludzkich st�p. W niekt�rych miejscach koleiny s� tak "+
			"g��bokie, i� w�z jad�cy t�dy musi bardzo zwolni� by nie "+
			"z�ama� zawieszenia.";
			break;
		case MT_LATO:
			str="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
			"G��wny trakt jest rozjechany przez ko�a pojazd�w, wida� "+
			"na nim gdzieniegdzie tak�e �lady ko�skich kopyt i "+
			"ludzkich st�p. W niekt�rych miejscach koleiny s� tak "+
			"g��bokie, i� w�z jad�cy t�dy musi bardzo zwolni� by "+
			"nie z�ama� zawieszenia. Bujna, polna "+
			"ro�linno�� rozpo�ciera si� po obu stronach traktu, kt�ry "+
			"zdaje si� by� wprost obl�ony zieleni�.";
			break;
	}
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}