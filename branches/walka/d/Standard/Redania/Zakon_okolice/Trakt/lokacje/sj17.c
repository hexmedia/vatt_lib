/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("przy pomo�cie");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj16.c","se",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj18.c","w",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "pomost.c","pomost",
				0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z zachodu na po�udniowy-wsch�d. Widnieje "+
	"tu tak�e wej�cie na pomost.\n";
}

string
dlugi_opis()
{
    string str = "";
    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. G�sto rosn�ce praktycznie"+
             " ze wszystkich stron wysokie drzewa przejmuj� groz�, a pow"+
             "ykr�cane krzewy i pnie dodaj� na niesamowito�ci otoczenia"+
             ". "+
			"Na p�nocy, gdy podniesiemy wzrok ponad martwe i zmarz�e "+
			"konary i krzewy dostrze�emy oblodzon� tafl� rozleg�ego "+
			"jeziora, za� na po�udniu swe miejsce znalaz�y g�sto rosn�ce "+
			"drzewa, teraz przykryte �niegiem. "+
			"Bia�y puch przykrywaj�cy ro�liny jest do�� gruby i sk"+
             "rzypi pod krokami, a gdzieniegdzie mo�na zauwa�y� na nim "+
             "drobne �lady ma�ych zwierz�t. Drzewa i okolica spowite s�"+
             " bia�ym puchem, a mocny mr�z powoduje, ze �nieg skrzypi "+
             "pod butami. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. G�sto rosn�ce praktycznie"+
             " ze wszystkich stron wysokie drzewa przejmuj� groz�, a pow"+
             "ykr�cane krzewy i pnie dodaj� na niesamowito�ci otoczenia"+
             ". "+
			"Na p�nocy, za rzadk� �cian� ro�lin i krzew�w rozpo�ciera "+
			"si� widok na b��kitn� to� jeziora, za� na po�udniu okazale "+
			"prezentuje si� zielona �ciana mieszanych drzew. "+
			"Soczysta ziele�, i wszechobecne odg�osy zamieszkuj�cych"+
             " ten las zwierz�t, powoduj� i� w�dr�wka t� �cie�yn�"+
             " jest naprawd� przyjemna. Wszystko dooko�a jest przyjemnie"+
             " zazielenione, a cisz� panuj�c� w lesie od czasu do czasu"+
             " przerywa jaki� �wiergot ptaka. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. G�sto rosn�ce praktycznie"+
             " ze wszystkich stron wysokie drzewa przejmuj� groz�, a pow"+
             "ykr�cane krzewy i pnie dodaj� na niesamowito�ci otoczenia"+
             ". "+
			"Na p�nocy, za rzadk� �cian� ro�lin i krzew�w rozpo�ciera "+
			"si� widok na b��kitn� to� jeziora, za� na po�udniu okazale "+
			"prezentuje si� zielona �ciana mieszanych drzew. "+
			"Soczysta ziele�, i wszechobecne odg�osy zamieszkuj�cych"+
             " ten las zwierz�t, powoduj� i� w�dr�wka t� �cie�yn�"+
             " jest naprawd� przyjemna. Wszystko dooko�a jest przyjemnie"+
             " zazielenione, a cisz� panuj�c� w lesie od czasu do czasu"+
             " przerywa jaki� �wiergot ptaka. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. G�sto rosn�ce praktycznie"+
             " ze wszystkich stron wysokie drzewa przejmuj� groz�, a pow"+
             "ykr�cane krzewy i pnie dodaj� na niesamowito�ci otoczenia"+
             ". "+
			"Na p�nocy, za rzadk� �cian� ro�lin i krzew�w rozpo�ciera "+
			"si� widok na b��kitn� to� jeziora, za� na po�udniu okazale "+
			"prezentuje si� zielona �ciana mieszanych drzew. "+
			"Opad�e z drzew li�cie powoduj� i� na pod�o�u utworzy"+
             "� sie dywan szumi�cy i szeleszcz�cy pod ka�dym krokiem p"+
             "odr�nego. Suche opadni�te na ziemi� li�cie szeleszcz�"+
             " pod krokami, zwierz�ta szykuj� si� do zimowego snu. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}