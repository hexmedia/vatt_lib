/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Przy bramie");
    //add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t30.c","n",0,TRAKT_ZO_FATIG,0);
    add_object(TRETOGOR_DRZWI + "brama_poludniowa_do");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t29.c","s",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);

	add_item("mury",
	"Szary kamie� wykorzystany do budowy tych mur�w zosta� ociosany "+
	"i po��czony z innymi blokami za pomoc� ciemno brunatnej spoiny. "+
	"Wysokie na trzydzie�ci st�p i grube tak by mogli mija� si� na "+
	"nich gwardzi�ci patroluj�cy blanki zapewniaj� miastu naprawd� "+
	"solidn� ochron� przed atakami naje�d�c�w.");
	
}

public string
exits_description() 
{
    return "Trakt prowadzi na po�udnie, a na p�nocy znajduje si� brama "+
	"miejska.\n";
}

string
dlugi_opis()
{
    string str="";

	str+="Szeroki, starannie wy�o�ony kocimi �bami trakt, urywa si� "+
	"przy wysokiej, pot�nej i zdobionej bramie, umocowanej w grubych "+
	"kamiennych murach, okalaj�cych miasto Tretogor. ";

	if(pora_roku()==MT_WIOSNA || pora_roku()==MT_LATO)
	str+="Powietrze przesycone zapachem zi� i kwiat�w dociera "+
		"tutaj z okolicznych ��k i p�l, a rozsiane dooko�a miasta "+
		"chaty i domki zamieszka�e s� przez miejscowych rolnik�w. ";
	else if(CZY_JEST_SNIEG(TO))
	str+="Ch�odne powietrze i gruba pokrywa �niegu zalegaj�ca na "+
	"okolicznych polach i ��kach powoduje i� okolica wydaje si� "+
	"nader czysta i spokojna. Z wi�kszo�ci domk�w, otaczaj�cych "+
	"miasto, kt�re nale�� do miejscowych rolnik�w wydobywa si� "+
	"lekko bury dym. ";
	else
	str+="W powietrzu czu� unosz�cy si� zapach ognisk i pieczonych "+
	"ziemniak�w, a po horyzoncie snuj� si� powoli delikatne opary "+
	"mg�y. Dooko�a miasta rozsiane s� domki i chaty nale��ce do "+
	"miejscowych rolnik�w. ";

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}