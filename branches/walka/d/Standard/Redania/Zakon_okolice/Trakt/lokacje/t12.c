/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Le�ny trakt");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t13.c","nw",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t11.c","se",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description() 
{
    return "Trakt prowadzi z po�udniowego-wschodu na p�nocny-zach�d.\n";
}

string
dlugi_opis()
{
    string str="";

	str+="Otaczaj�ca ci� �ciana wysokich, pot�nych drzew i krzew�w "+
	"jest nie do przenikni�cia dla podr�nego. Trakt jest do�� "+
	"szeroki, a gdzieniegdzie utworzy�y si� g��bokie koleiny wy��obione "+
	"przez przeje�d�aj�ce wozy. Liczne suche ga��zie, kt�re pospada�y "+
	"z g�ry cz�sto tarasuj� drog� i utrudniaj� podr�. ";

	if(pora_roku() == MT_WIOSNA)
		str+="Otaczaj�ca ci� przyroda budzi sie do �ycia po zimowym "+
		"�nie, a wiosenny wiaterek orze�wia i rozwiewa niepokoj�ce "+
		"my�li dotycz�ce puszczy.";
	else if(pora_roku() == MT_LATO)
		str+="Ca�a przyroda w pe�nym rozkwicie, zieleni otaczaj�ce ci� "+
		"drzewa i krzewy, a do twoich uszu dochodz� odg�osy "+
		"zamieszkuj�cych t� okolice zwierz�t.";
	else if(pora_roku() == MT_JESIEN)
		str+="Z drzew spadaj� li�cie tworz�c na ziemi kobierzec, "+
		"szumi�cy pod krokami i ko�ami przeje�d�aj�cych woz�w.";
	else if(CZY_JEST_SNIEG(TO))
		str+="Drzewa pokryte czapami �niegu, zwieszaj� swoje ga��zie, "+
		"a na ziemi mo�na zauwa�y� �lady zwierz�t.";

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}