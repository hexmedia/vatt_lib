/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Szeroki trakt");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t05.c","nw",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t03.c","se",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
	add_event("Na pobliskim polu wyl�dowa� czarny bocian.\n");
}
public string
exits_description() 
{
    return "Trakt prowadzi z po�udniowego-wschodu na p�nocny-zach�d.\n";
}

string
dlugi_opis()
{
    string str="";

	switch(pora_roku())
	{
		case MT_JESIEN:
		case MT_WIOSNA:
			str="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
			"W miernie utwardzonej, grz�skiej powierzchni drogi swoje "+
			"miejsce znalaz�y liczne koleiny - pozosta�o�ci po "+
			"przeje�d�aj�cych t�dy wozach, gdzieniegdzie da si� tak�e "+
			"zauwa�y� odciski podk�w, kt�re wraz z koleinami wype�ni�y "+
			"si� wod� tworz�c razem rozleg�e ka�u�e.";
			break;
		case MT_ZIMA:
			if(CZY_JEST_SNIEG(TO))
			str="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
			"Na oblodzonej, a gdzieniegdzie grz�skiej powierzchni drogi "+
			"trudno jest stawia� nogi, aby si� nie zapa�� w b�ocie, "+
			"czy nie po�lizgn��, a zapewne niejeden wo�nica, czy "+
			"je�dziec przekona� si� tak�e, �e r�wnie trudno jest "+
			"sterowa� w takich warunkach wozem lub wierzchowcem.";
			else
			str="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
			"W miernie utwardzonej, grz�skiej powierzchni drogi swoje "+
			"miejsce znalaz�y liczne koleiny - pozosta�o�ci po "+
			"przeje�d�aj�cych t�dy wozach, gdzieniegdzie da si� tak�e "+
			"zauwa�y� odciski podk�w, kt�re wraz z koleinami wype�ni�y "+
			"si� wod� tworz�c razem rozleg�e ka�u�e.";
			break;
		case MT_LATO:
			str="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
			"Na powierzchni drogi swoje miejsce znalaz�y liczne koleiny - "+
			"pozosta�o�ci po przeje�d�aj�cych t�dy wozach. Bujna, polna "+
			"ro�linno�� rozpo�ciera si� po obu stronach traktu, kt�ry "+
			"zdaje si� by� wprost obl�ony zieleni�.";
			break;
	}

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}