/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("le�na �cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj03.c","se",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj05.c","w",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z zachodu na po�udniowy-wsch�d.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Drzewa rosn�ce na p�nocy pn� si� w g�r� tworz�c jak"+
             "by naturalny p�ot, broni�cy las przed intruzami, jednocze�"+
             "nie drzewa te, dzi�ki roz�o�ystym koronom daj� spore za"+
             "cienienie przez co w�dr�wka t� �cie�yn� nie wydaje si�"+
             " by� a� tak m�cz�ca. Bia�y puch przykrywaj�cy ro�lin"+
             "y jest do�� gruby i skrzypi pod krokami, a gdzieniegdzie m"+
             "o�na zauwa�y� na nim drobne �lady ma�ych zwierz�t. Obl"+
             "odzona �cie�ka wije si� falist� wst�g� ze wschodu na z"+
             "ach�d, a gdzie nie spojrze� dooko�a zalega bia�y puch. D"+
             "rzewa i okolica spowite s� bia�ym puchem, a mocny mr�z po"+
             "woduje, ze �nieg skrzypi pod butami. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Drzewa rosn�ce na p�nocy pn� si� w g�r� tworz�c jak"+
             "by naturalny p�ot, broni�cy las przed intruzami, jednocze�"+
             "nie drzewa te, dzi�ki roz�o�ystym koronom daj� spore za"+
             "cienienie przez co w�dr�wka t� �cie�yn� nie wydaje si�"+
             " by� a� tak m�cz�ca. Soczysta ziele�, i wszechobecne o"+
             "dg�osy zamieszkuj�cych ten las zwierz�t, powoduj� i� w�"+
             "dr�wka t� �cie�yn� jest naprawd� przyjemna. Le�na �"+
             "cie�ka wije si� falist� wst�g� ze wschodu na zach�d, o"+
             "toczona zewsz�d natur�. Wszystko dooko�a jest przyjemnie "+
             "zazielenione, a cisz� panuj�c� w lesie od czasu do czasu "+
             "przerywa jaki� �wiergot ptaka. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Drzewa rosn�ce na p�nocy pn� si� w g�r� tworz�c jak"+
             "by naturalny p�ot, broni�cy las przed intruzami, jednocze�"+
             "nie drzewa te, dzi�ki roz�o�ystym koronom daj� spore za"+
             "cienienie przez co w�dr�wka t� �cie�yn� nie wydaje si�"+
             " by� a� tak m�cz�ca. Soczysta ziele�, i wszechobecne o"+
             "dg�osy zamieszkuj�cych ten las zwierz�t, powoduj� i� w�"+
             "dr�wka t� �cie�yn� jest naprawd� przyjemna. Le�na �"+
             "cie�ka wije si� falist� wst�g� ze wschodu na zach�d, o"+
             "toczona zewsz�d natur�. Wszystko dooko�a jest przyjemnie "+
             "zazielenione, a cisz� panuj�c� w lesie od czasu do czasu "+
             "przerywa jaki� �wiergot ptaka. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Drzewa rosn�ce na p�nocy pn� si� w g�r� tworz�c jak"+
             "by naturalny p�ot, broni�cy las przed intruzami, jednocze�"+
             "nie drzewa te, dzi�ki roz�o�ystym koronom daj� spore za"+
             "cienienie przez co w�dr�wka t� �cie�yn� nie wydaje si�"+
             " by� a� tak m�cz�ca. Opad�e z drzew li�cie powoduj� "+
             "i� na pod�o�u utworzy� sie dywan szumi�cy i szeleszcz�"+
             "cy pod ka�dym krokiem podr�nego. Le�na �cie�ka wije s"+
             "i� falist� wst�g� ze wschodu na zach�d, otoczona zewsz�"+
             "d natur�. Suche opadni�te na ziemi� li�cie szeleszcz� "+
             "pod krokami, zwierz�ta szykuj� si� do zimowego snu. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}