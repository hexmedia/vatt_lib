/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Trakt w�r�d p�l");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t26.c","nw",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t24.c","sw",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description() 
{
    return "Trakt prowadzi z po�udniowego-zachodu na p�nocny-zach�d.\n";
}

string
dlugi_opis()
{
    string str="";

	str+="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
			"W miernie utwardzonej, grz�skiej powierzchni drogi swoje "+
			"miejsce znalaz�y liczne koleiny. ";
	str+="Na p�nocy majacz� wysokie mury otaczaj�ce miasto, z kt�rego "+
	"bij� w g�r� wysokie wie�e.";

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}