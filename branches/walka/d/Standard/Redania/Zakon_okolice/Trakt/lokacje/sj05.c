/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("w�ska, le�na �cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj04.c","e",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj06.c","sw",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z po�udniowego-zachodu na wsch�d.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Zwarta �ciana drzew li�ciasto iglastych przeszkadza podr�"+
             "�nemu w zboczeniu ze �cie�ki, a masywne i g�ste korony pr"+
             "zys�aniaj� widok nieba. Bia�y puch przykrywaj�cy ro�lin"+
             "y jest do�� gruby i skrzypi pod krokami, a gdzieniegdzie m"+
             "o�na zauwa�y� na nim drobne �lady ma�ych zwierz�t. Drz"+
             "ewa i krzewy pokryte �niegiem uginaj� si� pod jego ci�"+
             "arem, a gdzieniegdzie pod krzakiem mo�na zauwa�y� na tym "+
             "bia�ym puchu ma�e �lady zaj�cy. Oblodzona �cie�ka wije"+
             " si� falist� wst�g� ze wschodu na zach�d, a gdzie nie s"+
             "pojrze� dooko�a zalega bia�y puch. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Zwarta �ciana drzew li�ciasto iglastych przeszkadza podr�"+
             "�nemu w zboczeniu ze �cie�ki, a masywne i g�ste korony pr"+
             "zys�aniaj� widok nieba. Soczysta ziele�, i wszechobecne o"+
             "dg�osy zamieszkuj�cych ten las zwierz�t, powoduj� i� w�"+
             "dr�wka t� �cie�yn� jest naprawd� przyjemna. Las ze sw"+
             "oj� r�norodn� zwierzyn�, wype�niony jest dochodz�cym"+
             "i z niego pomrukami, �wierkaniami i szelestami. Le�na �ci"+
             "e�ka wije si� falist� wst�g� ze wschodu na zach�d, oto"+
             "czona zewsz�d natur�. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Zwarta �ciana drzew li�ciasto iglastych przeszkadza podr�"+
             "�nemu w zboczeniu ze �cie�ki, a masywne i g�ste korony pr"+
             "zys�aniaj� widok nieba. Soczysta ziele�, i wszechobecne o"+
             "dg�osy zamieszkuj�cych ten las zwierz�t, powoduj� i� w�"+
             "dr�wka t� �cie�yn� jest naprawd� przyjemna. Las ze sw"+
             "oj� r�norodn� zwierzyn�, wype�niony jest dochodz�cym"+
             "i z niego pomrukami, �wierkaniami i szelestami. Le�na �ci"+
             "e�ka wije si� falist� wst�g� ze wschodu na zach�d, oto"+
             "czona zewsz�d natur�. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Zwarta �ciana drzew li�ciasto iglastych przeszkadza podr�"+
             "�nemu w zboczeniu ze �cie�ki, a masywne i g�ste korony pr"+
             "zys�aniaj� widok nieba. Opad�e z drzew li�cie powoduj� "+
             "i� na pod�o�u utworzy� sie dywan szumi�cy i szeleszcz�"+
             "cy pod ka�dym krokiem podr�nego. Od czasu do czasu na tw"+
             "� g�ow� spada jaki� zrzucony li��. Le�na �cie�ka wi"+
             "je si� falist� wst�g� ze wschodu na zach�d, otoczona ze"+
             "wsz�d natur�. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}