/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("le�na �cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj10.c","e",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj12.c","sw",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z po�udniowego-zachodu na wsch�d.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="G�sto rosn�ce praktycznie ze wszystkich stron wysokie drze"+
             "wa przejmuj� groz�, a powykr�cane krzewy i pnie dodaj� n"+
             "a niesamowito�ci otoczenia. Wszystko wok� wprost rozbrzm"+
             "iewa odg�osami przyrody - szumem wody, �piewem ptak�w, "+
             "czy szelestem ro�lin. Oblodzona �cie�ka wije si� falist�"+
             " wst�g� ze wschodu na zach�d, a gdzie nie spojrze� dook"+
             "o�a zalega bia�y puch. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="G�sto rosn�ce praktycznie ze wszystkich stron wysokie drze"+
             "wa przejmuj� groz�, a powykr�cane krzewy i pnie dodaj� n"+
             "a niesamowito�ci otoczenia. Wszystko wok� wprost rozbrzm"+
             "iewa odg�osami przyrody - szumem wody, �piewem ptak�w, "+
             "czy szelestem ro�lin. Le�na �cie�ka wije si� falist� w"+
             "st�g� ze wschodu na zach�d, otoczona zewsz�d natur�. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="G�sto rosn�ce praktycznie ze wszystkich stron wysokie drze"+
             "wa przejmuj� groz�, a powykr�cane krzewy i pnie dodaj� n"+
             "a niesamowito�ci otoczenia. Wszystko wok� wprost rozbrzm"+
             "iewa odg�osami przyrody - szumem wody, �piewem ptak�w, "+
             "czy szelestem ro�lin. Le�na �cie�ka wije si� falist� w"+
             "st�g� ze wschodu na zach�d, otoczona zewsz�d natur�. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="G�sto rosn�ce praktycznie ze wszystkich stron wysokie drze"+
             "wa przejmuj� groz�, a powykr�cane krzewy i pnie dodaj� n"+
             "a niesamowito�ci otoczenia. Wszystko wok� wprost rozbrzm"+
             "iewa odg�osami przyrody - szumem wody, �piewem ptak�w, "+
             "czy szelestem ro�lin. Le�na �cie�ka wije si� falist� w"+
             "st�g� ze wschodu na zach�d, otoczona zewsz�d natur�. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}