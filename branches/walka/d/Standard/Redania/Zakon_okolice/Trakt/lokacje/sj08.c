/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("w�ska, le�na �cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj07.c","ne",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj09.c","w",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z zachodu na p�nocny-wsch�d.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Drzewa i okolica spowite s�"+
             " bia�ym puchem, a mocny mr�z powoduje, ze �nieg skrzypi "+
             "pod butami. Na po�udniu rozci�gaj� si� niezmierzone po�"+
             "acie p�l, kt�re przykryte bia�ym �niegiem wygl�daj� ja"+
             "k tafla ogromnego jeziora, na kt�rym panuje ca�kowita cisz"+
             "a i spok�j. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Wszystko dooko�a jest przy"+
             "jemnie zazielenione, a cisz� panuj�c� w lesie od czasu do"+
             " czasu przerywa jaki� �wiergot ptaka. Na po�udniu rozci�"+
             "gaj� si� niezmierzone po�acie p�l, kt�re b�d�c pi�kn"+
             "ie zazielenione, s� prawdziwym rajem dla okolicznych ptak�"+
             "w i zwierz�t, kt�rych piski, skrzeki i mruczenia s�ycha�"+
             " nawet z takiej odleg�o�ci. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Wszystko dooko�a jest przy"+
             "jemnie zazielenione, a cisz� panuj�c� w lesie od czasu do"+
             " czasu przerywa jaki� �wiergot ptaka. Na po�udniu rozci�"+
             "gaj� si� niezmierzone po�acie p�l, kt�re b�d�c pi�kn"+
             "ie zazielenione, s� prawdziwym rajem dla okolicznych ptak�"+
             "w i zwierz�t, kt�rych piski, skrzeki i mruczenia s�ycha�"+
             " nawet z takiej odleg�o�ci. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Suche opadni�te na ziemi�"+
             " li�cie szeleszcz� pod krokami, zwierz�ta szykuj� si� d"+
             "o zimowego snu. Na po�udniu rozci�gaj� si� niezmierzone "+
             "po�acie p�l, nad kt�rymi snuje si� sie lekka mg�a nadaj"+
             "�ca im do�� niepokoj�cy wygl�d, a z otaczaj�cych ci� "+
             "drzew powoli spadaj� r�nokolorowe li�cie, kt�re po swo"+
             "bodnym locie, delikatnie opadaj� na ziemi�. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}