/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("�cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "s_zakonu01.c","w",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "s_zakonu03.c","ne",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�ka ci�gnie si� z zachodu na p�nocny-wsch�d.\n";
}


string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Zwarta �ciana drzew li�ciasto iglastych przeszkadza podr�"+
             "�nemu w zboczeniu ze �cie�ki, a masywne i g�ste korony pr"+
             "zys�aniaj� widok nieba. �cie�ka wydeptana przez stopy lu"+
             "dzkie jest nie szersza ni� na naprawd� malutki w�z. Bia�y puch przykrywaj�cy ro"+
             "�liny jest do�� gruby i skrzypi pod krokami, a gdzieniegd"+
             "zie mo�na zauwa�y� na nim drobne �lady ma�ych zwierz�t"+
             ".";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Zwarta �ciana drzew li�ciasto iglastych przeszkadza podr�"+
             "�nemu w zboczeniu ze �cie�ki, a masywne i g�ste korony pr"+
             "zys�aniaj� widok nieba. �cie�ka wydeptana przez stopy lu"+
             "dzkie jest nie szersza ni� na naprawd� malutki w�z. Soczysta ziele�, i wszechobe"+
             "cne odg�osy zamieszkuj�cych ten las zwierz�t, powoduj� i"+
             "� w�dr�wka t� �cie�yn� jest naprawd� przyjemna.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Zwarta �ciana drzew li�ciasto iglastych przeszkadza podr�"+
             "�nemu w zboczeniu ze �cie�ki, a masywne i g�ste korony pr"+
             "zys�aniaj� widok nieba. �cie�ka wydeptana przez stopy lu"+
             "dzkie jest nie szersza ni� na naprawd� malutki w�z. Soczysta ziele�, i wszechobe"+
             "cne odg�osy zamieszkuj�cych ten las zwierz�t, powoduj� i"+
             "� w�dr�wka t� �cie�yn� jest naprawd� przyjemna.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Zwarta �ciana drzew li�ciasto iglastych przeszkadza podr�"+
             "�nemu w zboczeniu ze �cie�ki, a masywne i g�ste korony pr"+
             "zys�aniaj� widok nieba. �cie�ka wydeptana przez stopy lu"+
             "dzkie jest nie szersza ni� na naprawd� malutki w�z. Opad�e z drzew li�cie powod"+
             "uj� i� na pod�o�u utworzy� sie dywan szumi�cy i szeles"+
             "zcz�cy pod ka�dym krokiem podr�nego.";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}