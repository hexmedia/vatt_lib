/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"


void create_trakt() 
{
    set_short("le�na �cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t08.c","ne",0,TRAKT_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj02.c","w",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�ka ci�gnie si� na zachodzie, za� na p�nocnym-wschodzie "+
		"mo�na wej�� na trakt.\n";
}

//For dummies
wejdz(string str)
{
	notify_fail("Wejd� gdzie?\n");
	if(str == "na trakt")
	{
		TP->command("ne");
		return 1;
	}
	return 0;
}
init()
{
	::init();
	add_action(wejdz,"wejd�");
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Na po�udniu rozci�gaj� si� niezmierzone po�acie p�l, k"+
             "t�re przykryte bia�ym �niegiem wygl�daj� jak tafla ogro"+
             "mnego jeziora, na kt�rym panuje ca�kowita cisza i spok�j."+
             " G�sto rosn�ce praktycznie ze wszystkich stron wysokie drz"+
             "ewa przejmuj� groz�, a powykr�cane krzewy i pnie dodaj� "+
             "na niesamowito�ci otoczenia. W�ska �cie�ka na kt�rej z "+
             "ledwo�ci� zmie�ci�by si� malutki w�z prowadzi na zach�"+
             "d, w g��b g�stego lasu. Zwarta �ciana drzew li�ciasto "+
             "iglastych przeszkadza podr�nemu w zboczeniu ze �cie�ki,"+
             " a masywne i g�ste korony przys�aniaj� widok nieba. Oblodz"+
             "ona �cie�ka wije si� falist� wst�g� ze wschodu na zach"+
             "�d, a gdzie nie spojrze� dooko�a zalega bia�y puch. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Na po�udniu rozci�gaj� si� niezmierzone po�acie p�l, k"+
             "t�re b�d�c pi�knie zazielenione, s� prawdziwym rajem dl"+
             "a okolicznych ptak�w i zwierz�t, kt�rych piski, skrzeki i"+
             " mruczenia s�ycha� nawet z takiej odleg�o�ci. G�sto ros"+
             "n�ce praktycznie ze wszystkich stron wysokie drzewa przejmu"+
             "j� groz�, a powykr�cane krzewy i pnie dodaj� na niesamow"+
             "ito�ci otoczenia. W�ska �cie�ka na kt�rej z ledwo�ci�"+
             " zmie�ci�by si� malutki w�z prowadzi na zach�d, w g��"+
             "b g�stego lasu. Zwarta �ciana drzew li�ciasto iglastych p"+
             "rzeszkadza podr�nemu w zboczeniu ze �cie�ki, a masywne "+
             "i g�ste korony przys�aniaj� widok nieba. Le�na �cie�ka "+
             "wije si� falist� wst�g� ze wschodu na zach�d, otoczona "+
             "zewsz�d natur�. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Na po�udniu rozci�gaj� si� niezmierzone po�acie p�l, k"+
             "t�re b�d�c pi�knie zazielenione, s� prawdziwym rajem dl"+
             "a okolicznych ptak�w i zwierz�t, kt�rych piski, skrzeki i"+
             " mruczenia s�ycha� nawet z takiej odleg�o�ci. G�sto ros"+
             "n�ce praktycznie ze wszystkich stron wysokie drzewa przejmu"+
             "j� groz�, a powykr�cane krzewy i pnie dodaj� na niesamow"+
             "ito�ci otoczenia. W�ska �cie�ka na kt�rej z ledwo�ci�"+
             " zmie�ci�by si� malutki w�z prowadzi na zach�d, w g��"+
             "b g�stego lasu. Zwarta �ciana drzew li�ciasto iglastych p"+
             "rzeszkadza podr�nemu w zboczeniu ze �cie�ki, a masywne "+
             "i g�ste korony przys�aniaj� widok nieba. Le�na �cie�ka "+
             "wije si� falist� wst�g� ze wschodu na zach�d, otoczona "+
             "zewsz�d natur�. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Na po�udniu rozci�gaj� si� niezmierzone po�acie p�l, n"+
             "ad kt�rymi snuje si� sie lekka mg�a nadaj�ca im do�� n"+
             "iepokoj�cy wygl�d, a z otaczaj�cych ci� drzew powoli spa"+
             "daj� r�nokolorowe li�cie, kt�re po swobodnym locie, de"+
             "likatnie opadaj� na ziemi�. G�sto rosn�ce praktycznie ze"+
             " wszystkich stron wysokie drzewa przejmuj� groz�, a powykr"+
             "�cane krzewy i pnie dodaj� na niesamowito�ci otoczenia. W"+
             "�ska �cie�ka na kt�rej z ledwo�ci� zmie�ci�by si� m"+
             "alutki w�z prowadzi na zach�d, w g��b g�stego lasu. Zwa"+
             "rta �ciana drzew li�ciasto iglastych przeszkadza podr�n"+
             "emu w zboczeniu ze �cie�ki, a masywne i g�ste korony przys"+
             "�aniaj� widok nieba. Le�na �cie�ka wije si� falist� w"+
             "st�g� ze wschodu na zach�d, otoczona zewsz�d natur�. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}

string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}