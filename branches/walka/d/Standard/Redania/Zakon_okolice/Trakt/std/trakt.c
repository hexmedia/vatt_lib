
/* std traktu mi�dzy Rinde, a Tretogorem + �cie�ka do jeziorka, 
	czyli ZAKON OKOLICE.
   Vera */  
inherit "/std/room";

#include "../dir.h"
#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>
#include <pogoda.h>
#include <filter_funs.h>
#include <macros.h>
#include <sit.h>

string opis_polmroku();
string dlugi_opis();

void
create_trakt()
{
}

string
event_traktu()
{
    return "";
}

string
evencik()
{
	string str="";
	switch(pora_roku())
	{
		case MT_LATO:
		case MT_WIOSNA:
		case MT_JESIEN:
			switch(random(16))
			{
				case 0: str="Jaki� drobny py�ek wpad� ci do oka.\n";
						break;
				case 1: str="Unosz�cy si� dooko�a piach jest do�� uci��liwy.\n";
						break;
				case 2: str="Gdzie� nad tob� przelecia� jaki� ptak, g�o�no "+
						"informuj�c o swojej obecno�ci.\n";
						break;
				case 3: str="W powietrzu unosi si� przyjemny zapach "+
						"kwiat�w dochodz�cy od strony ��ki.\n";
						break;
				case 4: str="Ta�cz�cy dooko�a wiatr, szumi cicho pomi�dzy trawami.\n";
						break;
				case 5: str="Drobny py�ek wpad� ci do oka wywo�uj�c "+
						"nieprzyjemne pieczenie.\n"; break;
				case 6: str="Unosz�cy si� dooko�a kurz jest do�� uci��liwy.\n";
						break;
				case 7: str="Trawy szumi� cicho.\n"; break;
				case 8: str="Z daleka dotar� do twych uszu ptasi trel.\n"; break;
				case 9: str="Nad twoj� g�ow� przelecia�o stado g�si.\n"; break;
				case 10: str="Trawa leniwie zafalowa�a pod wp�ywem wiatru.\n";
						break;
				case 11: str="Co� zapiszcza�o w trawie.\n"; break;
				case 12:
					if(TO->jest_dzien() && random(2))
						str="Stado wr�bli poderwa�o si� nagle do lotu.\n";
					else if(TO->jest_dzien())
						str="�agodny ptasi trel rozleg� si� gdzie� w pobli�u.\n";
					else
						str="Gdzie� w oddali s�ycha� wycie wilka.\n";
					break;
				case 13:
					if(TO->jest_dzien())
						str="Jaki� w�z mija ci� powoli jad�c w kierunku "+
					"po�udniowym.\n";
					else
						str="Od strony lasu s�ycha� ciche pohukiwanie.\n";
				case 14:
					if(TO->jest_dzien())
					{
						if(random(2))
							str="Przez drog� przemkn�a mysz.\n";
						else
							str="Nagle przez drog� przebieg� lis.\n";
					}
					else
						str="Przez drog� szybko przebieg�o jakie� zwierz�.\n";
					break;
				case 15:
					str="Potkn"+TP->koncowka("��e�","�a�")+
					" si� o jaki� konar, z trudem utrzymuj�c r�wnowag�.\n";
					break;
			}
			break;
		case MT_ZIMA:
			switch(random(12))
			{
				case 0: str="Gdzie� nad tob� przelecia� jaki� ptak, g�o�no "+
						"informuj�c o swojej obecno�ci.\n";
						break;
				case 1: str="Ta�cz�cy dooko�a wiatr, szumi cicho pomi�dzy trawami.\n";
						break;
				case 2: str="Trawy szumi� cicho.\n"; break;
				case 3: str="Co� zaszele�ci�o w trawie.\n"; break;
				case 4:
					//po�lizg randomowego livinga
					object *liv = FILTER_LIVE(all_inventory(TO));

					if(!sizeof(liv))
						return "";

					object delikwent = liv[random(sizeof(liv))];
					
					if(delikwent->query_prop(SIT_SIEDZACY))
					{
					}
					else
					{
					set_this_player(delikwent);
					write("Po�lizg na oblodzonej "+
					"powierzchni drogi, nieomal spowodowa� tw�j upadek!\n");
					 saybb(QCIMIE(TP,PL_MIA)+" po�lizgn"+
					TP->koncowka("��","�a","�o")+" si� na oblodzonej "+
					"powierzchni drogi i nieomal upad�"+
					TP->koncowka("","a","o")+" na ziemi�.");
					}
					break;
				case 5:
					if(CZY_JEST_SNIEG(TO))
						str="Ciemny �wistak stan�� s�upka na �niegu "+
						"by zaraz da� nurka w bia�y puch.\n";
					break;
				case 6:
					if(TO->jest_dzien())
						str="Stado wr�bli poderwa�o si� nagle do lotu.\n";
					else
						str="Gdzie� w oddali s�ycha� wycie wilka.\n";
					break;
				case 7:
					if(TO->jest_dzien())
						str="Jaki� w�z mija ci� powoli jad�c w kierunku "+
					"po�udniowym.\n";
					else
						str="Od strony lasu s�ycha� ciche pohukiwanie.\n";
					break;
				case 8:
					if(TO->jest_dzien())
						str="Nagle przez drog� przebieg� lis.\n";
					else
						str="Przez drog� szybko przebieg�o jakie� zwierz�.\n";
					break;
				case 9:
					str="Potkn"+TP->koncowka("��e�","�a�")+
					" si� o jaki� konar, z trudem utrzymuj�c r�wnowag�.\n";
					break;

				case 10..11: str=""; break;
			}
			
	}
	return str;
}

nomask void
create_room()
{
    set_long("@@dlugi_opis@@"); 
    set_polmrok_long("@@opis_polmroku@@");


    add_event("@@event_traktu:"+file_name(TO)+"@@");
	add_prop(ROOM_I_TYPE, ROOM_TRACT);
    set_event_time(410.0);
	add_event("@@evencik:"+file_name(TO)+"@@");

    add_sit("na ziemi","na ziemi","z ziemi",0);
	add_prop(ROOM_I_WSP_Y, 2); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, 0); //Rinde jest p�pkiem wszech�wiata ;)
    					//A Zakon_okolice na p�noc od niego...	
	
    dodaj_ziolo("arcydziegiel.c");
	dodaj_ziolo("babka_lancetowata.c");
	dodaj_ziolo(({"czarny_bez-lisc.c",
				"czarny_bez-kwiaty.c",
				"czarny_bez-owoce.c"}));
    dodaj_ziolo(({"krwawnik_ziele.c",
                "krwawnik_kwiatostan.c"}));
	dodaj_ziolo("piolun.c");
	
    create_trakt();
}


string
dlugi_opis()
{
	return "Ups! Zg�o� b��d!\n";
}

string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na trakcie.\n";
    else
        str="Nad traktem zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}

