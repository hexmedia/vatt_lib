/* Autor: Duana
  Opis : Tinardan, Duana
  Data : 02.05.2007
*/

inherit "/std/room";

#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <pl.h>
#include <options.h>
#include <formulas.h>
#include "dir.h"


void
create_room()
{
    set_short("Sypialnia");
    set_long("D^lugi i w^aski pok^oj jest nadzwyczaj ciemny, przy czym szyb^e okna pokrywa gruba wartstwa brudu. Z belkowanego sufitu, "+
        "niczym dziwaczne ozdoby zwisaj^a osypane kurzem, zeschni^ete "+
        "zio^la i warkocze czosnku, wype^lniaj^ac pomieszczenie szczeg^olnym "+
        "zapachem. @@sprzety@@\n");

    add_prop(ROOM_I_INSIDE,1);
    add_prop(ROOM_I_LIGHT,2);


    add_item("okno","Zwyk^le okno otoczone drewnian^a framug^a przepuszcza "+
        "niewiele ^swiat^la ze wzgl^edu na przydymion^a i wyj^atkowo brudn^a "+
        "szyb^e.\n");

    add_item(({"czosnek","warkocz czosnku"}),"D^lugi warkocz czosnku pokryty "+
        "jest r^o^zowo-bia^l^a ^lupin^a, miejscami od niego odstaj^ac^a. Intensywna "+
        "wo^n warzywa wyczuwalna jest w ka^zdym k^acie pokoju.\n");
    add_item(({"czosnek","warkocze czosnku"}),"D^lugie warkocze czosnku pokryte "+
        "s^a r^o^zowo-bia^l^a ^lupin^a, miejscami od nich odstaj^ac^a. Intensywna "+
        "wo^n warzyw wyczuwalna jest w ka^zdym k^acie pokoju.\n");
    add_item("zio^la","R^o^znorodne gatunki zi^o^l przywi^azano w k^epach sznurkiem "+
        "do sufitu, by mog^ly spokojnie schn^a^c. Ich zapach miesza si^e z woni^a "+
        "czosnku, dra^zni^ac nos osoby przebywaj^acej w pomieszczeniu.\n");

    set_event_time(120.0);
    add_event("Drzwi poskrzypuj^a cicho w przeci^agu.", 30);
    add_event("Zio^la ko^lysz^a si^e sypi^ac z g^ory kurzem i zeschni^etymi "+
                "listkami.", 20);
    add_event("Z g^l^ownej sali dochodz^a ci^e ^smiechy i g^lo^sne rozmowy.", 30);
    add_event("Co^s cicho szura w k^acie.", 40);


    add_object(BIALY_MOST_PRZEDMIOTY + "lozko.c");  
    dodaj_rzecz_niewyswietlana("stare drewniane ^l^o^zko", 1);
    add_object(BIALY_MOST_PRZEDMIOTY + "szafka.c");  
    dodaj_rzecz_niewyswietlana("ma^la drewniana szafka", 1);
    add_object(BIALY_MOST_PRZEDMIOTY + "dywan.c");  
    dodaj_rzecz_niewyswietlana("niedu^zy okr^ag^ly dywan", 1);
    add_object(BIALY_MOST_PRZEDMIOTY + "miednica.c");  
    dodaj_rzecz_niewyswietlana("jajowata sosnowa miednica", 1);
    add_object(BIALY_MOST_LOKACJE_DRZWI + "z_sypialni_karczmarza.c");  



}

string
sprzety()
{
  string str="";
  if(jest_rzecz_w_sublokacji(0,"stare drewniane ^l^o^zko")&& 
jest_rzecz_w_sublokacji(0,"ma^la drewniana szafka") && 
jest_rzecz_w_sublokacji(0,"niedu^zy okr^ag^ly dywan") &&
jest_rzecz_w_sublokacji(0,"jajowata sosnowa miednica"))
 str="Gdzie^s w k^acie stoi stare, drewniane ^l^o^zko, na kt^orym sm^etnie zwisa "+
      "przybrudzona kapa. Ustawionej z boku szafce nie domykaj^a si^e "+
      "drzwiczki i niekiedy poskrzypuj^a ^za^lo^snie w przeci^agu. Na pod^log^e "+
      "rzucony jest okr^ag^ly, pleciony dywanik o dawno ju^z wytartych "+
      "kolorach i postrz^epionych brzegach. Tu^z przy ^l^o^zku stoi nape^lniona wod^a do po^lowy drewniana miednica, "+
      "na pewno pami^etaj^aca du^zo "+
      "lepsze czasy.";
else if(jest_rzecz_w_sublokacji(0,"niedu^zy okr^ag^ly dywan") && 
jest_rzecz_w_sublokacji(0,"ma^la drewniana szafka") && 
jest_rzecz_w_sublokacji(0,"jajowata sosnowa miednica"))
	str="Gdzie^s w k^acie wida^c wytart^a, matow^a pod^log^e, kt^ora najwyra^xniej "+
    "sugeruje, ^ze co^s tu wcze^sniej sta^lo. Ustawionej z boku szafce nie "+
    "domykaj^a si^e drzwiczki i niekiedy poskrzypuj^a ^za^lo^snie w przeci^agu. "+
    "Na pod^log^e rzucony jest okr^ag^ly, pleciony dywanik o dawno ju^z "+
    "wytartych kolorach i postrz^epionych brzegach. Tu^z przy ^l^o^zku stoi nape^lniona wod^a do po^lowy drewniana miednica, "+
      "na pewno pami^etaj^aca du^zo "+
      "lepsze czasy.";
else if(jest_rzecz_w_sublokacji(0,"stare drewniane ^l^o^zko") && 
jest_rzecz_w_sublokacji(0,"niedu^zy okr^ag^ly dywan") && 
jest_rzecz_w_sublokacji(0,"jajowata sosnowa miednica"))
	str="Gdzie^s w k^acie stoi stare, drewniane ^l^o^zko, na kt^orym sm^etnie "+
    "zwisa przybrudzona kapa. Na pod^log^e rzucony jest okr^ag^ly, pleciony "+
    "dywanik, o dawno ju^z wytartych kolorach i postrz^epionych brzegach. "+
    "Tu^z przy ^l^o^zku stoi nape^lniona wod^a do po^lowy drewniana miednica, "+
      "na pewno pami^etaj^aca du^zo "+
      "lepsze czasy.";
else if(jest_rzecz_w_sublokacji(0,"stare drewniane ^l^o^zko") && 
jest_rzecz_w_sublokacji(0,"ma^la drewniana szafka") && 
jest_rzecz_w_sublokacji(0,"jajowata sosnowa miednica"))
	str="Gdzie^s w k^acie stoi stare, drewniane ^l^o^zko, na kt^orym sm^etnie "+
    "zwisa przybrudzona kapa. Ustawionej z boku szafce nie domykaj^a si^e "+
    "drzwiczki i niekiedy poskrzypuj^a ^za^lo^snie w przeci^agu. Drewniana "+
    "pod^loga niemal^ze zupe^lnie pokryta jest kurzem, pozostawiaj^ac na ^srodku "+
    "wytarte deski, zapewne ^slad po dywanie. Tu^z przy ^l^o^zku stoi nape^lniona wod^a do po^lowy drewniana miednica, "+
      "na pewno pami^etaj^aca du^zo "+
      "lepsze czasy.";
else if(jest_rzecz_w_sublokacji(0,"stare drewniane ^l^o^zko") && 
jest_rzecz_w_sublokacji(0,"ma^la drewniana szafka") && 
jest_rzecz_w_sublokacji(0,"niedu^zy okr^ag^ly dywan"))
	str="Gdzie^s w k^acie stoi stare, drewniane ^l^o^zko, na kt^orym sm^etnie "+
    "zwisa przybrudzona kapa. Ustawionej z boku szafce nie domykaj^a si^e "+
    "drzwiczki i niekiedy poskrzypuj^a ^za^lo^snie w przeci^agu. Na pod^log^e "+
    "rzucony jest okr^ag^ly, pleciony dywanik o dawno ju^z wytartych kolorach "+
    "i postrz^epionych brzegach.";
else if(jest_rzecz_w_sublokacji(0,"niedu^zy okr^ag^ly dywan") && 
jest_rzecz_w_sublokacji(0,"jajowata sosnowa miednica"))
	str="Gdzie^s w k^acie wida^c wytart^a, matow^a pod^log^e, kt^ora najwyra^xniej "+
    "sugeruje, ^ze co^s tu wcze^sniej sta^lo. Na pod^log^e rzucony jest okr^ag^ly, "+
    "pleciony dywanik o dawno ju^z wytartych kolorach i postrz^epionych "+
    "brzegach. Tu^z przy ^l^o^zku stoi nape^lniona wod^a do po^lowy drewniana miednica, "+
      "na pewno pami^etaj^aca du^zo "+
      "lepsze czasy.";
else if(jest_rzecz_w_sublokacji(0,"ma^la drewniana szafka") && 
jest_rzecz_w_sublokacji(0,"jajowata sosnowa miednica"))
	str="Gdzie^s w k^acie wida^c wytart^a, matow^a pod^log^e, kt^ora najwyra^xniej "+
    "sugeruje, ^ze co^s tu wcze^sniej sta^lo. Ustawionej z boku szafce nie "+
    "domykaj^a si^e drzwiczki i niekiedy poskrzypuj^a ^za^lo^snie w przeci^agu. "+
    "Drewniana pod^loga niemal^ze zupe^lnie pokryta jest kurzem, pozostawiaj^ac "+
    "na ^srodku wytarte deski, zapewne ^slad po dywanie. Tu^z przy ^l^o^zku stoi nape^lniona wod^a do po^lowy drewniana miednica, "+
      "na pewno pami^etaj^aca du^zo "+
      "lepsze czasy.";
else if(jest_rzecz_w_sublokacji(0,"ma^la drewniana szafka") && 
jest_rzecz_w_sublokacji(0,"niedu^zy okr^ag^ly dywan"))
	str="Gdzie^s w k^acie wida^c wytart^a, matow^a pod^log^e, kt^ora najwyra^xniej "+
    "sugeruje, ^ze co^s tu wcze^sniej sta^lo. Szafce, ustawionej z boku, nie "+
    "domykaj^a si^e drzwiczki i niekiedy poskrzypuj^a ^za^lo^snie w przeci^agu. "+
    "Na pod^log^e rzucony jest okr^ag^ly, pleciony dywanik o dawno ju^z "+
    "wytartych kolorach i postrz^epionych brzegach.";
else if(jest_rzecz_w_sublokacji(0,"stare drewniane ^l^o^zko") && 
jest_rzecz_w_sublokacji(0,"jajowata sosnowa miednica"))
	str="Gdzie^s w k^acie wida^c wytart^a, matow^a pod^log^e, kt^ora najwyra^xniej "+
    "sugeruje, ^ze co^s tu wcze^sniej sta^lo. Drewniana pod^loga niemal^ze "+
    "zupe^lnie pokryta jest kurzem, pozostawiaj^ac na ^srodku wytarte deski, "+
    "zapewne ^slad po dywanie. Tu^z przy ^l^o^zku stoi nape^lniona wod^a do po^lowy drewniana miednica, "+
      "na pewno pami^etaj^aca du^zo "+
      "lepsze czasy.";
else if(jest_rzecz_w_sublokacji(0,"stare drewniane ^l^o^zko") && 
jest_rzecz_w_sublokacji(0,"niedu^zy okr^ag^ly dywan"))
	str="Gdzie^s w k^acie stoi stare, drewniane ^l^o^zko, na kt^orym sm^etnie "+
    "zwisa przybrudzona kapa. Na pod^log^e rzucony jest okr^ag^ly, pleciony "+
    "dywanik o dawno ju^z wytartych kolorach i postrz^epionych brzegach.";
else if(jest_rzecz_w_sublokacji(0,"stare drewniane ^l^o^zko") && 
jest_rzecz_w_sublokacji(0,"ma^la drewniana szafka"))
	str="Gdzie^s w k^acie stoi stare, drewniane ^l^o^zko, na kt^orym sm^etnie "+
    "zwisa przybrudzona kapa. Szafce, ustawionej z boku, nie domykaj^a si^e "+
    "drzwiczki i niekiedy poskrzypuj^a ^za^lo^snie w przeci^agu. Drewniana "+
    "pod^loga niemal^ze zupe^lnie pokryta jest kurzem, pozostawiaj^ac na ^srodku "+
    "wytarte deski, zapewne ^slad po dywanie.";
else if(jest_rzecz_w_sublokacji(0,"jajowata sosnowa miednica"))
	str="Gdzie^s w k^acie wida^c wytart^a, matow^a pod^log^e, kt^ora najwyra^xniej "+
    "sugeruje, ^ze co^s tu wcze^sniej sta^lo. Drewniana pod^loga niemal^ze "+
    "zupe^lnie pokryta jest kurzem, pozostawiaj^ac na ^srodku wytarte deski, "+
    "zapewne ^slad po dywanie. Tu^z przy ^l^o^zku stoi nape^lniona wod^a do po^lowy drewniana miednica, "+
      "na pewno pami^etaj^aca du^zo "+
      "lepsze czasy.";
else if(jest_rzecz_w_sublokacji(0,"niedu^zy okr^ag^ly dywan")) 
	str="Gdzie^s w k^acie wida^c wytart^a, matow^a pod^log^e, kt^ora najwyra^xniej "+
    "sugeruje, ^ze co^s tu wcze^sniej sta^lo. Na pod^log^e rzucony jest okr^ag^ly, "+
    "pleciony dywanik o dawno ju^z wytartych kolorach i postrz^epionych "+
    "brzegach.";
else if(jest_rzecz_w_sublokacji(0,"ma^la drewniana szafka"))
	str="Gdzie^s w k^acie wida^c wytart^a, matow^a pod^log^e, kt^ora najwyra^xniej "+
    "sugeruje, ^ze co^s tu wcze^sniej sta^lo. Ustawionej z boku szafce nie "+
    "domykaj^a si^e drzwiczki i niekiedy poskrzypuj^a ^za^lo^snie w przeci^agu. "+
    "Drewniana pod^loga niemal^ze zupe^lnie pokryta jest kurzem, pozostawiaj^ac "+
    "na ^srodku wytarte deski, zapewne ^slad po dywanie.";
else if(jest_rzecz_w_sublokacji(0,"stare drewniane ^l^o^zko"))
	str="Gdzie^s w k^acie stoi stare, drewniane ^l^o^zko, na kt^orym sm^etnie "+
    "zwisa przybrudzona kapa. Drewniana pod^loga niemal^ze zupe^lnie pokryta "+
    "jest kurzem, pozostawiaj^ac na ^srodku wytarte deski, zapewne ^slad po "+
    "dywanie.";

else if(jest_rzecz_w_sublokacji(0,""))
str="Gdzie^s w k^acie wida^c wytart^a, matow^a pod^log^e, kt^ora "+
    "najwyra^xniej sugeruje, ^ze co^s tu wcze^sniej sta^lo. Drewniana "+
    "pod^loga niemal^ze zupe^lnie pokryta jest kurzem, pozostawiaj^ac na "+
    "^srodku wytarte deski, zapewne ^slad po dywanie.\n";
    return str;
}
public string
exits_description() 
{
    return "Jedyne wyj^scie st^ad prowadzi przez drzwi do sali g^l^ownej.\n";
}
