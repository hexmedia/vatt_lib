
/*
 * Karczma w BM
 * Opis:  Edrain
 * Autor: Faeve
 * Data:  25.05.2007
 */

inherit "/std/room";
inherit "lib/peek";
inherit "/lib/pub_new";

#define SIEDZACY "_siedzacy"
#include "dir.h"
#include <macros.h>
#include <filter_funs.h>
#include <ss_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <options.h>
#include <formulas.h>

int zasiedzenie;

void
create_room()
{
    set_short("Sala g^l^owna");
    set_long("@@dlugi_opis@@");
    
    add_prop(ROOM_I_INSIDE,1);
	    
    add_item(({"^zyrandol","lamp^e"}),
		"Okr^ag^ly ^zyrandol o ^srednicy d^lugo^sci ramienia ros^lego "+
		"m^e^zczyzny, zrobiony jest ze starego ^zelaza, sczernia^lego od "+
		"panosz^acego si^e wsz^edzie dymu i p^lomieni ^swiec, kt^ore "+
		"opaliwszy si^e przypala^ly metal. Na wystaj^acych szpikulcach "+
		"rozmieszczonych do^s^c niedaleko siebie osadzone s^a ^swiece, ktore "+
		"nieustannie pala sie jasnym p^lomieniem, pr^obuj^ac o^swietli^c "+
		"gospod^e pogr^a^zon^a w p^o^lmroku, jednak tylko znikome ilo^sci "+
		"dawanego przez knoty swiat^la dociera do ziemi. ^Zyrandol chwieje "+
		"si^e na boki przy ka^zdym st^apni^eciu na podlog^e karczmy, przy "+
		"mocniejszych wstrz^asach wyrzucaj^ac na boki fontann^e gor^acego "+
		"wosku.\n");

		 
//   add_peek("przez okno", "lokacja.c"); FIXME!!!! DA SI^E WYSKOCZY^C?


   add_npc(BIALY_MOST_NPC +"karczmarz_bialy_most.c"); 

   add_object(BIALY_MOST_LOKACJE_DRZWI +"do_lazni.c");
   add_object(BIALY_MOST_LOKACJE_DRZWI +"do_sypialni_karczmarza.c");
   add_object(BIALY_MOST_LOKACJE_DRZWI +"do_skladziku.c");
   add_exit(BIALY_MOST_LOKACJE + "sien.c","e",0,1,0);
   add_exit(BIALY_MOST_LOKACJE + "strych.c",({({"u","g^ora",
        "schody","strych"}), "na g^or^e"}),0,1,0);

   add_object(BIALY_MOST_PRZEDMIOTY + "stol_zajazd.c", 2);
   add_object(BIALY_MOST_PRZEDMIOTY + "menu.c");
   dodaj_rzecz_niewyswietlana("mocny drewniany st^o^l",2);


   add_object(BIALY_MOST_PRZEDMIOTY + "lawa_zajazd.c", 4);
   dodaj_rzecz_niewyswietlana("pot^e^zna d^luga ^lawa",4);


    add_food(({"kasza","kasza ze skwarkami"}), 0, "kaszy ze skwarkami. Mimo "+
        "i^z wygl^ada nieszczeg^olnie - szara ma^x upstrzona kilkoma "+
        "czerwonawymi kulkami - pachnie wy^smienicie. Wida^c, ^ze karczmarz "+
        "nie ^za^lowa^l s^loniny i skraja^l grube i t^luste p^laty. Kasza, "+
        "pulchna i nap^ecznia^la suto okraszona jest te^z t^luszczykiem, "+
        "ale wyj^atkowo ^swie^zym i wygl^adaj^acym na ca^lkiem dobry. Co "+
        "prawda micha wype^lniona jest po brzegi, ale nie jest to wybitnie "+
        "du^za miska, cho^c ^ladna i prawie w og^ole niepoobt^lukiwana. "+
        "Widocznie karczmarz woli przyoszcz^edzi^c na ilo^sci, jako^s^c "+
        "jednak pozostawiaj^ac najwy^zsz^a", 
        450,45,9,"miska","kaszy","Jest to "+
        "prosta, czysta miska.", 200);

    add_food("polewka", ({"rybny","rybni"}), "polewki rybnej. Szarawa, "+
        "m^etna zupa silnie pachnie rybami - co w tym wypadku ca^lkiem "+
        "dobrze o niej ^swiadczy. Cho^c sama w sobie jest do^s^c rzadka, "+
        "mo^zna wy^lowi^c z niej grube kawa^ly marchwi i pietruszki, a "+
        "tak^ze rybie ^lby, kt^orych pokryte bielmem oczy bezmy^slnie "+
        "si^e w ciebie wpatruj^a.", 
        500,45,11,"miska","polewki","Jest to "+
        "prosta, czysta miska.", 200);

    add_food(({"kurczak","kurczak z dodatkami"}), 0, "kurczaka z dodatkami. "+
        "Sk^or^e ma ciemnobr^azow^a, spieczon^a, lekko skwiercz^ac^a. "+
        "Ociekaj^acy t^luszcz apetycznie po^lyskuje na jego korpusie. "+
        "Wok^o^l u^lo^zone s^a we wdzi^eczny wianuszek grube, z^lociste i "+
        "mocno zarumienione talarki pieczonych ziemniak^ow, pokryte "+
        "pierzynk^a aromatycznego kopru, posiekanego na nier^owne, drobne "+
        "cz^astki. Wszystko sk^apane dodatkowo w g^estym, kremowym, "+
        "^smietanowo-grzybowym sosie, w kt^orym p^lywaj^a leniwie duszone "+
        "kawa^lki pieczarek.", 
        900,45,80,"polmisek","kurczaka","Jest to "+
        "prosty, czysty p^o^lmisek.", 200);

    add_food(({"barszcz","zupa"}), ({"ch^lopski","ch^lopscy"}), "barszczu. "+
        "K^l^eby "+
        "spiralnie zwijaj^acej si^e, srebrzystej pary, roznosz^a wok^o^l "+
        "smakowity aromat burak^ow i lekko wyczuwaln^a wo^n zio^lowych "+
        "przypraw. Ciemno-r^o^zowa polewka, si^egaj^aca samych brzeg^ow "+
        "misy, g^esto przetykana jest okami t^luszczu i nitkami zwarzonej "+
        "^smietany. Gdzieniegdzie wynurzaj^a si^e zar^o^zowione bry^lki "+
        "ziemniak^ow, a paski burak^ow pl^acz^a si^e w leniwym ta^ncu z "+
        "zielon^a fasol^a.", 
        500,45,10,"talerz","barszczu","Jest to "+
        "prosty, czysty talerz.", 200);

    add_drink("woda",0,"czystej wody",600,0,2,"kubek","wody",
        "Du^za, czysta szklanka.");
    add_drink("kompot", ({"jab^lkowy", "jab^lkowi"}), "kompotu jab^lkowego",
         400, 0, 9, "kubek", "kompotu", "Ma^la, czysta szklanka.");
    add_drink("piwo", ({ "jasny", "ja�ni"}), "jasno-bursztynowego, " +
    "wspaniale pieni�cego si^e piwa", 700, 4, 15, "kufel", "piwa",
    "Jest to kufel wykonany z krystalicznego szk^la, kt^orego uchwyt " +
    "wykonano z dobrego d^ebowego drewna.",10000);
    add_drink("piwo", ({ "ciemny", "ciemni"}), "ciemnego, " +
    "wspaniale pieni�cego si^e piwa", 700, 5, 16, "kufel", "piwa",
    "Jest to kufel wykonany z krystalicznego szk^la, kt^orego uchwyt " +
    "wykonano z dobrego d^ebowego drewna.",10000);

}
public string
exits_description() 
{
    return "Na p^o^lnocy znajduj^a drzwi do jakiego^s pokoju, na lewo od "+
		"nich znajduj^a si^e inne, prowadz^ace do ^la^xni, za^s na zachodzie "+
		"- niedu^ze drzwiczki, za kt^orymi mie^sci si^e ma^ly sk^ladzik. Na "+
		"wschodniej ^scianie znajduje si^e wej^scie do sieni.  \n";
}

void
init()
{
    ::init();
    init_pub();
    init_peek();

}


string dlugi_opis()
{
   string str;
if(jest_dzien())
    {
        str = "Wok^o^l rozci^agaj^a si^e smugi dymu spowijaj^ac izb^e "+
			"karczmy w mroku i zapachu gryz^acym w oczy i gard^lo. Mgie^lka "+
			"ta unosz^aca si^e znad paleniska po^lo^zonego w g^l^ebi "+
			"pomieszczenia, nadaje karczmie charakterystyczny wygl^ad "+
			"gospody. Stoj^a tu dwa d^lugie sto^ly, przy kt^orych mo^zna "+
			"usi^a^s^c na ^lawach - jeden pod ^scian^a, drugi za^s w samym "+
			"centrum sali, o^swietla je jedynie md^le ^swiat^lo wpadaj^ace "+
			"przez okienka z rybich b^lon. Materia^lem tworz^acym ca^lo^s^c "+
			"jest drewno, przy ka^zdym kroku, pod^loga trzeszczy - jak to "+
			"zwykle si^e dzieje w starych zabudowaniach, a belki "+
			"podtrzymuj^ace strop lekko dr^z^a, powoduj^ac bujanie si^e "+
			"starego ^zyrandola.";
    }
    else
    {
        str = "Wok^o^l rozci^agaj^a si^e smugi dymu spowijaj^ac izb^e "+
			"karczmy w mroku i zapachu gryz^acym w oczy i gard^lo. Mgie^lka "+
			"ta unosz^aca si^e znad paleniska po^lo^zonego w g^l^ebi "+
			"pomieszczenia, nadaje karczmie charakterystyczny wygl^ad "+
			"gospody. Stoj^a tu dwa d^lugie sto^ly, przy kt^orych mo^zna "+
			"usi^a^s^c na ^lawach - jeden pod ^scian^a, drugi za^s w samym "+
			"centrum sali. Materia^lem tworz^acym ca^lo^s^c jest drewno, "+
			"przy ka^zdym kroku, pod^loga trzeszczy - jak to zwykle si^e "+
			"dzieje w starych zabudowaniach, a belki podtrzymuj^ace strop "+
			"lekko dr^z^a, powoduj^ac bujanie si^e starego ^zyrandola "+
			"b^ed^acego o tej porze jedynym ^xr^od^lem ^swiat^la.";
    }
      
   str += "\n";
   return str;
}
