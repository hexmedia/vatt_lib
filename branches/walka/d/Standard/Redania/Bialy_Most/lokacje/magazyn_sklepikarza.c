#include <stdproperties.h>
#include <dir.h>

inherit "/std/room";

void create_room() 
{
    set_short("Kiesze� sklepikarza");
    set_long("Jeste� w kieszeni bez dna.\n");

    add_prop(ROOM_I_INSIDE,1);


    add_object(BIALY_MOST_PRZEDMIOTY+"wachlarz.c",4);
    add_object(BIALY_MOST_PRZEDMIOTY+"sakiewki/pojemna_materialowa.c",50);
    add_object(BIALY_MOST_PRZEDMIOTY+"sakiewki/szara_polatana.c",50);
    add_object(BIALY_MOST_PRZEDMIOTY+"sakiewki/mala_niebieska.c",50);
    add_object(BIALY_MOST_PRZEDMIOTY+"pasy/szary_parciany.c",50);
    add_object(BIALY_MOST_PRZEDMIOTY+"pasy/waski_czerwony.c",50);
    add_object(BIALY_MOST_PRZEDMIOTY+"pasy/mocny_zielonkawy.c",50);
    add_object(BIALY_MOST_PRZEDMIOTY+"pasy/mocny_skorzany.c",50);
    add_object(BIALY_MOST_PRZEDMIOTY+"pasy/matowy_czarny.c",50);
    add_object(BIALY_MOST_PRZEDMIOTY+"pasy/czerwony_zdobiony.c",50);
    add_object(BIALY_MOST_PRZEDMIOTY+"plecaki/brazowy.c",50);
    add_object(BIALY_MOST_PRZEDMIOTY+"plecaki/czarny_skorzany.c",50);


}
