/*
 * podw^orze karczmy w Bia^lym Mo^scie
 * opis by Tinardan
 * zepsu^l^a Faeve
 * 10 maja 2007
 *
 */

 // jakie^s eventy? co^s w stylu, ze zza drzwi zajazdu dochodzi gwar podniesionych, rozochoconych trunkiem g^los^ow.
 // a zza scian stodo^ly s^lycha^c r^zenie i poparskiwania koni?

inherit "/std/room";
#include "dir.h"

#include <macros.h>
#include <stdproperties.h>

void
create_room()
{
    set_short("Podw^orzec przed gospod^a.");
    set_long("Najwyra^xniej w^la^sciciel dobrze dba o sw^oj zajazd. "+
		"Podw^orze wy^lo^zone jest kocimi ^lbami i porz^adnie zamiecione. Na "+
		"wprost wyrasta pot^e^zny budynek stajni, spory, o pochy^lym dachu i "+
		"wielkich, przymkni^etych wrotach. Na prawo wida^c budynek samej "+
		"gospody, przytulony do stajni niczym do bli^xniaczej siostry. "+
		"Niezbyt wysoki, ale bardzo zadbany, zbudowany z grubych, "+
		"poprzecznych belek i podmurowany. Trzy r^owniutkie stopnie "+
		"prowadz^a do niego z podw^orca, drzwi uchylaj^a si^e zach^ecaj^aco. "+
		"Z komina widocznego na dachu karczmy unosi si^e dym. \n");

    add_exit(BIALY_MOST_LOKACJE_ULICE + "u07.c","e",0,1,0);
    add_object(BIALY_MOST_LOKACJE_DRZWI +"do_zajazdu");
    add_object(BIALY_MOST_LOKACJE_DRZWI +"do_stajni");
//    add_object(KARCZMA_DRZWI+"do_karczmy.c"); --> drzwi do zajazdu
// plus drzwi do stajni

add_item("stajnie","Budynek naprawd^e pot^e^zny, zbudowany z jesionowego "+
	"drewna o szerokich s^lojach. S^lomiany dach  po^lo^zony jest pod do^s^c "+
	"mocnym skosem i  tym samym znacznie przewy^zsza budynek gospody. Na "+
	"jego szczycie widnieje elegancko rze^xbiona ozdoba, smuk^la i "+
	"strzelista, podkre^slaj^aca jego wysoko^s^c. Pot^e^zne wrota s^a "+
	"najwyra^xniej rzadko u^zywane, cz^estsz^a drog^a s^a spore drzwi "+
	"umieszczone w jednym ze skrzyde^l. Dochodzi stamt^ad silny, ko^nski "+
	"zapach, s^lycha^c ciche parskania i r^zenia. \n");

add_item(({"zajazd","gospod^e"}),"Budynek do^s^c niski, ale za to szeroki, "+
	"zupe^lnie jakby rozpar^l si^e dumnie na sporym podw^orzu jest te^z "+
	"bardzo zadbany. Szerokie, poprzeczne bele, z kt^orych zosta^l niegdy^s "+
	"zbudowany dodaj^a mu uroku, na oknach o g^l^ebokich parapetach kto^s "+
	"ustawi^l donice z kwiatami. Dach jest niski, ale by^c mo^ze kryje pod "+
	"sob^a jeszcze jedno pi^etro. Z boku wida^c niewielkie okienko. Z "+
	"wn^etrza dochodz^a g^losy biesiadnik^ow i przyjezdnych. \n");

add_sit("na schodkach","na schodkach","ze schodk^ow",4);
}
public string
exits_description() 
{
    return "Znajduj^a si^e tutaj wej^scia do zajazdu oraz do stajni, za^s "+
		"na wschodzie dostrzegasz ulic^e. \n";
}