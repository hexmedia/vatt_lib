/*
 	Zak�ad pochwiarza w Bia�ym Mo�cie :P
 
 	Autor: Vera
   Data : 17.04.07
   Opis : Khaes */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit "/std/room";


void create_room() 
{
    set_short("Ciasna pracownia");
    add_npc(BIALY_MOST_NPC + "snycerz");
	add_object(BIALY_MOST_LOKACJE_DRZWI +"z_zakladu.c");
   

    add_prop(ROOM_I_INSIDE,1);
    
    add_item(({"sk�ry","zwierz�ce sk�ry"}),
    	"Najpospolitsze wygarbowane sk�ry rozwieszone s� na niemal "+
    	"ka�dej �cianie tej pracowni. Kto� kawa�kiem w�gla "+
    	"skre�li� na ich powierzchni wzory przypominaj�ce swymi "+
    	"konturami pochwy i uprz�e. Tu i �wdzie dostrzegasz "+
    	"naci�cia i szwy.\n");
    add_item("kloce",
    	"Niedbale ociosane, pod�u�ne kloce z przer�nych gatunk�w "+
    	"drewna zalegaj� pod �cianami pokoju. Niekt�re z bali - "+
    	"te nienaruszone - poustawiane s� w chwiejne stosy. Na "+
    	"nich za� zalega moc trocin oraz bardziej ukszta�towane "+
    	"drewno, kt�re przypomina swym wygl�dem pokrowce na or�.\n");
    add_sit("na klocu","na drewnianym klocu","z kloca",5);
    
    
}

public string
exits_description() 
{
	return "Wyj�cie st�d prowadzi na ulic�.\n";
}

string
dlugi_opis()
{
    string str;
    
    if(jest_dzien())
    {
    	str="W powietrzu wyczuwasz mocny, napastliwie atakuj�cy nozdrza "+
    	"zapach zwierz�cej sk�ry, drewna oraz metalu. Promienie s�oneczne "+
    	"leniwie s�cz� si� przez okna, odkrywaj�c przed twym wzrokiem "+
    	"przyciasne, zagracone pomieszczenie. Na �cianach wisz� "+
    	"wyprawione zwierz�ce sk�ry, w k�tach walaj� si� drewniane, "+
    	"nieoheblowane kloce";
    	 if(jest_rzecz_w_sublokacji(0,"kwadratowa skrzyneczka"))
    	 str+=", a na parapecie stoi niedbale zbita skrzyneczka";
    	 
    	 str+=". Ba�agan jaki tu panuje jest tym specyficznym "+
    	 "nie�adem, bez kt�rego nie mo�e obej�� si� wi�kszo�� "+
    	 "rzemie�lnik�w - wszystko u�o�one jest z my�l� o �atwym "+
    	 "dost�pie do czegokolwiek, nie o estetyce. ";
		if(jest_rzecz_w_sublokacji(0,"Dobromir"))
		str+="Z k�ta pomieszczenia, wsparty plecami o �cian� "+
			"przygl�da si� twojej osobie brodaty m�czyzna.";
    
    }
    else
    {
    	str="W powietrzu wyczuwasz mocny, napastliwie atakuj�cy "+
    	"nozdrza zapach zwierz�cej sk�ry, drewna oraz metalu. Pochodnia "+
    	"zawieszona przy wej�ciu pali si� jasnym ogniem, odkrywaj�c "+
    	"przed twym wzrokiem przyciasne, zagracone pomieszczenie. "+
    	"Na �cianach wisz� wyprawione zwierz�ce sk�ry, w k�tach "+
    	"walaj� si� drewniane, nieoheblowane kloce";
    	 if(jest_rzecz_w_sublokacji(0,"kwadratowa skrzyneczka"))
    	 str+=", a na parapecie stoi niedbale zbita skrzyneczka";
    	 
    	 str+=". Ba�agan jaki tu panuje jest tym specyficznym nie�adem, "+
    	 "bez kt�rego nie mo�e obej�� si� wi�kszo�� rzemie�lnik�w - "+
    	 "wszystko u�o�one jest z my�l� o �atwym dost�pie do "+
    	 "czegokolwiek, nie o estetyce. ";
		if(jest_rzecz_w_sublokacji(0,"Dobromir"))
   			str+="Brodaty m�czyzna ziewaj�c co chwila, krz�ta si� "+
   			"sennie po pomieszczeniu.";
    }
    
    /*if(jest_rzecz_w_sublokacji(0, "Lollo") ||
        jest_rzecz_w_sublokacji(0, "niski siwawy m�czyzna"))*/
        
    str += "\n";
    return str;
}
