#include <stdproperties.h>
#include </d/Standard/Redania/Bialy_Most/lokacje/dir.h>

inherit "/std/room";

void create_room() 
{
    set_short("Kiesze� haczyka");
    set_long("Jeste� w kieszeni bez dna.\n");

    add_prop(ROOM_I_INSIDE,1);


    add_object(BIALY_MOST_PRZEDMIOTY+"rybackie/duzy_zelazny_haczyk.c",90);
    add_object(BIALY_MOST_PRZEDMIOTY+"rybackie/niewielki_srebrzysty_haczyk.c",90);
    add_object(BIALY_MOST_PRZEDMIOTY+"rybackie/przynetachleb.c", 150);
	add_object(BIALY_MOST_PRZEDMIOTY+"rybackie/prosta_uzywana_wedka.c", 28);
    add_object(BIALY_MOST_PRZEDMIOTY+"rybackie/dluga_jasna_wedka.c", 14);
    add_object(BIALY_MOST_PRZEDMIOTY+"rybackie/szarawa_siec_rybacka.c", 19);
    add_object(BIALY_MOST_PRZEDMIOTY+"rybackie/mala_lekka_lopatka.c",17);
}
