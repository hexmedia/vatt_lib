/*
 * gabinet w garnizonie
 * Bia^ly Most
 * opis by Khaes
 * zepsu^la faeve
 * 10 maja 2007
 *
 */

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"

inherit "/std/room";
inherit "/lib/peek";

#define JEST(x) jest_rzecz_w_sublokacji(0, (x)->short())
    
string dlugi_opis();

void
create_room()
{
    set_short("Gabinet");
    set_long("@@dlugi_opis@@");

    add_prop(ROOM_I_INSIDE, 1);
    
   add_object(BIALY_MOST_LOKACJE_DRZWI +"z_garnizonu");
   add_object(BIALY_MOST_LOKACJE_DRZWI +"do_aresztu");

    add_object(BIALY_MOST_PRZEDMIOTY + "waski_drewniany_stolik.c");
    dodaj_rzecz_niewyswietlana("w^aski drewniany stolik");

    add_object(BIALY_MOST_PRZEDMIOTY + "proste_drewniane_krzeslo.c",2);
    dodaj_rzecz_niewyswietlana("proste drewniane krzes^lo",2);

    add_object(BIALY_MOST_PRZEDMIOTY + "wysoka_obszerna_polka");
    dodaj_rzecz_niewyswietlana("wysoka obszerna p^o^lka");

	set_event_time(200.0);	
	add_event("Jeden z papierowych stos^ow na p^o^lce zachwia^l si^e niebezpiecznie.\n");
	add_event("Pod^loga skrzypi cicho pod twoimi stopami. \n");
	add_event("Gdzie^s na zewn^atrz rozleg^l si^e g^lo^sny krzyk. \n");

}

string
dlugi_opis()
{
    string str;

    str = "We wn^etrzu panuje przyt^laczaj^acy zaduch. ";

    int i, il, il_krzesel;
    object *inwentarz;
   
    inwentarz = all_inventory();
    il_krzesel = 0;
    il = sizeof(inwentarz);
   
    for (i = 0; i < il; ++i) {
         if (inwentarz[i]->jestem_krzeslem_garnizonu()) {
                        ++il_krzesel; }}

    
    if(jest_rzecz_w_sublokacji(0,"w^aski drewniany stolik"))
    {
        str += "Mniej wi^ecej na ^srodku pomieszczenia zosta^l ustawiony "+
            "w^aski stolik";
        if(il_krzesel == 2)
        {
            str += ", z ustawionymi po jego przeciwnych stronach, dwoma "+
                "krzes^lami. ";
        }
        if(il_krzesel == 1)
        {
            str += ", z dostawionym do^n krzes^lem. ";
        }
        if(il_krzesel == 0)
        {
            str += ". ";
        }
    }
    else
    {
        if(il_krzesel == 2)
        {
            str += "Na samym ^srodku sali stoj^a dwa krzes^la. ";
        }
        
            if(il_krzesel == 1)
        {
            str += "Na samym ^sroku sali stoi krzes^lo. ";
        }
        
            if(il_krzesel == 0)
        {
            str += "";
        }
        
   }
   str += "Surowe ^sciany zosta^ly pobielone, co nada^lo im pozory "+
            "schludno^sci. ";
   if(jest_rzecz_w_sublokacji(0,"wysoka obszerna p^o^lka"))
	   {
	   str += "Pod jedn^a z nich znajduje si^e prosta p^o^lka "+
		   "zape^lniona stertami papier^ow. ";
	   }
	   str += "\n";
	   
	   return str;
}

public string
exits_description() 
{
    return "W po^ludniowej ^scianie znajduje si^e zakratowane wej^scie do "+
        "aresztu, za^s na p^o^lnocy dostrzegasz solidne drzwi wej^sciowe. \n";
}
