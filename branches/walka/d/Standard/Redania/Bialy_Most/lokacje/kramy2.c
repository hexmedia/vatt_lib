/* Autor: Avard
   Data : 6.04.07
   Opis : Tinardan */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ULICA_BIALY_MOST_STD;

void create_ulica() 
{
    set_short("Kramy rybne");
    add_exit(BIALY_MOST_LOKACJE + "kramy1.c","e",0,1,1);
    add_npc(BIALY_MOST_NPC + "dora");

    set_event_time(500.0);
    add_event("@@eventy_kramu_madafak:"+file_name(TO)+"@@");

    add_prop(ROOM_I_INSIDE,0);
}

string
eventy_kramu_madafak()
{
	if(jest_dzien())
	{
		switch(random(3))
		{
			case 0: return "Kramarze pokrzykuj� do siebie niezrozumiale.\n";
			case 1: return "Jakie� dziecko, przebiegaj�c, ochlapuje ci� b�otem.\n";
			case 2: return "Dochodzi ci^e nowa fala smrodu.\n";
		}
	}
	else
	{
		switch(random(3))
		{
			case 0..1: return "";
			case 2: return "Dochodzi ci^e nowa fala smrodu.\n";
		}
	}
}

string
dlugi_opis()
{
    string str;
    str = "Wydawa^c si^e mo^ze, ze to miejsce, ciemne i nieprzyjemne, "+
        "wygl^ada podobnie w ka^zdej porze roku. Lepkie, cuchn^ace "+
        "b^loto pokrywaj^ace nier^owne kamienie bruku, szare ^sciany "+
        "dom^ow, kt^ore do tej uliczki zdaj^a si^e odwraca^c plecami - "+
        "^zadnych okien, drzwi czy furtek. I najgorsze - wszechobecny "+
        "smr^od. Przemieszany od^or ^swie^zych i gnij^acych ryb, ludzkiego "+
        "potu, brudu i szlag wie czego jeszcze, zdaje si^e tworzy^c lepk^a, "+
        "niemal namacaln^a zawiesin^e unosz^ac^a si^e nad ulic^a, lepi^ac^a "+
        "si^e do ubra^n, okrywaj^ac^a ka^zd^a woln^a powierzchni^e. ";
    if(jest_rzecz_w_sublokacji(0, "Dora") ||
        jest_rzecz_w_sublokacji(0, "gruba brzydka kobieta"))
    {
        str += "Handlarze ryb pokrzykuj^a do siebie, dzieci biegaj^a, a "+
            "wok^o^l czaj^a si^e ca^le chmary bezdomnych kot^ow, chudych, "+
            "jednookich obdartus^ow o z^lowrogim wyrazie ponurych "+
            "pyszczk^ow. ";
    }
    str += "Nawet niebo wydaje si^e inne - ";
    if(jest_dzien() == 1)
    {
        str += "szarawe, ";
    }
    else
    {
        str += "szarogranatowe, ";
    }
    str += "przes^loni^ete wszechobecnym smrodem, przydymione i odleg^le.\n";
    return str;
}
string
opis_nocy()
{
    string str;
    str = "Wydawa^c si^e mo^ze, ze to miejsce, ciemne i nieprzyjemne, "+
        "wygl^ada podobnie w ka^zdej porze roku. Lepkie, cuchn^ace "+
        "b^loto pokrywaj^ace nier^owne kamienie bruku, szare ^sciany "+
        "dom^ow, kt^ore do tej uliczki zdaj^a si^e odwraca^c plecami � "+
        "^zadnych okien, drzwi czy furtek. I najgorsze � wszechobecny "+
        "smr^od. Przemieszany od^or ^swie^zych i gnij^acych ryb, ludzkiego "+
        "potu, brudu i szlag wie czego jeszcze, zdaje si^e tworzy^c lepk^a, "+
        "niemal namacaln^a zawiesin^e unosz^ac^a si^e nad ulic^a, lepi^ac^a "+
        "si^e do ubra^n, okrywaj^ac^a ka^zd^a woln^a powierzchni^e. ";
    if(jest_rzecz_w_sublokacji(0, "Dora")||
        jest_rzecz_w_sublokacji(0, "gruba brzydka kobieta"))
    {
        str += "Handlarze ryb pokrzykuj^a do siebie, dzieci biegaj^a, a "+
            "wok^o^l czaj^a si^e ca^le chmary bezdomnych kot^ow, chudych, "+
            "jednookich obdartus^ow o z^lowrogim wyrazie ponurych "+
            "pyszczk^ow. ";
    }
    str += "Nawet niebo wydaje si^e inne - ";
    if(jest_dzien() == 1)
    {
        str += "szarawe, ";
    }
    else
    {
        str += "szarogranatowe, ";
    }
    str += "przes^loni^ete wszechobecnym smrodem, przydymione i odleg^le.\n";
    return str;
}

