/* Autor: Avard
   Opis : Faeve
   Data : 20.06.07 */

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"

inherit "/std/room";
inherit "/lib/peek";

string dlugi_opis();

void
create_room()
{
    set_short("Na strychu");
    set_long("@@dlugi_opis@@");

    add_prop(ROOM_I_INSIDE, 1);
    add_exit(BIALY_MOST_LOKACJE + "sala.c",({({"d","schody","sala",
        "sala g^l^owna"}),"po schodach"}),0,1,0);

    add_object(BIALY_MOST_PRZEDMIOTY + "siennik",15);
    add_object(BIALY_MOST_PRZEDMIOTY + "szafa_strych");
    add_object(BIALY_MOST_LOKACJE_DRZWI + "okno_strych");

    dodaj_rzecz_niewyswietlana("stary wytarty siennik",15);
    dodaj_rzecz_niewyswietlana("stara spr^ochnia^la szafa");
/*
    set_event_time(250.0);	
	add_event("Pod^loga nieco skrzypi pod twoim ci^e^zarem.\n");
	add_event("Z g^l^ownej sali dochodz^a ci^e ^smiechy i weso^le okrzyki. \n");
	add_event("W stajni parska jaki^s ko^n. \n");
	add_event("Z podw^orza s^lycha^c jakie^s okrzyki. \n");
	add_event("Zza uchylonych drzwi stajni dolatuje charakterystyczny zapach "+
		"ko^nskiego nawozu. \n");
*/
}

string
dlugi_opis()
{
	string str;
    int i, il, il_siennikow;
    object *inwentarz;
   
    inwentarz = all_inventory();
    il_siennikow = 0;
    il = sizeof(inwentarz);
   
    for (i = 0; i < il; ++i) {
         if (inwentarz[i]->jestem_siennikiem_karczmy_bm()) {
                        ++il_siennikow; }}
    str = "Du^za przestrze^n strychu ci^agnie si^e przez poddasze ca^lej "+
        "karczmy, a wi^ec miejsca jest naprawd^e du^zo. W dodatku miejsca "+
        "niezagospodarowanego";
    if(jest_rzecz_w_sublokacji(0,"szafa"))
    {
        str += "-  tylko gdzie^s w k^acie b^lyskaj^a cienie jakiej^s starej "+
            "szafy. ";
    }
    else
    {
        str += ". ";
    }
    if(il_siennikow > 10)
    {
        str += "Dlatego te^z na pod^lodze porozk^ladano kilkana^scie "+
            "siennik^ow";
    }
    if(il_siennikow >= 5 && il_siennikow <= 10)
    {
        str += "Dlatego te^z na pod^lodze porozk^ladano kilka siennik^ow";
    }
    if(il_siennikow == 4)
    {
        str += "Dlatego te^z na pod^lodze roz^lo^zono cztery sienniki";
    }
    if(il_siennikow == 3)
    {
        str += "Dlatego te^z na pod^lodze roz^lo^zono trzy sienniki";
    }
    if(il_siennikow == 2)
    {
        str += "Dlatego te^z na pod^lodze roz^lo^zono dwa sienniki";
    }
    if(il_siennikow == 1)
    {
        str += "Dlatego te^z na pod^lodze roz^lo^zono jeden siennik";
    }
    if(il_siennikow > 1)
    {
        str += " - dla strudzonych w^edrowc^ow, kt^orzy pragn^a odpocz^a^c "+
            "w pierwszym - lepszym miejscu. ";
    }
    if(il_siennikow == 1)
    {
        str += " - dla strudzonego w^edrowca, kt^ory pragnie odpocz^a^c w "+
            "pierwszym - lepszym miejscu. ";
    }
    str += "Dodatkowym atutem tego pomieszczenia - opr^ocz du^zego metra^zu "+
        "- jest to, i^z znajduje si^e tu g^orna cz^e^s^c kominka, dzi^eki "+
        "czemu, nawet w zimie jest tu ciep^lo. Strych w dzie^n "+
        "roz^swietlaj^a lekko dwa niedu^ze okienka znajduj^ace si^e na "+
        "przeciwleg^lych ^scianach.  ";
    str += "\n";
    return str;
}

public string
exits_description() 
{
    return "Jedynym st^ad wyj^sciem s^a "+
        "prowadz^ace na do^l, do g^l^ownej sali karczmy - schody. \n";
}