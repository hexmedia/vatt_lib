/*
 *Ulica Bia�ego Mostu
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit ULICA_BIALY_MOST_STD;

void create_ulica() 
{
    set_short("Przy zaje�dzie");
    set_long("@@dlugasny@@");
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u10.c","ne",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u06.c","se",0,1,0);
    add_exit(BIALY_MOST_LOKACJE + "podworze","w",0,1,0);

	add_item(({"sklep","sklepy","kram","kramy"}),
			"Wszelkiej ma�ci budki, kramy, sklepiki i stragany "+
			"poutykane s� wsz�dzie gdzie znalaz� si� cho� s��e� "+
			"miejsca. Zbiegaj�ce si� w tym mie�cie szlaki kupieckie "+
			"zasilaj� je towarami z takich miast jak Wyzima czy Novigrad. "+
			"Jak zwykle przy nat�oku d�br, trzeba sporo si� naszuka� "+
			"by znale�� co� konkretnego.\n");
    add_prop(ROOM_I_INSIDE,0);
	add_object(BIALY_MOST_PRZEDMIOTY+"lampa_uliczna.c");
	add_npc(BIALY_MOST_NPC + "mieszczanka");

}
public string
exits_description() 
{
    return "Ulica ci�gnie z p�nocnego-wschodu na po�udniowy-wsch�d, za� "+
		"na zachodzie widnieje skromny podw�rzec przed gospod�.\n";
}


string
dlugasny()
{
	string str="";

	if(jest_dzien())
	str+="Szeroka ulica ��cz�ca p�nocny i po�udniowy kraniec miasteczka "+
	"mija w tym miejscu okaza�y zajazd. Schludne kamieniczki po obu jego "+
	"stronach dowodz� maj�tno�ci mieszka�c�w, za� w�a�ciciele sklep�w i "+
	"kram�w uwijaj� si� w�r�d nat�oku. Nawet blaszane lampy umieszczone "+
	"regularnie na �cianach s� nowe i nie�le wykonane. Wozy "+
	"zapakowane przer�nymi towarami regularnie przebywaj� j� w obie strony.";
	else
	str+="Szeroka ulica ��cz�ca p�nocny i po�udniowy kraniec miasteczka "+
	"mija w tym miejscu okaza�y zajazd, kt�ry mimo p�nej pory nadal jest "+
	"jasno roz�wietlony. Schludne kamieniczki po obu jego stronach dowodz� "+
	"maj�tno�ci mieszka�c�w, za� ich fronty poprzes�aniane s� straganami i "+
	"kramami. Blaszane lampy umieszczone regularnie na �cianach "+
	"roz�wietlaj� mroki nocnych godzin.";

	str+="\n";
	return str;
}
