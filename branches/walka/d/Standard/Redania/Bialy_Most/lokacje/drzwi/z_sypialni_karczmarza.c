/* Autor: Duana
  Opis : Barteusz
  Data : 03.06.2007
*/

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
        
    dodaj_przym("d^ebowy","d^ebowi");
    
    set_other_room(BIALY_MOST_LOKACJE + "sala");
    

    set_door_id("DRZWI_DO_SYPIALNI_KARCZMARZA_BIALY_MOST");
    
    set_door_desc("Zwyk^le d^ebowe drzwi na ^zelaznych zawiasach. Mi^edzy "+
        "deskami widocznych jest pare ma^lych szpar.\n");

    set_open_desc("");
    set_closed_desc("");
    set_lock_desc("^Sciemnia^ly zamek do drzwi, kt^ory cz^e^sciowo pokry^la ju^z "+
        "rdza.\n");
        
    set_pass_command(({({"drzwi","wyj^scie","wyjd^x z sypialni",
     "wyjd^x z sypialni do sali g^l^ownej","przejd^x przez drzwi"}),
     "do sali g^l^ownej","z sypialni"}));
        
    set_lock_command("zamknij");
    set_unlock_command("otw^orz");

    set_key("KLUCZ_DRZWI_DO_KARCZMY_BIALY_MOST");
    set_lock_name("zamek");

    
    set_open(0);
    set_locked(1);    
}