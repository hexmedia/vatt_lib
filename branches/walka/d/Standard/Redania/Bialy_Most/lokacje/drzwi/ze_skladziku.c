/* Autor: Duana
  Opis : Duana
  Data : 25.05.2007
*/

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
        
    dodaj_przym("stary","starzy");
    
    set_other_room(BIALY_MOST_LOKACJE + "sala");
    

    set_door_id("DRZWI_DO_SKLADZIKU_BIALY_MOST");
    
    set_door_desc("Drzwi, na kt^ore spogl^adasz s^a do^s^c niewielkich "+
        "rozmiar^ow, tak, ^ze musisz schyli^c g^low^e, by przez nie przej^s^c. "+
        "Wygl^adaj^a na stare i raczej mocno zniszczone. Barwa drewna, z "+
        "kt^orego je wykonano jest ledwo dostrzegalna przez grube warstwy "+
        "kurzu.\n");

    set_open_desc("");
    set_closed_desc("");
    set_lock_desc("^Sciemnia^ly zamek do drzwi, kt^ory cz^e^sciowo pokry^la ju^z "+
        "rdza.\n");
        
    set_pass_command(({({"drzwi","wyj^scie","sala","sala g^l^owna"}),
     "do sali g^l^ownej","ze sk^ladziku"}));
        
    set_lock_command("zamknij");
    set_unlock_command("otw^orz");

    set_key("KLUCZ_DRZWI_DO_KARCZMY_BIALY_MOST");
    set_lock_name("zamek");

    
    set_open(0);
    set_locked(1);    
}