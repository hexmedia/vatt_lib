/* Autor: Avard
   Opis : Sniegulak
   Data : 20.06.07 */
inherit "/std/window";
inherit "/lib/peek";

#include <pl.h>
#include <macros.h>
#include "dir.h"
void
create_window()
{
    ustaw_nazwe("okno"); 
    dodaj_nazwy("okienko");
    dodaj_przym("ma^ly", "mali");   
                                 
    set_window_id("OKNO_ZE_STRYCHU_KARCZMY_W_BIALYM_MOSCIE");
    set_open(0);
    set_locked(1);
    set_open_desc("");
    set_closed_desc("");
    set_window_desc("Ma^le okienko w drewnianej ramie zosta^lo zbite tak, "+
        "i^z nie masz najmiejszych szans na jego otworzenie. Rybie "+
        "p^echerze s^a brudne i nie przepuszczaj^a zbyt du^zo swiat^la, "+
        "a ich powierzchnie dodatkowo pokrywa gruba warstwa kurzu.\n");
    this_player()->set_hp(this_player()->query_hp()-90-random(50));

    set_pass_command("okno");
    set_pass_mess("przez niedu^ze okno na zewn^atrz");
}