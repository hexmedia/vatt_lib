/* Autor: Duana
  Opis : Barteusz
  Data : 03.06.2007
*/

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
        
    dodaj_przym("d^ebowy","d^ebowi");
    
    set_other_room(BIALY_MOST_LOKACJE + "sypialnia_karczmarza");
    

    set_door_id("DRZWI_DO_SYPIALNI_KARCZMARZA_BIALY_MOST");
    
    set_door_desc("Zwyk^le d^ebowe drzwi na ^zelaznych zawiasach. Mi^edzy "+
        "deskami widocznych jest pare ma^lych szpar.\n");

    set_open_desc("");
    set_closed_desc("");
    set_lock_desc("^Sciemnia^ly zamek do drzwi, kt^ory cz^e^sciowo pokry^la ju^z "+
        "rdza.\n");
        
    set_pass_command(({({"sypialnia","drzwi","p^o^lnoc"}),"do sypialni",
"z sali g^l^ownej"}));
        
    set_lock_command("zamknij");
    set_unlock_command("otw^orz");

    set_key("KLUCZ_DRZWI_DO_KARCZMY_BIALY_MOST");
    set_lock_name("zamek");

    
    set_open(0);
    set_locked(1);    
}