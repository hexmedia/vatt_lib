/* Autor: Avard
   Opis : Faeve
   Data : 4.06.07 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
    dodaj_przym("elegancki","eleganccy");
    dodaj_przym("ciemny", "ciemni");
    
    set_other_room(BIALY_MOST_LOKACJE + "gabinet.c");
    set_door_id("DRZWI_DO_BURMISTRZA_BIALEGO_MOSTU");
    set_door_desc("Te eleganckie drzwi wykonane s^a z ciemnego d^ebowego "+
        "drewna, dok^ladnie obrobionego i poci^agni^etego jakim^s "+
        "impregnatem. W g^ornej cz^e^sci znajduje si^e ma^le, "+
        "p^o^lokr^ag^le okienko, a nieco pod nim - mosi^e^zna ko^latka w "+
        "kszta^lcie paszczy lwa. Klamk^e, dopasowan^e stylowo do ca^lo^sci "+
        "drzwi, r^ownie^z wykonano z mosi^adzu - przypomina ona nieco "+
        "pl^atanin^e jakich^s ro^slin.\n");

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({({"kamienica","burmistrz","drzwi"}),
        "przez eleganckie ciemne drzwi","z zewn^atrz"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_open(0);
    set_locked(0);

    set_key("KLUCZ_DRZWI_DO_BURMISTRZA_BIALEGO_MOSTU");
    set_lock_name("zamek");
}