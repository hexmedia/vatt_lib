/*
 * drzwi ze stajni w BM
 * opis by faeve
 * popsu^la taz sama
 * dn. 4.06 2007
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("wrota"); 
    dodaj_przym("stary","starzy");
    dodaj_przym("pot^e^zny", "pot^e^zni");
    
    set_other_room(BIALY_MOST_LOKACJE + "podworze.c");
    set_door_id("DRZWI_DO_STAJNI_W_BM");
    set_door_desc("Drewniane, zbite z desek, po^l^aczone poprzecznymi "+
		"^latami, wygl^adaj^a solidnie i w miar^e estetycznie. ^Srednio "+
		"masywna budowa i zastosowanie zamka, zapewnia odpowiednie "+
		"zamkni^ecie pomieszczenia i ochron^e przed niechcianymi go^s^cmi.\n");

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({({"wrota","wyj^scie","podw^orze"}),"przez wrota",
        "ze stajni"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_open(0);
    set_locked(1);

    set_key("KLUCZ_DRZWI_DO_STAJNI_BM"); //POTRZEBNE?
    set_lock_name("zamek");
}