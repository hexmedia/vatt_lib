/*
 * drzwi do zajazdu BM
 * sysko faevki jes ^___^
 * dn. 3 czerwca 2007
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
    dodaj_przym("pot^e^zny","pot^e^zni");
    dodaj_przym("jednoskrzyd^lowy", "jednoskrzyd^lowi");
    
    set_other_room(BIALY_MOST_LOKACJE + "sien.c");
    set_door_id("DRZWI_DO_ZAJAZDU_BM");
    set_door_desc("S^a to do^s^c pot^e^zne jednoskrzyd^lowe drzwi wykonane z "+
		"jakiego^s do^s^c dobrego gatunku drewna, wcale porz^adnie "+
		"wyheblowane, poci^agni^ete jedynie jakim^s ^srodkiem, maj^acym "+
		"zakonserwowa^c drewno i zapobiec jego niszczeniu. Drzwi zaopatrzone "+
		"s^a w solidn^a mosi^e^zn^a klamk^e oraz prost^a, r^ownie^z "+
		"mosi^e^zn^a ko^latk^e. \n");

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({({"drzwi","zajazd","karczma"}),"przez zwyk^le drewniane drzwi",
        "do karczmy"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_open(0);
    set_locked(0);

    set_key("KLUCZ_DRZWI_WEJSCIOWE_DO_KARCZMY_BIALY_MOST");
    set_lock_name("zamek");
}