/*
 * drzwi do stajni w BM
 * opis by faeve
 * popsu^la ta^z sama
 * dn. 4.06.2007
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("wrota"); 
    dodaj_przym("stary","starzy");
    dodaj_przym("pot^e^zny", "pot^e^zni");
    
    set_other_room(BIALY_MOST_LOKACJE + "stajnia.c"); 
    set_door_id("DRZWI_DO_STAJNI_W_BM");
    set_door_desc("Dwuskrzyd^lowe wrota stajenne wykonano z d^lugich "+
		"postawionych pionowo desek, wzmocnionych poprzez przybicie do^n po "+
		"jednym - z g^ory i z do^lu, poziomych bali oraz, dodatkowo, jeszcze "+
		"jednego na skos - ot, dla pewno^sci, by konstrukcja wytrzyma^la "+
		"kapry^sny humor wierzchowca. Drzwi powleczono bia^l^a farb^a, "+
		"kt^ora teraz ^luszczy si^e i odpada p^latami, co nie wygl^ada zbyt "+
		"estetycznie.\n");

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({({"wrota","stajnia"}),"przez wrota",
        "z zewn^atrz"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_open(0);
    set_locked(1);

    set_key("KLUCZ_DRZWI_DO_STAJNI_BM"); // TO POTRZEBNE W OG^OLE?
    set_lock_name("zamek");
}