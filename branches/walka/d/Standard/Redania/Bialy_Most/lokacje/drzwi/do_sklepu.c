
/* 
 * Drzwi do sklepu w Bia�ym Mo�cie
 * Vera
 */

inherit "/std/door";

#include "dir.h"
#include <pl.h>


void
create_door()
{
    ustaw_nazwe("drzwi"); 
    dodaj_przym("zwyk^ly","zwykli");
    
    set_other_room(BIALY_MOST_LOKACJE + "sklep.c");
    set_door_id("DRZWI_DO_SKLEPU_W_BIALYM_MOSCIE");
    set_door_desc("Solidne, d^ebowe drzwi prowadz^a do jednego z miejskich " +
        "sklep^ow. Wiekowe ju^z drewno mocno pociemnia^lo od s^lo^nca.\n"+
		"Mniej wi�cej na wysoko�ci twych oczu widnieje krzywy napis niedbale"+
		" wyryty czym� ostrym, a brzmi on nad wyraz wymownie, gdy� "+
		"g�osi: ELF�W NIE OBS�U��! WYNO�CIE SI�!\n");
        
    set_pass_command(({"sklep","do sklepu","z zewn�trz"}));
    //set_pass_mess("przez zwyk^le drzwi do sklepu");
    set_open_desc("");
    set_closed_desc("");
    
    set_lock_command("zamknij");
    set_unlock_command("otw�rz");

    set_key("KLUCZ_DRZWI_DO_SKLEPU_BIALY_MOST");
    set_lock_name("zamek");
}
