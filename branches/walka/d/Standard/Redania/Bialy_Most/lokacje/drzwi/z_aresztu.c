/*
 * drzwi do aresztu w BM
 * opis by faeve
 * popsu^la ta^z sama
 * dn. 25 maja 2005
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("kraty"); 
    dodaj_nazwy("drzwi");
    
    set_other_room(BIALY_MOST_LOKACJE + "gabinet_w_garnizonie.c");
    set_door_id("DRZWI_DO_ARESZTU_BM");
    set_door_desc("W^askie drzwi z wbudowanym zamkiem, wykonane z metalowych "+
		"pr^et^ow. Na zawiasach da si^e dostrzec rdz^e, a co niekt^ore "+
		"fragmenty krat s^a powyginane. \n");

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({({"kraty","wyj^scie","drzwi"}),"przez kraty",
        "wychodz^ac z aresztu w stron^e gabinetu."}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_open(0);
    set_locked(1);

    set_key("KLUCZ_DRZWI_DO_ARESZTU_BM");
    set_lock_name("zamek");
}