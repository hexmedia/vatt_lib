#include <pl.h>
#include <macros.h>
#include "dir.h"

inherit "/std/window";

void
create_window()
{
    ustaw_nazwe("okienko");
    dodaj_nazwy("okno");
    dodaj_przym("niedu^zy", "nieduzi");
    
    set_window_id("OKNO_W_LAZNI_BM");
    set_open_desc("");
    set_closed_desc("");
    set_window_desc("Niedu^ze okienko, dzi^eki kt^oremu w ^la^xni nie jest "+
		"ca^lkowicie ciemno, a jednocze^snie nie jest tak^ze zbyt jasno, co "+
		"nie psuje do^s^c intymnej atmosfery.\n");
    set_open(0);
    set_locked(1);

//    set_owners(({karczmarz bm}));
}
