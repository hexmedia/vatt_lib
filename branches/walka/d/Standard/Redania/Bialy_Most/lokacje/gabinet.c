/*
Made by Eleb.
*/

inherit "/std/room";

#include <object_types.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include "dir.h"

#define SUBLOC_SCIANA ({"�ciana", "�ciany", "�cianie", "�cian�", "�cian�","�cianie", "na"})

string dlugi_opis();

void create_room(){
    
    set_short("Obszerny gabinet");
    set_long("@@dlugi_opis@@");
    
    /*sciana*/
    add_subloc(SUBLOC_SCIANA);
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_CANT_ODLOZ, 1);
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_CANT_POLOZ, 1);
    add_subloc_prop(SUBLOC_SCIANA, SUBLOC_I_MOZNA_POWIES, 1);
    add_subloc_prop(SUBLOC_SCIANA, SUBLOC_I_DLA_O, O_SCIENNE);
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_MAX_RZECZY, 3); 
    add_object(BIALY_MOST_PRZEDMIOTY + "obraz1.c",1,0,SUBLOC_SCIANA);
    add_object(BIALY_MOST_PRZEDMIOTY + "obraz2.c",1,0,SUBLOC_SCIANA);
    add_object(BIALY_MOST_PRZEDMIOTY + "obraz3.c",1,0,SUBLOC_SCIANA);
    add_object(BIALY_MOST_LOKACJE_DRZWI +"do_sypialni_burmistrza.c");
    add_object(BIALY_MOST_LOKACJE_DRZWI +"z_burmistrza.c");
    /*burmistrz*/
    add_npc(BIALY_MOST_NPC + "burmistrz"); 
    
    /*obiekty*/
    
    add_object(BIALY_MOST_PRZEDMIOTY + "masywne_debowe_biurko.c");
    dodaj_rzecz_niewyswietlana("masywne d�bowe biurko");
    dodaj_rzecz_niewyswietlana("zakurzony stary obraz");
    dodaj_rzecz_niewyswietlana("spory stary obraz");
    dodaj_rzecz_niewyswietlana("ma�y stary obraz");
    /*propy*/
    add_prop(ROOM_I_INSIDE, 1);    
}
string dlugi_opis(){
      string str = "Daje si� wyczu�, �e to pomieszczenie dawno nie by�o wietrzone -"+
                   " barwna mozaika przer�nych, cho� bez wyj�tku mocnych zapach�w uderza w twe nozdrza.";
      //opis od dnia/nocy
      if(jest_dzien()==1){
      // dzien
      str = str + " �wiat�o s�oneczne wpadaj�ce przez okna o�wietla ten, dosy�" +
                  " obszerny gabinet nie pozostawiaj�c �adnych szans ciemno�ciom.";
      }else{
      // noc
      str = str + " Jasne �wiat�o wydobywaj�ce si� z kandelabr�w zawieszonych przy wej�ciu" +
                  " z powodzeniem roz�wietla gabinet.";
      }
       int ileObrazow = 0;
       if(jest_rzecz_w_sublokacji(SUBLOC_SCIANA,"zakurzony stary obraz")){ileObrazow++;}
       if(jest_rzecz_w_sublokacji(SUBLOC_SCIANA,"spory stary obraz")){ileObrazow++;}
       if(jest_rzecz_w_sublokacji(SUBLOC_SCIANA,"ma�y stary obraz")){ileObrazow++;}
       if(ileObrazow == 3){
         str = str + " Ka�da ze �cian obwieszona jest licznymi obrazami, na kt�rych r�ne -"+
                     " cho� z regu�y podobne do siebie - osoby prezentuj� swoje sylwetki.";
       }
       if(ileObrazow == 2){
         str = str + " Jedna ze �cian obwieszona jest obrazami, na kt�rych r�ne -"+
                     " cho� z regu�y podobne do siebie - osoby prezentuj� swoje sylwetki.";
       }
       if(ileObrazow == 1){
         str = str + " Na jednej ze �cian wisi obraz przedstawiaj�cy sylwetke jakiej� osoby";
       }
       if(jest_rzecz_w_sublokacji(0,"masywne d�bowe biurko")){
         str = str + " W jednym z rog�w pomieszczenia ustawione jest masywne biurko,"+ 
              " kt�re ogromem swych zdobie� przyt�acza reszt� mebli znajduj�cych si� w"+
              " tym gabinecie.";
       }     
       str += "\n";
      return str;
}



void reakcjaNaNieludzi(object kto){
   object npc = present("migaail", TO);
   
   switch(kto->query_race()){
   	case "cz�owiek": break;
	case "elf": npc->command("powiedz do " + kto->query_name(PL_DOP) + " Osz.. wracaj do lasu w�skodupcu!");
		    break;
        case "krasnolud": npc->command("powiedz do " + kto->query_name(PL_DOP) + " Czego tu? Do kopalni nie w t� stron�.");
		          break;
        case "p�elf": npc->command("powiedz do " + kto->query_name(PL_DOP) + " Niemasz tu czego szuka� odmie�cu.");
		          break;
        default: npc->command("powiedz do " + kto->query_name(PL_DOP) + " Czego tu?");
		          break;
   }

}

command_present(object ob)
{
	if (environment(ob) == this_object() && !ob->query_prop(OBJ_I_INVIS)){ 
          return reakcjaNaNieludzi(ob);
	}else{
	   return 0;
	}
}

public void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);
    if(jest_rzecz_w_sublokacji(0,"oty�y d�ugow�osy m�czyzna"))
        set_alarm(3.0, 0.0,"command_present",ob);
}

string
exits_description()
{
    return "Mo^zesz st^ad wyj^s^c na zewn^atrz lub wej^s^c do sypialni.\n";
}

