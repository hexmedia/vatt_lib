
/* Autor: Avard
   Opis : Tinardan
   Data : 6.04.07 
   Info : NIE JEST SKONCZONA! */

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include <object_types.h>
#include "dir.h"

inherit BIALY_MOST_STD_NPC;
inherit "/lib/sklepikarz";


int czy_walka();
int walka = 0;

void
create_most_npc()
{
    ustaw_odmiane_rasy("kobieta");
    dodaj_nazwy("straganiarka");
    dodaj_nazwy("handlarka");
    set_gender(G_FEMALE);
    ustaw_imie(({"dora","dory","dorze","dor^e","dor^a",
        "dorze"}), PL_ZENSKI);
    dodaj_przym("gruby","grubi");
    dodaj_przym("brzydki","brzydcy");

    set_long("Niewiele mo^zna wyczyta^c z jej oboj^etnej twarzy. Spojrzenie "+
        "ma, o ironio, rybie i nie rozumiej^ace,  twarz nalan^a i "+
        "nieprzyjemn^a. By^c mo^ze kiedy^s by^la pi^ekn^a kobiet^a, ale "+
        "teraz jest to zaledwie wrak, bezz^ebna, brudna, gruba. Na "+
        "rzadkich, ciemnych w^losach zawi^aza^la chustk^e, wygl^adaj^ac^a "+
        "jakby wcze^sniej sprz^atano ni^a stragan. Za paznokciami pulchnych "+
        "d^loni wida^c brud, w^zarty tam niemal na sta^le. Kobieta ca^la "+
        "pokryta jest lepk^a warstw^a brudu, zupe^lnie jakby smr^od i "+
        "rybie pozosta^lo^sci osiad^ly na niej, tworz^ac drug^a sk^or^e. "+
        "Jej szara suknia jest podarta w kilku miejscach, ale doskonale "+
        "wida^c, ^ze nikt nie stara^l si^e nawet jej pozszywa^c.\n");

    set_stats (({ 50, 60, 50, 40, 25, 70 }));
    add_prop(CONT_I_WEIGHT, 90000);
    add_prop(CONT_I_HEIGHT, 160);

    set_act_time(60);
    add_act("emote d^lubie sobie w z^ebach.");
    add_act("emote spogl^ada w przestrze^n pustym wzrokiem.");
    add_act("emote podrywa si^e nagle i skrzekliwie zachwala sw^oj towar.");
    add_act("emote poprawia zsuwaj^ac^a si^e chustk^e.");
    add_act("emote wstaje i chwil^e gmera w rybach le^z^acych na straganie, "+
        "jakby chcia^la zrobi^c jaki^s porz^adek, ale szybko daje za "+
        "wygran^a.");
    add_act("emote spluwa do brudnej ka^lu^zy.");
    add_act("emote smarka w palce.");
    
    set_cact_time(10);
    add_cact("emote krzyczy wniebog^losy.");
    add_cact("krzyknij We^xcie no ode mnie tego posra^nca, on mi ^leb zara "+
        "rozwali.");
    add_cact("krzyknij Bogowie, morduj^a!");

    add_armour(BIALY_MOST_UBRANIA+"suknie/szara_dluga_Mc");
    add_armour(BIALY_MOST_UBRANIA+"chustki/brudna_postrzepiona_Mc");


    //add_ask(({"warsztat"}), VBFC_ME("pyt_o_warsztat"));
    //add_ask(({"pochwy","pochw^e"}), VBFC_ME("pyt_o_pochwy"));
    set_default_answer(VBFC_ME("default_answer"));

    config_default_sklepikarz();
    
	set_co_skupujemy(O_RYBY);
    set_store_room("/d/Standard/Redania/Bialy_Most/lokacje/magazyn_dory.c");
	set_money_greed_buy(103);
	set_money_greed_sell(100+random(20)); //hihi! :D
	set_money_greed_change(120);

}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_warsztat()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: Tak, to m^oj warsztat, chcesz co^s zam^owi^c?");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Wynocha!");
    }    
    return "";
}
string
pyt_o_pochwy()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: Tak, tym w^la^snie si^e zajmuj^e.");
        set_alarm(1.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: Wykonuj^e pochwy, chcesz jak^a^s zam^owi^c?");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Wynocha!");
    }    
    return "";
}
string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "'Kup ryb^e, zamiast mi g^low^e zawraca^c g^lupotami.");
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie "+ OB_NAME(ob));
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj": set_alarm(1.5,0.0, "zly",wykonujacy);
        case "prychnij":
              {
                switch(random(1))
                 {
                 case 0:command("spojrzyj lekcewazaco "+
                     "na "+ OB_NAME(wykonujacy)); break;        
                 }
                break;
               }
        case "przytul": set_alarm(1.5,0.0, "zly",wykonujacy);
                   break;
        case "poglaszcz": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "poklep": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
    switch(random(2))
    {
        case 0: this_player()->catch_msg(QCIMIE(this_object(),
            PL_MIA)+" spogl^ada na ciebie t^epym wzrokiem i zaczyna "+
            "d^luba^c w z^ebie.\n");
            
            saybb(QCIMIE(this_object(),PL_MIA)+" spogl^ada "+
                "na "+QIMIE(TP,PL_BIE)+" t^epym wzrokiem i zaczyna "+
                "d^luba^c w z^ebie.\n");break;
        case 1: command("zignoruj "+OB_NAME(wykonujacy));break;
    }
}
void
zly(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
    {
        switch(random(3))
        {
            case 0: command("powiedz Tfu, z chamstwem si^e nie "+
                "zadaj^e."); break;
            case 1: command("powiedz Id^x se rybk^e w dup^e "+
                "cmoknij. Jeno pierw zap^la^c.");break;
            case 2: command("powiedz Idiotka w rzy^c g^laskana.");break;
        }
    }
    else
    {
        switch(random(3))
        {
            case 0: command("powiedz Tfu, z chamstwem si^e nie "+
                "zadaj^e."); break;
            case 1: command("powiedz Id^x se rybk^e w dup^e cmoknij. "+
                "Jeno pierw zap^la^c.");break;
            case 2: command("powiedz Zboczeniec w rzy^c g^laskany.");break;
        }
    }
}

void
nienajlepszy(object wykonujacy)
{
    switch(random(2))
    {
        case 0: command("^Zebym ja ci^e zaraz w rzy^c nie kopn^e^la, po^lamancu ty!");break;
        case 1: command("krzyknij Jazda z mojego kramu, ryba twoja ma^c!");break;
    }
} 

void
attacked_by(object wrog)
{
    if(walka==0) 
    {
        set_alarm(0.5,0.0,"command","emote wyba^lusza oczy i blednie.");
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog); 
    }
    else
    {
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
       walka = 0;
       return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}
void
init() 
{
    ::init();
    init_sklepikarz();
}
void 
shop_hook_tego_nie_skupujemy(object ob)
{
    notify_fail("A wsad^x se to w rzy^c tak, ^zeby ci g^eb^a wylaz^lo.\n");
}
