
/* Autor: Vera
   Opis : Tinardan
   Data : 6.04.07 
   Info : Jest to sklepikarz w Bia�ym Mo�cie 




	Zrobi�em ze sklepikarza rasist�. Nie obs�uguje elf�w, zatem
	ma odpowiednie reakcje jak elf wchodzi, oraz w lokacji sklepu
	s� nadpisane funkcje sklepikarza takie jak 'kup' itd,
	by i tak nie zd��y� niczego zrobi�, bo reakcje s� na alarmach.
	Oczywi�cie sprawa jest udokumentowana -> zapytaj sklepikarza 
							o elfy lub rodzin� ;)
	Vera
*/

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include <mudtime.h>
#include <sit.h>
#include <object_types.h>
#include <filter_funs.h>
#include "dir.h"
inherit BIALY_MOST_STD_NPC;
inherit "/lib/sklepikarz";


int czy_walka();
int walka = 0;

void
create_most_npc()
{
    ustaw_odmiane_rasy("m�czyzna");
    dodaj_nazwy("kupiec");
    dodaj_nazwy("handlarz");
    dodaj_nazwy("sprzedawca");
    set_gender(G_FEMALE);
    ustaw_imie(({"lollo","lollo","lollo","lollo","lollo",
        "lollo"}), PL_MESKI_OS);
    dodaj_przym("niski","niscy");
    dodaj_przym("siwawy","siwawi");
	set_title("Gurs");

    set_long("Staruszek z niego ju� wiekowy, ale trzyma si� ca�kiem "+
    "dobrze i nadal wygl�da na krzepkiego i �wawego m�czyzn�. Ubrany "+
    "jest bardzo schludnie, jego koszula l�ni czysto�ci�, chustka pod brod� "+
    "zawi�zana r�wniutko, tak by �aden jej r�bek nie wystawa� ani nie zagina� "+
    "si� brzydko. M�czyzna spogl�da na ciebie pogodnie spod krzaczastych brwi, "+
    "kt�re, sko�tunione i siwe, zwieszaj� si� nad oczyma tak nisko i� zdaje si�, "+
    "�e staruszek ma�o co widzi. \n");
    

    set_stats (({ 84, 42, 59, 40, 65, 80 }));
    add_prop(CONT_I_WEIGHT, 77500);
    add_prop(CONT_I_HEIGHT, 153);

    set_act_time(70);
    
    add_act("emote wypuszcza z ust elegancko uformowane k�eczko z dymu.");
    add_act("emote przygl�da ci si� uwa�nie.");
	add_act("powiedz Ech, te parszywe elfy zn�w si� panosz�.");
    add_act("emote wzdycha ci�ko.");
    
    set_cact_time(20);
    add_cact("emote krzyczy wniebog^losy.");
    add_cact("krzyknij Stra�, na pomoc!");
    add_cact("krzyknij Na bog�w, nie!");
    add_cact("krzyknij Na pomoc, ludzie, morduj�!");
    add_cact("emote zatacza si�, nie spuszczaj�c z ciebie przera�onego wzroku.");

    add_armour(UBRANIA_GLOBAL+"koszule/biala_jedwabna_Mc");
    add_object(BIALY_MOST_UBRANIA + "buty/stare_skorzane_Mc");
    add_object(BIALY_MOST_UBRANIA + "spodnie/grube_lniane_Mc");

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_PARRY, 60 + random(10));
    set_skill(SS_UNARM_COMBAT, 25 + random(5));
    set_skill(SS_AWARENESS, 40 + random(10));

    config_default_sklepikarz();
    set_money_greed_buy(112);
    set_money_greed_sell(132);
	set_money_greed_change(102);

	set_store_room(BIALY_MOST_LOKACJE + "magazyn_sklepikarza.c");

	set_co_skupujemy(O_UBRANIA|O_BUTY|O_KUCHENNE|O_LINY|O_LAMPY|O_POCHODNIE|
    				O_ZBROJE|O_BRON_MIECZE|O_BRON_MIECZE_2H|O_BRON_SZABLE|
    				O_BRON_SZTYLETY|O_BRON_TOPORY|O_BRON_TOPORY_2H|
    				O_BRON_DRZEWCOWE_D|O_BRON_DRZEWCOWE_K|O_BRON_MACZUGI|O_BRON_MLOTY|O_INNE);

    //add_ask(({"warsztat"}), VBFC_ME("pyt_o_warsztat"));
	add_ask(({"elfy","elf�w"}), VBFC_ME("pyt_o_elfy"));
	add_ask(({"�on�","�onk�","dziecko","dzieci","rodzin�"}), VBFC_ME("pyt_o_rodzine"));
    set_default_answer(VBFC_ME("default_answer"));
    
    set_alarm_every_hour("co_godzine");
}

string
pyt_o_rodzine()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi cicho, a jego g�os zaczyna dr�e�: Ja ju� nie mam �adnej "+
		"rodziny. Dzi�ki tym dzikusom elfom...");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Wynocha!");
    }    
    return "";
}

string
pyt_o_elfy()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz A nie obs�u�� ich, cho�by mi tu mieli chat� z dymem pu�ci�! "+
		"Nie za to, co zrobili mej �once i dzieciom! Oni zapomnieli, ale "+
		"ja pami�tam.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Wynocha!");
    }    
    return "";
}

int
wygon_ostatecznie(object ob)
{
	if(ENV(ob) == ENV(TO))
	{
		if(random(2))
			command("powiedz Jeszcze tu jeste�?");
		else
			command("machnij");

	}
	else
		command("krzyknij I �ebym ci� tu wi�cej nie widzia�!");

	return 1;
}

int
wygon(object ob)
{
	command("powiedz A posz"+ob->koncowka("ed�","�a","�o")+" mi st�d, "+
		"czarcie nasienie! Elf�w nie obs�u��!");
	command("otworz drzwi");
	command("wskaz na drzwi");
	command("powiedz Wyno� si� elf"+ob->koncowka("ie","ko","ie")+", p�kim "+
			"dobry!");
	set_alarm(7.5,0.0,"wygon_ostatecznie",ob);
	return 1;
}

nomask void
signal_enter(object ob, object skad)
{
	if(ob->query_race_name() != "elf")
		return;

	if(!sizeof(FILTER_IS_SEEN(ob,({TO}))) ||
       !sizeof(FILTER_CAN_SEE_IN_ROOM(({TO}))))
            return;

	set_alarm(1.0,0.0,"wygon",ob);
}


int
zamykamy()
{
	command("No! Ju� pora zamyka�! Zapraszam ponownie o czwartej!");

	object *gracze = FILTER_PLAYERS(all_inventory(ENV(TO)));
	if(sizeof(gracze))
	{
		foreach(object g : gracze)
		{
			g->catch_msg("Sklepikarz wyprasza ci� ze swojego sklepu.\n");
			//g->move(BIALY_MOST_LOKACJE_ULICE+"u08.c");
			//g->do_glance(2);
			tell_room(BIALY_MOST_LOKACJE_ULICE + "u08", "Ze sklepu w�a�nie "+
	           "wyrzucono " + QIMIE(g,PL_BIE) + ".\n");
			g->remove_prop(SIT_SIEDZACY);
			g->remove_prop(OBJ_I_DONT_GLANCE);
			g->move_living("M",BIALY_MOST_LOKACJE_ULICE + "u08",1,0);
		}
	}
	
	command("zamknij drzwi");
	command("zamknij drzwi kluczem");
	
	(BIALY_MOST_LOKACJE_ULICE + "u08")->zamkniety_sklep();

	return 1;
}

int
otwieramy()
{
	command("otworz drzwi kluczem");
	command("otworz drzwi");

	(BIALY_MOST_LOKACJE_ULICE + "u08")->otwarty_sklep();

	return 1;
}

int
co_godzine()
{
	if(MT_GODZINA == 1)
		zamykamy();
	else if(MT_GODZINA == 4)
		otwieramy();
		
	return 1;
}


void
shop_hook_list_empty_store(string str)
{
    command("powiedz Nie mam nic na sprzeda� obecnie. Prosz� "+
    "przyj�� za jaki� czas.");
}
void 
shop_hook_tego_nie_skupujemy(object ob)
{
    command("powiedz Przykro mi, ale nie skupuj� �adnych "+
    			ob->query_pnazwa(PL_DOP)+".");
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

/*string
pyt_o_warsztat()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: Tak, to m^oj warsztat, chcesz co^s zam^owi^c?");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Wynocha!");
    }    
    return "";
}*/

string
default_answer()
{
    command("pokrec");
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie "+ OB_NAME(ob));
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj": set_alarm(1.5,0.0, "zly",wykonujacy); break;
        case "prychnij":
              {
                switch(random(1))
                 {
                 case 0:command("spojrzyj lekcewazaco "+
                     "na "+ OB_NAME(wykonujacy)); break;        
                 }
                break;
               }
        case "przytul": set_alarm(1.5,0.0, "zly",wykonujacy);
                   break;
        case "poglaszcz": set_alarm(0.5, 0.0, "zly", wykonujacy);
                   break;
        case "poklep": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "u�miechnij": set_alarm(0.5, 0.0, "usmiech", wykonujacy);
                   break;
    }
}

void
usmiech(object wykonujacy)
{
	if(random(2))
		command("usmiechnij sie szeroko");
	else
		command("emote szczerzy swoje pr�chniej�ce z�by.");
}

void
malolepszy(object wykonujacy)
{
    switch(random(2))
    {
        case 0: this_player()->catch_msg(QCIMIE(this_object(),
            PL_MIA)+" spogl^ada na ciebie t^epym wzrokiem i zaczyna "+
            "d^luba^c w z^ebie.\n");
            
            saybb(QCIMIE(this_object(),PL_MIA)+" spogl^ada "+
                "na "+QIMIE(TP,PL_BIE)+" t^epym wzrokiem i zaczyna "+
                "d^luba^c w z^ebie.\n");break;
        case 1: command("zignoruj "+OB_NAME(wykonujacy));break;
    }
}
void
zly(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
    {
        switch(random(4))
        {
            case 0: command("powiedz Mi�o mi panienko, ale "+
            "stary dziadyga ze mnie, nic z tego nie b�dzie."); break;
            case 1: command("powiedz Nie trzeba, nie trzeba, z�otko.");break; 
            case 2: command("emote u�miecha si� do ciebie ciep�o, a jego oczy "+
            		"wilgodniej�."); break;
            case 3: 
            	if(query_verb() ~= "pog�aszcz")
            		command("powiedz Ech, moja �onka tak mnie kiedy� g�adzi�a. "+
            		"Zmar�o jej si� w zesz�ym roku.");
            	else if(query_verb() ~= "przytul")
            	command("powiedz Ech, moja �onka tak mnie kiedy� tuli�a. "+
            	"Zmar�o jej si� w zesz�ym roku.");
            	else if(query_verb() ~= "poca�uj")
            	command("powiedz Ech, moja �onka tak mnie kiedy� ca�owa�a. "+
            	"Zmar�o jej si� w zesz�ym roku.");
            	
            	break;
        }
    }
    else
    {
        switch(random(4))
        {
            case 0: command("powiedz To tak dlatego, �e pan "+
            "mnie lubi, tak? "); break;
            case 1: command("powiedz Dziwne ludzie teraz chodz� "+
            "po �wiecie. ");break;
            case 2: command("emote wzdycha: Ju� nie te ch�opy co kiedy�, nie te...");
            		break;
            case 3: command("powiedz Wielki jak brzoza, g�upi jak koza, ot co.");
            		break;
        }
    }
}

void
nienajlepszy(object wykonujacy)
{
    switch(random(3))
    {
        case 0: command("powiedz Niedobre jakie� te ludzie dzisiaj.");break;
        case 1: command("powiedz A odczep si� ode mnie, wszarzu zafajdany. ");break;
        case 2: command("emote zatacza si� do ty�u i �apie blatu. Dyszy: Za "+
        		"co to, za co?"); break;
    }
} 

void
attacked_by(object wrog)
{
    if(walka==0) 
    {
        set_alarm(1.0, 0.0, "command", "emote krzyczy wniebog^losy.");
        //set_alarm(0.5,0.0,"command","emote wyba^lusza oczy i blednie.");
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog); 
    }
    else
    {
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
       walka = 0;
       return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}


int
xxx()
{
	if(TP->query_race_name() == "elf") //&& present("lollo"))
	{
		write("Zdaje si�, �e sklepikarz zupe�nie ci� ignoruje.\n");
		return 1;
	}
	else
		return 0;
}
void
init() 
{
    ::init();
    init_sklepikarz();
	add_action(xxx,   "kup");
    add_action(xxx,  "sprzedaj");
    add_action(xxx, "wyce�");
    add_action(xxx,  "poka�");
}

