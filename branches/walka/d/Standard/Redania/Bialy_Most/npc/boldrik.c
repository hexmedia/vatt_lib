/* 
 * pan z garnizonu
 * w Bia�ym Mo�cie
 * opia by Khaes
 * zepsula faeve
 * dn. 12 maja 2007
 *
 */


#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"
inherit BIALY_MOST_STD_NPC;

int czy_walka();
int walka = 0;

#define BRON BIALY_MOST_BRONIE + "miecze/obosieczny_krotki.c"
#define ZBRO BIALY_MOST_ZBROJE + "przeszywanice/pikowana_brazowa_Mc.c"
#define BUTY BIALY_MOST_UBRANIA + "buty/czarne_zolnierskie_Mc.c"
#define SPOD BIALY_MOST_UBRANIA + "spodnie/grube_lniane_Mc.c"

void
create_most_npc()
{
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_living_name("boldrik");
    set_gender(G_MALE);
    ustaw_imie(({"boldrik","boldrika","boldrikowi","boldrika","boldrikiem","boldriku"}),
        PL_MESKI_OS);
//    set_title("");
//    dodaj_nazwy("");

    set_long("Spod wp^o^lprzymkni^etych powiek m^e^zczyzna obserwuje swoje "+
		"otoczenie. Jego masywna budowa i kwadratowa twarz nie pasuj^a do "+
		"wizerunku kogo^s, kto pracuje za biurkiem, ale cz^lowiek ten "+
		"najwyra^xniej do wizerunku nie przyk^lada du^zej wagi i z "+
		"w^la^sciw^a sobie leniwo^sci^a przegl^ada oraz segreguje papiery. "+
		"Co chwila z jego ust wydobywa si^e st^lumione ziewni^ecie, a on sam "+
		"przeci^aga si^e sennie. \n");

    dodaj_przym("masywny","masywni");
    dodaj_przym("senny","senni");

    set_stats (({ 62, 43, 48, 70, 90, 35 }));

    add_prop(CONT_I_WEIGHT, 110000);
    add_prop(CONT_I_HEIGHT, 180);
  
    set_act_time(30);
    add_act("emote spogl^ada przez chwil^e w pustk^e, poczym wraca do swoich zaj^e^c.");
    add_act("emote z zapa^lem d^lubie w nosie.");
    add_act("emote ziewa otwieraj^ac szeroko usta i zamykaj^ac oczy.");
	add_act("popatrz . na kobiete ");
	add_act("popatrz . na mezczyzne ");
    add_act("emote ze znudzeniem spogl^ada w sufit. ");

    set_cchat_time(10);
    add_cchat("Stra^z! Do mnie! ");
    add_cchat("Bij, zabij, sukinkota! ");
    add_cchat("Ha! Po^za^lujesz, ze ze mn^a zadar^l"
	+ TP->koncowka("e^s!","a^s!"));
    
    add_weapon(BRON);
    add_armour(ZBRO);
    add_armour(BUTY);
    add_armour(SPOD);
 
    add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
    add_ask(({"Bia^ly Most","miasto"}), VBFC_ME("pyt_o_miasto"));
    set_default_answer(VBFC_ME("default_answer"));

    set_alarm_every_hour("co_godzine");
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_garnizon()
{
	if(!query_attack())
    {
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz A, tak� Tutaj trzymamy przest^epc^ow, mo^zna powiedzie^c, "+
			"^ze � m^e^zczyzna u^smiecha si^e kr^otko � jestem tu, ten� "+
			"Nadzorc^a� Bardzo odpowiedzialna funkcja, ot co! ");
    return "";
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Jak wida^c mam teraz wa^zniejsz^a spraw^e na g^lowie!");
    }
    return "";
    
}
string
pyt_o_miasto()
{
	if(!query_attack())
    {
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz No� Tak. Za du^zo si^e tu nie dzieje, ale stra^znicy zawsze "+
		"si^e przydaj^a, nie? ");
    return "";
	}
	else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Jak wida^c mam teraz wa^zniejsz^a spraw^e na g^lowie!");
    }
    return "";
    
}


string
default_answer()
{
	if(!query_attack())
	{
    switch(random(4))
	{
    case 0:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Nie wiem� Spa^c mi si^e chc^e.  "); break;
	case 1:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
		"powiedz Hmm� Kto wie. "); break;
	case 2:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
		"powiedz Hmm� Kto wie? "); break;
	case 3:set_alarm(2.0, 0.0, "command_present", this_player(),
		"wzrusz ramionami");break;
	}
	}
	else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Jak wida^c mam teraz wa^zniejsz^a spraw^e na g^lowie!");
    }
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));

}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
				  {
			  switch(random(3))
						{
                 case 0: set_alarm(0.5, 0.0, "command_present", wykonujacy,
					 "emote czerwieni si^e lekko i skupia wzrok na "+
					 "papierach. "); break;
                 case 1: set_alarm(0.5, 0.0, "command_present", wykonujacy,
					 "emote z za^zenowaniem odwraca wzrok. ");
						break;
                 case 2: set_alarm(0.5, 0.0, "command_present", wykonujacy,
					 "popatrz niesmialo na "+OB_NAME(wykonujacy));
                        break;
						}
				  }
					 else
				  {
                   switch(random(3))
					    {
                   case 0: set_alarm(0.5, 0.0, "command_present", wykonujacy,
					   "emote przez par^e chwil spogl^ada ze zdziwieniem na "+
					 OB_NAME(wykonujacy)+", poczym krzywi si^e wycieraj^ac "+
					  "usta. \n"); break;
                   case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy, 
					   "krzyknij Panie! Co pan wyrabia? ");
				   set_alarm(0.5, 0.0, "command_present", wykonujacy,
					   "splun"); break;
                   case 2:command("spojrzyj z obrzydzeniem na "+
                        OB_NAME(wykonujacy));
                            break;
						}
				  }				
              
               }
			   break;
        case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
	if(wykonujacy->query_gender() == 1)
           switch(random(2))
           {
			case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
				"powiedz Nie mam czasu, pracuje... Poza tym... ^spi^acy "+
				"jestem. "); break;
			case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
				"powiedz Mo^ze p^o^xniej, jak sko^ncz^e prac^e... Dobrze? "); 
			break;
		   }
		   else
			{
			   switch(random(2))
				{
				   case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
					"powiedz Zostaw mnie, czy ja ci, kurew, w czym^s "+
					   "przeszkadzam? "); break;
				   case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
					   "splun"); break;
				}
			}
					 
}

void
nienajlepszy(object kto)
{
   switch(random(3))
   {
      case 0: set_alarm(0.5, 0.0, "command_present", kto,
		  "pogroz " +OB_NAME(kto));
              break;
      case 1: set_alarm(0.5, 0.0, "command_present", kto,
              "popatrz chlodno na "+OB_NAME(kto));
              break;
      case 2: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
		  "powiedz Bo inaczej pogadamy...");
              break;
   }
} 

void
attacked_by(object wrog)
{
    if(walka==0) 
    {
        set_alarm(1.0, 0.0, "command", "krzyknij Zaraz po�a�ujesz, �e ze mn� "+
		  "zadar�" + TP->koncowka("e^s!","a^s!"));
	
        command("dobadz broni");
        set_alarm(5.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog); 
    }
    else if(walka == 1)
    {
       /*command("krzyknij Stra�! Do mnie!");
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;*/

        return ::attacked_by(wrog);
    }

}

int
czy_walka()
{
    if(!query_attack())
    {

       this_object()->command("powiedz Ha, takim straszny?");

       this_object()->command("opusc bron.");

       walka = 0;
       return 1;
    }
       else
       {
       set_alarm(5.0, 0.0, "czy_walka", 1);
       }

}
