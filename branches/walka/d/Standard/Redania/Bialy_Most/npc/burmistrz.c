/*
* Made by Eleb.
* skladanie podan by Vera na podst. kodu sekretarza Rinde
*/

#pragma strict types
#include <std.h>
#include <composite.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include <ss_types.h>
#include <money.h>
#include <pl.h>
#include <object_types.h>
#include "dir.h"
inherit BIALY_MOST_STD_NPC;

int czy_walka();
int walka = 0;
void negatywne(object kto);
void pozytywne(object kto);


void create_most_npc(){
  
   set_living_name("migaail");
   ustaw_imie(({"migaail","migaaila","migaailowi","migaaila","migaailem","migaailu"}),PL_MESKI_OS);

   dodaj_przym("oty�y","otyli");
   dodaj_przym("d�ugowlosy","d�ugow�osi");
   
   ustaw_odmiane_rasy(PL_MEZCZYZNA);
   dodaj_nazwy("burmistrz");
   
   set_title("Rubespier, burmistrz Bia�ego Mostu");
   set_long("@@dlugi_opis@@");
   set_gender(G_MALE);  
   
   set_stats(({ 60, 60, 45, 80, 65, 60})); 
   set_skill(SS_DEFENCE, 65 + random(15));
  
   add_prop(CONT_I_WEIGHT, 95000);
   add_prop(CONT_I_HEIGHT, 170);
  
   // Ke? czemu nie dziala :( Ubranie ma odpowiedni rozmiar!  
   add_armour(BIALY_MOST_UBRANIA + "spodnie/gustowne_czarne_Lc.c");
   add_armour(BIALY_MOST_UBRANIA + "koszule/biala_jedwabna_Lc.c");
   add_ask("prac�",VBFC_ME("pyt_o_prace"));
   add_ask("zadanie",VBFC_ME("pyt_o_zadanie"));
   add_ask(({"bia�y most","miasto"}),VBFC_ME("pyt_o_miasto"));
	add_ask(({"podanie", "podania","sk�adanie podania"}), VBFC_ME("pyt_podanie"));
	add_ask(({"pochodzenie"}), VBFC_ME("pyt_pochodzenie"));
   set_default_answer(VBFC_ME("default_answer"));
    // no to gadki opisane wieksosc jako emoty wiec damy emoty.
  
   set_act_time(10);
   add_act("emote mamrocze co� pod nosem, b��dz�c wzrokiem po pomieszczeniu.");
   add_act("emote m�wi cicho: A gdyby tak...");
   add_act("emote kiwa z przekonaniem g�ow�: To jedyne rozwi�zanie.");
   add_act("emote m�wi splataj�c d�onie za plecami: Tak. Tak zrobi�.");
   add_notify_meet_interactive("string pyt_o_zadanie");

} // end create monester

void powiedz_gdy_jest(object player, string tekst)
{
   if (environment(this_object()) == environment(player))
     this_object()->command(tekst);
}
// opisik
string dlugi_opis(){
   string str;
   str = "Przysadzisty i oty�y m�czyzna w podesz�ym wieku ubrany w proste,"+
         " dobrze skrojone ubrania zdaje si� by� poch�oni�ty jak�� wa�n�"+
         " spraw�. Jego d�ugie siwe w�osy sp�ywaj� mu po ramionach niczym kaskady"+
         " wodospadu. Starzec wolnym, acz pewnym krokiem porusza si� po"+
         " pomieszczeniu mamrocz�c co� cicho pod nosem. Gdy dostrzega, �e mu si�"+
         " przygl�dasz posy�a w twoj� stron� zdawkowe kiwni�cie g�owy i"+
         " zatrzymuj� na twojej sylwetce spojrzenie swych czarnych, �wi�skich oczu.\n";
   return str;
}
// pytania
string pyt_o_prace(){
   set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP) +
             " Ceni� sobie moj� prac�, nie�atwo by�o zosta� tu burmistrzem, o nie.");
   return "";
}

string pyt_podanie(){
   set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP) +
             " Hmm..Nie wiem. Mo�e masz na my�li pochodzenie?");
   return "";
}
string pyt_pochodzenie(){
   set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP) +
             " Chcesz otrzyma� pochodzenie? Je�li tak musisz mi wp�aci� pi�tna�cie "+
			"z�otych koron."+
			" C�, dokumenty s� takie drogie..");
	set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
             "us dyskretnie");
   return "";
}

string pyt_o_miasto(){
   
   set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP)+
            " Bia�y most? Tak, tak. To miasteczko pracuje na siebie, a zyski p�yn� wprost do mni... do mieszka�c�w.");
    command("u�miechnij si� lekko");
    return ""; 
                       
}
string pyt_o_zadanie(){
   
   set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP)+
             " Tak. Mam dla ciebie zadanie. Id� st^ad i daj porz�dnym ludziom pracowa�!");
   return "";
}

string default_answer(){        
    switch(random(3)){
    case 0: command("emote purpurowieje na twarzy i wskazuj�c drzwi m�wi: Tu ludzie pracuj�, nie maj� czasu odpowiada� na durne pytania!");
            break;
    case 1: command("emote kr�ci g�ow� lekko, nie zwracaj�c na nic uwagi.");
            break;
    case 2: command("emote macha r�k� ci�ko m�wi�c: Nie widzisz, �em zaj�ty?");
            break; 
    }
    return "";
}
// Przedstawianie sie.  Nie do konca skumalem :P  No juz skumalem xD
void return_introduce(string imie)
{
    object osoba;
    osoba = present(imie, environment()); 
    
    if (osoba){
    command("przedstaw si� " + OB_NAME(osoba));
    command("powiedz do " + OB_NAME(osoba) + " Milo cie poznac.");     
    }
}
void emote_hook(string emote, object wykonujacy){
   switch(emote){
      case "poca�uj":{
          if(wykonujacy->query_gender() == G_FEMALE){
             switch(random(2)){
                case 0: TP->catch_msg(QCIMIE(TO,PL_MIA) + " klepie ci� z u�miechem po po�ladku i m�wi: No, no. \n");
                        saybb(QCIMIE(TO,PL_MIA) + " klepie " + QIMIE(TP,PL_DOP) +" z usmiechem po po�ladku i m�wi: No, no. \n", wykonujacy);
                        break;
                case 1: command("u�miechnij sie b�ogo");
                        break;
             }// switch ran 2  
          }else{
             switch(random(2)){
               case 0:  command("emote czerwienieje na twarzy, poczym zaczyna krzycze�: Wyno� si� st�d, ty czarci pomiocie!"); 
                        break; 
               case 1:  TP->catch_msg(QCIMIE(TO,PL_MIA) + " uderza ci� z rozmachem w pier� poczym odsuwa si�. \n");
                        saybb(QIMIE(TO,PL_MIA) + " uderza " + QIMIE(TP,PL_DOP) + " z rozmachem w pier� poczym odsuwa si� od niego.\n",wykonujacy);
                        break;
             }
          }
      }// case pocaluj
      case "u�miechnij":{
          if(wykonujacy->query_gender() == G_FEMALE){
            TP->catch_msg(QIMIE(TO,PL_MIA) + " szczerzy do ciebie nadpsute z�by.\n");
            saybb(QIMIE(TO,PL_MIA) + " szczerzy do " + QIMIE(TP,PL_DOP) +" nadpsute z�by. \n",wykonujacy);
            break;
          }else{    
            TP->catch_msg(QCIMIE(TO,PL_MIA) + " patrzy na ciebie powoli poczym wraca do swych zaj��.\n");
            saybb(QIMIE(TO,PL_MIA) + " patrzy na " + QIMIE(TP,PL_DOP) + " powoli poczym wraca do swych zaj��.\n",wykonujacy);
            break;
          }
      }// case usmiechnij sie
      case "kopnij": negatywne(wykonujacy);
                     break;
      case "spoliczkuj": negatywne(wykonujacy);
                         break;
      case "opluj": negatywne(wykonujacy);
                    break;
      case "prychnij": negatywne(wykonujacy);
                       break;
      case "gr�": negatywne(wykonujacy);
                   break;
      case "nadepnij": negatywne(wykonujacy);
                       break;
      case "przytul": pozytywne(wykonujacy);
                      break;
      case "oczko": pozytywne(wykonujacy);
                    break;
      case "pociesz": pozytywne(wykonujacy);
                      break;
      case "podzi�kuj": pozytywne(wykonujacy);
                        break;
      case "pog�aszcz": pozytywne(wykonujacy);
                        break;
      case "zignoruj": negatywne(wykonujacy);
                       break;
   }
}
// reakcje
void negatywne(object kto){
    if(kto->query_gender() == G_FEMALE){
      switch(random(2)){
        case 0: command("emote kiwa g�ow� powoli, poczym u�miecha si� i m�wi: To panienka z takich.. No, no.");
                break;
        case 1: command("krzyknij O�, ty suko ch�do�ona! Wyno� si� mi st�d, ale to ju�!");
                break;     
      }
    }else{
       switch(random(2)){
         case 0:TP->catch_msg(QCIMIE(TO,PL_MIA) + " purpurowieje na twarzy i spogl�da na ciebie srogo.\n");
		 saybb(QCIMIE(TO,PL_MIA) + " purpurowieje na twarzy i spogl�ga srogo na " + QIMIE(TP,PL_DOP) +"\n");
                 break;
         case 1: command("emote zaczyna wrzeszcze�: Bo zawo�am stra�e!");
                 break;       
       }
    }
}
void pozytywne(object kto){
    if(kto->query_gender() == G_FEMALE){
       switch(random(2)){
         case 0: command("popatrz lubie�nie na " + kto->short(PL_DOP));
                 break;
         case 1: command("mrugnij do " + kto->short(PL_DOP));
                 command("emote U�miecha si� pod nosem");
                 break;
       }
    }else{
       switch(random(2)){
         case 0: command("pokiwaj zdawkowo");
                 break;
         case 1: command("machnij r�k� od niechcenia");
                 break;
       }
    }

}

int czy_walka()
{
    if(!query_attack())
    {
       this_object()->command("powiedz Kara Ci� nie minie!");
       walka = 0;
       return 1;
    }
    else
    {
       set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}

void attacked_by(object wrog)
{
    if(walka==0) {
    set_alarm(4.0, 0.0, "command", "krzyknij Stra�! Pomocy!");
    set_alarm(15.0, 0.0, "czy_walka", 1);
    walka = 1;
    return ::attacked_by(wrog); }

    else if(walka == 1)
    {
      return ::attacked_by(wrog);
    }
}

public void do_die(object killer)
{
	if(killer->query_race() != "cz�owiek"){
		command("emote ostatkiem si� krzyczy: B�dziesz wisie� parszywy odmie�cu!");
	}else{
	command("emote ostatkiem si� krzyczy: B�dziesz wisie�!");
	}
        ::do_die(killer);
}


int
wplac(string str)
{
	if(query_verb() ~= "wp�a�")
		notify_fail("Za co chcesz wp�aci�?\n");
	else
		notify_fail("Za co chcesz zap�aci�?\n");

	if(!str)
		return 0;

	if(str != "za pochodzenie")
		return 0;

	if(TP->query_origin() ~= "z Bia�ego Mostu")
	{
		set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP) +
             " Ej�e, przecie my si� znamy s�siedzie. Jak�e to tak?");
		return 1;
	}
	if(TP->query_origin())
	{
		set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP) +
             " Ho ho, przykro mi. Musia�"+TP->koncowka("by�","aby�","oby�")+
			" by� pariasem, bym m�g� ci� przyj��. Teraz to ju� po ptokach.");
		return 1;
	}

	if(!MONEY_ADD(TP,-3600))
	{
		command("'Co mi tu pokazujecie? Pi�tna�cie z�otych koron, rzek�em.");
		return 1;
	}

	string wiad = "";
	string przedst = TP->query_name();
	if(TP->query_surname())
		przedst+= " "+TP->query_surname();

    wiad += "\n\tNa mocy praw mi^lo^sciwie nam panuj^acego " +
          "Ja^snie Pana Vizimira I, z woli bo^zej i ludzkiej " +
          "kr^ola i jedynego prawowitego w^ladcy Redanii, a z " +
          "decyzji w^ladz miasta Bia�y Most niniejszym og^lasza si^e, " +
          "i^z z dniem dzisiejszym ";
    wiad += przedst;
	wiad+=" zostaje mieszka^ncem " +
            "miasta Bia�y Most, wobec czego otrzymuje wszelkie prawa "+
           "tego^z. Jednocze^snie zobowi^azuje si^e " +
           TP->koncowka("go", "j^a") + " do sumiennego " +
           "przestrzegania kodeksu miasta pod kar^a " +
           "cofni^ecia praw.\n\n\n";

    wiad += sprintf("%70s", BURMISTRZ + " " + BURMISTRZ_OPT + BURMISTRZ_N+",\n");
    wiad += sprintf("%70s", "burmistrz Bia�ego Mostu.\n");

	object doku = clone_object(BIALY_MOST_PRZEDMIOTY+"dokument_pochodzenia.c");
    doku->set_message(wiad);
    doku->init_doku_arg(doku->query_doku_auto_load());
    doku->move(this_object());
    set_alarm(1.0, 0.0, "command", "daj dokument " + OB_NAME(TP));
	set_alarm(1.5,0.0,"command","odloz dokument");
	TP->set_origin(POCHODZENIE);
	command("usmiechnij sie szeroko");
	command("'Doskonale. Prosz�, oto Wasz dokument.");
	



	return 1;
}


void
init_living()
{
    ::init_living();
	add_action(wplac,"wp�a�");
	add_action(wplac,"zap�a�");
}