/*
 * Stra�nik z Bia�ego Mostu.
 * Long: Niejaka Heryn alias Zwierzaczek, przeredagowany i nieco zmodyfikowany
 * przez ni�ej podpisanego
 * Eventy, acty i ca�� reszt� pope�ni�: Vera
 */

inherit "/std/monster";
inherit "/std/act/action";
inherit "/std/act/chat";
inherit "/std/act/asking";
inherit "/lib/straz/straznik";

#include "dir.h"

#include <macros.h>
#include <pl.h>
#include <ss_types.h>
#include <stdproperties.h>
#include <filter_funs.h>

#define MIASTO "z Bia�ego Mostu"

#define BRON BIALY_MOST_BRONIE + "palki/drewniana_ociosana.c"
#define ZBRO BIALY_MOST_ZBROJE + "przeszywanice/pikowana_brazowa_Mc.c"
#define BUTY BIALY_MOST_UBRANIA + "buty/czarne_zolnierskie_Mc.c"
#define SPOD BIALY_MOST_UBRANIA + "spodnie/grube_lniane_Mc.c"

object admin=TO->query_admin();

void random_przym(string str, int n);

void
losuj_long()
{
	string str;
	str="M�czyzna zdaje si� by� lekko znudzony, albo niewyspany. "+
        "Podkr��one i zaczerwienione oczy sugeruj�, �e przyda�by mu "+
		"si� odpoczynek. ";

	switch(random(4))
	{
		case 0:
        str+="Kr�tko ostrzy�one w�osy s� koloru orzechowego i stoj� "+
		"dok�adnie na sztorc. Pod nosem cz�owieka uk�adaj� si� r�wno "+
		"przyci�te w�sy, delikatnie podryguj�ce przy ka�dym jego oddechu. ";
		break;
		case 1:
        str+="Kr�tko ostrzy�one w�osy s� koloru popielatego i stoj� "+
		"dok�adnie na sztorc. Pod nosem cz�owieka uk�adaj� si� r�wno "+
		"przyci�te w�sy, a brod� okala kr�tki zarost. ";
		break;
		case 2:
        str+="Jasne jego w�osy s� ostrzy�one przy samej sk�rze, a pod "+
		"nosem uk�adaj� mu si� r�wno "+
		"przyci�te w�sy. ";
		break;
		case 3:
		break;
	}

    str+="Z postury przypomina nieco wysokiego draba, cho� pod "+
	"ubraniem rysuje si� stosunkowo muskularna sylwetka. Mimo wszystko "+
	"wida�, �e jest dobrze wytrenowany, gdy� trzyma wart� nienagannie, a "+
	"kto wie - mo�e i w walce okaza�by si� sprawny.";

	set_long(str+"\n");
	return;
}

void
create_monster()
{
    ustaw_odmiane_rasy("straznik");
    dodaj_nazwy("cz�owiek");
    set_gender(G_MALE);

    random_przym("zaspany:zaspani spokojny:spokojni opanowany:opanowani"+
	" zm�czony:zm�czeni ||umi�niony:umi�nieni muskularny:muskularni"+
	" ciemnooki:ciemnoocy jasnooki:jasnoocy niebieskooki:niebieskoocy", 2);

	losuj_long();
	set_spolecznosc("Bia�y Most");
    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 75000);
    add_prop(CONT_I_HEIGHT, 178);
    add_prop(NPC_I_NO_FEAR, 1);

    add_weapon(BRON);
    add_armour(ZBRO);
    add_armour(BUTY);
    add_armour(SPOD);

    set_stats(({100, 150,150, 150, 150, 150}));

    set_attack_chance(100);
    set_aggressive(&check_living());

    set_act_time(20+random(9));
	add_act("zanuc pod nosem");
	add_act("emote �piewa, potwornie fa�szuj�c: Hej wojenka, wojeeeenka!");
	add_act("zasmiej sie cicho");
	add_act("ziewnij");
	add_act("beknij ukradkiem");
	add_act("czas");
	add_act("podrap sie za uchem");	
	add_act("przetrzyj oczy");
	add_act("popatrz metnie na niebo");
	add_act("westchnij ciezko");
	add_act("':cicho: Psst, s�ysza�em, �e w Rinde mo�na si� nie�le zabawi� "+
		"w zamtuzie, prawda li to?");

	//teksty
	add_act("':weso�o: W tym ca�y ambaras, �eby dwoje chcia�o naraz.");
	add_act("'S�yszeli�cie ostatni� wie��? Pono Lollo ju� nie sprzedaje "+
		"swoich towar�w nieludziom, ech, pies go tr�ca�, tylko prowokuje "+
		"stary cap.");
	add_act("'Witaj w Bia�ym Mo�cie. Prosz� zachowuj spok�j i nie wszynaj "+
		"�adnych burd, to nie b�dziesz mia� z nami na pie�ku!");
	//acty do graczy
	add_act("oczko do kobiety");
	add_act("popatrz niepewnie na elfa");
	add_act("usmiechnij sie zalotnie do kobiety");
	

	add_act("@@do_straznika@@");
	add_act("@@spiewka@@");
    set_cact_time(11+random(19));
    add_cact("krzyknij");
    add_cact("powiedz Dalej comitiva! Naprz�d!");
    add_cact("warknij");
    
	add_ask(({"lollo","sklepikarza"}), VBFC_ME("pyt_o_lollo"));
	add_ask(({"rinde",}), VBFC_ME("pyt_o_rinde"));
    add_ask(({"stra�nik�w","stra�"}), VBFC_ME("pyt_o_straz"));
    add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
    add_ask(({"burmistrza"}),VBFC_ME("pyt_o_burmistrza"));
    set_default_answer(VBFC_ME("default_answer"));

    set_skill(SS_WEP_SWORD,68);
    set_skill(SS_UNARM_COMBAT, 50);
    set_skill(SS_PARRY, 46);
    set_skill(SS_DEFENCE,24);
    set_skill(SS_AWARENESS,64);
    set_skill(SS_BLIND_COMBAT,20);

    create_straznik();
}

void
nastepny_wers(int ktory = 1)
{
	string wers;
	switch(ktory)
	{
		case 1: wers= "Wiernie mienim, nie przemienim."; break;
		case 2: wers= "Kto to wzdruszy?"; break;
		case 3: wers= "Miej go w sercu zaw�dy pewnie."; break;
		case 4: wers= "Dyjabe� b�dzie pan jego duszy! Hej!"; break;
	}
	if(ktory != 4)
		set_alarm(2.0+itof(random(3)),0.0,"nastepny_wers",ktory+1);

	command("emote �piewa weso�o: "+wers);
	return;
}

void
reaguj(int na_co)
{
	switch(na_co)
	{
		case 0:
		if(random(2))
			command("wzrusz ramionami");
		else if(random(2))
			command("':od niechcenia: Taa, s�ysza�em.");
		else
			command("':od niechcenia: Sami sobie na to zapracowali.");
		break;

		case 1:
			if(random(2))
			command("powiedz Taa, i co byndziesz robi�? Zapindala� "+
			"w polu na stare lata?");
			else
			command("powiedz Powiedzia�, co wiedzia�.");
		break;

		case 2:
			if(random(2))
			command("powiedz Ech, nie gadaj, bo mi si� zaraz te� przypomina.");
			else if(random(2))
			command("popatrz z niechecia na buty");
			else
			command("pokiwaj ponuro");
		break;

		case 3:
			if(random(2))
			command("mrugnij pytajaco");
			else if(random(2))
			command("pokrec z usmiechem");
		break;

		case 4:
		command("':sennie: Cholera, przesta�, bo mi te� si� zachciewa.");
		command("splun");
		break;
	}
}

void
reakcja(object kto_reaguje, int na_co)
{
	kto_reaguje->reaguj(na_co);
	return;
}

void
jest_straznik(object kto)
{
	int akcja = random(5);
	switch(akcja)
	{
		case 0:
		command("powiedz do "+OB_NAME(kto)+ " Pono Krepp z Rinde rozpatruje "+
			"ekskomunik� tych, co si� z nielud�mi wi���. S�ysza�e�?");
		break;

		case 1: 
		command("powiedz do "+OB_NAME(kto)+ " Ja to tak sobie my�l�, "+
		"czy warto tak ryzykowa�. Na wschodzie pono� elfy ju� napadaj�, a nam "+
		"po kiego �ywot ryzykowa�? Nie lepiej gdzie zaszy� si� na wsi i spokojny "+
		"�ywot do ko�ca poprowadzi�? Z rodzin�. W komplecie.");
		break;

		case 2:
		command("powiedz do "+OB_NAME(kto)+ " Psia jucha, znowu mam odciski od "+
			"tych cholernych but�w.");
		break;

		case 3:
		command("szturchnij "+OB_NAME(kto));
		break;

		case 4:
		command("ziewnij ukradkiem");
		break;
	}
	set_alarm(2.0+itof(random(3)),0.0,"reakcja",kto,akcja);
}

string
do_straznika()
{
	object *liv = FILTER_LIVE(all_inventory(ENV(TO)));
	foreach(object x : liv)
	{
		if(x == TO)
			continue;
		if(x->query_straznik())
		{
			jest_straznik(x);
			return "";
		}
	}
}

string
spiewka()
{
	if(random(2)) //50% szans na spiewke
		return "";

	set_alarm(2.0+itof(random(3)),0.0,"nastepny_wers");
	command("emote �piewa weso�o: Mi�uj, mi�a, mi�uj wiernie!");
	return "";
}

void powiedz_gdy_jest(object player, string tekst)
{
	if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string pyt_o_lollo()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +" Noo! "+
			"Takem s�ysza�, �e nie sprzedaje on ju� nieludziom. Ech "+
			"dziecka mu kiedy� zat�ukli na trakcie...");
		set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "usmiechnij sie ponuro");
		set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +" "+
			"A, w�a�ciwie..To co mu si� dziwi�? Ech, wsp�czuj� temu "+
			"staremu prykowi.");
        return "";
}
string pyt_o_rinde()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +" "+
			"No gdzie� taaaam jest.");
		set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "wskaz na e");
        return "";
}

string pyt_o_straz()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +" "+
		"Eee, to ciebie pewnikiem nie interesuje, daruj takie grzeczno�ci.");
		set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "westchnij ciezko");

        return "";
}

string pyt_o_garnizon()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +
            "Garnizon znajdziesz na wsch�d od zajazdu, zaraz przed "+
			"p�nocnym wej�ciem do miasta.");
        return "";
}

string pyt_o_burmistrza()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +" A no, "+
		"Mamy jakiego�... Nie radz� z nim zadziera�.");
        return "";
}

string default_answer()
{
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "rozloz rece");
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "'A c� mi do tego?");
        return "";
}
    
int
query_straznik()
{
    return 1;
}

init_living()
{
  ::init_straznik();
}

void random_przym(string str, int n)
{
	mixed* groups = ({ });
        groups = explode(str, "||");
        int groups_size = sizeof(groups);
        if(n > groups_size)
                return;
        for(int i =0; i < groups_size; i++)
                groups[i] = explode(groups[i], " ");
        
        for(int a = 0; a < n; a++)
	{
                int group_index = random(sizeof(groups));
                int adj_index = random(sizeof(groups[group_index]));
                string* tmp = explode(groups[group_index][adj_index], ":");
                dodaj_przym(tmp[0], tmp[1]);
                groups = groups-({groups[group_index]});
        }
}


void emote_hook(string emote, object wykonujacy, string przyslowek)
{
	switch (emote)
	{
		case "klepnij":
		case "poca�uj":
		case "pog�aszcz":
		case "po�askocz":
		case "przytul":
		{
			if(wykonujacy->query_gender())
				set_alarm(1.0, 0.0, "kobieta_dobre", wykonujacy);
			else
				set_alarm(1.0, 0.0, "facet_zle", wykonujacy);
			break;
		}
		case "kopnij":
		case "opluj":
		case "nadepnij":
		{
			if(wykonujacy->query_gender())
				set_alarm(1.0, 0.0, "kobieta_zle", wykonujacy);
			else
				set_alarm(1.0, 0.0, "facet_zle", wykonujacy);
			break;
		}
		case "szturchnij":
		{
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)
				     +" Tak? Mog� w czym� pom�c?");
			break;
		}
	}
}

void kobieta_dobre(object wykonujacy)
{
	string rasa = wykonujacy->query_race();
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)
			+" Odczep si�! �onk� mam!");
}

void kobieta_zle(object wykonujacy)
{
	command("kopnij "+wykonujacy->query_real_name(3));
	set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
				" Hej! Odczep si�, nie prowokuj w�adzy!");
}

void facet_zle(object wykonujacy)
{
	int  i = random(2);
	
	switch (i)
	{
		case 0:
		{
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
				" Nie prowokuj w�adzy!");
			command("kopnij "+OB_NAME(wykonujacy));
			break;
		}
        case 1:
        {
        	admin->add_enemy(wykonujacy->query_real_name(),1);
			command("zabij "+OB_NAME(wykonujacy));
        	break;
        }
	}
}

public string
query_auto_load()
{
    return 0;
}
