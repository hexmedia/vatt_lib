/* Autor: Duana, zamawianie kapieli i wszystko zwiazane z laznia - Vera.
  Opis : sniegulak
  Data : 28.05.2007

	UWAGA NA FUNKCJE jebut()!!! (czytaj ni�ej) V.
*/

inherit "/std/monster";

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <object_types.h>
#include "dir.h"

//tj. 5 denarow
#define CENA_KAPIELI	100
//z Bia�ego Mostu p�ac� tylko 1 denara
#define CENA_KAPIELI_MIESZKANCA	20

string *imie;
object laznia;

//!!!!!!!!!!!!!==============AAAA CZY TO NICZEGO NIE POPSUJE?!
//CHODZI O TO, ZE JEST TAKI ZJEBANY BLAD - JAK RAZ WEJDZIE SIE DO LAZNI, TO
//MOZNA JUZ CALY CZAS (ADD_ACT(BLOK,"",1); W INIT() NIE DZIA�A, ALE
//JAK WYWOLUJE INIT ZA KAZDYM RAZEM, TO DZIAUA!
//I SAM W GLOWE ZACHODZE, DLACZEGO TAK JEST...
//NIE MAM POJECIA NA CZYM TO POLEGA, DLATEGO NIE WIEM CZY TO CZEGOS NIE POPSUJE
void
jebut()
{
	init();
}

void
create_monster()
{
	LOAD_ERR(BIALY_MOST_LOKACJE+"laznia");
	laznia = find_object(BIALY_MOST_LOKACJE+"laznia");

    set_living_name("nestir");

    ustaw_odmiane_rasy("m^e^zczyzna");
    set_gender(G_MALE);
    imie=({"nestir","nestira","nestirowi","nestira",
        "nestirem","nestirze"});
    ustaw_imie(imie,PL_MESKI_OS);

    dodaj_nazwy("karczmarz");

    set_long("Sporej postury m^e^zczyzna, r^ownie sporym brzuchem, wygl^ada "+
        "na osob^e, kt^ora zajmuje si^e tym zajazdem. Pulchna twarz z ma^lymi "+
        "niebieskimi oczami, i w^asami zakr^econymi do g^ory, sprawia wra^zenie "+
        "bardzo przyjaznej. Zmarszczki powsta^le dooko^la ust wskazuj^a, i^z "+
        "cz^lowiek ten u^smiecha si^e nader cz^esto i ma przyjazne nastawienie "+
        "do podr^o^znych. Ubrany w ciemnobr^azowy zapinany na przedzie ^zupan, "+
        "przewi^azany bia^lym czystym fartuchem, co chwila rzuca spojrzenia "+
        "na go^sci. Czujny wzrok wy^lawia klient^ow, kt^orzy chcieliby co^s "+
        "zam^owi^c.\n");

        dodaj_przym("gruby","grubi");
        dodaj_przym("w^asaty","w^asaci");

    set_stats(({ 55, 40, 52, 50, 50, 40}));
    set_skill(SS_PARRY, 45);
        set_skill(SS_DEFENCE, 50);
        set_skill(32, 60);

    add_prop(CONT_I_WEIGHT, 100000);
    add_prop(CONT_I_HEIGHT, 160);
    add_prop(LIVE_I_NEVERKNOWN, 0);

    set_act_time(30);
    add_act("emote przeciera kufel fartuchem.");
    add_act("emote czujnym wzrokiem szuka klienta.");
    add_act("emote podchodzi do jednego ze sto^l^ow i przeciera go szmat^a.");
    add_act("emote sapie przeci^agle.");
    add_act("emote w zadumie zaczyna d^luba^c sobie w z^ebach.");

    
    add_armour(BIALY_MOST_UBRANIA + "fartuchy/fartuch_bialy_czysty.c");
    add_armour(BIALY_MOST_UBRANIA + "zupany/zupan_ciemnobrazowy_dlugi.c");

    add_ask(({"karczm^e"}), VBFC_ME("pyt_o_karczme"));
    add_ask(({"potrawy"}), VBFC_ME("pyt_o_potrawy"));
    add_ask(({"prac^e"}), VBFC_ME("pyt_o_prace"));
    add_ask(({"zadanie"}), VBFC_ME("pyt_o_zadanie"));
	add_ask(({"bali^e","^la^xnie","k^apiel"}), VBFC_ME("pyt_o_kapiel"));

    set_default_answer(VBFC_ME("default_answer"));

    set_alarm_every_hour("co_godzine");
    set_wzywanie_strazy(1);
        set_reakcja_na_walke(0);
}

int
query_karczmarz()
{
	return 1;
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
pyt_o_karczme()
{
    set_alarm(0.5, 0.0, "command_present", TP,
        "powiedz Ta karczma to wszystko co posiadam. ");
}
string
pyt_o_potrawy()
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
        "powiedz Wszystko co mog^e ci poda^c mo^zesz znale^x^c wypisane na "+
        "tablicy. ");    
}

string
pyt_o_prace()
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
        "powiedz Nie trza mi tu nikogo do pracy. ");
    return "";
}

string
pyt_o_zadanie()
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
        "powiedz ^Zadnego zadania u mnie nie znajdziesz. ");   
}

string
pyt_o_kapiel()
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
        "powiedz A i owszem, wyk^apa^c si^e u nas mo^zna. Zam^owienie k^apieli jeno "+
			"pi^e^c reda^nskich denar^ow bedzie kosztowa^c.");
    return "";
}

string
default_answer()
{
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
            "powiedz Mam zbyt du^zo pracy by z tob^a rozmawia^c o jaki^s "+
            "pierdo^lach. ");
    return "";
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
            set_alarm(1.0, 0.0, "command_present", wykonujacy,
           "emote m^owi z oburzeniem: Nie pozwol^e sob^a tak pomiata^c!");
            break;

        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
            set_alarm(1.0, 0.0, "command_present", wykonujacy,
                "emote rozgl^ada si^e szybko, oblewaj^ac si^e rumie^ncem.");
            break;
    }
}


void
oddajemy(object ob, object old)
{
	if(ob->query_type() == O_MONETY) // :-)
	{
		command("dygnij");
		command("powiedz Dzi^ekuj^e.");
		return;
	}

    if (interactive(old) && (ENV(old) == ENV(TO))) {
        command_present(old, "emote m^owi: Nie interesuj^a mnie takie rzeczy.");

        command_present(old, "odloz " + OB_NAME(ob));
        return;
    }

}


void
add_introduced(string imie, string biernik)
{
    set_alarm(1.0, 0.0, "return_introduce", imie);
}

void
zamknij_drzwi()
{
	command("zamknij drzwi");
}

void
sprawdz_czy_wszedl(object kto)
{
	if(ENV(kto) == laznia)
		zamknij_drzwi();
}

int
sprawdz_czy_zajete()
{
	/*if(!objectp(co))
	{
		write("Szlag by to trafi^l! Wyst^api^l b^l^ad o numerze xx4wa3 "+
			"w zamawianiu k^apieli. Zg^lo^s to natychmiast!\n");
		return 1;
	}*/

	if(sizeof(FILTER_PLAYERS(all_inventory(laznia))))
		return 1;
	else
		return 0;
}

void
usun_propa(object komu)
{
	if(objectp(komu))
		komu->remove_prop(ZAPLACIL_ZA_LAZNIE);
}

//od zamawiania kapieli
int
zamawiamy(string str)
{
	int czy_pochodzimy = 0;

	if(stringp(str) && str ~= "k^apiel")
	{
		
		if(!objectp(laznia))
		{
			write("Szlag by to trafi^l! Wyst^api^l b^l^ad o numerze 234aw5 "+
				"w zamawianiu k^apieli. Zg^lo^s to natychmiast!\n");
			return 1;
		}

		if(sprawdz_czy_zajete())
		{
			set_alarm(0.5, 0.0, "command_present", TP,
				 "powiedz Och, w^la^snie kto^s bierze k^apiel. "+
					"Dy^c zara wyjdzie, to i wa^s^c si^e umyje.");
			return 1;
		}

		if(TP->query_origin() ~= "z Bia^lego Mostu")
			czy_pochodzimy = 1;

		if(czy_pochodzimy)
		{
			if(!MONEY_ADD(TP,-CENA_KAPIELI_MIESZKANCA))
			{
			    set_alarm(0.5,0.0,"command_present",TP,
				"powiedz Pani"+TP->koncowka("e","","e")+
				" przykro mi, ale nie sta^c Was na k^apiel. "+
				"Dla Was to kosztuje tylko jednego denara.");
			    return 1;
			}
		}
		else if (!MONEY_ADD(TP, -CENA_KAPIELI))
	    {
			set_alarm(0.5, 0.0, "command_present", TP,
				 "powiedz Pani"+TP->koncowka("e","","e")+", co mi tu pokazujecie? "+
				"Grzanie wody kosztuje! Pi^e^c bitych reda^nskich denar^ow i "+
				"ani grosza mniej.");
			return 1;
	    }

		LOAD_ERR("/d/Standard/Redania/Bialy_Most/lokacje/drzwi/do_lazni");
		object drzwi_lazni = 
				find_object("/d/Standard/Redania/Bialy_Most/lokacje/drzwi/do_lazni");
		if(!objectp(drzwi_lazni))
		{
			write("Szlag by to trafi^l! Wyst^api^l b^l^ad o numerze 2d4he33 "+
			"w zamawianiu k^apieli. Zg^lo^s to natychmiast!\n");
			return 1;
		}

		write("P^lacisz nale^zn^a kwot^e.\n");
		TP->add_prop(ZAPLACIL_ZA_LAZNIE,1);
		set_alarm(60.0, 0.0, "usun_propa",TP);

		//np. ktos zakonczyl w lazni, a wczesniej sie zamknal.
		if(drzwi_lazni->query_locked())
		{
			write("Karczmarz doskakuje szybko do drzwi prowadz^acych do ^la^xni "+
			"i gmera co^s przy nich. Po chwili od strony drzwi s^lycha^c ciche "+
			"klikni^ecie.\n");
			drzwi_lazni->do_unlock_door("");
		}

		set_alarm(0.5, 0.0, "command_present", TP,
			 "otworz zwykle drewniane drzwi");
		set_alarm(0.5, 0.0, "command_present", TP,
			 "wskaz na zwykle drewniane drzwi");
		set_alarm(0.5, 0.0, "command_present", TP,
			 "powiedz A prosim, prosim do ^la^xni. Wody ju^z nalano!");

		set_alarm(5.0,0.0,"sprawdz_czy_wszedl",TP);
		return 1;
	}

	return 0;
}



nomask int
blok(string str)
{
    string verb = query_verb();
	/*if(verb == "drzwi" && str ~= "do �a�ni")
		verb == "drzwi do �a�ni";*/
    string *nie_dozwolone = ({"�a�nia","drzwi do �a�ni"});

    if(member_array(verb,nie_dozwolone) == -1)
     	return 0;

	if(TP->query_leader())
		if((TP->query_leader())->query_prop(ZAPLACIL_ZA_LAZNIE))
			return 0;
/*3 najwieksze paradoksy:
1. Ch*j nie ma nogi, a stoi.
2.Ci*a mokra, a nie rdzewieje.
3. Swiat jest okragly, a ludzie pie**ola sie po katach!
*/

	if(sprawdz_czy_zajete())
	{
        write("Karczmarz jednym wielkim susem doskakuje do ciebie i "+
		"zagradza ci drog�.\n");
        saybb("Karczmarz jednym wielkim susem doskakuje do "+
              QIMIE(TP,PL_DOP) + " i zagradza "+
			TP->koncowka("mu","jej","temu")+" drog� do �a�ni.\n");
		set_alarm(0.5, 0.0, "command_present", TP,
			 "powiedz Przykro mi, w^la^snie kto^s bierze k^apiel. "+
				"Dy^c zara wyjdzie, to i wa^s^c si^e umyje.");
		return 1;
	}
	else if(!TP->query_prop(ZAPLACIL_ZA_LAZNIE))
	{
        write("Karczmarz zastawia ci drog� do �a�ni.\n");
        saybb("Karczmarz zastawia drog� " + 
              QIMIE(TP,PL_CEL) + " do �a�ni.\n");

		if(TP->query_prop(LIVE_O_LAST_ROOM) == laznia)
        set_alarm(1.0, 0.0, "command_present", TP,
              "powiedz do "+TP->query_name(PL_DOP) +
              " Ej�e! Pan"+TP->koncowka("a","i","a")+" czas"+
			" ju� si� sko�czy�, nast�pni klienci czekaj�.");
		else
        set_alarm(1.0, 0.0, "command_present", TP,
              "powiedz do "+TP->query_name(PL_DOP) +
              " Ej�e! Pan"+TP->koncowka("","i","")+" nie zamawia�"+
			TP->koncowka("","a","o")+
			" k�pieli u mnie. Prosz�, prosz�! Jedynie pi�� denar�w. "+
			"Dla s�siad�w jeden.");
		return 1;
    } 

     return 0;
}

/*int
asdf()
{
write("asdf!\n");
return 0;
}*/

void
init()
{
	add_action(blok, "", 1);
//add_action(asdf,"",1);
	add_action(zamawiamy, "zam^ow");
	::init();
}


nomask void
signal_enter(object ob, object skad)
{
	jebut();

    if(!skad || !laznia)
        return;
    
	if(file_name(skad) == file_name(laznia))
	{
		set_alarm(2.0,0.0,"zamknij_drzwi");
	}
}
int
signal_leave(object kto, object dokad)
{
    if (dokad->query_pub())
	return 0;

    if(sizeof(filter(deep_inventory(kto), &->query_naczynie())) > 0 )
    {
//	kto->command("poloz naczynia na stole");
	tell_object(kto, "Oddajesz wszystkie naczynia karczmarzowi.\n");
	filter(deep_inventory(kto), &->query_naczynie())->remove_object();
	return 0;
    }
}
void
start_me()
{
    set_alarm(1.0,0.0,"sprzatamy");
}
void
sprzatamy()
{
    int i;
    object *wszystko=all_inventory(this_object());   
    command("wez puste naczynia");
    set_alarm(1.0,0.0,"command","zdejmij puste naczynia ze stolu");
    set_alarm(3.0,0.0,"command","zdejmij puste naczynia z drugiego stolu");
    set_alarm(7.0,0.0,"myjemy");
    set_alarm(600.0,0.0,"sprzatamy");
}
void
myjemy()
{
    int i;
    object *wszystko=all_inventory(this_object());   
    for(i=0;i<sizeof(wszystko);i++)
    {
        if(wszystko[i]->query_naczynie())
            wszystko[i]->remove_object();
    }
}

