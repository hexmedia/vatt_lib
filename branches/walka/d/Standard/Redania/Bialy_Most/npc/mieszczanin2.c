/* Opis & tworca: Valor
   Data: 17.06.07
*/

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit BIALY_MOST_STD_NPC;

int czy_walka();
int walka = 0;

void
create_most_npc()
{
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_gender(G_MALE);
    dodaj_nazwy("mieszczanin");

    set_long("Gdyby nie nieliczne, lecz g^lebokie zmarszczki, ^smia^lo "+
        "mo^zby stwierdzi^c, ^ze m^e^zczyzna ten jest jeszcze "+
        "m^lodzie^ncem. W^sr^od d^lugich, kasztanowych w^los^ow "+
        "pr^o^zno szuka^c cho^cby jednego siwego pasmka, a spojrzenie "+
        "wci^a^z ma bystre i zdecydowane. Szczup^la, lecz zarazem "+
        "muskularna sylwetka wskazuje, ^ze m^e^zczy^xnie nie obca jest "+
        "ci^e^zka praca, co daje si^e zauwa^zy^c po jego spracowanych "+
        "d^loniach. Ciemnobr^azowe spodnie, kt^ore nosi wpuszczone s^a "+
        "w do^s^c wysokie czarne, sk^orzane buty. Ma na sobie r^ownie^z "+
        "lnian^a, jasn^a koszule, wi^azan^a pod szyj^a rzemieniem. \n");

    dodaj_przym("dojrza^ly","dojrzali");
    dodaj_przym("kasztanowow^losy","kasztanowow^losi");

     set_stats (({ 70, 60, 70, 50, 50, 60 }));

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 100000);
    add_prop(CONT_I_HEIGHT, 185);
  
    set_act_time(30);
    add_act("usmiechnij sie lekko do mezczyzny");
    add_act("usmiechnij sie milo do kobiety");
    add_act("podrap sie po glowie");
    add_act("emote pociera swoje r^ece.");
    add_act("rozejrzyj sie sennie");
    add_act("hmm");
    add_act("powiedz :do przechodz^acej kobiety: pi^eknie pani dzi^s wygl^ada.");
    add_act("emote k^lania si^e uprzejmie przechodz^acemu m^e^zczy^znie.");
    set_cchat_time(10);
    add_cchat("Lepiej mnie zostaw albo pogruchotam ci wszystkie ko^sci!");
    add_cchat("Wyno^s si^e stad! To spokojne miasto!");
    add_cchat("Uprzedzam, ^ze b^edzie bola^lo!");

    add_armour(BIALY_MOST_UBRANIA + "koszule/lniana_prosta_Mc.c");
    add_armour(BIALY_MOST_UBRANIA + "spodnie/ciemnobrazowe_skorzane_Mc.c");
    add_armour(BIALY_MOST_UBRANIA + "buty/czarne_zniszczone_Mc.c");

    add_ask(({"kram","kramy","kramy rybne","ryby"}), VBFC_ME("pyt_o_ryby"));
    add_ask(({"Bia^ly Most","miasto"}), VBFC_ME("pyt_o_miasto"));
    add_ask(({"prac^e"}), VBFC_ME("pyt_o_prace"));
    add_ask(({"pomoc"}), VBFC_ME("pyt_o_pomoc"));
    set_default_answer(VBFC_ME("default_answer"));

    set_random_move(100);
    set_restrain_path(BIALY_MOST_LOKACJE_ULICE);
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_ryby()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Nie lubie ryb, ale je^sli chcesz mo^zesz poszuka^c kram^ow w "+
            "mie^scie, gdzie^s napewno s^a.");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Nie widzisz, ^ze walcze o ^zycie?!");
    }
    return "";
    
}
string
pyt_o_miasto()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Ot, zwyk^la mie^scina, nic si^e tu ciekawego nie dzieje. ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Nie widzisz, ^ze walcze o ^zycie?!");
    }
    return "";
    
}

string
pyt_o_prace()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Nie, nie potrzebuje twojej pomocy.");
    }
else
    {
    set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
           "powiedz powiedz Nie widzisz, ^ze walcze o ^zycie?!");
    }
    return "";
    
}

string
pyt_o_pomoc()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz ^Swietnie sobie radz^e bez niczyjej pomocy.");
    }
    else
    {
    set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Nie widzisz, ^ze walcz^e o ^zycie?!");
    }
    return "";
}
string
default_answer()
{
    if(!query_attack())
    {
    switch(random(4))
    {
        case 0:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Nie wiem o co ci chodzi."); break;
    case 1:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Daj mi spok^oj."); break;
        case 2:set_alarm(2.0, 0.0, "command_present", this_player(),
                "wzrusz ramionami"); break;
        case 3:set_alarm(2.0, 0.0, "command_present", this_player(),
                "powiedz Zapytaj kogo^s innego."); break;

    }
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
           "powiedz Nie widzisz, ^ze walcz^e o ^zycie?!");
    }
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("powiedz Nie obchodzi mnie twoje imi^e.");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "zuy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "zuy", wykonujacy);
                    break;
        case "opluj" : set_alarm(1.5, 0.0, "zuy", wykonujacy);
                    break;
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
              {
              if(wykonujacy->query_gender() == 1)
                  {
              switch(random(3))
                        {
                 case 0:command("usmiechnij sie uprzejmie");
                        break;
                 case 1:command("powiedz W sumie nie mam ^zony.");
                        break;
                 case 2:command("powiedz Chcesz si^e spotka^c?");
                        break;
                        }
                  }
                     else
                        {
                   switch(random(3))
                        {
                   case 0:command("skrzyw sie");
                          break;
                   case 1:command("powiedz Odejd^x! Wol^e kobiety!");
                          break;
                   case 2:command("spojrzyj z obrzydzeniem na "+
                        OB_NAME(wykonujacy));
                          break;
                        }
                  }    
               }
                          break;
    }
}
void
zuy(object kto)
{
   switch(random(3))
   {
      case 0: set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto,
          "powiedz Odwal si^e! ");
              break;
      case 1: set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto,
          "powiedz Nie denerwuj mnie albo po^za^lujesz! ");
              break;
      case 2: set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto,
          "warknij");
              break;
   }
} 

void attacked_by(object wrog)
{
    if(walka == 0)
    {
        walka = 1;
        set_alarm(0.5,0.0,"command","krzyknij Wynocha!");
        set_alarm(15.0, 0.0, "czy_walka", 1);
        return ::attacked_by(wrog);
    }
    else
    {
        return ::attacked_by(wrog);
    }
}
int
czy_walka()
{
    if(!query_attack())
    {
        set_alarm(0.5,0.0,"command","usmiech krzywo");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}