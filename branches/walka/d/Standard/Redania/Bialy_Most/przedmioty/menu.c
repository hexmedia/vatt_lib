/* Autor: Avard
   Data : 06.05.07 */

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
int przeczytaj_menu();

create_object()
{
    ustaw_nazwe("tabliczka");
    dodaj_nazwy("tablica");
    dodaj_nazwy("menu");

    set_long("    _________________________________________\n   / o\t\t       MENU\t\t    o \\\n  |"+
             "\t\tDania g^l^owne:\t\t      |\n"+
             "  |\tKurczak z dodatkami\t72 grosze     |\n"+
             "  |\tKasza ze skwarkami\t4 grosze      |\n"+
             "  |\tPolewka rybna\t\t5 groszy      |\n"+
             "  |\tBarszcz\t\t\t5 groszy      |\n"+
             "  |\t\t\t\t\t      |\n  |\t\t\t\t\t      |\n"+
             "  |\t\tNapoje:\t\t\t      |\n"+
             "  |\tWoda\t\t\t1 grosze      |\n"+
             "  |\tKompot jab�kowy\t\t3 grosze      |\n"+
             "  |\tJasne piwo\t\t10 groszy     |\n"+
             "  |\tCiemne piwo\t\t11 groszy     |\n"+
	     "  |\t\t\t\t\t      |\n  |\t\t\t\t\t      |\n"+
	     "  |\tMo�na i u mnie k�piel zam�wi�         |\n"+
	     "  |\t5 denar�w dla go�ci, ziomkowie po 1   |\n"+
             "  \\__________________________________________/\n"
              );

    add_prop(OBJ_I_WEIGHT, 485);
    add_prop(OBJ_I_NO_GET, "Tabliczka zosta^la mocno przybita do kontuaru.\n");
    add_prop(OBJ_I_DONT_GLANCE,1);

    add_cmd_item(({"menu","tabliczk^e"}), "przeczytaj", przeczytaj_menu,
                   "Przeczytaj co?\n"); 
}

int
przeczytaj_menu()
{
   return long();
}
