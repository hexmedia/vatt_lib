

inherit "/std/sakiewka.c";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include <materialy.h>

void
create_sakiewka()
{
	ustaw_nazwe("sakiewka");
    dodaj_przym("pojemny","pojemni");
    dodaj_przym("materia�owy","materia�owi");
	set_long("Do^s^c du^zy materia^lowy woreczek, s^lu^z^acy do przechowywania monet "
	+"jest obszyty cienk^a, srebrn^a nici^a i ma w g^ornej cz^e^sci wyszyte "
	+"litery 'BM' wskazuj^ace na to, i^z wykonany zosta^l w Bia^lym Mo^scie. "
	+"Sakiewka jest dwukolorowa, sp�d i otoczka jest br^azowa, reszta "
	+"za^s ma rzucaj^acy si� w oczy jaskrawy zielony kolor, praktyczny i �adny. "
	+"Posiada rzemienn^a p�telk� dzi�ki kt�rej mo�esz przypi^a� ten woreczek "
	+"do pasa. @@opis_sublokacji|Dostrzegasz w niej |w|.\n|\n||"
	+ PL_BIE+ "@@");


    add_prop(CONT_I_MAX_VOLUME, 5000);
    add_prop(CONT_I_VOLUME, 860);

    add_prop(CONT_I_WEIGHT, 750);
    add_prop(CONT_I_MAX_WEIGHT, 3750);

	ustaw_material(MATERIALY_TK_LEN , 95);
	ustaw_material(MATERIALY_SZ_SREBRO, 5);
	
	add_prop(OBJ_I_VALUE, 410);

}
