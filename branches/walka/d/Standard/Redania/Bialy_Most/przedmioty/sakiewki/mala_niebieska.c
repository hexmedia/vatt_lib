/*
 * faevka, dn. 13.06.07
 * opis Anonima (pewnie Tinardan ;))
 *
 */

inherit "/std/sakiewka.c";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include <materialy.h>

void
create_sakiewka()
{
	ustaw_nazwe("sakiewka");
    dodaj_przym("ma^ly","mali");
    dodaj_przym("niebieski","niebiescy");
	set_long("Ta do^s^c ma^la sakiewka, kt^ora bez trudu zmie^sci si^e w "+
		"d^loni, zrobiona zosta^la z delikatnego i przyjemnego w dotyku "+
		"materia^lu. Sk^or^e zafarbowano na ciesz^acy oko, niebieski kolor, "+
		"kt^ory swoim odcieniem przypomina nieco bezchmurne niebo w ciep^ly, "+
		"wiosenny dzie^n. Sakiewka jest na tyle du^za, ^ze pomie^sci sporo "+
		"drobnych rzeczy, kt^ore bez obawy mo^zna w niej przechowywa^c i "+
		"wywija^c ni^a na wszystkie strony, ze wzgl^edu na to, i^z posiada "+
		"na g^orze dwa grube sznureczki, kt^orymi mo^zna zwi^aza^c ca^lo^s^c "+
		"i nie martwi^c si^e o sw^oj dobytek, gdy^z s^a niezwykle "+
		"wytrzyma^le. . @@opis_sublokacji|Dostrzegasz w niej |w|.\n|\n||"
		+ PL_BIE+ "@@");


    add_prop(CONT_I_MAX_VOLUME, 510);
    add_prop(CONT_I_VOLUME, 110);

    add_prop(CONT_I_WEIGHT, 400);
    add_prop(CONT_I_MAX_WEIGHT, 1200);

	ustaw_material(MATERIALY_SK_KROLIK , 95);
	ustaw_material(MATERIALY_TK_KORDONEK, 5);
	
	add_prop(OBJ_I_VALUE, 209);

}
