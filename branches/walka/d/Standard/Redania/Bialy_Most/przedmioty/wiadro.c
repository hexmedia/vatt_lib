/* Autor: Duana
  Opis : sniegulak
  Data : 23.05.2007
*/

inherit "/std/container";

#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "/sys/formulas.h"
#include "dir.h"

void
create_container()
{
    ustaw_nazwe("wiadro");
    dodaj_przym("drewniany", "drewniani");
    dodaj_przym("solidny", "solidni");

    set_long("Drewniane wiaderko zrobione z prostych desek zaci�ni�tych za "+
        "pomoc� stalowych obr�czy wygl�da na do�� solidne i pojemne.\n");
         
    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 270);
    set_type(O_NARZEDZIA);

    ustaw_material(MATERIALY_DR_DAB, 90);
    ustaw_material(MATERIALY_RZ_STAL, 10);
    set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));

}