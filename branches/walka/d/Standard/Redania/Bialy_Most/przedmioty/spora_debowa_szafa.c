/** @package

        spora_debowa_szafa.c
        
        Copyright(c) ppp 2000
        
        Author: Eleb
        Created: E   2007-04-17 11:20:21
Last change: E 2007-04-18 01:52:27
*/
inherit "/std/receptacle";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "dir.h"

void
create_receptacle()
{
    ustaw_nazwe("szafa",PL_MESKI_NOS_NZYW);
    dodaj_przym("spory", "sporzy");
    dodaj_przym("d^ebowy", "d^ebowi");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);

    set_long("Mebel ten zosta^l zbity z solidnych i grubych desek, kt^orych"+
        " powierzchnia"+
        " zosta^la wy^z^lobiona w rozmaite wzory, g^l^ownie przedstawiające"+
        " ryby."+
        " Drzwiczki"+
        " do szafy zaopatrzone sa w masywny mosie^zny zamek.\n"+
        "@@opis_sublokacji|Dostrzegasz na niej |na|.\n||" +
        PL_BIE+ "@@"+
        "@@opis_sublokacji|Dostrzegasz pod nia |pod|.\n||" +
        PL_BIE+ "@@");
    add_prop(CONT_I_RIGID, 1);
    add_prop(CONT_I_MAX_VOLUME, 100000);
    add_prop(CONT_I_MAX_WEIGHT, 200000);
    add_prop(CONT_I_VOLUME, 40000);
    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_NO_OPEN_DESC,1);
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 1000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 1000);
    set_owners(({BIALY_MOST_NPC + "burmistrz"}));
    add_subloc("pod");
    add_subloc_prop("pod", CONT_I_MAX_WEIGHT, 1000);
    add_subloc_prop("pod", CONT_I_MAX_VOLUME, 1000);
    
}
