/*
 * ^Lawka do ^la^xni w zaje^xdzie BM
 * opis by Edrain
 * zepsula faeve
 * dn. 28 maja 2007
 */

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>


create_object()
{
    ustaw_nazwe("^lawa");

    dodaj_przym("pot^e^zny","pot^e^zni");
    dodaj_przym("d^lugi","d^ludzy");

    make_me_sitable("na","na ^lawie","z ^lawy",5, PL_MIE,
"na");

    ustaw_material(MATERIALY_DR_DAB);

    set_long("Pot^e^zna d^luga ^lawa, stoj^aca obok sto^lu, s^lu^zy "+
		"niew^atpliwie do spocz^ecia po trudach w^edr^owki, cho^c jej "+
		"wygod^e mo^zna zakwestionowa^c. Twarde, drewniane siedzisko uwiera "+
		"w rzy^c i, przy d^lu^zszym siedzeniu na tym miejscu, powoduje "+
		"dr^etwienie po^sladk^ow. Mebel ten wykonany zosta^l z drewna "+
		"d^ebowego, dzi^eki czemu doskonale pasuje do sto^l^ow i ca^lego "+
		"wystroju wn^etrza.\n");
    
    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 52513);
    add_prop(OBJ_I_VALUE, 210);
    add_prop(OBJ_I_NO_GET, "Taaak? A gdzie usi^adziesz, jak nast^epnym razem "+
		"zachce ci si^e upi^c?");
    set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));
}

public int
query_type()
{
    return O_MEBLE;
}
