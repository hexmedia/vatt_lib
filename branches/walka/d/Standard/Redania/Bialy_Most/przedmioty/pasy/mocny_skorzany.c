inherit "/std/belt";
#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>

create_belt()
{
    ustaw_nazwe("pas");
    dodaj_nazwy("pasek");
    dodaj_przym("mocny", "mocni");
    dodaj_przym("sk^orzany", "sk^orzani");
    
    set_long("Pas ten zrobiony jest z ciemnobr^azowej sk^ory, kt^ora po " +
    "porz^adnym wygarbowaniu jest w miar^e mocna i do^s^c gi^etka. " +
    "Sprz^aczka zrobiona z miedzi nie posiada ^zadnych zdobie^n, co " +
    "powoduje, i^z pas ten wygl^ada jednocze^snie prosto i schludnie. " +
    "Du^za ilo^s^c dziurek pozwala nosi^c ten pas osobom szczup^lym, jak " +
    "i posiadaj^acym do^s^c sporych rozmiar^ow brzuch.\n");
    
    set_max_slots_zat(2);
    set_max_slots_prz(4);

    set_max_weight_zat(3600);
    set_max_weight_prz(3800);

    set_max_volume_zat(2600);
    set_max_volume_prz(1500);
    
    add_prop(OBJ_I_WEIGHT, 3000);
    add_prop(OBJ_I_VOLUME, 1550);

    add_prop(OBJ_I_VALUE, 1378);
    
    ustaw_material(MATERIALY_SK_BYDLE, 90);
    ustaw_material(MATERIALY_MIEDZ, 10);
}
