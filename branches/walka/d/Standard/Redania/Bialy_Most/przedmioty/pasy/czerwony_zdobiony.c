/* 
 * faeve, 13.06.07
 * opis znowu Anonima
 *
 */

inherit "/std/belt";
#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>

create_belt()
{
    ustaw_nazwe("pas");
    dodaj_nazwy("pasek");
    dodaj_przym("czerwony", "czerwoni");
    dodaj_przym("ozdobny", "ozdobni");
    
    set_long("Ten niezwykle szeroki pas, zosta^l wykonany z cie^nkiej lecz "+
		"elastycznej sk^ory, dzi^eki czemu, bez trudu b^edzie m^og^l obj^a^c "+
		"prawie ka^zde cia^lo. Pas zrobiono z tak^a elegancj^a, ^ze mo^ze "+
		"wzbudza^c niema^le zainteresowanie, tym bardziej, ^ze na ca^lej "+
		"d^lugo^sci ozdobiony zosta^l b^lyszcz^acym haftem. Sk^or^e "+
		"pofarbowano na krwistoczerwony kolor, a po^lyskuj^aca czer^n haftu "+
		"idealnie z nim wsp^o^lgra. Srebrna klamra dumnie prezentuje si^e na "+
		"tle ca^lo^sci i ponadto, swoim delikatnym kszta^ltem dodaje tylko "+
		"uroku. \n");
    
    set_max_slots_zat(2);
    set_max_slots_prz(1);
    set_max_weight_zat(3800);
    set_max_weight_prz(900);
    set_max_volume_zat(2000);
    set_max_volume_prz(2000);
    
    add_prop(OBJ_I_WEIGHT, 3100);
    add_prop(OBJ_I_VOLUME, 1200);
	add_prop(OBJ_I_VALUE, 1504);
    
    ustaw_material(MATERIALY_SK_KROLIK, 90);
    ustaw_material(MATERIALY_TK_MULINA, 10);
}
