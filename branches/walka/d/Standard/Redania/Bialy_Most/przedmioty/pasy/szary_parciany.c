/* 
 * faeve, 13.06.07
 * opis: Oldzio
 *
 */

inherit "/std/belt";
#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>

create_belt()
{
    ustaw_nazwe("pas");
    dodaj_nazwy("pasek");
    dodaj_przym("szary", "szarzy");
    dodaj_przym("parciany", "parciani");
    
    set_long("G^esty splot tego pasa niemal obiecuje wytrzyma^lo^s^c owego "+
		"wyrobu, jednak liczne przetarcia i wisz^ace gdzieniegdzie nitki "+
		"bawe^lny zdradzaj^a jego zak^lamany wizerunek. Cho^c na poz^or "+
		"mocny, w rzeczewisto^sci jest got^ow rozpa^s^c si^e w ka^zdej "+
		"chwili. Mosi^e^zna, krzywo przymocowana, klamra ledwo trzyma si^e "+
		"na miejscu. \n");
    
    set_max_slots_zat(2);
    set_max_slots_prz(1);
    set_max_weight_zat(2200);
    set_max_weight_prz(2500);
    set_max_volume_zat(2200);
    set_max_volume_prz(2000);
    
    add_prop(OBJ_I_WEIGHT, 600);
    add_prop(OBJ_I_VOLUME, 1350);
	add_prop(OBJ_I_VALUE, 416);
    
    ustaw_material(MATERIALY_TK_BAWELNA, 90);
    ustaw_material(MATERIALY_RZ_MOSIADZ, 10);
}
