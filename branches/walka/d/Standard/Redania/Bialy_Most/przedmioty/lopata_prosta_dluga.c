/* Autor: Duana
  Opis : sniegulak
  Data : 23.05.2007
*/

inherit "/std/holdable_object";

#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "/sys/formulas.h"
#include "dir.h"

void
create_holdable_object()
{
    ustaw_nazwe("^lopata");
    dodaj_przym("prosty", "pro^sci");
    dodaj_przym("d^lugi", "d^ludzy");

    set_long("Drewniany trzonek z oheblowanego drewna zosta^l zako^nczony "+
        "p^laska, niezbyt dok^ladnie wykut^a p^lyta zrobion^a ze stali.\n");
         
    add_prop(OBJ_I_WEIGHT, 7000);
    add_prop(OBJ_I_VOLUME, 7000);
    add_prop(OBJ_I_VALUE, 720);
    set_type(O_NARZEDZIA);

    ustaw_material(MATERIALY_RZ_STAL, 30);
    ustaw_material(MATERIALY_DR_DAB, 70);
    set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));

}