/*
 *
 * Opis jaki^s stary wynalaz^lam
 * w odm^etach dysku mego,
 * tako^z przerobi^lam 
 * i oto jest.
 * Faevka, 13.06.07
 *
 * M^OJ PRZEZAJEBISTY PLECAK MA KIESZE^N!!!!
 *
 */

inherit "/std/plecak.c";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>

void
create_backpack()
{
	ustaw_nazwe("plecak");
	dodaj_przym("czarny","czarni");
	dodaj_przym("sk^orzany","sk^orzani");
	odmien_short();
	set_long("Ten do^s^c du^zy, wykonany z grubej i solidnej czernionej "+
		"sk^ory plecak wygl^ada na ca^lkiem pojemny - w ^srodku podzielony "+
		"jest na kilka praktycznych przegr^odek. Jedyne jego zamkni^ecie "+
		"stanowi rzemyk przewleczony przez niewielkie otwory w kraw^edziach "+
		"plecaka. Dodatkowo z zewn^atrz przyszyto niewielk^a kieszonk^e na "+
		"jakie^s drobiazgi. Szelki plecaka mo^zna regulowa^c, dlatego "+
		"powinien nada^c si^e on zar^owno dla pot^e^znych os^ob jak i "+
		"male^nkich nizio^lk^ow.\n");

	set_keep(1);

	add_prop(OBJ_I_VALUE, 250);
	add_prop(CONT_I_CLOSED, 0);
	add_prop(CONT_I_WEIGHT, 1300);
	add_prop(CONT_I_VOLUME, 1000);
	add_prop(CONT_I_MAX_WEIGHT, 18000);
	add_prop(CONT_I_MAX_VOLUME, 15000);
	add_prop(CONT_I_REDUCE_VOLUME, 125);

	remove_prop(CONT_I_RIGID);

	add_subloc(({"kiesze^n", "kieszeni", "kieszeni", "kiesze^n", "kieszeni^a",
"kieszeni"}));
    add_subloc_prop("kiesze^n", SUBLOC_S_OB_GDZIE, "wewn^atrz kieszeni");
    add_subloc_prop("kiesze^n", SUBLOC_I_OB_PRZYP, PL_DOP);
    add_item("kiesze^n w", "Ot, zwyk^la kiesze^n doszyta do plecaka" +
        "@@opis_sublokacji| zawieraj^aca |kiesze^n|.|.|" + PL_BIE + "@@\n",
PL_MIE);
	add_subloc_prop("kiesze^n", CONT_I_MAX_WEIGHT, 700);
	add_subloc_prop("kiesze^n", CONT_I_MAX_VOLUME, 700);
}
