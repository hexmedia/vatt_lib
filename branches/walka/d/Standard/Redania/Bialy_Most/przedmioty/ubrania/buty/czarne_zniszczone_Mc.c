/* Opis & wykonanie: Valor
   Data: 17.06.07
*/
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <object_types.h>
#include <macros.h>
#include <materialy.h>
void
create_armour()
{  
        ustaw_nazwe("buty");
        ustaw_nazwe_glowna("para");
    dodaj_przym("czarny","czarni");
    dodaj_przym("zniszczony", "zniszczeni");
    set_long("Czarne, si^egaj^ace po^lowy ^lydki buty wygl^adaj^a ju^z "+
        "na do^s^c znoszone. Pop^ekana sk^ora i wytarta podeszwa to nie "+
        "wszystkie mankamenty obuwia. Jest jeszcze urobiona klamerka, ledwo "+
        "trzymaj^acy si^e pasek od zapi^ecia oraz wystrz^epiona g^orna "+
        "kraw^ed^x buta. \n");
        set_slots(A_FEET);
    add_prop(OBJ_I_WEIGHT, 1000); 
    add_prop(OBJ_I_VOLUME, 500);
    add_prop(OBJ_I_VALUE, 30);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
    ustaw_material(MATERIALY_SK_SWINIA);
    set_type(O_UBRANIA);
}
