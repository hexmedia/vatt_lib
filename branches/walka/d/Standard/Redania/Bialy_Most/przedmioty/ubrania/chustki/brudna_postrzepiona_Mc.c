
/* Autor: Faeve
   Opis : Duana
   Data : 22.06.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("chustka");

    dodaj_przym("brudny","brudni");
    dodaj_przym("postrz^epiony","postrz^epieni");

    set_long("Obszerny materia^l, poszarpany na brzegach, nadaje si^e na "+
		"otulenie nim ramion, b^ad^x g^lowy, zw^laszcza w ch^lodniejsze dni. "+
		"Okrycie, dawniej zapewne zielonego koloru, jest teraz brudne i "+
		"poszarza^le, a na ca^lej powierzchni materia^lu widniej^a jakie^s "+
		"plamy i przyklejone paprochy, nadaj^ac chu^scie przykrego wygl^adu, "+
		"przypominaj^acego star^a szmat^e.\n");

    set_slots(A_HEAD);
    add_prop(OBJ_I_WEIGHT, 150);
    add_prop(OBJ_I_VOLUME, 100);
    add_prop(OBJ_I_VALUE, 7);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_LEN, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}