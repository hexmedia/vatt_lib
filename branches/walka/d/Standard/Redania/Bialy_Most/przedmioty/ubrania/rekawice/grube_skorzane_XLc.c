
/* 
opis popelniony przez Grypina, zakodowany przez Seda 06.04.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
   ustaw_nazwe("r^ekawice");
    dodaj_przym("gruby", "grubi");
    dodaj_przym("sk^orzany","sk^orzani");


    set_long("Ta para r^ekawic jest wystarczaj^aco gruba by ochroni^c twe r^ece "
	+"przed mrozem w zimne dni. Lecz ich g^l^own^a funkcj^a jest ochrona d^loni "
	+"w czasie pracy. Lekko znoszone ubranie posiada liczne plamy barwnika, "
	+"oraz ma^le dziurki, w zewn^etrznej warstwie materia^lu. R^ekawice te nie "
	+"nadaj^a si^e do u^zywania w czasie walki gdy^z, ich niestaranne wykonanie "
	+"powoduje i^z nosz^acy nie b^edzie mia^l pewnego chwytu na rekoje^sci broni.\n");

    set_slots(A_HANDS);
    add_prop(OBJ_I_WEIGHT, 200);
    add_prop(OBJ_I_VOLUME, 700);
    add_prop(OBJ_I_VALUE, 800);

	set_size("XL");
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_F_PRZELICZNIK, 20.0);
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    ustaw_material(MATERIALY_SK_SWINIA);
}


