
/* Autor: Avard
   Opis : Tinardan
   Data : 21.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");

    dodaj_przym("wysoki","wysocy");
    dodaj_przym("sk^orzany","sk^orzani");

    set_long("Nie lada szewc pracowa^l nad tymi butami, nadaj^ac im ich "+
        "doskona^ly kszta^lt, ich mi^ekko^s^c i elegancj^e wyko^nczenia. "+
        "Noski maj^a nieco zadarte ku g^orze wedle najnowszej mody "+
        "dworskiej, wywini^ete cholewy przyci^ete nier^owno i spi^ete "+
        "srebrn^a klamr^a w kszta^lcie szpilki. Ich kolor jest do^s^c "+
        "jasny, co^s pomi^edzy br^aem, a czerwieni^a, podkre^slony "+
        "jeszcze przez zielone rzemyki, s^lu^z^ace do obwi^azywania "+
        "cholewy i czer^n twardej podeszwy.\n");
		
	set_slots(A_FEET);
	add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 100);
	add_prop(OBJ_I_WEIGHT, 510);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
	ustaw_material(MATERIALY_SK_SWINIA, 85);
    ustaw_material(MATERIALY_SREBRO, 15);
	set_type(O_UBRANIA);
}