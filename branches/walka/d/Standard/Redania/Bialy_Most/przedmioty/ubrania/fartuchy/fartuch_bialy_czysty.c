/* Autor: Duana
  Opis : sniegulak
  Data : 20.05.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include "/sys/object_types.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
   ustaw_nazwe("fartuch");
    dodaj_przym("bia�y","biali");
    dodaj_przym("czysty", "czy�ci");
    set_long("Czysty i �wie�o wyprany fartuch zwykle u�ywany przez "+
        "karczmarzy do ochrony ubrania przed brudem, zosta� w tym wypadku "+
        "skrojony z lnu o bia�ym kolorze. Za pomoc� ciemnoniebieskich "+
        "tasiemek przyszytych do bok�w, mo�na go przewi�za� w ok� pasa.\n");
    
    set_type(O_UBRANIA);
    set_slots(A_HIPS);
    add_prop(OBJ_I_WEIGHT, 400);
    add_prop(OBJ_I_VOLUME, 400);
    add_prop(OBJ_I_VALUE, 800);

    ustaw_material(MATERIALY_TK_LEN);
	set_size("M");
}