/*
 * kupca BM
 * opis by Sniegulak
 * zepsu^la faeve
 * dn. 16.06.2007
 *
 */

inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");

    dodaj_przym("przybrudzony", "przybrudzeni");
    dodaj_przym("br^azowy", "br^azowi");

    set_long("Proste spodnie o  najzwyklejszym kroju, pokryte s^a licznymi "+
		"t^lustymi plamami. Nogawki, nieco zbyt kr^otkie - mog^a ods^lania^c "+
		"^lydki nosz^cej je osoby. Ubranie lekko pchanie rybami i jeszcze "+
		"jakim^s trudnym do zidentyfikowania zapachem.\n");
    set_ac(A_THIGHS | A_HIPS | A_STOMACH, 2, 2, 2);
    ustaw_material(MATERIALY_LEN);
    set_type(O_UBRANIA);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 509);
    add_prop(OBJ_I_VALUE, 10);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}