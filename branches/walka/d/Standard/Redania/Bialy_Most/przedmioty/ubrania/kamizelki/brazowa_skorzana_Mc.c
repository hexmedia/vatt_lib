
/* Autor: faeve
   Opis : Sniegulak
   Data : 16.06.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
    ustaw_nazwe("kamizelka");
    dodaj_przym("br^azowy","br^azowi");
    dodaj_przym("sk^orzany","sk^orzani");

    set_long("Ciemnobr^azowa niezdarnie wybarbowana sk^ora u^zyta do uszycia "+
		"tej kamizelki, pokryta jest licznymi r^o^znokolorowymi t^lustymi "+
		"plamami zajmuj^acymi prawie ca^l^a powierzchnie ubrania. Szwy "+
		"gdzieniegdzie ponaci^agane i rozdarte b^ed^a ods^lania^c cia^lo "+
		" nosz^acego. Og^olny wygl^ad nie robi zbyt dobrego wra^zenia. \n");

    set_slots(A_TORSO, A_SHOULDERS);
    add_prop(OBJ_I_VOLUME, 1550);
    add_prop(OBJ_I_WEIGHT, 400);
    add_prop(OBJ_I_VALUE, 15);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
    ustaw_material(MATERIALY_SK_SWINIA, 100);
    set_type(O_UBRANIA);

}
