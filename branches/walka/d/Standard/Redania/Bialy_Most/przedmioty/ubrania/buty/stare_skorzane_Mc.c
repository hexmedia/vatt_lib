
/* stare skorzane buty, all by Faeve - 27.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>


void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");

    dodaj_przym("stary","starzy");
    dodaj_przym("sk^orzany","sk^orzani");

    set_long("Czarne, si^egaj^ace poni^zej kostki buty. S^a ju^z do^s^c mocno "+
        "zu^zyte, sk^ora, z kt^orej zosta^ly wykonane straci^la ju^z po^lysk, "+
        "miejscami jest przetarta, a nawet lekko pop^ekana. W^lasciciel musi "+
        "je bardzo lubi^c, skoro ich jeszcze nie wyrzuci^l. Wygl^ada na to, "+
        "^ze nawet dba o nie, chc^ac, by wyglada^ly jak najlepiej - s^a "+
        "dok^ladnie wyczyszczone i wypolerowane. \n");


    set_slots(A_FEET | A_SHINS);
    add_prop(OBJ_I_WEIGHT, 300);
    add_prop(OBJ_I_VOLUME, 1200);
    add_prop(OBJ_I_VALUE, 100);
    add_prop(ARMOUR_F_PRZELICZNIK, 10.0);
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    set_size("M");
    ustaw_material(MATERIALY_SK_SWINIA, 100);  
    add_prop(ARMOUR_F_POW_DL_STOPA, 2.75);
    add_prop(ARMOUR_F_POW_SZ_STOPA, 2.75);
}