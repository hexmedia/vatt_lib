/* 
* Opis Duany, 
* wrzuci�a Faeve 
* dla npcki w BM
*/


inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{


ustaw_nazwe("suknia");

    dodaj_przym("szary","szarzy");
    dodaj_przym("d^lugi", "d^ludzy");
    set_long("Miejscami po^latana i ubrudzona, szara suknia prezentuje si^e "+
		"nad wyraz biednie i skromnie. Si^ega samych kostek, zakrywaj^ac "+
		"zupe^lnie nogi nosz^acej j^a osoby, przez co przypomina d^lug^a "+
		"szat^e, a szorstki, wyblak^ly materia^l koloru szarego sprawia, ^ze "+
		"odzienie swym wygl^adem przywodzi na my^sl lniany worek. ");

    ustaw_material(MATERIALY_LEN, 100);

    set_slots(A_TORSO, A_ARMS, A_LEGS, A_BODY);
    add_prop(OBJ_I_WEIGHT, 400);
    add_prop(OBJ_I_VOLUME, 85);
    add_prop(OBJ_I_VALUE, 20);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 5);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);

    set_size("M");
    set_type(O_UBRANIA);
}