/* Autor: Valor
   Opis:  Faeve
   Data:  12.06.07
 */ 

inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");
        
    dodaj_przym("gruby","grubi");
    dodaj_przym("lniany","lniani");
        
    set_long("Uszyte z do^s^c grubego, zafarbowanego na ^lagodny, "+
        "orzechowy br^az lnu spodnie s^a do^s^c w^askie, tak by "+
        "mo^zna by^lo je wsun^a^c w buty, a jednoczesnie by nie "+
        "kr^epowa^ly ruch^ow. Nie ma w nich nic nadzwyczajnego - ot, "+
        "proste, do^s^c wygodne, w pasie wi^azane rzemieniem. \n");
        
    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VALUE, 50);      
    ustaw_material(MATERIALY_LEN, 100);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    set_size("M");
}