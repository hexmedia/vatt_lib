
/* Autor - Gjoef */

#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

inherit "/std/armour.c";

void
create_armour()
{
    ustaw_nazwe("koszula");
    
    dodaj_przym("bia^ly", "biali");
    dodaj_przym("jedwabny", "jedwabni");
    
    set_long("T^e koszul^e odr^o^znia od innych przede wszystkim materia^l - "+
             "jest ona bowiem wykonana z delikatnego, cienkiego jedwabiu. Na "+
             "mankietach i pod ko^lnierzem dostrzec mo^zna s^labo widoczne "+
             "kwietne motywy, misternie wyszyte srebrn^a nici^a. Ca^lo^s^c "+
             "prezentuje si^e naprawd^e okazale i nie pozostawia "+
             "w^atpliwo^sci, i^z jest to typowo elfia cz^e^s^c garderoby.\n");

    ustaw_material(MATERIALY_JEDWAB);
        
    add_prop(OBJ_I_VOLUME, 500);
    add_prop(OBJ_I_WEIGHT, 150);
    add_prop(OBJ_I_VALUE, 100);
    
    set_slots(A_TORSO, A_FOREARMS, A_ARMS); 
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
}