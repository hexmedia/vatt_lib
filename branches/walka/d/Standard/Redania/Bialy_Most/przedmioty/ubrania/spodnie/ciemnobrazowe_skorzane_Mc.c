/* Opis & wykonanie: Valor
   Data: 17.06.07
*/

inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <object_types.h>
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
        ustaw_nazwe("spodnie");
        ustaw_nazwe_glowna("para");
    dodaj_przym("ciemnobr^azowy","ciemnobr^azowi");
    dodaj_przym("sk^orzany", "sk^orzani");
    set_long("Ciemnobr^azowe spodnie, wykonane najprawdopodobniej z "+
        "^swi^nskiej sk^ory. Posiadaj^a dwie kieszenie z przodu, "+
        "s^lu^z^ace g^l^ownie jako ozdoba z powodu swoich "+
        "ma^lych rozmiar^ow. Spodnie zd^a^zy^ly si^e ju^z pomarszczy^c "+
        "i poprzeciera^c lecz dzia^la to na ich korzy^s^c, dzieki temu "+
        "wygladaj^a do^s^c oryginalnie i nietuzinkowo. \n");

        set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_WEIGHT, 500); 
    add_prop(OBJ_I_VOLUME, 500);
    add_prop(OBJ_I_VALUE, 10);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
    ustaw_material(MATERIALY_SK_SWINIA);
    set_type(O_UBRANIA);
}