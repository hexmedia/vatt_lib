/* Autor: Duana
  Opis : sniegulak
  Data : 20.05.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include "/sys/object_types.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
   ustaw_nazwe("^zupan");
    dodaj_przym("ciemnobr^azowy","ciemnobr^azowi");
    dodaj_przym("d^lugi", "d^ludzy");
    set_long("Uszyty z grubego ciemnobr^azowego materia^lu ^zupan, jest "+
        "jednocz^e^sciowym ubraniem zak^ladanym przez m^e^zczyzn. D^lugie r^ekawy "+
        "zako^nczone zapinanymi na guziki mankietami, s^a do^s^c lu^xne i nie "+
        "kr^epuj^a ruch^ow, po^l^aczone zosta^ly z sukni^a o tym samym kolorze. "+
        "Szata zapinana jest z przodu za pomoc^a drobnych guzik^ow i wygl^ada "+
        "na ca^lkiem czysta i zadban^a.\n");

    set_type(O_UBRANIA);
    //FIXME slot do poprawki
    set_slots(A_ROBE);
    add_prop(OBJ_I_WEIGHT, 800);
    add_prop(OBJ_I_VOLUME, 800);
    add_prop(OBJ_I_VALUE, 2500);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 10);
    add_prop(ARMOUR_I_DLA_PLCI, 0);

    ustaw_material(MATERIALY_JEDWAB);
	set_size("M");
}