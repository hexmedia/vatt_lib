/* Autor: Duana
  Opis : Barteusz
  Data : 03.06.2007
*/

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

create_object()
{
    ustaw_nazwe("miednica");
                 
    dodaj_przym("jajowaty","jajowaci");
    dodaj_przym("sosnowy","sosnowi");
   
    set_long("Jajowatego kszta^ltu miednic^e zbito z sosnowych klepech, kt^ore "+
        "po^l^aczono paroma stalowymi obr^eczami. Od drewna dawno ju^z odlaz^l "+
        "lakier, je^sli wog^ole by^lo nim kiedykowiek pokryte, obrecze mocno "+
        "prze^zera ju^z rdza. W dnie wida^c kilka za^latanych dziur. Miednic^e "+
        "do po^lowy wype^lnia woda.\n");
    add_prop(OBJ_I_WEIGHT, 1000);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 100);
    ustaw_material(MATERIALY_DR_SOSNA, 90);
    ustaw_material(MATERIALY_RZ_STAL, 10);

    set_type(O_NARZEDZIA);
    set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));
}
