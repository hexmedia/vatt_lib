/* Autor: Valor (poprawki: Sed)
   Opis : �niegulak
   Data : 10.04.07 */
inherit "/std/pochwa";
#include "/sys/formulas.h"
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <macros.h>
#include <cmdparse.h>
#include <pl.h>
#define POCHWA_PD_PLECY         1
#define POCHWA_PD_UDA           2
#define POCHWA_PD_GOLENIE       4
#define POCHWA_PD_PAS           8
#define POCHWA_NA_PLECACH       "_pochwa_na_plecach"
#define POCHWA_PRZY_UDZIE_P     "_pochwa_przy_udzie_p"
#define POCHWA_PRZY_UDZIE_L     "_pochwa_przy_udzie_l"
#define POCHWA_PRZY_GOLENI_P    "_pochwa_przy_goleni_p"
#define POCHWA_PRZY_GOLENI_L    "_pochwa_przy_goleni_l"
#define POCHWA_W_PASIE_P        "_pochwa_w_pasie_p"
#define POCHWA_W_PASIE_L        "_pochwa_w_pasie_l"
#define POCHWA_SUBLOC           "_pochwa_subloc"
void
create_pochwa()
{
    ustaw_nazwe("pochwa");
    dodaj_przym("ma�y","mali");
    dodaj_przym("miedziany","miedziani");
    set_long("Ogl^adasz ma^l^a miedzian^a pochw^e w kt^orej mo^zesz "+
        "schowa^c sztylet. Jest to odlew wykonany z miedzi z delikatnymi "+
        "guzami na powierzchni. Brak finezyjnych ornament^ow zast^apiony "+
        "zosta^l solidn^a robot^a, dzieki kt^orej pochwa bedzie mog^la "+
        "s^lu^zy^c przez lata. Otw^or, w kt^ory wsuwasz bro^n zosta^l "+
        "wy^lo^zony miekkim, czarnym materia^lem pozwalaj^ac "+
        "w^la^scicielowi pochwy na ciche i ^latwe wysuwanie broni. "+
        "Sk^orzane paski umo^zliwiaj^a przypi^ecie jej do pasa lub nogi. \n");
    set_przypinane_do(POCHWA_PD_UDA | POCHWA_PD_GOLENIE | POCHWA_PD_PAS );
    add_subloc_prop(0, SUBLOC_I_DLA_O, O_BRON_SZTYLETY);
    set_type(O_POCHWY);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 500);
    add_prop(CONT_I_REDUCE_VOLUME, 125);
    add_prop(CONT_I_MAX_WEIGHT, 5000);
    add_prop(CONT_I_MAX_VOLUME, 5000);
    add_prop(CONT_I_MAX_RZECZY, 1);

	ustaw_material(MATERIALY_RZ_MIEDZ, 80);
	ustaw_material(MATERIALY_SK_SWINIA, 15);
ustaw_material(MATERIALY_TK_BAWELNA, 5);

add_prop(OBJ_I_VALUE, 351);
}

