/*by ohm*/

inherit "/std/object";
#include <stdproperties.h>
#include <object_types.h>
void
create_object()
{
    ustaw_nazwe("kulka");
    dodaj_przym("ma^ly", "mali");
    dodaj_przym("chlebowy", "chlebowi");
	dodaj_nazwy("przyn^eta");
     set_long("Malutka kulka zrobiona z namoczonego w jakim^s soku chleba.\n");

    add_prop(OBJ_I_VALUE, 1);
    add_prop(OBJ_I_WEIGHT, 1);
    add_prop(OBJ_I_VOLUME,1);	
    set_type(O_WEDKARSKIE);

}

/* Zwraca rodzaj przynety. Mozliwe wartosci to:
 * 'chleb', 'robak', 'rybka'
 */
string
query_przyneta_typ()
{
    return "chleb";
}
