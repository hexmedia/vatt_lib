inherit "/std/object";

#include <macros.h>
#include <math.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <object_types.h>
#define ROBAK "/std/ryby/robak.c"

string *locs = ({ });
int ile = 0;

int wykop(string str);
int pomoc(string str);


void
create_object()
{
	ustaw_nazwe("�opatka");

	dodaj_przym("ma�y", "mali");
	dodaj_przym("lekki", "lekcy");

	set_long("�opatka, mimo i� niewielka, wygl�da na ca�kiem solidnie"
			+" wykonan�. Metalowa, spiczasta ko�c�wka nasadzona na"
			+" jasny, drewniany trzonek z pewno�ci� jest w stanie"
			+" przekopa� wiele ziemi. Co prawda nie nadaje si� ona"
			+" do wi�kszych wykop�w, jednak doskonale spe�nia swoj�"
			+" rol�, na przyklad podczas drobnych prac ogrodniczych.\n");

	add_prop(OBJ_I_WEIGHT, 340);
	add_prop(OBJ_I_VOLUME, 200);
	add_prop(OBJ_I_VALUE, 50);
	set_type(O_WEDKARSKIE);

	setuid();
    seteuid(getuid());

	set_alarm(240.0, 0.0, "clean_locs");
}

init()
{
	::init();
	add_action(&wykop(), "wykop");
	add_action(&pomoc(), "?", 2);
}

void clean_locs()
{
	locs = ({ });
	set_alarm(240.0, 0.0, "clean_locs");
}

void wykopane()
{
	object player = ENV(TO);
	if(ile)
	{
		for(int i = 0; i < ile; i++)
			clone_object(ROBAK)->move(player);

		player->catch_msg("Uda�o ci si� wykopa� kilka d�d�ownic.\n");

		if(sizeof(locs) > 7)
			locs = ({ });

		locs += ({ file_name(ENV(player)) });
		ile = 0;
	}
	else
		player->catch_msg("Przekopa�"+player->koncowka("e�", "a�")+" ca��"
				+" okolic�, jednak nie znalaz�"+player->koncowka("e�", "a�")
				+" �adnej d�d�ownicy.\n");
		
}

int wykop(string str)
{
	notify_fail("Co takiego chcesz wykopa�?\n");

	if(!str)
		return 0;

	if(!(str ~= "robaki"))
		return 0;

	if(member_array(ENV(TP)->query_prop(ROOM_I_TYPE),
	  ({ROOM_TRACT, ROOM_FOREST, ROOM_FIELD})) == -1)
	{
		notify_fail("Tutaj z pewno�ci� nie znajdziesz �adnych robak�w.\n");
		return 0;
	}

	float 	prob;

	if(member_array(file_name(ENV(TP)), locs) == -1)
	{
		prob = LINEAR_FUNC(0.0, 0.6, 100.0, 0.95, itof(TP->query_skill(SS_FISHING)));
		prob += itof(random(ftoi(15.0*prob)))/100.0;
	}
	else
		prob = 0.0;

	find_player("root")->catch_msg("prob="+ftoa(prob)+"\n");

	if(!(PROB_RANDOM( ([1:prob,0:(1.0-prob)]) )))
		ile = 0;
	else
		ile = 2+random(4);

	TP->catch_msg("Przykl�kasz i zaczynasz przekopywa� ziemi� "+short(PL_NAR)
			+" w poszukiwaniu d�d�ownic.\n");
	saybb(QCIMIE(TP, PL_MIA)+" przykl�ka i zaczyna przekopywa� ziemi� "
			+short(PL_NAR)+".\n");

	object paraliz = clone_object("/std/paralyze");
    paraliz->set_standard_paralyze("przkopywa� ziemi�");

    paraliz->set_stop_verb("przesta�");
    paraliz->set_stop_message("Przestajesz przekopywa� ziemi�.\n");

	paraliz->set_finish_fun("wykopane");
	paraliz->set_finish_object(TO);

    paraliz->set_remove_time(13+random(9));
    paraliz->set_fail_message("Przekopujesz teraz ziemi�, je�li chcesz zrobi�"
			+" co� innego - przesta�.\n");
	paraliz->move(this_player());

	return 1;
}

public int
pomoc(string str)
{                                                                              
    object pohf;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, pohf))
        return 0;

    if (pohf != this_object())
        return 0;

	write("�opatka z pewno�ci� nadaje si� do wykopywania robak�w.\n");
        return 1;
}

