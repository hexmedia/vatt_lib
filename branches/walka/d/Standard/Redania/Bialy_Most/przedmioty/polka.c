/* Autor: Duana
  Opis : sniegulak
  Data : 23.05.2007
*/

inherit "/std/container";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include <mudtime.h>
#include "dir.h"

void
create_container()
{
    ustaw_nazwe("p^o^lka");
    dodaj_przym("solidny", "solidni");
    dodaj_przym("drewniany", "drewniani");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);

    set_long("Solidna p^o^lka wykonana ze zbitych gwo^zdziami sosnowych "+
        "desek. Podzielono j^a na kilka przegr^odek, dla lepszego "+
        "roz^lo^zenia na niej przedmiot^ow. "+
        "@@opis_sublokacji|Dostrzegasz na niej |na|.\n|\n|" + PL_BIE+ "@@");
         
    add_prop(CONT_I_WEIGHT, 30000);
    add_prop(CONT_I_VOLUME, 30000);
    add_prop(OBJ_I_VALUE, 456);
    
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 100000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 150000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);

    add_object(BIALY_MOST_PRZEDMIOTY + "pojemnik_stalowy.c", 1, 0, "na");  
    add_object(BIALY_MOST_PRZEDMIOTY + "pojemnik_szczelny.c", 1, 0, "na");  
    add_object(BIALY_MOST_PRZEDMIOTY + "worek_lniany_gruby.c", 1, 0, "na");  

    set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));

}