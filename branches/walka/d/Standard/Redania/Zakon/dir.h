#include "../dir.h"


#define ZAKON_STD           (ZAKON + "std/zakon")
#define ZAKON_STD_D         (ZAKON + "std/zakon_dziedziniec")
#define ZAKON_STD_NPC       (ZAKON + "std/npc")
#define ZAKON_NPC           (ZAKON + "npc/")
#define ZAKON_PRZEDMIOTY    (ZAKON + "przedmioty/")
#define ZAKON_UBRANIA       (ZAKON_PRZEDMIOTY + "ubrania/")
#define ZAKON_BRONIE        (ZAKON_PRZEDMIOTY + "bronie/")
#define ZAKON_ZBROJE        (ZAKON_PRZEDMIOTY + "zbroje/")

#define GUILD_NAME "Zakon Karmazynowych Ostrzy"

#define KARMAZYNOWY_MIECZ   (ZAKON_BRONIE  + "miecze/karmazynowy_dlugi")
#define KAPLAN_ZAKONU       (ZAKON_NPC     + "eriel")
#define KAPLICA             (ZAKON_LOKACJE + "kaplica") 
#define SILNIK_GILDII       (ZAKON         + "std/silnik_gildii")

#define OBJ_PRZYJMOWANIA    (ZAKON + "std/przyjmowanie") 