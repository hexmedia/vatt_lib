/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/container";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_container()
{
    ustaw_nazwe("stolik");
    dodaj_przym("ma^ly", "mali");
    dodaj_przym("drewniany", "drewniani");

    set_long("Ma^ly drewniany stolik wykonany przez jakiego^s domoros^lego "+
        "cie^sle sk^lada si^e z czterech n^og, wykonanych z ^sredniej "+
        "grubo^sci konar^ow i drewnianego lekko poplamionego blatu "+
        "zrobionego z drewnianych oheblowanych desek. Plamy po ^swiecach i "+
        "czerwonym winie mog^a ^swiadczy^c o cz^estym u^zytkowaniu "+
        "sto^lu.\n");
    add_prop(OBJ_I_WEIGHT, 1000);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 400);
    set_type(O_MEBLE);
    add_subloc("na", 0, "ze");
    add_subloc_prop("na", SUBLOC_I_MOZNA_POLOZ, 1);

    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}
