/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_object()
{
    ustaw_nazwe("^lawa");
    dodaj_przym("wytarty", "wytarci");
    dodaj_przym("solidny", "solidni");

    set_long("Solidny mebel z d�bowego drewna poznaczony jest ^sladami "+
        "wieloletniego u^zytkowania, lecz nadal jest mocny i stabilny.\n");
    add_prop(OBJ_I_WEIGHT, 5000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 50);
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_DR_DAB);
    make_me_sitable("na", "na ^lawie", "z ^lawy", 10);
    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}
