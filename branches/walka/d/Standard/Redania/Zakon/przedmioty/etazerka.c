/* Autor: Faeve
   Opis : Sniegulak
   Data : 25.04.07 */
inherit "/std/receptacle";
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
void
create_receptacle()
{
    ustaw_nazwe("etazerka");
    dodaj_przym("ma^ly", "mali");
    dodaj_przym("wiklinowy", "wiklinowi");
    ustaw_material(MATERIALY_WIKLINA);
	ustaw_material(MATERIALY_DR_LIPA);
    set_type(O_MEBLE);
    set_long("Ma^la eta^zerka stoj^aca w rogu pokoju, jest wykona z wikliny "+
		"przybitej gwo^xd^xmi o du^zych ^lebkach do drewnianego szkieletu. "+
		"Pojemna i lekka, s^lu^zy do przechowywania ma^lych, delikatnych "+
		"przedmiot^ow. Po uchyleniu drzwiczek mo^zna sprawdzi^c co si^e w "+
		"niej znajduje. \n");
         
    add_prop(OBJ_I_VALUE, 1300);
    add_prop(CONT_I_RIGID, 1);
    add_prop(CONT_I_MAX_VOLUME, 8000);
    add_prop(CONT_I_MAX_WEIGHT, 15000);
    add_prop(CONT_I_VOLUME, 1800);
    add_prop(CONT_I_WEIGHT, 7000);
    add_prop(CONT_I_NO_OPEN_DESC, 1);
}