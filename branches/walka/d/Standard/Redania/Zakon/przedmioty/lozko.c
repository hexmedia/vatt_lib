/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_object()
{
    ustaw_nazwe("^l^o^zko");
    dodaj_przym("pi^etrowy", "pi^etrowi");
    dodaj_przym("mocny", "mocni");

    set_long("Zbudowane jest z dw^och mocnych sosnowych ram z czterema "+
        "nogami, stoj^acych jedna na drugiej. Materace po^lo^zone na "+
        "le^zysku robione s^a z materia^lowego pokrowca wype^lnionego "+
        "we^lnianymi ga^lganami. Prostota wykonania i komfort, kt^ory "+
        "zapewnia to ^lo^ze jest wystarczaj^ace dla os^ob kt^ore smaczny "+
        "i wygodny sen stawiaj^a na r^owni z wygl^adem przedmiotu.\n");
    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 600);
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_DR_SOSNA, 80);
    ustaw_material(MATERIALY_WELNA, 20);
    make_me_sitable("na", "na ^l^o^zku", "z ^l^o^zka", 4);
    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}
