/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/container";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_container()
{
    ustaw_nazwe("stojak");
    dodaj_przym("drewniany", "drewniani");
    dodaj_przym("prosty", "pro^sci");

    set_long("Jest to drewniany prosty stojak, zbudowany z struganego "+
        "drewna, kt^ore w ko^ncowej fazie obr^obki zosta^lo pomalowane "+
        "na czarny kolor. S^lu^zy do wieszania na nim broni.\n");
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 500);
    add_prop(OBJ_I_VALUE, 10);
    set_type(O_MEBLE);
    add_subloc("na", 0, "ze");
    add_subloc_prop("na", CONT_I_CANT_ODLOZ, 1);
    add_subloc_prop("na", CONT_I_CANT_POLOZ, 1);
    add_subloc_prop("na", SUBLOC_I_MOZNA_POWIES, 1);
    add_subloc_prop("na", SUBLOC_I_DLA_O, O_BRON_MIECZE | O_BRON_MIECZE_2H | 
        O_BRON_SZABLE | O_BRON_SZTYLETY | O_BRON_TOPORY | O_BRON_TOPORY_2H | 
        O_BRON_DRZEWCOWE_D | O_BRON_DRZEWCOWE_K | O_BRON_MACZUGI | 
        O_BRON_MLOTY | O_BRON_MIOTANE | O_BRON_STRZELECKIE);
    add_subloc_prop("na", CONT_I_MAX_RZECZY, 15);

    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}
public int
jestem_stojakiem_zakonu()
{
        return 1;
}
