/* zrobione: Valor
   data: 5 maj 07
   opis: tabakista
*/
inherit "/std/weapon";
#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <object_types.h>
#include <materialy.h>
void
create_weapon()
{
   ustaw_nazwe("kij");
   dodaj_przym("d^lugi","d^ludzy");
   dodaj_przym("prosty","pro^sci");
   set_long("Lekka treningowa bro^n wykonana zosta^la z "+
       "d^ebowego drewna. Oba ko^nce ukryte s^a pod grub^a "+
       "warstw^a ciasno zwini^etych szmat. W po^lowie d^lugo^sci "+
       "okr^econo go kawa^lkiem sk^ory zapezpieczonym przed "+
       "zsuni^eciem miedzianym drutem.\n");
   set_hit(28);
   set_pen(1);
   set_wt(W_CLUB);
   set_dt(W_BLUDGEON);
   set_hands(W_BOTH);
   add_prop(OBJ_I_WEIGHT, 100);
   add_prop(OBJ_I_VALUE, 50);
   add_prop(OBJ_I_VOLUME, 5);
   ustaw_material(MATERIALY_DR_DAB, 100);
   set_type(O_BRON_MACZUGI);
}
