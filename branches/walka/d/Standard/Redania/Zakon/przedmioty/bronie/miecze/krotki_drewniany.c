/* wykonanie: Valor
   opis: Tabakista
   data: 05.05.07
*/
inherit "/std/weapon";
#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <object_types.h>
#include <materialy.h>
void
create_weapon()
{
    ustaw_nazwe("miecz");
   dodaj_przym("kr�tki","kr�tcy");
   dodaj_przym("drewniany","drewniani");
   set_long("Lekka treningowa bro^n wykonana zosta^la z d^ebowego drewna. "+
       "Prowizoryczny jelec zamocowano za pomoc^a rzemienia, za^s "+
       "r^ekoje^s^c okr^econo sk^or^a zapezpieczon^a przed "+
       "zsuni^eciem miedzianym drutem. \n");
   set_hit(22);
   set_pen(1);
   set_wt(W_SWORD);
   set_dt(W_SLASH);
   set_hands(A_ANY_HAND);
   add_prop(OBJ_I_WEIGHT, 100);
   add_prop(OBJ_I_VALUE, 5);
   add_prop(OBJ_I_VOLUME, 100);
   ustaw_material(MATERIALY_DR_DAB, 100);
   set_type(O_BRON_MIECZE);
}
