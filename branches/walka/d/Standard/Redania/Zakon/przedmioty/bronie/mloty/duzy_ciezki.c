
/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */
inherit "/std/weapon";
inherit "/lib/tool";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>
#include <tools.h>

void
create_weapon()
{
    ustaw_nazwe("m^lot");
                    
    dodaj_przym("du^zy","duzi");
    dodaj_przym("ci^e^zki","ci^e^zcy");

    set_long("Prosty m^lot o bardzo du^zym lekko pokrytym czerni^a obuchu "+
        "wymaga na pewno du^zej si^ly by nim w^lada^c. Prawdopodobnie "+
        "nale^za^l do jakiego^s  kowala, swiadczy^c o tym mo^ze znak cechu "+
        "wybity na obuchu. Jego ci^e^zar powoduje, ^ze pos^lugiwanie si^e "+
        "nim jest mo^zliwe jedynie przy chwyceniu go dwoma rekami.\n");

    add_prop(OBJ_I_WEIGHT, 6000);
    add_prop(OBJ_I_VOLUME, 6000);
    add_prop(OBJ_I_VALUE, 1900);
   
    set_hit(17);
    set_pen(30);

    set_wt(W_WARHAMMER);
    set_dt(W_BLUDGEON);
    ustaw_material(MATERIALY_STAL, 80);
    ustaw_material(MATERIALY_SK_SWINIA, 20);
    set_type(O_BRON_TOPORY_2H);

    set_hands(A_HANDS);
    add_tool_domain(TOOL_DOMAIN_BLACKSMITH);
    set_tool_diffculty(60);
}
