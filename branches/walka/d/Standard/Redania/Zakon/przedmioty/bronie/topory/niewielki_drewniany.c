/* zrobione: Valor
   data: 5 maj 07
   opis: tabakista
*/
inherit "/std/weapon";
#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <object_types.h>
#include <materialy.h>
void
create_weapon()
{
    ustaw_nazwe("top^or");
   dodaj_przym("niewielki","niewielcy");
   dodaj_przym("drewniany","drewniani");
   set_long("Lekka treningowa bro^n wykonana zosta^la z d^ebowego drewna. "+
       "Ostrze nie zosta^lo nawet pozornie naostrzone. R^ekoje^s^c "+
       "okr^econo sk^or^a zapezpieczon^a przed zsuni^eciem "+
       "miedzianym drutem.\n");
   set_hit(23);
   set_pen(1);
   set_wt(W_AXE);
   set_dt(W_SLASH);
   set_hands(A_ANY_HAND);
   add_prop(OBJ_I_WEIGHT, 100);
   add_prop(OBJ_I_VALUE, 50);
   add_prop(OBJ_I_VOLUME, 5);
   ustaw_material(MATERIALY_DR_DAB, 100);
   set_type(O_BRON_TOPORY);
}
