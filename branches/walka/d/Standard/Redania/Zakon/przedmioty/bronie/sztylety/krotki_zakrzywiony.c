/* autor: Valor
   opis: Sniegulak
   wykonane: 06.05.07
*/
inherit "/std/weapon";
#include <macros.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>
    
void create_weapon() 
{
        ustaw_nazwe("sztylet");
        dodaj_przym("kr^otki", "kr^otcy");
        dodaj_przym("zakrzywiony", "zakrzywieni");
                    
        set_long("Bro^n, kt^orej si^e przygl^adasz posiada ostrze wykute "+
            "ze stali, zmatowione tak by nie odbija^lo ^swiat^la. "+
            "D^lugo^s^c ca^lego sztyletu nieznacznie przekracza p^o^l "+
            "^lokcia. R^ekoje^s^c obita sk^ora poprawia chwyt i pomaga "+
            "w lepszym w^ladaniu broni^a. Ma^le wg^l^ebienie na ostrzu, ma "+
            "za zadanie zmniejszy^c wag^e i wywa^zy^c bron, tak by mog^la "+
            "by^c u^zywana do celnego rzucania. \n");
        
        set_hit(28);
        set_pen(15);
        set_wt(W_KNIFE);
        set_dt(W_IMPALE | W_SLASH);
        set_hands(A_ANY_HAND);
     
        add_prop(OBJ_I_VALUE, 1400);
        add_prop(OBJ_I_WEIGHT, 400);
        add_prop(OBJ_I_VOLUME, 400);   
        set_type(O_BRON_SZTYLETY);
        ustaw_material(MATERIALY_STAL, 90);
        ustaw_material(MATERIALY_SK_BYDLE, 10);
}