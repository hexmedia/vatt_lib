/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */
inherit "/std/receptacle";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_receptacle()
{
    ustaw_nazwe("szafka");
    dodaj_przym("du^zy", "duzi");
    dodaj_przym("przeszklony", "przeszkleni");
    ustaw_material(MATERIALY_DR_MAHON);
    set_type(O_MEBLE);
    set_long("Du^zy przeszklony mebel posiadaj^acy szklane zamykane na "+
        "zamek drzwiczki wype^lniony jest r^o^znorakimi zwojami i "+
        "ksi^egami. Ciemne drewno u^zyte do wykonania tej szafki jest "+
        "precyzyjnie oheblowane i pokryte przezroczystym lakierem. Na "+
        "bokach szafki mo^zna zauwa^zy^c wzory w kszta^lcie winogron.\n");
	     
    add_prop(OBJ_I_VALUE, 1300);
    add_prop(CONT_I_RIGID, 1);
    add_prop(CONT_I_MAX_VOLUME, 15000);
    add_prop(CONT_I_MAX_WEIGHT, 30000);
    add_prop(CONT_I_VOLUME, 1500);
    add_prop(CONT_I_WEIGHT, 15000);
    add_prop(CONT_I_NO_OPEN_DESC, 1);
}

