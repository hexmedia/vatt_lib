/* Autor: Avard
   Opis : Sniegulak
   Data : 06.05.07 */

inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");
    dodaj_przym("mocny","mocni");
    dodaj_przym("wysoki","wysocy");
    set_long("Te na poz^or przeci^etne buty skrywaj^a sekret. Maja w sobie "+
        "przyczepion^a plecionk^e kolcz^a chroni^ac^a golenie i ty^l "+
        "^lydek. Sam but si^ega prawie do kolana, a cholewka wykonana jest "+
        "z czarnej sk^ory. But wi^azany jest z boku czarnymi rzemykami i w "+
        "cieple dni mo^ze zosta^c wywini^ety w dol do polowy nogi. Podeszwa "+
        "z lekkim obcasem o masywnym nosku chroni dodatkowo stop^e przed "+
        "wypadkami.\n");
    set_slots(A_FEET | A_SHINS);
    add_prop(OBJ_I_WEIGHT, 700);
    add_prop(OBJ_I_VOLUME, 700);
    add_prop(OBJ_I_VALUE, 200);
    
    add_prop(ARMOUR_S_DLA_RASY, "krasnolud");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
    ustaw_material(MATERIALY_SK_SWINIA, 70);
    ustaw_material(MATERIALY_STAL, 30);
   
    set_type(O_ZBROJE);
    set_ac(A_FEET,6,7,2, A_SHINS,6,7,2);
}