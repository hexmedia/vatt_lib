
/* Autor: Avard
   Opis : Sniegulak
   Data : 06.05.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <wa_types.h>
#include <materialy.h>
#include <pl.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("naramienniki");
    dodaj_przym("czerniony","czernieni");
    dodaj_przym("lekki","lekcy");
    set_long("Para naramiennik^ow wykuta ze dobrej jako^sciowo stali jest "+
        "lekka a zarazem zapewnia dobra ochron^e. Precyzja obr^obki "+
        "wskazuje na kowala dobrze znaj^acego si^e na swoim fachu. "+
        "Sk^orzane paski zafarbowane na czarny kolor idealnie zgrywaj^a "+
        "si^e z poczernian^a stal^a i pozwalaj^a na dobre przywi^azanie "+
        "zbroi.\n"); 

    ustaw_material(MATERIALY_STAL, 90);
    ustaw_material(MATERIALY_SK_CIELE, 10);
    set_type(O_ZBROJE);
    set_slots(A_SHOULDERS);
    add_prop(OBJ_I_VOLUME, 800); 
    add_prop(OBJ_I_VALUE, 1600);   
    add_prop(OBJ_I_WEIGHT, 800);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
    set_ac(A_SHOULDERS,15,15,13);
}
