/* autor: Valor
   opis: ^Sniegulak
   dnia: 9 maj 07
*/
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
    ustaw_nazwe("brygantyna");
    dodaj_przym("b^lyszcz^acy","b^lyszcz^acy");
    dodaj_przym("ci^e^zki","ci^e^zcy");
    set_long("Zbroja ta ju^z na pierwszy rzut oka wydaje si^e by^c "+
        "^swietn^a ochron^a cia^la. Kaftan z mocnej, grubej i garbowanej "+
        "sk^ory dodatkowo wzmocniony na ramionach i brzuchu kolejnymi "+
        "warstwami zosta^l zeszyty grubym rzemieniem. Nast^epnie "+
        "rzemie^slnik wykonuj^acy t^a zbroj^e, przynitowa^l do niej "+
        "grube, do^s^c szerokie p^lytki wielko^sci dw^och palc^ow. "+
        "Zachodz^a one lekko na siebie i przypominaj^a sk^or^e w^eza. "+
        "Z przodu na wysoko^sci klatki piersiowej umieszczono wi^eksz^a "+
        "wyprofilowan^a p^lyt^e, kt^ora ma chroni^c g^orna cz^e^s^c "+
        "tu^lowia przed mocniejszymi ciosami. \n");
    set_slots(A_TORSO, A_ARMS);
    
    add_prop(OBJ_I_WEIGHT, 12000); 
    add_prop(OBJ_I_VOLUME, 12000);
    add_prop(OBJ_I_VALUE, 1680);
    
    set_type(O_ZBROJE);
    
    add_prop(ARMOUR_S_DLA_RASY, "krasnolud");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    ustaw_material(MATERIALY_SK_BYDLE, 50);
    ustaw_material(MATERIALY_STAL, 50);
    set_size("L");

    set_ac(A_TORSO,6,6,6, A_ARMS,6,6,6);
}
