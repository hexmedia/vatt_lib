/* Acheron */
inherit "/std/armour";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>
#include </sys/materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("przeszywanica");
    set_type(O_ZBROJE);

    dodaj_przym("niebieski","niebiescy");
    dodaj_przym("gruby","grubi");

    ustaw_material(MATERIALY_BAWELNA, 70);
    ustaw_material(MATERIALY_SK_SWINIA, 30);

    set_long("Niebieska, gruba przeszywanica zosta^la wzmocniona poprzez " +
             "przyszyte dodatkowe warstwy runa owczego, za� od spodu " +
             "zosta^la ona obszyta kawa^lkami grubej sk^ory, co czyni z niej"+
             " zbroje absorbuj�c� g^lownie ataki broni� obuchow�, jednak " +
             "jest praktycznie bezu^zyteczna gdy jej bawe^lniane w^l^okna " +
             "spotkaj� si^e z dobrze naostrzon� kling� miecza.\n");
             
    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    set_ac(A_TORSO | A_FOREARMS | A_ARMS, 3,3,9);

    add_prop(ARMOUR_F_POW_PIERSI, 2);
    add_prop(ARMOUR_F_POW_PAS, 2);
    add_prop(OBJ_I_VOLUME,4000);
    add_prop(OBJ_I_VALUE,1400);
    add_prop(OBJ_I_WEIGHT, 4000);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}

