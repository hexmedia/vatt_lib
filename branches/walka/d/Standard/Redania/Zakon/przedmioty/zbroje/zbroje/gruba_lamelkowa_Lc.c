/* autor: Valor
   opis: Žniegulak
   dnia: 9 maj 07
*/
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
    ustaw_nazwe("zbroja");
    dodaj_przym("gruby","grubi");
    dodaj_przym("lamelkowy","lamelkowi");
    set_long("Gruba lamelkowa zbroja nale^zy do lekkich, chroni^acych "+
        "tu^l^ow, plecy i ramiona, cz^esto u^zywanych przez my^sliwych "+
        "i wojownik^ow pancerzy. Sk^lada si^e z grubego sk^orzanego kaftana "+
        "z na^lo^zonymi na na^n cienkimi a zarazem lekkimi stalowymi "+
        "p^lytkami po^l^aczonymi ze sob^a rzemieniem. ^Luski nachodz^a na "+
        "siebie lecz s^a lekkie co powoduje, ^ze w og^ole nie kr^epuj^a "+
        "ruch^ow. \n");
    set_slots(A_TORSO);
    
    add_prop(OBJ_I_WEIGHT, 9500); 
    add_prop(OBJ_I_VOLUME, 9500);
    add_prop(OBJ_I_VALUE, 5000);
    
    set_type(O_UBRANIA);
    
    ustaw_material(MATERIALY_SK_BYDLE, 70);
    ustaw_material(MATERIALY_STAL, 30);
    set_size("L");
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_ac(A_TORSO, 11,11,8);
}
