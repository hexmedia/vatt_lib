/* autor: Valor
   opis: ^Sniegulak
   dnia: 9 maj 07
*/
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
    ustaw_nazwe("helm");
    dodaj_przym("ci^e^zki","ci^e^zcy");
    dodaj_przym("garnczkowy","garnczkowi");
    set_long("Przygl^adasz si^e uwa^znie ci^e^zkiemu garnczkowemu he^lmowi, "+
        "kt^ory jest okazem dobrze chroni^acej zbroi. Zrobiony zosta^l z "+
        "nitowanych stalowych p^lyt, wzmocnionych obr^eczami. Nie posiada "+
        "on ruchomej zas^lony tylko w^askie szpary i ma^le otwory "+
        "u^latwiaj^ace oddychanie. Ogranicza to widoczno^s^c ale sprawia, "+
        "^ze ca^la twarz jest dobrze chroniona przed uderzeniem. "+
        "^Srodek zosta^l wy^scielony mi^ekkim materia^lem, dzi^eki "+
        "kt^oremu noszenie he^lmu nie powoduje otar^c, a otrzymanie ciosu "+
        "nie jest a^z tak bolesne. \n");
    set_slots(A_HEAD);
    
    add_prop(OBJ_I_WEIGHT, 3000); 
    add_prop(OBJ_I_VOLUME, 3000);
    add_prop(OBJ_I_VALUE, 2400);
    
    set_type(O_ZBROJE);
    
    ustaw_material(MATERIALY_STAL, 90);
    ustaw_material(MATERIALY_TK_TK_PIKOWA, 10);
    set_size("L");
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_ac(A_HEAD,16,14,14);
}
