
/* Autor: Avard
   Opis : Sniegulak
   Data : 06.05.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <wa_types.h>
#include <materialy.h>
#include <pl.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("kolczuga");
    dodaj_przym("^luskowy","^luskowi");
    dodaj_przym("rycerski","rycerscy");
    set_long("^Swietna robota, w kt^or^a  kowal w^lo^zy^l ca^le swoje "+
        "serce. Plecionka kolcza jest tak g^esta i^z z pewno^sci^a zatrzyma "+
        "sztylet, a dodatkowe ^luski na ramionach i klatce piersiowej "+
        "zwi^ekszaj^a ochron^e , jednoczenie tylko troch^e ja "+
        "obci^a^zaj^ac. Wewn^etrzna wy^sci^o^lka, zrobiona z ciel^ecej "+
        "sk^ory zmniejsza znacznie mo^zliwo^s^c otarcia cia^la i "+
        "skaleczenia przez ostre kraw^edzie, lecz nie stanowi dodatkowej "+
        "ochrony przed ciosami.\n"); 

    ustaw_material(MATERIALY_STAL, 80);
    ustaw_material(MATERIALY_SK_CIELE, 20);
    set_type(O_ZBROJE);
    set_slots(A_TORSO, A_ARMS, A_FOREARMS, A_THIGHS);
    add_prop(OBJ_I_VOLUME, 15000); 
    add_prop(OBJ_I_VALUE, 7200);   
    add_prop(OBJ_I_WEIGHT, 15000);
    add_prop(ARMOUR_S_DLA_RASY, "krasnolud");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
    set_ac(A_TORSO,12,13,4, A_ARMS,12,13,4, A_FOREARMS,12,13,4, A_THIGHS,11,12,3);
}
