/* Autor: Avard
   Opis : Sniegulak
   Data : 06.05.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("kaptur");
    dodaj_przym("obszerny", "obszerni");
    dodaj_przym("kolczy","kolczy");


    set_long("Zbroja na kt^ora patrzysz jest zrobiona z setek ma^lych "+
        "k^o^leczek po^l^aczonych ze sob^a w skomplikowany wz^or. Kowal "+
        "musia^l sp^edzi^c nad ni^a wiele czasu staraj^ac sie otrzyma^c "+
        "zamierzony efekt. Niestety cho^c dzie^lo wykonane bardzo "+
        "dok^ladnie nie jest wybitnie dobra ochrona g^lowy. ^Zaden cios "+
        "obuchem nie zostanie zatrzymany przez ta zbroje, a pchni^ecia i "+
        "ciecia mog^a zosta^c jedynie delikatnie zamortyzowane. Plecionka "+
        "zosta^la przed^lu^zona i chroni tak^ze szyj^e nosz^acego. Jest to "+
        "idealny wyb^or dla wojownika, kt^ory za^lo^zy ten czepiec pod "+
        "normalny he^lm. \n");

    set_slots(A_HEAD);
    add_prop(OBJ_I_WEIGHT, 1800);
    add_prop(OBJ_I_VOLUME, 1800);
    add_prop(OBJ_I_VALUE, 720);

	set_size("L");
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    ustaw_material(MATERIALY_STAL);
    set_type(O_ZBROJE);

    set_ac(A_HEAD,6,6,1);

}


