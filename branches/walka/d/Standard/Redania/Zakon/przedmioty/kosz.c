/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/container";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_container()
{
    ustaw_nazwe("kosz");
    dodaj_przym("wiklinowy", "wiklinowi");
    dodaj_przym("pleciony", "plecieni");

    set_long("Jest do do^s^c sporych rozmiar^ow, pleciony z wikliny kosz "+
        "w kt^orym cz^lonkowie Zakonu przechowuj^a ubrania i ekwipunek.\n");
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 500);
    add_prop(OBJ_I_VALUE, 10);
    set_type(O_MEBLE);
    add_subloc("w");
    add_subloc_prop("w", CONT_I_MAX_VOLUME, 10000);
    add_subloc_prop("w", CONT_I_MAX_WEIGHT, 10000);

    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}
