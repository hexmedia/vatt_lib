/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */
inherit "/std/receptacle";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_receptacle()
{
    ustaw_nazwe("szafa");
    dodaj_przym("ciemnozielony", "ciemnozieleni");
    dodaj_przym("drewniany", "drewniani");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Zbita z drewnianych desek pomalowana na ciemnozielony kolor, "+
        "bez ^zadnych zdobie^n sugeruje, i^z przychodz^acy go^scie mog^a "+
        "tu zostawi^c swoje wierzchnie okrycie.\n");
	     
    add_prop(OBJ_I_VALUE, 1000);
    add_prop(CONT_I_RIGID, 1);
    add_prop(CONT_I_MAX_VOLUME, 250000);
    add_prop(CONT_I_MAX_WEIGHT, 500000);
    add_prop(CONT_I_VOLUME, 10000);
    add_prop(CONT_I_WEIGHT, 20000);
    add_prop(CONT_I_NO_OPEN_DESC, 1);

}

