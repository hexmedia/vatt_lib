/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/container";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_container()
{
    ustaw_nazwe("st^o^l");
    dodaj_przym("d^lugi", "d^ludzy");
    dodaj_przym("d^ebowy", "d^ebowi");

    set_long("Nosz^acy liczne plamy t^luszczu mebel posiada wykonany jest "+
        "wprawdzie do^s^c topornie, lecz twarde sokorowe drewno nie "+
        "zamierza podda^c si^e byle ci^e^zarowi.\n");
    add_prop(OBJ_I_WEIGHT, 25000);
    add_prop(OBJ_I_VOLUME, 25000);
    add_prop(OBJ_I_VALUE, 500);
    set_type(O_MEBLE);
    add_subloc("na", 0, "ze");
    add_subloc_prop("na", SUBLOC_I_MOZNA_POLOZ, 1);
    ustaw_material(MATERIALY_DR_DAB);

    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}
