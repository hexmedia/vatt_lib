inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <object_types.h>
#include <macros.h>
#include <materialy.h>
void
create_armour()
{  
    ustaw_nazwe("kaftan");
    dodaj_przym("mi^ekki","mi^ekcy");
    dodaj_przym("wygodny", "wygodni");
    set_long("Zrobiony z dobrej gatunkowo we^lny ma^ly, zielono czerwony "+
        "kaftan ze znakiem du^zej czarnej tarczy ze skrzy^zowanymi "+
        "na niej trzema mieczami o srebrzystych r^ekoje^sciach i "+
        "karmazynowych ostrzach, przedstawia si^e wyj^atkowo okazale. "+
        "Zapinany z przodu na guziki zrobione z rogu jelenia, "+
        "wygl^ada na str^oj wizytowy, nale^z^acy do jakiej^s "+
        "dobrze usytuowanej osoby. \n");
 
    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    add_prop(OBJ_I_WEIGHT, 100); 
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 50);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("S");
    ustaw_material(MATERIALY_WELNA, 100);
    set_type(O_UBRANIA);
}