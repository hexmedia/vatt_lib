
/* 
opis popelniony przez Sniegulaka, zakodowany przez Seda 12.03.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
   ustaw_nazwe("sukienka");
    dodaj_przym("lniany", "lniani");
    dodaj_przym("prosty","pro^sci");


    set_long("Lniany materia^l nie przylega do cia^la, czyni^ac sukienk^e "
	+"obszern^a i wygodn^a w noszeniu. Si^egaj^acy do samych kostek str^oj "
	+"zakrywa zupe^lnie nogi w^la^scicielki, powi^ekszaj^ac tym samym jej figur^e. "
	+"Zauwa^zasz, ^ze sukienka posiada lekko bufiaste r^ekawy, ods^laniaj^acy "
	+"kobiece wdzi^eki g^l^eboki dekolt.\n");

    set_slots(A_THIGHS | A_CHEST | A_SHINS | A_STOMACH | A_HIPS);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 2100);
    add_prop(OBJ_I_VALUE, 2000);

	set_size("L");
    add_prop(ARMOUR_F_PRZELICZNIK, 20.0);
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
	add_prop(ARMOUR_F_POW_PIERSI, 1.25);
	add_prop(ARMOUR_F_POW_BIODRA, 1.25);
    ustaw_material(MATERIALY_LEN);
}


