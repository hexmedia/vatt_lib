/* zwykla lniana koszula, all made by Fav, 25.05.06 */


inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("koszula");
    dodaj_przym("ma^ly","mali");

    dodaj_przym("lekki","lekcy");
    set_long("Wykonana z cienkiego ciemnego materia^lu ma^la koszula, w "+
		"kt^ora zmie^sci^c si^e mo^ze co najwy^zej ch^lopiec, jest "+
		"pobrudzona gdzieniegdzie sadz^a i t^luszczem. Wida^c na niej "+
		"tak^ze liczne cerowania i ^laty, kt^ore wskazuj^a, i^z nowo^s^c "+
		"tego ubrania ju^z dawno min^e^la. \n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    add_prop(OBJ_I_WEIGHT, 320);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 13);

    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_BAWELNA, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("S");
}