/*
 * spodnie dla kuchcika
 * opis by Sniegulak,
 * zepsu^la faeve, 8 maja 2007
 *
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>


void
create_armour() 
{
      ustaw_nazwe_glowna("para");
      ustaw_nazwe("spodnie");
      


     dodaj_przym("krotki","krotcy");
     dodaj_przym("lniany","lniani");
      
     set_long("Kr^otkie spodnie zrobione z lnu, s^a pobrudzone w wielu "+
		 "miejscach, sosami sadza i jakimi^s innymi trudnymi do "+
		 "zidentyfikowania plamami. Du^za ilo^s^c ^lat na nogawkach, "+
		 "^swiadczy, ^ze osoba nosz^aca to ubranie jest niez^lym urwisem. W "+
		 "spodnie te co najwy^zej m^og^lby si^e zmie^sci^c kilkuletni "+
		 "ch^lopiec. \n");

     set_slots(A_LEGS);
     add_prop(OBJ_I_VOLUME, 200);
     add_prop(OBJ_I_VALUE, 10);
     add_prop(OBJ_I_WEIGHT, 150);

     add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
     add_prop(ARMOUR_I_DLA_PLCI, 0);
     
     set_size("S");


     ustaw_material(MATERIALY_LEN, 100);

     set_type(O_UBRANIA);
}