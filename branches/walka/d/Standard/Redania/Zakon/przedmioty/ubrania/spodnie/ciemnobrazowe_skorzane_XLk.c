
/* 
opis popelniony przez Grypina, zakodowany przez Seda 06.04.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
   ustaw_nazwe("spodnie");
    dodaj_przym("ciemnobr^azowy", "ciemnobr^azowi");
    dodaj_przym("sk^orzany","sk^orzani");


    set_long("Wykonane z ciemnobr^azowej sk^ory spodnie nosz^a ^slady noszenia. "
	+"Ma^le dziurki, lekkie przetarcia, gdzieniegdzie plamy po barwniku. "
	+"Wida^c, i^z jest to ubranie nale^z^ace do rzemie^slnika. Ich kr^oj i spos^ob "
	+"wykonania wskazuj^a na to, ^ze w^la^scicielowi nie zale^zy na modnym "
	+"wygl^adzie, lecz praktyczno^sci. Spodnie posiadaj^a du^ze boczne kieszenie, "
	+"do kt^orych mog^lyby si^e zmie^sci^c ma^le przedmioty. Niestety przy "
	+"dok^ladniejszym ich obejrzeniu dostrzegasz dziury w kieszeniach "
	+"uniemo^zliwiaj^ace trzymanie w nich czegokolwiek.\n");

    set_slots(A_LEGS | A_HIPS);
    add_prop(OBJ_I_WEIGHT, 200);
    add_prop(OBJ_I_VOLUME, 700);
    add_prop(OBJ_I_VALUE, 800);

	set_size("XL");
    add_prop(ARMOUR_S_DLA_RASY, "krasnolud");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_F_PRZELICZNIK, 20.0);
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    ustaw_material(MATERIALY_SK_SWINIA);
}


