inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");
    dodaj_przym("prosty","pro^sci");
    dodaj_przym("sk^orzany","sk^orzani");
    set_long("Czarna sk^ora, drewniana podeszwa z niskim obcasem i "+
       "prosty kr^oj, to okaz przeci^etnych wygodnych but^ow "+
       "u^zywanych przez ^srednio zamo^znych mieszczan. "+
       "Pozwalaj^a one na wygodne chodzenie po brukowanych "+
       "uliczkach pe^lnych nieczysto^sci. R^ownie dobrze "+
       "powinny sprawowa^c sie na trakcie dzi^eki temu, "+
       "i^z s^a porz^adnie zaimpregnowane pszczelim woskiem i "+
       "krople wody oraz b^loto sp^lywaj^a szybko po materiale. \n");
    set_slots(A_FEET | A_SHINS);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 500);
    add_prop(OBJ_I_VALUE, 100);
    
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("S");
    ustaw_material(MATERIALY_SK_CIELE, 70);
    ustaw_material(MATERIALY_DR_BUK, 30);
   
    set_type(O_UBRANIA);
}