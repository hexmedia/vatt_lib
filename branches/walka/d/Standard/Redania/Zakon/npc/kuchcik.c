/* 
 * kuchcik w Zakonie
 * opis: Sniegulak
 * zepsu^la faeve
 * dn. 6, 8 maja 2007
 *
 */


#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit ZAKON_STD_NPC;

int czy_walka();
int walka = 0;

void
create_zakon_npc()
{
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_living_name("brik");
    set_gender(G_MALE);
    ustaw_imie(({"brik","brika","brikowi","brika","brikiem","briku"}),
        PL_MESKI_OS);
    set_title(", kuchcik zakonny");
    dodaj_nazwy("kuchcik");

    set_long("Niski, ma^ly ch^lopiec, zapewne syn kucharki zajmuje si^e "+
		"drobnymi pracami w sto^l^owce. Jego twarz lekko pobrudzona sadz^a "+
		"wyra^za szcz^e^scie i zadowolenie, co u dzieci wykonuj^acych takie "+
		"prace jest rzadko^sci^a, Zapewne w chwilach wolnych chodzi po "+
		"okolicy i wiecznymi pytaniami zasypuje przebywaj^acych tu rycerzy. "+
		"Du^ze niebieskie oczy, i blond w^losy ^swiadcz^a o tym, i^z kiedy^s "+
		"na pewno b^edzie ^lama^l kobiece serca. Ubrany w sanda^ly lekk^a "+
		"ma^la koszule i d^lugie spodnie krz^ata si^e po kuchni. \n");

    dodaj_przym("ma^ly","mali");
    dodaj_przym("weso^ly","weseli");

    set_stats (({ 30+random(10), 63+random(10), 25, 15, 20, 20 }));

    add_prop(CONT_I_WEIGHT, 40000);
    add_prop(CONT_I_HEIGHT, 150);
  
    set_act_time(30);
    add_act("emote zamiata pod^log^e energicznie.");
    add_act("emote rozgl^ada si^e szybko, po czym ziewa dyskretnie.");
    
    set_cchat_time(10);
    add_cchat("Zostaw mnie, bo powiem tym panom w zbrojach i oni ci^e spior^a "+
		"na kwa^sne jab^lko! ");
    add_cchat("Mama! Mama! Pomocy! ");
    add_cchat("Zostaw mnie, to boli! ");
    
    add_armour(ZAKON_UBRANIA + "koszule/mala_lekka_Sc");
    add_armour(ZAKON_UBRANIA + "spodnie/krotkie_lniane_Sc");
    add_armour(ZAKON_UBRANIA + "sandaly/jasne_drewniane_Sc");
 
    add_ask(({"mam^e"}), VBFC_ME("pyt_o_mame"));
    add_ask(({"rycerzy"}), VBFC_ME("pyt_o_rycerzy"));
    add_ask(({"kuchni^e"}), VBFC_ME("pyt_o_kuchnie"));
    add_ask(({"prac^e"}), VBFC_ME("pyt_o_prac^e"));
	add_ask(({"zadanie"}), VBFC_ME("zadanie"));
    set_default_answer(VBFC_ME("default_answer"));

    set_alarm_every_hour("co_godzine");
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_mame()
{
	if(!query_attack())
	{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Moja mama jest kuchark^a! ");
	}
	else
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "krzyknij Zostawcie mnie wszyscy w spokoju! ");
	}
    return "";
}
string
pyt_o_rycerzy()
{
	if(!query_attack())
	{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Kiedy^s b^ed^e wielkim rycerzem i b^ed^e pomaga^l innym.  ");
	}
	else
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "krzyknij Zostawcie mnie wszyscy w spokoju! ");
	}
    return "";
}
string
pyt_o_prace()
{
	if(!query_attack())
	{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Praca jest ci^e^zka, ale j^a lubi^e. ");
	}
	else
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "krzyknij Zostawcie mnie wszyscy w spokoju! ");
	}
    return "";
}
string
pyt_o_zadanie()
{
	if(!query_attack())
	{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Mam za zadanie pozmywa^c naczynia. ");
	}
	else
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "krzyknij Zostawcie mnie wszyscy w spokoju! ");
	}
    return "";
}
string
pyt_o_kuchnie()
{
	if(!query_attack())
	{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Lubi^e pracowa^c w kuchni, zawsze mo^zna spotka^c kogo^s "+
		"ciekawego.  ");  
	}
	else
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "krzyknij Zostawcie mnie wszyscy w spokoju! ");
	}
    return "";
}
string
default_answer()
{
	if(!query_attack())
	{
    switch(random(2))
	{
    case 0:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz O... ja tez mam wiele pyta^n.  "); break;
	case 1:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
		"powiedz Sam chcia^lbym to wiedzie^c. "); break;
	}
	}
	else
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "krzyknij Zostawcie mnie wszyscy w spokoju! ");
	}
    return "";

}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));

}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
				  {
			  switch(random(1))
						{
				  case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
					  "emote m�wi krzywi�c si�: Bleee... Nie lubi� ca�us�w! "); 
				  set_alarm(0.5, 0.0, "command_present", wykonujacy,
					  "kopnij "+OB_NAME(wykonujacy)+" w kostke");                 
                        break;
						}
				  }
					 else
				  {
                   switch(random(1))
					    {
                   case 0:command("powiedz Nawet tata mnie nie ca�uje. "); break;
                   set_alarm(0.5, 0.0, "command_present", wykonujacy,
					  "kopnij "+OB_NAME(wykonujacy)+" w kostke");                 
                        break;
						}
				  }
				
              
               }
			   break;
        case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
	if(wykonujacy->query_gender() == 1)
           switch(random(2))
           {
			case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
				"powiedz Zostaw mnie, nie lubi^e ci^e, bo nie masz wacka! ");
				set_alarm(0.5, 0.0, "command_present", wykonujacy,
					"kopnij "+OB_NAME(wykonujacy)+" w kostke");
			break;
			case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
				"powiedz Nie g^laszcz mnie, nie jeste^s moj^a mam^a! "); 
			set_alarm(0.5, 0.0, "command_present", wykonujacy,
					"kopnij "+OB_NAME(wykonujacy)+" w kostke"); break;
		   }
		   else
			{
			   switch(random(2))
				{
				   case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
					"powiedz Nie lubi^e by^c g^laskany. "); 
				   set_alarm(0.5, 0.0, "command_present", wykonujacy,
					"kopnij "+OB_NAME(wykonujacy)+" w kostke"); break;
				   case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
					   "powiedz Zostaw mnie, mam du^zo pracy. "); 
				   set_alarm(0.5, 0.0, "command_present", wykonujacy,
					"kopnij "+OB_NAME(wykonujacy)+" w kostke");break;
				}
			}
					 
}

void
nienajlepszy(object kto)
{
   switch(random(3))
   {
      case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
		  "powiedz Bo powiem mamie a ona wygoni ci^e ^scier^a! ");
              break;
      case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
              "emote m^owi smutno wykrzywiaj^ac buzi^e: Nie bawi^e si^e tak!");
              break;
      case 2: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
		  "powiedz Mama! Mama! "+TP->koncowka("ten pan","ta pani")+ 
		  " mi robi krzywd^e! ");     
              break;
   }
} 

void
attacked_by(object wrog)
{
    if(walka == 0)
    {
        walka = 1;
		command("krzyknij Zostawcie mnie! Nie chc^e si^e w to bawi^c!");
        command("dobadz miecza");
        set_alarm(5.0, 0.0, "czy_walka", 1);
        return ::attacked_by(wrog);
    }
    else 
    {
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {

       command("powiedz Ha! Wiedzia^lem, ^ze to tylko taki "+
		   "kawa^l!");
       command("opusc bron.");

       walka = 0;
       return 1;
    }
       else
       {
       set_alarm(3.0, 0.0, "czy_walka", 1);
       }

}

void
sprzatamy()
{
    int i;
    object *wszystko=all_inventory(this_object());   
    command("wez puste naczynia");
    set_alarm(1.0,0.0,"command","zdejmij puste naczynia ze stolu");
    set_alarm(3.0,0.0,"command","zdejmij puste naczynia z drugiego stolu");
    set_alarm(5.0,0.0,"command","zdejmij puste naczynia z trzeciego stolu");
    set_alarm(7.0,0.0,"myjemy");
    if(wszystko[i]->query_naczynie())
    {
        set_alarm(8.0,0.0,"command","emote zmywa wszystkie naczynia.");
    }
    set_alarm(600.0,0.0,"sprzatamy");
}
void
myjemy()
{
    int i;
    object *wszystko=all_inventory(this_object());   
    for(i=0;i<sizeof(wszystko);i++)
    {
        if(wszystko[i]->query_naczynie())
            wszystko[i]->remove_object();
    }
}
void
start_me()
{
    set_alarm(1.0,0.0,"sprzatamy");
}

nomask int 
query_reakcja_na_walke()
{
    return 1;
}