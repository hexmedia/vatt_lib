
/* Autor: Sedzik i Avard
   Opis : Sniegulak
   Data : 06.05.07 */


inherit "/std/act/action";
inherit "/std/act/chat";
inherit "/std/act/asking";

#include "dir.h"
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
inherit ZAKON_STD_NPC;

void
create_zakon_npc()
{ 
    ustaw_odmiane_rasy("kobieta");
    dodaj_przym("pulchny","pulchni");
    dodaj_przym("rumiany","rumiani");
    dodaj_nazwy("kucharka");
    set_gender(G_FEMALE);

    set_long("Pulchna rumiana kobieta ubrana w czyst^a sukienk^e, z haftowanym "
    +"czepkiem na g^lowie przygotowuje straw^e ^zo^lnierzom stacjonuj^acym w tym "
    +"garnizonie. Jednocze^snie podaje im j^a do sto^l^ow ustawionych pod ^scian^a. "
    +"Kucharka pomimo swoich rozmiar^ow, porusza si^e po pomieszczeniu z gracj^a "
    +"m^lodej, lekkiej budowy kelnerki, a szeroki u^smiech goszcz^acy prawie "
    +"ca^ly czas na jej twarzy odejmuje jej lat. Czujne oczy przyzwyczajone "
    +"do wy^lawiania, g^lodnych i czekaj^acych os^ob, szybko rozgl^adaj^a si^e po "
    +"jadalni. \n");

    set_stats (({ 60, 60+random(10), 70, 40, 25, 100 }));
    add_prop(CONT_I_WEIGHT, 90000);
    add_prop(CONT_I_HEIGHT, 170);
    
    set_act_time(60);
    add_act("emote miesza co^s w kotle.");
    add_act("emote przygotowuje jedn^a z potraw.");


    set_cact_time(10);
    add_cact("krzyknij Rycerze na pomoc! ");
    add_cact("krzyknij Zb^ojcy w budynku! ");
    add_cact("krzyknij Atakuj^a, pom^o^zcie! ");
    
    add_armour(ZAKON_UBRANIA + "sandaly/jasne_drewniane_Lc.c");
    add_armour(ZAKON_UBRANIA + "sukienki/lniana_prosta_Lc.c");
    add_armour(ZAKON_UBRANIA + "czepki/haftowany_bialy_Lc.c");


    add_prop(LIVE_I_NEVERKNOWN, 1);

    add_ask(({"kuchnie"}), VBFC_ME("pyt_o_kuchnie"));
    add_ask(({"potrawy", "jedzenie",}),
        VBFC_ME("pyt_o_potrawy"));
    add_ask(({"zadanie", "prac^e"}), VBFC_ME("pyt_o_zadanie"));
    set_default_answer(VBFC_ME("default_answer"));

}

int
query_karczmarz()
{
	return 1;
}
 
void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
default_answer()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz A co ja? Jaki czarownik jest, ze wszystko wiem? "+
            "Patrz tylko, jak ci ryja obije...");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Nie b^ed^e szcz^epi^c j^ezyka po pr^o^znicy "+
            "kiedy me ^zycie jest zagro^zone!");
    }
    return "";
}




string
pyt_o_zadanie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: ^Zadnego zadania u mnie nie znajdziesz. ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Nie b^ed^e szcz^epi^c j^ezyka po pr^o^znicy "+
            "kiedy me ^zycie jest zagro^zone!");
    }
    return "";
}

string
pyt_o_potrawy()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: Wszystko co mog^e ci poda^c mo^zesz znale^x^c wypisane na "
        +"tablicy. ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Nie b^ed^e szcz^epi^c j^ezyka po pr^o^znicy "+
            "kiedy me ^zycie jest zagro^zone!");
    }
    return "";
}

string
pyt_o_kuchnie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: Gotowanie to moja pasja. ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Nie b^ed^e szcz^epi^c j^ezyka po pr^o^znicy "+
            "kiedy me ^zycie jest zagro^zone!");
    }
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("usmiechnij si^e mi^lo do "+ OB_NAME(ob));
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(1))
                 {
                 case 0: command("emote m^owi:  No kobieto nie przesadzaj! Dobra? "); break;        
                }
              else
                {
                   switch(random(2))
                   {
                   case 0:command("emote m^owi: Upi^le^s sie czy co? "); break;
                   case 1:command("emote m^owi: Nie mam zamiaru, dawa^c si^e "
                   +"ca^lowa^c pierwszemu lepszemu. "); break;
                   }
                }
              
                break;
                }
        case "przytul":      
            {
              if(wykonujacy->query_gender() == 1)
                switch(random(1))
                 {
                 case 0: command("emote m^owi: Nie potrzebuj^e pieszczot z "
                 +"twojej strony. "); break;        
                }
              else
                {
                   switch(random(1))
                   {
                   case 0:command("emote m^owi: Precz z ^lapskami! Nie "
                   +"obmacuj mnie! "); break;
                   }
                }
              
                break;
              }
    }
}


void
nienajlepszy(object wykonujacy)
{
  switch(random(2))
    {
    case 0: 
      command("emote m^owi: Nie pozwol^e sob^a tak pomiata^c! ");
      break;
    case 1: 
      command("emote m^owi: Won mi z mojej kuchni! ");
      break;
    }
} 

nomask int 
query_reakcja_na_walke()
{
    return 1;
}


