/**
 * Soul Zakonu Karmazynowych Ostrzy
 *
 * Autor: Krun
 * Data: 15.05.2007
 */

inherit "/cmd/std/command_driver";

#include <pl.h>
#include "dir.h"
#include <macros.h>
#include <cmdparse.h>
#include <composite.h>

#define CAN_DO(x) if(TP->query_ranga_gildiowa(GUILD_NAME) < x) return 0;
#define MSAYBB(x) saybb(QCIMIE(TP, PL_MIA) + " " + x + "\n");

string
get_soul_id()
{
    return GUILD_NAME;
}

int
query_cmd_soul()
{
    return 1;
}

public mapping
query_cmdlist()
{
    return ([
        //ZESTAW PAZIOWY
        "zkuk�o�"           : "zkuklon",
        "zkziewnij"         : "zkziewnij",
        "zku�miech"         : "zkusmiech",
        "zku�miechnij"      : "zkusmiech",
        "zkpoca�uj"         : "zkpocaluj",
        "zkpowitaj"         : "zkpowitaj",
        "zkpo�egnaj"        : "zkpozegnaj",
        //ZESTAW GIERMKOWSKI
        "zkdob�d�"          : "zkdobadz",
        "zkopu��"           : "zkopusc",
        "zkoce�"            : "zkocen",
        "zkzapami�taj"      : "zkzapamietaj", //Narazie nie koduje bo mi si� g�upie wydaje.
        //ZESTAW RYCERSKI
        "zkz�orzecz"        : "zkzlorzecz",
        "zkprzysi�gnij"     : "zkprzysiegnij",
        "zkgot�w"           : "zkgotow",
        "zksalutuj"         : "zksalutuj",
        "zkbaczno��"        : "zkbacznosc",
        "zkwyzwij"          : "zkwyzwij",
        //EMOTY NIE WIEM JAKIE, chwilowo wszystkich.
        "zkhonory"          : "zkhonory",
        "zkwyra�"           : "zkwyraz",
        //ZESTAW SZEFA... awanse itp.
        "zkawansuj"         : "zkawansuj",
        "zkdegraduj"        : "zkdegraduj",
        //ZESTAW DLA WSZYSTKICH
        "zkopu��"           : "zkopusc",
        "zkpomoc"           : "zkpomoc"]);
}

static
jest_ktos_na_lokacji()
{
    if(!TP) 
        return 0;
    foreach(object ob : all_inventory(ENV(TP)))
    {
        if(interactive(ob) && ob != TP)
            return 1;
    }
}

int
zkpomoc(string arg)
{
    return 0; //Avard napisz jak�� pomoc:P
}

int
zkuklon(string arg)
{
    CAN_DO(1);
    
    NF("Co chcesz zrobi�?\n");
    
    if(!arg)
        return 0;
    
    if(arg ~= "si�")
    {
        if(!jest_ktos_na_lokacji())
            write("Opuszczaj�c lekko g�ow� wykonujesz g��boki uk�on.\n");
        else
        {
            write("Opuszczaj�c lekko g�ow� wykonujesz g��boki uk�on, w kierunku zgromadzonych tutaj os�b.\n");
            MSAYBB("opuszczaj�c lekko g�ow� wykonuje g��boki uk�on, w kierunku zgromadzonych tutaj os�b.");   
        }
    }
    else if(wildmatch("sie *", arg))
    {
        NF("Komu chcesz si� uk�oni�?\n");
        object *c = parse_this(arg, "'si�' %l:" + PL_CEL);//, SUBL_ACS_SEE);
        
        if(!sizeof(c))
            return 0;
        
        actor("Opuszczaj�c lekko g�ow� wykonujesz g��boki uk�on, w kierunku", c, PL_DOP);
        targetbb("opuszczaj�c lekko g�ow� wykonuje g��boki uk�on, w twoim kierunku.", c);
        all2actbb("opuszczaj�c lekko g�ow� wykonuje g��boki uk�on, w kierunku", c, PL_DOP); 
    }
    else
    {
        NF("Co chcesz zrobi�?\n"); 
        return 0;
    }
    return 1;
}

int
zkziewnij()
{
    CAN_DO(1);
    write("Delikatnie zas�aniaj�c usta d�oni�, ziewasz przeci�gle" +
        (jest_ktos_na_lokacji() ? " daj�c zgromadzonym do zrozumienia, "+
        "i� ogarnia ci� znudzenie.\n" : ".\n"));
    MSAYBB("zas�aniaj�c usta d�oni�, ziewa przeci�gle.");
    return 1; 
}

int
zkusmiech(string arg)
{
//     find_player("krun")->catch_msg("Bzzzzzz.\n");
    CAN_DO(1);
//     find_player("krun")->catch_msg("Kurwa dlaczego to nie dzia�a?\n");
    NF("Co chcesz zrobi�?\n");
    
    if(!arg)
        return 0;
    
    string verb = query_verb();
       
    if(verb ~= "zku�miechnij")
    {
        mixed c;
        if(arg ~= "si� czaruj�co")
        {
            write("Twoje oczy rozb�yskuj� na chwile, a na twarzy pojawia sie pe�en u�miech.\n");
            say("Oczy " + QCIMIE(TP, PL_DOP) + " rozb�yskuj� na chwil�, a na jego twarzu pojawia si� pe�en u�miech.\n");
            return 1;
        }
        else if(wildmatch("si� *", arg))
            arg = arg[4..-1];
        else
        {
            NF("Co chcesz zrobi�?\n");
            return 0;
        }
    }

    NF("Do kogo chcesz si� u�miechn��?\n");

    object *c = parse_this(arg, "'do' %l:" + PL_DOP);

    if(!sizeof(c))
        return 0;

    actor("U�miechasz si� do", c, PL_DOP, " p�g�bkiem zachowuj�c przy "+
        "tym kamienn� twarz, staraj�c si� nie ujawni� ogarniaj�cej ci� weso�o�ci.");
    targetbb("u�miecha si� do ciebie p�g�bkiem zachowuj�c przy tym kamienn� twarz, "+
        "lecz w jego oczach skacz� weso�e ogniki.", c, "weso�o");
    all2actbb("u�miecha si� do", c, PL_DOP, " p�g�bkiem zachowuj�c przy tym "+
        "kamienn� twarz, lecz w jego oczach skacz� weso�e ogniki.");

    return 1;
}

int
zkpocaluj(string arg)
{
    CAN_DO(1);
    
    NF("Kogo chcesz poca�owa�?\n");
    
    if(!arg)
        return 0;
    
    int nwd;
    string patt;
    if(!wildmatch("* w d�o�", arg))
    {
        nwd = 1;
        patt = "%l:" + PL_BIE;
    }
    else
        patt = "%l:" + PL_BIE + " 'w' 'd�o�'";
    
    if(nwd)
    object *c = parse_this(arg, patt);    
    
    if(!sizeof(c))
        return 0;
    
    if(sizeof(c) > 1)
    {
        NF("Nie mo�esz poca�owa� na raz kilku os�b.\n");
        return 0;
    }
    
    if(!nwd)
    {
        NF("W co chcesz " + c[0]->kocowka("go", "j�", "je") + " poca�owa�?\n");
        return 0;
    }
    // �niegu chcia� tylko dla kobiet, ale nie widz� sensu w technicznym ograniczeniu:P
    actor("Opuszczasz lekko g�ow�, po czym sk�adasz delikatny poca�unek na d�oni", c, PL_DOP);
    targetbb("opuszcza lekko g�ow�, po czym sk�ada delikatny poca�unek na twojej d�oni.", c, "delikatnie");
    all2actbb("opuszcza lekko g�ow�, po czym sklada delikatny poca�unek na d�oni", c, PL_DOP);
    
    return 1;
}

int
zkpowitaj(string arg)
{
    CAN_DO(1);
    
    NF("Kogo chcesz powita�?\n");
    
    if(!arg)
        return 0;
    
    object *c = parse_this(arg, "%l:" + PL_BIE);
    
    if(!sizeof(c))
        return 0;
    
    if(sizeof(c) > 1)
    {
        NF("Nie mo�esz powita� kilku os�b jednocze�nie.\n");
        return 0;
    }
    
    // �niegu chcia� jeszcze przys��wek jak, ale nie wiem jak to stosowa� jeszcze:P
    actor("Podnosisz do g�ry g�ow� i wykonuj�c g��boki uk�on witasz", c, PL_BIE);
    target("podnosi g�ow� do g�ry i wykonuj�c g��boki uk�on wita ci�.", c);
    all2actbb("podosi g�ow� do g�ry i wykonuj�c g��boki uk�on wita", c, PL_BIE);
    
    return 1;
}

int
zkpozegnaj(string arg)
{
    CAN_DO(1);
    
    NF("Kogo chcesz po�egna�?\n");
    
    if(!arg)
        return 0;
    
    object *c = parse_this(arg, "%l:" + PL_BIE);
    
    if(!sizeof(c))
        return 0;
    
    if(sizeof(c) > 1)
    {
        NF("Nie mo�esz po�egna� kilku os�b jednocze�nie.\n");
        return 0;
    }
    
    actor("W ge�cie po�egnania wykonujesz lekki uk�on, a unosz�c sw� "+
        "g�ow� w g�r� patrzysz", c, PL_CEL, " g��boko w oczy i r�wnocze�nie "+
        "przyk�adasz rek� do piersi.");
    targetbb("w ge�cie po�egnania wykonuje lekki uk�on, a unosz�c sw� g�ow�" +
        "w g�r� patrzy Ci g��boko w oczy, r�wnocze�nie przyk�adaj�c sw^a r�k� "+
        "do piersi.", c);
    all2actbb("w ge�cie po�egnania wykonuje lekki uk�on, a unosz�c sw� "+
        "g�ow� w g�r� patrzy", c, PL_CEL, " g��boko w oczy, r�wnocze�nie "+
        "przyk�adaj�c sw^a r�k� do piersi.");
     
    return 1;
}

int
zkdobadz(string arg)
{
    CAN_DO(2);
    
    NF("Czego chcesz doby�?\n");
    
    if(!arg)
        return 0;
    
    object *c = parse_this(arg, "%i:" + PL_DOP);
    
    if(!sizeof(c))
        return 0;
    
    if(sizeof(c) > 1)
    {
        NF("Nie mo�esz doby� kilku broni jednocze�nie.\n");
        return 0;
    }
    
    if(!c[0]->is_weapon())
    {
        NF("Doby� mo�esz tylko broni.\n");
        return 0;
    }
    
    mixed wield_com = c[0]->wield_me(1);
    
    if(stringp(wield_com))
    {
        NF(wield_com);
        return 0;
    }
    
    write("Powolnym, wy�wiczonym ruchem dobywasz " + COMPOSITE_DEAD(c, PL_DOP) + ".\n");
    MSAYBB("powolnym, wy�wiczonym ruchem dobywa " + QCOMPDEAD(PL_DOP) + ".");
    
    return 1;
}

int
zkopusc(string arg)
{
    CAN_DO(2);
    
    NF("Co chcesz opu�ci�?\n");
    
    if(!arg)
        return 0;
    
    object *c = parse_this(arg, "%i:" + PL_BIE);
    
    if(!sizeof(c))
        return 0;
    
    if(sizeof(c) > 1)
    {
        NF("Nie mo�esz opu�ci� kilku broni jednocze�nie.\n");
        return 0;
    }
    
    if(!c[0]->is_weapon())
    {
        NF("Opu�ci� mo�esz tylko bro�.\n");
        return 0;
    }
    
    int unwield_success = c[0]->unwield_me(1);
    
    if(!unwield_success)
    {
        NF("Nie dobywasz "+ COMPOSITE_DEAD(c, PL_DOP) + ".\n");
        return 0;
    }
    
    write("Bez po�piechu rozgl�dasz sie w ko�o i opuszczasz " + COMPOSITE_DEAD(c, PL_BIE) + ".\n");
    MSAYBB("bez po�piechu rozgl�daj�c si� w ko�o opuszcza " + QCOMPDEAD(PL_BIE) + ".");
    
    return 1;
}

int
zkocen(string arg)
{
    CAN_DO(2);
    
    NF("Kogo lub co chcesz oceni�?\n");
    
    if(!arg)
        return 0;
    
    object *c = parse_this(arg, "%i:" + PL_BIE);
    
    if(!sizeof(c))
        return 0;
    
    if(sizeof(c) > 1)
    {
        NF("Nie mo�esz oceni� wi�cej ni� jedn� rzecz na raz.\n");
        return 0;
    }
    
    actor("Patrz�c czujnym wzrokiem i marszcz�c brwi oceniasz", c, PL_BIE);
    targetbb("parz�c czujnym wzrokiem i marszcz�c brwi rzuca Ci oceniaj�ce spojrzenie.", c);
    all2actbb("patrz�c czujnym wzrokiem i marszcz�c brwi rzuca oceniaj�ce spojrzenie", c, PL_CEL);
    
    return 1;
}

int
zkzapamietaj(string arg)
{
    CAN_DO(1);
    return 0; //KOMENDY CHWILOWO NIE MA BO MI SI� NIE PODOBA�A WI�C NIE KODOWA�EM:P
}

int
zkzlorzecz(string arg)
{
    CAN_DO(4);
    
    NF("Co chcesz zrobi�?\n");
    
    if(arg)
        return 0;
    
    write("Twoja twarz przybiera kamienny wyraz, po czym nie�wiadomie "+
        "zaciskaj�c pie�ci, zaczynasz mrucze� pod nosem obelgi.\n");
    saybb("Twarz " + QCIMIE(TP, PL_DOP) + " przybiera kamienny wyraz, "+
        "po czym zaciskaj�c pie�ci, zaczyna co� mrucze� pod nosem.\n");
     
    return 1;
}

int
zkprzysiegnij(string arg)
{
    CAN_DO(4);
    
    NF("Komu i co chcesz przysi�gn��?\n");
    
    if(!arg)
        return 0;
    
    object *c;
    
    if(!parse_command(arg, all_inventory(ENV(TP)), "%l:" + PL_CEL + "%s", c, arg))
        return 0;
    
    if(!sizeof(c))
        return 0;
    
    c = NORMAL_ACCESS(c, 0, 0);
    
    if(sizeof(c) > 1)
    {
        NF("Nie mo�esz przysi�ga� kilku osobom na raz.\n");
        return 0;
    }
    
    if(!arg || !strlen(arg))
    {
        NF("Co chcesz przysi�gn�� " + COMPOSITE_LIVE(c, PL_CEL) + ".\n");
        return 0;
    }
    
    actor("Przykl�kasz na jedno kolano i opuszczaj�c g�ow� przed", c, PL_NAR,
        " pe�nym szacunku g�osem wymawiasz s�owa: " + UC(arg) + ".");
    targetbb("przykl�ka na jedno kolano i opuszczaj�c g�ow� przed "+
        "Tob� pe�nym szacunku g�osem wymawia s�owa: " + UC(arg) + ".", c);
    all2actbb("przykl�ka na jedno kolano i opuszczaj�c g�ow� przed", c, PL_NAR,
        " pe�nym szacunku g�osem wymawia s�owa: " + UC(arg) + ".");

    return 1;
}

private static object *
noszone_bronie()
{
    object *weapons = ({});
    object weapon;
    
    if(weapon = TP->query_weapon(W_BOTH))
        weapons += ({weapon});
    else
    {
        if(weapon = TP->query_weapon(W_RIGHT))
            weapons += ({weapon});
        if(weapon = TP->query_weapon(W_LEFT))
            weapons += ({weapon});
    }
    if(weapon = TP->query_weapon(W_FOOTL))
        weapons += ({weapon});
    if(weapon = TP->query_weapon(W_FOOTR))
        weapons += ({weapon});
    
    return weapons;
}

int
zkgotow(string arg)
{
    CAN_DO(4);
    
    if(arg)
    {
        NF("Co chcesz zrobi�?\n");
        return 0;
    }
    
    object *bronie;
    
    if(!sizeof(bronie = noszone_bronie()))
    {
        write("Podnosz�c praw� rek� do g�ry oznajmiasz swoj� gotowo�� do walki.\n");
        MSAYBB("podnosi do g�ry praw� r�k� oznajmiaj�c tym samym swoj� gotowo�� do walki.");
        return 1;
    }
    
    write("Podnosz�c do g�ry " + COMPOSITE_DEAD(bronie, PL_BIE) + " oznajmiasz swoj� gotowo�� do walki.\n");
    MSAYBB("podnosi do g�ry " + QCOMPDEAD(PL_BIE) + " oznajmiaj�c tym samym swoj� gotowo�� do walki.");
    
    return 1;
}

int
zksalutuj(string arg)
{
    CAN_DO(4);
    
    if(!arg)
    {
        write("Staj�c na baczno�� przyk�adasz pi�� do klatki piersiowej.\n");
        saybb("Staj�c na baczno�� " + QCIMIE(TP, PL_MIA) + " przyk�ada pi�� "+
            "do swojej klatki piersiowej.\n");
        return 1;
    }
    
    object *c = parse_this(arg, "%l:" + PL_CEL);
    
    NF("Komu chcesz zasalutowa�?\n");
    
    if(!sizeof(c))
        return 0;
    
    actor("Staj�c na baczno�� przyk�adasz pi�� do klatki piersiowej i oddajesz honory",
         c, PL_CEL);
    targetbb("staj�c na baczno�� przyk�ada pi�� do klatki piersiowej i oddaje ci honory.", c);
    all2actbb("staj�c na baczno�� przyk�ada pi�� do klatki piersiowej i oddaje honory", c, PL_CEL);
    
    return 1;
}

int
zkbacznosc(string arg)
{
    CAN_DO(4);
    
    if(arg)
    {
        NF("Co chcesz zrobi�?\n");
        return 0;
    }
    
    write("Prostuj�c sie, r�wnocze�nie wypinasz klatk� piersiow� do przodu, "+
        "a twoja twarz przybiera kamienny wyraz.\n");
    MSAYBB("prostuje sie, r�wnocze�nie wypina klatk� piersiow� do przodu, "+
        "a jego twarz przybiera kamienny wyraz.");
    
    return 1;
}

int
zkwyzwij(string arg)
{
    CAN_DO(4);
    
    NF("Kogo chcesz wyzwa�?\n");
    
    if(!arg)
        return 0;

    object *c = parse_this(arg, "%l:" + PL_BIE);

    if(!sizeof(c))
        return 0;

    if(sizeof(c) > 1)
    {
        NF("Nie mo�esz wyzwa� kilku os�b na raz.\n");
        return 0;
    }

    actor("Ze spokojna twarz�, szybkim i energicznym krokiem podchodzisz do",
        c, PL_BIE, " i wymierzasz mu siarczysty policzek, b�d�cy wyzwaniem "+
        "na rycerski pojedynek.");
    targetbb("ze spokojn� twarz� podchodzi do ciebie szybkim i energicznym krokiem i "+
        "wymierza ci siarczysty policzek, b�d�cy wyzwaniem na rycerski pojedynek.", c);
    all2actbb("ze spokoj� twarz� podchodzi do", c, PL_DOP, " szybkim i energicznym krokiem i "+
        "wymierza " + c[0]->koncowka("mu", "jej") + " siarczysty policzek, "+
        "b�d�cy wyzwaniem na rycerski pojedynek.");

    return 1;
}

int
czy_odpowiednia_bron(object ob)
{
    return (ob->is_weapon() && (ob->query_wt() == W_MIECZ || ob->query_wt() == W_SZTYLET));
}

int
zkhonory(string arg)
{
    CAN_DO(3);

    NF("Komu chcesz czyni� honory?\n");	

    if(!arg)
        return 0;

    object *bron, *c;

    if(parse_command(arg, all_inventory(ENV(TP)) + all_inventory(TP), "%l:" + PL_CEL + " %i:" + PL_NAR, c, bron))
        return 0;

    if(sizeof(c))
        c = NORMAL_ACCESS(c, 0, 0);
    if(sizeof(bron))
        bron = NORMAL_ACCESS(c, 0, 0);

    if(!sizeof(c))
        return 0;

    bron = bron || filter(all_inventory(TP), czy_odpowiednia_bron);

    if(!sizeof(c))
    {
        NF("Komu chcesz czyni� honory " + COMPOSITE_DEAD(bron, PL_NAR) + "?\n");
        return 0;
    }

    if(sizeof(c) > 1)
    {
        NF("Honory mo�esz czyni� tylko jednej osobie w tym samym czasie.\n");
        return 0;
    }

    if(!sizeof(bron))
    {
        NF("Nie masz czym czyni� honor�w " + COMPOSITE_LIVE(c, PL_CEL) + "?\n");
        return 0;
    }

    bron = filter(bron, &operator(==)(TP,) @ &->query_wielded());

    if(!sizeof(bron))
    {
        NF("Aby to zrobi� musisz mie� dobyty jaki� miecz lub sztylet.\n");
        return 0;
    }

    if(sizeof(bron) > 1)
    {
        NF("Zdecyduj si�, kt�r� broni� chcesz czyni� honory " + COMPOSITE_LIVE(c, PL_CEL) + "?\n");
        return 0;
    }
    
    if(bron[0]->query_wielded() != TP)
    {
        NF("Najpierw musisz doby� " + COMPOSITE_DEAD(bron, PL_DOP) + ".\n");
        return 0;
    }

    actor("Szybkim ruchem unosisz " + COMPOSITE_DEAD(bron, PL_BIE) + " p�azem do twarzy "+
        "salutujac", c, PL_CEL, ", po czym sprawnie przechodzisz do pozycji "+
        "szermierczej i zamierasz got�w do walki.");
    targetbb("szybkim ruchem unosi " + QCOMPDEAD(PL_BIE) + " p�azem do twarzy salutujac "+
        "Ci, po czym sprawnie przechodzi do pozycji szermierczej i zamiera got�w do walki.", c);
    all2actbb("szybkim ruchem unosi " + QCOMPDEAD(PL_BIE) + " p�azem do twarzy salutujac",
        c, PL_CEL, " po czym sprawnie przechodzi do pozycji szermierczej i zamiera got�w do walki."); 

    return 1;
}

int
zkwyraz(string arg)
{
    CAN_DO(3);

    if(!arg)
    {
        NF("Co chcesz wyrazi�?\n");
        return 0;
    }

    NF("Komu chcesz wyrazi� uznanie?\n");

    object *c = parse_this(arg, "'uznanie' %l:" + PL_CEL);

    if(!sizeof(c))
        return 0;

    if(sizeof(c) > 1)
    {
        NF("W jednym mom�cie mo�esz wyra�i� uznanie tylko jednej osobie.\n");
        return 0;
    }

    actor("K�adziesz r�k� na ramieniu", c, PL_DOP, " i kiwasz g�ow� z uznaniem, u�miechaj�c si� przy tym lekko.");
    targetbb("k�adzie ci r�k� na ramieniu i kiwa g�ow� z uznaniem, u�miechaj�c si� przy tym lekko.", c);
    all2actbb("k�adzie r�k� na ramieniu", c, PL_DOP, " i kiwa g�ow� z uznaniem, u�miechaj�c si� przy tym lekko.");

    return 1;
}

int
zkdegraduj(string arg)
{
    CAN_DO(7);

    NF("Kogo chcesz zdegradowa�?\n");

    if(!arg)
        return 0;

    object c = SECURITY->finger_player(arg, PL_BIE);
    
    string imie = c->query_real_name();
    string guild_driver = TP->query_gildia(GUILD_NAME);
    
    if((guild_driver)->query_czlonek(imie) < 1)
    {
        NF(UC(arg) + " nie jest cz�onkiem gildii. Nie mo�esz wi�c go zdegradowa�.\n");
        return 0;
    }
    
    if(imie == TP->query_real_name())
    {
        NF("Sam siebie nie mo�esz zdegradowa�.\n");
        return 0;
    }
    
    int ranga;
    if((ranga = (guild_driver)->query_ranga_czlonka(imie)) >= (guild_driver)->query_ranga_czlonka(TP->query_real_name()))
    {
        NF("Nie mo�esz zdegradowa� kogo� o randze wi�kszej lub r�wnej twojej randze.\n");
        return 0;
    }
    
    if(ranga == 1)
    {
        NF("Nie mo�esz ju� zdegradowa� " + UC(arg) + ". On ju� ma najni�sz� rang�.\n");
        return 0;
    }
    
    if((guild_driver)->ustaw_range_czlonka(imie, --ranga) < 1)
    {
        NF("Wyst�pi� powa�ny b��d, zg�o� go opiekunowi gildii, kt�rym jest " + 
            UC((guild_driver)->query_opiekun()) + " opisuj�c okoliczno�ci "+
            "w jakich do niego dosz�o.\n");
        return 0;
    }
    
    write("Degradujesz " + UC(arg) + " na " + (guild_driver)->query_tytul_czlonka(imie) + ".\n");
    
    return 1;
}

int
zkawansuj(string arg)
{
    CAN_DO(7);
    
    NF("Kogo chcesz awansowa�?\n");
    
    if(!arg)
        return 0;
    
    object c = SECURITY->finger_player(arg, PL_BIE);
    
    string imie = c->query_real_name();
    string guild_driver = TP->query_gildia(GUILD_NAME);
    
    if((guild_driver)->query_czlonek(imie) < 1)
    {
        NF(UC(imie) + " nie jest cz�onkiem gildii. Nie mo�esz wi�c go awansowa�.\n");
        return 0;
    }
    
    if(imie == TP->query_real_name())
    {
        NF("Sam siebie nie mo�esz awansowa�.\n");
        return 0;
    }
    
    int ranga;
    if((ranga = (guild_driver)->query_ranga_czlonka(imie) + 1) >= (guild_driver)->query_ranga_czlonka(TP->query_real_name()))
    {
        NF("Nie mo�esz awansowa� nikogo do swojej lub te� wy�szej rangi.\n");
        return 0;
    }
    
    if(sizeof((guild_driver)->query_tytuly()) < ranga)
    {
        NF(UC(imie) + " ma ju� najwy�szy poziom. Nie mo�esz go dalej awansowa�.\n");
        return 0;
    }
    
    if((guild_driver)->ustaw_range_czlonka(imie, ranga++) < 1)
    {
        NF("Wyst�pi� powa�ny b��d, zg�o� go opiekunowi gildii, kt�rym jest " + 
            UC((guild_driver)->query_opiekun()) + " opisuj�c okoliczno�ci "+
            "w jakich do niego dosz�o.\n");
        return 0;
    }
    
    write("Awansujesz " + UC(arg) + " na " + (guild_driver)->query_tytul_czlonka(imie) + ".\n");
    
    return 1;
}