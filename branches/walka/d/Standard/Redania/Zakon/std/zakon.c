inherit "/std/room";

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>

void
create_zakon()
{
}

nomask void
create_room()
{
    set_long("@@dlugi_opis@@"); 
    set_polmrok_long("@@opis_nocy@@");
    
    add_prop(ROOM_I_TYPE,ROOM_NORMAL);
    add_prop(ROOM_I_INSIDE,1);
    
    create_zakon();
}
