
#include "dir.h"
#include <macros.h>

inherit "/std/monster";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

void
create_zakon_npc()
{
}

nomask void
create_monster()
{
    create_zakon_npc();
    set_wzywanie_strazy(0); 
    set_reakcja_na_walke(1); 
    set_spolecznosc("Zakon Karmazynowych Ostrzy");
}

