inherit "/std/room";

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>

void
create_zakon()
{
}

nomask void
create_room()
{
    set_long("@@dlugi_opis@@"); 
    set_polmrok_long("@@opis_nocy@@");
    add_sit("pod �cian�", "pod �cian�", "spod �ciany", 0);
	
	add_prop(ROOM_I_TYPE,ROOM_IN_CITY);
    add_prop(ROOM_I_INSIDE,0);
    set_event_time(500.0);
    add_event("Podmuch wiatru owiewa tw^a twarz.\n");
	
    create_zakon();
}
