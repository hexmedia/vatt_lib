inherit "/std/room";

#include <stdproperties.h>
#include <macros.h>
#include "dir.h"
#define REP_MAT "/d/Standard/items/materialy/"
void create_room() 
{
    set_short("Tajny magazyn");
    set_long("Jeste� w tajnym magazynie. Nie ma st�d �adnego wyj�cia!\n");
    add_prop(ROOM_I_INSIDE,1);

    add_object(REP_MAT + "sk_swinska",4);
    add_object(REP_MAT + "stal_sztabka",4);
    add_object(REP_MAT + "zelazo_sztabka",4);
    add_object(REP_MAT + "sk_cieleca",4);
    add_object(REP_MAT + "tk_pikowa",4);
    add_object(REP_MAT + "debowe_drewno",4);
    add_object(REP_MAT + "sk_bydle",4);

    add_object(ZAKON_BRONIE + "mloty/duzy_ciezki");
}

