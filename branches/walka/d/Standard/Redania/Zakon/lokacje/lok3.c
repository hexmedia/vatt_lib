/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ZAKON_STD;

void create_zakon() 
{
    set_short("Sie^n");
    add_exit(ZAKON_LOKACJE + "lok4.c","nw",0,1,0);
    add_exit(ZAKON_LOKACJE + "lok5.c","ne",0,1,0);
    add_object(ZAKON_DRZWI + "drzwi_z_zakonu");
    add_object(ZAKON_PRZEDMIOTY + "lawa");
    add_object(ZAKON_PRZEDMIOTY + "dywan");
    add_object(ZAKON_PRZEDMIOTY + "lampa_srodek");
    add_object(ZAKON_PRZEDMIOTY + "szafa");
    add_object(ZAKON_PRZEDMIOTY + "stojak");
    dodaj_rzecz_niewyswietlana("ciemnozielona drewniana szafa",1);
    dodaj_rzecz_niewyswietlana("gruby we^lniany dywan",1);
    dodaj_rzecz_niewyswietlana("ciemna rze^xbiona ^lawa",1);
    dodaj_rzecz_niewyswietlana("drewniany prosty stojak",1);
    add_npc(ZAKON_NPC + "mlodzieniec");
    add_npc(ZAKON_NPC + "straznik_krasnolud");
    add_npc(ZAKON_NPC + "straznik_mezczyzna");
}

public string
exits_description() 
{
    return "W po^ludniowej ^scianie znajduj^a si^e drzwi na zewn^atrz, "+
        "za^s na p^o^lnocnym-zachodzie i p^o^lnocnym-wschodzie wida^c "+
        "korytarze.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Znajdujesz si^e w sieni Zakonu Karmazynowych Ostrzy, jednej z "+
        "organizacji strzeg^acej prawa i chroni^acych biednych. ";
    if(jest_rzecz_w_sublokacji(0, "otwarta ciemnozielona drewniana szafa") || 
    jest_rzecz_w_sublokacji(0, "zamkni^eta ciemnozielona drewniana szafa") || 
        jest_rzecz_w_sublokacji(0, "ciemna rze^xbiona ^lawa") || 
        jest_rzecz_w_sublokacji(0, "drewniany prosty stojak"))
    {
        str += "Komnata jasna i przestronna jest skromnie umeblowana w ";
    }
    if(jest_rzecz_w_sublokacji(0, "ciemna rze^xbiona ^lawa"))
    {
        str += "^law^e na kt^orej mo^zna przysi^a^s^c w oczekiwaniu na "+
            "kogo^s";
    }
    if(jest_rzecz_w_sublokacji(0, "otwarta ciemnozielona drewniana szafa") || 
    jest_rzecz_w_sublokacji(0, "zamkni^eta ciemnozielona drewniana szafa") && 
        jest_rzecz_w_sublokacji(0, "ciemna rze^xbiona ^lawa"))
    {
        str += ", ";
    }
    if(jest_rzecz_w_sublokacji(0, "otwarta ciemnozielona drewniana szafa") || 
    jest_rzecz_w_sublokacji(0, "zamkni^eta ciemnozielona drewniana szafa"))
    {
        str += " szaf^e do kt^orej mo^zna wsadzi^c p^laszcze i "+
            "ub^locone buty";
    }
    if(jest_rzecz_w_sublokacji(0, "drewniany prosty stojak"))
    {
        str += " oraz stojak na bronie";
    }
    if(jest_rzecz_w_sublokacji(0, "otwarta ciemnozielona drewniana szafa") || 
    jest_rzecz_w_sublokacji(0, "zamkni^eta ciemnozielona drewniana szafa") || 
        jest_rzecz_w_sublokacji(0, "ciemna rze^xbiona ^lawa") || 
        jest_rzecz_w_sublokacji(0, "drewniany prosty stojak"))
    {
        str +=". ";
    }
    if(jest_rzecz_w_sublokacji(0, "gruby we^lniany dywan"))
    {
        str += "Dywan, kt^ory le^zy na ziemi jest mi^ekki i wycisza "+
            "kroki, zarazem dobrze ociepla pomieszczenie. ";
    }
    str += "^Sciany proste i niczym nie przyozdobione nadaj^a komnacie "+
        "ascetyczny wygl^ad, wisz^a na nich jedynie proste, olejne lampy.\n";
    return str;
}



