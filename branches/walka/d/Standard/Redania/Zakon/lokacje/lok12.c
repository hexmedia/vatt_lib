/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ZAKON_STD;

void create_zakon() 
{
    set_short("Sala rycerska");

    add_exit(ZAKON_LOKACJE + "lok10.c","nw",0,1,0);
    add_exit(ZAKON_LOKACJE + "lok11.c","ne",0,1,0);
    add_exit(ZAKON_LOKACJE + "lok8.c","n",0,1,0);
    add_exit(ZAKON_LOKACJE + "sypialnia.c","e",0,1,0);
    add_object(ZAKON_PRZEDMIOTY + "lampa_srodek");
    add_object(ZAKON_PRZEDMIOTY + "okragly_stol");
    add_object(ZAKON_PRZEDMIOTY + "wysokie_krzeslo",15);
    add_object(ZAKON_PRZEDMIOTY + "kominek");
    add_object(ZAKON_DRZWI + "drzwi_do_zbrojowni");
    dodaj_rzecz_niewyswietlana("wysokie rze^xbione krzes^lo",15);
    dodaj_rzecz_niewyswietlana("kamienny kominek");
}

public string
exits_description() 
{
    return "Korytarz prowadzi na p�nocny-zach^od i p�nocny-wsch^od"+
        ", na zachodniej ^scianie znajduj^a si^e drzwi prowadz^ace "+
        "do zbrojowni, na wschodzie wida^c sypialnie, za^s na p^o^lnocy "+
        "mo^zna dostrzec pomieszczenie ze schodami.\n";  
}

string
dlugi_opis()
{
    string str;
    int i, il, il_krzesel;
    object *inwentarz;
   
    inwentarz = all_inventory();
    il_krzesel = 0;
    il = sizeof(inwentarz);
   
    for (i = 0; i < il; ++i) {
         if (inwentarz[i]->jestem_krzeslem_zakonu()) {
                        ++il_krzesel; }}

    str = "Centralne miejsce tego obszernego pomieszczenia zajmuje "+
        "olbrzymi, okr^ag^ly st^o^l";
    if(il_krzesel == 0)
    {
        str += ". ";
    }
    if(il_krzesel == 1)
    {
        str += ", a przy nim jedno wysokie, wprawnie rze^xbione krzes^lo. ";
    }
    if(il_krzesel == 2)
    {
        str += ", a przy nim dwa wysokie, wprawnie rze^zbione krzes^la. ";
    }
    if(il_krzesel == 3)
    {
        str += ", a przy nim trzy wysokie, wprawnie rze^zbione krzes^la. ";
    }
    if(il_krzesel == 4)
    {
        str += ", a przy nim cztery wysokie, wprawnie rze^zbione krzes^la. ";
    }
    if(il_krzesel > 5)
    {
        str += ", otoczony wysokimi, wprawnie rze^xbionymi krzes^lami. ";
    }
    str += "Pok^oj ogrzewany i o^swietlany jest dzi^eki kamiennemu "+
        "murowanemu kominkowi stoj^acemu na p^o^lnocnej ^scianie. Kilka "+
        "w^askich okien wychodzi na po^ludnie, za^s w zachodniej i "+
        "wschodniej ^scianie umieszczone zosta^ly drzwi. Najwi^eksze "+
        "przej^scie wiedzie wprost do zbrojowni.\n";
    return str;
}

