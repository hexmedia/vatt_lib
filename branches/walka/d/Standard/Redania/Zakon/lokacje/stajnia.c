/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ZAKON_STD;

void create_zakon() 
{
    set_short("Przestronna stajnia");
    add_item(({"sprz^et je^xdziecki","osprz^et je^xdziecki","sprz^et",
        "osprz^et"}),"Cz^e^s^c stajni oddzielona od reszty pomieszczenia "+
        "niewysokim murkiem wype^lniona jest stojakami obwieszonymi "+
        "przer^o^znym sprz^etem je^xdzieckim. Znajdzie si^e tu wszystko od "+
        "ci^e^zkich kawaleryjskich kulbak po delikatne my^sliwskie siod^la. "+
        "Znajd^a si^e tu og^lowia na wszelk^a okazj^e, z ogromnym, bojowym "+
        "munsztrunkiem na czele. Wszystko jest w miar^e nowe i zosta^lo "+
        "dok^ladnie naoliwione.\n");
    add_item(({"kszta^lty","mroczne kszta^lty"}),"Cz^e^s^c stajni "+
        "oddzielona od reszty pomieszczenia niewysokim murkiem wype^lniona "+
        "jest ton^acymi w ciemno^sciach stojakami obwieszonymi sprz^etem "+
        "je^xdzieckim.\n");
    add_item(({"boksy"}),"@@boksy@@");
    add_item(({"jask^o^lki"}),"@@jaskolki@@");
    add_object(ZAKON_DRZWI + "wrota_ze_stajni");
}

public string
exits_description() 
{
    return "Wrota prowadz^a na dziedziniec.\n";  
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        str = "Z boks^ow rozmieszczonych po obu stronach sporego "+
            "pomieszczenia dochodz^a co chwila parskni^ecia lub "+
            "tupni^ecia. ";
        if(pora_roku() != MT_ZIMA)
        {
            str += "Jask^o^lki, niczym strza^ly, przelatuj^a pod "+
            "sufitem, wylatuj^a, lub wlatuj^a przez niewielkie, wysoko "+
            "umieszczone okienka, z kt^orych s^acz^a si^e promienie "+
            "^swiat^la z trudem przedzieraj^ace si^e przez unosz^acy "+
            "si^e w powietrzu kurz i py^l. ";
        }
        else
        {
            str += "Wysoko umieszczone okienka wpuszczaj^a senne "+
            "promienie ^swiat^la z trudem przedzieraj^ace si^e przez "+
            "unosz^ac^a si^e znad boks^ow par^e. ";
        }
        str += "Zapachem siana, ko^nskiego "+
           "potu i oliwy przesi^akni^ety jest tutaj niemal ka^zdy "+
            "przedmiot. W cz^e^sci pomieszczenia znajduj^acej si^e "+
            "naprzeciw wr^ot z^lo^zony zosta^l osprz^et je^xdziecki.\n";
    }
    else
    {
        str = "Z ciemnych boks^ow rozmieszczonych po obu stronach sporego "+
            "pomieszczenia dochodz^a czasami pojedyncze parskni^ecia lub "+
            "tupni^ecia. W przesyconym kurzem powietrzu unosi si^e zapach "+
            "siana, ko^nskiego potu i oliwy. W najciemniejsze cz^e^sci "+
            "pomieszczenia znajduj^acej si^e naprzeciw wr^ot pi^etrz^a "+
            "si^e jakie^s mroczne kszta^lty.\n";
    }
    return str;
}
string
boksy()
{
    string str;
    if(jest_dzien() == 1)
    {
        str = "Wysokie, d^ebowe przegrody oddzielaj^a zwierz^eta od siebie, "+
            "a okute bramki nie pozwalaj^a im si^e wydosta^c. ^Z^loby s^a "+
            "pe^lne siana, a same zwierz^eta co do jednego wyczyszczone i "+
            "najwyra^xniej dobrze utrzymane. S^a to w wi^ekszo^sci du^ze "+
            "kawaleryjskie rumaki, ale znajdzie si^e te^z kilka lekkich "+
            "k^lusak^ow.\n";
    }
    else
    {
        str = "Wysokie, d^ebowe przegrody oddzielaj^a zwierz^eta od siebie, "+
            "a okute bramki nie pozwalaj^a im si^e wydosta^c. Zwierz^eta "+
            "stanowi^a tylko mroczne kszta^lty, od czasu do czasu "+
            "parskaj^ace, lub tupi^ace kopytami.\n";
    }
    return str;
}
string
jaskolki()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() == MT_ZIMA)
        {
            str = "Kilkana^scie gniazd poprzylepianych jest przy belkach. "+
                "S^a teraz puste i ciche.\n";
        }
        else
        {
            str = "Ca^le mn^ostwo zwinnych, czarnych ptak^ow bez przerwy "+
                "mija si^e w szalonych akrobacjach pod sufitem. Ich "+
                "szczebiotanie wype^lnia ca^le pomieszczenie. Kilkana^scie "+
                "gniazd poprzylepianych jest przy belkach. Wychylaj^a si^e "+
                "z nich r^o^zowe dzioby do kt^orych co chwila podlatuj^a "+
                "doros^le ptaki.\n";
        }
    }
    else
    {
        str = "Nie zauwa^zasz niczego takiego.\n";
    }
    return str;
}