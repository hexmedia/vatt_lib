/* Autor: Avard
   Data : 23.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ZAKON_STD;

void create_zakon() 
{
    set_short("Ku^xnia");
    add_object(ZAKON_DRZWI + "drzwi_z_kuzni");
    add_npc(ZAKON_NPC + "kowal");

    set_event_time(500.0);
    add_event("Kropla potu sp^lywa po twym czole.\n");
    add_event("K^l^eby pary unosz^a sie z nad korytka z woda.\n");
    add_event("Od strony paleniska s^lycha^c ciche syki w^egla.\n");

    add_item(({"kowad^lo","kowad^la"}),"Ustawione w rz^edzie pod ^sciana "+
        "od najmniejszego do najwi^ekszego, nosz^a liczne ^slady u^zywania. "+
        "Kowalowi, b^ed^acemu ich w^la^scicielem na pewno zale^zy na "+
        "porz^adku, co r^ownie^z mo^ze ^swiadczy^c, i^z jako^s^c "+
        "wykonywanych przez niego zbroi i broni jest bardzo wysoka. \n");

    add_item("narz^edzia","Pozawieszane na wieszakach m^lotki, rylce, "+
        "pilniki, kleszcze s^a uporz^adkowane i czyste.\n");
    
    add_item(({"palenisko","piec"}),"Kamienne palenisko stoi w k^acie "+
        "pomieszczenia, wida^c w nim  gor^ace do czerwono^sci w^egle, "+
        "kt^ore sycz^a co chwila i pryskaj^a iskrami. ^Zar bij^acy od "+
        "niego jest strasznie uci^a^zliwy i nie wydaje ci si^e by^s "+
        "m^og^l sp^edzi^c tu d^lu^zszy okres czasu.\n");
}

public string
exits_description() 
{
    return "Drzwi wyj^sciowe prowadz^a na dziedziniec.\n";  
}

string
dlugi_opis()
{
    string str;
    str = "Znajdujesz si^e w obszernym, zadymionym pomieszczeniu, kt^ore "+
        "nale^zy do zakonnego kowala. Wszystkie narz^edzia pouk^ladane na "+
        "swoich miejscach, oraz brak kurzu ^swiadcz^a o schludno^sci "+
        "pracuj^acego tu rzemie^slnika. Du^ze palenisko ustawione w k^acie "+
        "pomieszczenia powoduje, i^z jest tu niebywale gor^aco i co chwila "+
        "kropla potu sp^lywa z twego czo^la. Kowad^la ustawione pod jedn^a "+
        "ze ^scian maj^a na sobie liczne ^slady u^zywania, obok nich stoj^a "+
        "stalowe pr^ety, kt^ore w przysz^lo^sci zostan^a wykorzystane do "+
        "wyrobu r^o^znorakich broni i zbroi.\n";
    return str;
}

