/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include <object_types.h>
#include "dir.h"

#define SUBLOC_SCIANA ({"^sciana","^sciany","^scianie","^scian^e","^scian^a","^scianie","na"})
inherit ZAKON_STD;

void create_zakon() 
{
    set_short("Kwatera kapitana");
    add_object(ZAKON_PRZEDMIOTY + "szafka");
    add_object(ZAKON_PRZEDMIOTY + "dywan");
    add_object(ZAKON_PRZEDMIOTY + "biurko");
    add_object(ZAKON_PRZEDMIOTY + "etazerka");
    add_object(ZAKON_PRZEDMIOTY + "fotel");
    add_object(ZAKON_DRZWI + "okno_w_kwaterze");
    add_object(ZAKON_DRZWI + "drzwi_z_kwatery");
    dodaj_rzecz_niewyswietlana("du^za przeszklona szafka",1);
    dodaj_rzecz_niewyswietlana("gruby we^lniany dywan",1);
    dodaj_rzecz_niewyswietlana("masywne ciemne biurko",1);
    dodaj_rzecz_niewyswietlana("ma^la wiklinowa eta^zerka",1);
    dodaj_rzecz_niewyswietlana("du^zy ciemny fotel",1);
    add_item(({"proporzec","flag^e"}),"D^lugi na cztery ^lokcie i szeroki "+
        "na dwa, materia^lowy proporzec zawieszony jest na wprost wej^scia "+
        "do gabinetu Kapitana. Ciemnozielony w bia^la krat^e materia^l jest "+
        "bardzo gruby i mocno zszyty. Na ^srodku proporca znajduje sie herb "+
        "Zakonu Karmazynowych Ostrzy, przedstawiaj^acy olbrzymi^a rycersk^a "+
        "tarcze koloru czarnego, a na niej trzy, d^lugie, nagie miecze o "+
        "srebrnych r^ekoje^sciach i karmazynowych klingach skierowanych w "+
        "d^o^l i skrzy^zowanych po ^srodku.\n");
    add_item(({"zas^lony","zas^lonki"}),"D^lugie, si^egaj^ace prawie ziemi "+
        "zas^lony, zrobione z zielonkawego materia^lu s^a zawieszone po "+
        "bokach okien. Brak na nich jakichkolwiek zdobie^n oraz prostota "+
        "kroju i szycia wskazuje, ^ze ich w^la^scicielowi nie zale^zy na "+
        "pi^eknie lecz funkcjonalno^sci.\n");

    add_subloc(SUBLOC_SCIANA);
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_CANT_ODLOZ, 1);
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_CANT_POLOZ, 1);
    add_subloc_prop(SUBLOC_SCIANA, SUBLOC_I_MOZNA_POWIES, 1);
    add_subloc_prop(SUBLOC_SCIANA, SUBLOC_I_DLA_O, O_BRON_MIECZE);
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_MAX_RZECZY, 1);

    add_prop(ROOM_I_ALWAYS_LOAD, 1);

}

public string
exits_description() 
{
    return "Drzwi wyj^sciowe znajduj^a sie we wschodniej ^scianie.\n";  
}

string
dlugi_opis()
{
    string str;
    str = "Znajdujesz sie w prywatnej kwaterze kapitana dowodz^acego "+
        "oddzia^lem Zakonu Karmazynowych Ostrzy. Nie ka^zdy ma szanse "+
        "wej^s^c tutaj, wiec zapewne jeste^s szcz^e^sliwcem nale^z^acym "+
        "do tego nielicznego grona albo masz du^ze problemy i zosta^le^s "+
        "wezwany na powa^zn^a rozmow^e. Pok^oj - przestronny, urz^adzony "+
        "jest gustownie w kolorze ciemnej czerwieni i zieleni. ";
    if(jest_rzecz_w_sublokacji(0,"masywne ciemne biurko"))
    {
        str += "W centrum pomieszczenia stoi biurko";
        if(jest_rzecz_w_sublokacji(0,"du^zy ciemny fotel"))
        {
            str += " z dostawionym do^n sk^orzanym fotelem";
        }
        if(jest_rzecz_w_sublokacji(0,"du^za przeszklona szafka"))
        {
            str += ", przy p^o^lnocnej ^scianie stoi szafka";
        }
        str += ", za^s na wprost wej^scia, na ^scianie zawieszono proporzec "+
            "Zakonu Karmazynowych Ostrzy";
        if(jest_rzecz_w_sublokacji(0,"mala wiklinowa eta^zerka"))
        {
            str += ", pod kt^orym znajduje si^e ma^la wiklinowa eta^zerka";
        }
        str += ". ";
    }
    else
    {
        if(jest_rzecz_w_sublokacji(0,"du^zy ciemny fotel"))
        {
            str += "W centrum pomieszczenia stoi fotel";
            if(jest_rzecz_w_sublokacji(0,"du^za przeszklona szafka"))
            {
                str += ", przy p^o^lnocnej ^scianie stoi szafka";
            }
            str += ", za^s na wprost wej^scia, na ^scianie zawieszono "+
                "proporzec Zakonu Karmazynowych Ostrzy";
            if(jest_rzecz_w_sublokacji(0,"mala wiklinowa eta^zerka"))
            {
                str += ", pod kt^orym znajduje si^e ma^la wiklinowa "+
                    "eta^zerka";
            }
            str += ". ";
        }
        else
        {
            if(jest_rzecz_w_sublokacji(0,"du^za przeszklona szafka"))
            {
                str += "Przy p^o^lnocnej ^scianie stoi szafka, za^s na "+
                    "wprost wej^scia, na ^scianie zawieszono proporzec "+
                    "Zakonu Karmazynowych Ostrzy";
                if(jest_rzecz_w_sublokacji(0,"mala wiklinowa eta^zerka"))
                {
                    str += ", pod kt^orym znajduje si^e ma^la wiklinowa "+
                        "eta^zerka";
                }
                str += ". ";
            }
            else
            {
                str += "Na wprost wej^scia, na ^scianie zawieszono "+
                    "proporzec Zakonu Karmazynowych Ostrzy";
                if(jest_rzecz_w_sublokacji(0,"mala wiklinowa eta^zerka"))
                {
                    str += ", pod kt^orym znajduje si^e ma^la wiklinowa "+
                        "eta^zerka";
                }
                str += ". ";
            }
        }
    }
    if(subloc_items("^sciana") == "karmazynowy d^lugi miecz")
    {
        str += "Zaraz obok chor^agwi powieszony jest d^lugi karmazynowy "+
            "miecz. ";
    }
    if(jest_rzecz_w_sublokacji(0,"gruby we^lniany dywan"))
    {
        str += "Drewnian^a pod^log^e przykrywa ciemnoczerwony dywan. ";
    }
    str += "^Swiat^lo do pokoju dostarczaj^a ods^loni^ete okna oraz "+
        "zawieszone na ^scianach lampy.\n";
    return str;
}
