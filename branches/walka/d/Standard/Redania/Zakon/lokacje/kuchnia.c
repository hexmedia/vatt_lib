/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit "/lib/pub_new";
inherit ZAKON_STD;

void create_zakon() 
{
    set_short("Kuchnia");

    add_exit(ZAKON_LOKACJE + "lok6.c","e",0,1,0);
    add_object(ZAKON_PRZEDMIOTY + "lampa_srodek");
    add_item(({"piec"}),"Kamienny piec z ^zeliwn^a p^lyta "+
        "grzewcz^a jest bardzo du^zych rozmiar^ow, zapewne by "+
        "mo^zna szybko i smacznie nape^lni^c ^zo^l^adki ^zo^lnierzy "+
        "przychodz^acych do tej kuchni.\n");

    add_item(({"palenisko"}),"Jest to bardzo du^zych rozmiar^ow kominek na "+
        "kt^orym piecze sie kawa^l mi^esa.\n");

    add_object(ZAKON_PRZEDMIOTY + "garnki");
    add_object(ZAKON_PRZEDMIOTY + "stol_kuchnia1");
    add_object(ZAKON_PRZEDMIOTY + "stol_kuchnia2");
    add_object(ZAKON_PRZEDMIOTY + "lawa_kuchnia",4);
    add_object(ZAKON_PRZEDMIOTY + "menu");
    dodaj_rzecz_niewyswietlana("d^lugi d^ebowy st^o^l");
    dodaj_rzecz_niewyswietlana("d^lugi szeroki st^o^l");
    dodaj_rzecz_niewyswietlana("wytarta solidna ^lawa",4);

    
    add_npc(ZAKON_NPC + "kucharka");
    add_npc(ZAKON_NPC + "kuchcik");

    add_drink("woda",0,"czystej wody",600,0,8,"kubek","wody",
        "Du^za, czysta szklanka.");
    add_drink("kompot", ({"jab^lkowy", "jab^lkowi"}), "kompotu jab^lkowego",
         400, 0, 9, "kubek", "kompotu", "Ma^la, czysta szklanka.");
    add_drink("piwo", ({ "jasny", "ja�ni"}), "jasno-bursztynowego, " +
    "wspaniale pieni�cego si^e piwa", 700, 4, 11, "kufel", "piwa",
    "Jest to kufel wykonany z krystalicznego szk^la, kt^orego uchwyt " +
    "wykonano z dobrego d^ebowego drewna.",10000);
    add_drink("piwo", ({ "ciemny", "ciemni"}), "ciemnego, " +
    "wspaniale pieni�cego si^e piwa", 700, 6, 12, "kufel", "piwa",
    "Jest to kufel wykonany z krystalicznego szk^la, kt^orego uchwyt " +
    "wykonano z dobrego d^ebowego drewna.",10000);

    add_food(({"gulasz","gulasz z zajaca"}), 0, "gulaszu z zaj^aca", 
        450,45,13,"talerz","gulaszu","Jest to prosty, czysty talerz.", 200);
    add_food(({"grochowka","grochowka z boczkiem"}),0,
        "groch^owki z boczkiem",450,45,12,"talerz","groch^owki",
        "Jest to prosty, czysty talerz.", 200);
    add_food(({"rosol","rosol z kury"}),0,"roso^lu z kury",500,45,15,
    "talerz", "roso^lu", "Jest to g^l^eboki, czysty talerz.", 200);
    add_food(({"baranina","baranina z ziemniakami"}),0,
        "baraniny z ziemniakami",1000,46,20,"talerz", "baraniny", 
        "Jest to du^zy, czysty talerz.", 200);
    add_food("mieso",({"wo�owy","wo�owi"}),"mi^eso wo^lowe",600,45,20,
        "talerz","mi^esa","Jest to du^zy, czysty talerz.",200);
    add_food(({"pierogi","pierogi ze skwarkami"}),0,"pierogi ze skwarkami",
        450,45,20,"talerz","pierog^ow","Jest to prosty, czysty talerz.",200);
    add_food(({"kluski","kluski ze skwarkami"}),0,"kluski ze skwarkami",450,
        45,18,"talerz","klusk^ow","Jest to prosty, czysty talerz.",200);
}
void
init()
{
    ::init();
    init_pub();
}

public string
exits_description() 
{
    return "Na wschodzie znajduje si^e jaka^s sala.\n";  
}

string
dlugi_opis()
{
    string str;
    str = "Parne pomieszczenie wype^lnia zapach gotowanego i pieczonego "+
        "mi^esiwa, zi^o^l i kapusty. Ogromny piec zajmuj^acy niemal "+
        "ca^l^a ^scian^e naprzeciw szerokich drzwi zastawiony jest "+
        "wszelkiej ma^sci rondlami i garami, podobnie zreszt^a jak kilka "+
        "sto^l^ow ustawionych pod ^scianami. Na tych dostrzec mo^zna te^z "+
        "sterty warzyw, kilka mis smalcu";
    if(jest_rzecz_w_sublokacji(0,"n^o^z"))
    {
        str += " i no^ze";
    }
    str += ". W rogu znajduje si^e tak^ze niewielkie palenisko z piek^ac^a "+
        "si^e na nim p^o^ltuszk^a. Unosz^aca si^e w powietrzu para i gor^ac "+
        "bij^acy od pieca i paleniska powoduj^a, ^ze ju^z po chwili czujesz "+
        "sp^lywaj^ace ci po plecach krople potu. ";
    if(jest_rzecz_w_sublokacji(0,"d^lugi d^ebowy st^o^l") || 
        jest_rzecz_w_sublokacji(0,"d^lugi szeroki st^o^l"))
    {
        if(jest_rzecz_w_sublokacji(0,"d^lugi d^ebowy st^o^l") && 
            jest_rzecz_w_sublokacji(0,"d^lugi szeroki st^o^l"))
        {
            str += "Na ^srodku pomieszczenia stoj^a dwa d^lugie "+
                "sto^ly nie u^zyte do prac w kuchni.";
        }
        else
        {
            str += "Na ^srodku pomieszczenia stoi d^lugi st^o^l "+
                "nie u^zyty do prac w kuchni.";
        }
    }
    str += "\n";
    return str;
}

