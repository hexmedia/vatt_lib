
/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
    dodaj_przym("gruby","grubi");
    dodaj_przym("solidny", "solidni");
    
    set_other_room(ZAKON_LOKACJE + "zbrojownia.c");
    set_door_id("DRZWI_DO_ZBROJOWNI_ZAKONU_KARMAZYNOWYCH_OSTRZY");
    set_door_desc("Masywne drewniane drzwi z miedzianymi okuciami, pokryte "+
        "warstewk^a pszczelego wosku zamykaj^a dost^ep niepowo^lanym osobom "+
        "do zbrojowni Zakonu. Drube deski dodatkowo zmocnione stanowi^a "+
        "doskona^le zabezpieczenie, a zamek w drzwiach umo^zliwia "+
        "zamkni^ecie pomieszczenia.\n");

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({({"drzwi","zbrojownia"}),"przez grube solidne drzwi",
        "wychodz^ac ze zbrojowni"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");
   
    set_open(0);
    set_locked(1);

    set_key("KLUCZ_DRZWI_DO_ZBROJOWNI_ZAKONU_KARMAZYNOWYCH_OSTRZY");
    set_lock_name("zamek");
}