
/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
    dodaj_przym("drewniany","drewniani");
    dodaj_przym("okuty", "okuci");
    
    set_other_room(ZAKON_LOKACJE + "kuznia.c");
    set_door_id("DRZWI_DO_KUZNI_ZAKONU_KARMAZYNOWYCH_OSTRZY");
    set_door_desc("Zbite zosta^ly z drewnianych desek oheblowanych i "+
        "po^l^aczonych ze sob^a dwoma mocnymi poprzecznymi ^latami. Z "+
        "boku drzwi mo^zna zauwa^zy^c uchwyt s^lu^z^acy do otwierania i "+
        "zamykania.\n");

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({({"ku^xnia","drzwi","wsch^od"}),
        "przez drewniane okute drzwi","z zewn^atrz"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_key("KLUCZ_DRZWI_DO_KUZNI_ZAKONU_KARMAZYNOWYCH_OSTRZY");
    set_lock_name("zamek");
}