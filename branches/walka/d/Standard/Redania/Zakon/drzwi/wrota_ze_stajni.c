
/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("wrota"); 
    dodaj_przym("drewniany","drewniani");
    dodaj_przym("prosty", "pro^sci");
    
    set_other_room(ZAKON_LOKACJE + "lok1.c");
    set_door_id("DRZWI_DO_STAJNI_ZAKONU_KARMAZYNOWYCH_OSTRZY");
    set_door_desc("Drewniane wrota zbite z prostych desek, wysokie na 5 "+
        "^lokci zamykaj^a stajnie przed nieproszonymi go^s^cmi.\n");

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({({"wyj^scie","drzwi","dziedziniec"}),
        "przez drewniane proste wrota",
        "przychodz^ac z zewn^atrz"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_open(0);
    set_locked(1);

    set_key("KLUCZ_DRZWI_DO_STAJNI_ZAKONU_KARMAZYNOWYCH_OSTRZY");
    set_lock_name("zamek");
}