
/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
    dodaj_przym("drewniany","drewniani");
    dodaj_przym("solidny", "solidni");
    
    set_other_room(ZAKON_LOKACJE + "alchemik.c");
    set_door_id("DRZWI_DO_ALCHEMIKA_ZAKONU_KARMAZYNOWYCH_OSTRZY");
    set_door_desc("Drewniane zbite z desek, po^l^aczone poprzecznymi "+
        "^latami, wygl^adaj^a solidnie i estetycznie. Masywna budowa "+
        "zapewnia odpowiedni^a ochron^e pomieszczenia.\n");

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({({"drzwi","alchemik","medyk","zielarz",
        "konowa�"}),"przez drewniane solidne drzwi",
        "ze wschodu"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_open(0);
    set_locked(1);

    set_key("KLUCZ_DRZWI_DO_ALCHEMIKA_ZAKONU_KARMAZYNOWYCH_OSTRZY");
    set_lock_name("zamek");
}