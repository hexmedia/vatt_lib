/* Autor: Avard
   Opis : Sniegulak
   Data : 24.04.07 */

inherit "/std/gate.c";
#include <stdproperties.h>
#include <macros.h>
#include "dir.h"

void
create_gate()
{
    set_other_room(TRAKT_ZAKON_OKOLICE_LOKACJE + "s_zakonu04.c");
    set_long("Pot^e^zna, wysoka na dziesi^e^c ^lokci brama, zbudowana jest "+
        "z drewnianych bali, kt^ore przylegaj^a do siebie tak szczelnie, "+
        "i^z nie jeste^s w stanie zauwa^zy^c co si^e za ni^a znajduje. "+
        "Powierzchnia drewna jest g^ladka i zaimpregnowana, ale "+
        "wzmacniaj^ace bram^e, przynitowane stalowe okucia s^a "+
        "zardzewia^le. Wrota otwierane s^a na pot^e^znych zawiasach, "+
        "kt^ore przymocowane zosta^ly z lewej strony do palisady.\n");
    set_open(1);
    set_locked(0);
    add_prop(GATE_IS_INSIDE,1);
    set_gate_id("ZAKON_KARMAZYNOWYCH_BRAMA");
    set_pass_command(({"po^ludnie","s"}));
    set_security_lvl(0);
    godzina_zamkniecia(21);
    godzina_otwarcia(5);
    set_skad("");
    dodaj_przym("wysoki","wysocy");
}

string
standard_open_mess2()
{
    return "Do "+short(PL_DOP) +" po chwili podchodzi stra^znik odsuwa "+
        "cie^zka okut^a belke zamykajaca wrota i uchyla jedno skrzyd^lo.\n";
}
