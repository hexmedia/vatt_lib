/* Autor: Avard
   Opis : Sniegulak
   Data : 28.04.07 */
inherit "/std/window";
inherit "/lib/peek";

#include <pl.h>
#include <macros.h>
#include "dir.h"
//int wyskocz_przez_okno();
void
create_window()
{
    ustaw_nazwe("okno"); 
    dodaj_przym("du^zy", "duzi");   
                                 
    set_other_room(ZAKON_LOKACJE + "plac");
    add_peek("przez okno", ZAKON_LOKACJE + "plac");   
    set_window_id("OKNO_W_ZBROJOWNI_ZAKONU_KARMAZYNOWYCH_OSTRZY_OBOK_RINDE");
    set_open(0);
    set_open_desc("");
    set_closed_desc("");
    set_window_desc("Du^za drewniana rama, z dwoma skrzyd^lami, "+
        "wy^lo^zonymi p^lytkami szklanymi pozwala s^lonecznemu "+
        "^swiat^lu na dotarcie do tego pomieszczenia. \n");
    this_player()->set_hp(this_player()->query_hp()-90-random(50));

    set_pass_command("okno");
    set_pass_mess("przez niedu^ze okno na zewn^atrz");
}
/*void init()
{
    ::init();
    init_peek();
    add_action("wyskocz_przez_okno","wyskocz");
}


int
wyskocz_przez_okno()
{
   
   object *okna;
   
   okna = filter(all_inventory(environment( this_player() ) ),
             &operator(!=)(0,) @ &->query_window_desc());
   okna = filter(okna, &operator(!=)(0,) @ &->query_open());
   
   notify_fail("Przez co chcesz wyskoczy^c?");
   
   if (!sizeof(okna))
   {
      notify_fail("Otw^orz najpierw okno.\n");
      return 0;
   }
   
   write("Podchodzisz do okna i wyskakujesz przez nie.\n");
   say(QCIMIE(this_player(), PL_MIA) + " wyskakuje przez okno!\n");
   tell_room(ZAKON_LOKACJE + "plac", 
          QCIMIE(this_player(), PL_MIA) + " spad^l"+
	  this_player()->koncowka("", "a", "o") +" w^la^snie na ziemi^e!\n");
      this_player()->set_hp(this_player()->query_hp()-100-random(50));
   this_player()->move_living("M",ZAKON_LOKACJE + "plac",1,0);
	     
   return 1;
}
*/