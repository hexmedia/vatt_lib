inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(ZAKON_ZBROJE + "buty/mocne_wysokie_Lc");
    set_estimated_price(200);
    set_time_to_complete(4000);
    enable_want_sizes();
}
