inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(ZAKON_ZBROJE + "naramienniki/czernione_lekkie_Lc.c");
    set_estimated_price(400);
    set_time_to_complete(8800);
    enable_want_sizes();
}
