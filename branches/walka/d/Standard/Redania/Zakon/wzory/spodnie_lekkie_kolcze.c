inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(ZAKON_ZBROJE + "spodnie/lekkie_kolcze_Lc.c");
    set_estimated_price(6000);
    set_time_to_complete(8800);
    enable_want_sizes();
}
