/*
 *Brzeg pod Bia�ym Mostem (w Bia�y_Most_okolice)
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit BRZEG_BIALY_MOST_STD;

void create_brzeg() 
{
    set_short("Przy brzegu");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t01.c","n",0,9,0);
    add_exit(BRZEG_BIALY_MOST_LOKACJE + "b03.c","sw",0,9,0);
	//add_exit(TRAKT_BIALY_MOST_LOKACJE + "s01.c","nw",0,FATIGUE_SCIEZKA,0);
	add_item(({"most","filar","filary"}),"@@filary@@");
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Brzeg ci�gnie si� na po�udniowym-zachodzie, za� na p�nocy "+
	"znajduje si� trakt.\n";
}


string
dlugasny()
{
	string str="";

	if(jest_dzien())
	str+="Znajdujesz si� po�r�d niskich, zapuszczonych chat "+
	"podgrodzia. Widoczny ponad nimi most szczyci si� smuk�ymi, "+
	"bia�ymi filarami podtrzymuj�cymi lekk� konstrukcje, zwie�czon� "+
	"ledwie z t�d widocznymi barierkami. Na po�udniowym zachodzie "+
	"rozci�ga si� nabrze�e nad kt�rym g�ruj� mury miejskie, za� na "+
	"p�nocy zaczyna si� strome podej�cie wiod�ce pod bram� miejsk�. "+
	"Gdy odwr�cisz si� w przeciwn� stron� widzisz wolno p�yn�c�, "+
	"szerok� rzek� nios�c� sporo mu�u. Mo�na te� okazjonalnie "+
	"dostrzec ga��zie, czy nawet pnie.";
	else
	str+="Z nielicznych okien nadbrze�nych chat s�czy si� md�e "+
	"�wiat�o, pozwalaj�ce zorientowa� si� na tyle by nie wpa�� "+
	"do rzeki. Na tle nieba wznosz� si� mroczne kszta�ty mostu.";


	str+="\n";
	return str;
}
