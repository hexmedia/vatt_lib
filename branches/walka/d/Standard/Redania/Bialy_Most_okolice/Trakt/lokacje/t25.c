/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 16th of June 2007 06:45:41 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t24.c","e",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t26.c","n",0,FATIGUE_TRAKT,0);
    add_exit(BIALY_MOST_LOKACJE + "ulice/u10.c",
        ({"s","do miasta","z traktu"}),0,8,0);

    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na wsch^od i p^o^lnoc i po^ludnie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Ca^la okolica usiana jest du^zymi kwadratami p^ol uprawnych "+
             "oraz ^l^ak, wykorzystywanych przez okolicznych mieszka^nc^ow"+
             ". ^L^aki i pola uprawne, pokryte srebrzystobia^lym puchem, w"+
             "ygl^adaj^a na opuszczone przez prawie wszelak^a zwierzyn^e. "+
             "Wok^o^l jak okiem si^egn^a^c rozpo^sciera sie pofa^ldowany ^"+
             "lagodnymi wzg^orzami krajobraz. Trakt wij^acy si^e mi^edzy m"+
             "a^lymi pag^orkami, przecina w tym miejscu pola uprawne przyk"+
             "ryte grub^a pokryw^a ^snie^zna, na kt^orych gdzieniegdzie mo"+
             "^zna zauwa^zy^c ^slady drobnej zwierzyny. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Ca^la okolica usiana jest du^zymi kwadratami p^ol uprawnych "+
             "oraz ^l^ak, wykorzystywanych przez okolicznych mieszka^nc^ow"+
             ". ^L^aki i pola uprawne, pokryte kie^lkuj^aca ro^slinno^sci^"+
             "a, zaczynaj^a zape^lnia^c si^e budz^ac^a si^e do ^zycia faun"+
             "^a. Wok^o^l jak okiem si^egn^a^c rozpo^sciera sie pofa^ldowa"+
             "ny ^lagodnymi wzg^orzami krajobraz. Trakt wij^acy si^e mi^ed"+
             "zy ma^lymi pag^orkami, przecina w tym miejscu lekko ju^z zaz"+
             "ielenione pola uprawne, dostarczaj^ace okolicznym mieszka^nc"+
             "om po^zywienia. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Ca^la okolica usiana jest du^zymi kwadratami p^ol uprawnych "+
             "oraz ^l^ak, wykorzystywanych przez okolicznych mieszka^nc^ow"+
             ". ^L^aki i pola uprawne, pokryte bujnymi plonami, s^a wype^l"+
             "nione d^xwi^ekami przer^o^znych zwierz^at. Wok^o^l jak okiem"+
             " si^egn^a^c rozpo^sciera sie pofa^ldowany ^lagodnymi wzg^orz"+
             "ami krajobraz. Trakt wij^acy si^e mi^edzy ma^lymi pag^orkami"+
             ", przecina w tym miejscu g^esto obsiane pola uprawne, dostar"+
             "czaj^ace okolicznym mieszka^ncom po^zywienia. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Ca^la okolica usiana jest du^zymi kwadratami p^ol uprawnych "+
             "oraz ^l^ak, wykorzystywanych przez okolicznych mieszka^nc^ow"+
             ". ^L^aki i pola uprawne, ogo^locone z ro^slinno^sci, powoli "+
             "cichn^a wraz ze zwierz^etami k^lad^acymi si^e do zimowego sn"+
             "u. Wok^o^l jak okiem si^egn^a^c rozpo^sciera sie pofa^ldowan"+
             "y ^lagodnymi wzg^orzami krajobraz. Trakt wij^acy si^e mi^edz"+
             "y ma^lymi pag^orkami, przecina w tym miejscu przygotowane ju"+
             "^z do zimy pola uprawne, dostarczaj^ace okolicznym mieszka^n"+
             "com po^zywienia. ";
    }

    str+="\n";
    return str;
}