
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

void create_trakt() 
{
    set_short("Trakt nad brzegiem Pontaru");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t17.c","e",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t15.c","sw",0,FATIGUE_TRAKT,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi na po�udniowy-zach�d i na wsch�d.\n";
}

string
dlugasny()
{
	string str="";

	str+="Trakt wij�cy si� nad brzegiem Pontaru jest na tyle szeroki, "+
	"i� bez najmniejszego trudu mog� wymin�� si� na nim wy�adowane "+
	"towarami wozy. ";

	if(CZY_JEST_SNIEG(TO))
		str+="Powierzchni� traktu pokrywa gruba i �liska warstwa lodu, a"+
		"pobocze drogi porastaja liczne zaro�la lekko przypruszone "+
		"bia�ym puchem. W oddali na p�nocy dostrzegasz ko�ysz�ce si� "+
		"na wietrze samotnie stoj�ce drzewo b�d�ce jedynym urozmaiceniem "+
		"tego r�wninnego terenu. ";
	else
		str+"Szeroka droga nosi na sobie liczne �lady cz�stego "+
		"u�ytkowania, wiele fa�d i do�k�w wy��obionych przez "+
		"rozp�dzone wozy kupieckie, �wiadcz� i� od dawna trakt ten nie "+
		"przechodzi� �adnego remontu nawierzchni. "+
		"Pobocze drogi porastaj� liczne zaro�la, kt�re nieustannie "+
		"staraj� si� zapu�ci� swoje p�dy na szerok� scie�ke, a tym "+
		"samym utrudni� podr�nym w�dr�wk�. ";


	if((pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO) && jest_dzien())
	{	str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
			"zieleniej� delikatne li�cie drzew. ";
	}
	else if(pora_roku() == MT_JESIEN && jest_dzien())
		str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
		"czerwieni� si� i z�oc� jesienne szczyty drzew. ";
	else if(pora_roku() == MT_ZIMA)
	{
		if(CZY_JEST_SNIEG(TO))
			str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
			"stercz� pokryte szronem ga��zie drzew ";
		else
			str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
			"stercz� ogo�ocone z li�ci ga��zie drzew. ";
	
	}
	else if(!jest_dzien())
		str+="Ponad szczytem le��cego na wschodzie wzg�rza majacz� "+
		"na tle rozgwie�d�onego nieba czarne ga��zie drzew. ";


	if(dzien_noc())
	{
		if(ZACHMURZENIE(TO) <= POG_ZACH_SREDNIE)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej l�ni�ce w blasku ksi�yca pola uprawne od ciemnej "+
			"zieleni lasu pokrywaj�cej obszary na wschodzie. ";
		else
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej ciemne pola uprawne od lasu pokrywaj�cego obszary na "+
			"wschodzie. ";
	}
	else
	{
		if(pora_roku() == MT_ZIMA)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej poro�ni�te jasnozielon� ozimin� od ciemnej zieleni "+
			"lasu pokrywaj�cej obszary na wschodzie. ";
		else if(pora_roku() == MT_WIOSNA)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej �wie�o zaorane pola uprawne od ciemnej zieleni "+
			"lasu pokrywaj�cej obszary na wschodzie. ";
		else if(pora_roku() == MT_LATO)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej faluj�ce z�ocistym �anem dojrza�ego zbo�a od ciemnej zieleni lasu pokrywaj�cej obszary na wschodzie. ";
		else
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej ciemne pola uprawne od lasu pokrywaj�cego obszary na "+
			"wschodzie. ";
	}




	str+="\n";
	return str;
}
