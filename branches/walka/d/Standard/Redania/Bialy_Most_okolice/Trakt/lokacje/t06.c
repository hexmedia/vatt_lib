
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

void create_trakt() 
{
    set_short("Trakt po�r�d ��k");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t05.c","sw",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t07.c","se",0,FATIGUE_TRAKT,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt ci�gnie si� na po�udniowy-zach�d oraz na po�udniowy-wsch�d.\n";
}
string
dlugasny()
{
	string str="";


	str+="Trakt wij�cy si� nad brzegiem Pontaru jest na tyle szeroki, "+
	"i� bez najmniejszego trudu mog� wymin�� si� na nim wy�adowane "+
	"towarami wozy. ";
	
	if(CZY_JEST_WODA(TO))
		str+="Wy�o�on� dopasowanymi kamieniami powierzchni� traktu "+
		"pokrywa gruba warstwa brunatnego b�ocka. ";
	else
		str+="Wy�o�on� dopasowanymi kamieniami powierzchni� traktu "+
		"pokrywa gruba warstwa gruba warstwa zaskorupia�ego b�ocka. ";



	if(MOC_WIATRU(TO) > POG_WI_SREDNIE)
	{
		if(pora_roku() == MT_WIOSNA)
			str+="Tocz�ce si� z w�ciek�ym rykiem wody Pontaru cz�ciowo "+
			"zas�aniaj� rosn�ce po po�udniowej stronie traktu niewysokie "+
			"spl�tane, zaro�la pokryte delikatnymi bia�ymi kwiatami. ";
		else if(pora_roku() == MT_LATO)
			str+="Tocz�ce si� z w�ciek�ym rykiem wody Pontaru cz�ciowo "+
			"zas�aniaj� rosn�ce po po�udniowej stronie traktu niewysokie "+
			"spl�tane, zaro�la pokryte jakimi� owocami. ";
		else
			str+="Tocz�ce si� z w�ciek�ym rykiem wody Pontaru cz�ciowo "+
			"zas�aniaj� rosn�ce po po�udniowej stronie traktu niewysokie "+
			"spl�tane, zaro�la pokryte mgie�k� zielonych li�ci. ";
	}
	else
	{
		if(pora_roku() == MT_WIOSNA)
			str+="Leniwie p�yn�ce wody Pontaru cz�ciowo zas�aniaj� rosn�ce "+
			"po po�udniowej stronie traktu niewysokie spl�tane, zaro�la "+
			"pokryte delikatnymi bia�ymi kwiatami. ";
		else if(pora_roku() == MT_LATO)
			str+="Leniwie p�yn�ce wody Pontaru cz�ciowo zas�aniaj� rosn�ce "+
			"po po�udniowej stronie traktu niewysokie spl�tane, zaro�la "+
			"pokryte jakimi� owocami. ";
		else
			str+="Leniwie p�yn�ce wody Pontaru cz�ciowo zas�aniaj� rosn�ce "+
			"po po�udniowej stronie traktu niewysokie spl�tane, zaro�la "+
			"pokryte mgie�k� zielonych li�ci. ";
	}







	str+="\n";
	return str;
}
