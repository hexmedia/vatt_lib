/*
 *�cie�ka mi�dzy Bia�ym Mostem a Rinde (w Bia�y_Most_okolice) prowadz�ca
 *do wioski
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;
inherit "/std/drzewa";

void create_trakt() 
{
    set_short("�cie�ka mi�dzy ��kami");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "s03.c","se",0,FATIGUE_SCIEZKA,0);
	add_exit(TRAKT_BIALY_MOST_LOKACJE + "s01.c","nw",0,FATIGUE_SCIEZKA,0);
	add_sit("na g�azie","na g�azie","z g�azu",2);
    add_prop(ROOM_I_INSIDE,0);
	add_event("Od strony lasu dochodzi ci� weso�y �wiergot jakiego� ptaka.\n");
	add_event("Od p�nocy s�ycha� jakies pokrzykiwania.\n");
	add_event("Cisz� panuj�c� wok� przerywa natarczywe ujadanie psa.\n");
	add_prop(ROOM_I_TYPE, ROOM_FOREST);
	add_subloc(({"drzewo", "drzewa", "drzewu", "drzewo",
        "drzewem", "drzewie"}));
    add_subloc_prop("drzewo", SUBLOC_I_MOZNA_POLOZ, 1);
    add_subloc_prop("drzewo", SUBLOC_I_MOZNA_ODLOZ, 1);
    add_subloc_prop("drzewo", SUBLOC_I_TYP_POD, 1);

    //ustaw_liste_drzew(({"topola"}));
    //    init_drzewa();

}
public string
exits_description() 
{
    return "�cie�ka ci�gnie si� na po�udniowym-wschodzie oraz na "+
		"p�nocnym-zachodzie.\n";
}


string
dlugasny()
{
	string str="";

	if(!CZY_JEST_SNIEG(TO))
	{
		str+="Niezbyt szeroka �cie�ka biegnie prosto, mi�dzy barwnymi polami "+
		"i ��kami. Droga pokryta jest niezliczon� ilo�ci� wyrw i wg��bie�, "+
		"szlifowanych ko�ami woz�w. Na wschodzie zieleni si� ciemna �ciana "+
		"lasu, od kt�rej dochodzi �wiergot ptak�w.";
		if(jest_rzecz_w_sublokacji("topola",0))
			str+=" Tu� przy dr�ce ro�nie "+
			"wysoka i smuk�a topola, rzucaj�ca na ziemi� spory cie�.";

		str+=" Obok le�y "+
		"g�az, na kt�rym od biedy mo�na usi��� i da� ul�y� zbola�ym od "+
		"w�dr�wki stopom.";
	}
	else
	{
	str+="Niezbyt szeroka �cie�ka biegnie prosto mi�dzy polami i ��kami "+
		"przysypanymi bia�� pierzyn�. Droga pokryta jest niezliczon� ilo�ci� "+
		"wyrw i wg��bie�, szlifowanych ko�ami woz�w. Teraz, zim�, �nieg "+
		"powype�nia� cz�� dziur, dzi�ki czemu poruszanie si� po dr�ce "+
		"jest nieco bardziej komfortowe. Na wschodzie czerni si� ponura "+
		"�ciana lasu. ";
		if(jest_rzecz_w_sublokacji("topola",0))
		str+="Za� tu� przy drodze ro�nie wysoka i smuk�a topola, "+
		"a obok niej le�y g�az, na kt�rym od biedy mo�na spocz�� i da� "+
		"ul�y� zbola�ym od w�dr�wki nogom.";
		else
		str+="Obok niej le�y g�az, na kt�rym od biedy mo�na spocz�� i da� "+
		"ul�y� zbola�ym od w�dr�wki nogom.";
	}

	str+="\n";
	return str;
}

