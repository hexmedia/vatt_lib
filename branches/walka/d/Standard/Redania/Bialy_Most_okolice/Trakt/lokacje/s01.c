/*
 *�cie�ka mi�dzy Bia�ym Mostem a Rinde (w Bia�y_Most_okolice) prowadz�ca
 *do wioski
 *Opis: Vortak (kapk� przeradagowany, m.in. wywalony strumyk)
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>
inherit TRAKT_BIALY_MOST_STD;

/*DO ZABRANIA. STRUMYKA TU MA NIE BY� I JU�.
string
strum()
{
 string str = "Na p^o^lnocy " +
    "wida^c wst^a^zk^e ";

    if (TEMPERATURA(TO) < 0)
        str += "skutego lodem ";

    str += "strumienia oddzielaj^acego wiosk^e od traktu.\n";

    return str;
}*/

void
create_trakt()
{
    set_short("W�ska �cie�yna");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "s02.c","se",0,FATIGUE_SCIEZKA,0);
	add_exit(WIOSKA_BIALY_MOST_OKOLICE_LOKACJE+ "wioska3.c",
			({"ne","do wioski","z po�udnia"}),0,FATIGUE_SCIEZKA,0);
	add_event("Od strony lasu dochodzi ci� weso�y �wiergot jakiego� ptaka.\n");
	add_event("Od p�nocy s�ycha� jakies pokrzykiwania.\n");
	add_event("Cisz� panuj�c� wok� przerywa natarczywe ujadanie psa.\n");
    add_prop(ROOM_I_INSIDE,0);
	add_prop(ROOM_I_WSP_Y, 0);	//bezposrednio na W od Rinde...
    add_prop(ROOM_I_WSP_X, -3);
}
public string
exits_description()
{
    return "�cie�ka ci�gnie si� na po�udniowym-wschodzie, za� "+
			"�cie�ka na p�nocnym-wschodzie wiedzie w kierunku chat.\n";
}


string
dlugasny()
{
    string str = "Znajdujesz si^e raptem kilkadziesi^at jard^ow na p^o^lnoc od trakt" +
    "u. Od^l^acza si^e od niego w tym " +
    "miejscu ^scie^zka szeroka na tyle, ^ze bez wi^ekszych problem^ow " +
    "przejedzie ni^a kupiecki pow^oz. ";
    
    if (!CZY_JEST_SNIEG(TO))
    {
        str += "Tak^ze wyra^xnie odci^sni^ete w ";

        if (TEMPERATURA(TO) <= 0)
            str += "zmarzni^etej, ";

        str += "ubitej ziemi ^slady ko^nskich kopyt ^swiadcz^a, i^z droga " +
        "ta jest wcale nierzadko ucz^eszczana. Na p^o^lnocy wida^c ma^l^a wiosk^e";
        
        if (jest_dzien())
            str += ", z kt^orej dobiega ujadanie gospodarczych ps^ow. ";
		else
			str+=". ";
    }
    else
    {
        str += "Tak^ze wyra^xnie odci^sni^ete w g^l^ebokim ^sniegu ^slady " +
        "ko^nskich kopyt ^swiadcz^a, i^z droga ta jest wcale nierzadko " +
        "ucz^eszczana. Na p^o^lnocy wzd^lu^z drogi wida^c ma^l^a wiosk^e. ";
		if (MT_GODZINA > 6)
		str+="W^la^snie z tamtej strony" +
        " dobiega do ciebie ledwie wyczuwalny zapach dymu, kt^ory leniwie " +
        "snuje si^e nad strzechami dom^ow. ";
		
 		str += "Obok drogi stoi nieco wi^ekszy od pozosta^lych budynek, zapewne " +
    	"karczma lub ober^za. ";
    }
    
    str += "\n";
    
    return str;
}
