#define REDANIA "/d/Standard/Redania/"
#define ZIOLA "/d/Standard/ziola/"
#define UBRANIA_GLOBAL "/d/Standard/items/ubrania/"


#define RINDE               	(REDANIA + "Rinde/")
#define RINDE_NPC           	(RINDE + "npc/")

#define POLUDNIE            	(RINDE + "Poludnie/")
#define POLUDNIE_LOKACJE    	(POLUDNIE + "lokacje/")

#define TRAKT_RINDE         	(REDANIA + "Rinde_okolice/Trakt/")
#define TRAKT_RINDE_LOKACJE 	(TRAKT_RINDE + "lokacje/")
#define TRAKT_RINDE_NPC     	(TRAKT_RINDE + "npc/")

#define BIALY_MOST			(REDANIA + "Bialy_Most/")

#define BIALY_MOST_LOKACJE		(BIALY_MOST + "lokacje/")
#define BIALY_MOST_LOKACJE_ULICE	(BIALY_MOST_LOKACJE+"ulice/")

#define BIALY_MOST_PRZEDMIOTY	(BIALY_MOST+"przedmioty/")
#define BIALY_MOST_BRONIE		(BIALY_MOST_PRZEDMIOTY + "bronie/")
#define BIALY_MOST_ZBROJE		(BIALY_MOST_PRZEDMIOTY + "zbroje/")
#define BIALY_MOST_UBRANIA      	(BIALY_MOST + "przedmioty/ubrania/")
#define RINDE_UBRANIA       	(RINDE + "przedmioty/ubrania/")

#define BIALY_MOST_OKOLICE		(REDANIA + "Bialy_Most_okolice/")
#define TRAKT_BIALY_MOST_OKOLICE	(BIALY_MOST_OKOLICE+"Trakt/")
#define TRAKT_BIALY_MOST_OKOLICE_LOKACJE	(TRAKT_BIALY_MOST_OKOLICE + "lokacje/")
#define BRZEG_BIALY_MOST_OKOLICE 	(BIALY_MOST_OKOLICE+"Brzeg/")
#define BRZEG_BIALY_MOST_LOKACJE 	(BRZEG_BIALY_MOST_OKOLICE+"lokacje/")

#define WIOSKA_BIALY_MOST_OKOLICE (REDANIA+"Wioska_BM/")
#define WIOSKA_BIALY_MOST_OKOLICE_LOKACJE (WIOSKA_BIALY_MOST_OKOLICE+"lokacje/")

#define KANALY				(REDANIA + "Rinde_kanaly/")
#define KANALY_LOKACJE			(KANALY + "lokacje/")
#define KANALY_DRZWI				(KANALY + "drzwi/")
#define KANALY_OBIEKTY			(KANALY + "obiekty/")

#define ZAKON               		(REDANIA + "Zakon/")
#define ZAKON_LOKACJE			(ZAKON + "lokacje/")
#define ZAKON_DRZWI         		(ZAKON + "drzwi/")

#define ZAKON_OKOLICE 			(REDANIA+"Zakon_okolice/")
#define TRAKT_ZAKON_OKOLICE 		(ZAKON_OKOLICE+"Trakt/")
#define TRAKT_ZAKON_OKOLICE_LOKACJE 	(TRAKT_ZAKON_OKOLICE+"lokacje/")

#define TRETOGOR				(REDANIA + "Tretogor/")
#define TRETOGOR_LOKACJE			(TRETOGOR + "lokacje/")
#define TRETOGOR_DRZWI			(TRETOGOR + "drzwi/")

#define PIANA					(REDANIA + "Piana/")
#define PIANA_LOKACJE			(PIANA + "lokacje/")