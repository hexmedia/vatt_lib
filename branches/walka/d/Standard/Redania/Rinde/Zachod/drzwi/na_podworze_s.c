
/* Autor: Avard
   Data : 10.08.06
   Info : Brama na jednej z ulic Rinde */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("brama"); 
    dodaj_przym("drewniany", "drewnani");
    dodaj_przym("dwudrzwiowy","dwudrzwiowi");
    
    set_other_room(ZACHOD_LOKACJE+"ulica6");
    set_door_id("BRAMA_NA_PODWORZE_RINDE_S");
    set_door_desc("Dwudrzwiowa brama zosta^la umiejscowiona w "+
        "p^o^lnocnej ^scianie.\n");

 /* set_open_desc("Na p^o^lnocnej ^scianie znajduje si^e otwarta "+
        "brama.\n");
    set_closed_desc("Na p^o^lnocnej ^scianie znajduje si^e zamkni^eta "+
        "brama.\n"); */

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({({"brama","p^o^lnoc","p^o^lnocna brama",
        "przejdz przez bram^e","przejdz przez p^o^lnocn^a bram^e"}),
        "przez bram^e na p^o^lnoc","zza bramy z p^o^lnocy"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_key("KLUCZ_DO_BRAMY_NA_PODWORZE_RINDE_S");
    set_lock_name("zamek");
}
