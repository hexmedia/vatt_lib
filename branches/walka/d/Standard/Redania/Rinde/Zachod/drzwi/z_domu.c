/*
 * Autor: Rantaur
 * Data: 18.06.06
 * Opis: Drzwi wyjsciowe z domu swiatynnego w rinde
 */
 
inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
        
    dodaj_przym("drewniany", "drewniani");
    
    set_other_room(ZACHOD_LOKACJE+"ulica4.c");
    
    set_door_id("DRZWI_RINDE_DOM_SWIATYNNY");
    
    set_door_desc("Matowe drzwi najlepsze lata maj� ju� z"
		  +" pewno�ci� za sob�, jednak mimo to wci�� wygladaj� bardzo"
		  +" solidnie. Pod mosi�na klamk� zauwa�asz niewielk� dziurk�"
		  +" na klucz.\n");

    set_open_desc("Prowadz�ce na ulic� drzwi s� otwarte.\n");
    set_closed_desc("Wyj�ciowe drzwi s� zamkni�te.\n");
    set_lock_desc("Zamek w tych drzwiach nie jest niczym niezwyk�ym."
		  +" Ot, zwyk�a dziurka na klucz.\n");
        
    set_pass_command(({({"drzwi","ulica","wyj^scie","wyjd^x z domu",
     "wyjd� z domu na ulic�","przejd^x przez drzwi"}),"na ulice","z domu"}));
        
    set_lock_command("zamknij");
    set_unlock_command("otw�rz");

    set_key("KEY_RINDE_DOM_SWIATYNNY_DRZWI");
    set_lock_name("zamek");
    
    set_open(0);
    set_locked(1);    
}
