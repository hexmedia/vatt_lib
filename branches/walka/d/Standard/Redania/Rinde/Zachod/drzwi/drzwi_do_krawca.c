
/* Autor: Avard
   Opis : Arivia
   Data : 31.03.07 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
    dodaj_przym("ci^e^zki", "ci^e^zcy");
    dodaj_przym("solidny","solidni");
    
    set_other_room(ZACHOD +"Krawiec/lokacje/krawiec.c");
    set_door_id("DRZWI_DO_KRAWCA_W_RINDE");
    set_door_desc("Grube, wykonane z twardego drewna drzwi, pokryte "+
        "zosta^ly ciemn^a farb^a, spod kt^orej wystaj^a koliste s^eki. W "+
        "pobli^zu futryny zamontowano ci^e^zk^a, mosi^e^zn^a klamk^a, "+
        "kt^ora pozwala na swobodne otwieranie i zamykanie drzwi.\n");
        
    set_pass_command(({({"warsztat","krawiec","wej^scie do warsztatu",
        "wejd^z do warsztatu","przejd^z przez drzwi do warsztatu","drzwi"}),
        "przez drzwi do zak�adu krawca","z zewn^atrz"}));

    set_open_desc("");
    set_closed_desc("");
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_key("KLUCZ_DRZWI_DO_KRAWCA_W_RINDE");
    set_lock_name("zamek");
}