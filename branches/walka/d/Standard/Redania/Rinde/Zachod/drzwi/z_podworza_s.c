
/* Autor: Avard
   Data : 10.08.06
   Info : Brama na jednej z ulic Rinde */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("brama"); 
    dodaj_przym("po^ludniowy", "po^ludniowi");
    dodaj_przym("drewniany", "drewnani");
    dodaj_przym("dwudrzwiowy","dwudrzwiowi");
    
    set_other_room(ZACHOD_LOKACJE+"ulica1");
    set_door_id("BRAMA_NA_PODWORZE_RINDE_S");
    set_door_desc("Dwudrzwiowa brama zosta^la umiejscowiona w "+
        "po^ludniowej ^scianie.\n");

 /* set_open_desc("Na po^ludniowej ^scianie znajduje si^e otwarta "+
        "brama.\n");
    set_closed_desc("Na po^ludniowej ^scianie znajduje si^e zamkni^eta "+
        "brama.\n"); */

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({({"po^ludnie","po^ludniowa brama",
        "przejdz przez po^ludniow^a bram^e"}),"przez bram^e na po^ludnie",
        "zza bramy z po^ludnia"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_key("KLUCZ_DO_BRAMY_NA_PODWORZE_RINDE_S");
    set_lock_name("zamek");
}
