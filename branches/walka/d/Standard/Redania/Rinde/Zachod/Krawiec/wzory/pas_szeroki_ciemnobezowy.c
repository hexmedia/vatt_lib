inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(RINDE_UBRANIA +"pasy/szeroki_ciemnobezowy_M");
  set_estimated_price(330);
  set_time_to_complete(1000);
  enable_want_sizes();
}