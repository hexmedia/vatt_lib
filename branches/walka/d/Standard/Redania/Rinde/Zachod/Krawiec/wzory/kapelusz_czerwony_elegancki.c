inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(RINDE_UBRANIA +"kapelusze/czerwony_elegancki_Mc");
  set_estimated_price(140);
  set_time_to_complete(7200);
  enable_want_sizes();
}