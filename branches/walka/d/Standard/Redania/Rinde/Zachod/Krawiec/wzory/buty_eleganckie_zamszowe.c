inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(BIALY_MOST_UBRANIA +"buty/eleganckie_zamszowe_Mc");
  set_estimated_price(275);
  set_time_to_complete(7200);
  enable_want_sizes();
}