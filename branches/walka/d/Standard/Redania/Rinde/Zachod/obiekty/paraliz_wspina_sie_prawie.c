#pragma strict_types

#include <macros.h>
#include "dir.h"
#include <ss_types.h>
#include <exp.h>

inherit "/std/paralyze";

static int alarm_id;

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta�");
    set_stop_message("Przestajesz si� wspina� na drzewo.\n");
    set_fail_message("Jeste� teraz zaj�t"+TP->koncowka("y","a")+
                     " wspinaniem si� na drzewo. Musisz wpisa� "+
                "'przesta�' by m�c zrobi�, to co chcesz.\n");
    set_finish_object(this_object());
    set_finish_fun("zatrzymanie");
    set_remove_time(4);
    setuid();
    seteuid(getuid());
}

int
zatrzymanie(string str)
{
    TP->catch_msg(query_stop_message());
    write("Puszczasz si� pnia i spadasz bole�nie na bruk.\n"+
           "Czujesz, �e gdyby� potrafi�"+this_player()->koncowka("","a")+
	   " jeszcze troch� bardziej si� "+
	   "wspina� podo�a�"+this_player()->koncowka("by�","aby�")+" temu zadaniu.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " puszcza si� pnia i "+
           "spada bole�nie na bruk.\n");
    this_player()->set_hp(this_player()->query_hp()-9);
    TP->add_fatigue(-40);
    TP->increase_ss(SS_CLIMB,EXP_WSPINAM_PRAWIE_UDANY_CLIMB);
	TP->increase_ss(SS_STR,EXP_WSPINAM_PRAWIE_UDANY_CLIMB_STR);
    TP->increase_ss(SS_DEX,EXP_WSPINAM_PRAWIE_UDANY_CLIMB_DEX);
	TP->increase_ss(SS_CON,EXP_WSPINAM_PRAWIE_UDANY_CLIMB_CON);
    remove_object();
    return 1;
}
