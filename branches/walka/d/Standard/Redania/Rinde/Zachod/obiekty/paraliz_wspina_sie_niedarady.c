#pragma strict_types

#include <macros.h>
#include "dir.h"
#include <exp.h>
#include <ss_types.h>

inherit "/std/paralyze";

static int alarm_id;

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta�");
    set_stop_message("Przestajesz si� wspina� na drzewo.\n");
    set_fail_message("Jeste� teraz zaj�t"+TP->koncowka("y","a")+
                     " wspinaniem si� na drzewo. Musisz wpisa� "+
                "'przesta�' by m�c zrobi�, to co chcesz.\n");
    set_finish_object(this_object());
    set_finish_fun("zatrzymanie");
    set_remove_time(3);
    setuid();
    seteuid(getuid());
}

int
zatrzymanie(string str)
{

    write("Ju� po chwili oceniasz, �e nie dasz rady si� tam wspi��.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " spostrzegsz�y, �e drzewo jest "+
         "jak dla nie"+this_player()->koncowka("go","j")+" zbyt du�ym "+
	 "wyzwaniem zmienia swoj� decyzj� i przestaje si� wspina�.\n");
    TP->catch_msg(query_stop_message());
    TP->add_fatigue(-30);
    TP->increase_ss(SS_CLIMB,EXP_WSPINAM_NIEUDANY_CLIMB);
	TP->increase_ss(SS_STR,EXP_WSPINAM_NIEUDANY_CLIMB_STR);
    TP->increase_ss(SS_DEX,EXP_WSPINAM_NIEUDANY_CLIMB_DEX);
	TP->increase_ss(SS_CON,EXP_WSPINAM_NIEUDANY_CLIMB_CON);
	
    remove_object();
    return 1;
}
