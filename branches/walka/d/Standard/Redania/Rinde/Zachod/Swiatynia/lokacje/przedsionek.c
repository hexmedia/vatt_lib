
/* Przedsionek swiatyni Kreve, w Rinde 
   Made by Avard, 01.06.06 
   Opis by Tinardan */
   
   // dokonczyc!

inherit "/std/room";

#include <stdproperties.h>
#include <filter_funs.h>
#include <macros.h>
#include "dir.h"

void
create_room() 
{
    set_short("Sala wej^sciowa ^swi^atyni Kreve");

    add_exit(SWIATYNIA_LOKACJE + "nawa_glowna", "n");

    add_prop(ROOM_I_INSIDE, 1);

    add_object(SWIATYNIA_DRZWI + "ze_swiatyni.c");
    set_alarm_every_hour("co_godzine");
    add_npc(RINDE_NPC + "brudny_straznik");
}
public string
exits_description() 
{
    return "Wyj^scie prowadzi na ulice, za^s na p^o^lnocy znajduje si^e "+
           "nawa g^l^owna ^swi^atyni.\n";
}


string
dlugi_opis()
{
    string str;
   
    str = "Jak na przedsionek ogromnej ^swi^atyni, pomieszczenie jest " +
    "niezwykle ma^le; ledwie starcza miejsca dla tych kilku os^ob, kt^ore " +
    "przystan^e^ly tu przed wej^sciem do g^l^ownych komnat ^swi^atyni. " +
    "Ogromne, d^ebowe drzwi, otwierane tylko podczas ^Swi^etego Tygodnia " +
    "zajmuj^a niemal^ze ca^le le^z^ace naprzeciw siebie ^sciany; jedne " +
    "wrota na p^o^lnocy, prowadz^ace wg^l^ab ^swi^atyni, i drugie na " +
    "po^ludniu, na zewn^atrz. Wierni wpuszczani s^a jedynie przez ma^le " +
    "furtki, wzmocnione ^zelaznymi okuciami i uchylaj^ace si^e z potwornym " +
    "skrzypieniem. ";

    return str;
}
int
co_godzine()
{
    LOAD_ERR(SWIATYNIA_LOKACJE+"nawa_boczna_w.c");
    LOAD_ERR(SWIATYNIA_LOKACJE+"nawa_glowna.c");
    LOAD_ERR(SWIATYNIA_LOKACJE+"nawa_boczna_e.c");
    object *gracze = FILTER_PLAYERS(all_inventory(this_object()));

    if((present("krepp", find_object(SWIATYNIA_LOKACJE+"nawa_boczna_w")) ||
       present("krepp", find_object(SWIATYNIA_LOKACJE+"nawa_glowna")) ||
       present("krepp", find_object(SWIATYNIA_LOKACJE+"nawa_boczna_e"))) && 
       (MT_GODZINA == 23)) 
    {
        tell_room(QCIMIE(find_living("krepp"),PL_MIA)+ " przybywa z "+
            "p^o^lnocy i wyprowadza ci^e na ulic^e, m^owi^ac: Zamykam "+
            "^swi^atynie, zapraszam jutro.\n");
        gracze->move(ZACHOD_LOKACJE+"ulica8");
        find_living("krepp")->move(ZACHOD_LOKACJE+"ulica8");
    }
    if(present("krepp", TO) && (MT_GODZINA == 23))
    {
        tell_roombb(this_object(), QCIMIE(find_living("krepp"),PL_MIA)+ " wyprowadza ci^e "+
            "na ulic^e, m^owi^ac: Zamykam ^swi^atynie, zapraszam jutro.\n");
        gracze->move(ZACHOD_LOKACJE+"ulica8");
        find_living("krepp")->move(ZACHOD_LOKACJE+"ulica8");
    }
}
