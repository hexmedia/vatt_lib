/* Lokacja zwyk�ej alejki przed �wi�tyni� w Rinde.
 * Kamienie clonuj� sie na bie��co, przez add_action we�, bo chyba tak
 * lepiej jest, ni� clone_object x999999999 :P
 * 
 * Opis troch� by Solothurn, reszta Lil. 24.08.2005 */

#include <stdproperties.h>
#include "dir.h"
#include <macros.h>

inherit RINDE_STD;

int wez(string str);
int sa_kamienie = 1;
void
create_rinde_street()
{
   set_short("Alejka przed �wi�tyni�");
   set_long("Przepi�kna prosta alejka"+
	" prowadzi do �wi�tyni boga o imieniu Kreve."+
	" Bo�nica majaczy przed tob� w ca�ej swej okaza�o�ci,"+
	" ukazuj�c swoj� strzelist� wie�� oraz pos�g w kszta�cie ognia"+
	" utkwiony kilkana�cie st�p przed masywnymi wrotami."+
	" Sama alejka jest usypana r�nokolorowymi kamykami, a kar�owate,"+
	" starannie przystrzy�one drzewka czyni� z niej przeromantyczne"+
	" miejsce dla schadzek r�nych par. ");

   add_event("Wiatr igra z ga��ziami kar�owatych drzewek.\n");

   add_exit("ulica7.c","po�udnie");

   add_item("alejk�", "Usypana kolorowymi kamykami alejka ci�gnie si� "+
		   "do wr�t �wi�tyni Kreve. Tu� przed ni� rozszerza si� nieco"+
		   " na lewo i prawo od pos�gu w kszta�cie ognia "+
		   "utkwionego na samym �rodku alejki.\n");
    add_item(({"wie��","strzelist� wie��","�wi�tynn� wie��","bia�� wie��",
	           "strzelist� bia�� wie��","bia�� strzelist� wie��",
		   "�wi�tynn� bia�� wie��","�wi�tynn� strzelist� wie��"}),
		   "�wi�tynna w�ska i bia�a wie�a wybija si� ponad dachy "+
		   "innych budynk�w i wygl�da bez trudu przez fortyfikacj� miasta.\n");

    add_item(({"wie^z^e","smuk^l^a wie^z^e","wie^z^e ratusza",
               "smuk^la wie^z^e ratusza"}),
           "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic, "+
           "niczym palec, wskazuj^acy centrum tego miasta.\n");

    add_object(SWIATYNIA_DRZWI+"do_swiatyni.c");
    dodaj_rzecz_niewyswietlana("wielkie wrota", 1);

}
public string
exits_description() 
{
    return "Ulica prowadzi na po^ludnie, za^s na p^o^lnocy znajduje si^e "+
           "wej^scie do ^swi^atyni.\n";
}


public void
init()
{
    ::init();                                                                  
    add_action(wez,"we�");
}

/*Tak, tandetnie, ale jednak znacznie spowalnia branie kamieni
 *w niesko�czono�� ;) */
int
daj_kamienie()
{
    sa_kamienie = 1;
    return 1;
}

int wez(string str)
{
    if((str ~= "kamie�" || str ~= "kamie� z alejki") && sa_kamienie)
    {
        write("Bierzesz kamie� z alejki.\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " bierze kamie� z alejki.\n");
        clone_object(RINDE+"przedmioty/kamien")->move(this_player());
        sa_kamienie=0;
        set_alarm(2.0,0.0,&daj_kamienie());
        return 1;      
    }

    return 0;
}
