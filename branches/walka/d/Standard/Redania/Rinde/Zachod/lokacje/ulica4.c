#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;

void
create_rinde_street() 
{
    set_short("Ulica");

    add_exit("ulica3", "po�udnie");
    add_exit("ulica5", "wsch�d");

    add_object(ZACHOD_DRZWI+"do_domu.c");

    add_item(({"wie��","strzelist� wie��","�wi�tynn� wie��","bia�� wie��",
               "strzelist� bia�� wie��","bia�� strzelist� wie��",
               "�wi�tynn� bia�� wie��","�wi�tynn� strzelist� wie��"}),
               "�wi�tynna w�ska i bia�a wie�a wybija si� ponad dachy "+
               "innych budynk�w i wygl�da bez trudu przez fortyfikacj� miasta.\n");
    add_item(({"wie^z^e","smuk^l^a wie^z^e","wie^z^e ratusza",
               "smuk^la wie^z^e ratusza"}),
               "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic, "+
               "niczym palec, wskazuj^acy centrum tego miasta.\n");
}
public string
exits_description() 
{
    return "Ulica prowadzi na po^ludnie i wsch^od, s^a tu tak^ze drzwi do "+
           "domu.\n";
}


string
dlugi_opis()
{
    string str;

    str = "Droga w tym miejscu tworzy ostry zakr�t. W kierunku po�udniowym " +
	"prowadzi do niewyra�nie widocznego st�d skrzy�owania, za� na " +
	"wsch�d w kierunku ratusza na placu g��wnym. Nieco na po�udniu, po " +
	"zachodniej stronie drogi widnieje wielki i okaza�y budynek " +
	"�wi�tyni, kt�r� wie�czy smuk�a wie�a z dzwonnic�. Tu� przy " +
	"zakr�cie stoi odosobniony spory budynek, niepodobny do reszty " +
	"zabudowa� w tej dzielnicy. Z wygl�du mo�na pozna�, �e jest to " +
	"jeden z budynk�w �wi�tynnych. Na wschodzie zaczyna si� dzielnica " +
	"z�o�ona z niewysokich, odrapanych kaminiczek, ci�gnaca si� a� do " +
	"placu g��wnego.\n";

    return str;
}
