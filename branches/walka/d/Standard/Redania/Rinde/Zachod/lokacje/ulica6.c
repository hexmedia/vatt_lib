/* Made in China;) by Lil 03.05 */

#include <stdproperties.h>
#include "dir.h"
#include <macros.h>

inherit RINDE_STD;

string czy_sa_smiecie();
//int wez_butelke();

//int ile_butelek = 6;

void
create_rinde_street() 
{
    set_short("Na podw�rzu kamienic");
    set_long("Podw�rze to wyznaczaj� ty�y blok�w kamienic, "+
    	    "zaniedbane i obklejone b�otem. Z p�nocy jak i z "+
	    "po�udnia skrzypi� czasem w swych zawiasach skrzyd�a "+
	    "bram, kt�re s� tutaj jedynymi widocznymi wyj�ciami. "+
	    "Na parterze wi�kszo�� okien kamienic wychodz�cych "+
	    "na t� cz�� miasta zabito deskami, lub zas�oni�to "+
	    "ciemnymi p�achtami, by unikn�� nieciekawych "+
	    "widok�w. Ka�dy odg�os odbija si� od nagich, "+
	    "nad�artych z�bem czasu �cianach, tworz�cych "+
	    "charakterystyczn� dla takich opuszczonych miejsc "+
	    "akustyk�@@czy_sa_smiecie@@\n");

   add_item(({"kamienice","�ciany"}),
            "Nagie �ciany kamienic nie zosta�y obdarzone �adnymi "+
	    "urzekaj�cymi elementami, chyba, �e dodaj� uroku "+
	    "dla� b�oto oraz te tajemnicze plamy w dolnych jej "+
	    "partiach.\n");
   add_item("plamy",
            "Pewnikiem s� one dzie�em miejscowych pijaczk�w, "+
	    "niemog�cych za�atwi� swych potrzeb nigdzie indziej.\n");
   add_item(({"bram�","bramy"}),
            "Dwudrzwiowe bramy wyprowadzaj�ce z tego podw�rza na "+
	    "miasto umiejscowiono w �cianie p�nocnej i po�udniowej.\n");
   add_item("okna",
            "Te na dole s� przewa�nie pozabijane deskami, lub "+
	    "zas�onione materia�em. G�rne za� s� dwukrotnie mniejsze "+
	    "i zmuszone na wiecznie ten sam, monotonny widok "+
	    "- �ciany naprzeciw.\n");
   add_item(({"ziemi^e","pod�o�e"}),
            "Pe^lno tutaj b^lota i ci^e^zko znale^x^c skrawek suchej ziemi. "+
            "@@czy_sa_smiecie@@\n");
   

   add_object(RINDE+"przedmioty/bronie/butelka_los_rozbita.c", 4);
   if (random(2)) add_object(RINDE+"przedmioty/bronie/butelka_los_rozbita.c");
   if (random(2)) add_object(RINDE+"przedmioty/bronie/butelka_los_rozbita.c");
   if (random(2)) add_object(RINDE+"przedmioty/bronie/butelka_los_rozbita.c");
   if (random(2)) add_object(RINDE+"przedmioty/bronie/butelka_los_rozbita.c");
   if (random(2)) add_object(RINDE+"przedmioty/bronie/butelka_los_rozbita.c");
   if (random(2)) add_object(RINDE+"przedmioty/bronie/butelka_los_rozbita.c");

   dodaj_rzecz_niewyswietlana("rozbita brudna butelka", 0);
   dodaj_rzecz_niewyswietlana("rozbita zielona butelka", 0);
   dodaj_rzecz_niewyswietlana("rozbita br�zowa butelka", 0);
   dodaj_rzecz_niewyswietlana("brudna rozbita butelka", 0);
   dodaj_rzecz_niewyswietlana("zielona rozbita butelka", 0);
   dodaj_rzecz_niewyswietlana("br�zowa rozbita butelka", 0);
   
   add_object(ZACHOD_DRZWI+"z_podworza_s.c");
   add_object(ZACHOD_DRZWI+"z_podworza_n.c");
   dodaj_rzecz_niewyswietlana("drewniana dwudrzwiowa brama",2);

   add_event("Z trzaskiem zamkni�to jedno z okien na pi�trze.\n");
   add_event("Skrzyd�o po�udniowej bramy skrzypi na wietrze.\n");
   add_event("Skrzyd�o p�nocnej bramy skrzypi na wietrze.\n");
}
public string
exits_description() 
{
    return "Na p^o^lnocy i po^ludniu znajduj^a si^e dwie bramy.\n";
}


string
czy_sa_smiecie()
{
   string str;
   int i, il, ile_butelek;
        object *inwentarz;
	
   inwentarz = all_inventory();
        ile_butelek = 0;
        il = sizeof(inwentarz);
   for (i = 0; i < il; ++i) {
                if (inwentarz[i]->test_jestem_rozbita_butelka1()) {
                        ++ile_butelek;}
                }
                
   if (ile_butelek > 1) {
             str = ". Jakby tego by^lo ma^lo, w miejscach wolnych od b^lota "+
                   "le^zy mn^ostwo rozbitych butelek.";
                }

 /*  if(jest_rzecz_w_sublokacji(0,"brudna rozbita butelka") ||
      jest_rzecz_w_sublokacji(0,"zielona rozbita butelka") ||
      jest_rzecz_w_sublokacji(0,"br�zowa rozbita butelka"))*/
   		 
   else
   str = ". ";

   return str;
}
