/* Mala lokacja na drzewie :)
      Bedzie mozna stad przeskoczyc na mur.
                                            Lil */

inherit "/std/room";

#include <stdproperties.h>
#include "dir.h"

void create_room() 
{
	set_short("Na ga��zi d�bu");
	set_long("Ga��� jest gruba, lecz i tak jest to do�� niestabilne miejsce. "+
	         "Ga��ziane rami� si�ga muru, lecz przebywa� mo�na tylko przy konarze, "+
		 "gdzie zmieszcz� si� dwie osoby.\n");
	add_sit("na ga��zi","na ga��zi","z ga��zi",2);
	add_exit("ulica7.c", ({ "d", "na d^o^l", "z drzewa" }), 0, 20);
        add_prop(CONT_I_MAX_VOLUME, 200000);
	add_prop(ROOM_I_INSIDE,0);
	add_prop(ROOM_I_TYPE,ROOM_TREE);

}
public string
exits_description() 
{
    return "Na dole znajduje si^e ulica.\n";
}

