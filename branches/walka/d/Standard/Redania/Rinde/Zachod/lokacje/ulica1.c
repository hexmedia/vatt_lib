#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;

void
create_rinde_street() 
{
    set_short("Ulica");

    add_exit("ulica2", "zach�d");
    add_exit(CENTRUM_LOKACJE + "placsw", "wsch�d");
    add_object(ZACHOD_DRZWI+"na_podworze_s.c");
    dodaj_rzecz_niewyswietlana("drewniana dwudrzwiowa brama");
	

    add_object(RINDE + "przedmioty/lampa_uliczna");

    add_item(({"wie��","strzelist� wie��","�wi�tynn� wie��","bia�� wie��",
               "strzelist� bia�� wie��","bia�� strzelist� wie��",
               "�wi�tynn� bia�� wie��","�wi�tynn� strzelist� wie��"}),
               "�wi�tynna w�ska i bia�a wie�a wybija si� ponad dachy "+
               "innych budynk�w i wygl�da bez trudu przez fortyfikacj� miasta.\n");
    add_item(({"wie^z^e","smuk^l^a wie^z^e","wie^z^e ratusza",
               "smuk^la wie^z^e ratusza"}),
               "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic, "+
               "niczym palec, wskazuj^acy centrum tego miasta.\n");
}
public string
exits_description() 
{
    return "Ulica prowadzi na zach^od, natomiast na wschodzie znajduje si^e "+
           "plac. Znajduje si^e tu tak^ze brama prowadz^aca na p^o^lnoc.\n";
}

string dlugi_opis()
{
    string str;
    
    str = "Rozgl�daj�c si� zauwa�asz, �e jeste� na jednej z g��wnych " +
        "ulic, tu� przy zachodniej granicy wielkiego placu w " +
	"centrum miasta. Na jego �rodku, kilkadziesi�t metr�w " +
	"st�d rysuje si� masywna sylwetka imponuj�cych rozmiar�w " +
	"ratusza miejskiego, zwie�czonego smuk��, strzelist� " +
	"wie��, opieku�czo g�ruj�c� nad miastem. Obrze�a ulicy " +
	"s� w tym miejscu ciasno zabudowane wiekowymi niemal�e, " +
	"szarymi kamienicami. Na p�nocy dwie z nich zespolone " +
	"s� bram� na podw�rze, od kt�rej dochodzi nieprzyjemna " +
	"wo� za�atwianych napr�dce potrzeb fizjologicznych.\n";
	
    return str;
}
