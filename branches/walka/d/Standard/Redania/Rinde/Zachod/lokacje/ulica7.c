#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <sit.h>
#include "dir.h"

inherit RINDE_STD;

int wespnij(string str);
string drzefko(string str);

void
create_rinde_street() 
{
	set_short("Ulica");

	add_exit("ulica8.c","p^o^lnoc");
	add_exit("ulica2.c","wsch^od");

	LOAD_ERR(ZACHOD_LOKACJE+"nadrzewie");
	add_item(({"ulic�","pas ulicy","bruk","brukowan� ulic�","wyg�adzone kamienie"}),
	         "Starannie wy�o�ona brukiem, szeroka ulica ma tu "+
		 "sw�j koniec na ceglanym murze. Na �rodku wyrasta "+
		 "okaza�e drzewo.\n");
	add_item(({"mur","ceglany mur"}), "Wysoki na oko�o dwadzie�cia "+
	         "st�p, ceglany mur zatacza kr�g wok� ca�ego miasta.\n");
	add_item(({"drzewo","d�b","pie�","gruby pie�","pie� drzewa","pie� d�bu",
	           "gruby pie� drzewa","gruby pie� d�bu"}), "@@drzefko@@");
	add_item(({"alejk�","usypan� kamieniami alejk�"}), "Od g��wnej ulicy "+
	           "odbija na p�noc usypana kamieniami alejka, a prowadzi ona "+
		   "do okaza�ej �wi�tynnej budowli.\n");
	add_item(({"okaza�� budowl�","budowl�","okaza�� �wi�tynn� budowl�","�wi�tyni�",
	           "okaza�� �wi�tyni�"}), "Na p�nocy majaczy przed tob� okaza�a "+
		   "�wi�tynna budowla, zwie�czona strzelist�, bia�� wie��. Prowadzi "+
		   "do niej usypana kamieniami alejka.\n");
       add_item(({"wie��","strzelist� wie��","�wi�tynn� wie��","bia�� wie��",
                  "strzelist� bia�� wie��","bia�� strzelist� wie��",
                  "�wi�tynn� bia�� wie��","�wi�tynn� strzelist� wie��"}),
                  "�wi�tynna w�ska i bia�a wie�a wybija si� ponad dachy "+
                  "innych budynk�w i wygl�da bez trudu przez fortyfikacj� miasta.\n");
       add_item(({"wie^z^e","smuk^l^a wie^z^e","wie^z^e ratusza",
                  "smuk^la wie^z^e ratusza"}),
                  "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic, "+
                  "niczym palec, wskazuj^acy centrum tego miasta.\n");
	add_item(({"kamienice","szeregi m�tnych kamienic","m�tne kamienice",
	           "szeregi kamienic"}), "Szare kamienice ci�gn� si� wzd�u� szerokiej "+
		   "wybrukowanej ulicy.\n");
}
public string
exits_description() 
{
    return "Ulica prowadzi na p^o^lnoc i wsch^od.\n";
}


string dlugi_opis()
{
   string str;
   
   str="D�ugi wybrukowany pas ulicy prowadz�cy przez ca�e "+
       "miasto przecina tutaj brutalnie wysoki na oko�o "+
       "dwadzie�cia st�p ceglany mur. Spo�r�d wyg�adzonych kamieni, "+
       "z okr�g�ej dziury majestatycznie bije w g�r� zdrowy d�b, o "+
       "bardzo grubym pniu. ";
    if(pora_roku() != MT_ZIMA)
{
      str+="Na p�noc, usypana w�r�d zieleni kamienna alejka wiedzie "+
       "do okaza�ej �wi�tyni, kt�rej strzelista, bia�a wie�a wygl�da bez "+
       "trudu przez fortyfikacj� miasta. ";
}
    else
{
      str+="Na p�noc, usypana w�r�d ^sniegu kamienna alejka wiedzie "+
       "do okaza�ej �wi�tyni, kt�rej strzelista, bia�a wie�a wygl�da bez "+
       "trudu przez fortyfikacj� miasta. ";
}

    if(!dzien_noc())
      str+="W przeciwn� temu budynkowi stron� ci�gn� si� szeregi m�tnych "+
           "kamienic, mi�dzy kt�rymi toczy si� codzienno�� ich mieszka�c�w. ";
    else
      str+="W przeciwn� temu budynkowi stron� ci�gn� si� szeregi m�tnych "+
           "kamienic, mi�dzy kt�rymi za dnia toczy si� codzienno�� "+
	   "ich mieszka�c�w. ";


    str+="\n";
    return str;
}

public void
init()
{
    ::init();                                                                  
    add_action(wespnij,"wespnij");
    add_action(wespnij,"wejd�");
}

int
wespnij(string str)
{
    object *drzef;

    if(query_verb() == "wespnij")
        notify_fail("Wespnij si� gdzie?\n");
    else
        notify_fail("Wejd� gdzie?\n");

    if(!strlen(str))
       return 0;

    if(!HAS_FREE_HANDS(this_player()))
    {
        write("Musisz mie� obie d�onie wolne, aby m�c to zrobi�.\n");
        return 1;
    }
    if(TP->query_prop(SIT_SIEDZACY))
    {
        write("Czujesz, �e wspinaczka na siedz�co nie jest twoj� domen�.\n");
        return 1;
    }
    if(TP->query_prop(SIT_LEZACY))
    {
        write("Czujesz, �e wspinaczka na le��co nie jest twoj� domen�.\n");
        return 1;
    }
    if(TP->query_fatigue() < 25)
    {
       write("Nie masz ju� na to si�y.\n");
       return 1;
    }

    if(TP->move(ZACHOD_LOKACJE+"nadrzewie", 0, 0, 1) == 8)
    {
        write("Wygl�da na to, �e ju� si� tam nie zmie�cisz.\n");
        return 1;
    }
    
    if(!parse_command(str, environment(TP), "'si^e' 'na' 'd^ab' / 'drzewo' ") &&
        !parse_command(str, environment(TP), "'na' 'd^ab' / 'drzewo' "))
        return 0;


    TP->set_fatigue(TP->query_fatigue()-random(10));
    write("Zaczynasz wspina� si� na drzewo.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " zaczyna wspina� si� na drzewo.\n");
	
	if(this_player()->query_skill(SS_CLIMB) <= 13)
	{
        clone_object(ZACHOD_OBIEKTY+"paraliz_wspina_sie_niedarady")->move(this_player());
	return 1;
	}
	if(this_player()->query_skill(SS_CLIMB) >= 14 &&
	   this_player()->query_skill(SS_CLIMB) <= 23)
	{
	clone_object(ZACHOD_OBIEKTY+"paraliz_wspina_sie_prawie")->move(this_player());
	return 1;
	}
	
    clone_object(ZACHOD_OBIEKTY+"paraliz_wspina_sie")->move(this_player());
    return 1;
}

string drzefko(string str)
{
    object *kto;

     str="Niesamowicie gruby d^ab "+
	 "pnie si� ku niebu ponad kamienice. Jego rozczapierzone "+
	 "ga��ziane ramiona muskaj� cegie� muru, poruszane wiatrem. ";
   
    kto = FILTER_LIVE(all_inventory(find_object(ZACHOD_LOKACJE+"nadrzewie")));

    if (sizeof(kto) == 1)
	str+="Na ga^l^ezi siedzi " + COMPOSITE_LIVE(kto, PL_MIA) + ".";
    if (sizeof(kto) > 1)
	str+="Na ga^l^ezi siedz^a " + COMPOSITE_LIVE(kto, PL_MIA) + ".";
   
   str+="\n";
   return str;
}
