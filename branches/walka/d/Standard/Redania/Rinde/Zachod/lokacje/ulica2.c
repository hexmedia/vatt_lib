#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;

void
create_rinde_street() 
{
    set_short("Ulica");

    add_exit("ulica3", "p�noc");
    add_exit(POLUDNIE_LOKACJE + "ulica8", "po�udnie");
    add_exit("ulica7", "zach�d");
    add_exit("ulica1", "wsch�d");	

    add_object(RINDE + "przedmioty/lampa_uliczna");
    add_object(ZACHOD_DRZWI + "drzwi_do_krawca");

    add_item(({"wie��","strzelist� wie��","�wi�tynn� wie��","bia�� wie��",
               "strzelist� bia�� wie��","bia�� strzelist� wie��",
               "�wi�tynn� bia�� wie��","�wi�tynn� strzelist� wie��"}),
               "�wi�tynna w�ska i bia�a wie�a wybija si� ponad dachy "+
               "innych budynk�w i wygl�da bez trudu przez fortyfikacj� miasta.\n");
    add_item(({"wie^z^e","smuk^l^a wie^z^e","wie^z^e ratusza",
               "smuk^la wie^z^e ratusza"}),
               "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic, "+
               "niczym palec, wskazuj^acy centrum tego miasta.\n");
}
public string
exits_description() 
{
    return "Ulica prowadzi na zach^od, wsch^od, po^ludnie i p^o^lnoc, "+
        "znajduj^a si^e tu tak^ze drzwi do zak^ladu krawieckiego.\n";
}



string
dlugi_opis()
{
    string str;

    str = "Du�e i ruchliwe skrzy�owanie w zachodniej cz�^sci miasta. Na zach�d ci^agnie si� " +
	"w^aska zadbana uliczka, skr�caj^aca nieco dalej na p�noc w kierunku dobrze st^ad " +
	"widocznej miejskiej ^swi^atyni. Szeroka i prosta ulica prowadz^aca na wsch�d " +
	"ci^agnie si� hen, a� do bramy miejskiej, kt�rej niewyra^xne zarysy jeste^s w stanie " +
	"st^ad dostrzec. Na p�nocy ci^agnie si� rz^ad kamienic, urywaj^acy si� tu� przy " +
	"widocznych st^ad murach miejskich. Niewielka gruntowa uliczka prowadzi te� na " +
	"po�udnie, w g^aszcz niewielkich i zaniedbanych drewnianych domostw, i dalej na " +
	"po�udniowy wsch�d w kierunku bramy. ";
    if (dzien_noc())
	str += "Na obrze�ach opustosza�ego o tej porze placu porozstawiane s^a drewniane stragany.\n";
    else
	str += "T�tni^acy �yciem plac wype�niony jest t�umem mieszczan, wie^sniak�w i kupc�w, " +
	    "tworz^acych ha�a^sliw^a i barwn^a mieszank�. Najwi�kszy zgie�k panuje na obrze�ach " +
	    "placu, gdzie poustawiane s^a drewniane stragany, wok� kt�rych zbieraj^a si� " +
	    "grupki potencjalnych klient�w, przekrzykuj^acych si� nawzajem i zawzi�cie " +
	    "gestykuluj^acych.\n";
    return str;
}
