
/*Troche Delvert, troche Lil. Opis Tinardan. :)*/

inherit "/std/room";
inherit "/lib/pub_new";

#include <stdproperties.h>
#include "dir.h"
#include <mudtime.h>

void
create_room() 
{
    set_short("Piekarnia \"S�odka bu�eczka\"");
    set_long("@@dlugi_opis@@");

    add_item("tabliczk�", "Rze�by, kt�rymi tabliczka jest ozdobiona u do�u i po bokach, przedstawiaj� " +
	"piekarza otoczonego ciastami, chlebami i bu�eczkami. Na jej �rodku wisi " +
	"karteczka zapisana eleganckim, r�wnym pismem. Jasne, sosnowe drewno �adnie " +
	"odbija si� od poszarza�ej przez szereg lat boazerii.\n");
    add_item("st�", "St� l�ni czysto�ci�, a jego s�katy blat wygl�da do�� solidnie.\n");
    add_item("�aw�", "Solidna, d�bowa �awa stoi tu� obok najwi�kszego sto�u. Rze�bienia, kt�rymi " +
	"jest pokryta, przedstawiaj� wypieki serwowane przez piekarza.\n");
	
    add_object(WSCHOD_OBIEKTY+"menu_piekarnia");
    add_object(MEBLER+"stol_piekarnia");
    add_object(MEBLER+"lawa_piekarnia", 2);
    add_object(MEBLER+"krzeslo_piekarnia", 6);

    add_object(WSCHOD_DRZWI+"piekarnia");

    add_npc(RINDE_NPC + "piekarz");

    add_prop(ROOM_I_INSIDE,1);

    dodaj_rzecz_niewyswietlana("solidny st�");
    dodaj_rzecz_niewyswietlana("drewniane krzes�o", 6);
    dodaj_rzecz_niewyswietlana("d�bowa �awa", 2);
	
    add_wynos_food("chleb",({ ({"^zytni"}),({"^zytni"}) }), 801, 5,
     "Oto bochenek ^zytniego chleba.\n", 2800, ({ "sple^snia^ly", "sple^sniali" }));
    add_wynos_food("bu^lka", ({ ({ "s^lodki" }),
        ({ "s^lodcy" }) }), 400, 4, "Jest to niewielka s^lodka bu^lka.\n",
        2400, ({ "sple^snia^ly", "sple^sniali" }));
    add_wynos_food(({"rogalik z czekolad�","rogalik"}),
         0, 421, 6, "Niewielki rogalik z czekoladowym nadzieniem.\n",
         2001, ({ "zepsuty", "zepsuci" }));
    add_wynos_food(({"pty^s z kremem","pty^s"}),
         0, 125, 5, "S^lodki pty^s z kremem.\n",
         1201, ({ "zepsuty", "zepsuci" }));
    add_wynos_food("sernik",
         0, 400, 5, "Jest to kawa^lek bia^lego puszystego sernika.\n",
         1909, ({ "zepsuty", "zepsuci" }));
	
    if((this_object()->pora_roku() == MT_LATO) || 
	   (this_object()->pora_roku() == MT_JESIEN))
    {
	add_wynos_food(({"ciasto wi�niowe","ciasto"}),
         0, 700, 8, "Prostok�tny kawa�ek znakomitego ciasta wi�niowego.\n",
         1701, ({ "zepsuty", "zepsuci" }));
    }

    if((this_object()->pora_roku() == MT_JESIEN))
    {
	add_wynos_food(({"placuszek z jab�kami","placuszek"}),
         0, 380, 5, "Okr�g�y jab�kowy placuszek.\n",
         1901, ({ "zepsuty", "zepsuci" }));
    }

    if((this_object()->pora_roku() == MT_JESIEN) || 
	   (this_object()->pora_roku() == MT_ZIMA))
    {
	add_wynos_food(({"placuszek z konfiturami","placuszek"}),
         0, 330, 7, "Okr�g�y placuszek z konfiturami.\n",
         1901, ({ "zepsuty", "zepsuci" }));
    }

    if((this_object()->pora_roku() == MT_LATO))
    {
	add_wynos_food(({"ciastko z malinami","ciastko"}),
         0, 331, 6, "Niewielkie ciastko z malinami.\n",
         1701, ({ "zepsuty", "zepsuci" }));
    }

    if((this_object()->pora_roku() == MT_WIOSNA))
    {
	add_wynos_food(({"ciastko poziomkowe","ciastko"}),
         0, 321, 6, "Niewielkie ciastko poziomkowe.\n",
         1701, ({ "zepsuty", "zepsuci" }));
    }

    if((this_object()->pora_roku() == MT_LATO))
    {
	add_wynos_food(({"ciastko truskawkowe","ciastko"}),
         0, 329, 7, "Niewielkie ciastko truskawkowe.\n",
         1701, ({ "zepsuty", "zepsuci" }));
    }
}
public string
exits_description() 
{
    return "Wyj^scie z piekarni prowadzi na ulice.\n";
}

string
dlugi_opis()
{
	string str;
	int i, il, il_krzesel, il_stolow, il_law;
	object *inwentarz;

	str = "Ju� na pierwszy rzut oka mo�na zauwa�y�, �e w�a�ciciel tego przybytku kocha " +
		"swoj� prac�. ";
	
	/* opis zale�ny od obecno�ci krzese� i sto��w na lokacji */
	inwentarz = all_inventory();
	il_krzesel = 0;
	il_stolow = 0;
	il_law = 0;
	il = sizeof(inwentarz);
	
	for (i = 0; i < il; ++i) {
		if (inwentarz[i]->test_rwops1()) {
			++il_stolow;
		}
		if (inwentarz[i]->test_rwopk1()) {
			++il_krzesel;
		}
		if (inwentarz[i]->test_rwopl1()) {
			++il_law;
		}
	}
	if (il_krzesel + il_law) {
		if (il_stolow) {
			str += "Dooko�a sto�u wsz�dzie ";
		}
		else {
			str += "Wsz�dzie ";
		}

		if ((il_krzesel > 1) && (il_law > 1)) {
			str += "porozstawiane s� krzes�a i �awy ";
		}
		else if ((il_krzesel > 0) && (il_law > 0)) {
			str += ((il_krzesel == 1) ? "postawione jest krzes�o" : "porozstawiane s� krzes�a") +
				" i " + ((il_law == 1) ? "�awa " : "�awy ");
		}
		else if (il_law > 0) {
			if (il_law == 1) {
				str += "postawiona jest �awa ";
			}
			else {
				str += "porozstawiane s� �awy ";
			}
		}
		else if (il_krzesel > 0) {
			if (il_krzesel == 1) {
				str += "postawione jest krzes�o ";
			}
			else {
				str += "porozstawiane s� krzes�a ";
			}
		}

		str += "dla klient�w, kt�rzy chcieliby zje�� na miejscu, a za ";
	}
	else {
		if (il_stolow) {
			str += "Na �rodku pomieszczenia stoi st� dla klient�w, kt�rzy chcieliby " +
				"zje�� co� na miejscu, a za ";
		}
		else {
			str += "Za ";
		}
	}
	/* --- */
	
	str += "barem zalegaj� kusz�ce stosy s�odkich " +
		"bu�eczek, ciastek wi�niowych, zio�owych chleb�w i plack�w o najr�niejszych " +
		"smakach. Na �cianie wisi rze�biona tabliczka z przytwierdzon� do niej kartk�. " +
		"Pod�oga, kt�ra widocznie jest �wie�o zamieciona, lekko skrzypi pod ci�kimi " +
		"krokami klient�w. ";
	
	if (dzien_noc())
		str += "Ulica, kt�ra jest widoczna za szerokim, frontowym oknem " +
			"�wieci o tej porze pustkami. Zza przymkni�tych drzwi prowadz�cych do kuchni " +
			"co jaki� czas mo�na us�ysze� chrapanie zm�czonego kuchcika.\n";
	else
	{
// TODO: Jak juz beda enpece, to trzeba dac sprawdzanie czy za oknem sa dzieciaki
//		if (random(2)) str += "Za szerokim, frontowym oknem wida� rozbawione " +
//			"dzieci�ce postacie, wskazuj�ce ubrudzonymi r�czkami ulubione wypieki i " +
//			"figluj�ce mi�dzy sob�. ";
		str += "Zza przymkni�tych drzwi prowadz�cych do kuchni " +
			"dochodzi ha�as czyniony przez zabieganych kuchcik�w i buzuj�cy w piecach " +
			"ogie�.\n";
	}
			

	return str;
}
	
void
init()                                                                         
{
    ::init();
    init_pub();
}
