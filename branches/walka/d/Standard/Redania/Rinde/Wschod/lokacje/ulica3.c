#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;

void create_rinde_street() 
{
	set_short("Uliczka przy furtce");
	set_long("Uliczka ta prowadzi do ma^lej furtki z "
	    + "kt^orej korzystaj^a najpewniej z sobie tylko "
	    + "znanych powod^ow ^zolnierze pobliskiego garnizonu.\n");

	add_exit("ulica1.c","po^ludniowy-zach^od");
//	add_exit("aaaaa","furtka");

	add_prop(ROOM_I_INSIDE,0);

}
public string
exits_description() 
{
    return "Ulica prowadzi na po^ludniowy-zach^od, znajduje si^e tu tak^ze "+
           "furtka w murze.\n";
}
	
