/* Lokacja z dzieciakami ^_^
 * Tinardan & Lil 22.08.2005 */
// uwaga: z piekarni widac dzieciaki na tej lokacji, so be wary... ;)

#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;
inherit "/lib/peek";

void create_rinde_street() 
{
	set_short("Wschodnia ulica");
	set_long("@@dlugi_opis@@");

	add_exit("ulica4.c","zach^od");
	add_exit("bramae.c","wsch^od");

	add_sit("na bruku","na bruku","z bruku",0);
	add_prop(ROOM_I_INSIDE,0);


	add_item("szyld",
           "Dzie�o jakiego� niezbyt utalentowanego stolarza przedstawia "+
	   "nieco krzywo wyrze�bion� bu�eczk� i czerwony, grawerowany napis "+
	   "\"Piekarnia Grubego Berta\". �a�cuchy, kt�rymi szyld zosta� "+
	   "przymocowany, poskrzypuj� cicho na wietrze.\n");
	if(!dzien_noc())
	{add_item("wystaw�",
	   "Za szyb� rozci�ga si� �wiat dzieci�cych marze�. Mo�na znale�� "+
	   "tam wszystko: od s�odkich bu�eczek pocz�wszy, przez ciastka "+
	   "owocowe, �wie�utkie chleby, placuszki i ptysie. Szyba upstrzona "+
	   "jest odciskami dzieci�cych nosk�w, palc�w i r�czek.\n");
	 add_peek(({"przez okno wystawowe","przez okno"}),
			 WSCHOD_LOKACJE + "piekarnia.c"); }
	
    add_object(WSCHOD_DRZWI+"do_piekarni");
    add_object(RINDE+"przedmioty/lampa_uliczna");

    add_object(RINDE_NPC+"dzieciak4");
}
public string
exits_description() 
{
    return "Ulica prowadzi na zach^od i wsch^od w stron^e bramy miasta, "+
           "znajduje si^e tu tak^ze wej^scie do piekarni.\n";
}
void init()
{
    ::init();
    init_peek();
}

string
dlugi_opis()
{
	string str;
	
	if(!dzien_noc()) /*czyli: jesli dzien :P */
	{
	str = "Gwar rozm�w, skrzypienie woz�w i stukot but�w spiesz�cych "+
	"gdzie� ludzi - wszystko to wype�nia ca�� ulic� i przelewa si� "+
	"przez jej brzegi. Nawet brukowe kamienie zdaj� si� wibrowa� pod "+
	"ci�arem dziesi�tek przechodni�w. Kolorowy t�um niesie ze sob� "+
	"wszystkich: umorusane dzieci, przygarbionych, starych �ebrak�w, "+
	"zaaferowane mieszczki i przekupki, rzemie�lnik�w i kupc�w. ";
	/*�obuzerski jasnow�osy ch�opczyk
	 *Jasnow�osy grubiutki ch�opczyk
	 *�liczna rudow�osa dziewczynka
	 *czarnooka roze�miana dziewczynka
	 */
	
	if(jest_rzecz_w_sublokacji(0, "ma�a umorusana dziewczynka") &&
	   jest_rzecz_w_sublokacji(0, "�obuzerski jasnow�osy ch�opczyk") ||
	   jest_rzecz_w_sublokacji(0, "jasnow�osy grubiutki ch�opczyk") &&
	   jest_rzecz_w_sublokacji(0, "�liczna rudow�osa dziewczynka"))
	{
		str += "Rozkrzyczana grupka dzieciak�w t�oczy si� przed "+
		"szerok� szyb� wystawow� p�nocnego budynku. Rozchichotane, "+
		"dzieci przekrzykuj� si� nawzajem i pokazuj� sobie co� "+
		"palcami. Ponad ich g�owami wisi elegancki, drewniany szyld.";
	}
	}
	else
	{
	str = "Wra�enie spokoju i ciszy czaj�cej si� na tej ulicy jest "+
	"bardzo ulotne. Gwar g�os�w jest mo�e nieco przyt�umiony, gdy� "+
	"dochodzi zza �cian okolicznych budynk�w, ale wida�, �e aleja "+
	"�yje w�asnym, nocnym �yciem. �rodkiem ulicy co jaki� czas przechodzi "+
	"spokojnym krokiem jaki� przechodzie�, a pod murami przemykaj� si� "+
	"cienie tych w�drowc�w, kt�rzy nie chc� by� widziani. Na p�nocy "+
	"widniej� zamkni�te drzwi jakiego� budynku, a wisz�cy ponad nimi "+
	"szyld poskrzypuje cicho na lekkim, nocnym wietrze.";
	}
	
str +="\n";
return str;
}
