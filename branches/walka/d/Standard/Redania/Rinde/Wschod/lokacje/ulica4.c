/* Opis by Alcyone.
 * Lil. 22.08.2005 */

#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;

void
create_rinde_street() 
{
    set_short("Wschodnia ulica");
    set_long("@@dlugi_opis@@");
    add_exit("ulica2.c","p^o^lnoc");
    add_exit("ulica6.c","po^ludnie");
    add_exit(CENTRUM_LOKACJE + "placse.c","zach^od");
    add_exit("ulica5.c","wsch^od");
	
    add_object(WSCHOD_DRZWI+"do_garnizonowej");
    add_object(WSCHOD_OBIEKTY+"tabliczka_karczmy");
    add_sit("na bruku","na bruku","z bruku",0);
    add_sit("pod �cian�","pod �cian�","spod �ciany",0);
	
    add_object(RINDE+"przedmioty/lampa_uliczna");
}

public string
exits_description() 
{
    return "Ulica prowadzi na p^o^lnoc, po^ludnie i wsch^od, na zachodzie "+
           "znajduje si^e plac. Jest tutaj tak^ze wej^scie do karczmy.\n";
}

// TODO: doda� eventy
// TODO: doda� livinga: myszk�
/*	
eventy:
*Pod murem przebiegla mala biala myszka.
*Z karczmy dochodza glosne spiewy.
*Dookola unosi sie przyjemny zapach jedzenia dochodzacego od strony karczmy.
*Spod murow dochodzi cie cichy pisk.

/ob myszke
Malenka myszka, ktorej futerko bylo niegdys biale teraz wyglada na
jedno wielkie skupisku kurzu i brudu. Przybrudzona i wychudzona
popiskuje cichutko i rozglada sie w poszukiwaniu jakiegos zagrozenia.
*/


string
dlugi_opis()
{
	string str;


	if(dzien_noc())
	{
	str = "Uliczka, ktor� o tej porze odwiedza ju� "+
	"mniej zagubionych osob, rozga��zia si� na cztery kierunki oraz "+
	"prowadzi do sporych rozmiar�w budynku, z kt�rych ci�gle "+
	"dochodz� jakie� �piewy i niezrozumia�e wrzaski. Od strony "+
	"karczmy unosi si� zach�caj�cy zapach jedzenia, kt�ry tylko "+
	"wabi potencjalnych klient�w. Na p�nocy mo�na dostrzec zarysy "+
	"maluj�cych si� dom�w i wi�kszych zabudowa�. Uliczka ci�gn�ca "+
	"si� w kierunku wschodnim doprowadzi do bram miasta, pi�trzacych "+
	"si� znad ma�ych dom�w. Pod��aj�c na zach�d mo�na doj�� do "+
	"placu g��wnego na kt�rym nie panuje teraz tak wielki t�ok "+
	"jak za dnia. Na po�udniu wznosi si� do�� sporych rozmiar�w "+
	"budynek.\n";
	}
	else
	{
	str = "Zat�oczona uliczka na kt�rej panuje wprost niewyobra�alny "+
	"�cisk, rozga��zia si� na cztery kierunki oraz prowadzi do "+
	"sporych rozmiar�w budynku, z kt�rych stale dochodz� jakie� "+
	"�piewy i niezrozumia�e wrzaski. Od strony karczmy unosi si� "+
	"zach�caj�cy zapach jedzenia, kt�ry tylko wabi przechodz�ce "+
	"osoby. Na p�nocy mo�na dostrzec zarysy maluj�cych si� dom�w i "+
	"wi�kszych zabudowa�. Uliczka ci�gn�ca si� w kierunku wschodnim "+
	"doprowadzi do bram miasta, pi�trz�cych si� znad ma�ych dom�w. "+
	"Pod��aj�c na zach�d mo�na doj�� do placu g��wnego, z kt�rego "+
	"dochodz� dra�ni�ce krzyki targuj�cych si� przekupek. Na po�udniu "+
	"wznosi si� jaki� wi�kszy budynek. Na drzwiach karczmy przybito "+
	"drewnian� tabliczk�, na kt�rej jest co� napisane.\n";
	}

	return str;
}
