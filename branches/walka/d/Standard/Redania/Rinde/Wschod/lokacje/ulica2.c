#include <stdproperties.h>
#include <macros.h>
#include "dir.h"

inherit RINDE_STD;

void create_rinde_street() 
{
    set_short("Ulica");

    add_exit("ulica1.c","p^o^lnoc");
    add_exit("ulica4.c","po^ludnie");

    add_prop(ROOM_I_INSIDE,0);

    add_object(GARNIZON_DRZWI + "wyjscie-przedsionek");
}

public string
exits_description() 
{
    return "Ulica prowadzi na p^o^lnoc i po^ludnie, jest tu tak^ze "+
           "wej^scie do garnizonu.\n";
}

string
opis_wrot()
{
  return "Pot�ne wrota prowadz�ce do garnizonu s� " +
    (present("wrota", TO)->query_open() ? "szeroko otwarte" : "zamkni�te");
}

string
dlugi_opis()
{
    string str;

    str = "Ze wszystkich stron otacza ci� ciasna zabudowa. Odrapane kamieniczki pomimo " +
	"tego, �e niewysokie, zas�aniaj� widok z ka�dej strony. W�ska niebrukowana " +
	"uliczka prowadzi z po^ludnia na p�noc, jednak tworzy w tym miejscu kilka " +
	"lekkich zakr�t^ow i nie jeste� w stanie dostrzec co znajduje si� dalej " +
	"w obu kierunkach. Kilka widocznych st�d pobliskich w�skich otwor�w bram " +
	"przyci�ga uwag� ziej�ca ze �rodka ciemn� pustk� i nieprzyjemnym zapachem " +
	"odchod�w. �ciany budynk�w w niekt�rych miejscach s� pobazgrane farb�, " +
	"a gdzieniegdzie odpadaj�ce tynki ods�aniaj� skryt� pod spodem czerwie� " +
	"ceglanych �cian. Jeden z budynk�w na wschodzie jest znacznie wi�kszy i solidniej " +
	"wygl�daj�cy od reszty. W jego oknach zauwa�asz jakie� pr�ty, s�u��ce " +
	"zapewne jako zabezpieczenie, co od razu przywodzi na my�l wi�zienie lub " +
	"jak�� budowl� militarn�. " + opis_wrot() + ".\n";
    return str;
}
