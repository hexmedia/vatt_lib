#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;

void
create_rinde_street() 
{
	set_short("Ulica");

	set_long("Niewielkie skrzy�owanie w p�nocno wschodniej cz�ci miasta " +
		"otacza zwarta zabudowa kamieniczek. S� one niewysokie, podobne do " +
		"siebie, zniszczone i odrapane. " +
		"Lekki wiaterek unosi ze sob� zapach rynsztoka. Jedna z ulic, " +
		"brukowana, biegnie w kierunku p�nocno-zachodnim, wprost ku " +
		"p�nocnej bramie miasta. Druga, utwardzona, prowadzi na po�udnie, " +
		"wzd�u� muru miejskiego garnizonu. Niedaleko na zachodzie wida� " +
		"fragment du�ego placu, a nad dachami kamienic g�ruje strzelista " +
		"ratuszowa wie�a.\n");

	add_exit(POLNOC_LOKACJE + "ulica6.c","p^o^lnocny-zach^od");
	//add_exit("ulica3.c","p^o^lnocny-wsch^od");
	add_exit(WSCHOD_LOKACJE + "ulica2.c","po^ludnie");
	add_exit(CENTRUM_LOKACJE + "placne.c","zach^od");

	add_prop(ROOM_I_INSIDE,0);

}
public string
exits_description() 
{
    return "Ulica prowadzi na p^o^lnocny-zach^od i "+
           "po^ludnie, na zachodzie wida^c plac.\n";
}
	
	
