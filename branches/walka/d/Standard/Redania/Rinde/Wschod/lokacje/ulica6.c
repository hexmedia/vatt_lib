#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;

void create_rinde_street() 
{
	set_short("Boczna uliczka");
	set_long("Ma�o ucz�szczana, w�ska uliczka prowadzi od placu ratuszowego "
	    + "do mniejszego placyku w po�udniowej cz�ci miasta.\n");

	add_exit("ulica4.c","p�noc");
	add_exit("ulica7.c","po�udnie");

	add_prop(ROOM_I_INSIDE,0);

}
public string
exits_description() 
{
    return "Ulica prowadzi na p^o^lnoc i po^ludnie.\n";
}


/*
U14

Short: w�ska brukowana uliczka

Long: Domy po obu stronach ciasno zamykaj� si� nad drog�, tak �e niebo jest prawie niewidoczne.

*/