/*
 * Jest do klucz do drzwi do piekarni w Rinde.
 * 23.08.2005 Lil */

inherit "/std/key";
#include "dir.h"

#define KOD_KLUCZA "drzwi_do_piekarni_rinde"

#include <stdproperties.h>
#include <pl.h>
#include <materialy.h>

void
create_key()
{
    ustaw_nazwe( "klucz" );

    set_long("Ca�kiem sporawy, stalowy klucz, z do�� skomplikowanym " +
       "wzorem wci��.\n");
    
    dodaj_przym("stalowy", "stalowi");
    dodaj_przym("niebieski","niebiescy");
    ustaw_material(MATERIALY_MOSIADZ);
    
    set_key(KOD_KLUCZA);

set_owners(({RINDE_NPC + "piekarz"}));
}
