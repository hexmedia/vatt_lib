inherit "/std/door";

#include <pl.h>
#define KOD_DRZWI "piekarnia_rinde"
#define KOD_KLUCZA "drzwi_do_piekarni_rinde"

#include "dir.h"

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami", 
        "drzwiach" }), PL_NIJAKI_OS); 
        
    dodaj_przym("drewniany", "drewniani");
    
    set_other_room(WSCHOD_LOKACJE + "piekarnia.c");
    set_door_id(KOD_DRZWI);
    
    set_door_desc("Drewniane drzwi prowadz� do piekarni, o czym �wiadczy� "+
		    "mo�e chocia�by szyld  poskrzypuj�cy na wietrze.\n");
    
    set_open_desc("Otwarte drewniane drzwi zapraszaj� do piekarni.\n");
    set_closed_desc("Drzwi piekarni s� zamkni�te.\n");
        
    set_pass_command(({"piekarnia","przez drewniane drzwi do piekarni",
                         "z zewn�trz"}));
    //set_pass_mess("przez drzwi do piekarni");

    set_key(KOD_KLUCZA);
    set_lock_name("zamek");
    set_lock_command("zamknij");                                               
    set_unlock_command("otw�rz");


}
