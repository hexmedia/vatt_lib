// Delvert, na podstawie menu z Baszty by Lil ;)

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <mudtime.h>
#include <materialy.h>

int przeczytaj_menu();

create_object()
{
    string str;

    ustaw_nazwe(({"tabliczka","tabliczki","tabliczce","tabliczk^e","tabliczk^a","tabliczce"}),
               ({"tabliczki","tabliczek","tabliczkom","tabliczki","tabliczkami","tabliczkach"}),
                  PL_ZENSKI);

    set_long("@@zmienny_long@@");
    ustaw_material(MATERIALY_DR_DAB);

    add_prop(OBJ_I_WEIGHT, 485);
    add_prop(OBJ_I_NO_GET, "Tabliczka zosta^la mocno przybita do ^sciany.\n");
    add_prop(OBJ_I_DONT_GLANCE, 1);

    add_cmd_item(({"menu","tabliczk^e"}), "przeczytaj", przeczytaj_menu,
                   "Przeczytaj co?\n"); 
}

int
przeczytaj_menu()
{
   return long();
}

string
zmienny_long()
{
    string str;

    str = "     _________________________________________\n    / o\t\t       MENU\t\t    o \\\n   |"+
//                      "\t\tDania g^l^owne:\t\t      |\n"+
                      "    Chleb\t\t\t5 groszy      |\n"+
                      "   |\tS�odka bu�ka\t\t4 grosze      |\n"+
		      "   |\tRogalik z czekolad�\t6 groszy      |\n";
    if (environment(this_object())->pora_roku() == MT_LATO || environment(this_object())->pora_roku() == MT_JESIEN)
	                str += "   |\tCiasto wi�niowe\t\t8 groszy      |\n";
    if (environment(this_object())->pora_roku() == MT_JESIEN)
	                str += "   |\tPlacuszek z jab�kami\t5 groszy      |\n";
    if (environment(this_object())->pora_roku() == MT_JESIEN || environment(this_object())->pora_roku() == MT_ZIMA)
	    		str += "   |\tPlacuszek z konfiturami\t7 groszy      |\n";
    if (environment(this_object())->pora_roku() == MT_LATO)
                	str += "   |\tCiastko z malinami\t6 grosze      |\n";
    if (environment(this_object())->pora_roku() == MT_WIOSNA)
	                str += "   |\tCiastko poziomkowe\t6 groszy      |\n";
    if (environment(this_object())->pora_roku() == MT_WIOSNA || environment(this_object())->pora_roku() == MT_LATO)
	                str += "   |\tCiastko truskawkowe\t7 groszy      |\n";
    str += "   |\tPty� z kremem\t\t5 groszy      |\n"+
		      "   |\tSernik\t\t\t5 groszy      |\n"+
	"   | o\t\t           \t\t    o |\n    \\________________________________________/\n"; 

    return str;
}