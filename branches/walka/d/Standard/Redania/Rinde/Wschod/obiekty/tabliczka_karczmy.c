/* Tabliczka przybita na drzwiach karczmy garnizonowej z Rinde.
   Lil 22.08.2005               */

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>

int dlugi_opis();

int
create_object()
{
	ustaw_nazwe("tabliczka");
	dodaj_przym("drewniany","drewniani");
	set_long("Na tabliczce jest napisane du�ymi literami:\n"+
	"KARCZMA GARNIZONOWA ZAPRASZA!\n"+
	"Ni�ej dopisano krzywymi literami: Nieludziom wst�p wzbroniony!\n");
	add_prop(OBJ_I_NO_GET, "Tabliczka zosta^la bardzo mocno przybita do drzwi. Ani drgnie.\n");
    add_prop(OBJ_I_DONT_GLANCE,1);
    ustaw_material(MATERIALY_DR_DAB);
    add_prop(OBJ_I_WEIGHT,606);
    add_prop(OBJ_I_VOLUME,879);

  add_cmd_item("tabliczk�", "przeczytaj", dlugi_opis, "Przeczytaj co?\n");
}
int dlugi_opis()
{
	write(this_object()->long());
	return 1;
}
