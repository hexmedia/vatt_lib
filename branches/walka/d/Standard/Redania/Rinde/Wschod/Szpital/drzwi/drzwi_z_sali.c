/* Autor: Avard
   Data : 20.03.07 
   Info : Z sali operacyjnej */

inherit "/std/door";

#include <pl.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("oszklony","oszkleni");
    dodaj_przym("drewniany", "drewniani");
    set_other_room(SZPITAL_LOKACJE + "poczekalnia.c");
    set_door_id("DRZWI_NA_SALE_OPERACYJNA_RINDE");
    set_door_desc("Do^s^c solidnie wykonane, drewniane drzwi. Aby ulatwi^c "+
        "personelowi szpitalnemu wwiezienie pacjenta na sal^e operacyjn^a "+
        "drzwi sk^ladaj^a si^e z dw^och skrzyde^l, oba s^a lekko "+
        "przeszklone oraz zaopatrzone w metalowe ga^lki.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({({"wyj^scie","poczekalnia"}),
     "przez oszklone drewniane drzwi do poczekalni",
     "przez oszklone drewniane drzwi z poczekalni"}));

    set_lock_command("zamknij");
    set_unlock_command("otw^orz");

    set_key("KLUCZ_DRZWI_DO_SZPITALA");
    set_lock_name("zamek");
    
    set_type(O_INNE);
    set_open(0);
}
