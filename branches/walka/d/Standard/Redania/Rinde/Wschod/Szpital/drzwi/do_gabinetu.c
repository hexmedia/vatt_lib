inherit "/std/door";

#include <pl.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");

    dodaj_przym("drewniany", "drewniani");

    set_other_room(SZPITAL_LOKACJE + "gabinet.c");

    set_door_id("ale co");

    set_door_desc("Drewniane drzwi z metalow� ga�k�.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({({"gabinet","wejd^x do gabinetu","gabinet medyka",
     "wejd^x do gabinetu medyka","drzwi do gabinetu",
     "drzwi do gabinetu medyka","przejd^x przez drzwi do gabinetu",
     "przejd^x przez drzwi do gabinetu medyka"}),
     "przez drewniane drzwi do gabinetu medyka",
     "przez drewniane drzwi z poczekalni"}));

    set_lock_command("zamknij");
    set_unlock_command("otw�rz");
    
    set_lock_mess( ({ "przekr�ca klucz w du�ym zamku, co spowodowa�o zasuni�cie ogromnej zasuwy.\n",
          "S�yszysz szcz�k przesuwanej zasuwy.\n", "Przekr�casz klucz w du�ym zamku, co spowodowa�o " +
          "zasuni�cie ogromnej zasuwy.\n" }));

    set_unlock_mess( ({ "przekr�ca klucz w du�ym zamku, co spowodowa�o odsuni�cie ogromnej zasuwy.\n",
          "S�yszysz szcz�k przesuwanej zasuwy.\n", "Przekr�casz klucz w du�ym zamku, co spowodowa�o " +
          "odsuni�cie ogromnej zasuwy.\n" }));

    set_key("ale co?");
    set_lock_name("zamek1a");
    
    set_type(O_INNE);
    set_open(0);
    set_locked(1);
}
