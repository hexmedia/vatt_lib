/* Autor: Avard
   Data : 20.03.07 */

inherit "/std/door";

#include <pl.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("du^zy","duzi");
    dodaj_przym("drewniany", "drewniani");
    set_other_room(SZPITAL_LOKACJE + "poczekalnia.c");
    set_door_id("DRZWI_DO_SZPITALA");
    set_door_desc("Du^ze, drewniane drzwi.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({({"szpital"}),
     "przez du^ze drewniane drzwi do szpitala",
     "przez du^ze drewniane drzwi z ulicy"}));

    set_lock_command("zamknij");
    set_unlock_command("otw^orz");

    set_key("KLUCZ_DRZWI_DO_SZPITALA");
    set_lock_name("zamek");
    
    set_type(O_INNE);
    set_open(1);
}
