inherit "/std/room";

#include <stdproperties.h>
#include "dir.h"

void create_room() 
{
    set_short("Gabinet medyka");
    set_long("Zupe�nie pusty pok�j w kt�rym "
            + "zapewne kiedy� rezydowa� tutejszy lekarz. Pod�og� pokrywa " 
            + "gruba warstwa kurzu, jednak w powietrzu wci�� unosi si� "
            + "s�aby zapach medykament�w.\n");

    add_object(SZPITAL_DRZWI+"z_gabinetu");

    add_prop(ROOM_I_INSIDE,1);
}
public string
exits_description() 
{
    return "Wyj^scie z gabinetu prowadzi do poczekalni.\n";
}

	
	
