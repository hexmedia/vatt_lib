/* Made by Avard 10.06.06 */
inherit "/std/room";

#include <stdproperties.h>
#include "dir.h"

void create_room() 
{
    set_short("Sala sypialna.");
    add_exit("poczekalnia.c","p^o^lnoc");

    add_object(WSCHOD_OBIEKTY +"tabliczka_szpital_sala.c");  
    dodaj_rzecz_niewyswietlana("niewielka drewniana tabliczka", 1);
    add_object(MEBLER +"prycza_duza_twarda.c", 8);
    dodaj_rzecz_niewyswietlana("du^za twarda prycza", 8);


    add_prop(ROOM_I_INSIDE,1);
}
public string
exits_description() 
{
    return "Wyj^scie z sali sypialnej na p^o^lnocy prowadzi do poczekalni.\n";
}


string
dlugi_opis()
{
    string str;
    int i, il, il_prycz;
    object *inwentarz;
   
    inwentarz = all_inventory();
    il_prycz = 0;
    il = sizeof(inwentarz);
   
    for (i = 0; i < il; ++i) {
         if (inwentarz[i]->jestem_prycza_szpitalna()) {
                        ++il_prycz; }}
    
    str = "Sporej wielko^sci pomieszczenie jest ";
    if (!dzien_noc())
    {
       str += "jasne dzi^eki umieszczeniu dw^och du^zych okien na "+
           "zachodniej ^scianie. ";
    }
    else
    {
       str += "dosy^c jasne, dzi^eki blaskowi ksi^ezyca wpadaj^acego "+
           "przez dwa du^ze okna umieszczone na zachodniej ^scianie. ";
    }
    if (il_prycz == 0)
    {
       str += "O dziwo sala jest zupe^lnie pusta, ale mimo tego kto^s "+
           "zadba^l o to by sala byla utrzymana w czysto^sci. "+
           "Posadzka jest wypolerowana, a ^sciany pomalowano bia^la farb^a. ";
    }
    if (il_prycz == 1)
    {
       str += "Stoi pod nimi tylko jedna prycza przygotowana dla pacjenta. ";
    }
    if (il_prycz == 2)
    { 
       str += "Stoj^a pod nimi dwie prycze przygotowane dla pacjent^ow. ";
    }
    if (il_prycz == 3)
    {
       str += "Stoj^a pod nimi trzy prycze przygotowane dla pacjent^ow. ";
    }
    if (il_prycz == 4)
    {
       str += "Stoj^a pod nimi cztery prycze przygotowane dla pacjent^ow. "; 
    }
    if (il_prycz == 5)
    {
       str += "Stoj^a pod nimi cztery prycze przygotowanye dla pacjent^ow, "+
           "za^s po przeciwnej stronie stoi jeszcze jedna. ";
    }
    if (il_prycz == 6)
    {
       str += "Stoj^a pod nimi cztery prycze przygotowanye dla pacjent^ow, "+
           "za^s po przeciwnej stronie znajduj^a si^e jeszcze dwie. ";
    }
    if (il_prycz == 7)
    {
       str += "Stoj^a pod nimi cztery prycze przygotowanye dla pacjent^ow, "+
           "za^s po przeciwnej stronie znajduj^a si^e jeszcze trzy. ";
    }
    if (il_prycz == 8)
    {
       str += "Stoj^a pod nimi cztery prycze przygotowanye dla pacjent^ow, "+
           "za^s po przeciwnej stronie znajduj^a si^e jeszcze cztery. ";
    }
    if (il_prycz > 0)
    {
       str += "Kto^s zadba^l o to by sala byla utrzymana w czysto^sci, "+
           "posadzka jest wypolerowana, a ^sciany pomalowano bia^la farb^a. ";
    }
       str += "Na lewo od wej^scia kto^s przybi^l do niej niewielk^a "+
           "tabliczke.\n";

        return str;
}
    
