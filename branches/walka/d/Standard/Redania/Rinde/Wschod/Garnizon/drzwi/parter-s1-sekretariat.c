inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <materialy.h>
#include <object_types.h>

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami",
        "drzwiach" }), PL_NIJAKI_OS);

    dodaj_przym("prosty", "pro�ci");

    set_other_room(GARNIZON_LOKACJE + "sekretariat.c");

    set_door_id(KOD_DRZWI_Se_S);

    set_door_desc("Proste, drewniane drzwi zbite z d�bowych, grubych desek. Przyczepiona " +
                  "jest do nich tabliczka z napisem 'Sekretariat'.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command( ({"sekretariat", "przez drewniane drzwi do sekretariatu", "z korytarza"}));

    set_lock_command("zamknij");
    set_unlock_command("otw�rz");
    
    set_key(KOD_KLUCZA_Se_S);
    set_lock_name("zamek");
    
    ustaw_material(MATERIALY_STAL, 1);
    ustaw_material(MATERIALY_DR_DAB, 99);
    set_type(O_INNE);
}
