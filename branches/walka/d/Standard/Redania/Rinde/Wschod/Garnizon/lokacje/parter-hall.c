inherit "/std/room";

#include <stdproperties.h>
#include <object_types.h>
#include "dir.h"
#include <pl.h>
#include <macros.h>

#define SUBLOC_SCIANA ({"�ciana", "�ciany", "�cianie", "�cian�", "�cian�", "�cianie", "na"})

void
create_room() 
{
    set_short("Korytarz");

    add_exit("parter-s1.c","po�udnie");

    add_prop(ROOM_I_INSIDE,1);

    add_object(GARNIZON_DRZWI + "parter-hall-przedsionek");
    add_object(GARNIZON_DRZWI + "parter-hall-n1");

    add_subloc(SUBLOC_SCIANA);

    add_subloc_prop("�ciana", CONT_I_CANT_ODLOZ, 1);
    add_subloc_prop("�ciana", CONT_I_CANT_POLOZ, 1);
    add_subloc_prop("�ciana", SUBLOC_I_MOZNA_POWIES, 1);
    add_subloc_prop("�ciana", SUBLOC_I_DLA_O, O_SCIENNE);
    add_subloc_prop("�ciana", CONT_I_MAX_RZECZY, 2);

    add_object(GARNIZON_OBIEKTY + "tablica", 1, 0, SUBLOC_SCIANA);
    add_object(MEBLE + "lawka_prosta_drewniana", 2);
}

int
policz_rzeczy(object* arr, string nazwa)
{
 int i, il, ile;

  ile = 0;
  il = sizeof(arr);
  for (i = 0; i < il; ++i) {
    if (arr[i]->short() ~= nazwa) {
      ++ile;
    }
  }
  return ile;
}

string
opis_drzwi1()
{
  return "Wykonane z grubych, metalowych pr�t�w okratowane drzwi, " +
    "stoj�ce po zachodniej stronie pomieszczenia, s� " +
    (filter(all_inventory(TO), &->is_drzwi_parter_hall_przedsionek())[0]->query_open() ?
     "otwarte" : "zamkni�te");
}

string
opis_drzwi2()
{
  return "W p�nocny portal wprawione s� " +
    (filter(all_inventory(TO), &->is_drzwi_parter_hall_n1())[0]->query_open() ? "otwarte" : "zamkni�te") +
    " okratowane drzwi wykonane z grubych, metalowych pr�t�w. ";
}

string
opis_na_scianie()
{
  object *obarr = subinventory("�ciana");

  if (sizeof(obarr) != 0) {
    return "Na �cianie " + ilosc(policz_rzeczy(obarr, obarr[0]->short()), "wisi", "wisz�", "wisi") + " " + COMPOSITE_DEAD(obarr, PL_MIA) + ". ";
  }
  return "";
}
	
string
dlugi_opis()
{
		string str= "Szeroki, niezbyt dobrze utrzymany korytarz. Na suficie wida� ciemne plamy " +
                "po przeciekaj�cej niegdy� wodzie. Na �cianach trzymaj� si� tylko resztki, kiedy� " +
                "zapewne bia�ego, cienkiego tynku. " + opis_drzwi1() + ". Korytarz ci�gnie si� " +
                "w kierunku po�udniowym. " + opis_drzwi2() + opis_na_scianie() + "\n";

		// TODO: jacy� stra�nicy
		
		return str;
}
