inherit "/std/room";

#include <stdproperties.h>
#include <pl.h>
#include "dir.h"
#include <mudtime.h>
#include <object_types.h>
#include <composite.h>
#include <macros.h>

void
create_room() 
{
    set_short("Dy�urka");

    add_prop(ROOM_I_INSIDE,1);

    dodaj_rzecz_niewyswietlana_plik(GARNIZON_OBIEKTY + "polka");

    set_niewyswietlane_mask(O_MEBLE);

    add_object(GARNIZON_OBIEKTY + "polka.c");
    add_object(GARNIZON_DRZWI + "parter-dyzurka-n1");

    add_object(MEBLER + "maly_stol.c");
    add_object(MEBLER + "taboret.c", 2);
    add_object(MEBLER + "szafa.c", 2);
}

string
za_oknem()
{
  switch (pora_dnia()) {
    case MT_SWIT:
    case MT_WCZESNY_RANEK:
      return "ulice miasta budz�ce si� do �ycia wraz ze wschodem s�o�ca";
    case MT_RANEK:
      return "sk�pane w porannym s�o�cu ulice miasta";
    case MT_POLUDNIE:
    case MT_POPOLUDNIE:
      return "wisz�ce wysoko s�o�ce o�wietlaj�ce ulice miasta";
    case MT_WIECZOR:
    case MT_POZNY_WIECZOR:
      return "ulice miasta pod szarzej�cym z ka�d� chwil� niebem";
    case MT_NOC:
      return "pogr��one w ciemno�ciach miasto";
  }
}

int
type_filter(object ob, int typ)
{
  return (ob->query_type() == typ);
}

int
policz_rzeczy(object* arr, string nazwa)
{
 int i, il, ile;

  ile = 0;
  il = sizeof(arr);
  for (i = 0; i < il; ++i) {
    if (arr[i]->short() ~= nazwa) {
      ++ile;
    }
  }
  return ile;
}

string
opis_mebli()
{
  object *obarr = filter(all_inventory(), &type_filter(, O_MEBLE));
  
  if (sizeof(obarr) == 0) {
    return "W pomieszczeniu tym nie widzisz �adnych mebli";
  }
  return "Na pod�odze " + ilosc(policz_rzeczy(obarr, obarr[0]->short()), "stoi", "stoj�", "stoi") + " " + COMPOSITE_DEAD(obarr, PL_MIA);
}

string
opis_drzwi()
{
  return "We wschodniej �cianie widniej� " +
    (present("drzwi", TO)->query_open() ? "otwarte" : "zamkni�te") +
    " drewniane drzwi";
}

string
dlugi_opis()
{
  string str = "Ma�e pomieszczenie ze sporych rozmiar�w zakratowanym oknem, " +
    "za kt�rym wida� " + za_oknem() + ". W po�udniowej �cianie znajduje " +
    "si� niedu�y otw�r z wprawion� metalow� krat�. W jej dolnej cz�ci " +
    "stalowe pr�ty u�o�one s� znacznie rzadziej, umo�liwiaj�c prze�o�enie " +
    "przeze� nieco wi�kszych przedmiot�w. Pod okienkiem tym, wmurowana " +
    "w �cian�, wyr�nia si� marmurowa p�ka, mocno ju� powycierana. " + opis_mebli() +
    ". Spod resztek odpadaj�cego tynku na �cianach wy�aniaj� si� czerwone, palone ceg�y. " +
    opis_drzwi() + ".\n";

  // TODO: doda� stra�nika

  return str;
}
