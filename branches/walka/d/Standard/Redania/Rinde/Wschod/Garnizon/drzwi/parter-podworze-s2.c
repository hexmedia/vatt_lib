inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <materialy.h>
#include <object_types.h>

void
create_door()
{
  ustaw_nazwe("wrota");

  dodaj_przym("drewniany", "drewniani");

  set_other_room(GARNIZON_LOKACJE + "parter-s2.c");

  set_door_id(KOD_DRZWI_P_SP);

  set_door_desc("Grube, drewniane dechy zbite s� niezwykle r�wno. Przytwierdzone du�ymi gwo�dziami " +
      "szerokie belki dodatkowo wzmacniaj� jeszcze te solidne wrota. " +
      "Ca�o�� wisi na trzech ogromnych, metalowych zawiasach.\n");

  set_open_desc("");
  set_closed_desc("");

  set_pass_command( ({"po�udnie", "przez drewniane drzwi do po�udniowego skrzyd�a garnizonu", "z podw�rza"}));

  set_lock_command("zamknij");
  set_unlock_command("otw�rz");

  set_key(KOD_KLUCZA_P_SP);
  set_lock_name("zamek");

  ustaw_material(MATERIALY_STAL, 5);
  ustaw_material(MATERIALY_DR_DAB, 95);
  set_type(O_INNE);
}

public int
is_drzwi_parter_podworze_s2()
{
    return 1;
}
