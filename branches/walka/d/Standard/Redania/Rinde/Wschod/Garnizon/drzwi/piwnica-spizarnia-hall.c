inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <materialy.h>
#include <object_types.h>

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami",
        "drzwiach" }), PL_NIJAKI_OS);

    dodaj_przym("prosty", "pro�ci");

    set_other_room(GARNIZON_LOKACJE + "piwnica-hall.c");

    set_door_id(KOD_DRZWI_P_S);

    set_door_desc("Proste, drewniane drzwi zbite z d�bowych, grubych desek. Przyczepiona " +
                  "jest do nich tabliczka z napisem 'Wyj�cie'.\n");

    set_open_desc("W zachodniej �cianie widniej� otwarte drewniane drzwi.\n");
    set_closed_desc("W zachodniej �cianie widniej� zamkni�te drewniane drzwi.\n");

    set_pass_command( ({"wyj�cie","przez drewniane drzwi na korytarz",
                      "z"}));
    //set_pass_mess("przez drewniane drzwi na korytarz");

    set_lock_command("zamknij");
    set_unlock_command("otw�rz");
    
    set_key(KOD_KLUCZA_P_S);
    set_lock_name("zamek");
    
    ustaw_material(MATERIALY_STAL, 1);
    ustaw_material(MATERIALY_DR_DAB, 99);
    set_type(O_INNE);
}
