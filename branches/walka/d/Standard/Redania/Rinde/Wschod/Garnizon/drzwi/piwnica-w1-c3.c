inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <materialy.h>
#include <object_types.h>

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami",
        "drzwiach" }), PL_NIJAKI_OS);

    dodaj_przym("okratowany", "okratowani");

    set_other_room(GARNIZON_LOKACJE + "cela3.c");

    set_door_id(KOD_DRZWI_P_C3);

    set_door_desc("Wykonane z grubych, metalowych pr�t�w drzwi wygl�daj� na " +
                  "bardzo solidne. Ogromna zasuwa wraz z du�ym zamkiem pog��biaj� " +
                  "jeszcze to wra�enie.\n");

    set_open_desc("W p�nocn� �cian� wstawione s� otwarte okratowane drzwi wykonane " +
                  "z grubych, metalowych pr�t�w.\n");
    set_closed_desc("W p�nocn� �cian� wstawione s� zamkni�te okratowane drzwi wykonane " +
                    "z grubych, metalowych pr�t�w.\n");

    set_pass_command( ({"p�noc"}));
    set_pass_mess("przez okratowane drzwi na p�noc");

    set_lock_command("zamknij");
    set_unlock_command("otw�rz");

    set_lock_mess( ({ "przekr�ca klucz w du�ym zamku, co spowodowa�o zasuni�cie ogromnej zasuwy.\n",
          "S�yszysz szcz�k przesuwanej zasuwy.\n", "Przekr�casz klucz w du�ym zamku, co spowodowa�o " +
          "zasuni�cie ogromnej zasuwy.\n" }));

    set_unlock_mess( ({ "przekr�ca klucz w du�ym zamku, co spowodowa�o odsuni�cie ogromnej zasuwy.\n",
          "S�yszysz szcz�k przesuwanej zasuwy.\n", "Przekr�casz klucz w du�ym zamku, co spowodowa�o " +
          "odsuni�cie ogromnej zasuwy.\n" }));

    set_key(KOD_KLUCZA_P_C3);
    set_lock_name("zamek");

    ustaw_material(MATERIALY_STAL);
    set_type(O_INNE);
}
