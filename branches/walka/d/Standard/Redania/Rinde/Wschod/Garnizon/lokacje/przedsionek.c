inherit "/std/room";

#include <stdproperties.h>
#include <pl.h>
#include <macros.h>
#include "dir.h"

void
create_room() 
{
    set_short("Przedsionek");

    add_prop(ROOM_I_INSIDE,1);

    add_item(({"�ciany", "bielone �ciany", "mury", "�cian�", "bielon� �cian�"}),
            "Solidnie wygl�daj�ce mury grubo pokryte bia�ym tynkiem, " +
            "kt�ry mocno ju� przybrudzony i pop�kany dobitnie �wiadczy " +
            "o czasie, kt�ry min�� od ostatniego remontu.\n");
    add_item(({"p�kni�cie", "malownicze p�kni�cie", "rysunki", "chaotyczne rysunki", "kompozycje"}),
            "Liczne, mniejsze i wi�ksze p�kni�cia ��cz� si� i rozdzielaj� " +
            "tworz�c zupe�nie nieprzewidywalne kompozycje.\n");
    add_item(({"otw�r", "okno", "okienko", "kraty", "metalowe kraty"}),
            "Dosy� du�e okienko chronione solidnie wygl�daj�cymi metalowymi kratami " +
            "posiada u do�u otwart� przestrze�.\n");

    dodaj_rzecz_niewyswietlana_plik(GARNIZON_OBIEKTY + "polka");

    add_object(GARNIZON_OBIEKTY + "polka.c");

    add_object(GARNIZON_DRZWI + "przedsionek-wyjscie");
    add_object(GARNIZON_DRZWI + "parter-przedsionek-hall");
    add_object(GARNIZON_DRZWI + "parter-s1-sekretariat");
}
public string
exits_description() 
{
    return "Znajduj^a si^e tutaj wrota prowadz^ace na zewn^atrz, drzwi do "+
        "sekretariatu oraz drzwi prowadz^ace do dalszej cz^e^sci garnizonu.\n";
}
string
opis_drzwi1()
{
    return "Pot�ne wrota prowadz�ce na zewn�trz s� " +
        (filter(all_inventory(TO), &->is_drzwi_przedsionek_wyjscie())[0]->query_open() ?
         "szeroko otwarte" : "zamkni�te");
}

string
opis_drzwi2()
{
    return "Po wschodniej stronie pomieszczenia widoczne s� " +
        (filter(all_inventory(TO), &->is_drzwi_parter_przedsionek_hall())[0]->query_open() ?
         "otwarte" : "zamkni�te") +
        " okratowane drzwi wykonane z grubych, metalowych pr�t�w";
}

string
dlugi_opis()
{
    string str = "Ma�e, ascetycznie urz�dzone pomieszczenie. Bielone " +
        "�ciany, mocno przybrudzone w niekt�rych miejscach, " +
        "nosz� �lady up�ywaj�cego czasu. Liczne p�kni�cia " +
        "malowniczo pokrywaj� ca�� ich powierzchni�, tworz�c " +
        "mniej lub bardziej chaotyczne rysunki. " + opis_drzwi1() +
        ". W p�nocnej �cianie widnieje poka�nych rozmiar�w otw�r " +
        "przys�oni�ty solidnie wygl�daj�cymi metalowymi kratami. " +
        "Pod okienkiem tym, wmurowana w �cian�, wyr�nia si� marmurowa " +
        "p�ka, mocno ju� powycierana. " + opis_drzwi2() + ".\n";

    return str;
}
