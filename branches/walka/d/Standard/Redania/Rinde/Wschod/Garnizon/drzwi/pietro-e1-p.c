inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <materialy.h>
#include <object_types.h>

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami",
        "drzwiach" }), PL_NIJAKI_OS);

    dodaj_przym("prosty", "pro�ci");

    set_other_room(GARNIZON_LOKACJE + "pietro-p1.c");

    set_door_id(KOD_DRZWI_K_P);

    set_door_desc("Proste, drewniane drzwi zbite z d�bowych, grubych desek. Na obrze�ach " +
                  "oraz w poprzek drzwi umocowane s� stalowe wzmocnienia.\n");

    set_open_desc("W p�nocnej �cianie widniej� otwarte drewniane drzwi.\n");
    set_closed_desc("W p�nocnej �cianie widniej� zamkni�te drewniane drzwi.\n");

    set_pass_command( ({"p�noc"}));
    set_pass_mess("przez drewniane drzwi na p�noc");

    set_lock_command("zamknij");
    set_unlock_command("otw�rz");
    
    set_key(KOD_KLUCZA_K_P);
    set_lock_name("zamek");
    
    ustaw_material(MATERIALY_STAL, 3);
    ustaw_material(MATERIALY_DR_DAB, 97);
    set_type(O_INNE);
}
