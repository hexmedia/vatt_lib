#pragma strict_types

inherit "/std/receptacle";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include "/sys/materialy.h"
#include "/sys/object_types.h"

nomask void
create_container()
{
  ustaw_nazwe("biurko");

  set_long("Ogromne, d�bowe biurko. Z grubsza ociosane deski zbite s� dosy� solidnie, " +
      "przez co mebel wygl�da na bardzo wytrzyma�y. Cztery grube nogi nie stoj� na " +
      "pod�odze, lecz s� w ni� jakby wbite. Biurko posiada cztery szuflady." +
      "@@opis_sublokacji| Na biurku widzisz |na|.|||" +
      PL_BIE + "@@\n");

  add_prop(CONT_I_MAX_VOLUME, 200000);
  add_prop(CONT_I_VOLUME, 80000);
  add_prop(CONT_I_WEIGHT, 90000);
  add_prop(CONT_I_MAX_WEIGHT, 200000);
  add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
  add_prop(CONT_I_RIGID, 1);
  add_prop(OBJ_I_NO_GET, "Nie da si� go podnie��.\n");
  add_prop(CONT_I_CANT_WLOZ_DO, 1);
  add_prop(CONT_I_NO_OPEN_DESC, 1);
  add_prop(CONT_I_CANT_OPENCLOSE, 1);

  add_subloc("na");
  add_subloc_prop("na", CONT_I_MAX_VOLUME, 5000);
  add_subloc_prop("na", CONT_I_MAX_WEIGHT, 50000);

  /* pierwsza szuflada */
  add_subloc(({"pierwsza szuflada", "pierwszej szuflady", "pierwszej szufladzie",
               "pierwsz� szuflad�", "pierwsz� szuflad�", "pierwszej szufladzie"}));
  add_subloc_prop("pierwsza szuflada", CONT_I_MAX_VOLUME, 10000);
  add_subloc_prop("pierwsza szuflada", CONT_I_MAX_WEIGHT, 20000);
  add_subloc_prop("pierwsza szuflada", SUBLOC_S_OB_GDZIE, "wewn�trz pierwszej szuflady");
  add_subloc_prop("pierwsza szuflada", SUBLOC_I_OB_PRZYP, PL_DOP);
  add_subloc_prop("pierwsza szuflada", CONT_M_OPENCLOSE, "pierwsz� szuflad�");
  add_subloc_prop("pierwsza szuflada", SUBLOC_I_RODZAJ, PL_ZENSKI);

  add_item(({"szuflad�", "szuflady"}), "Biurko posiada a� cztery szerokie szuflady.\n");
  add_item("pierwsz� szuflad�", "Jest to szeroka szuflada" +
      "@@opis_sublokacji| zawieraj�ca |pierwsza szuflada|.|.@@\n" +
      "@@if_subloc_closed|pierwsza szuflada|Jest zamkni�ta.\n@@");

  /* druga szuflada */
  add_subloc(({"druga szuflada", "drugiej szuflady", "drugiej szufladzie",
               "drug� szuflad�", "drug� szuflad�", "drugiej szufladzie"}));
  add_subloc_prop("druga szuflada", CONT_I_MAX_VOLUME, 10000);
  add_subloc_prop("druga szuflada", CONT_I_MAX_WEIGHT, 20000);
  add_subloc_prop("druga szuflada", SUBLOC_S_OB_GDZIE, "wewn�trz drugiej szuflady");
  add_subloc_prop("druga szuflada", SUBLOC_I_OB_PRZYP, PL_DOP);
  add_subloc_prop("druga szuflada", CONT_M_OPENCLOSE, "drug� szuflad�");
  add_subloc_prop("druga szuflada", SUBLOC_I_RODZAJ, PL_ZENSKI);

  add_item("drug� szuflad�", "Jest to szeroka szuflada" +
      "@@opis_sublokacji| zawieraj�ca |druga szuflada|.|.@@\n" +
      "@@if_subloc_closed|druga szuflada|Jest zamkni�ta.\n@@");

  /* trzecia szuflada */
  add_subloc(({"trzecia szuflada", "trzeciej szuflady", "trzeciej szufladzie",
               "trzeci� szuflad�", "trzeci� szuflad�", "trzeciej szufladzie"}));
  add_subloc_prop("trzecia szuflada", CONT_I_MAX_VOLUME, 10000);
  add_subloc_prop("trzecia szuflada", CONT_I_MAX_WEIGHT, 20000);
  add_subloc_prop("trzecia szuflada", SUBLOC_S_OB_GDZIE, "wewn�trz trzeciej szuflady");
  add_subloc_prop("trzecia szuflada", SUBLOC_I_OB_PRZYP, PL_DOP);
  add_subloc_prop("trzecia szuflada", CONT_M_OPENCLOSE, "trzeci� szuflad�");
  add_subloc_prop("trzecia szuflada", SUBLOC_I_RODZAJ, PL_ZENSKI);

  add_item("trzeci� szuflad�", "Jest to szeroka szuflada" +
      "@@opis_sublokacji| zawieraj�ca |trzecia szuflada|.|.@@\n" +
      "@@if_subloc_closed|trzecia szuflada|Jest zamkni�ta.\n@@");

  /* czwarta szuflada */
  add_subloc(({"czwarta szuflada", "czwartej szuflady", "czwartej szufladzie",
               "czwart� szuflad�", "czwart� szuflad�", "czwartej szufladzie"}));
  add_subloc_prop("czwarta szuflada", CONT_I_MAX_VOLUME, 10000);
  add_subloc_prop("czwarta szuflada", CONT_I_MAX_WEIGHT, 20000);
  add_subloc_prop("czwarta szuflada", SUBLOC_S_OB_GDZIE, "wewn�trz czwartej szuflady");
  add_subloc_prop("czwarta szuflada", SUBLOC_I_OB_PRZYP, PL_DOP);
  add_subloc_prop("czwarta szuflada", CONT_M_OPENCLOSE, "czwart� szuflad�");
  add_subloc_prop("czwarta szuflada", SUBLOC_I_RODZAJ, PL_ZENSKI);

  add_item("czwart� szuflad�", "Jest to szeroka szuflada" +
      "@@opis_sublokacji| zawieraj�ca |czwarta szuflada|.|.@@\n" +
      "@@if_subloc_closed|czwarta szuflada|Jest zamkni�ta.\n@@");

  ustaw_material(MATERIALY_DR_DAB, 98);
  ustaw_material(MATERIALY_STAL, 2);
  set_type(O_MEBLE);
}
