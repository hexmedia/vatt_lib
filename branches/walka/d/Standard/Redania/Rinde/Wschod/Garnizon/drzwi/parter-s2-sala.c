inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <materialy.h>
#include <object_types.h>

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami",
        "drzwiach" }), PL_NIJAKI_OS);

    dodaj_przym("prosty", "pro�ci");

    set_other_room(GARNIZON_LOKACJE + "sala_rozpraw.c");

    set_door_id(KOD_DRZWI_S_S);

    set_door_desc("Proste, drewniane drzwi zbite z d�bowych, grubych desek. Przyczepiona " +
                  "jest do nich tabliczka z napisem 'Sala rozpraw'.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command( ({"sala"}));
    set_pass_mess("przez drewniane drzwi do sali rozpraw");

    set_lock_command("zamknij");
    set_unlock_command("otw�rz");
    
    set_key(KOD_KLUCZA_S_S);
    set_lock_name("zamek");
    
    ustaw_material(MATERIALY_STAL, 1);
    ustaw_material(MATERIALY_DR_DAB, 99);
    set_type(O_INNE);
    set_open(0);
    set_locked(1);
}

public int
is_drzwi_parter_s2_sala()
{
    return 1;
}
