inherit "/std/room";

#include <stdproperties.h>
#include <pl.h>
#include <macros.h>
#include "dir.h"

void
create_room() 
{
    set_short("Podw�rze");

    add_object(GARNIZON_DRZWI + "parter-podworze-n2");
    add_object(GARNIZON_DRZWI + "parter-podworze-s2");

    dodaj_rzecz_niewyswietlana_plik(MEBLE + "lawka_prosta_drewniana", 2);
    add_object(MEBLE + "lawka_prosta_drewniana", 2);
}

string
opis_drzwi()
{
    object drzwi1 = filter(all_inventory(TO), &->is_drzwi_parter_podworze_n2())[0];
    object drzwi2 = filter(all_inventory(TO), &->is_drzwi_parter_podworze_s2())[0];
    if (drzwi1->query_open() && drzwi2->query_open()) {
        return "Drewniane wrota w p�nocnym i po�udniowym skrzydle garnizonu s� " +
            "teraz otwarte. ";
    }
    else if (drzwi1->query_open() && (!drzwi2->query_open())) {
        return "Drewniane wrota w p�nocnym skrzydle garnizonu s� lekko uchylone. " +
            "Ich bli�niacza kopia w po�udniowym skrzydle wygl�da na zamkni�t�. ";
    }
    else if ((!drzwi1->query_open()) && drzwi2->query_open()) {
        return "Drewniane wrota w po�udniowym skrzydle garnizonu s� lekko uchylone. " +
            "Ich bli�niacza kopia w p�nocnym skrzydle wygl�da na zamkni�t�. ";
    }
    else {
        return "Drewniane wrota prowadz�ce do skrzyd�a p�nocnego jak i " +
            "po�udniowego s� w tej chwili zamkni�te. ";
    }
}

string
opis_lawek()
{
    object* inwentarz = all_inventory(this_object());
    int amount = 0;
    foreach (object obj : inwentarz) {
        if (explode(file_name(obj), "#")[0] == "/d/Standard/items/meble/lawka_prosta_drewniana") {
            amount += 1;
        }
    }
    if (amount == 1) {
        return " W po�udniowej cz�ci podw�rza stoi samotnie prosta drewniana �awka.";
    }
    if (amount > 1) {
        return " W po�udniowej cz�ci podw�rza stoj� dwie identyczne proste drewniane �awki.";
    }
    return "";
}

string
dlugi_opis()
{
    string str = "Sporej wielko�ci podw�rze zamkni�te jest ze wszystkich " +
        "stron wysokimi murami garnizonu. Ziemia tutaj jest mocno " +
        "udeptana, zapewne od odbywaj�cej si� cz�sto musztry �o�nierzy. " +
        "W du�ej cz�ci okien garnizonu wida� wmurowane we framugi " +
        "metalowe kraty, co dobitnie �wiadczy o militarnym przeznaczeniu " +
        "otaczaj�cej to miejsce budowli. " + opis_drzwi() +
        "Nie dostrzegasz wok� siebie ani jednego elementu zieleni, a ca�e " +
        "otoczenie utrzymane jest raczej w tonacji szaroburej." + opis_lawek() + "\n";
    return str;
}
