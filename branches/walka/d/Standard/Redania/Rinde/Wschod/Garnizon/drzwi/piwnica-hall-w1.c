inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <materialy.h>
#include <object_types.h>

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami",
        "drzwiach" }), PL_NIJAKI_OS);

    dodaj_przym("wzmacniany", "wzmacniani");

    set_other_room(GARNIZON_LOKACJE + "piwnica-w1.c");

    set_door_id(KOD_DRZWI_P_W1);

    set_door_desc("Wzmacniane stalowymi sztabami, drewniane drzwi zbite z d�bowych, grubych desek. " +
                  "Na ca�ej ich p�aszczy�nie poumieszczane s� du�e nity, po��czone szerokimi " +
                  "metalowymi komponentami.\n");

    set_open_desc("W zachodniej �cianie widniej� otwarte wzmacniane drzwi.\n");
    set_closed_desc("W zachodniej �cianie widniej� zamkni�te wzmacniane drzwi.\n");

    set_pass_command( ({"zach�d"}));
    set_pass_mess("przez wzmacniane drzwi na zach�d");

    set_lock_command("zamknij");
    set_unlock_command("otw�rz");
    
    set_key(KOD_KLUCZA_P_W1);
    set_lock_name("zamek");
    
    ustaw_material(MATERIALY_STAL, 60);
    ustaw_material(MATERIALY_DR_DAB, 40);
    set_type(O_INNE);
}
