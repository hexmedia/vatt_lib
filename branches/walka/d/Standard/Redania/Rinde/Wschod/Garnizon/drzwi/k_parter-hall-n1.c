inherit "/std/key";

#include "dir.h"
#include <stdproperties.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>

void
create_key()
{
    ustaw_nazwe(({ "klucz", "klucza", "kluczowi", "klucz", "kluczem",
        "kluczu" }), ({ "klucze", "kluczy", "kluczom", "klucze",
        "kluczami", "kluczach" }), PL_MESKI_NOS_NZYW);

    set_long("Solidny, sporej wielko�ci stalowy klucz. Lekko wytarta ko�c�wka " +
             "�wiadczy o cz�stym jego u�ywaniu. Wygrawerowany na nim jest napis " +
             "'gr gw'.\n");
    
    dodaj_przym("solidny", "solidni");
    dodaj_przym("stalowy", "stalowi");
    
    set_key(KOD_KLUCZA_P_G);

    ustaw_material(MATERIALY_STAL);
    set_type(O_INNE);
}
