inherit "/std/room";

#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

void
create_room() 
{
  set_short("Dow�dca.");
  
  add_exit("sekretariat.c","zach�d");

  add_prop(ROOM_I_INSIDE,1);
}

string
dlugi_opis()
{
  string str = "Skromnie urz�dzony, niewielki pok�j. Niezbyt szerokie okno " +
               "w �cianie po�udniowej przys�oni�te jest od strony zewn�trznej " +
               "�elaznymi kratami. Go�e �ciany pokryte s� kremowobia�ym " +
               "tynkiem, utrzymanym w nienagannej czysto�ci. Jedyn� ozdob� " +
               "w ca�ym pomieszczeniu wydaje si� by� sporych rozmiar�w herb " +
               "wyryty w murze naprzeciwko wej�cia. Wej�cia tego nie zas�aniaj� " +
               "�adne drzwi, lecz zwisaj�ce z sufitu koraliki w kolorze ciemnoszarym.\n";

    // TODO: doda� jakie� biurko, szafki

  return str;
}
