inherit "/std/room";

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>

void
create_rinde_street()
{
}

nomask void
create_room()
{
    set_long("@@dlugi_opis@@"); // no i teraz nie trzeba dawac set_long
				    // wystarczy tylko zrobic funkcje dlugi_opis
				    // zwracajaca opis lokacji

	// tutaj np. eventy i inne rzeczy wspolne dla lokacji rinde
    add_item(({"miasto","miasteczko"}), "Tak, to jest w�a�nie Rinde!\n");
	

    //add_event("@@event_ulicy_rinde@@");
	
    set_event_time(300.0);
    set_alarm_every_hour("dzwon");

    add_sit("pod �cian�", "pod �cian�", "spod �ciany", 0);
    add_sit("na bruku", "na bruku", "z bruku", 0);
	
	add_prop(ROOM_I_TYPE,ROOM_IN_CITY);
	
    create_rinde_street();

    /* prop�w o warto�ci 0 si� nie dodaje, ale jakby kto� mia� skorzysta�
     * z tego std do innych miast, musi te propy uwzgl�dni�...
    add_prop(ROOM_I_WSP_Y, 0); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, 0); //Rinde jest p�pkiem wszech�wiata ;)
     */
}

// Dzwon wybijajacy godzine, slychac to na ulicach. Lil.
int
dzwon()
{
    int godzinka = MT_NHOUR;
   
    if (godzinka >= 4 && godzinka <= 22)
    {
	if (godzinka > 12) godzinka -= 12;
	tell_room(this_object(), "Bim! Bam! Ratuszowy dzwon wybija godzin^e " +
	    LANG_SORD(godzinka, PL_BIE, PL_ZENSKI) + ".\n");
    }
}

string
event_ulicy_rinde()
{
    switch (pora_dnia())
    {
	case MT_RANEK:
	case MT_POLUDNIE:
	case MT_POPOLUDNIE:
	    return ({ "Z pobliskiego domostwa dobiegaj� ci� odg�osy k��tni. Po chwili cichn�.\n",
	        "Przez ulic� przebiega wrzeszcz�cy dzieciak.\n",
	        "Przez ulic� przebiega banda wrzeszcz�cych dzieciak�w.\n",
	        "Jaki� dzieciak ci�gle przypatruje ci si� z okna.\n",
	        "Ulic� spokojnie przechodzi pies.\n",
	        "Przez ulic� galopuje jaki� je�dziec.\n",
	        "Stado go��bi poderwa�o si� z dachu kamieniczki.\n",
	        "Chwiejnym krokiem mija ci� pijany m�czyna.\n",
	        "Gdzie� niedaleko skowycz� psy.\n",
	        "Zamo�nie ubrany rycerz przeje�d�a konno ulic� wzbudzaj�c boja�liwe i zazdrosne spojrzenia mieszczan.\n",
	        "Ulic� przechodzi schlany krasnolud ko�ysz�c si� na boki. Widz�cy to ludzie szemraj� mi�dzy sob�.\n",
	        "Goniec pocztowy szybkim truchtem przebiega ulic�.\n",
	        "Nas miastem przelatuje sok�.\n",
	        "Czujesz zapach gotowanych ziemniak�w.\n",
	        "Powolnym krokiem przemierza ulic� stary podr�nik.\n",
	        "Wyj�tkowo umi�niony m�czyzna zmierza w stron� najbli�szej karczmy.\n",
	        "Panuje tu potworny gwar.\n",
	        "S�yszysz rytmiczne uderzenia b�bn�w.\n",
	        "Czujesz pi�kny zapach kwitn�cych kwiat�w.\n",
	        "Drzewa w lesie coraz bardziej si� zieleni�.\n" })[random(20)];
	default: // wieczor, noc
	    return ({ "Mija ci� jaki� podejrzany typ.\n",
	        "Przez ulic� przeszed� u�miechni�ty podchmielony m�czyzna.\n",
	        "Czarny kot przebiega ci drog�.\n",
	        "Z oddali dochodzi ci� nag�y wrzask.\n",
	        "Dochodzi ci� zapach przypalonej dziczyzny.\n",
	        "Gdzie� w okolicy wyje pies.\n",
	        "Jaki� mieszczanin wrzeszczy, �e zosta� obrabowany.\n",
	        "S�yszysz weso�e �piewy pijaczk�w.\n",
	        "Z okolicznych budynk�w dochodzi ci� d�wi�k skrzypi�cych desek.\n",
	        "Czujesz si� jako� nieswojo.\n",
	        "Zza okna jednego z budynk�w kto� ci si� przygl�da.\n",
	        "Czujesz smr�d odchod�w.\n",
	        "Chodz� tu jacy� podejrzani ludzie.\n",
	        "Nagle nastaje absolutna cisza.\n" })[random(14)];
    }
}

