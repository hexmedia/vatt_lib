
/* 
 * Drzwi z sklepu w Rinde
 * Wykonane przez Avarda, dnia 07.06.06
 */

inherit "/std/door";
#include "dir.h"

#include <pl.h>


void
create_door()
{
    ustaw_nazwe("drzwi"); 
    dodaj_przym("zwyk^ly","zwykli");
    
    set_other_room(CENTRUM_LOKACJE + "placn.c");
    set_door_id("DRZWI_DO_SKLEPU_W_RINDE");
    set_door_desc("Solidne, d^ebowe drzwi z jednego z miejskich " +
        "sklep^ow. Wiekowe ju^z drewno mocno pociemnia^lo od s^lo^nca.\n");
        
    set_pass_command(({"wyj^scie","do wyj�cia","ze sklepu"}));
    set_pass_mess("przez zwyk^le drzwi na zewn^atrz");
    set_open_desc("");
    set_closed_desc("");
    
    set_lock_command("zamknij");
    set_unlock_command("otw�rz");

    set_key("KLUCZ_DRZWI_DO_SKLEPU_RINDE");
    set_lock_name("zamek");
}
