inherit "/std/key";
#include "dir.h"

#define KOD_KLUCZA "drzwirinderatuszklucz"
#include <stdproperties.h>
#include <pl.h>
#include <materialy.h>

void
create_key()
{
    ustaw_nazwe("klucz");

    set_long("Du^zy, pozbawiony wszelkich zdobie^n klucz.\n");

    dodaj_przym("du^zy", "duzi");
    dodaj_przym("prosty", "pro^sci");

    set_key(KOD_KLUCZA);

set_owners(({RINDE_NPC + "sekretarz"}));
}
