inherit "/std/key";

#include <stdproperties.h>
#include <pl.h>

#include "dir.h"

void
create_key()
{
    ustaw_nazwe("klucz");
    
    dodaj_przym("du^zy", "duzi");
    dodaj_przym("prosty", "pro^sci");

    set_long("Du^zy i ci^e^zki klucz, praktycznie bez ^zadnych zdobie^n.\n");

    set_key("KLUCZ_DRZWI_DO_BANKU_RINDE");

set_owners(({RINDE_NPC + "ruygen"}));
}
