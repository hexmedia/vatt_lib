inherit "/std/door";

#include <pl.h>
#define KOD_DRZWI "drzwirinderatusz"
#define KOD_KLUCZA "drzwirinderatuszklucz"
#include "/d/Standard/Redania/Rinde/dir.h"

string otwarte();

void
create_door()
{
    ustaw_nazwe("drzwi");

    dodaj_przym("wielki", "wielcy");
    dodaj_przym("dwuskrzyd^lowy", "dwuskrzyd^lowi");

    set_other_room(RINDE+"Centrum/Ratusz/lokacje/korytarz.c");

    set_door_id(KOD_DRZWI);

    set_door_desc("Wielkie, ci^e^zkie drzwi z mocnego d^ebu prowadz^a " +
                  "do ^srodka ratusza. W tej chwili s^a @@otwarte:"+file_name(this_object())+"@@.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"ratusz","do ratusza","z dworu"}));
    
     set_lock_command("zamknij");
    set_unlock_command("otw�rz");

    set_key(KOD_KLUCZA);
    set_lock_name("zamek");
    set_pick(69);
    set_open(0);
    set_locked(0);
}

string
otwarte()
{
    if (this_object()->query_open())
        return "otwarte";
    else
        return "zamkni^ete";
}
