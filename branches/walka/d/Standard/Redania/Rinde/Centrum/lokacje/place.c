#include "dir.h"

#include <mudtime.h>
#include <stdproperties.h>
#include <macros.h>

inherit RINDE_STD;

void odnow_golebie();

void
create_rinde_street() 
{
    set_short("Miejski rynek");

    add_item(({"plac", "rynek"}),
        "Starannie wybrukowany plac, na ^srodku kt^orego wznosi si^e " +
    "zbudowany z czerwonej ceg�y gmach ratusza, stanowi centrum tego " +
    "miasta.\n");
    add_item("ratusz",
        "Budynek ten, cho^c prezentuje si^e dumnie, nie wyr^o^znia si^e " +
    "bynajmniej architektur^a g^ornych lot^ow i nie by^lby nijak " +
    "wyj^atkowym, gdyby nie budz^aca podziw smuk^la wie^za pn^aca si^e " +
	"wysoko ponad dachy innych budowli. Ponadto szczeg^olno^sci temu " +
    "gmachowi dodaje fakt, i^z spe^lnia on niebanaln^a rol^e w ^zyciu " +
    "miasta.\n");
    add_item(({"wie^z^e", "smuk^l^a wie^z^e", "wie^z^e ratusza"}),
        "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic "+
	"niczym palec, wskazuj^acy centrum tego miasta.\n");
    add_item(({"kamienice", "stare kamienice", "pi^etrowe kamienice",
        "stare pi^etrowe kamienice"}),
	"Stare, ponure kamienice wybudowane tutaj zosta^ly na planie kwadratu, " +
    "pozostawiaj^ac wewn^atrz niema^l^a cz^e^s^c terenu, na kt^orej " +
    "powsta^l ^ow plac - miejsce spotka^n, handlu i wszelkich miejskich " +
    "uroczysto^sci.\n");
    add_item("bruk",
        "Ten, a mo^ze raczej ci, kt^orzy wbijali w ziemi^e te kocie ^lby, " +
    "musieli wyla^c z siebie si^odme poty, by wybrukowa^c plac tak " +
    "starannie. Jednak^ze g^ladko^s^c ich powierzchni oraz dok^ladne " +
    "wyprofilowanie kamieni nie jest ju^z zas^lug^a budowniczych a " +
    "jedynie przechodni�w, bezustannie krz^ataj^acych si^e po rynku.\n");
    add_item("bank",
        "Z rz^edu szarych i jednakowych kamienic miejskich wyr^o^znia si^e " +
        "elegancki budynek lokalnego banku.\n");

    add_exit("placne.c","p^o^lnoc");
    add_exit("placse.c","po^ludnie");

    add_object(CENTRUM_DRZWI + "do_banku.c");

    add_sit("na bruku","na bruku","z bruku",0);

    add_object(RINDE+"przedmioty/lampa_uliczna");

    odnow_golebie();
    set_alarm_every_hour(&odnow_golebie());
}
	
string
dlugi_opis()
{
    string str;

    str = "Ta cz^e^s^c rynku jest o wiele cichsza i spokojniejsza od " +
    "p^o^lnocnej. Kawa^lek na zach^od pnie si^e dumnie w g^or^e " +
    "miejski ratusz, na wschodzie za^s z rz^edu kamienic wyr�nia si� " +
    "spory i elegancki budynek lokalnego banku";

    if (!dzien_noc())
        str += ", przy kt�rym koncentruje si� raczej niewielki po tej " +
        "stronie placu ruch. ";
    else
        str += ". ";

    str += "Na p�nocy widniej� ";

    if (pora_dnia() < 18)
        str += "porozstawiane stragany z r�no�ciami, oblegane przez " +
        "ha^la^sliwych mieszczan. ";
    else
        str += "opuszczone na noc kupieckie stragany. ";

    if(this_object()->jest_dzien())
    {
    str += "Po drobnym granitowym bruku przechadzaj� si� grupki go��bi.\n";
    }
    else
    {
    str += "Nocna pora odes�a�a spaceruj�ce tu za dnia go��bie"
      +" do miejsc, gdzie spokojnie b�d� mog�y odnowi� si�y"
      +" na jutrzejszy dzie�.\n";
    }
    return str;
}

void odnow_golebie()
{
  if(this_object()->jest_dzien() && !present("go��b", this_object()))
    {
      int n = random(4)+2;
      for(int i = 0; i < n; i++)
        clone_object(RINDE_NPC+"golab_plac.c")->move(TO);
    }
}

public string
exits_description() 
{
    return "Dalsza cz^e^s^c placu widoczna jest na p^o^lnocy i po^ludniu, "+
           "znajduje si^e tu tak^ze wej^scie do banku.\n";
}
