#include "dir.h"
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

inherit RINDE_STD;

void find_stragany();
void odnow_kupcow();
int czy_kupcow();

void
create_rinde_street() 
{
    set_short("Miejski rynek");
	
    add_item(({"plac", "rynek"}),
        "Starannie wybrukowany plac, na ^srodku kt^orego wznosi si^e " +
    "zbudowany z czerwonej ceg�y gmach ratusza, stanowi centrum tego " +
    "miasta.\n");
    add_item("ratusz",
        "Budynek ten, cho^c prezentuje si^e dumnie, nie wyr^o^znia si^e " +
    "bynajmniej architektur^a g^ornych lot^ow i nie by^lby nijak " +
    "wyj^atkowym, gdyby nie budz^aca podziw smuk^la wie^za pn^aca si^e " +
	"wysoko ponad dachy innych budowli. Ponadto szczeg^olno^sci temu " +
    "gmachowi dodaje fakt, i^z spe^lnia on niebanaln^a rol^e w ^zyciu " +
    "miasta.\n");
    add_item(({"wie^z^e", "smuk^l^a wie^z^e", "wie^z^e ratusza"}),
        "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic "+
	"niczym palec, wskazuj^acy centrum tego miasta.\n");
    add_item(({"kamienice", "stare kamienice", "pi^etrowe kamienice",
        "stare pi^etrowe kamienice"}),
	"Stare, ponure kamienice wybudowane tutaj zosta^ly na planie kwadratu, " +
    "pozostawiaj^ac wewn^atrz niema^l^a cz^e^s^c terenu, na kt^orej " +
    "powsta^l ^ow plac - miejsce spotka^n, handlu i wszelkich miejskich " +
    "uroczysto^sci.\n");
    add_item("bruk",
        "Ten, a mo^ze raczej ci, kt^orzy wbijali w ziemi^e te kocie ^lby, " +
    "musieli wyla^c z siebie si^odme poty, by wybrukowa^c plac tak " +
    "starannie. Jednak^ze g^ladko^s^c ich powierzchni oraz dok^ladne " +
    "wyprofilowanie kamieni nie jest ju^z zas^lug^a budowniczych a " +
    "jedynie przechodni�w, bezustannie krz^ataj^acych si^e po rynku.\n");
     add_item(({"stragany", "kramy"}),
        "Ci^agn^ace si^e wzd^lu^z ulicy kolorowe stragany pe^lne s^a " +
    "przer^o^znych przedmiot^ow, sprowadzanych z ca^lego ^swiata. Chyba " +
    "ka^zdy znajdzie tu co^s dla siebie, gdy^z r^o^znorodno^s^c towar^ow "+
    "jest doprawdy zdumiewaj^aca: od tutejszych i zagranicznych owoc^ow " +
    "i warzyw, przez wielobarwne stroje uszyte z r^o^znych, niekiedy " +
    "nieco dziwacznych i niespotykanych w tych stronach tkanin, a^z po " +
    "najr^o^zniejsze bronie.\n");
    add_item(({"^swi^atyni^e", "dzwonnic^e", "dzwonnic^e ^swi^atyni"}),
        "Na zachodzie, nieco przys�oni�ta przez ratusz, widnieje " +
    "okaza�a �wi�tynia z dzwonnic^a.\n");
	    
    add_sit("na bruku","na bruku","z bruku",0);
	
    add_exit(POLNOC_LOKACJE + "ulica5.c","p^o^lnoc");
    add_exit("place.c","po^ludnie");
    add_exit("placn.c","zach^od");
    add_exit(WSCHOD_LOKACJE + "ulica1.c","wsch^od");	

    add_object(RINDE+"przedmioty/lampa_uliczna");
    
    add_object(CENTRUM_OBIEKTY + "stragan");
    add_object(CENTRUM_OBIEKTY + "stragan");
    add_object(CENTRUM_OBIEKTY + "stragan");

    seteuid(getuid());

    dodaj_rzecz_niewyswietlana_plik(CENTRUM_OBIEKTY + "stragan", 99);

    add_npc(RINDE_NPC + "kupiec_maly", 1, "czy_kupcow");
    add_npc(RINDE_NPC + "kupcowa", 1, "czy_kupcow");

    set_alarm(0.1, 0.0, &find_stragany());

    set_alarm_every_hour("odnow_kupcow");
}

void
odnow_ich()
{
	object n1 = present(find_living("kupieczrynku"), TO);
    if(!present(find_living("kupieczrynku"), TO))
    {
        object n1;
        (n1 =  clone_object(RINDE_NPC + "kupiec_maly"));
        tell_roombb(TO, QCIMIE(n1, PL_MIA)+" wychodzi "+
            "z jednej z kamienic.\n");
        n1->init_arg(0);
        n1->move(TO);

    }

	object n2 = present(find_living("kupcowazrynku"), TO);
    if(!present(find_living("kupcowazrynku"), TO))
    {
        object n2;
        (n2 =  clone_object(RINDE_NPC + "kupcowa"));
        tell_roombb(TO, QCIMIE(n2, PL_MIA)+" wychodzi "+
            "z jednej z kamienic.\n");
        n2->init_arg(0);
        n2->move(TO);
    }
}

void
odnow_kupcow()
{
	if(czy_kupcow)
    {
        set_alarm(30.0,0.0, "odnow_ich");
    }


    /*if (!find_living("kupieczrynku"))
        add_npc(RINDE_NPC + "kupiec_maly", 1, "czy_kupcow");

    if (!find_living("kupcowazrynku"))
        add_npc(RINDE_NPC + "kupcowa", 1, "czy_kupcow");*/
}

int
czy_kupcow()
{
    switch (pora_roku())
    {
	case MT_LATO:
	case MT_WIOSNA:
	    if(MT_GODZINA >= 2 && pora_dnia() <= 16)
	    	return 1;
	default:
	    if(MT_GODZINA >= 8 && pora_dnia() <= 16)
	    	return 1;
    }
    return 0;
}

void
find_stragany()
{
    object *stragwszy = object_clones(find_object(CENTRUM + "obiekty/stragan"));
    object *stragte = ({});
    int i;

    i = sizeof(stragwszy) - 1;
    while (i > -1)
    {
        if (ENV(stragwszy[i]) == TO)
            stragte += stragwszy[i..i];

        i -= 1;
    }

    if (sizeof(stragte) > 0)
    stragte[0]->dodaj_przym("pierwszy", "pierwsi");
    if (sizeof(stragte) > 1)
    stragte[1]->dodaj_przym("drugi", "drudzy");
    if (sizeof(stragte) > 2)
    stragte[2]->dodaj_przym("trzeci", "trzeci");
}
	
string
dlugi_opis()
{
    string str;
    object *liv = FILTER_OTHER_LIVE(all_inventory(TO));
    object *stoja = filter(liv, &operator(!=)(0) @ &->query_prop("stoi_za_straganem"));

    str = "P^o^lnocno-wschodni kraniec rynku jest centrum handlowym miasta. " +
	"To tutaj w^la^snie kupcy z przer�nych, cz^esto bardzo odleg�ych " +
	"stron �wiata wystawiaj� swe towary na sprzeda�. ";

    if (pora_dnia() < 18)
        str += "Porozstawiane wsz^edzie ";
    else
        str += "Rozstawiane tu dzie^n w dzie^n ";

	str += "stragany oferuj� bogactwo przer�nych, mniej i bardziej " +
	"egzotycznych towar�w. ";
    if(pora_dnia() < 18)
    {
        str += "T�um ludzi w tym miejscu, cz�stokro� fantazyjnie ubranych, "+
            "mieni si� mn�stwem kolor�w. ";
    }
    str += "Granice placu " +
	"wyznaczaj� niewysokie szare kamieniczki, z kt�rych wygl�daj� " +
	"rz�dem ciemne prostok^aty okien. Na po�udniowym zachodzie, w samym " +
	"centrum placu wznosi si� dumnie miejski ratusz, kt^orego gmach " +
	"wie^nczy strzelista wie�a.\n";

    switch (sizeof(stoja))
    {
        case 0:
            break;
        case 1:
            str += "Za jednym ze stragan^ow stoi " + COMPOSITE_LIVE(stoja, 0) + ".\n";
            break;
        default:
            str += "Za straganami stoj^a " + COMPOSITE_LIVE(stoja, 0) + ".\n";
            break;
    }

    return str;
}

public string
exits_description() 
{
    return "Dalsza cz^e^s^c placu widoczna jest na po^ludniu i zachodzie, "+
           "za^s ulice prowadz^a st^ad na p^o^lnoc i wsch^od.\n";
}
