#include "dir.h"

#include <stdproperties.h>
#define BIZUTERIA "/d/Standard/items/bizuteria/"

inherit "/std/room";

void create_room() 
{
    set_short("Kiesze� straganiarza");
    set_long("Jeste� w kieszeni bez dna.\n");

    add_prop(ROOM_I_INSIDE,1);
    
    
    add_object("/std/brzytwa",10);
    
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    add_object("/d/Standard/items/kostka_k6");
    
    add_object(BIZUTERIA+"kolczyki/dlugi_posrebrzany");
    add_object(BIZUTERIA+"kolczyki/dlugi_posrebrzany");
    add_object(BIZUTERIA+"kolczyki/dlugi_posrebrzany");
    add_object(BIZUTERIA+"kolczyki/dlugi_posrebrzany");
    add_object(BIZUTERIA+"kolczyki/filigranowy_srebrny",10);
    add_object(BIZUTERIA+"kolczyki/prosty_okragly");
    add_object(BIZUTERIA+"kolczyki/prosty_okragly");
    add_object(BIZUTERIA+"kolczyki/prosty_okragly");
    add_object(BIZUTERIA+"kolczyki/prosty_okragly");
    add_object(BIZUTERIA+"kolczyki/prosty_okragly");
    add_object(BIZUTERIA+"kolczyki/prosty_okragly");
    add_object(BIZUTERIA+"kolczyki/prosty_okragly");
    add_object(BIZUTERIA+"kolczyki/prosty_okragly");
    add_object(BIZUTERIA+"kolczyki/prosty_okragly");
    add_object(BIZUTERIA+"kolczyki/prosty_okragly");
    add_object(BIZUTERIA+"kolczyki/prosty_okragly");
    add_object(BIZUTERIA+"kolczyki/prosty_okragly");
    add_object(BIZUTERIA+"koraliki/kolorowe_drewniane");
    add_object(BIZUTERIA+"koraliki/kolorowe_drewniane");
    add_object(BIZUTERIA+"koraliki/kolorowe_drewniane");
    add_object(BIZUTERIA+"koraliki/kolorowe_drewniane");
    add_object(BIZUTERIA+"koraliki/kolorowe_drewniane");
    add_object(BIZUTERIA+"koraliki/kolorowe_drewniane");
    add_object(BIZUTERIA+"koraliki/kolorowe_drewniane");
    add_object(BIZUTERIA+"koraliki/kolorowe_drewniane");
    add_object(BIZUTERIA+"koraliki/kolorowe_drewniane");
 
    add_object("/d/Standard/items/sloik.c",15);
}
