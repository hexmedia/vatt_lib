#include "dir.h"
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

inherit RINDE_STD;

void find_stragany();
void odnow_kupcow();
int czy_kupcow();

void
create_rinde_street() 
{
    set_short("Miejski rynek");

    add_item(({"plac", "rynek"}),
        "Starannie wybrukowany plac, na ^srodku kt^orego wznosi si^e " +
    "zbudowany z czerwonej ceg�y gmach ratusza, stanowi centrum tego " +
    "miasta.\n");
    add_item("ratusz",
        "Budynek ten, cho^c prezentuje si^e dumnie, nie wyr^o^znia si^e " +
    "bynajmniej architektur^a g^ornych lot^ow i nie by^lby nijak " +
    "wyj^atkowym, gdyby nie budz^aca podziw smuk^la wie^za pn^aca si^e " +
	"wysoko ponad dachy innych budowli. Ponadto szczeg^olno^sci temu " +
    "gmachowi dodaje fakt, i^z spe^lnia on niebanaln^a rol^e w ^zyciu " +
    "miasta.\n");
    add_item(({"wie^z^e", "smuk^l^a wie^z^e", "wie^z^e ratusza"}),
        "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic "+
	"niczym palec, wskazuj^acy centrum tego miasta.\n");
    add_item(({"kamienice", "stare kamienice", "pi^etrowe kamienice",
        "stare pi^etrowe kamienice"}),
	"Stare, ponure kamienice wybudowane tutaj zosta^ly na planie kwadratu, " +
    "pozostawiaj^ac wewn^atrz niema^l^a cz^e^s^c terenu, na kt^orej " +
    "powsta^l ^ow plac - miejsce spotka^n, handlu i wszelkich miejskich " +
    "uroczysto^sci.\n");
    add_item("bruk",
        "Ten, a mo^ze raczej ci, kt^orzy wbijali w ziemi^e te kocie ^lby, " +
    "musieli wyla^c z siebie si^odme poty, by wybrukowa^c plac tak " +
    "starannie. Jednak^ze g^ladko^s^c ich powierzchni oraz dok^ladne " +
    "wyprofilowanie kamieni nie jest ju^z zas^lug^a budowniczych a " +
    "jedynie przechodni�w, bezustannie krz^ataj^acych si^e po rynku.\n");
    add_item(({"stragany", "kramy"}),
        "Ci^agn^ace si^e wzd^lu^z ulicy kolorowe stragany pe^lne s^a " +
    "przer^o^znych przedmiot^ow, sprowadzanych z ca^lego ^swiata. Chyba " +
    "ka^zdy znajdzie tu co^s dla siebie, gdy^z r^o^znorodno^s^c towar^ow "+
    "jest doprawdy zdumiewaj^aca: od tutejszych i zagranicznych owoc^ow " +
    "i warzyw, przez wielobarwne stroje uszyte z r^o^znych, niekiedy " +
    "nieco dziwacznych i niespotykanych w tych stronach tkanin, a^z po " +
    "najr^o^zniejsze bronie.\n");
    add_item("sklep",
        "Lokal sklepu mie^sci si^e w jednej z otaczaj^acych plac szarych " +
    "i zupe^lnie do siebie podobnych kamieniczek.\n");
	    
    add_sit("na bruku","na bruku","z bruku",0);

    add_exit("placnw.c","zach�d");
    add_exit("placne.c","wsch�d");	
    
    add_object(RINDE + "przedmioty/lampa_uliczna");

    add_object(CENTRUM_DRZWI + "do_sklepu.c");
    
    add_object(CENTRUM_OBIEKTY + "stragan");
    add_object(CENTRUM_OBIEKTY + "stragan");
    add_object(CENTRUM_OBIEKTY + "stragan");
    
    seteuid(getuid());

    dodaj_rzecz_niewyswietlana_plik(CENTRUM_OBIEKTY + "stragan", 99);

    set_alarm(0.1, 0.0, &find_stragany());

    odnow_kupcow();
    set_alarm_every_hour("odnow_kupcow");
}

void
odnow_kupcow()
{
    LOAD_ERR(RINDE_NPC + "straganiarz.c");

    if(!present("kupieczrynkupolelf", TO) && czy_kupcow() && 
            (sizeof(object_clones(find_object(RINDE_NPC+"straganiarz.c"))) == 0))
    {
        object kupiec;
        kupiec = clone_object(RINDE_NPC + "straganiarz.c");
        kupiec->init_arg(0);
        kupiec->start_me();
        kupiec->move(TO);
    }

}

int
czy_kupcow()
{
    switch (pora_roku())
    {
	case MT_LATO:
	case MT_WIOSNA:
	    return MT_GODZINA >= 7 && pora_dnia() <= 18;
	default:
	    return MT_GODZINA >= 8 && pora_dnia() <= 18;
    }
}

void
find_stragany()
{
    object *stragwszy = object_clones(find_object(CENTRUM + "obiekty/stragan"));
    object *stragte = ({});
    int i;

    i = sizeof(stragwszy) - 1;
    while (i > -1)
    {
        if (ENV(stragwszy[i]) == TO)
            stragte += stragwszy[i..i];

        i -= 1;
    }

    if (sizeof(stragte) > 0)
    stragte[0]->dodaj_przym("pierwszy", "pierwsi");
    if (sizeof(stragte) > 1)
    stragte[1]->dodaj_przym("drugi", "drudzy");
    if (sizeof(stragte) > 2)
    stragte[2]->dodaj_przym("trzeci", "trzeci");
}

string
dlugi_opis()
{
    string str, str2;
    object *liv = FILTER_OTHER_LIVE(all_inventory(TO));
    object *stoja = filter(liv, &operator(!=)(0) @ &->query_prop("stoi_za_straganem"));

    if(jest_dzien() == 1)
    {
        /* poni�szy if-else jest pewnie niezgodny z naszymi nowymi 
           funkcjami obrabiaj�cymi d�ugi opis w zale�no�ci od 
           obecno�ci przedmiot�w itp. niech mnie kto� pouczy
            jak powinno by� :P - molder. */
        if(present("lampa", TO)->query_prop(OBJ_I_LIGHT))
            str2  = "rozganiaj�ca czer� nocy ";
        else
            str2 = "";

        str = "Na starannie u�o�onym, eleganckim bruku porozk�adali " +
	    "swoje stragany kupcy najr�niejszego rodzaju. Ju� z daleka " +
	    "po�yskuj� w s�o�cu zamorskie tkaniny, z�otog�owia i jedwabie, " +
	    "stosy owoc�w, bi�uteria, bro� i ca�a mnogo�� innych bardziej " +
           "i mniej dziwacznych rzeczy. Kramy, trzeba przyzna�, stoj� w " +
           "nale�ytym porz�dku i nale�� do tych \"lepszego gatunku\", " +
           "tak �e wcale nie psuj^a sw� obecno�ci� tej atmosfery spokojnej " +
           "elegancji i porz�dku panuj�cej na placu. T�um kr�c�cy si� wok� " +
           "wystawionych towar�w mieni si� kolorami stroj�w, pulsuje " +
           "pokrzykiwaniami kupc�w, piskiem dzieci i szmerem nieustaj�cych " +
           "rozm�w. Ponad g�owami wszystkich wznosi si� " + str2 +
           "uliczna lampa.\n";
    }
    else
    {
        if(present("lampa", TO)->query_prop(OBJ_I_LIGHT))
            str2  = "rozganiaj�ca czer� nocy ";
        else
            str2 = "";

        str = "Na starannie u�o�onym, eleganckim bruku porozk�adali " +
	    "swoje stragany kupcy najr�niejszego rodzaju. Kramy, "+
           "trzeba przyzna�, stoj� w nale�ytym porz�dku i nale�� do "+
           "tych \"lepszego gatunku\", tak �e wcale nie psuj^a sw� "+
           "obecno�ci� tej atmosfery spokojnej elegancji i porz�dku "+
           "panuj�cej na placu. Ponad twoja g�ow^a wznosi si� " + str2 +
           "uliczna lampa.\n";
    }

    switch (sizeof(stoja))
    {
        case 0:
            break;
        case 1:
            str += "Za jednym ze stragan^ow stoi " + COMPOSITE_LIVE(stoja, 0) + ".\n";
            break;
        default:
            str += "Za straganami stoj^a " + COMPOSITE_LIVE(stoja, 0) + ".\n";
            break;
    }
    return str;
}

public string
exits_description() 
{
    return "Dalsza cz^e^s^c placu widoczna jest na zachodzie i "+
           "wschodzie, jest tutaj tak^ze wej^scie do sklepu.\n";
}
