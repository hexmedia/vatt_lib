#include "dir.h"

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include <pogoda.h>

inherit RINDE_STD;

void odnow_golebie();

void
create_rinde_street() 
{
	set_short("Miejski rynek");

    add_item(({"plac", "rynek"}),
        "Starannie wybrukowany plac, na ^srodku kt^orego wznosi si^e " +
    "zbudowany z czerwonej ceg�y gmach ratusza, stanowi centrum tego " +
    "miasta.\n");
    add_item("ratusz",
        "Budynek ten, cho^c prezentuje si^e dumnie, nie wyr^o^znia si^e " +
    "bynajmniej architektur^a g^ornych lot^ow i nie by^lby nijak " +
    "wyj^atkowym, gdyby nie budz^aca podziw smuk^la wie^za pn^aca si^e " +
	"wysoko ponad dachy innych budowli. Ponadto szczeg^olno^sci temu " +
    "gmachowi dodaje fakt, i^z spe^lnia on niebanaln^a rol^e w ^zyciu " +
    "miasta.\n");
    add_item(({"wie^z^e", "smuk^l^a wie^z^e", "wie^z^e ratusza"}),
        "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic "+
	"niczym palec, wskazuj^acy centrum tego miasta.\n");
    add_item(({"kamienice", "stare kamienice", "pi^etrowe kamienice",
        "stare pi^etrowe kamienice"}),
	"Stare, ponure kamienice wybudowane tutaj zosta^ly na planie kwadratu, " +
    "pozostawiaj^ac wewn^atrz niema^l^a cz^e^s^c terenu, na kt^orej " +
    "powsta^l ^ow plac - miejsce spotka^n, handlu i wszelkich miejskich " +
    "uroczysto^sci.\n");
    add_item("bruk",
        "Ten, a mo^ze raczej ci, kt^orzy wbijali w ziemi^e te kocie ^lby, " +
    "musieli wyla^c z siebie si^odme poty, by wybrukowa^c plac tak " +
    "starannie. Jednak^ze g^ladko^s^c ich powierzchni oraz dok^ladne " +
    "wyprofilowanie kamieni nie jest ju^z zas^lug^a budowniczych a " +
    "jedynie przechodni�w, bezustannie krz^ataj^acych si^e po rynku.\n");
	    
   add_sit("na bruku","na bruku","z bruku",0);
	
	add_exit("placw.c","p^o^lnoc");
	add_exit(POLUDNIE_LOKACJE + "ulica1.c","po^ludnie");
	add_exit(ZACHOD_LOKACJE + "ulica1.c","zach^od");
	add_exit("placs.c","wsch^od");	


	add_prop(ROOM_I_INSIDE,0);
    add_object(RINDE+"przedmioty/lampa_uliczna");

    odnow_golebie();
    set_alarm_every_hour(&odnow_golebie());
}

string
dlugi_opis()
{
	string str;

	str = "Nie jest to samo serce rynku, gdzie wznosi si� smuk�y " +
		"budynek ratusza miejskiego, jednak ta cz^e^s^c placu przedstawia " +
        "si^e r�wnie elegancko. "; //Bruk jest u�o�ony starannie i schludnie.

    if (!dzien_noc())
    {
		str += "Mimo wsz�dobylskich t�um�w wok� panuje atmosfera spokoju " +
		"wzgl�dnej ciszy, nie przerywanej nawet pokrzykiwaniem " +
		"straganiarzy, kt�rzy roz�o�yli swe towary w przeciwleg�ym " +
		"rogu placu. Na p�nocy wida� ";

        if (pora_roku() == MT_ZIMA)
            str += "nieczynn^a w zimie fontann^e";
        else
            if (TEMPERATURA(this_object()) <= 0)
                str += "nieczynn^a z powodu mrozu fontann^e";
            else
                str += "fontann^e wyrzucaj�c� w g�r� kaskady l�ni�cej wody";

        if (CZY_JEST_WODA(this_object()))
            str += ", kt^ora stoi samotnie w szarych strugach deszczu. ";
        else
            str += ", przy kt^orej co i rusz siadaj^a przechodnie, by napoi" +
        "^c si^e cisz^a i spokojem tego miejsca. "; /* niechze i nawet w zimie
                                                      siadaja, wylaczona fon-
                                                      tanna na zdrowy rozum
                                                      jeszcze poteguje "cisze
                                                      i spokoj tego
                                                      miejsca" ;) */
    }
    else
    {
        str += "Jak zawsze panuje tu atmosfera spokoju wzgl^ednej ciszy - " +
        "nawet w ^srodku dnia, gdy t^lum przekrzykuj^acych si^e mieszczan " +
        "oblega stoj^ace w przeciwleg^lym rogu placu kupieckie stragany. " +
        "Na p^o^lnocy wida^c ";

        if (pora_roku() == MT_ZIMA)
            str += "nieczynn^a w zimie fontann^e";
        else
            str += "nieczynn^a w nocy fontann^e";

        str += ", kt^ora co dzie^n umila czas spacerowiczom swoim koj^acym " +
            "uszy szumem. ";
    }

	if(this_object()->jest_dzien())
	  {
	    str += "Grupa go��bi przechadza si� dostojnie w t� i z powrotem, ";
	    if (pora_roku() == MT_ZIMA && TEMPERATURA(this_object()) <= 0)
	      str += "zostawiaj�c na �niegu drobniutkie �lady pazurk�w, ";
	    str += "podkre�laj�c atmosfer� spokoju radosnym " +
	      "gruchaniem.\n";
	  }
	else
	  str += "Nocna pora odes�a�a spaceruj�ce tu za dnia go��bie "+
		  "do miejsc, gdzie spokojnie b�d� mog�y odnowi� si�y "+
		  "na jutrzejszy dzie�.\n";

	return str;
}

void odnow_golebie()
{
  if(this_object()->jest_dzien() && !present("go��b", this_object()))
    {
      int n = random(4)+2;
      for(int i = 0; i < n; i++)
        clone_object(RINDE_NPC+"golab_plac.c")->move(TO);
    }
}
public string
exits_description() 
{
    return "Dalsza cz^e^s^c placu widoczna jest na p^o^lnocy i wschodzie, "+
           "za^s ulice prowadz^a st^ad na po^ludnie i zach^od.\n";
}

