#include "dir.h"

#include <stdproperties.h>

inherit "/std/room";

void create_room() 
{
    set_short("Kiesze� straganiarza");
    set_long("Jeste� w kieszeni bez dna.\n");

    add_prop(ROOM_I_INSIDE,1);

//dopoki luki niedzialaja! FIXME
//    add_object("/d/Standard/items/bronie/luki/dlugi_cisowy.c", 5);
    add_object("/d/Standard/items/bronie/strzaly/wysmukla_czerwonopiora.c", 200);
    add_object("/d/Standard/items/bronie/noze/maly_poreczny.c", 10);
    add_object("/d/Standard/Redania/Rinde/przedmioty/bronie/miecze/dlugi_stalowy.c", 3);
    add_object("/d/Standard/Redania/Rinde/przedmioty/bronie/topory/poreczny_stalowy.c", 20);
    add_object("/d/Standard/items/bronie/dzidy/prosta_jesionowa.c", 3);
    add_object("/d/Standard/items/bronie/wlocznie/prymitywna_dluga.c", 2);
}
