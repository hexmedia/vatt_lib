/* By Tinardan & Lil. 20.09.2005 */
#include "dir.h"

#include <stdproperties.h>
#include <macros.h>

inherit RINDE_STD;

string czy_noc();
string reszta_opisu();

void create_rinde_street() 
{
	set_short("Miejski rynek");
	set_long("Po�udniowa cz�� miejskiego rynku r�ni znacznie mniejsza "+
	         "powierzchnia od innych jego miejsc, kt�re masz w zasi�gu "+
		 "wzroku. Tu^z pod lini^a kamienic wida� wg��bienie "+
		 "rynsztoku, a w�a�ciwie rynsztoczku, nad kt�rym zr�cznie "+
		 "przeskakuj� zar�wno doro^sli jak i dzieci. Wbrew pozorom, "+
		 "kt�re sprawia� mog� obdarte z tynku ^sciany dom^ow, plac "+
		 "wcale nie wygl^ada przygn^ebiaj^aco i "+
		 "@@czy_noc@@t�tni �yciem@@reszta_opisu@@");

    add_item(({"plac", "rynek"}),
        "Starannie wybrukowany plac, na ^srodku kt^orego wznosi si^e " +
    "zbudowany z czerwonej ceg�y gmach ratusza, stanowi centrum tego " +
    "miasta.\n");
    add_item("ratusz",
        "Budynek ten, cho^c prezentuje si^e dumnie, nie wyr^o^znia si^e " +
    "bynajmniej architektur^a g^ornych lot^ow i nie by^lby nijak " +
    "wyj^atkowym, gdyby nie budz^aca podziw smuk^la wie^za pn^aca si^e " +
	"wysoko ponad dachy innych budowli. Ponadto szczeg^olno^sci temu " +
    "gmachowi dodaje fakt, i^z spe^lnia on niebanaln^a rol^e w ^zyciu " +
    "miasta.\n");
    add_item(({"wie^z^e", "smuk^l^a wie^z^e", "wie^z^e ratusza"}),
        "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic "+
	"niczym palec, wskazuj^acy centrum tego miasta.\n");
    add_item(({"kamienice", "stare kamienice", "pi^etrowe kamienice",
        "stare pi^etrowe kamienice"}),
	"Stare, ponure kamienice wybudowane tutaj zosta^ly na planie kwadratu, " +
    "pozostawiaj^ac wewn^atrz niema^l^a cz^e^s^c terenu, na kt^orej " +
    "powsta^l ^ow plac - miejsce spotka^n, handlu i wszelkich miejskich " +
    "uroczysto^sci.\n");
    add_item("bruk",
        "Ten, a mo^ze raczej ci, kt^orzy wbijali w ziemi^e te kocie ^lby, " +
    "musieli wyla^c z siebie si^odme poty, by wybrukowa^c plac tak " +
    "starannie. Jednak^ze g^ladko^s^c ich powierzchni oraz dok^ladne " +
    "wyprofilowanie kamieni nie jest ju^z zas^lug^a budowniczych a " +
    "jedynie przechodni�w, bezustannie krz^ataj^acych si^e po rynku.\n");
	    
   add_sit("na bruku","na bruku","z bruku",0);
	
	add_exit("placsw.c","zach^od");
	add_exit("placse.c","wsch^od");	

	add_prop(ROOM_I_INSIDE,0);
    add_object(RINDE+"przedmioty/lampa_uliczna");
    add_object(RINDE+"przedmioty/meble/podest_plac");
    dodaj_rzecz_niewyswietlana("niedu�y podest",1);

    add_object(CENTRUM_OBIEKTY + "rynsztok");
    dodaj_rzecz_niewyswietlana("rynsztok");
}
	
string czy_noc()
{
    if(dzien_noc())
    return "nawet o tej porze ";
    
    return "";
}

public string
exits_description() 
{
    return "Dalsza cz^e^s^c placu widoczna jest na wschodzie i zachodzie.\n";
}

/*


Pod �cian�, na niewielkim pode�cie stoi w�drowny �piewak w kolorowym ubraniu 
i cicho przygrywa ballady na lutni. Niewielki t�umek zebra� si� wok� niego 
i z zainteresowaniem s�ucha spokojnej/�ywej/skocznej/sm�tnej/weso�ej melodii. 
(1. noc: wida� niewielki podest, zapewne cz�sto u�ywany, gdy� deski s� 
wy�lizgane i pociemnia�e od �lad�w wielu st�p 2.  je�li rozbili podest: pod 
murem wida� sm�tne resztki jakiej� drewnianej konstrukcji. Jaki� cz�owiek 
krz�ta si� wok� niego, staraj�c si� go naprawi�.)*/

string reszta_opisu()
{
    string str;
    if(jest_rzecz_w_sublokacji(0,"niedu�y podest"))
        str=". Pod �cian� ratusza stoi niedu�y podest, zapewne cz�sto "+
             "u�ywany, gdy� deski s� wy�lizgane i pociemnia�e od �lad�w wielu "+
             "st�p";
    else
        str="";

    str+=".\n";
    return str;
}
