#include "dir.h"

#include <stdproperties.h>
#include <filter_funs.h>

inherit "/std/room";
inherit "/lib/sklep.c";

void create_room() 
{
    set_short("Sklep");
    set_long("@@dlugi_opis");

    add_object(CENTRUM_DRZWI +"ze_sklepu.c");
    add_object("/d/Standard/items/kosz_na_smieci");

    add_object(RINDE +"przedmioty/meble/lada_do_sklepu.c");
    dodaj_rzecz_niewyswietlana("nier^owna drewniana lada", 1);

    add_item(({ "instrukcj�", "tabliczk�" }), "@@standard_shop_sign@@");
    
    add_prop(ROOM_I_INSIDE,1);

    add_npc(RINDE_NPC +"sklepikarz", 1);
        
}
public string
exits_description() 
{
    return "Wyj^scie ze sklepu prowadzi na miejski rynek.\n";
}


string dlugi_opis()
{
    string str;
    
    str = "Drewniana pod^loga, przykryta sporym dywanem "+
          "z kurzu i b^lota, co rusz skrzypi cicho pod naporem twoich "+
          "st^op. ";

    if(sizeof(FILTER_LIVE(all_inventory()))>=2)
    {
        str += "Nieustannie otwieraj^ace sie drzwi sklepu przypominaj^a o "+
               "tym, ^ze do pomieszczenia wszed^l bad^x wyszed^l z niego "+
               "jaki^s potencjalny klient. ";
    }
    if(jest_rzecz_w_sublokacji(0, "nier^owna drewniana lada"))
    {
       str += "Naprzeciw wej^scia postawiono nadgryzion^a ju^z duchem czasu, "+
           "ale nadal czyst^a, drewnian^a lad^e";
    }
    if(jest_rzecz_w_sublokacji(0, "stary niski m�czyzna") ||
       jest_rzecz_w_sublokacji(0, "Eugeniusz"))
    {
        if(jest_rzecz_w_sublokacji(0, "nier^owna drewniana lada"))
        {
        str += ", za kt^or^a stoi krzepki staruszek. ";
        }
        else
        {
        str += ". Krzepki staruszek stoj^acy na ^srodku wygl^ada na lekko "+
            "zdezorientowanego.";
        }
    }
    else
    {

        if(jest_rzecz_w_sublokacji(0, "nier^owna drewniana lada"))
        str += ". ";
        str += "W�a�ciciela tego przybytku nie wida� jednak�e w pobli�u. ";
    }
    
   str += "\n";    

   return str;
}


void
init()
{
    ::init();
    init_sklep();
}
