#include "dir.h"

#include <stdproperties.h>

inherit RINDE_STD;

void create_rinde_street() 
{
   set_short("Miejski rynek");
   set_long("Znajdujesz si^e na poka^xnych rozmiar^ow "+
	    "placu w samym centrum miasta. Otaczaj^a ci^e ze "+
	    "wszech stron stare, pi^etrowe kamienice, "+
	    "kt^ore ponuro spozieraj^a na prawie dwukrotnie "+
	    "wy^zsz^a od nich smuk^l^a wie^z^e ratuszow^a. "+
	    "Panuje tu najwi^ekszy ruch, niezale^znie od pory "+
	    "dnia czy roku. Wszystkie g^l^owne ulice Rinde "+
	    "zbiegaj^a si^e tutaj, dzi^eki czemu ^latwiej trafi^c "+
	    "st^ad dok^adkolwiek.\n");
   
    add_item(({"plac", "rynek"}),
        "Starannie wybrukowany plac, na ^srodku kt^orego wznosi si^e " +
    "zbudowany z czerwonej ceg�y gmach ratusza, stanowi centrum tego " +
    "miasta.\n");
    add_item("ratusz",
        "Budynek ten, cho^c prezentuje si^e dumnie, nie wyr^o^znia si^e " +
    "bynajmniej architektur^a g^ornych lot^ow i nie by^lby nijak " +
    "wyj^atkowym, gdyby nie budz^aca podziw smuk^la wie^za pn^aca si^e " +
	"wysoko ponad dachy innych budowli. Ponadto szczeg^olno^sci temu " +
    "gmachowi dodaje fakt, i^z spe^lnia on niebanaln^a rol^e w ^zyciu " +
    "miasta.\n");
    add_item(({"wie^z^e", "smuk^l^a wie^z^e", "wie^z^e ratusza"}),
        "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic "+
	"niczym palec, wskazuj^acy centrum tego miasta.\n");
    add_item(({"kamienice", "stare kamienice", "pi^etrowe kamienice",
        "stare pi^etrowe kamienice"}),
	"Stare, ponure kamienice wybudowane tutaj zosta^ly na planie kwadratu, " +
    "pozostawiaj^ac wewn^atrz niema^l^a cz^e^s^c terenu, na kt^orej " +
    "powsta^l ^ow plac - miejsce spotka^n, handlu i wszelkich miejskich " +
    "uroczysto^sci.\n");
    add_item("bruk",
        "Ten, a mo^ze raczej ci, kt^orzy wbijali w ziemi^e te kocie ^lby, " +
    "musieli wyla^c z siebie si^odme poty, by wybrukowa^c plac tak " +
    "starannie. Jednak^ze g^ladko^s^c ich powierzchni oraz dok^ladne " +
    "wyprofilowanie kamieni nie jest ju^z zas^lug^a budowniczych a " +
    "jedynie przechodni�w, bezustannie krz^ataj^acych si^e po rynku.\n");
	    
   add_exit(POLNOC_LOKACJE + "ulica1.c","p^o^lnoc");
   add_exit("placw.c","po^ludnie");
   add_exit(ZACHOD_LOKACJE + "ulica5.c","zach^od");
   add_exit("placn.c","wsch^od");
   add_prop(ROOM_I_INSIDE,0);
   add_sit("na bruku","na bruku","z bruku",0);
   add_object(RINDE+"przedmioty/lampa_uliczna");
}

public string
exits_description()
{
    return "Dalsza cz^e^s^c placu widoczna jest na po^ludniu i wschodzie, "+
           "za^s ulice prowadz^a st^ad na p^o^lnoc i zach^od.\n";
}
	
