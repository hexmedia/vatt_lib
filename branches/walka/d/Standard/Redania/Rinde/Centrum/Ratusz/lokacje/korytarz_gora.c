//Korytarz (poczekalnia) na pietrze w ratuszu w Rinde.
// Lil.

inherit "/std/room";

#include <filter_funs.h>
#include <stdproperties.h>
#include "dir.h"
string dlugasny();

void create_room() 
{
    set_short("Poczekalnia");
    set_long("@@dlugasny@@");
    add_exit("korytarz.c",({"schody","schodami na d�","schodami z g�ry"}));
    add_exit("korytarz.c",({"d^o^l","schodami na d�","schodami z g�ry"}));

    add_prop(ROOM_I_INSIDE,1);
    dodaj_rzecz_niewyswietlana("zniszczone drewniane krzes�o", 8);

    add_object(RINDE + "Centrum/Ratusz/obiekty/korytarz_gora_krzeslo", 8);

    add_item(({"�ciany","bia�e �ciany"}),
               "Wiecznie ch�odne bia�e �ciany...\n");
    add_item("lampy",
             "Kilka lamp zwisaj�cych z sufitu rzuca blade �wiat�o na ten "+
              "korytarz.\n");
    add_item(({"pod^log^e", "posadzk^e"}),
              "Tak jak na parterze ratusza, przez ca^ly korytarz ci^agnie "+
              "si^e posadzka wy^lo^zona w czerwono-bia^ly wz^or karo.\n");
              
    add_object(RINDE + "Centrum/Ratusz/drzwi/korytarz_gora");
    
    add_sit("na posadzce", "na posadzce", "z posadzki");
}

public string
exits_description() 
{
    return "S^a tutaj schody prowadz^ace na d^o^l oraz drzwi do gabinetu "+
           "burmistrza.\n";
}

string dlugasny()
{
    string str;
    str="Z powodu wielu interesant�w posiadaj�cych "+
        "pewne sprawy do burmistrza Rinde, z tego "+
	"korytarza uczyniono poczekalni�. ";

    int i, il, il_krzesel;
    object *inwentarz;
    inwentarz=all_inventory(this_object());
    il_krzesel=0;
    il=sizeof(inwentarz);
    for (i = 0; i < il; ++i)
    {
       //if (inwentarz[i]->jestem_super_stolem())
       if (inwentarz[i]->query_name()=="krzes�o" &&
           inwentarz[i]->query_przym()=="zniszczone")
       {
         ++il_krzesel;
       }
    }

    if(il_krzesel==0)
    {
      str+="Nagie, bia�e �ciany o�wietla kilka lamp "+
           "dyndaj�cych z sufitu.";
    }
    if(il_krzesel==1)
    {
      str+="Nagie, bia�e �ciany i ustawione pod jedn� z nich "+
           "stare, zniszczone krzes�o o�wietla kilka lamp dyndaj�cych "+
	   "z sufitu.";
    }
    if(il_krzesel==2)
    {
      str+="Nagie, bia�e �ciany i ustawione pod jedn� z nich "+
          "dwa stare, zniszczone krzes�a o�wietla kilka lamp dyndaj�cych "+
	   "z sufitu.";
    }
    if(il_krzesel==3)
    {
      str+="Nagie, bia�e �ciany i ustawione pod jedn� z nich "+
          "trzy stare, zniszczone krzes�a o�wietla kilka lamp dyndaj�cych "+
	   "z sufitu.";
    }
    if(il_krzesel==4)
    {
      str+="Nagie, bia�e �ciany i ustawione pod jedn� z nich "+
          "cztery stare, zniszczone krzes�a o�wietla kilka lamp dyndaj�cych "+
	   "z sufitu.";
    }
    if(il_krzesel==5)
    {
      str+="Nagie, bia�e �ciany i ustawione pod jedn� z nich "+
          "pi�� starych, zniszczonych krzese� o�wietla kilka lamp dyndaj�cych "+
	   "z sufitu.";
    }
    if(il_krzesel==6)
    {
      str+="Nagie, bia�e �ciany i ustawione pod jedn� z nich "+
          "sze�� starych, zniszczonych krzese� o�wietla kilka lamp dyndaj�cych "+
	   "z sufitu.";
    }
    if(il_krzesel==7)
    {
      str+="Nagie, bia�e �ciany i ustawione pod jedn� z nich "+
         "siedem starych, zniszczonych krzese� o�wietla kilka lamp dyndaj�cych "+
	   "z sufitu.";
    }
    if(il_krzesel==8)
    {
      str+="Nagie, bia�e �ciany i ustawione pod jedn� z nich "+
          "osiem starych, zniszczonych krzese� o�wietla kilka lamp dyndaj�cych "+
	   "z sufitu.";
    }
    
    inwentarz = FILTER_LIVE(inwentarz - ({this_player()}));
    i = sizeof(inwentarz);
    
    switch(i)
    {
        case -1:
        case 0:
            str+=" W tej chwili nikt nie stoi w kolejce.";
            break;
        case 1:
            str+=" W tej chwili tylko jedna osoba stoi w kolejce.";
            break;
        case 2..8:
            str+=" W tej chwili kilka os^ob stoi w kolejce.";
            break;
        default:
            str+=" W tej chwili kolejka ci^agnie si^e a^z do ko^nca poczekalni.";
            break;
    }
    /*str+=" W tej chwili / nikt nie stoi w kolejce / tylko kilka os�b czeka na swoj� kolej / kolejka ci�gnie si� a� do ko�ca poczekalni.";*/
    
    str+="\n";
    return str;
}
