inherit "/std/key";
#include "dir.h"

#define KOD_KLUCZA_ADMINISTRACJA "drzwirinderatuszadministracjaklucz"
#include <stdproperties.h>
#include <pl.h>
#include <materialy.h>

void
create_key()
{
    ustaw_nazwe("klucz");

    set_long("Stalowy klucz z do�� skomplikowanym " +
       "wzorem wci��. Na uchu wygrawerowano "+
       "napis \"Administracja\".\n");

    dodaj_przym("stalowy", "stalowi");
    dodaj_przym("z�bkowany","z�bkowani");

    set_key(KOD_KLUCZA_ADMINISTRACJA);

set_owners(({RINDE_NPC + "sekretarz"}));
}
