/* Opis by Lil & Rivo */

inherit "/std/room";

#include <stdproperties.h>
#include <macros.h>
#include "dir.h"
string dlugasny();

int
czy_przejscie()
{
    if ((present("rindesekretarz") || present("wysoki szczup�y m�czyzna")) && CAN_SEE(present("rindesekretarz"), TP))
    {
        find_living("rindesekretarz")->powiedz_gdy_jest(TP, "powiedz do " +
        OB_NAME(TP) + " Dok^ad, dok^ad? Za chwil^e zamykam ratusz.");
        return 1;
    }
    else
        return 0;
}

void create_room() 
{
	set_short("Korytarz ratusza");
	set_long("@@dlugasny@@");
	
	add_item(({"strome marmurowe schody","strome schody","marmurowe schody",
	           "schody"}), "Schody o bardzo w�skich i wysokich stopniach "+
		   "pn� si� wysoko na g�r�. Otoczone �cianami z obu stron "+
		   "nie posiadaj� nawet por�czy.\n");
	add_item(({"b�yszcz�c� posadzk�","posadzk�","marmurow� posadzk�", "pod^log^e"}),
	           "Starannie wy�o�on^a marmurowymi kaflami posadzk^e ozdabia "+
		   "czerwono-bia�y wz�r karo.\n");

 	add_object(RINDE + "Centrum/Ratusz/obiekty/tablica");
 	add_object(RINDE + "Centrum/Ratusz/obiekty/korytarz_stolik");
	dodaj_rzecz_niewyswietlana("tablica og�oszeniowa");
	dodaj_rzecz_niewyswietlana("niewielki marmurowy stolik");
	
	add_exit("korytarz_gora.c",({"schody","na g�r� po schodach", "z do�u"}), VBFC_ME("czy_przejscie"));
    add_exit("korytarz_gora.c", ({"g^ora", "na g^or^e po schodach", "z do^lu"}), VBFC_ME("czy_przejscie"));
	add_exit("hall",({"hall","do G��wnego Hallu","z korytarza"}), VBFC_ME("czy_przejscie"));
	
 	add_object(RINDE + "Centrum/Ratusz/drzwi/korytarz");
  	add_object(RINDE + "Centrum/drzwi/z_ratusza");

	
	add_prop(ROOM_I_INSIDE, 1);

    add_sit("na posadzce", "na posadzce", "z posadzki", 0);
}

public string
exits_description() 
{
    return "Wyj^scie z ratusza prowadzi na rynek, za^s naprzeciw korytarz " +
           "przechodzi w hall. S^a tutaj tak^ze schody na pi^etro oraz " +
           "drzwi.\n";
}

string dlugasny()
{
   string str;
   if(!dzien_noc())
   { 
     if(jest_rzecz_w_sublokacji(0, "niewielki marmurowy stolik"))
     str="Co do faktu, i� miejsce to jest w^sr^od mieszczan bardzo " +
       "popularne, nie ma w^atpliwo^sci. Nie da si^e r^ownie^z zaprzeczy^c, "+
       "�e najwi�kszym zainteresowaniem "+
       "cieszy si� tu du�a, w ca�o�ci pokryta najrozmaitszymi "+
       "og�oszeniami i notkami tablica. Przed ni� stoi nawet niewielki "+
       "stolik, by nast�pne anonse m�c tworzy� w wygodniejszej pozycji. "+
       "Zaraz obok niego strome, "+
       "marmurowe schody pn� si� w g�r�, a na parterze stapiaj� z b�yszcz�c� "+
       "posadzk� wy�o�on� w czerwono-bia�y wz�r karo. "+
       "Korytarz ci�gnie si� od wyj�cia do g��wnego hallu, po drodze mijaj�c drzwi "+
       "do kolejnego pomieszczenia. Jego przestronne wn�trze niesie i podbija ka�dy "+
       "odg�os, dlatego ratusz sprawia wra�enie wiecznie opanowanego przez "+
       "harmider.";
     else
     str="Co do faktu, i� miejsce to jest w^sr^od mieszczan bardzo " +
       "popularne, nie ma w^atpliwo^sci. Nie da si^e r^ownie^z zaprzeczy^c, "+
       "�e najwi�kszym zainteresowaniem "+
       "cieszy si� tu du�a, w ca�o�ci pokryta najrozmaitszymi "+
       "og�oszeniami i notkami tablica. Zaraz obok niej strome, "+
       "marmurowe schody pn� si� w g�r�, a na parterze stapiaj� z b�yszcz�c� "+
       "posadzk� wy�o�on� w czerwono-bia�y wz�r karo. "+
       "Korytarz ci�gnie si� od wyj�cia do g��wnego hallu, po drodze mijaj�c drzwi "+
       "do kolejnego pomieszczenia. Jego przestronne wn�trze niesie i podbija ka�dy "+
       "odg�os, dlatego ratusz sprawia wra�enie wiecznie opanowanego przez "+
       "harmider.";   
   }    
   else
   {
     if(jest_rzecz_w_sublokacji(0, "niewielki marmurowy stolik"))
     str="Co do tego, i� miejsce to jest nieprzerwanie atakowane "+
       "przez kolejne t�umy ludzi nie ma w�tpliwo�ci, aczkolwiek o tej porze "+
       "ruch powinien i by� mo�e jest troch^e mniejszy. "+
       "Najwi�kszym zainteresowaniem "+
       "cieszy si� tu du�a, w ca�o�ci pokryta najrozmaitszymi "+
       "og�oszeniami i notkami tablica. Przed ni� stoi nawet niewielki "+
       "stolik, by nast�pne anonse m�c tworzy� w wygodniejszej pozycji. "+
       "Zaraz obok niego strome, "+
       "marmurowe schody pn� si� w g�r�, a na parterze stapiaj� z b�yszcz�c� "+
       "posadzk� wy�o�on� w czerwono-bia�y wz�r karo. "+
       "Korytarz ci�gnie si� od wyj�cia do g��wnego hallu, po drodze mijaj�c drzwi "+
       "do kolejnego pomieszczenia. Jego przestronne wn�trze niesie i podbija ka�dy, "+
       "cho�by najbardziej dyskretny odg�os.";
     else
     str="Co do tego, i� miejsce to jest nieprzerwanie atakowane "+
       "przez kolejne t�umy ludzi nie ma w�tpliwo�ci, aczkolwiek o tej porze "+
       "ruch powinien i by� mo�e jest troch^e mniejszy. "+
       "Najwi�kszym zainteresowaniem "+
       "cieszy si� tu du�a, w ca�o�ci pokryta najrozmaitszymi "+
       "og�oszeniami i notkami tablica. Zaraz obok niej strome, "+
       "marmurowe schody pn� si� w g�r�, a na parterze stapiaj� z b�yszcz�c� "+
       "posadzk� wy�o�on� w czerwono-bia�y wz�r karo. "+
       "Korytarz ci�gnie si� od wyj�cia do g��wnego hallu, po drodze mijaj�c drzwi "+
       "do kolejnego pomieszczenia. Jego przestronne wn�trze niesie i podbija ka�dy, "+
       "cho�by najbardziej dyskretny odg�os."; 
    }
          
    str+="\n";
    return str;
}          
