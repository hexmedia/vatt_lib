//opis: yran

inherit "/std/container.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>
#include <mudtime.h>
#include <composite.h>
#include <sit.h>

void
create_container()
{
    ustaw_nazwe("biurko");
    dodaj_przym("wielki", "wielcy");
    dodaj_przym("d^ebowy", "d^ebowi");
    
    set_long("@@dlugi_opis@@");

    make_me_sitable("na", "na wielkim d^ebowym biurku",
        "z wielkiego d^ebowego biurka", 3, PL_MIE, "na");

    ustaw_material(MATERIALY_DR_DAB);
    
    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_VOLUME, 13000);
    add_prop(CONT_I_MAX_VOLUME, 30000);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_MAX_WEIGHT, 100000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    add_subloc("na");

    set_owners(({RINDE_NPC + "sekretarz"}));
}

public int
query_type()
{
    return O_MEBLE;
}

string
dlugi_opis()
{
    string str = "Wielkie biurko z ciemnego d^ebu prezentuje si^e nad podziw " +
    "elegancko, a g^ladki i l^sni^acy blat podkre^sla klas^e tego mebla. " +
    "Po jednej stronie ma trzy szuflady z posrebrzanymi uchwytami w " +
    "kszta^lcie p^o^lkola, natomiast z drugiej znajduje si^e obszerna " +
    "szafka. Ponadto na bokach wyryto w drewnie motywy przedstawiaj^ace " +
    "li^scie paproci pn^ace si^e ku g^orze.  ";

    switch (sizeof(all_inventory(this_object())))
    {
        case 0:
            str += "\n";
            break;
            
        case 1:
            str += "Le^zy na nim " +
            this_object()->subloc_items("na")[..-1] + ".\n";
            break;
            
        default:
            str += "Le^z^a na nim " +
            this_object()->subloc_items("na")[..-1] + ".\n";
            break;
    }
    
    return str;
}
