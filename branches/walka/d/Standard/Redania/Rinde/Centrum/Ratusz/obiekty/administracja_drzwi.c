inherit "/std/door";

#include <pl.h>
#define KOD_DRZWI "drzwirinderatuszadministracja"
#define KOD_KLUCZA_ADMINISTRACJA "drzwirinderatuszadministracjaklucz"
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("d^ebowy", "d^ebowi");
    dodaj_przym("solidny","solidni");

    set_other_room(RINDE + "Centrum/Ratusz/lokacje/korytarz.c");

    set_door_id(KOD_DRZWI);

    set_door_desc("Solidne drzwi z d^ebu, zabezpieczone mocnym zamkiem.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","przez drzwi na korytarz",
                        "przez otwarte drzwi"}));
    //set_pass_mess("przez drewniane drzwi na po�noc");

    set_lock_command("zamknij");
    set_unlock_command("otw�rz");
    set_key(KOD_KLUCZA_ADMINISTRACJA);
    set_lock_name("zamek");
   /* set_lock_mess( ({ "zasuwa zasuwke w drewnianych drzwiach.\n",
        "Slyszysz jakis szczek po drugiej stronie drzwi... jakby ktos " +
        "przesuwal zasuwke?\n", "Zasuwasz zasuwke w drewnianych drzwiach.\n"
}));

    set_unlock_mess( ({ "odsuwa zasuwke w drewnianych drzwiach.\n",
         "Slyszysz jakis szczek po drugiej stronie drzwi... jakby ktos " +
         "przesuwal zasuwke?\n", "Odsuwasz zasuwke w drewnianych drzwiach.\n"
}));*/

    set_lock_desc("Spory, stalowy zamek od drzwi wygl^ada na do^s^c mocny.\n");
    set_pick(59);
    set_open(0);
}
