// opis: valor

inherit "/std/lampa.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>
#include <mudtime.h>

void
create_lamp()
{
    ::create_lamp();
    
    ustaw_nazwe("lampka");
    dodaj_przym("ma^ly", "mali");
    dodaj_przym("olejny", "olejni");
    
    set_long("^Srednich rozmiar^ow klosz o jasnobr^azowym zabarwieniu " +
        "przymocowany zosta^l do ^zelaznego uchwytu. Obudowany ciemnym " +
        "drewnem, na kt^orym dostrzec mo^zna finezyjnie wyrze^xbione " +
        "ga^l^azki. Wewn^atrz szklanego okrycia dostrzegasz knot, a zaraz " +
        "pod nim szklany, owalny zbiorniczek, w kt^orym migocze olej.\n");

    add_prop(OBJ_I_WEIGHT, 1200);
    add_prop(OBJ_I_VOLUME, 700);

    set_time(9223372036854775807);
    set_time_left(9223372036854775807);

    set_owners(({RINDE_NPC + "sekretarz"}));
}

public int
query_type()
{
    return O_INNE;
}
