//Lawa do Hallu Ratusza w Rinde. Lil.
inherit "/std/object";

#include "dir.h"
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("�awa");
    dodaj_przym("drewniany", "drewniani");
    dodaj_przym("d�ugi", "d�udzy");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Ot, proste, drewniane �awy, niemniej jednak "+
             "solidnie wykonane i dosy� spore.\n");
    add_prop(OBJ_I_DONT_GLANCE, 1);
    add_prop(OBJ_I_WEIGHT, 192000);
    add_prop(OBJ_I_VOLUME, 240300);
    add_prop(OBJ_I_NO_GET, "�awa jest zbyt du�a.\n"); /*Lepiej by bylo
                           z komunikatem "Nie zmie�cisz jej w przej�ciu"
			   przy wychodzeniu z ni�, albo przesuwaniu jej.L*/
    add_prop(OBJ_I_VALUE, 99);
    make_me_sitable("na","na d�bowej �awie","z d�bowej �awy",15, PL_MIE, "na");

    set_owners(({RINDE_NPC + "sekretarz"}));
}
