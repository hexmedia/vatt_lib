#include <pl.h>
#include <macros.h>
#include "dir.h"

inherit "/std/window";

void
create_window()
{
    ustaw_nazwe("okno");
    dodaj_nazwy(({"okna", "okien", "oknom", "okna", "oknami", "oknach"}));
    dodaj_przym("wysoki", "wysocy");
    
    set_window_id("OKNO_W_POKOJU_BURMISTRZA_RINDE");
    set_open_desc("");
    set_closed_desc("");
    set_window_desc("Trzy du^ze po^ludniowe okna wpuszczaj^a do pokoju " +
        "wiele ^swiat^la.\n");
    set_open(0);
    set_locked(1);

    set_owners(({RINDE_NPC + "sekretarz"}));
}
