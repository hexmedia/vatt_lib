/* stolik do korytarza Ratusza w Rinde,
      Lil*/


#pragma strict_types

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <materialy.h>
#include <object_types.h>

nomask void
create_container()
{
    ustaw_nazwe("stolik");
    dodaj_przym("niewielki","niewielcy");
    dodaj_przym("marmurowy","marmurowi");

    set_long("Marmurowy stolik b�yszczy niczym lustro. Jego niepozorne rozmiary "+
             "zdaj� si� nie by� proporcjonalne do wagi."+
             "@@opis_sublokacji| Na b�yszcz�cym blacie |na|.|||le�y |le�� |le�y @@\n");


    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 41243);
    add_prop(CONT_I_VOLUME, 34020);

    add_prop(CONT_I_CANT_WLOZ_DO, 1); /* By nie mo�na by�o nic do niego w�o�y� komend� 'wloz do' */

    add_prop(CONT_I_WEIGHT, 342316);
    add_prop(CONT_I_MAX_WEIGHT, 354316);

    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(OBJ_I_VALUE, 299);

    /* dodajemy sublokacj� 'na', by mo�na by�o co� na st� k�a�� */
    add_subloc("na");
    ustaw_material(MATERIALY_MARMUR);
    set_type(O_MEBLE);

    set_owners(({RINDE_NPC + "sekretarz"}));
}
