inherit "/std/key";

#define KOD_KLUCZA_BURMISTRZ "drzwirinderatuszburmistrzklucz"
#include <stdproperties.h>
#include <pl.h>
#include <materialy.h>
#include "dir.h"

void
create_key()
{
    ustaw_nazwe("kluczyk");

    set_long("Niewielkich rozmiar^ow kluczyk ze srebra.\n"); // ze srebra?

    dodaj_przym("ma^ly", "mali");
    dodaj_przym("srebrny", "srebrni");

    set_key(KOD_KLUCZA_BURMISTRZ);

set_owners(({RINDE_NPC + "sekretarz"}));
}
