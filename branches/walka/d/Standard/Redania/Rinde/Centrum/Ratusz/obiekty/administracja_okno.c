#include <pl.h>
#include <macros.h>
#include "dir.h"

inherit "/std/window";

#define PROP "otworzyl_okno_w_administracji"

string dlugi();

void
create_window()
{
    ustaw_nazwe("okienko");
    dodaj_przym("ma^ly", "mali");
    dodaj_przym("kwadratowy", "kwadratowi");
    
    dodaj_nazwy("okno");
    set_other_room(CENTRUM_LOKACJE + "placs.c");
    set_window_id("OKNO_W_POKOJU_ADMINISTRACYJNYM_RINDE");
    set_open_desc("");
    set_closed_desc("");
    set_window_desc(VBFC_ME("dlugi"));
    set_open(0);

    set_owners(({RINDE_NPC + "sekretarz"}));
}

string
dlugi()
{
    string str = "Ma^le, kwadratowe okienko ma swoje miejsce dok^ladnie ";
    
    if (present(RINDE + "Centrum/Ratusz/obiekty/administracja_biurko",
        environment(this_object())))
        str += "nad biurkiem. ";
    else
        str += "naprzeciw drzwi. ";

    if (!(this_object()->query_open()))
        if (!(environment(this_object()))->dzien_noc())
            str += "Cho^c jest zamkni^ete, to i tak przez jego szczeliny " +
                "wpadaj^a tu nieliczne promyki ^swiat^la. ";
        else
            str += "W tej chwili jest zamkni^ete. ";
    else
        str += "W tej chwili jest otwarte. ";

    str += "\n";
    
    return str;
}

int
funkcja()
{
    object sekretarz = find_living("rindesekretarz");
    
    if (present(sekretarz, environment(this_object())))
    {
        switch(this_player()->query_prop(PROP))
        {
            case 1:
                 break;

            case 2:
                 sekretarz->command("powiedz do " + OB_NAME(this_player()) +
                 " Prosz^e nie otwiera^c okna.");
                 break;

            case 3:
                 sekretarz->command("powiedz do " + OB_NAME(this_player()) +
                 " Niech^ze pan" + this_player()->koncowka("", "i") +
                 " zostawi to okno!");
                 break;
                 
            case 4:
                 sekretarz->command("powiedz do " + OB_NAME(this_player()) +
                 " Zostaw to okno!");
                 break;

            case 5:
                 sekretarz->command("powiedz do " + OB_NAME(this_player()) +
                 " Zostaw, do diab^la, to okno!");
                 break;
                 
            case 6:
                 sekretarz->command("powiedz do " + OB_NAME(this_player()) +
                 " No to ^ze^s si^e, cholera, doigra^l" +
                 this_player()->koncowka("", "a", "o") + "!");
                 this_player()->remove_prop(PROP);
                 sekretarz->command("wstan");
                 sekretarz->command("zabij " + OB_NAME(this_player()));
                 break;
                 
            case 7:
                 sekretarz->command("powiedz Bogowie!"); // bodaj sie
                 sekretarz->remove_object();                     // nigdy nie
                 break;                                          // wywolalo :P
        }
    }

    return 1;
}

void
do_open_window(string mess)
{
    ::do_open_window(mess);

    this_player()->add_prop(PROP, this_player()->query_prop(PROP) + 1);
    set_alarm(2.0, 0.0, "funkcja");

    if (this_player()->query_name(0) != "Gjoef")
        write_file("/d/Aretuza/gjoef/otwieranie",
        this_player()->query_name(0) + "    " + ctime(time()) + "\n");
}
