#pragma no_clone
#pragma no_inherit

inherit "/std/object.c";

#include "admin.h"

void
sklonuj(string tresc, string imie)
{
 /* seteuid(getuid());
    
    call_other("/d/Standard/Redania/Rinde/Centrum/Ratusz/admin/dokument.c", "??");
    
    object podanie;
    podanie = clone_object("/d/Standard/Redania/Rinde/Centrum/Ratusz/admin/dokument.c");
    podanie->set_message("\n" + tresc);
    podanie->set_long("Czyje^s podanie adresowane na burmistrza Rinde.\n"); //dobrze?:P
    podanie->init_doku_arg(podanie->query_doku_auto_load());

    mixed polka;
    
    if (wildmatch("[abcdefghi]*", imie))
        polka = ({"pierwsza p^o^lka", "pierwszej p^o^lki", "pierwszej p^o^lce",
                 "pierwsz^a p^o^lk^e", "pierwsz^a p^o^lk^a", "pierwszej p^o^lce", "na"});
    else
        if (wildmatch("[jklmnopqr]*", imie))
            polka = ({"druga p^o^lka", "drugiej p^o^lki", "drugiej p^o^lce",
                 "drug^a p^o^lk^e", "drug^a p^o^lk^a", "drugiej p^o^lce", "na"});
        else
            if (wildmatch("[stuvwxyz]*", imie))
                polka = ({"trzecia p^o^lka", "trzeciej p^o^lki", "trzeciej p^o^lce",
                 "trzeci^a p^o^lk^e", "trzeci^a p^o^lk^a", "trzeciej p^o^lce", "na"});
            else
                polka = "pod";
                
    object *inv = all_inventory(find_object("/d/Standard/Redania/Rinde/Centrum/Ratusz/lokacje/pokoj_burmistrza"));
    podanie->move(inv[member_array(find_object("/d/Standard/Redania/Rinde/Centrum/Ratusz/obiekty/pokoj_burmistrza_regal")->short(),
    all_inventory(find_object("/d/Standard/Redania/Rinde/Centrum/Ratusz/lokacje/pokoj_burmistrza"))->short())], polka); */

    return;
}



void umiesc_w_archiwum(mixed podanie);

string Imie;
mixed Pod;

create_object()
{
    seteuid(getuid());
}

mixed wczytaj_podanie(string imie)
{
    string plik;
    int kat;
    mixed pod;

    imie = lower_case(imie);

    if (file_size(P_NOWE + imie + ".o") > 0) kat = NOWE_P;
    else if (file_size(P_AKTUALNE + imie + ".o") > 0) kat = AKTUALNE_P;
    else if (file_size(P_ODRZUCONE + imie + ".o") > 0) kat = ODRZUCONE_P;
    else if (file_size(P_ZAAKCEPTOWANE + imie + ".o") > 0) kat = ZAAKCEPTOWANE_P;
    else return 0;

    switch (kat)
    {
       case NOWE_P: plik = P_NOWE + imie;
                    break;
       case AKTUALNE_P: plik = P_AKTUALNE + imie;
                    break;
       case ODRZUCONE_P: plik = P_ODRZUCONE + imie;
                    break;
       case ZAAKCEPTOWANE_P: plik = P_ZAAKCEPTOWANE + imie;
                    break;
       default: plik = 0;
                break;
    }

    pod = restore_map(plik)["podanie"];

    pod[0] = kat;

    return pod;
}

void
usun_podania(string imie)
{
    rm(P_NOWE + imie + ".o");
    rm(P_AKTUALNE + imie + ".o");
    rm(P_ODRZUCONE + imie + ".o");
    rm(P_ZAAKCEPTOWANE + imie + ".o");
}

varargs void
zapisz_podanie(mixed podanie, int dupa = 0)
{
    string plik;
    int kat = podanie[0];
    int a, b;

    usun_podania(podanie[1]);

    switch(kat)
    {
       case NOWE_P: plik = P_NOWE + podanie[1];
                    break;
       case AKTUALNE_P: plik = P_AKTUALNE + podanie[1];
                    break;
       case ODRZUCONE_P: plik = P_ODRZUCONE + podanie[1];
                    break;
       case ZAAKCEPTOWANE_P: plik = P_ZAAKCEPTOWANE + podanie[1];
                    break;
       default: plik = 0;
                break;
    }
    
    if (dupa != 0)
    {
        rm(plik + ".o");
        podanie[0] = ZAAKCEPTOWANE_P;
        plik = P_ZAAKCEPTOWANE + podanie[1];
        save_map((["podanie" : podanie]), plik);

        return;
    }

    if (kat = ZAAKCEPTOWANE_P && file_size(plik) > 0)
	umiesc_w_archiwum(wczytaj_podanie(podanie[1]));

    save_map((["podanie" : podanie]), plik);
}

int
zmien_graczowi_nazwisko(string na)
{
    if (na == "" || !na)
    {
        this_player()->catch_msg("Nie poda^le^s nazwiska, jeszcze raz. " +
        "Mo^zesz:\n\t1) zmieni^c nazwisko,\n\t2) da^c pochodzenie,\n\t3) da^c zezwolenie"+
    " na handel,\n\t4) da^c prawo w^lasno^sci nieruchomo^sci lub gruntu,\n\t"+
    "5) inne (r^eczna edycja dokumentu),\n\t9) nic,\n\t0) wyj^scie.\n");
        input_to("zmien_graczowi");
        return 0;
    }
    else
    {
        seteuid(getuid());
        rm(LPATH + "exec/" + Imie + ".c");
        write_file(LPATH + "exec/" + Imie + ".c",
        "inherit \"/std/living\";create_living" +
        "(){ustaw_nazwe(\"kamien\");seteuid(getuid());}int ustaw(){find_pla" +
        "yer(\"" + Imie + "\")->set_surname(\"" + na +
        "\");}int oco(){return 1;}string jakie(){return \"" + na + "\";}");
        zapisz_podanie(Pod, 1);
        this_player()->catch_msg("Petent otrzyma odpowiedni dokument.\n");
        return 1;
    }
}

//wywolywane automatycznie (nie podajemy recznie 'na')
int
zmien_graczowi_pochodzenie(string na)
{
    if (na == "" || !na)
    {
        this_player()->catch_msg("Nie poda^le^s pochodzenia, jeszcze raz. " +
        "Mo^zesz:\n\t1) zmieni^c nazwisko,\n\t2) da^c pochodzenie,\n\t3) da^c zezwolenie"+
    " na handel,\n\t4) da^c prawo w^lasno^sci nieruchomo^sci lub gruntu,\n\t"+
    "5) inne (r^eczna edycja dokumentu),\n\t9) nic,\n\t0) wyj^scie.\n");
        input_to("zmien_graczowi");
        return 0;
    }
    else
    {
		seteuid(getuid());
        rm(LPATH + "exec/" + Imie + ".c");
        write_file(LPATH + "exec/" + Imie + ".c",
        "inherit \"/std/living\";create_living" +
        "(){ustaw_nazwe(\"kamien\");seteuid(getuid());}int ustaw(){find_pla" +
        "yer(\"" + Imie + "\")->set_origin(\""+na+"\");}int oco(){return"+
        " 2;}string jakie(){return \""+na+"\";}"); //string jakie() tu nieuzywane.. na razie
        zapisz_podanie(Pod, 1);
        this_player()->catch_msg("Petent otrzyma odpowiedni dokument.\n");
    }
}

int
zmien_graczowi_nieruchomosc(string gdzie)
{
    if (gdzie == "" || !gdzie)
    {
        this_player()->catch_msg("Nie poda^le^s lokalizacji, jeszcze raz. " +
        "Mo^zesz:\n\t1) zmieni^c nazwisko,\n\t2) da^c pochodzenie,\n\t3) da^c zezwolenie"+
    " na handel,\n\t4) da^c prawo w^lasno^sci nieruchomo^sci lub gruntu,\n\t"+
    "5) inne (r^eczna edycja dokumentu),\n\t9) nic,\n\t0) wyj^scie.\n");
        input_to("zmien_graczowi");
        return 0;
    }
    else
    {
        seteuid(getuid());
        rm(LPATH + "exec/" + Imie + ".c");
        write_file(LPATH + "exec/" + Imie + ".c",
        "inherit \"/std/living\";create_living" +
        "(){ustaw_nazwe(\"kamien\");seteuid(getuid());}int oco(){return"+
        " 4;}string gdzie(){return \"" + gdzie + "\";}");
        zapisz_podanie(Pod, 1);
        this_player()->catch_msg("Petent otrzyma odpowiedni dokument.\n");
        return 1;
    }
}

int
zmien_graczowi_custom(string tresc)
{
    if (tresc == "" || !tresc)
    {
        this_player()->catch_msg("Nie poda^le^s tre^sci, jeszcze raz. " +
        "Mo^zesz:\n\t1) zmieni^c nazwisko,\n\t2) da^c pochodzenie,\n\t3) da^c zezwolenie"+
    " na handel,\n\t4) da^c prawo w^lasno^sci nieruchomo^sci lub gruntu,\n\t"+
    "5) inne (r^eczna edycja dokumentu),\n\t9) nic,\n\t0) wyj^scie.\n");
        input_to("zmien_graczowi");
        return 0;
    }
    else
    {
        seteuid(getuid());
        rm(LPATH + "exec/" + Imie + ".c");
        write_file(LPATH + "exec/" + Imie + ".c",
        "inherit \"/std/living\";create_living" +
        "(){ustaw_nazwe(\"kamien\");seteuid(getuid());}int oco(){return"+
        " 5;}string tresc(){return \"" + tresc + "\";}");
        zapisz_podanie(Pod, 1);
        this_player()->catch_msg("Petent otrzyma odpowiedni dokument.\n");
        return 1;
    }
}

int
zmien_graczowi(string co)
{
   switch (co)
   {
       case "1":
           this_player()->catch_msg("Podaj wybrane nazwisko: ");
           input_to("zmien_graczowi_nazwisko");
           return 1;
           break;

       case "2":
			mixed pod = wczytaj_podanie(Imie);
			zmien_graczowi_pochodzenie(pod[6]);
           return 1;
           break;
           
       case "3":
           seteuid(getuid());
           rm(LPATH + "exec/" + Imie + ".c");
           write_file(LPATH + "exec/" + Imie + ".c",
           "inherit \"/std/living\";create_living" +
           "(){ustaw_nazwe(\"kamien\");seteuid(getuid());}int oco(){return " +
           "3;}");
           zapisz_podanie(Pod, 1);
           this_player()->catch_msg("Petent otrzyma odpowiedni dokument.\n");
           return 1;
           break;
           
       case "4":
           this_player()->catch_msg("Podaj lokalizacj^e nieruchomo^sci lub gruntu: ");
           input_to("zmien_graczowi_nieruchomosc");
           return 1;
           break;
           
       case "5":
           this_player()->catch_msg("Na mocy praw mi^lo^sciwie nam panuj^acego " +
                         "Ja^snie Pana Vizimira I, z woli bo^zej i ludzkiej " +
                         "kr^ola i jedynego prawowitego w^ladcy Redanii, a z " +
                         "decyzji w^ladz miasta Rinde niniejszym og^lasza si^e, " +
                         "i^z z dniem dzisiejszym " + capitalize(Imie) + "... ");
           input_to("zmien_graczowi_custom");
           return 1;
           break;
           
       case "9":
           seteuid(getuid());
           rm(LPATH + "exec/" + Imie + ".c");
           write_file(LPATH + "exec/" + Imie + ".c",
           "inherit \"/std/living\";create_living" +
           "(){ustaw_nazwe(\"kamien\");seteuid(getuid());}int oco(){return " +
           "9;}");
           zapisz_podanie(Pod, 1);
           return 1;
           break;
           
       case "0":
           return 0;
           break;

       default:
           this_player()->catch_msg("Nieprawid^lowa warto^s^c. "+
           "Mo^zesz:\n\t1) zmieni^c nazwisko,\n\t2) da^c pochodzenie,\n\t3) da^c zezwolenie"+
    " na handel,\n\t4) da^c prawo w^lasno^sci nieruchomo^sci lub gruntu,\n\t"+
    "5) inne (r^eczna edycja dokumentu),\n\t9) nic,\n\t0) wyj^scie.\n");
           input_to("zmien_graczowi");
           return 0;
           break;
    }
}

void
zatwierdz_podanie(object kto, string imie, int jakie_poparcie)
{
    imie = lower_case(imie);
    Imie = imie;
    mixed pod;
    pod=wczytaj_podanie(imie);
    Pod=wczytaj_podanie(imie);
        
    if (!pod)
    {
        kto->catch_msg("Osoba o imieniu " + capitalize(imie) + " nie oczekuje na akceptacj^e podania.\n");
        return;
    }

    if (pod[0] != AKTUALNE_P && pod[0] != NOWE_P)
    {
        kto->catch_msg("Osoba o imieniu " + capitalize(imie) + " nie oczekuje na akceptacj^e podania.\n");
	return;
    }       

    if (jakie_poparcie == 1)
    {
        if (member_array(kto->query_real_name(), pod[2]) != -1)
        {
            kto->catch_msg("Mo^zesz tylko raz zaakceptowa^c podanie dla jednej osoby.\n");
            return;
	}
        pod[2] += ({ kto->query_real_name() });
    }
    else
    {
        if (member_array(kto->query_real_name(), pod[3]) != -1)
        {
            kto->catch_msg("Mo^zesz tylko raz zaakceptowa^c podanie dla jednej osoby.\n");
            return;
        }
        pod[3] += ({ kto->query_real_name() });
    }

    kto->catch_msg("Zatwierdzi^l" + kto->koncowka("e^s","a^s") + " podanie " +
    "osoby o imieniu " + capitalize(imie)+". Wobec tego mo^zesz jej teraz:\n\t" +
    "1) zmieni^c nazwisko,\n\t2) da^c pochodzenie,\n\t3) da^c zezwolenie"+
    " na handel,\n\t4) da^c prawo w^lasno^sci nieruchomo^sci lub gruntu,\n\t"+
    "5) inne (r^eczna edycja dokumentu),\n\t9) nic,\n\t0) wyj^scie.\n");
    input_to("zmien_graczowi");
    
    return;
}

void
umiesc_w_archiwum(mixed podanie)
{
    string zapis;
    string skad;
    int kat, a, b;

    kat = podanie[0];

    switch (kat)
    {
        case NOWE_P: skad = "nowych"; break;
        case AKTUALNE_P: skad = "aktualnych"; break;
        case ODRZUCONE_P: skad = "odrzuconych"; break;
        case ZAAKCEPTOWANE_P: skad = "zaakceptowanych"; break;
        default: skad = "nieokre^slonych"; break;
    }

    zapis = "Podanie przeniesione z kategorii poda^n " + skad + " w " + ctime(time(), 1)+".\n";
/*
    b = sizeof(podanie[2]);

    if (b > 0)
    {
        zapis += "W momencie przeniesienia mia^lo poparcie nast^epuj^acych radnych:";
        for (a = 0; a < b; a++) zapis += " " + capitalize(podanie[2][a]) + ",";
        zapis += "\b.\n";
    }

    b = sizeof(podanie[3]);

    if (b > 0)
    {
        zapis += "W momencie przeniesienia mia^lo poparcie nast^epuj^acych cz^lonk^ow:";
        for (a = 0; a < b; a++) zapis += " " + capitalize(podanie[3][a]) + ",";
        zapis += "\b.\n";
    }
*/
    zapis += "Podanie zosta^lo napisane w " + ctime(podanie[5], 1) + ".\n";

    zapis += podanie[4] + "\n\n";

    write_file(P_ARCHIWALNE + podanie[1], zapis);

    rm(P_NOWE + podanie[1] + ".o");
    rm(P_AKTUALNE + podanie[1] + ".o");
    rm(P_ODRZUCONE + podanie[1] + ".o");
    rm(P_ZAAKCEPTOWANE + podanie[1] + ".o");
}

void
wczytaj_z_archiwum(object dla_kogo, string imie)
{
    if (!imie)
    {
	dla_kogo->catch_msg("Musisz poda^c jakie^s imi^e!\n");
	return;
    }
    if (file_size(P_ARCHIWALNE + imie) > 0)
    {
        dla_kogo->more(read_file(P_ARCHIWALNE + imie));
	return;
    }

    dla_kogo->catch_msg("Nie ma takiego podania w archiwum.\n");
}

void
przeczytaj_podanie(object komu, string kogo)
{
    string tekst;
    string kat;
    mixed pod;
    int a,b;

    kogo = lower_case(kogo);

    pod = wczytaj_podanie(kogo);

    if (!pod)
    {
	komu->catch_msg("Nie ma podania osoby o imieniu " + capitalize(kogo) + ".\n");
	return;
    }

    switch(pod[0])
    {
        case NOWE_P: kat = "nowe"; break;
        case AKTUALNE_P: kat = "aktualne"; break;
        case ODRZUCONE_P: kat = "odrzucone"; break;
        case ZAAKCEPTOWANE_P: kat = "zaakceptowane"; break;
        default: kat = "nieokre^slone"; break;
    }

    tekst = "\nAutor podania: " + capitalize(kogo) + ".\nPodanie znajduje si^e w kategorii \"" + kat + "\". Zosta^lo napisane w " + ctime(pod[5]) + " "+pod[6]+".\n";
/*
    if (sizeof(pod[2]) > 0)
    {
        tekst += "Kandydatura autora zosta^la poparta przez nastepuj^acych radnych:";
        b = sizeof(pod[2]);
        for (a = 0; a < b; a++) tekst += " " + capitalize(pod[2][a]) + ",";
        tekst += "\b.\n";
    }
    if (sizeof(pod[3]) > 0)
    {
        tekst += "Autor podania zosta^l poparty przez nastepuj^acych cz^lonk^ow:";
        b = sizeof(pod[3]);
        for (a = 0; a < b; a++) tekst += " " + capitalize(pod[3][a]) + ",";
        tekst += "\b.\n";
    }
*/
    tekst += "Tre^s^c podania:\n";
    tekst += pod[4] + "\n";

    komu->more(tekst);

    return;
}

void
dodaj_nowe_podanie(string text, object autor,string origin)
{
    mixed podanko;
    mixed pod2;

    podanko = ({NOWE_P, autor->query_real_name(), ({ }), ({ }), text, time(),origin });
    pod2 = wczytaj_podanie(autor->query_real_name());
        
    if (pod2)
	umiesc_w_archiwum(pod2);

    zapisz_podanie(podanko);
    
    sklonuj(text, autor->query_real_name());
}

void
przesun_podanie(object kto, string czyje, string gdzie)
{
    mixed pod;
    int kat = 0;
    string nap = "W DZIWNYM MIEJSCU!!! ZG^LO^S B^L^AD";

    gdzie = lower_case(gdzie);
    czyje = lower_case(czyje);

    pod = wczytaj_podanie(czyje);

    if (!pod)
    {
	kto->catch_msg("Nie ma podania osoby o imieniu " + capitalize(czyje) + ".\n");
	return;
    }

    if(pod[0] != NOWE_P && pod[0] != AKTUALNE_P && pod[0] != ODRZUCONE_P)
    {
	kto->catch_msg("Podania mo^zesz przenosi^c jedynie z kategorii poda^n nowych i odrzuconych.\n");
    	    return;
    }
   
   switch(gdzie)
   {
      case "w nowych":
      case "w kategorii nowe":
      case "podanie w nowych":
      case "podanie w kategorii nowe": kat=AKTUALNE_P;
                                           nap="w kategorii poda^n nowych";
           break;
      case "w odrzuconych":
      case "w kategorii odrzucone":
      case "podanie w odrzuconych":
      case "podanie w kategorii odrzucone": kat=ODRZUCONE_P;
                                            nap="w kategorii poda^n odrzuconych";
           break;      
      default: kat=0;
   }

   if(!kat)
   {
       kto->catch_msg("Mo^zesz umieszcza^c podania jedynie w kategoriach \"nowe\" i \"odrzucone\". "+
		              "Aby umie^sci^c podanie w archiwum, nale^zy je usun^a^c. W pozosta^lych kategor"+
		              "iach podania nie mog^a by^c umieszczane w ten spos^ob.\n");
       return;
   }

   pod[0]=kat;

   zapisz_podanie(pod);

   kto->catch_msg("Podanie osoby o imieniu " + capitalize(czyje)+" zosta^lo umieszczone " +nap+".\n");

   return;
}

void
usun_podanie(object kto,string imie)
{
   mixed *pod=wczytaj_podanie(lower_case(imie));
   int kat;
   string skad;

   if(!pod)
   {
       kto->catch_msg("Nie ma podania osoby o imieniu "+capitalize(lower_case(imie))+"!\n");
       return;
   }

   kat=pod[0];

    switch(kat)
    {
        case NOWE_P: skad="nowych";
                     break;
        case AKTUALNE_P: skad="aktualnych";
                     break;
        case ODRZUCONE_P: skad="odrzuconych";
                     break;
        case ZAAKCEPTOWANE_P: skad="zaakceptowanych";
                     break;
        default: skad="nieokre^slonych";
    }

   umiesc_w_archiwum(pod);

   kto->catch_msg("Podanie zosta^lo usuni^ete z kategorii poda^n \"" + skad+"\" i umieszczone w archiwum.\n");
   return;
}

int
czas_powstania(string dir, string arg1, string arg2)
{
   if (file_time(dir+arg1)<file_time(dir+arg2)) return -1;
   if (file_time(dir+arg1)==file_time(dir+arg2)) return (arg1<arg2?-1:1);
   if (file_time(dir+arg1)>file_time(dir+arg2)) return 1;
}

void
listuj_podania(object komu, int jakie)
{
    string kat;
    string kats;
    string *lista;
        int a,b;
        string napis;

    switch(jakie)
    {
        case NOWE_P:          kat=P_NOWE;
                              kats="nowe";
                              break;
        case AKTUALNE_P:      kat=P_AKTUALNE;
                              kats="aktualne";
                              break;
        case ODRZUCONE_P:     kat=P_ODRZUCONE;
                              kats="odrzucone";
                              break;
        case ZAAKCEPTOWANE_P: kat=P_ZAAKCEPTOWANE;
                              kats="zaakceptowane";
                              break;
        case ARCHIWALNE_P:    kat=P_ARCHIWALNE;
                              kats="archiwalne";
                              break;
        default:              kat=P_NOWE;
                              kats="nowe";
     }

     lista=get_dir(kat);
     if(!sizeof(lista))
       {
          komu->catch_msg("Nie ma ani jednego podania w kategorii \""+kats+"\".\n");
          return;
       }
     lista=sort_array(lista,&czas_powstania(kat));
        
         b=sizeof(lista);
        
        napis="W tej chwili w kategorii \""+kats+"\" znajduj^a si^e podania nast^epuj^acych os^ob:\n";
        
        if(jakie!=ARCHIWALNE_P) for(a=0;a<b;a++) napis+=(capitalize(lower_case(lista[a])[0..(strlen(lista[a])-3)])+(strlen(lista[a])<=9?"\t":"") +"\t" + ctime(file_time(kat+lista[a]))+"\n");

        else for(a=0;a<b;a++) napis+=(capitalize(lower_case(lista[a]))+(strlen(lista[a])<=9?"\t":"")+"\t" + ctime(file_time(kat+lista[a]))+"\n");

    komu->catch_msg(napis+"\n");
        return;
}

/*
 * Funkcja listuje katalogi podan.
 */

void
przeglada_podania(object kto, string str)
{
   int co=-1;

   if(!str)
   {
       kto->catch_msg("Co chcesz przejrze^c? Podania czy archiwum?\n");
       return;
   }
   if(str=="podania")
   {
        kto->catch_msg("Jakie podania chcesz przejrze^c? Nowe, odrzucone czy zaakceptowane?\n");
        return;
   }
   if(str=="nowe podania") co=NOWE_P;
   else if(str=="aktualne podania") co=AKTUALNE_P;
   else if(str=="odrzucone podania") co=ODRZUCONE_P;
   else if(str=="zaakceptowane podania") co=ZAAKCEPTOWANE_P;
   else if(str=="archiwum") co=ARCHIWALNE_P;

   if(co!=-1)
      {
        listuj_podania(kto,co);
        return;
      }

   kto->catch_msg("Co chcesz przejrze^c? Podania czy archiwum?\n");

   return;
}
