inherit "/std/door";

#include <pl.h>
#define KOD_DRZWI "drzwirinderatuszadministracja"
#define KOD_KLUCZA_ADMINISTRACJA "drzwirinderatuszadministracjaklucz"
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("d^ebowy", "d^ebowi");
    dodaj_przym("solidny","solidni");

    set_other_room(RINDE + "Centrum/Ratusz/lokacje/administracja.c");

    set_door_id(KOD_DRZWI);

    set_door_desc("Solidne drzwi z d^ebu, zabezpieczone mocnym zamkiem. " +
                  "przymocowana jest do nich mosi^e^zna tabliczka z " +
                  "wyra�nym napisem: \"Administracja\".\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command( ({({"administracja", "drzwi"}),"przez drzwi do administracji","z korytarza"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otw�rz");

    set_key(KOD_KLUCZA_ADMINISTRACJA);
    set_lock_name("zamek");
    set_lock_desc("Spory, stalowy zamek od drzwi wygl^ada na do^s^c mocny.\n");
    set_pick(59);
    set_open(0);
    set_locked(0);
    
    add_cmd_item(({"tabliczk^e", "tabliczk^e na drzwiach", "mosi^e^zn^a " +
        "tabliczke na drzwiach", "mosi^e^zn^a tabliczk^e", "mosi^e^zn^a " +
        "tabliczke z napisem", "tabliczk^e z napisem"}), ({"ob", "obejrzyj",
        "przeczytaj"}), ({"Do drzwi przymocowana jest niewielka, mosi^e^zna "+
        "tabliczka z wyra^xnym napisem: \"Administracja.\"\n",
        "\"Administracja\".\n"}));
        
    add_cmd_item(({"tabliczk^e", "tabliczk^e na drzwiach", "mosi^e^zn^a " +
        "tabliczke na drzwiach", "mosi^e^zn^a tabliczk^e", "mosi^e^zn^a " +
        "tabliczke z napisem", "tabliczk^e z napisem"}), ({"we^x", "podnie^s",
        "zabierz"}), ({"Tabliczka jest zbyt mocno przytwierdzona.\n"}));
}
