//opis: yran

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("fotel");

    dodaj_przym("ciemny", "ciemni");
    dodaj_przym("sk^orzany", "sk^orzani");
   set_type(O_MEBLE);
   ustaw_material(MATERIALY_SK_SWINIA, 50);
   ustaw_material(MATERIALY_DR_DAB, 50);
   make_me_sitable("na","na ciemnym sk^orzanym fotelu","z ciemnego eleganckiego fotela",1, PL_MIE, "na");
    set_long("Masywny fotel z oparciami obity jest czarn^a sk^or^a. Wysokie "+
    "oparcie zapewnia znakomite u^lo^zenie plec^ow, a boki fotela " +
    "pozwalaj^a wygodnie si^e rozsi^a^s^c, dzi^eki czemu jest on doskona" +
    "^lym siedzeniem. Cztery niewielkie n�ki wykonane s^a z ciemnego d^ebu. "+
    "Kunszt wykonania tego mebla podkre^slaj^a wyko^nczenia srebrn^a nici^a.\n");
    
    add_prop(OBJ_I_WEIGHT, 60000);
    add_prop(OBJ_I_VOLUME, 50000);
    add_prop(OBJ_I_VALUE, 2);
    add_prop(OBJ_M_NO_SELL, 1);

    set_owners(({RINDE_NPC + "sekretarz"}));
}
