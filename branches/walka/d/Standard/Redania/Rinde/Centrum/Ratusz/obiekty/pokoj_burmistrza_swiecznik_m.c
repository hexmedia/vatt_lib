//opis: yran

#include "dir.h"
#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>
#include <object_types.h>

inherit "/std/torch";

nomask void
create_torch()
{
    ustaw_nazwe("^swiecznik");
    
    dodaj_przym("ma^ly", "mali");
    dodaj_przym("srebrny", "srebrni");
    
    set_long("Elegancki ^swiecznik stoi na cienkiej n^o^zce, kt^ora " +
    "u do^lu rozga^l^ezia si^e, tworz^ac podstawk^e przypominaj^ac^a kurz^a "+
    "stop^e. Na szczycie znajduje si^e co^s w rodzaju p^lytkiego kielicha " +
    "z umieszczon^a we^n ^swieczk^a. Wypolerowana, srebrna powierzchnia " +
    "przedmiotu mieni si^e w ^swietle pi^eknym blaskiem.\n");
    
    set_type(O_LAMPY);
    ustaw_material(MATERIALY_SREBRO);
    
    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VOLUME, 400);
    
    set_time(MAXINT);
    remove_name("pochodnia");

    set_owners(({RINDE_NPC + "sekretarz"}));
}

int
move(mixed dest, mixed subloc)
{
  object          old;
  int             is_room, rw, rv, is_live_dest, is_live_old,
                  srw, srv, ra, sra, ogrRze, ogrWag, ogrObj, sa,
                  uw,uv,
                  sw,sv;
  mixed           tmp;
  mixed          tmpsubloc;

  ogrRze = 0;
  ogrWag = 0;
  ogrObj = 0;

  if (!dest)
    return 5;
  old = environment(this_object());
  if (stringp(dest))
  {
    call_other(dest, "??");
    dest = find_object(dest);
  }
  if (!objectp(dest))
    dest = old;

  if (subloc == 1)
  {
    move_object(dest);

    obj_subloc = 0;
    subloc = 0;

  }
  else if (old != dest)
  {
    if (!dest || !dest->query_prop(CONT_I_IN) || dest->query_prop(CONT_M_NO_INS))
      return 5;
    if ((old) && (old->query_prop(CONT_M_NO_REM)))
      return 3;
    if (old && old->query_prop(CONT_I_CLOSED) &&
        intp(this_object()->query_subloc()) &&
        (this_object()->query_subloc() == 0))
      return 9;
    if (old && !(intp(this_object()->query_subloc()) && (this_object()->query_subloc() == 0)) &&
        old->query_subloc_prop(this_object()->query_subloc(), CONT_I_CLOSED)) {
      return 9;
    }

    if (old)
      is_live_old = (function_exists("create_container",
            old) == "/std/living");
    is_live_dest = (function_exists("create_container",
          dest) == "/std/living");

    if (old && is_live_old && this_object()->query_prop(OBJ_M_NO_DROP))
      return 2;

    is_room = (int) dest->query_prop(ROOM_I_IS);

    if (!is_live_dest)
    {
      if ((!is_room) && (this_object()->query_prop(OBJ_M_NO_INS)))
        return 4;
      if (dest && dest->query_prop(CONT_I_CLOSED) &&
          intp(subloc) &&
          (subloc == 0))
        return 10;
      if (dest && !(intp(subloc) && (subloc == 0)) &&
          dest->query_subloc_prop(subloc, CONT_I_CLOSED)) {
        return 10;
      }
    }
    else
    {
      if ((!is_live_old) && (this_object()->query_prop(OBJ_M_NO_GET)))
        return 6;
      else if (is_live_old && this_object()->query_prop(OBJ_M_NO_GIVE))
        return 3;
    }

    rw = dest->query_prop(CONT_I_MAX_WEIGHT) -
      dest->query_prop(OBJ_I_WEIGHT);
    if (dest->is_prop_set(CONT_I_MAX_WEIGHT)) {
      ogrWag = 1;
    }
    rv = dest->volume_left();
    if (dest->is_prop_set(CONT_I_MAX_VOLUME)) {
      ogrObj = 1;
    }
    ra = dest->query_prop(CONT_I_MAX_RZECZY) -
      dest->query_prop(CONT_I_IL_RZECZY);
    if (dest->is_prop_set(CONT_I_MAX_RZECZY)) {
      ogrRze = 1;
    }


    if  (subloc != 1) {
      srw = dest->query_subloc_prop(subloc, CONT_I_MAX_WEIGHT) -
        dest->query_subloc_prop(subloc, CONT_I_WEIGHT);
      srv = dest->query_subloc_prop(subloc, CONT_I_MAX_VOLUME) -
        dest->query_subloc_prop(subloc, CONT_I_VOLUME);
      sra = dest->query_subloc_prop(subloc, CONT_I_MAX_RZECZY) -
        dest->query_subloc_prop(subloc, CONT_I_IL_RZECZY);

      if (srw < 0) srw = 0;
      if (srv < 0) srv = 0;
      if (sra < 0) sra = 0;

      if (dest->is_subloc_prop_set(subloc, CONT_I_MAX_WEIGHT)) {
        if (ogrWag) {
          rw = (srw > rw ? rw : srw);
        }
        else {
          rw = srw;
          ogrWag = 1;
        }
      }
      if (dest->is_subloc_prop_set(subloc, CONT_I_MAX_VOLUME)) {
        if (ogrObj) {
          rv = (srv > rv ? rv : srv);
        }
        else {
          rv = srv;
          ogrObj = 1;
        }
      }
      if (dest->is_subloc_prop_set(subloc, CONT_I_MAX_RZECZY)) {
        if (ogrRze) {
          ra = (sra > ra ? ra : sra);
        }
        else {
          ra = sra;
          ogrRze = 1;
        }
      }
    }

    if (rw < 0) rw = 0;
    if (rv < 0) rv = 0;
    if (ra < 0) ra = 0;

    if (!query_prop(HEAP_I_IS))
    {
      if (ogrWag) {
        if (rw < query_prop(OBJ_I_WEIGHT)) {
          return 1;
        }
      }
      if (ogrObj) {
        if (rv < query_prop(OBJ_I_VOLUME)) {
          return 8;
        }
      }
      if (ogrRze) {
        if (ra == 0) {
          return 11;
        }
      }
    }
    else
    {
      sw = 0;
      sv = 0;
      sa = 0;
      if (ogrWag) {
        if (rw < query_prop(OBJ_I_WEIGHT))
        {
          uw = query_prop(HEAP_I_UNIT_WEIGHT);
          if (uw > rw)
          {
            return 1;
          }

          sw = rw / uw; /* Taka ilo�� stosu mo�e by� uniesiona */
          sv = sw;
        }
      }
      if (ogrObj) {
        if (rv < query_prop(OBJ_I_VOLUME))
        {
          uv = query_prop(HEAP_I_UNIT_VOLUME);
          if (uv > rv)
            return 8;

          sv = rv / uv; /* Taka ilo�� stosu mo�e by� uniesiona */
          if (!sw)
            sw = sv;
        }
      }
      if (ogrRze) {
        sa = ra;
        if (sw || sv || sa) {
          this_object()->split_heap((sw < sv) ?
              ((sw < sa) ? sw : sa) : ((sv < sa) ? sv : sa));
        }
      }
      else {
        if (sw || sv)
           this_object()->split_heap((sw < sv) ? sw : sv);
      }
    }

    if (old && old->prevent_leave(this_object()))
      return 7;

    if (dest && dest->prevent_enter(this_object()))
      return 7;

    move_object(dest);
  }

  if (old != dest)
  {
    tmpsubloc = obj_subloc;
    obj_subloc = subloc;

    if (old)
    {
      if (sizeof(filter(all_inventory(old) - ({ this_object() }), &->signal_leave(this_object(), dest))))
        return 7;
      this_object()->leave_env(old, dest, tmpsubloc);
      old->leave_inv(this_object(),dest);
      old->subloc_leave_inv(tmpsubloc, this_object());
    }

    if (dest)
    {
      this_object()->enter_env(dest, old);
      dest->enter_inv(this_object(),old);
      dest->subloc_enter_inv(subloc, this_object());
      (all_inventory(dest) - ({ this_object() }))->signal_enter(this_object(),
old);
    }
  }

  mark_state();
  return 0;
}
