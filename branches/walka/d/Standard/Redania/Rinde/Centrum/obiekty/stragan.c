#pragma strict_types

inherit "/std/living";
inherit "/std/container";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <sit.h>

int ile_rzeczy;
object za_straganem;

nomask void
create_container()
{
    ustaw_nazwe("stragan");
    set_long("@@dlugi_opis@@");
        
    add_prop(CONT_I_MAX_VOLUME, 220000);
    add_prop(CONT_I_VOLUME, 120000);
    add_prop(OBJ_I_NO_GET, "Stragan jest zbyt du^zy, aby^s m^og^l go w " +
        "jakikolwiek spos^ob podnie^s^c.\n");
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_WEIGHT, 15000);
    add_prop(CONT_I_MAX_WEIGHT, 100000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(OBJ_I_DONT_GLANCE, 1);

    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 100000);
}

string
dlugi_opis()
{
    string str;

    str = "Ci^agn^ace si^e wzd^lu^z ulicy kolorowe stragany pe^lne s^a " +
        "przer^o^znych przedmiot^ow, sprowadzanych z ca^lego ^swiata. Chyba " +
        "ka^zdy znajdzie tu co^s dla siebie, gdy^z r^o^znorodno^s^c towar^ow "+
        "jest doprawdy zdumiewaj^aca: od tutejszych i zagranicznych owoc^ow " +
        "i warzyw, przez wielobarwne stroje uszyte z r^o^znych, niekiedy " +
        "nieco dziwacznych i niespotykanych w tych stronach tkanin, a^z po " +
        "najr^o^zniejsze bronie. ";

    if (za_straganem && za_straganem->query_npc())
    {
    switch (sizeof(all_inventory(find_object(za_straganem->query_store_room()))))
    {
        case 0:
            str += "To jednak stanowisko ^swieci zupe^ln^a pustk^a. ";
            ile_rzeczy = 0;
            break;
        case 1:
            str += "Na tym za^s stanowisku le^zy jedynie " +
            find_object(za_straganem->query_store_room())->subloc_items()[..-1] + ". ";
            ile_rzeczy = 1;
            break;
        case 2..4:
            str += "Na tym za^s stanowisku le^z^a jedynie " +
            find_object(za_straganem->query_store_room())->subloc_items()[..-1] + ". ";
            ile_rzeczy = 2;
            break;
        default:
            str += "Na tym za^s stanowisku le^z^a " +
            find_object(za_straganem->query_store_room())->subloc_items()[..-1] + ". ";
            ile_rzeczy = 3;
            break;
    }
    }
    else
    {
    switch (sizeof(all_inventory(this_object())))
    {
        case 0:
            str += "To jednak stanowisko ^swieci zupe^ln^a pustk^a. ";
            ile_rzeczy = 0;
            break;
        case 1:
            str += "Na tym za^s stanowisku le^zy jedynie " +
            this_object()->subloc_items("na")[..-1] + ". ";
            ile_rzeczy = 1;
            break;
        case 2..4:
            str += "Na tym za^s stanowisku le^z^a jedynie " +
            this_object()->subloc_items("na")[..-1] + ". ";
            ile_rzeczy = 2;
            break;
        default:
            str += "Na tym za^s stanowisku le^z^a " +
            this_object()->subloc_items("na")[..-1] + ". ";
            ile_rzeczy = 3;
            break;
    }
    }
    
    if (za_straganem)
    {
        if (za_straganem == this_player())
            str += "\n";
        else
            str += "Naprzeciw ciebie stoi " +
            za_straganem->query_imie(this_player(), PL_MIA) + ", ani na " +
            "chwil^e nie spuszczaj^ac wzroku z twoich r^ak.\n";
    }
    else
        str += "Co wi^ecej, nigdzie w pobli^zu nie " +
        "dostrzegasz w^la^sciciela straganu.\n";
        
    return str;
}

/* wariactwo. kazdy straganiarz musi miec zdefiniowane nastepujace funkcje:
 *
 * PIPIPIPIPI! NIEAKTUALNE DO ODWOLANIA
 *
 * hook_zabr_podrz_wlas_drog(object kto, object co),
 * hook_zabr_podrz_wlas_tan(object kto, object co),
 * hook_zabr_podrz_niewl_wljest(object kto, object co),
 * hook_zabr_podrz_niewl_wlniema(object kto, object co),
 * hook_zabr_druga_rzecz(object kto, object co),
 * hook_zabr_pierwsza_rzecz_vip(object kto, object co),
 * hook_zabr_pierwsza_rzecz(object kto, object co),
 * hook_zabr_podrz_wlas_x(object kto, object co)*,
 *
 * hook_pol_swoj_tan(object kto, object co),
 * hook_pol_swoj_drog_wljest(object kto, object co),
 * hook_pol_swoj_drog_wlniema(object kto, object co),
 * hook_pol_zabrane_nieten(object kto, object co),
 * hook_pol_zabrane_ten(object kto, object co),
 * hook_pol_swoj_ten(object kto, object co),
 * hook_pol_zabrane_zlodziej(object kto, object co)
 *
 * *hook_zabr_podrz_wlas_x - nie wiem, jak inaczej to nazwac... chodzi o taka
 * sytuacje, ze gracz cos zabierze, polozy swoja rzecz, a potem zabierze te
 * swoja rzecz, nie oddawszy tej zabranej.
 */

void
leave_inv(object ob, object to)
{
    ::leave_inv(ob, to);
    
    tell_object(find_player("gjoef"), "leave_inv: " + ob->query_short() +
    " - " + to->query_name() + "\n");
    
    if (to != za_straganem && za_straganem->query_npc())
    {
        if (ob->query_prop("podrzucone_na_stragan") != 0)
        {
            if (ob->query_prop("podrzucone_na_stragan") == to->query_name())
            {
                if (to->query_prop("zabral_ze_straganu") == 1)
                {
                    za_straganem->hook_zabr_podrz_wlas_x(to, ob);
                    ob->remove_prop("podrzucone_na_stragan");
                }
                else
                {
                    if (ob->query_prop(OBJ_I_VALUE) > 400)
                    {
                        za_straganem->hook_zabr_podrz_wlas_drog(to, ob);
                        ob->remove_prop("podrzucone_na_stragan");
                    }
                    else
                    {
                        za_straganem->hook_zabr_podrz_wlas_tan(to, ob);
                        ob->remove_prop("podrzucone_na_stragan");
                    }
                }
            }
            else
            {
                if (environment() ==
                environment(find_player(lower_case(ob->query_prop("podrzucone_na_stragan")))))
                {
                    to->add_prop("zabral_ze_straganu", 1);
                    za_straganem->hook_zabr_podrz_niewl_wljest(to, ob,
                        ob->query_prop("podrzucone_na_stragan"));
                }
                else
                {
                    za_straganem->hook_zabr_podrz_niewl_wlniema(to, ob);
                 // ob->add_prop("zabrane_ze_straganu", 1);
                    to->add_prop("zabral_ze_straganu", 1);
                }
            }
        }
        else
        {
            if (to->query_prop("zabral_ze_straganu") == 1)
            {
                za_straganem->hook_zabr_druga_rzecz(to, ob);
                ob->add_prop("zabrane_ze_straganu", 1);
                to->add_prop("zlodziej", za_straganem->query_name());
                write_file("/d/Aretuza/gjoef/log/stragany",
            ctime(time()) + "    " +to->query_name(0) + "    " +
            TO->query_name(0) + "\n");
                // i tu nie wiem co.
            }
            else
            {
                if (to->query_rasa() ~= "stra^znik" || to->query_rasa() ~=
                "oficer" || to->query_name() == "Neville")
                    za_straganem->hook_zabr_pierwsza_rzecz_vip(to, ob);
                else
                {
                    za_straganem->hook_zabr_pierwsza_rzecz(to, ob);
                    to->add_prop("zabral_ze_straganu", 1);
                    ob->add_prop("zabrane_ze_straganu", 1);
                }
            }
        }
    }
}

void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    tell_object(find_player("gjoef"), "enter_inv: " + ob->query_short() +
    " - " + from->query_name() + "\n");
    
    if (from != za_straganem && za_straganem->query_npc())
    {
        if (from->query_prop("zabral_ze_straganu") == 0)
        {
            if (ob->query_prop("zabrane_ze_straganu") == 0)
            {
                if (ob->query_prop(OBJ_I_VALUE) < 400)
                {
                    za_straganem->hook_pol_swoj_tan(from, ob);
                    ob->add_prop("podrzucone_na_stragan", from->query_name());
                }
                else
                {
                    if (environment() == environment(from))
                    {
                        za_straganem->hook_pol_swoj_drog_wljest(from, ob);
                        ob->add_prop("podrzucone_na_stragan", from->query_name());
                    }
                    else
                    {
                        za_straganem->hook_pol_swoj_drog_wlniema(from, ob);
                        ob->add_prop("podrzucone_na_stragan", from->query_name());
                    }
                }
            }
            else
            {
                if (ob->query_prop("podrzucone_na_stragan"))
                {
                    za_straganem->hook_pol_zabrane_ten(from, ob);
                    from->remove_prop("zabral_ze_straganu");
                    ob->remove_prop("zabrane_ze_straganu"); // jesli jest
                }
                else
                {
                    za_straganem->hook_pol_zabrane_nieten(from, ob);
                    ob->remove_prop("zabrane_ze_straganu");
                }
            }
        }
        else
        {
            if (ob->query_prop("zabrane_ze_straganu") == 0)
            {
                if (ob->query_prop("podrzucone_na_stragan"))
                {
                    za_straganem->hook_pol_zabrane_ten(from, ob);
                    from->remove_prop("zabral_ze_straganu");
                    ob->remove_prop("zabrane_ze_straganu");
                    return;
                }
                else
                {
                    za_straganem->hook_pol_swoj_ten(from, ob);
                    ob->add_prop("podrzucone_na_stragan", from->query_name());
                }
            }
            else
            {
                if (from->query_prop("zlodziej") == za_straganem->query_name())
                {
                    za_straganem->hook_pol_zabrane_zlodziej(from, ob);
                    from->remove_prop("zlodziej");
                }
                else
                {
                    za_straganem->hook_pol_zabrane_ten(from, ob);
                    from->remove_prop("zabral_ze_straganu");
                    ob->remove_prop("zabrane_ze_straganu");
                }
            }
        }
    }
}

void
init()
{
    add_action("stan", "sta^n");
    add_action("wyjdz", "wyjd^x");
}

int
stan(string str)
{
    object *stragan;
    int ret;

    notify_fail("Gdzie chcesz stan^a^c?\n");

    if (!strlen(str))
        return 0;

    if(this_player()->query_prop(SIT_SIEDZACY))
    {
        notify_fail("Przecie^z ju^z gdzie^s siedzisz.\n");
        return 0;
    }
    
    if (za_straganem == this_player())
    {
	notify_fail("Przecie^z ju^z tu stoisz.\n");
	return 0;
    }

    if (TP->query_prop("stoi_za_straganem"))
    {
        notify_fail("Przecie^z ju^z stoisz za jednym ze stragan^ow.\n");
        return 0;
    }

    if (!parse_command(str, all_inventory(this_player()) +
	all_inventory(environment(this_player())),
        "'za' %i:"+ PL_NAR, stragan))
        return 0;

    stragan = NORMAL_ACCESS(stragan, 0, 0);

    if (!sizeof(stragan))
        return 0;

    if (za_straganem)
    {
	notify_fail("Stoi tam kto^s inny.\n");
	return 0;
    }

    za_straganem = this_player();
    this_player()->add_prop(OBJ_I_DONT_GLANCE, 1);
    this_player()->add_prop(LIVE_S_SOULEXTRA, "stoi za straganem");
    this_player()->add_prop(LIVE_M_NO_MOVE, "Najpierw wyjd^x zza straganu.\n");
    this_player()->add_prop("stoi_za_straganem", OB_NAME(this_object()));
    write("Stajesz za " + this_object()->query_short(PL_NAR) + ".\n");
    saybb(QCIMIE(this_player(),PL_MIA)+" staje za "+
          this_object()->query_short(PL_NAR)+".\n");

    return 1;
}

int
wyjdz(string str)
{
    object *stragan;
    int ret;

    notify_fail("Wyjd^x sk^ad?\n");

    if (!strlen(str))
        return 0;

    if (!parse_command(str, all_inventory(environment(this_player())),
        "'zza' %i:"+ PL_DOP, stragan))
        return 0;

    stragan = NORMAL_ACCESS(stragan, 0, 0);

    if (!sizeof(stragan))
        return 0;

    if (za_straganem != this_player())
    {
	notify_fail("Przecie^z si^e tam nie znajdujesz.\n");
	return 0;
    }

    za_straganem = 0;
    this_player()->remove_prop(OBJ_I_DONT_GLANCE);
    this_player()->remove_prop(LIVE_S_SOULEXTRA);
    this_player()->remove_prop(LIVE_S_EXTRA_SHORT);
    this_player()->remove_prop(LIVE_M_NO_MOVE);
    this_player()->remove_prop("stoi_za_straganem");
    write("Wychodzisz zza " + this_object()->query_short(PL_DOP) + ".\n");
    saybb(QCIMIE(this_player(),PL_MIA) + " wychodzi zza " +
          this_object()->query_short(PL_DOP) + ".\n");

    return 1;
}

int
query_humanoid()  //:P
{
    return 0;
}
