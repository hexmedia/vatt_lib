// rynkowy rynsztok. dobry do chowania roznych skarbow.

#include "dir.h"
#include <stdproperties.h>
#include <macros.h>

inherit "/std/container";

void
create_container()
{
    ustaw_nazwe("rynsztok");
    
    set_long("@@dlugi_opis@@");
    
    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(OBJ_M_NO_GET, "Jak chcesz to zrobi^c?\n");
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(OBJ_I_DONT_GLANCE, 1);
}

string
dlugi_opis()
{
    string str = "Biegn^acy przez t^e cz^e^s^c placu rynsztok to " +
    "prowizoryczne wg^l^ebienie w bruku, po brzegi wype^lnione pomyjami wylewanymi " +
    "z okien kamienic. ";
    
    str += "\n";
    
    return str;
}
