inherit "/std/weapon";

#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <wa_types.h>

void create_weapon()
{
	ustaw_nazwe("topor");
	dodaj_przym("prosty", "pro�ci");
	dodaj_przym("jednor�czny", "jednor�czni");
	set_long("Patrzysz na sporych rozmiarow top�r ze l�ni�ym ostrzem. Wygl�da"
		+" on na starannie wykonana i zadbana bro�, cho� jest praktycznie pozbawiony"
		+" jakichkolwiek ozd�b i udziwnie�. Od zwyk�ej siekiery r�ni si� w�a�ciwie"
		+" tylko ksztaltem ostrza, zastosowaniem i zapewne skutecznosci� w walce. Jego"
		+" drewniana r�koje�� jest odpowiednio zabezpieczona przed wy�lizgni�ciem"
		+" si� z r�ki w�adaj�cego nim wojownika.\n");
	
	set_hit(24);
	set_pen(20);

	add_prop(OBJ_I_WEIGHT, 4150);
	add_prop(OBJ_I_VOLUME, 1306);
	add_prop(OBJ_I_VALUE, 2659);

	set_wt(W_AXE);
	set_dt(W_SLASH);
	
	set_hands(A_ANY_HAND);
	ustaw_material(MATERIALY_STAL, 100);
	set_type(O_BRON_TOPORY);
}
