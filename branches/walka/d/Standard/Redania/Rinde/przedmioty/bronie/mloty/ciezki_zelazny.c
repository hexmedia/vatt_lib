inherit "/std/weapon";

#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <wa_types.h>

void create_weapon()
{
	ustaw_nazwe("mlot");
	dodaj_przym("ci�ki", "ci�cy");
	dodaj_przym("�elazny", "�ela�ni");
	set_long("Jest to m�ot stosunkowo prosty w wykonaniu, lecz ubytki w wygl�dzie"
			+" z pewno�ci� r�wnowa�y efektywnosci� u�ycia. Bro� jest starannie"
			+" wywa�ona i dzi�ki temu nawet dla niezbyt wprawnego wojownika,"
			+" posiadaj�cego za to odpowiedni� si��, mo�e by� broni� niezwykle"
			+" skuteczn� i poreczn�. Jego trzon jest obity sk�r� obwiazana rzemieniami," 
			+" by wzmocni� chwyt i uczyni� go wygodniejszym.\n");
	
	set_hit(19);
	set_pen(24);

	add_prop(OBJ_I_WEIGHT, 8210);
	add_prop(OBJ_I_VOLUME, 4654);
	add_prop(OBJ_I_VALUE, 2185);

	set_wt(W_WARHAMMER);
	set_dt(W_BLUDGEON);
	
	set_hands(A_HANDS);
	ustaw_material(MATERIALY_ZELAZO, 100);
	set_type(O_BRON_MLOTY);
}