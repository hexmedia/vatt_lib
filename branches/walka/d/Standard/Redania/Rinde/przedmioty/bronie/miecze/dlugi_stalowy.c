
/* Autor: Avard
   Opis : Tharieal
   Data : 18.08.06
   Info : Tym mieczem powinien walczyc dowodca, 
          albo inna wazna osoba ze strazy Rinde. */

inherit "/std/weapon";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>


void
create_weapon()
{
    ustaw_nazwe("miecz");
    dodaj_przym("d^lugi","d^ludzy");
    dodaj_przym("oficerski","oficerscy");
    
    set_long("Stalow^a r^ekoje^s^c o owalnym przekroju oplataj^a pasy "+
        "czarnej, wzmocnionej sk^ory. Prosta g^lownia przyci^aga oko "+
        "dziwerowaniem, a tak^ze niepospolicie d^lugim, podw^ojnym "+
        "zbroczem. Okr^ag^la g^lowica i masywny jelec miecza zapewniaj^a "+
        "szermierzowi dobr^a ochron^e przed ciosem skierowanym na d^lo^n, "+
        "a tak^ze przed pr^obami rozbrojenia. Na taszce wygrawerowano "+
        "otoczony motywem ro^slinnym herb miasta Rinde. Bro^n jest "+
        "przyzwoicie wywa^zona, a cho^c jej konstrukcja jest prosta i "+
        "niespecjalnie wyszukana, to znakomicie nadaje si^e ona do celu "+
        "w jakim j^a stworzono - egzekwowania porz^adku w mie^scie.\n");
    
    set_hit(26);
    set_pen(25);
    
    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(OBJ_I_VALUE, 5280);
    
    set_wt(W_SWORD);
    set_dt(W_SLASH);

    set_hands(A_ANY_HAND);
    ustaw_material(MATERIALY_STAL, 90);
    ustaw_material(MATERIALY_SK_SWINIA, 10);
    set_type(O_BRON_MIECZE);
}