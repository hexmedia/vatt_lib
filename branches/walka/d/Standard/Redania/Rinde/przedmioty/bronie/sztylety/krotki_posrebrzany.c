/*
 * Sztylet dla kupca-przemytnika w Rinde
 * Autor: Rantaur
 * Data: 10.11.06
 */

inherit "/std/weapon";

#include <materialy.h>
#include <object_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>

void create_weapon() 
{
     ustaw_nazwe("sztylet");
                    
     dodaj_przym("kr�tki", "kr�tcy" );
     dodaj_przym("posrebrzany", "posrebrzani" );
                    
     set_long("Niewielkie ostrze sztyletu pokryte jest pi�knym,"
		+" szlachetnym metalem jakim jest srebro. Mimo,"
		+" i� bro� z powodu swoich rozmiar�w nie wygl�da"
		+" zbyt gro�nie, to w odpowiednio zr�cznych d�oniach"
		+" z pewno�ci� mog�aby zadawa� �miertelne pchni�cia."
		+" Na �rodku klingi wygrawerowano wz�r wyobra�aj�cy"
		+" dwie, splecione ze sob� p�dy dzikiej r�zy.\n");
	
     set_hit(28);
     set_pen(15);

     set_wt(W_KNIFE);
     set_dt(W_IMPALE | W_SLASH);

     set_hands(W_ANYH);
	
     set_type(O_BRON_SZTYLETY);
     ustaw_material(MATERIALY_SZ_SREBRO, 15);
     ustaw_material(MATERIALY_STAL, 85);
     
     add_prop(OBJ_I_VALUE, 1032);
     add_prop(OBJ_I_WEIGHT, 820);
     add_prop(OBJ_I_VOLUME, query_prop(OBJ_I_WEIGHT)/5);
 }

