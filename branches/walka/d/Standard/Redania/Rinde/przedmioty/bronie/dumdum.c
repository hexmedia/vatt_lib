#include <macros.h>
#include <stdproperties.h>
#include <wa_types.h>

inherit "/std/pocisk";

void
create_pocisk()
{
    ustaw_nazwe("strzala");

    dodaj_przym("d^lugi", "d^ludzy");
    dodaj_przym("jesionowy", "jesionowi");

    set_long("Z pozoru niczym nie r^o^zni si^e od innych, jednak sekret tej "+
             "strza^ly tkwi w samym grocie. Mocno nadpi^lowany w kilku "+
             "miejscach, gdy po wystrzeleniu natyka si^e na cia^lo, rozpada "+
             "si^e w drobne igie^lki i rozrywa tkank^e ofiary na strz^epy, "+
             "zapewniaj^ac jej powoln^a ^smier^c w m^eczarniach. To "+
             "t^lumaczy, dlaczego posiadanie tego typu strza^l jest w Redanii "+
             "bezwzgl^ednie karane stryczkiem.\n");
             
    add_prop(OBJ_I_WEIGHT, 200);
    
    set_jakosc(8);
    set_typ(W_IMPALE);
}
