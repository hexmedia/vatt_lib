inherit "/std/weapon";

#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <wa_types.h>

void create_weapon()
{
	ustaw_nazwe("topor");
	dodaj_przym("kr�tki", "kr�tcy");
	dodaj_przym("zdobiony", "zdobieni");
	set_long("Srebrzystobr�zowej barwy top�r ozdobiony jest z�ocistymi ornamentami."
		+" Wz�r wyobra�aj�cy naje�on� kolcami �odyg� r�zy wije si� wzd�u� ca�ego"			
		+" ostrza, tu� przy jego kraw�dzi, jak gdyby chcia� opu�ci� powierzchni�"
		+" metalu przy najbli�szej okazji. Kasztanowa r�koje�� broni jest starannie"
		+" pokryta b�yszcz�cym lakierem, zako�czono j� srebrn� nasadk� na kt�rej"
		+" wygrawerowano faliste linie tak, aby jak najbardziej przypomina�a powierzchni�"
		+" drewna.\n");
	
	set_hit(25);
	set_pen(21);

	add_prop(OBJ_I_WEIGHT, 3910);
	add_prop(OBJ_I_VOLUME, 1040);
	add_prop(OBJ_I_VALUE, 2880);

	set_wt(W_AXE);
	set_dt(W_SLASH);
	
	set_hands(A_ANY_HAND);
	ustaw_material(MATERIALY_STAL, 90);
	ustaw_material(MATERIALY_MOSIADZ, 10);
	set_type(O_BRON_TOPORY);
}
