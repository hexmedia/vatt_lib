/* Palka spod lady Damira,
    Karczmarza"Baszty" w Rinde.
    Fragment operacji "Karczma startowa"
                 by Lil  01.05                             */
                 
/*U�ywany tak�e
 * jako produkt w sklepie (RINDE+"centrum/magazyn_sklepowy")
 */
inherit "/std/weapon";
#include <wa_types.h>
#include <stdproperties.h>
#include <pl.h>
#include <macros.h>

object wielder;

    void
    create_weapon()
{
    ustaw_nazwe(({ "pa�ka","pa�ki","pa�ce","pa�k�","pa�k�","pa�ce"}),
                                ({"pa�ki","pa�ek","pa�kom","pa�ki",
                                   "pa�kami","pa�kach"}),PL_ZENSKI);
    dodaj_przym("d�bowy", "d�bowi");
    dodaj_przym("gruby", "grubi");
    set_long("Drewniana, prosta i gruba do�� pa�ka to bardzo prymitywna "+
                   "bro�, jednak mimo tego stosowana nader cz�sto, bo tania "+
		   "i skuteczna.\n");
    set_hit(14);
    set_pen(12);
    set_wt(W_CLUB);
    set_dt(W_BLUDGEON);
    set_hands(W_ANYH);
    add_prop(OBJ_I_WEIGHT, 2000);
    add_prop(OBJ_I_VOLUME, 1200);
    add_prop(OBJ_I_VALUE, 47);
}
