inherit "/std/weapon";

#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <wa_types.h>

void create_weapon()
{
	ustaw_nazwe("toporek");
	dodaj_przym("por^eczny", "por^eczni");
	dodaj_przym("stalowy", "stalowi");
	set_long("Ostrze tego toporka jest w^askie i wyd^lu^zone, a przy tym "+
		"bardzo cienko kute, co znacznie obni^za wag^e ca^lego or^e^za. "+
		"Jesionowy trzonek broni, jest niezwykle kr^otki i nie przekracza "+
		"d^lugo^sci przedramienia przeci^etnego krasnoluda. Zar^owno waga "+
		"ostrza jak i styliska zosta^la dopasowana w ten spos^ob, by broni^a "+
		"t^a mo^zna by^lo miota^c na spore dystanse, za^s wywa^zony ^srodek "+ 
		"ci^e^zko^sci pozwala na skuteczne celowanie przy minimalnych "+
		"umiej^etno^sciach. Trzonek dodatkowo owini^eeto ciel^ec^a sk^ork^a by "+
		"wzmocni^c uchwyt w czasie miotania. Na ostrzu wypuncowano znak "+
		"skrzy^zowanych m^lot^ow, ^swiadcz^acy o Mahakamskim pochodzeniu "+
		"broni.\n");
	
	set_hit(13);
	set_pen(14);

	add_prop(OBJ_I_WEIGHT, 1200);
	add_prop(OBJ_I_VOLUME, 1130);
	add_prop(OBJ_I_VALUE, 2050);
    /* o ile procent bardziej obiekt rozp�dza si� przy rzucaniu */
    add_prop(OBJ_I_AERODYNAMICZNOSC, 50);

	set_wt(W_AXE);
	set_dt(W_SLASH | W_BLUDGEON);
	
	set_hands(A_ANY_HAND);
	ustaw_material(MATERIALY_STAL, 70);
	ustaw_material(MATERIALY_DR_JESION, 30);
	set_type(O_BRON_TOPORY);
}