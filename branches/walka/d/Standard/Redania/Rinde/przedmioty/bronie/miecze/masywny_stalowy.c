/*
 * Mieczyk dla strazy w Rinde
 * Zplodzony przez Rantaura dnia 10.11.06
 */
 
inherit "/std/weapon";

#include <materialy.h>
#include <macros.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <object_types.h>
#include <pl.h>

void create_weapon() 
{
    ustaw_nazwe("miecz");
    dodaj_przym("masywny", "masywni");
    dodaj_przym("stalowy", "stalowi");

    set_long("Miecz o do�� grubej klindze nie wygl�da na zbyt por�czny, jednak"
    +" z pewno�ci� jest on wytrzyma�y i mo�e s�u�y� w�a�cicielowi przez"
    +" d�ugi czas. R�koje�� broni owini�to czarn�, chropowat� sk�r�, dzi�ki"
    +" kt�rej miecz le�y w d�oni du�o pewniej. Na jelcu za� zauwa�asz niewielkie"
    +" god�o Redanii, wyobra�aj�ce or�a na czerwonym tle.\n");

    set_hit(26);
    set_pen(25);

    set_wt(W_SWORD);
    set_dt(W_IMPALE | W_SLASH);

    set_hands(W_ANYH);

    ustaw_material(MATERIALY_RZ_STAL, 100);
    set_type(O_BRON_MIECZE);

    add_prop(OBJ_I_VALUE, 3000);
    add_prop(OBJ_I_WEIGHT, 4250);
    add_prop(OBJ_I_VOLUME, query_prop(OBJ_I_WEIGHT)/5);
}


