
#include "dir.h"
#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

inherit "/std/object";

create_object()
{
    ustaw_nazwe(({"kawa^lek p^l^otna", "kawa^lka p^l^otna", "kawa^lkowi " +
    "p^l^otna", "kawa^lek p^l^otna", "kawa^lkiem p^l^otna", "kawa^lku" +
    "p^l^otna"}), ({"kawa^lki p^l^otna", "kawa^l^k^ow p^l^otna", "kawa^lkom" +
    "p^l^otna", "kawa^lki p^l^otna", "kawa^lkami p^l^otna", "kawa^lkach " +
    "p^l^otna"}), PL_MESKI_NOS_NZYW);
    
    dodaj_nazwy(({"p^l^otno", "p^l^otna", "p^l^otnu", "p^l^otno", "p^l^otnem",
    "p^l^otnie"}), ({"p^l^otna", "p^l^ocien", "p^l^otnom", "p^l^otna",
    "p^l^otnami", "p^l^otnach"}), PL_NIJAKI_NOS);

    set_long("Niewielki skrawek materia^lu, p^l^otna kiepskiej jako^sci.\n");

    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VOLUME, 200);

    ustaw_material(MATERIALY_TK_LEN);
}
