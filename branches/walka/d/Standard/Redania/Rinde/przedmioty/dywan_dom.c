/*
 * Autor: Rantaur
 * Data: 18.06.06
 * Opis: Dywanik do domu swiatynnego w Rinde
 */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <sit.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>

int zwin_dywan(string str);
int rozwin_dywan(string str);

string opis_zwiniety();

void create_object()
{
  ustaw_nazwe("dywan");
  dodaj_przym("bia�y", "biali");
  dodaj_przym("kosmaty", "kosmaci");

  set_long("Na kosmatym dywanie dostrzegasz kilka t�ustych,"
	   +" woskowych plam powsta�ych zapewne podczas nocnych przechadzek"
	   +" jego w�a�ciciela. Mimo to dywanik wygl�da ca�kiem �adnie,"
	   +" na pewno by�oby mi�o na nim posiedzie�.\n@@opis_zwiniety@@\n");
  add_prop(OBJ_I_WEIGHT, 1050);
  add_prop(OBJ_I_VOLUME, 900);
  add_prop(OBJ_I_VALUE, 860);
  add_prop(OBJ_M_NO_GET, "Dywan jest troch� niepor�czny, Mo�e lepiej"
	   +" by�oby go najpierw zwin��?\n");
  make_me_sitable("na", "wygodnie na dywanie", "leniwie z dywanu", 2);
    set_owners(({RINDE_NPC + "krepp"}));
}

init()
{
  ::init();
  add_action(&zwin_dywan(), "zwi�");
  add_action(&rozwin_dywan(), "rozwi�");
}

int zwin_dywan(string str)
{
  object obj;

  if(!str) return 0;

  if(!parse_command(lower_case(str), environment(this_player()), "%o:"+PL_BIE, obj))
    return 0;

  if (obj != this_object())
    return 0;

  if(!this_object()->query_prop(OBJ_M_NO_GET))
    {
      notify_fail("Przecie� dywan jest ju� zwini�ty!\n");
      return 0;
    }

	if(TO->query_prop(SIT_I_NUM_SIE) || TO->query_prop(SIT_I_NUM_LEZ))
	{
		notify_fail("Kto� tam jest!\n");
	      return 0;
	}


  write("Zwijasz dywan w nieco niezgrabny rulon.\n");
  saybb(QCIMIE(this_player(), PL_MIA)+" zwija "+this_object()->short(PL_BIE)
	+" w nizegrabny rulon.\n");
  remove_prop(OBJ_M_NO_GET); 
  remove_prop("_siedzonko");

	if(ENV(TO)->query_prop(ROOM_I_IS))
		ENV(TO)->remove_sit("na", 2, ({ this_object() }));

  return 1;
}

int rozwin_dywan(string str)
{
  object obj;

  if(!str) return 0;
  if(!parse_command(lower_case(str), environment(this_player()), "%o:"+PL_BIE, obj))
    return 0;

  if(obj != this_object())
    return 0;
  
  if(function_exists("create_container", environment(this_object())) != "/std/room")
    {
      notify_fail("Mo�e najpierw go gdzie� po�o�ysz?\n");
      return 0;
    }

  if(!this_object()->query_prop(OBJ_M_NO_GET))
    {      
      write("Kilkoma sprawnymi ruchami rozwijasz dywan.\n");
      saybb(QCIMIE(this_player(), PL_MIA)+" kilkoma sprawnymi ruchami rozwija "
	    +this_object()->short(PL_BIE)+".\n");
      add_prop(OBJ_M_NO_GET, "Dywan jest troch� niepor�czny. Mo�e lepiej"
	       +" by�oby go najpierw zwin��?\n");

		if((ENV(TO)->query_prop(ROOM_I_IS)) == 1)
			ENV(TO)->add_sit("na", "wygodnie na dywanie", "leniwie z dywanu", 2, ({ this_object() }));
		add_prop("_siedzonko", 1);
      //make_me_sitable("na", "wygodnie na dywanie", "leniwie z dywanu", 2);
      return 1;
    }
  notify_fail("Przecie� dywan jest ju� rozwini�ty!\n");
  return 0;
}

string opis_zwiniety()
{
  if(!this_object()->query_prop(OBJ_M_NO_GET))
    return "W tej chwili jest zwini�ty.";
  return "W tej chwili jest rozwini�ty.";
}



