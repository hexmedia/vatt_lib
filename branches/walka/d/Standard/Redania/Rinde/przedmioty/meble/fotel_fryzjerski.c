/*fotel dla zak�adu fryzjerskiego w Rinde,
   Lil&Tinardan */
inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>

create_object()
{
    ustaw_nazwe("fotel");

    dodaj_przym("fryzjerski","fryzjerscy");
    dodaj_przym("sk�rzany","sk�rzani");

   make_me_sitable("na","na fryzjerskim sk�rzanym fotelu","z fotela",1);

    set_long("Fotel jest mi�kki, wy�cie�any jak�� sk�r�, "+
             " a jego oparcie na tyle wysokie, �e plecy mog� "+
	     "wygodnie si� na nim u�o�y�. Br�zowobury, ale "+
	     "elegancki i schludnie utrzymany, cho� zapewne "+
	     "zosta� nabyty ze wzgl�du na funkcjonalno��, a nie "+
	     "urod�.\n");
    add_prop(OBJ_I_WEIGHT, 11250);
    add_prop(OBJ_I_VOLUME, 22502);
    add_prop(OBJ_I_VALUE, 334);
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_DR_DAB, 50);
    ustaw_material(MATERIALY_SK_NIEDZWIEDZ, 30);
    ustaw_material(MATERIALY_STAL, 20);
    set_owners(({RINDE_NPC + "fryzjer"}));

}

public int
jestem_fotelem_fryzjera()
{
        return 1;
}
