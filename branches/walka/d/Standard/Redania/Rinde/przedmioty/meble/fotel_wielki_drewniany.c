/* Fotel w banku
 * By Adept Bara
 * 04 01 06
 */

inherit "/std/object";

#include "dir.h"
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("fotel");
    dodaj_przym("wielki", "wielcy");
    dodaj_przym("drewniany", "drewniani");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Jest to du^zy fotel, wykonany w ca�o�ci z drewna. "+
             "Niegdy^s zosta^l obity jasnym i mi^ekkim pluszem. Jednak "+
             "teraz w po^lowie jest nadjedzony przez mole.\n");
             
    add_prop(OBJ_I_WEIGHT, 50000); // do poprawki
    add_prop(OBJ_I_VOLUME, 100000); // jw
    add_prop(OBJ_I_VALUE, 58);    // jw

    make_me_sitable("na","na wielkim drewnianym fotelu","z fotela",1);
    set_owners(({RINDE_NPC+"ruygen"}));
}

public int
query_type()
{   
    return O_MEBLE;
}
