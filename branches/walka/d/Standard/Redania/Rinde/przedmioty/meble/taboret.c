inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>

create_object()
{
  ustaw_nazwe("taboret");

  dodaj_przym("ma�y","mali");

  make_me_sitable("na","na taborecie","z taboretu",1);

  set_long("Ma�y, drewniany taboret. Niewielkich rozmiar�w sto�ek nosi �lady cz�stego u�ywania. " +
      "Jego nieheblowane kraw�dzie nie s� zbyt r�wne, co w po��czeniu z powycieranymi miejscami " +
      "nie sprawia wra�enia solidnej roboty. Jednak�e wydaje si�, �e mebel ten potrafi odegra� " +
      "jeszcze swoj� rol�.\n");

  add_prop(OBJ_I_WEIGHT, 5250);
  add_prop(OBJ_I_VOLUME, 8502);
  add_prop(OBJ_I_VALUE, 30);

  set_type(O_MEBLE);
  ustaw_material(MATERIALY_DR_DAB);
    set_owners(({RINDE + "straz/straznik",RINDE+"straz/oficer"}));
}
