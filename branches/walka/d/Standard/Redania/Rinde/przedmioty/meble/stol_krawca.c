/* Autor: Avard
   Opis : Arivia
   Data : 31.03.07 */
inherit "/std/container";

#include "dir.h"
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_container()
{
    ustaw_nazwe("st^o^l");
    dodaj_przym("du^zy", "duzi");
    dodaj_przym("drewniany", "drewniani");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Typowy st^o^l, zbudowany z kilku oheblowanych desek ciemnego "+
        "drewna, przyci^aga uwag^e ze wzgl^edu na swoje rozmiary. G^ladki, "+
        "l^sni^acy blat o poka^xnej powierzchni, idealnie nadaje si^e na "+
        "przechowywanie r^o^znego rodzaju przedmiot^ow. Pomimo prostej "+
        "konstrukcji, nogi sto^lu zosta^ly ozdobione wyrze^xbionymi "+
        "ornamentami, nadaj^ac meblowi eleganckiego wygl^adu. "+
        "@@opis_sublokacji|Dostrzegasz na nim |na|.\n|\n|" + PL_BIE+ "@@");
         
    add_prop(CONT_I_WEIGHT, 20000);
    add_prop(CONT_I_VOLUME, 20000);
    add_prop(OBJ_I_VALUE, 56);
    
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 100000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 150000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    set_owners(({RINDE_NPC+"krawiec"}));

}

