
/* Autor: Avard
   Opis : Artonul
   Data : 22.07.05 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <wa_types.h>
#include <materialy.h>
#include <pl.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("napiersnik");
    dodaj_przym("polerowany","polerowani");
    dodaj_przym("p^lytowy","p^lytowi");
    set_long("Ten wykonany z kilku warstw p^lyt napier^snik powinien by^c "+
        "cz^e^sci^a wyposa^zenia ka^zdego szanuj^acego si^e rycerza. "+
        "Po^l^aczone ze pomoc^a czarnych rzemieni stalowe p^Lyty s^a w "+
        "stanie wyparowa^c wi^ekszo^s^c cios^ow wroga ale niestety trzeba "+
        "za to zap^laci^c wysok^a cene w postaci ograniczenia ruch^ow. "+
        "Ka^zda p^lyta jest pi^eknie wypolerowana co ^swiadczy o szacunku "+
        "kt^orym da^zy t^a rzecz jej w^la^sciciel.\n"); 

    ustaw_material(MATERIALY_STAL, 90);
    ustaw_material(MATERIALY_SK_SWINIA, 10);
    set_type(O_ZBROJE);
    set_slots(A_TORSO, A_STOMACH);
    add_prop(OBJ_I_VOLUME, 8500); 
    add_prop(OBJ_I_VALUE, 6240);   
    add_prop(OBJ_I_WEIGHT, 8500);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
    set_ac(A_TORSO,16,16,13,A_STOMACH,16,16,13);

}
