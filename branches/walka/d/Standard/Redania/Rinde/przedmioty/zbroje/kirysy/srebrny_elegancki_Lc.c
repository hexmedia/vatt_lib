
/* Autor: Avard
   Opis : Kotra
   Data : 27.07.06 
   Uzyte: RINDE/npc/landsknecht.c */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <wa_types.h>
#include <materialy.h>
#include <pl.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("kirys");
    dodaj_przym("srebrny","srebrni");
    dodaj_przym("elegancki","eleganccy");
    set_long("Srebrny, a w^la^sciwie posrebrzany stalowy, kirys sk^lada "+
        "si^e z dw^och r^ownorz^ednie wa^znych cz^e^sci. Przedniej, czyli "+
        "napier^snika, w tym wypadku z wygrawerowanymi wzorami kt^ore "+
        "uk^ladaj^a si^e w przedziwn^a, ale i eleganck^a ca^lo^s^c oraz "+
        "tylniej, zwanej naplecznikiem, w przeciwie^nstwie do napier^snika "+
        "bez ozd^ob. Obie cz^e^sci po^l^aczone s^a ze sob^a dwoma parami "+
        "czarnych, sk^orzanych rzemieni. Jedna para na obu ramionach, "+
        "druga za^s po obu przy pasie. Pomijaj^ac nawet warto^sci "+
        "estetyczne, nie ka^zdego przecie^z sta^c na srebro przy swojej "+
        "zbroi, napier^snik ten posiada do^s^c znaczne walory ochronne "+
        "mimo, ^ze posiadacz sam musi zatroszczy^c si^e o odpowiedni^a "+
        "ochron^e swoich r^ak.\n"); 

    ustaw_material(MATERIALY_STAL, 89);
    ustaw_material(MATERIALY_SK_SWINIA, 1);
    ustaw_material(MATERIALY_SREBRO, 10);
    set_type(O_ZBROJE);
    set_slots(A_TORSO, A_STOMACH);
    add_prop(OBJ_I_VOLUME, 9000); 
    add_prop(OBJ_I_VALUE, 10800);   
    add_prop(OBJ_I_WEIGHT, 9000);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
    set_ac(A_TORSO,16,16,13,A_STOMACH,16,16,13);    
    add_item(({"wzory na","zdobienia na"}), "Wzory wygrawerowane na napier^sniku to g^l^ownie "+
        "zakr^econe S-owate figury kt^ore w wielu miejscach, w dodatku "+
        "nieregularnie, przecinaj^a si^e ze sob^a pod wszelkimi mo^zliwymi "+
        "k^atami.\n", PL_MIE);
}
