/*
 * Spodnie dla strazy w Rinde
 * Rantaur 10.11.06
 */

inherit "/std/armour";

#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <stdproperties.h>
#include <wa_types.h>

void create_armour()
{
    ustaw_nazwe("spodnie");
    dodaj_przym("wzmacniany", "wzmacniani");
    dodaj_przym("sk�rzany", "sk�rzani");
    set_long("Po dok�adnym przyjrzeniu si� stwierdzasz, i� w rzeczywisto�ci"
    +" spodnie z�ozone s� z kilku warstw. Pod br�zow� sk�r� jest prawdopodobnie"
    +" umieszczona druciana siatka, kt�ra nadaje ubraniu wi�ksz� wytrzyma�o��, a"
    +" przede wszystkim chroni w�a�ciciela przed drobniejszymi obra�eniami."
    +" Natomiast od wewn�trznej strony znajduje si� warstwa mi�kkiego,"
    +" bawe�nianego materia�u, dzi�ki kt�remu spodnie s� wygodne i mozna"
    +" nosi� je przez d�ugi czas nie odczuwaj�c �adnego dyskomfortu.\n");

    set_ac(A_THIGHS | A_HIPS, 3, 3, 8);

    ustaw_material(MATERIALY_SK_SWINIA, 85);
    ustaw_material(MATERIALY_RZ_STAL, 15);
    set_type(O_UBRANIA);
    set_size("M");

    add_prop(OBJ_I_WEIGHT, 1200);
    add_prop(OBJ_I_VOLUME, 860);
    add_prop(OBJ_I_VALUE, 600);
}