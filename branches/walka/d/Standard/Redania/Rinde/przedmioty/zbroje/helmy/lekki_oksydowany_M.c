/*
 * Kaptur kolczy dla strazy w Rinde
 * Rantaur 10.11.06
 */
 
inherit "/std/armour.c";

#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
#include <stdproperties.h>
#include <wa_types.h>


void create_armour()
{
	
	ustaw_nazwe( ({"kaptur kolczy", "kaptura kolczego", "kapturowi kolczemu",
			"kaptur kolczy", "kapturem kolczym", "kapturze kolczym"}),
			({"kaptury kolcze", "kaptur�w kolczych", "kapturom kolczym",
			"kaptury kolcze", "kapturami kolczymi", "kapturach kolczych"}), 
			PL_MESKI_NOS_NZYW );
			
	dodaj_nazwy( ({"kaptur", "kaptura", "kapturowi", "kaptur", "kapturem", "kapturze"}),
			({"kaptury", "kaptur�w", "kapturom", "kaptury", "kapturami", "kapturach"}), 
			PL_MESKI_NOS_NZYW );
	
	dodaj_przym("lekki", "lekcy");
	dodaj_przym("oksydowany", "oksydowani");
			
			
	set_long("Kaptury takie jak ten, dzi�ki swej niewielkiej wadze wykorzystywane s�"
		+" przez lekk� piechot� i doskonale sprawdzaj� si� podczas d�ugich marsz�w."
		+" Jego dodatkowym atutem jest du�a swoboda ruchu jaka zapewnia, przez co"
		+" du�o �atwiej unika� w nim cios�w. Ten egzemplarz jest pokryty czarn� oksyd�,"
		+" zapobiegaj�c� szybkiemu rdzewieniu stali.\n");
			
	set_size("M");
	set_ac(A_HEAD, 6, 6, 1);
	ustaw_material(MATERIALY_STAL, 100);
	set_type(O_ZBROJE);
			
	add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
    add_prop(OBJ_I_WEIGHT, 2000);
    add_prop(OBJ_I_VALUE, 960);
			
}