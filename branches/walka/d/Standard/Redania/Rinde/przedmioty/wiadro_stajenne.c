inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

create_object()
{
    ustaw_nazwe(({"wiadro","wiadra","wiadru","wiadro","wiadrem","wiadrze"}),
               ({"wiadra","wiader","wiadrom","wiadra","wiadrami","wiadrach"}),
                  PL_NIJAKI_NOS);
                 
    dodaj_przym("zardzewia�y","zardzewiali");
    dodaj_przym("dziurawy","dziurawi");
   
   make_me_sitable("na","na wiadrze odwracaj�c je denkiem do g�ry","z wiadra",1);
   
    set_long("Ma�e wiadro, kt�re kiedy� zdaje si�, by�o srebrne, teraz "+
             "pokryte jest rdz� a� do granic mo�liwo�ci. "+
	     "Du�a dziura z boku nie pozwala ju� na dalsz� jego "+
	     "eksploatacj�.\n");
    add_prop(CONT_I_WEIGHT, 100);
    add_prop(CONT_I_VOLUME, 422);
    add_prop(OBJ_I_VALUE, 0);
}
