/* Autor: Avard
   Opis : Arivia
   Data : 31.03.07 */
inherit "/std/object";
inherit "/lib/tool";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <tools.h>

void
create_object()
{
    ustaw_nazwe("no^zyczki");
    dodaj_przym("niewielki","niewielcy");
    dodaj_przym("krawiecki","krawieccy");

    set_long("Wielko^sci^a przypominaj^ace m^esk^a d^lo^n no^zyczki, "+
        "zosta^ly w ca^lo^sci wykonane z metalu. Zbudowany z dw^och "+
        "l^sni^acych ostrzy przyrz^ad, s^lu^zy do przecinania materia^lu, "+
        "o czym ^swiadczy charakterystyczny kszta^lt. Zmatowia^ly metal na "+
        "brzegach narz^edzia u^swiadamia, ^ze s^a one w ci^ag^lym u^zytku\n");

    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 90);


    ustaw_material(MATERIALY_STAL);
    add_tool_domain(TOOL_DOMAIN_TAILOR);
}

public mixed
notify_use_tool(object ob, int domain, int sslvl)
{
    return ::notify_use_tool();
}