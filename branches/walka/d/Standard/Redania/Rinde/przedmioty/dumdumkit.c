
#include "dir.h"
#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>
#include <object_types.h>

inherit "/std/object";

create_object()
{
    ustaw_nazwe("zawini^atko");
    dodaj_przym("p^o^lcienny", "p^o^lcienni");
    
    set_long("Jaki^s pod^lu^zny przedmiot zawini^ety w kawa^lek p^l^otna.\n");
    
    add_prop(OBJ_I_WEIGHT, 3000);
    add_prop(OBJ_I_VOLUME, 1800);
    
    ustaw_material(MATERIALY_TK_LEN, 10); //whatever
    ustaw_material(MATERIALY_DR_JESION, 80);
    ustaw_material(MATERIALY_RZ_STAL, 10);
}

int
otworz(string str)
{
    object zawi;

    notify_fail("Otw^orz co?\n");

    if (!str || !parse_command(str, TP, "%o:" + PL_BIE, zawi) || zawi != TO)
	    return 0;

    this_player()->catch_msg("Otwierasz " + QSHORT(TO, 3) +
        ", ods^laniaj^ac ukryte w ^srodku d^lugie jesionowe strza^ly.\n");
    saybb(QCNAME(TP) + " otwiera " + QSHORT(TO, 3) +
        ", ods^laniaj^ac ukryte w ^srodku d^lugie jesionowe strza^ly.\n");

    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    
    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    
    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    clone_object("/d/Standard/Redania/Rinde/przedmioty/bronie/dumdum")->move(TP);
    
    clone_object("/d/Standard/Redania/Rinde/przedmioty/plotno")->move(TP);
    
    TO->remove_object();

    return 1;
}

void
init()
{
    ::init();
    
    add_action(otworz, "otw^orz");
}
