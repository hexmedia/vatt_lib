inherit "/std/object";
inherit "/lib/kwiat";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/cmdparse.h"
#include "/sys/ss_types.h"
#include <object_types.h>

//int powachaj(string str);
string zapach(string str);
string reakcja(string str);

void
create_object()
{
 ustaw_nazwe("roza");
 dodaj_przym("czarny", "czarni");
 dodaj_nazwy("kwiat");
 set_long("Ta wyj�tkowo pi�kna r�a roztacza wok� siebie " +
         "aur� tajemniczo�ci dzi�ki atrementowoczarnej " +
          "barwie, podobnej niebu w bezksi�ycow� noc.\n");
 
 add_prop(OBJ_I_WEIGHT, 50);
 add_prop(OBJ_I_VOLUME, 65);
 add_prop(OBJ_I_VALUE, 24);

 set_type(O_KWIATY);
 set_wplatalny(1);
 
 jak_pachnie="@@zapach@@";
 jak_wacha_jak_sie_zachowuje="@@reakcja@@";
}

int
zgniec(string str)
{
 object lilia;

 if (parse_command(str, all_inventory(this_player()), "%o:" + PL_BIE, lilia) &&
    lilia == this_object())
 { 
  write("Zamykasz w d�oni delikatne p�atki czarnej r�y " +
        "i gwa�townym ruchem mia�d�ysz kwiat w palcach " +
        "wy�adowywuj�c sw� w�ciek�o��. " +
        "Chwil� po tym lekkim ruchem r�ki odrzucasz zniszczony " +
        "kwiat na bok.\n");
  saybb(QCIMIE(this_player(), PL_MIA)+" mru�y oczy z w�ciek�o�ci� " +
        "mia�dzac w d�oni delikatne p�atki czarnej r�y. " +
        "Chwil� potem zniszczony kwiat l�duje na ziemi, a jego " +
        "pomi�te p�atki wiruj� przez chwil� w powietrzu" +
        (environment(this_player())->query_prop(ROOM_I_INSIDE) ?
        " i opadaj� na ziemi�.\n" : " po czym odlatuj� w dal niesione " +
        "podmuchem wiatru.\n"));
  remove_object();
  return 1; 
 }
 notify_fail("Zgnie� co ?\n");
 return 0;       
}

/*
int
powachaj(string str)
{
 if (str == "roze" || str == "czarna roze" || str == "kwiat")
 {
  write("Zbli^zasz powoli do twarzy czarn^a r^o^z^e i zamykasz oczy " +
        "delektuj^ac sie intensywnym, jednak wci^a^z delikatnym " +
        "zapachem, kt^ory przenika ka^zd^a cz^astk^e twojego cia^la.\n");
  saybb(QCIMIE(this_player(), PL_MIA)+" zbli^za do twarzy czarna r^o^ze " +
       "i napawa si^e jej delikatnym zapachem.\n");
 return 1;
 }
 if (str != "roze" || str != "czarna roze" || str != "kwiat")
 {
  write("Co chcesz powachac? Moze jakis kwiat?\n");
  return 1;
 }
}*/
string
zapach(string str)
{
  str="Zbli�asz powoli do twarzy czarn� r�� i zamykasz oczy " +
      "delektuj�c si� intensywnym, jednak wci�� delikatnym " +
      "zapachem, kt�ry przenika ka�d� cz�stk� twojego cia�a.";
  return str;
}

string
reakcja(string str)
{
  str="zbli�a do twarzy czarn� r�� " +
      "i napawa si� jej delikatnym zapachem.";
  return str;
}
