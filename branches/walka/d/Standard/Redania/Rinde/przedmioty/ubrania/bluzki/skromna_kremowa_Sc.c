/*zrobione przez valor dnia 3 listopada 2006
*/
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <object_types.h>
#include <macros.h>
#include <materialy.h>
void
create_armour()
{  
    ustaw_nazwe("bluzka");
    dodaj_przym("skromny","skromni");
    dodaj_przym("kremowy", "kremowi");
    set_long("Kremowa, prosta lecz schludna bluzka zapinana jest "+
"pod sam^a szyj^e drobnymi guziczkami. D^lugie r^ekawy poszerzaj^a si^e "+
"wzd^lu^z ramion by na koncu zako^nczy^c sztywnym mankietem. Jedyn^a ozdob^a "+
"jak^a posiada jest drobniutki haft dooko^la ko^lnie^zyka.\n");
 
    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    add_prop(OBJ_I_WEIGHT, 30); 
    add_prop(OBJ_I_VOLUME, 40);
    add_prop(OBJ_I_VALUE, 50);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    set_size("S");
    ustaw_material(MATERIALY_BAWELNA, 100);
    set_type(O_UBRANIA);
}