
/* Autor: Avard
   Opis : Tinardan
   Data : 19.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
    ustaw_nazwe("sp^odnica");
    dodaj_przym("d^lugi","d^ludzy");
    dodaj_przym("prze^xroczysty","prze^xroczy^sci");

    set_long("Delikatne wzory pokrywaj^ace cieniutki, prze^zroczysty "+
        "materia^l sk^ladaj^a si^e w wi^ekszo^sci z serpentyn, spirali "+
        "i niezwyk^lych splot^ow. Ciemnozielona satyna kontrastuje z "+
        "mocnym, czarnym paskiem, kt^ory tworzy g^or^e sp^odnicy. Pas "+
        "jest zrobiony z czarnej sk^ory i ozdabiaj^a go dwie srebrzone "+
        "klamry, kt^ore s^lu^z^a tak^ze jako zapi^ecie. Tkanina jest "+
        "przytwierdzona do paska jedynie w trzech czwartych jego "+
        "d^lugo^sci, w ten spos^ob, by w pe^lni ods^lania^la jedn^a "+
        "nog^e nosz^acej j^a kobiety, a drug^a okrywa^la a^z poni^zej "+
        "kostki u cz^lowieka. \n");


    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(OBJ_I_VALUE, 108);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
    ustaw_material(MATERIALY_SATYNA, 90);
    ustaw_material(MATERIALY_SK_CIELE, 10);
    set_type(O_UBRANIA);
}