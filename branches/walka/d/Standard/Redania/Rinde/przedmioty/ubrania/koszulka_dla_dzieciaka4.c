
inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <object_types.h>
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
	ustaw_nazwe("koszula");
    dodaj_przym("szorstki","szorstcy");
    dodaj_przym("lniany", "lniani");
    set_long("Szara koszulka zapinana jest na dwa guziczki umieszczone tu^z "+
		"pod brod^a. Szorstki w dotyku materia^l nieprzyjemnie drapie cia^lo, "+
        "jednak niska cena sprawia, ^ze biedniejsi mieszczanie "+
		"mog^a pozwoli^c sobie na jej zakup. Koszulka posiada kr^otkie r^ekawki "+
		"i niedbale postawiony ko^lnierzyk.\n");

 
	set_slots(A_TORSO, A_FOREARMS, A_ARMS);


    add_prop(OBJ_I_WEIGHT, 100); 
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 50);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);

    set_size("XS");

    ustaw_material(MATERIALY_LEN, 100);

set_type(O_UBRANIA);
}