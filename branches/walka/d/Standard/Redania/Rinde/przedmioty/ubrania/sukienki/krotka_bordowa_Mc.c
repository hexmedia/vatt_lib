/* Autor: Valor
   Opis: Arivia
   Dnia: 30.03.07 */
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
    ustaw_nazwe("sukienka");
    dodaj_przym("kr^otki","kr^otcy");
    dodaj_przym("bordowy","bordowi");
    set_long("Jedwabny materia^l przylega g^ladko do cia^la, czyni^ac "+
        "sukienk^e niezwykle delikatn^a i zwiewn^a. Si^egaj^acy do po^lowy "+
        "uda str^oj ods^lania prawie zupe^lnie nogi w^la^scicielki, "+
        "wysmuklaj^ac tym samym jej figur^e. Zauwa^zasz, ^ze sukienka "+
        "trzyma si^e jedynie na cienkich rami^aczkach, ods^laniaj^ac "+
        "g^l^eboki dekolt i plecy, a od pasa w d^o^l materia^l lekko "+
        "rozszerza si^e i marszczy. Masz wra^zenie, ^ze takie odzienie "+
        "doda uroku ka^zdej kobiecie, podkre^slaj^ac jej urod^e. \n");
    set_slots(A_BODY, A_LEGS);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VALUE, 85);
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
    ustaw_material(MATERIALY_JEDWAB, 100);
    set_type(O_UBRANIA);
}
