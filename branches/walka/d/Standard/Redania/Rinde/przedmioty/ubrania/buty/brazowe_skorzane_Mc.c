
/* Autor: Avard
   Opis : Adves
   Data : 07.06.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");

    dodaj_przym("br^azowy","br^azowi");
    dodaj_przym("sk^orzany","sk^orzani");

    set_long("Ju^z na pierwszy rzut oka, buty te, wygl^adaj^a na wygodne i "+
        "stabilne. Mimo, i^z z pozoru wydaj^a si^e by^c zniszczone, to gruba "+
        "sk^ora, z kt^orej zosta^ly wykonane, z pewno^scia wiele jeszcze "+
        "wytrzyma. Si^egaj^a nieco ponad kostk^e i tym samym idealnie j^a "+
        "uszywniaj^a przez co noga, nie musi si^e obawiac ^zadnych "+
        "uszkodze^n, spowodowanych jakimikolwiek nier^owno^sciami terenu. \n");
		
	set_slots(A_FEET);
	add_prop(OBJ_I_VOLUME, 1000);
	add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VALUE, 85);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
	ustaw_material(MATERIALY_SK_SWINIA, 100);
	set_type(O_UBRANIA);
}