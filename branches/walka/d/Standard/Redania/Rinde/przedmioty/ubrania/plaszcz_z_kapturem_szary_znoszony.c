inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>

void
create_armour()
{
    ustaw_nazwe("p�aszcz z kapturem");
    dodaj_nazwy("p�aszcz");

    dodaj_przym("szary", "szarzy");
    dodaj_przym("znoszony", "znoszeni");

    set_long("P�aszcz ten zapewne jest, a raczej by� niezast�pionym " +
	"elementem podczas licznych w�dr�wek. Grubszy materia� oraz " +
	"podszywka sprawiaj�, �e nawet podczas zimnych dni pozwala " +
	"utrzyma� jak najd�u�ej ciep�o. Kr�j zgrabnie dopasowany do " +
	"cia�a, a materia� si�ga a� do kolan, rozszerzaj�c si� lekko " +
	"u do�u. W tej chwili jednak na d�ugo na pewno si� nie przyda, " +
	"bowiem nie do�� �e kolor zblak� znacznie, to jeszcze w wielu " +
	"miejscach mo�na dostrzec dziury i przetarcia, wynikaj�ce z " +
	"cz�stego noszenia.\n");

    set_slots(A_CLOAK);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_SK_BYDLE, 80);
    ustaw_material(MATERIALY_LEN, 20);
    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 26);
//    set_ac();
}