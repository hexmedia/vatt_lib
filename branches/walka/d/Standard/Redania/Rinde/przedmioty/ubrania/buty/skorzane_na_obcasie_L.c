
/* 
opis popelniony przez Faeve, zakodowany przez Seda 10.03.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>


void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty na obcasie");
    dodaj_nazwy("buty");

    dodaj_przym("sk�rzany","sk�rzani");

    set_long("Buty uszyte tak, by stopa wygl�da�a w nich zgrabnie "
	+"i elegancko. Si�gaj�ca nieco ponad kostk� cholewka oraz niewysoki "
	+"obcasik, kt�ry nadaje butkom i stopom gracji, sprawiaj�, �e obuwie jest "
	+"wygodne i mo�na w nim do�� d�ugo pochodzi�, nie czyni�c stopom krzywdy. "
	+"Dodatkow� zalet� jest to, �e czubki but�w s� nieco wyd�u�one, a na "
	+"ko�cach lekko zaokr�glone, co pozwala na swobodne u�o�enie palc�w.\n");

    set_slots(A_FEET);
    add_prop(OBJ_I_WEIGHT, 120);
    add_prop(OBJ_I_VOLUME, 2400);
    add_prop(OBJ_I_VALUE, 100);
    

    add_prop(ARMOUR_F_PRZELICZNIK, 10.0);
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    set_size("L");
    ustaw_material(MATERIALY_SK_SWINIA);
    add_prop(ARMOUR_F_POW_DL_STOPA, 2.75);
    add_prop(ARMOUR_F_POW_SZ_STOPA, 2.75);
}