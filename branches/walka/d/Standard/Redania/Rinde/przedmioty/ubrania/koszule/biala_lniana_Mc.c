
/* Autor: Avard
   Opis : Zwierzaczek
   Data : 17.08.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("koszula");
    dodaj_przym("bia^ly","biali");
    dodaj_przym("lniany","lniani");
    set_long("Ta cz^e^s^c ubrania wykonana zosta^la ze ^sredniej jako^sci "+
        "lnu. Bia^ly kolor, nieco w niekt^orych miejscach prawie "+
        "niezauwa^zalnie po^z^o^lkly, sprawia, ^ze koszula wydaje si^e "+
        "nieco bardziej elegancka ni^z jest w rzeczywisto^sci. Praktycznie "+
        "rzecz bior^ac jest ona zgrabnym po^l^aczeniem subtelnie dobrego "+
        "wygl^adu z funkcjonalno^sci^a. Zapewne stworzona zosta^la z "+
        "my^sl^a o noszeniu jej w wolnym czasie, na pewno nie na jakie^s "+
        "bankiety lub do pracy. \n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    add_prop(OBJ_I_WEIGHT, 300);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 25);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_LEN, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}