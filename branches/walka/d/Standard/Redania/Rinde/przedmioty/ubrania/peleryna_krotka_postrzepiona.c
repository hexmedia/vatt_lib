inherit "/std/armour.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>

void
create_armour()
{

   ustaw_nazwe( ({"peleryna", "peleryny", "pelerynie", "peleryn�", "peleryn�", 
                "pelerynie"}),
                ({"peleryny", "peleryn", "pelerynom", "peleryny", "pelerynami",
                  "pelerynach"}), PL_ZENSKI);
   dodaj_przym("kr�tki","kr�tcy");
   dodaj_przym("postrz�piony","postrz�pieni");   

   set_long("Ta poprzecierana w niekt�rych miejscach peleryna, na kt�r� "
          + "spogl�dasz nie charakteryzuje si� niczym wyj�tkowym. Mo�e za "
          + "wyj�tkiem tego, i� jest trudnego do okre�lenia koloru. Niczym "
          + "zmieszana przegni�a ziele� z szaro�ci� i brudem ulicy. Strz�py "
          + "na ko�cach tej peleryny dope�niaj� tylko widoku marno�ci tego "
          + "kawa�ka szmaty, jak i osoby go nosz�cej.\n");

   set_slots(A_CLOAK);
   add_prop(OBJ_I_WEIGHT, 770);
   add_prop(OBJ_I_VALUE, 11);
   set_type(O_UBRANIA);
   add_prop(OBJ_I_VOLUME, 1000);
}
