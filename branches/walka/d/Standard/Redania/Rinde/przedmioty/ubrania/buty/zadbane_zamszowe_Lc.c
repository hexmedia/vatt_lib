
/* zadbane zamszowe buty zielarza z Rinde
opis popelniony przez Tabakiste, na forum vattgherna 18.02.2007
zakodowany przez Seda 05.03.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>


void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");

    dodaj_przym("zadbany","zadbane");
    dodaj_przym("zamszowy","zamszowe");

    set_long("Zamszowa sk^ora, z kt^orej wykonano ta par^e but^ow, zosta^la "
	+"zabarwiona na ciemnozielony kolor. Wzd^lu^z wysokich cholewek "
	+"znajduj^a si^e rz^edy niezwykle delikatnych, srebrnych klamerek, "
	+"wprawdzie pe^lni^acych jedynie funkcj^e ozdoby, lecz z pewno^sci^a "
	+"dodaj^acych obuwiu elegancji. Podobnie zreszt^a jak ostro zako^nczone "
	+"noski. Buty sprawiaj^a wra^zenie wygodnych, za^s grube podeszwy na pewno "
	+"wytrzymaj^a d^lugie lata.\n");

    set_slots(A_FEET | A_SHINS);
    add_prop(OBJ_I_WEIGHT, 120);
    add_prop(OBJ_I_VOLUME, 2400);
    add_prop(OBJ_I_VALUE, 400);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_F_PRZELICZNIK, 10.0);
	add_prop(ARMOUR_I_DLA_PLCI, 0);
	set_size("L");
    ustaw_material(MATERIALY_SZ_SREBRO, 10);
	ustaw_material(MATERIALY_SK_SWINIA, 30);
	ustaw_material(MATERIALY_SK_CIELE, 60);
    add_prop(ARMOUR_F_POW_DL_STOPA, 2.75);
    add_prop(ARMOUR_F_POW_SZ_STOPA, 2.75);
}

