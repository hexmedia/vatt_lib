/* Artonul
 */
inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");

    dodaj_przym("szary", "szarzy");
    dodaj_przym("prosty", "pro�ci");

    set_long("Spodnie kt�re masz przed sob� s� na pewno dzie�em " +
	"do�wiadczonego krawca. Mimo i� wykonano je z niskiej jako�ci " +
	"jedwabiu i lnu to jednak dzi�ki temu �e zastosowano przy ich " +
	"szyciu bardzo dobry kr�j b�d� na pewno dobrze i d�ugo s�u�y� " +
	"swemu u�ytkownikowi.\n");

    set_slots(A_LEGS);
//    set_ac();
}