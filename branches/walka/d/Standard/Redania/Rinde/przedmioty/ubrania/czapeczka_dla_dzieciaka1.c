/*zrobione przez valora dnia 11 lipca
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

string

create_armour()
{  
    ustaw_nazwe("czapeczka");
    dodaj_przym("kolorowy","kolorowi");
    dodaj_przym("finezyjny","finezyjni");


   set_long("Bordowa czapeczka wyszywana jest kolorowymi ni^cmi, kt^ore "+
	   "przeplataj^a si^e wzajemnie, tworz^ac r^o^znobarwne "+
	   "wzory. Na czubku przyszyto niewielkich rozmiar^ow czerwony guziczek, "+
	   "a wok^o^l niego mniejsze o jagodowej barwie. Brzegi czapki zawini^ete "+
	   "zosta^ly do ^srodka, a z prawej strony w^etkni^eto w rondo ba^zancie "+
	   "pi^oro. \n");


    set_slots(A_HEAD);
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VOLUME, 150);
    add_prop(OBJ_I_VALUE, 100);
    

    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("S");
   
    ustaw_material(MATERIALY_SK_CIELE, 100);
   
    set_type(O_UBRANIA);
}