
/* 
opis popelniony przez Samaie, zakodowany przez Seda 07.08.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
   ustaw_nazwe("spodnie");
    dodaj_przym("w^aski", "w^ascy");
    dodaj_przym("czarny","czarni");


    set_long("Te w^askie spodnie, z czarnego, dosy^c szorstkiego materia^lu, "
	+"zosta^ly niew^atpliwie uszyte przez zdolnego krawca. R^owne szwy zosta^ly "
	+"urozmaicone srebrzyst^a nici^a, kt^ora b^lyszczy si^e przy ka^zdym ruchu. "
	+"Posiadaj^a dwie niewielkie kieszenie umieszczone na po^sladkach, oraz "
	+"podwini^ete nogawki.\n");

    set_slots(A_LEGS | A_HIPS);
    add_prop(OBJ_I_WEIGHT, 600);
    add_prop(OBJ_I_VOLUME, 800);
    add_prop(OBJ_I_VALUE, 90);

	set_size("M");
    add_prop(ARMOUR_F_PRZELICZNIK, 20.0);
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    ustaw_material(MATERIALY_LEN);
}
