/*zrobione przez valor dnia 4 listopada 2006
*/
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour() 
{
      ustaw_nazwe("sp^odnica");
     dodaj_przym("pomarszczony","pomarszczeni");
     dodaj_przym("d^lugi","d^ludzy");
      
     set_long("Sp^odnica si^egaj^aca troch^e poni^zej kolan, pomarszczona "+
         "zosta^la w^askimi sciegami, przez co materia^l przypomina kor^e "+
         "drzewa. Be^zow^a tkanin^e przyozdobiono kilkoma kolorowymi "+
         "motylkami naszytymi na p^l^otno, brzegi natomiast obszyto "+
         "fioletowymi ni^cmi tworz^acymi wzorek przypominaj^acy powyginane "+
         "ga^l^ezie wierzby.\n");
     set_slots(A_LEGS | A_HIPS);
     add_prop(OBJ_I_VOLUME, 100);
     add_prop(OBJ_I_VALUE, 75);
     add_prop(OBJ_I_WEIGHT, 60);
     add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
     add_prop(ARMOUR_I_DLA_PLCI, 1);
     
     set_size("S");
     ustaw_material(MATERIALY_ATLAS, 100);
     set_type(O_UBRANIA);
}
