inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");

    dodaj_przym("jasny", "ja�ni");
    dodaj_przym("bufiasty", "bufia�ci");

    set_long("W miar� d�ugie spodnie zosta�y zrobione na osob� wysok�, " +
	"o d�ugich nogach. Ich miodowy kolor wraz z bufiastym krojem " +
	"sprawia, �e na ustach wielu os�b wykwita u�miech. Wykonane z " +
	"mi�kkiego w dotyku materia�u, zapewne z my�l� o osobach " +
	"lubi�cych odmian� i nieco finezyjny styl ubierania. Bufiaste " +
	"na udach, stercz� figlarnie, za� ich nogawki rozszerzaj� si� " +
	"u do�u nieznacznie. Dodatkowo wzd�u� bok�w ci�gnie si� ciemny " +
	"paseczek, b�d�cy jakby ozdobnikiem.\n");

    set_slots(A_LEGS);
//    set_ac();
}