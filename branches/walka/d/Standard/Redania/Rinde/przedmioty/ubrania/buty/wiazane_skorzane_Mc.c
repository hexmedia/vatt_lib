/* Autor: Valor
   Opis: Arivia
   Dnia: 30.03.07 */
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");
    dodaj_przym("wi^azany","wi^azani");
    dodaj_przym("sk^orzany","sk^orzani");
    set_long("Wysokie, si^egaj^ace kolana buty, zosta^ly wykonane ze sk^ory "+
        "zabarwionej na granatowy kolor. Delikatne w dotyku obuwie idealnie "+
        "przylega do nogi, oplataj^ac ciasno ^lydk^e, w czym pomaga d^lugi, "+
        "br^azowy rzemie^n, kt^orym mo^zesz obwi^aza^c buty. Zgrabny kr^oj "+
        "sprawia wra^zenie, ^ze b^ed^a one idealnie pasowa^ly na damskie "+
        "stopy, zar^owno do spodni, jak i kr^otszych sp^odnic. \n");
    set_slots(A_FEET);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VALUE, 58);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
    ustaw_material(MATERIALY_SK_SWINIA, 100);
    set_type(O_UBRANIA);
}
