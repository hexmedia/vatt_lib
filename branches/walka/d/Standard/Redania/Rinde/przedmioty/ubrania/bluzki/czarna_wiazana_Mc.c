/* Autor: Valor
   Opis: Arivia
   Dnia: 30.03.07 */
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
    ustaw_nazwe("bluzka");
    dodaj_przym("czarny","czarni");
    dodaj_przym("wi^azany","wi^azani");
    set_long("Niezwykle delikatny materia^l, z jakiego zosta^la wykonana "+
        "bluzka, przywodzi na my^sl cienk^a paj^eczyn^e zabarwion^a na "+
        "czarny kolor. Ta cz^e^s^c odzienia na pewno nie uchroni ci^e "+
        "przed zimnem, gdy^z pozostawia prawie zupe^lnie odkryte "+
        "ramiona i plecy ^l^acz^ac materia^l cienkimi, powi^azanymi ze "+
        "sob^a paseczkami. Pomi^edzy niewielkim, tr^ojk^atnym dekoltem "+
        "zauwa^zasz male^nki haft przedstawiaj^acy motyla z roz^lo^zonymi "+
        "skrzyde^lkami. \n");
    set_slots(A_TORSO);
    add_prop(OBJ_I_WEIGHT, 300); 
    add_prop(OBJ_I_VOLUME, 400);
    add_prop(OBJ_I_VALUE, 71);
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
    ustaw_material(MATERIALY_JEDWAB, 100);
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    set_type(O_UBRANIA);
}
