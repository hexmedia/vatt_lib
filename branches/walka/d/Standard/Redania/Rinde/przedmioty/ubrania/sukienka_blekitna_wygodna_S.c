/* B��kitna wygodna sukienka
 * Lil, Opis:dinrahia(aneta)
 *
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("sukienka");
    dodaj_przym("b��tkitny","b��kitni");
    dodaj_przym("wygodny", "wygodni");
    set_long("W przeciwie�stwie do innych, ta sukieneczka jest "+
             "wyra�nie przystosowana dla os�b, kt�re lubi� ruch i "+
             "aktywnie sp�dzaj� czas. Ma delikatny odcie� lazurowego nieba, "+
             "kt�ry uwidacznia si� najbardziej w chwilach gdy pojawiaj� si� "+
             "drobne i liczne zagniecenia. Z pewno�ci� nie kr�powa�aby ruch�w "+
             "zar�wno na polowaniu jak i wykwintnym przyj�ciu.\n");
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_ATLAS, 80);
    ustaw_material(MATERIALY_TIUL, 20);
    set_slots(A_BODY, A_LEGS);
    add_prop(ARMOUR_F_PRZELICZNIK, 40.0); //rozciagliwosc
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40); //rozciagliwosc w dol na 40%
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 30); //rozciagliwosc w gore na 30%
    add_prop(OBJ_I_WEIGHT, 630);
    add_prop(OBJ_I_VOLUME, 990);
    add_prop(OBJ_I_VALUE, 30);
    set_size("S");
    add_prop(ARMOUR_I_DLA_PLCI, 1);
}