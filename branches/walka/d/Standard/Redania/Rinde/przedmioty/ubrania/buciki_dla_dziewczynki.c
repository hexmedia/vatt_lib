/*zrobione przez valora dnia 11 lipca
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

string

create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("pantofelki");
    dodaj_przym("^sliczny","^sliczni");
    dodaj_przym("czarny","czarni");


   set_long("^Sliczne pantofelki o niewielkich rozmiarach, przeznaczone s^a "+
	   "dla w^askiej dziewcz^ecej n^o^zki. Wykonano je z czarnej, b^lyszcz^acej "+
	   "sk^orki. Buciki zapinane s^a na srebrn^a, kwadratow^a klamerk^e "+
	   "umieszczon^a na grzbiecie bucika. Owalny kszta^lt dodaje stopie gracji, "+
	   "natomiast p^laska podeszwa u^latwia poruszanie si^e po ulicach miasta. \n");


    set_slots(A_FEET | A_SHINS);
    add_prop(OBJ_I_WEIGHT, 150);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 155);
    

    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    set_size("XS");
   
    ustaw_material(MATERIALY_SK_CIELE, 90);
    ustaw_material(MATERIALY_SREBRO, 10);
   
    set_type(O_UBRANIA);
}