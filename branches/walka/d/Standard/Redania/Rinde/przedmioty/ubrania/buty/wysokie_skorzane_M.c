/*
 * Butki dla strazy w Rinde
 * Rantaur 10.11.06
 */

inherit "/std/armour";

#include <macros.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <pl.h>

void
create_armour() 
{
    ustaw_nazwe("buty");
    ustaw_nazwe_glowna("para");

    dodaj_przym("wysoki", "wysocy");
    dodaj_przym("sk�rzany", "sk�rzani");

     set_long("Ciemne sk�rzane buty wy�o�one s� od �rodka cienkim, szarawym"+
     " futrem, dzi�ki czemu obuwie jest dobre niemal na ka�d� por� roku."+
     " Wysokie cholewki poznaczone s� kilkoma otarciami i zadrapaniami,"+
     " jednak nie wp�ywa to znacz�co na jako�� but�w.\n");

    add_prop(OBJ_I_WEIGHT, 670);
    add_prop(OBJ_I_VOLUME, 2800);
    add_prop(OBJ_I_VALUE, 37);
    set_ac(A_FEET, 5, 5, 5);   
    set_size("M");
}