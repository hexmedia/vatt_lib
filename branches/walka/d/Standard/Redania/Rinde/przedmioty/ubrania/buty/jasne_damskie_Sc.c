
/* Autor: Avard
   Opis : Katiara
   Data : 29.12.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");

    dodaj_przym("jasny","ja^sni");
    dodaj_przym("damski","damscy");

    set_long("Si^egaj^ace powy^zej kostki, obite mi^ekk^a, jasnobr^azow^a "+
        "sk^or^a, damskie buty, sznurowane s^a ciemnymi rzemieniami. "+
        "Cieniutka podeszwa zapewne nienajlepiej chroni stop^e przed "+
        "urazami na wyboistych traktach, ale dodaje obuwiu niebywa^lej "+
        "lekko^sci i gracji. G^ora butk^ow wyko^nczona jest delikatnym "+
        "ro^slinnym motywem, wyszytym ciemn^a, grub^a nici^a.\n");
        
    set_slots(A_FEET);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VALUE, 85);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    set_size("S");
    ustaw_material(MATERIALY_SK_CIELE, 100);
    set_type(O_UBRANIA);
}