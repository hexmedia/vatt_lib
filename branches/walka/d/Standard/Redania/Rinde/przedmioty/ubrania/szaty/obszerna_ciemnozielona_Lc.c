/* obszerna ciemnozielona szata zielarza z Rinde
opis popelniony przez Tabakiste, na forum vattgherna 18.02.2007
zakodowany przez Seda 1.03.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{  
   ustaw_nazwe("szata");
    dodaj_przym("obszerny","obszerni");
    dodaj_przym("ciemnozielony", "ciemnozieloni");

    set_long("D^luga szata z ciemnozielonego materia^lu doskonale ukrywa zarys "
	+"cia^la nosz^acej j^a osoby. Przesi^akni^eta jest zio^lowym zapachem, "
	+"szczeg^olnie silnie dochodz^acym z mn^ostwa mniejszych i wi^ekszych "
	+"kieszonek. Szerokie r^ekawy zapraszaj^a by skry^c w nich d^lonie, "
	+"za^s drobne guziki nadaj^a ca^lo^sci wyraz skromnej elegancji. Szata si^ega "
	+"nieco poni^zej po^lowy ^lydek pozostawiaj^ac buty dobrze widoczne. "
	+"Brak kaptura, czy ciep^lej podpinki sk^lania do za^lo^zenia na^n szuby "
	+"czy p^laszcza. \n");

    set_slots(A_ROBE);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 2100);
    add_prop(OBJ_I_VALUE, 2000);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    set_size("L");
    add_prop(ARMOUR_F_PRZELICZNIK, 20.0);
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    ustaw_material(MATERIALY_WELNA);
}