//lil, opis dinrahia(aneta)
inherit "/std/armour.c";
#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("p�aszcz");
    dodaj_przym("obszerny", "obszerni");
    dodaj_przym("krwistoczerwony", "krwistoczerwoni");
    set_long("Materia�, z kt�rego zosta� wykonany ten p�aszcz, "+
            "pokryty zosta� krwistoczerwonym barwnikiem, "+
            "nadaj�c mu po�ysku. P�aszcz nie posiada �adnych wzor�w, pr�cz "+
            "iskrz�cych i idealnie wkomponowanych z�otych guzik�w w "+
            "kszta�cie g�owy wilka z d�ugimi k�ami. Wewn�trzna strona "+
            "jest koloru ciemnego i powoduje, �e osoba nosz�ca takie "+
            "odzienie wygl�da nieco tajemniczo.\n");
    ustaw_material(MATERIALY_SK_BYDLE, 80);
    ustaw_material(MATERIALY_LEN, 20);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 100); // zeby sie nie srac...
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 100);
    set_slots(A_CLOAK);
    add_prop(OBJ_I_VOLUME,1300);
    add_prop(OBJ_I_VALUE,19);
    add_prop(OBJ_I_WEIGHT,1500);
}