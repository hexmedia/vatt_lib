/*zrobione przez valora dnia 11 lipca
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

string

create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("trzewiki");
    dodaj_przym("brudny","brudni");
    dodaj_przym("sk^orzany","sk^orzani");


   set_long("Wykonane z br^azowej sk^orki ma^le trzewiki zawi^azywane s^a "+
	   "cienk^a sznur^owk^a. Dok^ladnie oblepione suchym b^lotem i kurzem, "+
	   "zdaj^a si^e krzycze^c o odrobin^e wody po to, by zmy^c brud z ich "+
	   "powierzchni. Trudno stwierdzi^c czy s^a one nowe, a py^l kt^ory "+
	   "zakrywa prawie ca^le obuwie, skutecznie maskuje niedoskona^lo^sci "+
	   "trzewik^ow. \n");


    set_slots(A_FEET | A_SHINS);
    add_prop(OBJ_I_WEIGHT, 200);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 100);
    

    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("XS");
   
    ustaw_material(MATERIALY_SK_CIELE, 100);
   
    set_type(O_UBRANIA);
}