
/* gustowny z^loty pier^scie^n z brylantem zielarza z Rinde
opis pope^lniony przez Tabakist^e, na forum vattgherna 18.02.2007
zakodowany przez Seda 26.02.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("pierscien z brylantem");
	dodaj_nazwy("pier^scie^n");
          
    dodaj_przym("gustowny","gustowni");
    dodaj_przym("z^loty","z^loci");

    set_long("Niecodzienny wygl^ad pier^scie^n ten zawdzi^ecza "
	+"sposobowi w jaki zosta^l wprawiony kamie^n. Wydaje si^e on wysuwa^c "
	+"z pomi^edzy zwoj^ow g^estego, niezwykle skomplikowanego w^ez^la, "
	+"wykonanego z ciemnej odmiany z^lota. Sam kamie^n wprawdzie nie jest "
	+"wielki, lecz za to zosta^l wprawnie oszlifowany tak, ^ze nie posiada "
	+"^zadnych widocznych skaz.\n");

    set_slots(A_ANY_FINGER);
    set_type(O_BIZUTERIA);
    add_prop(OBJ_I_WEIGHT, 25);
    add_prop(OBJ_I_VOLUME, 25);
    add_prop(OBJ_I_VALUE, 2850);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
    ustaw_material(MATERIALY_ZLOTO, 60);
	ustaw_material(MATERIALY_DIAMENT, 40);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
}

