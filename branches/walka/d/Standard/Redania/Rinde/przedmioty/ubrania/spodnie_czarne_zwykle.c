inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");

    dodaj_przym("czarny", "czarni");
    dodaj_przym("zwyk�y", "zwykli");

    set_long("Najzwyklejsze czarne spodnie, wykonane zosta�y z jakiego� " +
	"nierozci�gliwego materia�u, pozwalajac na lu�ne u�o�enie. U g�ry " +
	"znajduje si� kilka szlufek, w razie jakby osoba je nosz�ca " +
	"potrzebowa�a do nich paska, z kolei nogawki posiadaj� lekkie " +
	"rozci�cie, �eby dobrze uk�ada� si� na wszelkiego rodzaju butach.\n");

    set_slots(A_LEGS);
//    set_ac();
}