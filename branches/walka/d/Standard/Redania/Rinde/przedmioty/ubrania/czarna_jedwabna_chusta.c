/* TODO: Zrobic to bardziej ubraniowo ;) */
inherit "/std/object";

#include <cmdparse.h>
#include <macros.h>
#include <materialy.h>
#include <pl.h>
#include <object_types.h>
#include <stdproperties.h>
#include <wa_types.h>

int zawiazana;		// Czy chusta jest zawiazana

string *gdzie_mozna = ({"na lewym ramieniu", "na prawym ramieniu", "na ramieniu",
                         "na g�owie", "na lewym nadgarstku", "na prawym nadgarstku",
                         "na nadgarstku", "pod szyj�"});

int gdzie_index=0;	// Gdzie jest zawiazana

void create_object()
{
	ustaw_nazwe("chusta");
	
	dodaj_przym("czarny", "czarni");
	dodaj_przym("jedwabny", "jedwabny");
	

	set_long("Czarny niczym niebo w bezgwiezdn� noc materia�"
            +" po�yskuje delikatnie przy ka�dym, najmniejszym nawet"
            +" ruchu. Chusta utkana jest z wysokiej jako�ci jedwabiu i"
            +" z pewno�ci� stanowi towar z wy�szej p�ki. Krawi�dzie"
            +" materia�u obszyte s� srebrzyst� nici�, kt�ra jak gdyby"
            +" zamyka czer� w sztywnej ramie nie pozwalaj�c jej rozp�yn��"
            +" si� po otoczeniu.\n");
	
	ustaw_material(MATERIALY_JEDWAB, 100);
	set_type(O_UBRANIA);
	
	add_prop(OBJ_I_WEIGHT, 6);
	add_prop(OBJ_I_VOLUME, 8);
	add_prop(OBJ_I_VALUE,  22);
}

init()
{
    ::init();
    add_action("zawiaz", "zawi��");
    add_action("odwiaz", "odwi��");
    add_action("pomoc", "?", 2);
    
}

string show_subloc(string subloc, object on_obj, object for_obj)
{
    if (wildmatch("rinde_chusta_subloc*", subloc))
    {
	    if(zawiazana)
	    {
	        if (for_obj == on_obj)
		        return capitalize(gdzie_mozna[gdzie_index])+" masz zawi�zan� "
                         +short(PL_BIE)+".\n";
 
	        return capitalize(gdzie_mozna[gdzie_index])+ " ma zawi�zan� "
                    +short(PL_BIE)+".\n";
	    }
    }
}

int zawiaz(string str)
{
    if(!str)
    {
	notify_fail("Co i gdzie chcesz zawi�za�?\n");
	return 0;
    }
	
    object *co;
    string gdzie;
    if(!parse_command(lower_case(str), TP, " %i:"+PL_BIE+" %s ", co, gdzie))
    {
		notify_fail("Co i gdzie chcesz zawi�za�?\n");
	    return 0;
    }
	
    co = CMDPARSE_STD->normal_access(co, 0, 0);
	
    if(!co || (sizeof(co) == 0))
    {
	notify_fail("Co i gdzie chcesz zawi�za�?\n");
	return 0;
    }
	
    if(sizeof(co) > 1)
    {
	notify_fail("Mo�esz za�o�y� tylko jedn� rzecz na raz!\n");
	return 0;
    }
	
    if(co[0]!=this_object())
    {
	return 0;
    }
    
    if(zawiazana)
    {
	notify_fail("Przecie� chusta jest ju� zawi�zana!\n");
	return 0;
    }
	
	
    if((gdzie_index = member_array(gdzie, gdzie_mozna)) == -1)
    {
	notify_fail("Gdzie chcesz zawi�za� "+co[0]->short(PL_BIE)+"?\n");
	return 0;
    }
	
    /* Jesli na ramie - zmieniamy na prawe */
    if(gdzie_index == 2)
		gdzie_index = 1;

	/* Jesli na nadgarstek to na prawy */
	if(gdzie_index == 6)
		gdzie_index = 5;

	if(TP->query_subloc_obj("rinde_chusta_subloc_"+gdzie_mozna[gdzie_index]) != 0)
	{
		notify_fail("W tym miejscu masz ju� zawi�zan� jedn� chust�!\n");
		return 0;
	}
	
    /* Zawiazujemy na ramieniu */
    if(gdzie_index < 2)
    {
	TP->catch_msg("Sk�adasz chust� na p�, po czym starannie zawi�zujesz j�"
				+" sobie "+gdzie_mozna[gdzie_index]+".\n");
		
	saybb(QCIMIE(TP, PL_MIA)+" sk�ada "+short(TP, PL_BIE)+" na p�, po czym"
		+" starannie zawi�zuje j� sobie "+gdzie_mozna[gdzie_index]+".\n");
    }
	
    /* Zawiazujemy na glowie */
    if(gdzie_index == 3)
    {
	TP->catch_msg("Zwijasz "+short(TP, PL_BIE)+" w niezbyt szeroki pas,"
				+" po czym wi��esz j� sobie na g�owie i szybkim ruchem"
				+" wyg�adzasz wszystkie nier�wno�ci.\n");
	
	saybb(QCIMIE(TP, PL_MIA)+" zwija "+short(TP, PL_BIE)+" w niezbyt szeroki"
		+" pas, po czym zawi�zuje j� sobie na g�owie i szybkim ruchem wyg�adza"
		+" wszystkie jej nier�wno�ci.\n");
    }
	
    /* Zawiazujemy na nadgarstku */
	if(gdzie_index == 4 || gdzie_index == 5)
	{
		TP->catch_msg("Trzykrotnie sk�adasz chust� otrzymuj�c zgrabny pasek,"
					+" kt�ry wi��esz �ci�le "+gdzie_mozna[gdzie_index]+".\n");

		saybb(QCIMIE(TP, PL_MIA)+" trzykrotnie sk�ada "+short(TP, PL_BIE)
			+" otrzymuj�c zgrabny pasek, kt�ry wi��e sobie "
			+gdzie_mozna[gdzie_index]+".\n");
	}
	
	/* Zawiazujemy na szyji */
	if(gdzie_index == 7)
	{
		TP->catch_msg("Uk�adasz sobie chust� pod szyj� i wi��esz z ty�u mocny"
					+" w�ze�.\n");
		
		saybb(QCIMIE(TP, PL_MIA)+" uk�ada sobie chust� pod szyj� i wi��e z ty�u"
					+" mocny w�ze�.\n");
	}
	
    TP->add_subloc("rinde_chusta_subloc_"+gdzie_mozna[gdzie_index], this_object());
    zawiazana = 1;
    set_no_show_composite(1);
    add_prop(OBJ_M_NO_GIVE, "Najpierw j� odwi��!\n");
	add_prop(OBJ_M_NO_DROP, "Najpierw j� odwi��!\n");
	
    return 1;
}

int odwiaz(string str)
{
    notify_fail("Co chcesz odwi�za�?\n");
    
    if(!str)
	return 0;
	
    object *co;
    if(!parse_command(lower_case(str), environment(), " %i:"+PL_BIE, co))
	return 0;
	
    co = NORMAL_ACCESS(co, 0, 0);
	
    if(!co)
	return 0;
	
    if(sizeof(co) > 1)
	return 0;

	
    if(co[0]!=this_object())
	return 0;
    
    if(!zawiazana)
    {
	notify_fail("Ale� chusta nie jest nawet zawi�zana!\n");
	return 0;
    }
    
    TP->catch_msg("Kilkoma sprawnymi ruchami odwi�zujesz za�o�on� "+gdzie_mozna[gdzie_index]
		+" chust�.\n");
    saybb(QCIMIE(TP, PL_MIA)+" kilkoma sprawnymi ruchami odwi�zuje za�o�on� "
	+gdzie_mozna[gdzie_index]+" "+short(PL_BIE)+".\n");
    TP->remove_subloc("rinde_chusta_subloc_"+gdzie_mozna[gdzie_index]);
    zawiazana = 0;
    set_no_show_composite(0);
    remove_prop(OBJ_M_NO_GIVE);
	remove_prop(OBJ_M_NO_DROP);
    
    return 1;
}


int pomoc(string str)
{                                                                              
    object pohf;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) 
	return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, pohf))
        return 0;

    if (pohf != this_object())
        return 0;

    write("*****************************************************\n");
    write("*                                                   *\n");
    write("*  Chust� mo�esz zawi�za�:                          *\n");
    write("*                                                   *\n");
    write("*    - na kt�rym� z ramion                          *\n");
    write("*    - na g�owie                                    *\n");
    write("*    - pod szyj�                                    *\n");
    write("*    - na kt�rym� z nadgarstk�w                     *\n");
    write("*                                                   *\n");
    write("*****************************************************\n");
	
    return 1;
}

