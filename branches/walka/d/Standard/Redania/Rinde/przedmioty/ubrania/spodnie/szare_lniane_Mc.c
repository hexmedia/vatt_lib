/* Autor: Valor
   Opis: Arivia
   Dnia: 30.03.07 */
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");
    dodaj_przym("szary","szarzy");
    dodaj_przym("lniany","lniani");
    set_long("Spodnie zosta^ly w ca^lo^sci wykonane z lnu, czyni^ac t^e "+
        "cz^e^s^c ubrania niezwykle wygodn^a i przewiewn^a. Praktyczny "+
        "kr^oj sprawia, ^ze nie kr^epuj^a one ruch^ow w^la^sciciela i "+
        "nadaj^a si^e zar^owno do pracy, jak i na co dzie^n. Szary, "+
        "pozbawiony jakichkolwiek ozd^ob, materia^l sugeruje, ^ze spodnie "+
        "te nie nadaj^a si^e na od^swi^etne ubranie. \n");
    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VALUE, 30);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
    ustaw_material(MATERIALY_LEN, 100);
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_type(O_UBRANIA);
}
