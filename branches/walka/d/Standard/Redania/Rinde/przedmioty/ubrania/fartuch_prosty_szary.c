/* Artonul
 write("Oczy^s^c co? Mo^ze fartuch?");
   return 1;
     }
       
       saybb(QCIMIE(this_player(), PL_MIA) + " spogl^ada na prosty szary fartuch krytycznie i"
       +   " szybko wyciera nagromadzony na nim brud.\n");
         write(" Spogladasz na prosty szary fartuch i zauwa^zywszy na nim mn^ostwo brudu szybko przecierasz"
	 +   " go r^ek^a.\n");
	 write("Wyg^lad^x co? Mo^ze fartuch?");
	   return 1;
	     }
	       saybb(QCIMIE(this_player(), PL_MIA) + " szybkim ruchem r^eki wyg^ladza fa^ldy na prostym szarym fartuchu.\n");
	         write(" Szybkim ruchem r^eki wyg^ladzasz fa^ldy na prostym szarym fartuchu.\n");
		 
*/
inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>

void
create_armour()
{
    ustaw_nazwe("fartuch");

    dodaj_przym("prosty", "pro�ci");
    dodaj_przym("szary", "szarzy");

    set_long("Fartuch kt�ry widzisz przed sob� jest ca�kiem nowiutki, " +
	"co mo�e �wiadczy� albo o zamo�no�ci jego w�a�ciciela, albo o " +
	"tym, �e nale�y on do �wie�o upieczonego kucharza. Wykonano go " +
	"ze �redniej jako�ci jedwabiu przeplatanego warstwami lnu. Do " +
	"wi�zania go s�u�� umieszczone na spodzie czarne rzemienie. " +
	"Mo�esz go 'wyg�adzi�' lub 'oczy�ci�'.\n");

    set_slots(A_STOMACH);
//    set_ac();
}