/*spodnie dla brudnych,biednych npcow;)
                  Lil  01.05                                   */

inherit "/std/armour";

#include <pl.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <materialy.h>
#include <macros.h>
#include <object_types.h>

string *losowy_przym()
 {
   string *lp;
   string *lm;
   int i;
   lp=({ "brudny", "poplamiony", "przybrudzony", "przetarty", "wytarty", "wymi�ty"});
   lm=({"brudni", "poplamieni", "przybrudzeni", "przetarci",  "wytarci", "wymi�ty"});
   i=random(6);
   return ({ lp[i] , lm[i] });
 }
 string *losowy_przym2()
 {
   string *lp;
   string *lm;
   int i;
   lp=({ "szary", "lu�ne",  "br�zowy", "ciemny", "ciemnoszary"});
   lm=({"szarzy", "lu�ni", "br�zowi", "ciemny", "ciemnoszarzy"});
   i=random(5);
   return ({ lp[i] , lm[i] });
 }
void
create_armour()
{
   string *przm = losowy_przym();
   string *przm2 = losowy_przym2();
   ustaw_nazwe("spodnie");
   ustaw_nazwe_glowna("para");
   dodaj_przym( przm[0], przm[1] );
   dodaj_przym( przm2[0], przm2[1] );

   set_long("Ich prosty kr^oj, jak i materia^l, z kt^orego zosta^ly uszyte "
          + "^swiadcza o tym, i^z nie s^a wyrobem cho^cby ^sredniej jako^sci. "
          + "Dodatkowo, para tych spodni posiada kilka czarnych plam, kt^ore "
          + "s^a na trwa^le ju^z zapisane do historii tej cz^e^sci odzie^zy.\n");

   set_slots(A_TORSO | A_STOMACH | A_THIGHS);
   add_prop(ARMOUR_F_PRZELICZNIK, 40.0);
   add_prop(OBJ_I_VALUE, 5);
   add_prop(OBJ_I_WEIGHT, 1440);
   add_prop(OBJ_I_VOLUME, 1500);
   set_type(O_UBRANIA);
   ustaw_material(MATERIALY_LEN);

}