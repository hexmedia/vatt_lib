inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>

void
create_armour()
{
    ustaw_nazwe("koszula");

    dodaj_przym("br�zowy", "br�zowi");
    dodaj_przym("lniany", "lniani");

    set_long("Koszula wykonana zosta�a ze �redniej jako�ci lnu i " +
	"zabarwiona na kolor br�zowy. Zgrabny ko�nierzyk sprawia, �e " +
	"wydaje si� nieco bardziej elegancka ni� jest w rzeczywistosci. " +
	"Jej poka�ne rozmiary dostosowane s� dla grubszych os�b, zapewne " +
	"z du�ym brzuchem, sprawiaj�c, �e nie opina ona cia�a, lu�no " +
	"zwisaj�c. Dodatkowo przyszyto dwa rz�dy czarnych guzik�w by osoba " +
	"nosz�ca bez problemu mog�a dopasowa� koszul� do swoich rozmiar�w.\n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    ustaw_material(MATERIALY_LEN);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "p�elf");
    add_prop(OBJ_I_WEIGHT, 412);
    add_prop(OBJ_I_VOLUME, 950);
    add_prop(OBJ_I_VALUE, 15);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 50);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_F_PRZELICZNIK, 60.0); //koszula jest bardzo rozciagliwa.

}