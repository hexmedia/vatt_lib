
/* wytarte czarne spodnie made by Ulryk 26.05.06 */
inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");
    dodaj_przym("wytarty","wytarci");
    dodaj_przym("czarny","czarni");
    set_long("Trudno powied^zie^c kiedy mine^ly lata ^swietno^sci tych "+
        "spodni. Ale z pewno^sci^a by^lo to dawno temu. ^Swiadcz^a o tym "+
        "chocia^zby wytarte kolana, niewielkie dziurki w kilku miejscach "+
        "czy wystaj^ace nitki. W spodniach tych przewa^za kolor czarny, bo "+
        "taki mia^l by^c najpewniej w pierwowzorze. Zakurzone i poplamione "+
        "na pewno nie robi^a dobrego wra^zenia.\n");

    set_slots(A_LEGS, A_HIPS);   
    add_prop(OBJ_I_WEIGHT, 950);
    add_prop(OBJ_I_VOLUME, 1100);
    add_prop(OBJ_I_VALUE, 15);
    ustaw_material(MATERIALY_LEN);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}
