inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");

    dodaj_przym("ciemny", "ciemni");
    dodaj_przym("zwyk�y", "zwykli");

    set_long("Spodnie wykonane zosta�y z ciemnego " +
	"materia�u, nieco szorstkiego w dotyku. Ma typowo m�ski kr�j, " +
	"jednocze�nie nie posiada �adnych udziwnie� ani zdobie�, " +
	"sprawiaj�c �e spodnie te s� najzwyklejsze. Posiadaj� d�ugie " +
	"nogawki, a u g�ry umieszczono kilka szlufek na ewentualny pasek. " +
	"Dostrzegasz na nich liczne przetarcia, �wiadcz�ce o tym, �e " +
	"cz�sto by�y noszone.\n");
    set_slots(A_LEGS, A_HIPS);
    ustaw_material(MATERIALY_LEN);
    set_type(O_UBRANIA);
    add_prop(OBJ_I_VOLUME, 1200);
    add_prop(OBJ_I_WEIGHT, 669);
    add_prop(OBJ_I_VALUE, 13);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}
