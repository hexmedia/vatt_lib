
/* 
opis popelniony przez Faeve, zakodowany przez Seda 12.03.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
   ustaw_nazwe("sukienka");
    dodaj_przym("prosty","pro^sci");
    dodaj_przym("czerwony", "czerwoni");

    set_long("Sukienka ta uszyta jest z do^s^c dobrego gatunkowo, czerwonego "
	+"sukna. Skrojona prosto, pozbawiona zb^ednych dodatk^ow, a mimo to do^s^c "
	+"elegencka - kwadratowy dekolcik nie jest zbyt g^l^eboki - ods^lania ledwie "
	+"obojczyki, co kontrastuje nieco z brakiem r^ekaw^ow. Skromnie si^egaj^aca "
	+"poni^zej kolan sp^odniczka zosta^la zaopatrzona w klilka strategicznie "
	+"roz^lo^zonych zak^ladek maskuj^acych ewentualne mankamenty kobiecego cia^la.\n");

    set_slots(A_THIGHS | A_CHEST | A_SHINS | A_STOMACH | A_HIPS);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 2100);
    add_prop(OBJ_I_VALUE, 2000);

	set_size("L");
    add_prop(ARMOUR_F_PRZELICZNIK, 20.0);
    add_prop(ARMOUR_I_DLA_PLCI, 1);
	add_prop(ARMOUR_F_POW_PIERSI, 1.25);
	add_prop(ARMOUR_F_POW_BIODRA, 1.25);
    ustaw_material(MATERIALY_WELNA);
}
