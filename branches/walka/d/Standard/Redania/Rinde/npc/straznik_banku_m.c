
/* Straznik - mezczyzna z banku w Rinde  *
 * Made by Avard 04.06.07                *
 * Opis by Tinardan & Avard              */

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"
 inherit RINDE+"std/npc.c";
 void
create_rinde_npc()
 {
     ustaw_odmiane_rasy(PL_MEZCZYZNA);
     set_gender(G_MALE);
     dodaj_nazwy("straznik");
     dodaj_przym("wysoki","wysocy");
     dodaj_przym("czarnobrody","czarnobrodzi");

     set_long("Od tego m^e^zczyzny jedzie alkoholem jak od beczki z w^odk�, "+
         "ale, o dziwo, trzyma si^e prosto i sprawia wra^zenie w miar^e "+
         "trze^xwego. Uzbrojony jest w d^lugi, poszczerbiony miecz, a cia^lo "+
         "skrywa mu ci^e^zka zbroja, kt^ora najwyra^xniej niejedno ju^z "+
         "przesz^la. Czarna broda jest sko^ltuniona i spl^atana, a ponad "+
         "ni^a wida� uniesione wargi, spomi^edzy kt^orych wy^laniaj^a si^e "+
         "pogni^le resztki z^eb^ow.\n");

     set_stats (({ 80, 60, 90, 35, 35, 80 }));
     add_prop(CONT_I_WEIGHT, 100000);
     add_prop(CONT_I_HEIGHT, 185);
     add_prop(LIVE_I_NEVERKNOWN, 1);

    set_act_time(30);
	add_act("emote u^smiecha si^e ukazuj^ac resztki z^eb^ow.");
	add_act("emote czka pijacko.");
	add_act("rozejrzyj sie leniwie");
    add_act("emote spluwa na ziemi^e.");
    add_act("emote wybucha nag^lym ^smiechem.");
	
	set_cchat_time(8);
	add_cchat("Ha! Umierasz!");
	add_cchat("Nie prze^zyjesz naszego spotkania!");
	add_cchat("Gi^n!");
    add_cchat("Wybebeszymy cie!");

    //add_armour(cos);
    //add_armour(cos);
    //clone_object(sdf)->move(npc);

    add_ask(({"bank"}), VBFC_ME("pyt_o_bank"));
	add_ask(({"ruygena","bankiera"}), VBFC_ME("pyt_o_bankiera"));
	add_ask(({"stra^znik^ow","stra^z"}), VBFC_ME("pyt_o_straz"));
	set_default_answer(VBFC_ME("default_answer"));

 }
 
	void
	powiedz_gdy_jest(object player, string tekst)
	{
		if (environment(this_object()) == environment(player))
		this_object()->command(tekst);
	}

	string
	pyt_o_bank()
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
		"emote m^owi: Jeste^s w nim, "+
            "idio"+this_player()->koncowka("o","tko")+".");
		return "";
	}
	string
	pyt_o_bankiera()
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
			"emote m^owi: Jest obok, wyno^s si^e.");
		return "";
	}
	string
	pyt_o_straz()
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
			"emote m^owi u^smiechaj^ac si^e paskudnie: Ta, to my, m"+
            "amy si^e tob^a zaj^a^c?");
		return "";
	}
	string
	default_answer()
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_BIE) +
        "G^owno ci do tego, wyno^s si^e.");
		return "";
	}


    void
    add_introduced(string imie_mia, string imie_bie)
    {
        set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
    }
    void
    return_introduce(object ob)
    {
    command("powiedz A na co mi twoje imie?");
    }


    void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
			        break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
		case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(2))
                 {
                 case 0:command("pocaluj "+OB_NAME(wykonujacy)); break;		
                 case 1:command("pocaluj "+OB_NAME(wykonujacy)); break;	
                        command("powiedz Przyjd^z do mnie wieczorem."); break;
                }
              else
                {
                   switch(random(2))
                   {
                   case 0:command("emote czerwienieje ze z^lo^sci.");
                          command("zabij "+OB_NAME(wykonujacy)); break;
                   case 1:command("spojrzyj z obrzydzeniem na "+
                        OB_NAME(wykonujacy));
                          command("zabij "+OB_NAME(wykonujacy)); break;
                   }
                }
			  
                break;
                }
        case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{

   switch(random(2))
   {
     case 0: command("spojrzyj krzywo na "+OB_NAME(wykonujacy)); break;
     case 1: command("powiedz Wyno^s si^e zanim wyrwe ci nogi."); break;
 
   }
}

void
nienajlepszy(object wykonujacy)
{
   switch(random(1))
   {
      case 0: command("zabij "+OB_NAME(wykonujacy)); break;
   }
} 

