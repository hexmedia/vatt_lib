/* Autor: Sedzik	
   Opis : Yran + poprawia� Molder
   Data : 21.01.07
   Info : Zielarz, zielarzuje w zielarni */

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include "dir.h"
#include <object_types.h>
inherit "/lib/sklepikarz";
inherit RINDE+"std/npc.c";

void
create_rinde_npc()
{
    dodaj_nazwy("zielarz");
	dodaj_nazwy("aptekarz");
    ustaw_odmiane_rasy("mezczyzna");
    set_gender(G_MALE);
    ustaw_imie(({"wawrzynosek","wawrzynoska","wawrzynoskowi","wawrzynoska",
		"wawrzynoskiem","wawrzynosku"}), PL_MESKI_OS);
    dodaj_przym("krzywonosy","krzywonosi");
	dodaj_przym("wysoki","wysocy");

    set_long("Wysoki, niem�ody m�czyzna bacznie, cho� z pewn� nerwowo�ci� �widruje "
    +"ci� spojrzeniem swych ciemnych oczu. Ma lekko przekrzywiony nos, co mo�e by� pami�tk� "
    +"po jakiej� m�odzie�czej b�jce, kr�tkie lecz g�ste, jasne w�osy oraz du�e i zdecydowanie "
	+"odstaj�ce uszy. Jego pomarszczona twarz i kozia br�dka jednoznacznie wskazuj� na "
    +"s�dziwy wiek. Pomimo wysokiego wzrostu jest bardzo chudy, a obszerna "
    +"ciemnozielona szata przewi�zana w pasie w�skim sznurkiem wisi na nim jak, nie "
    +"przymierzaj�c, worek po ziemniakach. Kr�j szaty jest jednak elegancki, a noszony " 
    +"przez m�czyzn� z�oty naszyjnik oraz pier�cie� z b�yszcz�cym brylantem wskazuj�, "
	+"�e aptekarz jest osob� bogat� i wp�ywow�.\n");

    set_stats (({ 70, 60, 60, 40, 25, 100 }));
    add_prop(CONT_I_WEIGHT, 70000);
    add_prop(CONT_I_HEIGHT, 185);

    set_act_time(60);
    add_act("@@eventy@@");
    
    set_cact_time(10);
    add_cact("krzyknij Zostaw mnie! Stra^z! Do jasnej Cholery! Stra^z!");
    add_cact("krzyknij Psia ma^c! Jeszcze jeden z^lodziej! Stra^z!");

    MONEY_MAKE_D(15)->move(this_object());
    MONEY_MAKE_G(30)->move(this_object());

    add_ask(({"ratusz"}), VBFC_ME("pyt_o_ratusz"));
    add_ask(({"^swi^atynie"}), VBFC_ME("pyt_o_swiatynie"));
    add_ask(({"prace"}), VBFC_ME("pyt_o_prace"));
    add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
    add_ask(({"zio^a"}), VBFC_ME("pyt_o_ziola"));
    add_ask(({"burmistrza","Nevilla","nevilla"}),VBFC_ME("pyt_o_burmistrza"));
    add_ask(({"karczm^e","baszt^e","garnizonow^a"}),VBFC_ME("pyt_o_karczme"));
    add_ask(({"wawrzynoska","Wawrzynoska","krzywonosego wysokiego m^e^zczyzne",
        "krzywonosego m^e^zczyzn^e","wysokiego m^e^zczyzn^e"}),
        VBFC_ME("pyt_o_wawrzynoska"));
    set_default_answer(VBFC_ME("default_answer"));
	
    add_armour(RINDE_UBRANIA + "pierscienie/gustowny_zloty_Lc.c");
    add_armour(RINDE_UBRANIA + "szaty/obszerna_ciemnozielona_Lc.c");
    add_armour(RINDE_UBRANIA + "buty/zadbane_zamszowe_Lc.c");
    add_armour(RINDE_UBRANIA + "naszyjniki/elegancki_zdobiony_Lc.c");
	
    config_default_sklepikarz();
    set_money_greed_buy(104);
    set_money_greed_sell(110);
	set_money_greed_change(104);

    set_store_room(POLUDNIE_LOKACJE + "magazyn_apteki.c");
    set_co_skupujemy(O_ZIOLA);

}
   
 
void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_ratusz()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: Jest w centrum miasta, nie spos^ob nie trafi^c.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Mo^ze co^s poda^c?");
    }    
    return "";
}
string
pyt_o_swiatynie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: To du^za budowla z wie^z^a na zachodzie miasta.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Mo^ze co^s poda^c?");
    }    
    return "";
}
string
pyt_o_prace()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: O tak, bycie zielarzem to ci^e^zka praca, ale to r^ownie^z "
		+"moja pasja.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Mo^ze co^s poda^c?");
    }    
    return "";
}
string
pyt_o_garnizon()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: Garnizon jest we wschodniej cz^e^sci miasta.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "ppowiedz Mo^ze co^s poda^c?");
    }    
    return "";
}
string
pyt_o_burmistrza()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: Neville nie jest z^lym burmistrzem, mo^zesz "+
        "go spotka^c w ratuszu.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Mo^ze co^s poda^c?");
    }    
    return "";
}
string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Przykro mi, ale na ten temat niczego ci nie powiem.");
    return "";
}
string
pyt_o_karczme()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: Je^sli chodzi o karczmy to polecam Baszt^e, jest "+
        "na p^o^lnocy miasta, prosta droga od ratusza.");
    }
    else
    {

         set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }    
    return "";
}
string
pyt_o_wawrzynoska()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: To ja, o co chodzi?");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
        return "";
    }        
}
string
pyt_o_ziola()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: Wszystko co mam do sprzedania jest wystawione.");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
        return "";
    }        
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie "+ OB_NAME(ob));
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(1))
                 {
                 case 0: command("emote u^smiecha si^e mi^lo."); break;        
                }
              else
                {
                   switch(random(1))
                   {
                   case 0:command("emote m^owi: Uznam to za przyjacielski "+
                       "gest."); break;
                   }
                }
              
                break;
                }
        case "prychnij":
              {
                switch(random(1))
                 {
                 case 0:command("spojrzyj lekcewazaco "+
                     "na "+ OB_NAME(wykonujacy)); break;        
                 }
                break;
               }
        case "przytul": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
    switch(random(1))
    {
        case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
			"emote m^owi: Dzi^ekuje, to mi^le.");
    }
}

void
nienajlepszy(object wykonujacy)
{
  switch(random(2))
    {
    case 0: 
      command("emote m^owi: Wyno^s si^e kmiocie!");
      break;
    case 1: 
      command("emote m^owi: Czego? Zreszt^a, nie b^ed^e rozmawia^l "
		+"z takim chamem.");
      break;
    }
} 

void
init() 
{
    ::init();
    init_sklepikarz();
}

