
/* Dzieciak z Rinde        *
 * Made by Avard 17.06.06  *
 * Opis by Tinardan        */

#include "dir.h"
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <filter_funs.h>
inherit RINDE+"std/npc.c";
int czy_walka();
int walka = 0;
 void
create_rinde_npc()
 {
     set_living_name("dzieciak2");
     ustaw_odmiane_rasy("ch^lopiec");
     set_gender(G_FEMALE);
     dodaj_nazwy("dziecko");
     dodaj_przym("^lobuzerski","^lobuzerscy");
     dodaj_przym("jasnow^losy","jasnow^losi");

     set_long("Ubrany zosta^l schludnie i do^s^c bogato, ale ubranka ju^z "+
         "zd^a^zy^ly si^e pobrudzi^c w odwiecznej, dzieci^ecej bieganinie "+
         "po okolicznych uliczkach i podw^orkach. Za pas ma wetkni^ety "+
         "drewniany mieczyk, a na w^losach fantazyjn^a czapeczk^e z "+
         "pi^orkiem. Spogl^adaj^ac na przechodni^ow u^smiecha si^e "+
         "^lobuzersko i figlarnie. Co jaki^s czas przechadza si^e wzd^lu^z "+
         "uliczki ci^e^zkim krokiem, zapewne podpatrzonym gdzie^s u "+
         "^zo^lnierzy.\n");

     set_stats (({15,15,15,10,10,100})); //odwaga - mlodziencza glupota ;)
     add_prop(CONT_I_WEIGHT, 45000);
     add_prop(CONT_I_HEIGHT, 145);
     add_prop(CONT_I_VOLUME, 45000);
     add_prop(LIVE_I_NEVERKNOWN, 1);

    set_act_time(10);
	add_act("emote przechadza si^e wzd^lu^z ulicy dumny jak paw, "+
        "^sciskaj^ac r^ekoje^s^c drewnianego mieczyka.");
	add_act("emote wo^la: Ha, z^lodzieje i szubrawcy! Strze^zcie si^e!");
	add_act("@@event_dziewczynka@@");
	
	set_cact_time(10);
	add_cact("echo W jego oczach b�yska przera�enie.");
	add_cact("krzyknij M^oj tata jest ^zo^lnierzem, przyjdzie tutaj i "+
        "ci^e zabije!");

    add_armour("/d/Standard/Redania/Rinde/przedmioty/ubrania/czapeczka_dla_dzieciaka1.c");
    add_armour("/d/Standard/Redania/Rinde/przedmioty/ubrania/" +
        "buty_dla_dzieciaka4.c");
    add_armour("/d/Standard/Redania/Rinde/przedmioty/ubrania/" +
        "koszulka_dla_dzieciaka4.c");
    add_armour("/d/Standard/Redania/Rinde/przedmioty/ubrania/" +
        "spodenki_dla_dzieciaka4.c");

    add_weapon("/d/Standard/Redania/Rinde/przedmioty/bronie/miecze/mieczyk.c");

	set_default_answer(VBFC_ME("default_answer"));
}
 
void
powiedz_gdy_jest(object player, string tekst)
{
	if (environment(this_object()) == environment(player))
	this_object()->command(tekst);
}

string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
    "powiedz do "+ this_player()->query_name(PL_DOP) +
    " Ja nie wiem, ale m�j tata na pewno wie.");

	return "";
}


void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}

void
return_introduce(object ob)
{
    command("powiedz Zapami^etam, mo^ze b^edziemy razem s^lu^zy^c w wojsku. ");
    command("pokiwaj dumnie");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)  
    {
        case "kopnij":
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
            break;
            
        case "opluj": set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
            break;
            
		case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                {
                 command("powiedz Pani, wszak to nie przystoi!");           
                }
              else
                {
                   command("powiedz Oj, nie jestem ju� dzieckiem.");
                }
			  
            break;
              }
        
        case "przytul": 
        {
            command("powiedz ^Zo^lnierza na s^lu^zbie nie wolno tuli^c!");
            write("Ucieka od ciebie na drug^a stron^e ulicy i poprawia "+
                "przekrzywion^a czapeczk^e.\n");
            saybb("Ucieka od "+ QIMIE(this_player(),PL_DOP) +" na drug^a "+
                "stron^e ulicy i poprawia przekrzywion^a czapeczk^e.\n");
        }
            break;
            
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
            break;
    }
}

void
nienajlepszy(object wykonujacy)
{
    write("^Lobuzerski jasnow^losy ch^lopiec m^owi odskakuj^ac od ciebie: "+
        "Bo zaraz posmakujesz mego gniewu!\n");
    saybb("^Lobuzerski jasnow^losy ch^lopiec m^owi odskakuj^ac "+
        "od "+ QIMIE(this_player(),PL_DOP) +": Bo zaraz posmakujesz mego "+
        "gniewu!\n");
} 

void
attacked_by(object wrog)
{
    if(walka==0) 
    {
	command("krzyknij M^oj tata jest ^zo^lnierzem, przyjdzie tutaj i ci^e "+
        "zabije!");
    set_alarm(5.0, 0.0, "czy_walka", 1);
    walka = 1;
    return ::attacked_by(wrog); 
    }

    else if(walka == 1)
    {
      set_alarm(1.0, 0.0, "command", "spojrzyj z przerazeniem "+
          "na "+ this_player()->query_name(PL_DOP) +"");
      set_alarm(10.0, 0.0, "czy_walka", 1);
      walka = 1;

      return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {

       this_object()->command("emote rozgl^ada si^e ze ^lzami w oczach.");

       walka = 0;
       return 1;
    }
       else
       {
       set_alarm(5.0, 0.0, "czy_walka", 1);
       }

}

string
event_dziewczynka()
{
    string str;
    object dzieciak = find_living("dzieciak3");
    if (objectp(dzieciak)) {
        if (environment(this_object())==environment(dzieciak)) {
            // FIXME: trzeba by jako� dynamicznie wydobywa� shorta dziewczynki.
            str ="emote wskazuje na �liczn� rudow�os� dziewczynk�" +
                ": Tak naprawd^e to jej matka jest krawcow^a, a " +
                "ojca nikt nie zna...";
        }
    }
    return str;
}

