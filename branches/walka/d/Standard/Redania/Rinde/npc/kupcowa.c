/* opisy Faeve,
   zrobil gjoef*/



#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <object_types.h>
#include <pogoda.h>
#include "dir.h"
inherit RINDE+"std/npc.c";
inherit "/lib/sklepikarz";

int walka = 0;

int
co_godzine();

create_rinde_npc()
{
    set_living_name("kupcowazrynku");

    ustaw_odmiane_rasy("kobieta");
    set_gender(G_FEMALE);

    set_long("@@dlugi_opis@@");

    dodaj_przym("t^egawy", "t^egawi");
    dodaj_przym("pogodny", "pogodni");
    
    set_act_time(38);
    add_act("emote szybkim ruchem r^eki odgarnia w^losy z czo^la.");
    add_act("usmiech milo");
    add_act("usmiech zachecajaco");
    add_act("emote odrzuca na plecy gruby, przewi^azany czerwon^a " +
    "aksamitk^a warkocz.");
    add_act("krzyknij Najlepsze tkaniny z dalekich krain!");
    add_act("krzyknij Jedwabie, at^lasy, z^lotog^lowia! Wszystko u mnie!");
    add_act("krzyknij Najlepsze tkaniny z dalekich krain!");
    add_act("krzyknij Jedwabie, at^lasy, z^lotog^lowia! Wszystko u mnie!");

    set_cchat_time(15);
    add_cchat("Nie, prosz^e! Mam male^nkie dzieci!");
    add_cchat("Stra^z!");
    add_cchat("Stra^z, na pomoc!");

    set_default_answer(VBFC_ME("default_answer"));
    add_ask("prac^e", VBFC_ME("pyt_praca"));
    add_ask("pomoc", VBFC_ME("pyt_pomoc"));
    add_ask("towar", VBFC_ME("pyt_towar"));

    set_stats (({30, 60, 28, 50, 60, 35}));

    set_skill(SS_APPR_OBJ, 70 + random(4));
    set_skill(SS_APPR_VAL, 83 + random(10));
    set_skill(SS_AWARENESS, 70 + random(10));
    set_skill(SS_UNARM_COMBAT, 30 + random(5));

    add_armour(RINDE_UBRANIA + "sukienka_prosta_czerwona_L.c");
    add_armour(RINDE_UBRANIA + "buty/skorzane_na_obcasie_L.c");
    
    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_HEIGHT, 175);
    
    set_alarm_every_hour("co_godzine");
    co_godzine();

    config_default_sklepikarz();
    set_money_greed_buy(110);
    set_money_greed_sell(109);
	set_money_greed_change(106);
    set_co_skupujemy(O_SKORY);

    set_store_room("/d/Standard/Redania/Rinde/Centrum/lokacje/magazyn_kupcowej.c");
    
    set_alarm(6.0, 0.0, "command", "stan za drugim straganem");
    set_alarm(9.0, 0.0, "command", "emote k^ladzie towar na straganie.");
    
    //add_prop(NPC_M_NO_ACCEPT_GIVE, 0);
}

void 
shop_hook_tego_nie_skupujemy(object ob)
{
    command("powiedz Ale ja skupuj� tylko sk�ry!");
}

string
dlugi_opis()
{
    string str;
    str = "Lekka oty^lo^s^c wcale nie przeszkadza tej kobiecie w " +
    "nieustannym krz^ataniu si^e wok^o^l straganu. Mimo ^ze ca^ly czas " +
    "zaj^eta, jest gotowa odpowiedzie^c chyba na ka^zde pytanie ogl^adaj^" +
    "acych towar mieszczan. Na jej twarzy przez ca^ly czas go^sci " +
    "przyjazny i serdeczny u^smiech. Widoczna w ruchach kobiety swoboda " +
    "wskazuje, ^ze handel ma we krwi. ";

    if (environment(this_object())->pora_roku() != MT_ZIMA)
        str += "Ubrana jest w prost^a, czerwon^a sukienk^e bez r^ekaw^ow";
    else
        str += "Ubrana jest w czerwon^a sukienk^e i narzucon^a " +
        "na^n ciep^l^a we^lnian^a opo^ncz^e";

    str += ", na nogach za^s ma czarne sk^orzane buty na niewysokim, ale zgrabnym obcasiku, " +
    "z cholewk^a nieco ponad kostk^e. Pod kolor sukni dobrana jest " +
    "aksamitna wst^a^zka, kt^or^a kobieta zwi^aza^la gruby, si^egaj^acy " +
    "pasa warkocz kasztanowych w^los^ow.\n";
    
    return str;
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if ((environment(this_object()) == environment(player)) && CAN_SEE(this_object(),
    player))
	this_object()->command(tekst);
	else
    this_object()->command("wzrusz ramionami");
}

string
default_answer()
{
    string odp = " ";
    
    TO->command("usmiechnij sie serdecznie do " + OB_NAME(TP));
    
    switch(random(3))
    {
        case 0:
            odp += "Przykro mi, ale nie rozumiem.";
            break;
        case 1:
            odp += "Nie wiem, co masz na my^sli.";
            break;
    }
    
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(), "powiedz do " +
        OB_NAME(this_player()) + odp);
    return "";
}

string
pyt_pomoc()
{
    if (this_player()->query_prop("pytal_kupcowazrynku_praca") == 0)
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " Nie, dzi^ekuj^e. Ale je^sli b^ed^e czego^s potrzebowa^la - dam " +
        "zna^c.");
        set_alarm(2.5, 0.0, "powiedz_gdy_jest", TP, "usmiech milo do " + OB_NAME(TP));
        TP->add_prop("pytal_kupcowazrynku_praca", 1);
    }
    else
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " Przecie^z ju^z m^owi^lam.");
        
    return "";
}

string
pyt_praca()
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " Handel to moje ^zycie i utrzymanie.");
    set_alarm(2.5, 0.0, "command", "wskaz drugi stragan");

    return "";
}

string
pyt_towar()
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
    " Sprowadzam wszystko z dalekich krain. Jednak je^sli masz co^s " +
    "ciekawego, to s^adz^e, ^ze mo^zemy zawrze^c uk^lad.");
    set_alarm(2.5, 0.0, "powiedz_gdy_jest", TP, "oczko do " + OB_NAME(TP));
}

void
return_introduce(object ob)
{
    if (environment() == environment(ob))
    {
        set_alarm(1.0, 0.0, "command", "powiedz do " +
        OB_NAME(this_player()) + " Bardzo mi mi^lo.");
        set_alarm(2.0, 0.0, "command", "usmiech zachecajaco do " + OB_NAME(TP));
    }
    
    return;
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
    	case "nadepnij":
	    set_alarm(2.0, 0.0, "zly", wykonujacy);
            break;

        case "szturchnij":
	    set_alarm(2.0, 0.0, "taki_sobie", wykonujacy);
            break;

        case "za�miej":
        case "poca�uj":
        case "przytul":
        case "pog�aszcz":
	    set_alarm(2.0, 0.0, "dobry", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
    set_alarm(1.5, 0.0, "command", "zagryz wargi nerwowo");

    if (kto->query_prop("wkurzyl_kupcowazrynku_blabla") < 2)
    {
        powiedz_gdy_jest(kto, "powiedz do " + OB_NAME(kto) +
        " Czy ja co^s pan" + TP->koncowka("u", "i") + " zrobi^lam?");
        kto->add_prop("wkurzyl_kupcowazrynku_blabla", kto->query_prop("wkurzy" +
        "l_kupcowazrynku_blabla") + 1);
    }
    else
    {
        command("krzyknij Stra^zeee!");
        set_alarm(1.0, 0.0, "command", "sapnij ciezko");
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", kto, "popatrz z wyrzutem " +
        "na " + OB_NAME(TP));
    }
    
    return;
}

void
taki_sobie(object kto)
{
    command("zagryz wargi");
}

void
dobry(object kto)
{
    switch(random(3))
    {
        case 0:
            powiedz_gdy_jest(kto, "powiedz do " + OB_NAME(kto) +
            " Nie r^ob tego wi^ecej.");
            break;
        case 1:
             powiedz_gdy_jest(kto, "powiedz do " + OB_NAME(kto) +
             " Prosz^e mnie zostawi^c w spokoju!");
             break;
    }
    
    set_alarm(2.0, 0.0, "command", "skrzyw sie z niesmakiem");
    return;
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {
	    command("wyjdz zza straganu");

        set_alarm(5.0, 0.0, "czy_walka", 1);
        walka = 1;

        return ::attacked_by(wrog);
    }

    else if(walka == 1)
    {
        set_alarm(1.0, 0.0, "command", "sapnij szybko");
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;

        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
        set_alarm(1.0,0.0, "command", "sapnij szybko");
        go_to("/d/Standard/Redania/Rinde/Centrum/lokacje/placne",
            &command("stan za drugim straganem"));
        walka = 0;

        return 1;
    }
    else
        set_alarm(5.0, 0.0, "czy_walka", 1);
}

void
shop_hook_list_empty_store(string str)
{
    command("powiedz Obecne nie mam niczego na sprzeda�, ale prosz� "+
    		"zajrze� jeszcze niebawem!");
}


void
hook_zabr_podrz_wlas_drog(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "usmiech leciutko");
}

void
hook_zabr_podrz_wlas_tan(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Prosz^e pilnowa^c swoich rzeczy, ja tu wszystkiego nie ogarn^e.");
}

void
hook_zabr_podrz_niewl_wljest(object kto, object co, string czyje)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Hej, zaraz! T" + co->koncowka("en", "a", "o", "e", "e") + " " +
    co->query_nazwa() + " jest te" + find_player(lower_case(co->query_prop("podrzucon" +
    "e_na_stragan")))->koncowka("go pana", "ej pani", "go pana") + "!");
    set_alarm(1.5, 0.0, "command", "wskaz " +
    OB_NAME(find_player(co->query_prop("podrzucone_na_stragan"))));
    co->add_prop("podrzucone_na_stragan", czyje);
}

void
hook_zabr_podrz_niewl_wlniema(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Hej, to nie pan" + kto->koncowka("a", "i", "a") + "!");
}

void
hook_zabr_pierwsza_rzecz(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Prosz^e pan" + TP->koncowka("a", "i", "a") + ", te przedmioty nie " +
    "s^a do brania! Niech^ze to pan" + TP->koncowka("", "i") + " od^lo^zy!");
}

void
hook_zabr_pierwsza_rzecz_vip(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "popatrz . na " + OB_NAME(kto));
}

void
hook_zabr_druga_rzecz(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "krzyknij Hej, oddaj to! Straaa^ze!");
    set_alarm(2.0, 0.0, "command", "wyjdz zza straganu");
    set_alarm(3.0, 0.0, "command", "zabij " + OB_NAME(kto));
}

void
hook_zabr_podrz_wlas_x(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Prosz^e natychmiast odda^c to, co pan" + kto->koncowka(" zabra^l",
    "i zabra^la") + "!");
}

void
hook_pol_swoj_tan(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Lepiej niech to pan" + TP->koncowka("", "i") + " st^ad zabierze, " +
    "jeszcze kto^s ukradnie.");
}

void
hook_pol_swoj_drog_wljest(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "rozejrzyj sie szybko");
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Prosz^e to zabra^c z mojego straganu!");
}

void
hook_pol_swoj_drog_wlniema(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "powiedz :cicho: Gdzie ci ludzie maj^a oczy?");
    set_alarm(2.0, 0.0, "command", "wez " + OB_NAME(co));
    set_alarm(3.0, 0.0, "command", "pokrec ciezko");
}

void
hook_pol_zabrane_nieten(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "powiedz O! No i si^e znalaz^la zguba.");
    set_alarm(2.0, 0.0, "command", "podziekuj serdecznie " + OB_NAME(kto));
}

void
hook_pol_zabrane_ten(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Nast^epnym razem prosz^e poprosi^c, to pan" + TP->koncowka("u", "i") +
    " poka^z^e.");
    set_alarm(2.0, 0.0, "command", "popatrz uwaznie na " + OB_NAME(kto));
}

void
hook_pol_swoj_ten(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Co to za ^zarty? W tej chwili prosz^e mi odda^c moj^a w^lasno^s^c!");
    set_alarm(2.0, 0.0, "command", "sapnij ciezko");
}

void
hook_pol_zabrane_zlodziej(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " A reszta?");
}

void
hook_oddal_zabr_ten(object kto, object co)
{
    hook_pol_zabrane_ten(kto, co);
}

void
hook_oddal_zabr_nieten(object kto, object co)
{
    hook_pol_zabrane_nieten(kto, co);
}

void
hook_dal_ten(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Co to za ^zarty? W tej chwili prosz^e mi odda^c moj^a w^lasno^s^c!");
    if (environment() == environment(kto))
    {
        co->remove_prop("podrzucone_na_stragan");
        set_alarm(2.0, 0.0, "command", "daj " + OB_NAME(co) + " " +
        OB_NAME(kto));
    }
    else
        set_alarm(2.0, 0.0, "command", "poloz " + OB_NAME(co) + " na " +
        this_object()->query_prop("stoi_za_straganem"));
}

void
hook_dal_ktos(object kto, object co)
{
    if(!co->query_coin_type())
    {

    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Dzi^ekuj^e, nie potrzebuj^e niczego od pan" + kto->koncowka("a", "i") +".");
    if (environment() == environment(kto))
    {
        co->remove_prop("podrzucone_na_stragan");
        set_alarm(2.0, 0.0, "command", "daj " + OB_NAME(co) + " " +
        OB_NAME(kto));
    }
    else
        set_alarm(2.0, 0.0, "command", "poloz " + OB_NAME(co) + " na " +
        this_object()->query_prop("stoi_za_straganem"));
    }
    
}

void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if (from->query_humanoid() == 0)
        return;

    if (ob->query_prop("zabrane_ze_straganu"))
    {
        if (from->query_prop("zabral_ze_straganu"))
        {
            hook_oddal_zabr_ten(from, ob);
            ob->remove_prop("zabrane_ze_straganu");
            from->remove_prop("zabral_ze_straganu");
            return;
        }
        else
        {
            hook_oddal_zabr_nieten(from, ob);
            ob->remove_prop("zabrane_ze_straganu");
            return;
        }
    }
    else
    {
        if (from->query_prop("zabral_ze_straganu"))
        {
            ob->add_prop("podrzucone_na_stragan", from->query_name());
            hook_dal_ten(from, ob);
            return;
        }
        else
        {
            ob->add_prop("podrzucone_na_stragan", from->query_name());
            hook_dal_ktos(from, ob);
            return;
        }
    }
}

void
leave_inv(object ob, object to)
{
    ::leave_inv(ob, to);
}

void
znik()
{
    set_alarm(5.0, 0.0, "command", "emote znika w drzwiach jednej z kamieniczek.");
    remove_object();
}

void
do_domu()
{
    if(query_attack())
    {
        set_alarm(10.0,0.0,"do_domu");
        return;
    }
    else
    {
        int k = 0;
        mixed droga2=znajdz_sciezke(environment(this_object()),
            POLNOC + "lokacje/ulica3");
        command(droga2[k]);
        if(k==sizeof(droga2)-1)
        {
            znik();
            return;
        }
        else
        {
            k++;
            set_alarm(15.0,0.0,"do_domu");
            return;
        }
    }
}

int
co_godzine()
{
    if (environment(TO)->pora_dnia() >= MT_WIECZOR)
    {
        set_alarm(50.0, 0.0, "command", "emote zdejmuje towar ze straganu.");
        set_alarm(53.0, 0.0, "command", "wyjdz zza straganu");
        set_alarm(55.0, 0.0, "do_domu");
    }
}

void
init()
{
    ::init();
    init_sklepikarz();
}
