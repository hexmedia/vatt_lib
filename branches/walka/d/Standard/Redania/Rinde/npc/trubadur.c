#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include "dir.h"
inherit RINDE+"std/npc.c";

void
create_rinde_npc()
{
    set_living_name("urlen");

    ustaw_odmiane_rasy("trubadur");
    set_gender(G_MALE);

    set_surname("Jowult");
    set_origin("z Rinde");

    ustaw_imie(({"urlen", "urlena", "urlenowi", "urlena", "urlenem",
	"urlenie"}), PL_MESKI_OS);

    set_long("Trze�wo�� najwyra�niej nie przystaje do wizerunku tego " +
	"m�czyzny. Jego jasnoniebieskie oczy bez ustanku b��dz� po " +
	"pomieszczeniu w poszukiwaniu bratniej duszy. Co chwila obdarza " +
	"przechodz�ce obok niego kelnerki spojrzeniami pe�nymi uczucia i " +
	"po��dania, szczeg�lnie bacznie przygl�daj�c si� kobiecym kr�g�o�ciom. " +
	"Czasem jaki� sta�y bywalec stawia mu kolejny nap�j, kt�ry " +
	"trubadur spo�ywa z wielk� ch�ci� i bez oci�gania. W d�oniach " +
	"kurczowo trzyma sw� mandolin�, a czasem nawet zdarzy mu si� co� na " +
	"niej pobrzd�ka�. Figlarnie przekrzywiony kapelusz na jego g�owie " +
	"dope�nia wizerunku pijanego barda w karczemnej sali.\n");

    dodaj_przym("pogodny", "pogodni");
    dodaj_przym("nietrze�wy", "nietrze�wi");

    set_act_time(30);
    add_act("emote brzd�ka co� cicho na swojej mandolinie.");
    add_act("emote przygl�da si� z przej�ciem po�ladkom jednej z kelnerek.");
    add_act("emote stuka si� kieliszkiem z jednym z go�ci.");
    add_act("emote zaczyna �piewa� jak�� znan� pie��, " +
	"jednak ze wzgl�du na stan swego upojenia po chwili przestaje.");
    add_act("emote czka pijacko.");
    add_act("emote wyznaje mi�o�� jakiej� przedstawicielce p�ci pi�knej, " +
	"po czym zostaje przez ni� spoliczkowany.");
    add_act("emote zaczyna be�kota� o nieszcz�liwej mi�o�ci.");
    add_act("emote poprawia sw�j kapelusz, przekrzywiaj�c go zawadiacko.");

    set_default_answer(VBFC_ME("default_answer"));

    set_stats ( ({ 65, 40, 80, 21, 30, 58 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    add_armour(RINDE_UBRANIA + "spodnie_jasne_bufiaste");
    add_armour(RINDE_UBRANIA + "kamizelka_pomaranczowa_fikusna");
    add_armour(RINDE_UBRANIA + "kapelusz_z_piorkiem_czarny");
    
//    add_prop(CONT_I_WEIGHT, 91000);
//    add_prop(CONT_I_HEIGHT, 172);
}

void
start_me()
{
    command("usiadz przy pierwszym stole");
}

string
default_answer()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
        "emote be�kocze pijacko: Tak! To ja jestem bardem nad bardami, " +
	"wiedzia�em, �e o mnie s�yszeli wszyscy!");
     return "";
}

void
return_introduce(string imie)
{
    object osoba = present(imie, environment());

    if (osoba)
    {
        command("przedstaw sie " + OB_NAME(osoba));
	command("emote be�kocze z dum�: Najlepszy trubadur w�r�d trubadur�w!");
	command("emote czka pewnie.");
    }
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
	   case "nadepnij":
	    set_alarm(1.0, 0.0, "command_present", this_player(),
		"emote be�kocze: No wiesz?");
	    set_alarm(1.5, 0.0, "command_present", this_player(),
		"emote czka z oburzeniem.");
            break;

        case "poca�uj":
        case "przytul":
        case "pog�aszcz":
	    set_alarm(1.0, 0.0, "command_present", this_player(),
		"emote czka pijacko.");
	    set_alarm(1.5, 0.0, "command_present", this_player(),
    		"emote spogl�da na ciebie niepewnie, a po chwili na jego ustach " +
		"pojawia si� rozanielony u�miech.");
            break;
    }
}

void
attacked_by(object wrog)
{
    if (random(2))
	command("emote zaczyna drze� si� wniebog�osy.");
    else
	command("emote kuli si� z przestrachem, zas�aniaj�c g�ow� r�kami.");

    ::attacked_by(wrog);
}
