#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>

#include "dir.h"
inherit RINDE+"std/npc.c";

int golebie_alrm1;
int golebie_alrm2;

void
create_rinde_npc()
{
    set_living_name("goured");

    ustaw_imie( ({"goured", "goureda", "gouredowi", "goureda", "gouredem",
	"gouredzie"}), PL_MESKI_OS );
    set_surname("Korenf");
    set_origin("z Rinde");
    set_title(", w�a�ciciel karczmy \"Pod weso�ym D�inem\"");

    ustaw_odmiane_rasy("m�czyzna");
    dodaj_nazwy("karczmarz");
    set_gender(G_MALE);

    set_long("M�czyzna na kt�rego spogl�dasz bez w�tpienia jest " +
	"tutejszym karczmarzem i ka�dego go�cia wita zawsze szerokim " +
	"u�miechem, nie omieszkaj�c zamieni� z ka�dym par� s��w. �atwo " +
	"si� domy�le�, �e jest w swoim �ywiole, ci�gle przebywaj�c z " +
	"innymi i z nimi rozmawiaj�c. Wbrew pozorom jest szczup�y jednak " +
	"posiada du�y nabrzmia�y brzuch, przypominaj�cy nieco brzuch " +
	"ci�arnej przedstawicielki p�ci pi�knej. W pasie owini�ty jest " +
	"bia�ym fartuchem, nieco ju� przybrudzonym od cz�stego wycierania " +
	"w niego r�k. M�czyzna ani na chwil� nie staje d�u�ej w miejscu, " +
	"ci�gle krz�taj�c si� za barem lub poganiaj�c kelnerki.\n");

    dodaj_przym("gadatliwy", "gadatliwi");
    dodaj_przym("brzuchaty", "brzuchaci");

    set_act_time(30);
    add_act("emote rozgl�da si� energicznie po sali.");
    add_act("emote pucuje kufle szmatk�.");
    add_act("emote m�wi oblizuj�c si� ze smakiem: Ale� bym sobie go��bia przyrz�dzi�...");
    add_act("emote wita szerokim u�miechem nowoprzyby�ych go�ci.");
    add_act("emote podchodzi do grupy klient�w i udziela si� w weso�ej pogaw�dce.");
    add_act("emote pogania szybko kelnerki.");
    add_act("emote m�wi drapi�c si� po g�owie: Lepszy go��b pieczony, czy duszony? Hmmm...");
    add_act("emote kr�ci si� z przej�ciem po gospodzie.");
    add_act("emote wychodzi na chwil� do kuchni, a po chwili wraca z jakimi� potrawami.");

    set_default_answer(VBFC_ME("default_answer"));

    set_stats ( ({ 65, 31, 70, 21, 30, 58 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    add_ask(({"go��bie", "go��bki", "go��bka", "go��bia"}), VBFC_ME("pyt_o_golebie"));
    add_ask(({"prac�", "robot�", "pomoc"}), VBFC_ME("pyt_o_prace"));

    add_armour(RINDE_UBRANIA + "fartuch_bialy_przybrudzony");
    add_armour(RINDE_UBRANIA + "spodnie/ciemne_zwykle_Mc.c");
    add_armour(RINDE_UBRANIA + "koszula_brazowa_lniana");

    remove_prop(NPC_M_NO_ACCEPT_GIVE);
    
//    add_prop(CONT_I_WEIGHT, 91000);
//    add_prop(CONT_I_HEIGHT, 172);
}

int
query_karczmarz()
{
	return 1;
}

string
pyt_o_golebie()
{
    set_alarm(0.5, 0.0, "command_present", this_player(),
            "emote m�wi mru��c oczy: Smacznych go��bi nigdy za wiele! Sma�one, duszone, pieczone...");
    set_alarm(2.0, 0.0, "command_present", this_player(),
            "emote m�wi: Jak b�dziesz jakie� mia�, daj mi, a godziwe pieni�dze wtedy otrzymasz...");
    set_alarm(2.5, 0.0, "command_present", this_player(),
            "emote mruga porozumiewawczo.");
    return "";
}

string
pyt_o_prace()
{
    set_alarm(0.5, 0.0, "command_present", this_player(),
            "emote m�wi: Chwilowo nie potrzebuj� �adnej pomocy.");
    set_alarm(2.0, 0.0, "command_present", this_player(),
            "emote m�wi u�miechaj�c si� lekko: Ale w sumie gdyby� zdoby� jakie� go��bie...");
    set_alarm(2.5, 0.0, "command_present", this_player(),
            "emote mruga porozumiewawczo.");
    return "";
}

string
default_answer()
{
     set_alarm(1.0, 0.0, "command_present", this_player(),
             "emote macha niedbale r�k�.");
     set_alarm(1.5, 0.0, "command_present", this_player(),
             "emote m�wi: Tak, tak, ciekawe co na to inni!");
     set_alarm(2.0, 0.0, "command_present", this_player(),
             "usmiech serdecznie do " + OB_NAME(this_player()));
     return "";
}

void
return_introduce(string imie)
{
    object osoba = present(imie, environment());

    if (osoba)
    {
        command("przedstaw sie " + OB_NAME(osoba));
    }
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
	case "nadepnij":
	    set_alarm(1.0, 0.0, "command_present", wykonujacy,
		"emote m�wi z oburzeniem �ywo gestykuluj�c r�kami: Je�li tak masz robi�, lepiej id� precz!");
            break;

        case "poca�uj":
        case "przytul":
        case "pog�aszcz":
	    set_alarm(1.0, 0.0, "command_present", wykonujacy,
		"emote chrz�ka niepewnie i rumieni si� delikatnie.");
            break;
    }
}

void
attacked_by(object wrog)
{
    command("krzyknij Na pomoc! Zb�je!");
    ::attacked_by(wrog);
}

void
oddajemy(object ob, object old)
{
    if (interactive(old) && (ENV(old) == ENV(TO))) {
        command_present(old, "emote m�wi: Nie interesuj� mnie takie rzeczy.");
        command_present(old, "daj " + OB_NAME(ob) + " " + OB_NAME(old));
        return;
    }
}

void
nowy_golab(object ob, object old)
{
    if (interactive(old) && (ENV(old) == ENV(TO))) {
        int zaplata = ((ob->query_prop(OBJ_I_WEIGHT) > 560) ? random(3)+5 : random(4)+1);
        MONEY_ADD(old, zaplata);
        old->catch_msg(QCIMIE(TO, PL_MIA)+" wyp�aca ci " + zaplata + " groszy za " +
                ((zaplata >= 5) ? "du�ego go��bia" : "ma�ego go��bka") +".\n");
        tell_room(ENV(old), QCIMIE(TO, PL_MIA)+" wyp�aca "+QIMIE(old, PL_CEL)+" jakie� monety.\n", old);
        if (golebie_alrm1)
            remove_alarm(golebie_alrm1);
	    golebie_alrm1 = set_alarm(1.0, 0.0, "command_present", old,
                "emote m�wi: Je�li znajdziesz wi�cej go��bi, wiesz ju�, co z nimi robi�...");
        if (golebie_alrm2)
            remove_alarm(golebie_alrm2);
        golebie_alrm2 = set_alarm(1.5, 0.0, "command_present", old,
                "emote mruga porozumiewawczo.");
    }
    ob->remove_object();
}

void
enter_inv(object ob, object old)
{
    ::enter_inv(ob, old);
    if ((function_exists("create_container", ob) == "/std/corpse") &&
            (ob->query_race() == "go��b")) {
        set_alarm(0.1, 0.0, &nowy_golab(ob, old));
    }
    else {
        set_alarm(0.1, 0.0, &oddajemy(ob, old));
    }
}
