
/* Straznik - krasnolud z banku w Rinde  *
 * Made by Avard 04.06.07                *
 * Opis by Tinardan & Avard              */

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"
 inherit RINDE+"std/npc.c";
 void
create_rinde_npc()
 {
     ustaw_odmiane_rasy(PL_KRASNOLUD);
     set_gender(G_MALE);
     dodaj_nazwy("straznik");
     dodaj_przym("ponury","ponurzy");
     dodaj_przym("umi^e^sniony","umi^e^snieni");

     set_long("Spod g^estej grzywy spl^atanych w^los^ow spogl^adaj^a na "+
         "ciebie stalowe oczy, w kt^orych wyczyta^c mo^zna jedynie "+
         "uwielbienie dla zadawania b^olu. Broda, elegancko uczesana w "+
         "warkoczyki i nat^luszczona opada a^z na brzuch, os^loniony grubym "+
         "pancerzem. W r^eku krasnoluda b^lyszczy ogromny top^or.\n");

     set_stats (({ 100, 30, 110, 40, 25, 80 }));
     add_prop(CONT_I_WEIGHT, 100000);
     add_prop(CONT_I_HEIGHT, 140);
     add_prop(LIVE_I_NEVERKNOWN, 1);

    set_act_time(30);
	add_act("emote rozgl^ada si^e.");
	add_act("emote wa^zy top^or w d^loni.");
	add_act("rozejrzyj sie leniwie");
       add_act("emote spluwa na ziemi^e.");
	
	set_cchat_time(10);
	add_cchat("Ha! Ju^z po tobie!");
	add_cchat("Nikt nie prze^zy^l spotkania z moim toporem!");
	add_cchat("Gi^n!");
    add_cchat("Haaa!");

    //add_armour(cos);
    //add_armour(cos);
    //clone_object(sdf)->move(npc);

    add_ask(({"bank"}), VBFC_ME("pyt_o_bank"));
	add_ask(({"ruygena","bankiera"}), VBFC_ME("pyt_o_bankiera"));
	add_ask(({"stra^znik^ow","stra^z"}), VBFC_ME("pyt_o_straz"));
	set_default_answer(VBFC_ME("default_answer"));

 }
 
	void
	powiedz_gdy_jest(object player, string tekst)
	{
		if (environment(this_object()) == environment(player))
		this_object()->command(tekst);
	}

	string
	pyt_o_bank()
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
		"emote m^owi: Ta, pracuje tutaj. Ale co ci do tego?");
		return "";
	}
	string
	pyt_o_bankiera()
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
			"emote m^owi: P^laci mi, a to najwa^zniejsze.");
		return "";
	}
	string
	pyt_o_straz()
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
			"emote m^owi u^smiechaj^ac si^e w nieprzyjemny spos^ob: "+
            "Lubie z nimi pracowa^c, to profesjonali^sci w zadawaniu b^olu.");
		return "";
	}
	string
	default_answer()
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_BIE) +
        "G^owno ci^e to obchodzi.");
		return "";
	}


    void
    add_introduced(string imie_mia, string imie_bie)
    {
        set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
    }
    void
    return_introduce(object ob)
    {
    command("powiedz Nic mnie to nie obchodzi.");
    command("splun");
    }


    void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
			        break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
		case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(2))
                 {
                 case 0:command("pocaluj "+OB_NAME(wykonujacy)); break;		
                 case 1:command("usmiechnij sie lubieznie"); break;
                }
              else
                {
                   switch(random(2))
                   {
                   case 0:command("emote czerwienieje ze z^lo^sci.");
                          command("zabij "+OB_NAME(wykonujacy)); break;
                   case 1:command("spojrzyj z obrzydzeniem na "+
                        OB_NAME(wykonujacy));
                          command("zabij "+OB_NAME(wykonujacy)); break;
                   }
                }
			  
                break;
                }
        case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{

   switch(random(2))
   {
     case 0: command("spojrzyj gniewnie na "+OB_NAME(wykonujacy)); break;
     case 1: command("powiedz Wyno^s si^e."); break;
 
   }
}

void
nienajlepszy(object wykonujacy)
{
   switch(random(1))
   {
      case 0: command("zabij "+OB_NAME(wykonujacy)); break;
   }
} 

