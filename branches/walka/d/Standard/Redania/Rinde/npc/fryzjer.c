/* Super fryzjer :>
 * W wiekszosci opis by Tinardan, reszta Lil.
 */  

#include <stdproperties.h>
#include <macros.h>
#include <money.h>
#include <pl.h>
#include <sit.h>
#include "dir.h"

inherit RINDE+"std/npc.c";
inherit "/lib/fryzjer";
//inherit "/lib/balwierz"; //TODO ;)
inherit "/lib/tatuazysta";

int czy_walka();
int pomoc_nadchodzi();
int walka = 0;
int przekluwanie (string str);


void
create_rinde_npc()
{
    ustaw_imie( ({"fryderyk","fryderyka","fryderykowi","fryderyka",
             "fryderykiem","fryderyku"}),
              PL_MESKI_OS);

    dodaj_przym("szczup�y","szczupli");
    dodaj_przym("nerwowy","nerwowi");
    dodaj_nazwy("fryzjer");

    set_long("Wysoki, szczup�y jegomo�� ubrany elegancko, "+
         "cho� nieco krzykliwie. Ciemne w�osy ma utrefione "+
	 "nad czo�em w male�kie loczki, a na brodzie "+
	 "sterczy modna obecnie, kozia br�dka. Ruchy "+
	 "fryzjera s� troch� nerwowe, artystyczne a "+
         "jednocze�nie troch� kobiece.\n");
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_gender(G_MALE);
    set_title("di Babeu");

    add_prop(CONT_I_WEIGHT, 85000);
    add_prop(CONT_I_HEIGHT, 190); 
    add_prop(NPC_I_NO_RUN_AWAY, 1);

    add_armour(RINDE_UBRANIA + "cizemki/miekkie_skorzane_Mc.c");
    add_armour(RINDE_UBRANIA + "spodnie/czarne_skorzane_Mc.c");
    add_armour(RINDE_UBRANIA + "koszule/biala_jedwabna_Mc.c");

    set_stats(({ 19, 65, 35, 55, 44, 37}));
    
    set_default_answer(VBFC_ME("default_answer"));


    set_chat_time(10);
    add_chat("Kto ci^e oszpeci^l tak^a brzydk^a fryzur^a?");
    add_chat("Czas na ma^le strzy^zenie?");
    add_chat("Jestem najlepszy w swoim fachu!");


    set_cchat_time(5);
    add_cchat("Pomocy!");
    add_cchat("Dlaczego mnie bijesz?");
    add_cchat("Ja tu tylko strzyg�!");
    add_cchat("Prosz�! Przesta�!");

    set_act_time(10);
    add_act("emote zbiera ^sci^ete w^losy z pod^logi.");
    add_act("emote czy^sci no^zyczki.");
    add_act("emote poprawia swoj^a fryzur^e.");
    add_act("emote wrzuca kawa^lki ^sci^etych w^los^ow do " +
            "work^ow le^z^acych pod ^scian^a.");
    add_act("emote rozgl^ada si^e z roztargnieniem.");

    create_fryzjer();
    create_tatuazysta();
//	create_balwierz();
    set_cena_tatuazu(1440);
    set_cena_usuniecia_tatuazu(43200);
    set_cena_fryzury(960);
    set_cena_farbowania(4320);
    
}

void
init()
{
    ::init();
    init_tatuazysta();
    init_fryzjer();
    add_action(przekluwanie, "przek�uj");
}


int
przekluwanie(string str)
{
        int wynik;
        object dziurka;

        notify_fail("Przek�uj co?\n");
        if(!str)
                return 0;

        if(!this_player()->query_prop(SIT_SIEDZACY))
	  {
	     say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
             this_object()->short(PL_NAR)+".\n");
	     set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Najpierw usi�d�, prosz�.");
             command("wskaz na fotel");
	     return 1;
	  }
	


        if(!(dziurka = present("_dziurka_od_kolczykow",this_player())))
                (dziurka = clone_object("/std/dziurka"))->move(this_player(), 1);

        if (!dziurka)
                {
		say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
                this_object()->short(PL_NAR)+".\n");
		set_alarm(1.0, 0.0, "command_present", this_player(),
                   "szepnij " + this_player()->query_name(PL_CEL) +
                   " Jejku jej! Wyst�pi� jaki� dziwny b��d, zg�o� go czarodziejom "+
		   "oni na pewno co� na to poradz�!");
	          return 1;
                }

        wynik = dziurka->sprawdz_dziurke(str);
        if(wynik == -1)
        {
	  say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
          this_object()->short(PL_NAR)+".\n");
	   set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Nie, nie, nie, w takich miejscach dziurek nie robi�!");
	          return 1;
	 }
        if(wynik == 0)
	{   
     		say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
           this_object()->short(PL_NAR)+".\n");
	   set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Ojej! Tam ju� nie zmie�ci si� �adna kolejna dziurka!");
	          return 1;
	 }

	if (!MONEY_ADD(this_player(),-480))
                {
		say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
                this_object()->short(PL_NAR)+".\n");
                  set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Ojej, zdaje si�, �e nie masz tyle pieni��k�w! "+
		   "Przek�uwanie kosztuje u mnie jedyne dwie korony.");
	          return 1;
                }
   say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
         this_object()->short(PL_NAR)+".\n");
	 	
   write(this_object()->short(PL_MIA) +
        " zbli�a si� ku tobie z du�� ig��, po czym "
       + "szybkim i wprawnym ruchem przek�uwa ci " + str + 
	 " pozostawiaj�c tam malutk� dziurk�.\n");

   say(this_object()->short(PL_MIA) + " zbli^za si� do " +
         QCIMIE(this_player(),PL_DOP)+" z du�� ig��, po czym szybkim "+
	  "i wprawnym ruchem przek�uwa "+this_player()->koncowka("jego","jej")+
          " " + str + " pozostawiaj�c tam malutk� dziurk�.\n");
   
   switch(random(4))
   {
     case 0: write("Nawet nie zabola�o...\n"); break;
     case 1: write("Troszeczk� ci� zabola�o...\n"); break;
     case 2: write("Nawet nic nie poczu�"+this_player()->koncowka("e�","a�")+".\n");
             break;
     case 3: write("Auuu! Zabola�o...\n"); break;
   }   


   dziurka->dodaj_dziurke(str);   
   command("usmiechnij sie z zadowoleniem");
   command("powiedz Gotowe! Mo�e odrobink� pokrwawi�, ale prosz� "+
           "si� tym nie przejmowa� i nie pi� alkoholu, p�ki si� "+
	   "nie zagoi!");
   write("Za us�ug� p�acisz dwie korony.\n");
        return 1;
}

void
hook_nie_ma_takiego_tatuazu()
{
   say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
         this_object()->short(PL_NAR)+".\n");
   set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Przykro mi, ale nie znam takiego wzoru.");

}

void
hook_masz_cos_na_sobie()
{
   say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
         this_object()->short(PL_NAR)+".\n");
   set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Hmmm, a mo�e najpierw si� rozbierzesz?");
   command("usmiechnij sie niepewnie");   
}

void
hook_trzymasz_cos()
{
   say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
         this_object()->short(PL_NAR)+".\n");
   set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Tak, tak, ale od�� wpierw prosz� swoje zabawki, "+
		   "kt�re trzymasz w �apkach.");
}

void
hook_juz_masz_tam_tatuaz()
{
    say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
         this_object()->short(PL_NAR)+".\n");
   set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Ale, ale! Ju� masz tam chyba tatua�, czy� nie?");
}

void
hook_nie_masz_tam_tatuazu()
{
    say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
         this_object()->short(PL_NAR)+".\n");
   set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Ale, ale! Ja tam �adnego tatua�u nie widz�!");
}


void
hook_tworzenie_tatuazu()
{
    say(QCIMIE(TO,PL_MIA)+" zabiera si� do pracy.\n");
    TP->catch_msg(QCIMIE(TO,PL_MIA)+" zabiera si� do pracy.\n");
    set_alarm(6.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Gotowe!");
}

void
hook_usuwanie_tatuazu()
{
  set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Gotowe!");
}
void
hook_nie_starczy_na_tatuaz()
{
   say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
         this_object()->short(PL_NAR)+".\n");
   set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Oh, przykro mi, ale nie masz tyle pieni��k�w.");
}
void
hook_nie_starczy_na_fryzure()
{
   say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
         this_object()->short(PL_NAR)+".\n");
   set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Oh, przykro mi, ale zdaje si�, �e nie "+
		   "masz tyle pieni��k�w. Strzy�enie kosztuje u mnie " +
		   "tylko cztery korony!");
}
void
hook_nie_starczy_na_farbowanie()
{
   say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
         this_object()->short(PL_NAR)+".\n");
   set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Oh, przykro mi, ale zdaje si�, �e nie "+
		   "masz tyle pieni��k�w. Farbowanie kosztuje u mnie " +
		   "tylko osiemna�cie koron!");
}

void
hook_za_krotkie_wlosy()
{
   say(QCIMIE(this_player(),PL_MIA)+" wymienia pare s��w z "+
         this_object()->short(PL_NAR)+".\n");
   set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Przykro mi, ale zdaje si�, �e masz za kr�tkie w�oski "+
		   "na tak� fryzurk�. Wr�� do mnie, jak urosn�.");
   command("rozloz rece");
}

void
hook_za_dlugie_wlosy()
{
   write(this_object()->short(PL_MIA)+" podcina ci w�osy do odpowiedniej "+
            "d�ugo�ci.\n");
   say(this_object()->short(PL_MIA)+" podcina " +
         this_player()->short(PL_CEL)+" w�osy.\n");    
}

/*void
hook_nie_pasuje_dlugosc()
{
}*/

void
hook_zamow_co()
{
	if(TP->query_gender() || TP->query_race() == "elf" || TP->query_wiek()<19)
    notify_fail("Co chcesz zam�wi�? Fryzur� czy tatua�? Je�li fryzur�, to "+
          "poprawn� sk�adni� jest:\nzam�w <d�ugo��> w�osy "+
	  "<typ>.\nNatomiast je�li interesuje "+
	  "ci� zamawianie tatua�u, u�yj sk�adni:\nzam�w <typ> tatua� "+
	  "<numer> <gdzie>.\n");
	else
	notify_fail("Co chcesz zam�wi�? Fryzur� czy tatua�? A mo�e brod� lub w�sy? "+
	"Je�li fryzur�, to "+
          "poprawn� sk�adni� jest:\nzam�w <d�ugo��> w�osy "+
	  "<typ>.\nNatomiast je�li interesuje "+
	  "ci� zamawianie tatua�u, u�yj sk�adni:\nzam�w <typ> tatua� "+
	  "<numer> <gdzie>.\n");/*+
		"A brod� lub zarost zamawiamy w ten spos�b:\nzam�w <d�ugo��> <typ> brod�/zarost.\n");*/
//    command("usmiechnij sie niepewnie");
}

void
hook_nie_ma_takiej_fryzury()
{
    say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
         this_object()->short(PL_NAR)+".\n");
    //write("Nie ma takiej fryzury.\n");
    set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Oh! Przykro mi, ale zdaje si�, �e nie znam "+
		   "takiej fryzury!");
    command("westchnij cicho");

}
/*   //To juz nie potrzebne, takiej opcji nie ma. L
void
hook_juz_masz_fryzure()
{
    write("Najpierw musisz usun�� star� fryzur�.\n");
    set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Ach! Musia�"+this_player()->koncowka("by�","aby�")+
		   " usun�� wpierw swoje poprzednie w�oski! Mo�esz "+
		   "zrobi� to u mnie, za grosze.");
}*/

int
hook_inne_sprawdzenia_przed_fryzura()
{ 
    if(!this_player()->query_prop(SIT_SIEDZACY))
    {
        say(QCIMIE(this_player(),PL_MIA)+" wymienia par^e s��w z "+
        this_object()->short(PL_NAR)+".\n");
	set_alarm(1.0, 0.0, "command_present", this_player(),
                  "powiedz do " + this_player()->query_name(PL_DOP) +
                  " Prosz�, usi�d�.");
        command("wskaz na fotel");
	return 0;
     }

    return 1;
}

int
hook_inne_sprawdzenia_przed_tatuazem()
{
    return hook_inne_sprawdzenia_przed_fryzura();	
}	
	
void
hook_tworzenie_fryzury()
{
    if(this_player()->query_gender())
    
      set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " No, niech si� pani przejrzy, bardzo dobrze "+
		   "pani w tym uczesaniu. Cud-mi�d!");
    else
    {
      set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " No, w tym uczesaniu wygl�da pan naprawd� bardzo, "+
		   "bardzo poci�gaj�co.");
      this_player()->catch_msg("Fryzjer spogl�da na ciebie z "+
                   "uznaniem, a jego oczy przybieraj� dziwny, "+
		   "rozmarzony wyraz.\n");
      say("Fryzjer spogl�da na "+this_player()->short(PL_DOP)+
                   " z uznaniem, a jego oczy przybieraj� dziwny, "+
		   "rozmarzony wyraz.\n");
    }
}
/*   //To juz nie potrzebne, takiej opcji nie ma. L
void
hook_usuwanie_fryzury()
{
    write("Usuwasz fryzur�.\n");
    saybb(QCIMIE(this_player(),PL_MIA) + " usuwa swoj� fryzur�.\n");
    set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do " + this_player()->query_name(PL_DOP) +
                   " Ju�! No to jak� teraz fryzur� sobie za�yczysz "+
		   this_player()->koncowka("mi�y panie","mi�a damo")+
		   "?"); 
}*/

void
return_introduce(string imie)
{
    object osoba;
    osoba = present(imie, environment());

    if (osoba->query_gender())
    {
        command("przedstaw sie " + osoba->query_nazwa(PL_CEL));
	return;
    }
    else
    {
	command("przedstaw sie " + osoba->query_nazwa(PL_CEL));
        switch(random(2))
	{
	case 0: command("powitaj " + osoba->query_nazwa(PL_DOP));break;
        case 1: command("usmiechnij sie milo do "+
	         osoba->query_nazwa(PL_DOP));break;
        }
              return;
    }
}

string
default_answer()
{
     switch(random(6))
     {
     case 0: set_alarm(0.5, 0.0, "command_present", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP) +
                  " Ja nic nie wiem!");
	     break;
     case 1: set_alarm(0.5, 0.0, "command_present", this_player(),
             "rozloz rece");
             break;
     case 2: set_alarm(0.5, 0.0, "command_present", this_player(),
             "pokiwaj niepewnie");
             break;
     case 3: set_alarm(0.5, 0.0, "command_present", this_player(),
             "popatrz przelotnie na "+this_player()->query_imie(PL_DOP)); 
             break;
     case 4: set_alarm(0.5, 0.0, "command_present", this_player(),
             "wzrusz ramionami");
             break;
     case 5: set_alarm(0.5, 0.0, "command_present", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP) +
                  " Ah, nie przeszkadzaj mi ju�, prosz�.");
             break;
     }
     
     return "";
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(0.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(0.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(0.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
	case "pocaluj": 
	      if(this_player()->query_gender())
	        {
		set_alarm(0.5, 0.0, "command_present", this_player(),
                "powiedz do "+ this_player()->query_name(PL_DOP) +
                  " Co te� pani...To nie przystoi.");
                    break;
		}
	      else
	        {
		set_alarm(0.5, 0.0, "command_present", this_player(),
                "poglaszcz "+ this_player()->query_name(PL_DOP) +
                  " po policzku");
                    break;
		}
	case "przytul": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
	case "prztyknij": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "poglaszcz": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object kto)
{
    if(kto->query_gender())
        command("emote m�wi z "+
	          "roztargnieniem: Tak, tak, dzi�kuj�... tak. No. Mi�o.");
    else
        command("emote m�wi z "+
	     "u�miechem: Ale� dzi�kuj�, bardzo dzi�kuj�. Gdyby "+
	     "pan kiedy� chcia�, tak wpa��... na winko. Mam "+
	     "bardzo dobre wino. Naprawd�. Bardzo dobre.");
}

void
nienajlepszy(object kto)
{
   switch(random(6))
   {
      case 0: set_alarm(0.5, 0.0, "command_present", kto,
             "krzyknij Bij�! Ratunku!");
	      break;
      case 1: set_alarm(0.5, 0.0, "command_present", kto,
              "krzyknij Bij�! Na pomoc!");
	      break;
      case 2: command("emote m�wi piskliwym g�osem: Ale� to "+
              "ordynarne! Ordynarne i niemi�e.");
	      break;
      case 3: command("emote m�wi a g�os mu si� za�amuje: "+
                      "Prosz� przesta�, doprawdy.");
	      break;
      case 4: command("emote m�wi piskliwym g�osem: Ale� to "+
              "ordynarne! Ordynarne i niemi�e.");
	      break;
      case 5: command("emote m�wi piskliwym g�osem: Ale� to "+
              "ordynarne! Ordynarne i niemi�e.");
	      break;
   }
} 
 
void
attacked_by(object wrog)
{
    if(walka==0) 
    {
    //command("emote wyci�ga spod kontuaru ci�k� pa�k� i dobywa jej.");
    command("krzyknij Pomocy!");
    
    if (!query_attack())
	set_alarm(1.0, 0.0, "pomoc_nadchodzi", 1);
     
    set_alarm(5.0, 0.0, "czy_walka", 1);
    walka = 1;
    return ::attacked_by(wrog); 
    }

    else if(walka == 1)
    {
      set_alarm(1.0, 0.0, "command", "krzyknij Pomocy!");
      set_alarm(5.0, 0.0, "czy_walka", 1);
      walka = 1;
      return ::attacked_by(wrog);
    }

}

int
czy_walka()
{
    if(!query_attack())
    {

       this_object()->command("powiedz Ojej!");

       walka = 0;
       return 1;
    }
       else
       {
       set_alarm(5.0, 0.0, "czy_walka", 1);
       }
}
int
pomoc_nadchodzi()
{
    //if(query_attack())
    //{
    tell_room(environment(this_object()), 
          "Zza ukrytych drzwi po prawej stronie "+
          "wyskakuje trzech krasnolud�w. Nie "+
	  "wygl�daj� na zbyt zadowolonych, za to mi�nie "+
	  "na ich ramionach graj� niebezpiecznie, a "+
	  "miecze uniesione w r�kach sugeruj�, �e zaraz "+
	  "rzuc� si� do walki.\n");
     clone_object(RINDE+"npc/krasnolud_fryzjera1.c")->move(environment(this_object()));
     clone_object(RINDE+"npc/krasnolud_fryzjera2.c")->move(environment(this_object()));
     clone_object(RINDE+"npc/krasnolud_fryzjera3.c")->move(environment(this_object()));
     
  
    return 1;
    //}
}

public void
do_die(object killer)
{
        command("westchnij cicho");
        ::do_die(killer);
}
/*
void
ma_moj_przedmiot(object kto)
{
   set_alarm(2.0, 0.0, "command", "powiedz do "+OB_NAME(kto)+" Oddawaj m�j stolik,
z�odzieju ch�do�ony!");
}

void
wychodzi_z_moim_przedmiotem(object kto)
{
   set_alarm(2.0, 0.0, "command", "powiedz do "+OB_NAME(kto)+" Kurwaaaa!");
}

int
signal_leave(object kto, object dokad)
{
    if (dokad->query_pub())
        return 0;

    if(sizeof(filter(deep_inventory(kto), &->query_naczynie())) > 0 )
    {
//      kto->command("poloz naczynia na stole");
        tell_object(kto, "Oddajesz wszystkie naczynia karczmarzowi.\n");
        filter(deep_inventory(kto), &->query_naczynie())->remove_object();
        return 0;
    }
}
*/
