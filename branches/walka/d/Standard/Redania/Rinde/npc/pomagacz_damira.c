/*
 * Taki tam sobie pomagacz dla Damira z karczmy Baszta w Rinde.
 * Sprz�ta nam on puste naczynia z sali i zza kotary.
 *          Vera
 */
#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <ss_types.h>
#include "dir.h"
inherit RINDE+"std/npc.c";
/*object obj;
void misa(int faza);
void sedno_sprawy(int faza);
void woda(int faza);
int obsluga();
int s;
int a;*/


void
create_rinde_npc()
{
    ustaw_odmiane_rasy(PL_CZLOWIEK);
    set_gender(G_MALE);
    dodaj_przym("zapracowany","zapracowani");
    dodaj_przym("kr�py","kr�pi");
    ustaw_imie (({"adam", "adama", "adamowi", "adama", "adamem", "adamie"}), PL_MESKI_OS);

    set_long("Skromny, ubrany w jakie� �achmany m�czyzna wygl�da "+
              "na ubogiego s�ug� jakiego� panicza.\n");
    add_armour("/d/Standard/items/ubrania/fartuchy/losowy_rzemieslniczy.c");
    set_stats ( ({ 45, 31, 40, 21, 30, 58 }) );
    set_skill(SS_DEFENCE, 30 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 45 + random(5));
//    add_ask(({"wode", "mise", "miske"}), VBFC_ME("odpowiedz"));
    set_alarm(50.0,0.0,"robimy_akcje");
    //set_alarm(100.0,0.0,"czyscimy_naczynka_al");
}

void
robimy_akcje_al()
{
    switch(random(5))
    {
        case 0: set_alarm(150.0,0.0,"robimy_akcje"); break;
        case 1: set_alarm(1225.0,0.0,"robimy_akcje"); break;
        case 2: set_alarm(1132.0,0.0,"robimy_akcje"); break;
        case 3: set_alarm(1600.0,0.0,"robimy_akcje"); break;
        case 4: set_alarm(1318.0,0.0,"robimy_akcje"); break;
    }
}

void
next4()
{
    command("n");
    int i;
    object *wszystko=all_inventory(this_object());   
    for(i=0;i<sizeof(wszystko);i++)
    {
        if(wszystko[i]->query_naczynie())
            wszystko[i]->remove_object();
    }
    set_alarm(20.0,0.0,"robimy_akcje_al");   
}
void
next3()
{
    command("odsun kotare");
    command("kotara");
    command("zasun kotare");
    set_alarm(1.0,0.0,"next4");
}
void
czyscimy_naczynka_kotara()
{
    command("wez puste naczynia");
    command("zdejmij puste naczynia ze stolu");
    command("zdejmij puste naczynia z drugiego stolu");
    set_alarm(2.0,0.0,"next3");
}
void
next2()
{
    command("kotara");
    command("dygnij");
    set_alarm(3.0,0.0,"czyscimy_naczynka_kotara");
}
void
next()
{
    command("odsun kotare");
    set_alarm(2.0,0.0,"next2");
}
void
czyscimy_naczynka()
{
    command("wez puste naczynia");
    command("zdejmij puste naczynia ze stolu");
    command("zdejmij puste naczynia z drugiego stolu");
    set_alarm(2.0,0.0,"next");
}
void robimy_akcje()
{
    command("s");
    set_alarm(3.0,0.0,"czyscimy_naczynka");
}

int
query_pomagacz()
{
    return 1;
}

void attacked_by(object wrog)
{
    command("odsun kotare");
    command("kotara");
    command("n");
    //jakby byl w sali glownej to jesio raz spierdalamy ;)
    command("odsun kotare");
    command("kotara");
    command("n");
    return ::attacked_by(wrog);
}


/*int
obsluga()
{
    if (s == 0)
    {
        s = 1;
        obj = environment();

        if(file_name(obj) != "/d/Redania/gregh/wioska/chata3")
        {

            if(file_name(obj) == "/d/Redania/gregh/wioska/stodola")
            {
                set_alarm(5.0,0.0,"misa",2);
                return 1;
            }

        set_alarm(20.0,0.0,"misa",0);
        return 1;
        }



        set_alarm(15.0,0.0,"misa",1);
        return 1;
    }
}



void
misa(int faza)
{
    switch(faza)
    {
    case 0:
    if(query_attack()){s = 0;break;}
    command("emote wychodzi, mamroczac cos pod nosem nerwowo.");
    move("/d/Redania/gregh/wioska/chata3.c");
    command("emote przybywa, klnac cos pod nosem.");
    a = set_alarm(5.0,0.0,"misa",1);
    break;

    case 1:
    if(query_attack()){s = 0;break;}
    command("se");
    a = set_alarm(2.0,0.0,"misa",2);
    break;

    case 2:
    if(query_attack()){s = 0;break;}
    command("powiedz Czego???");
    a = set_alarm(15.0,0.0,"misa",3);
    break;

    case 3:
    if(query_attack()){s = 0;break;}
    command("tupnij");
    a = set_alarm(10.0,0.0,"misa",4);
    break;

    case 4:
    if(query_attack()){s = 0;break;}
    command("emote mruczy cos pod nosem.");
    a = set_alarm(2.0,0.0,"misa",5);
    break;

    case 5:
    if(query_attack()){s = 0;break;}
    command("nw");
    s = 0;
    break;

   }
}


string
odpowiedz()
{
    remove_alarm(a);
    sedno_sprawy(0);

}

void
sedno_sprawy(int faza)
{

    switch(faza)
    {
    case 0:
    if(query_attack()){s = 0;break;}
    
    if(file_name(environment()) != "/d/Redania/gregh/wioska/stodola")
    {
    woda(0);
    break;
    }
    if (s != 2)
    {
        s = 2;
        set_alarm(2.0,0.0,"sedno_sprawy",1);
        break;
    }
    break;

    case 1:
    if(query_attack()){s = 0;break;}
    command("splun");
    set_alarm(2.0,0.0,"sedno_sprawy",2);
    break;

    case 2:
    if(query_attack()){s = 0;break;}
    command("powiedz A gdzie ta miska co tu miala byc???");
    set_alarm(3.0,0.0,"sedno_sprawy",3);
    break;

    case 3:
    if(query_attack()){s = 0;break;}
    command("nw");
    s = 0;
    break;
    }


}

void
woda(int faza)
{

    switch(faza)
    {
    case 0:
    if (s != 2)
    {
        s = 2;
        set_alarm(2.0,0.0,"woda",1);
        break;
    }
    break;

    case 1:
    if(query_attack()){s = 0;break;}
    command("powiedz Taka mam robote, ze wode roznosze po pokojach...");
    s = 0;
    break;
    }


}*/
