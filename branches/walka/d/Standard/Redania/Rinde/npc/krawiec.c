/* Autor: Avard
   Opis : Eria
   Data : 30.03.07 */

#pragma strict types

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <pattern.h>
#include "dir.h"

inherit RINDE+"std/npc.c";
inherit "/lib/craftsman.c";

#define WZORY (ZACHOD + "Krawiec/wzory/")
#define REP_MAT "/d/Standard/items/materialy/"
int czy_walka();
int walka = 0;

void
create_rinde_npc()
{
    set_living_name("ernest");
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    dodaj_nazwy("krawiec");
    set_gender(G_MALE);
    set_title(", Krawiec");
    ustaw_imie(({"ernest","ernesta","ernestowi","ernesta","ernestem",
        "erne^scie"}),PL_MESKI_OS);
    set_long("Spod prymitywnych okular^ow, kt^ore tylko na pierwszy rzut "+
        "oka wyglad^aja na porz^adne, co chwile spoglad^aja na ciebie "+
        "szare, ma^le oczka, ^sledz^ace ka^zdy tw^oj najmniejszy nawet "+
        "ruch. Mimo tego, ^ze ten nieski m^e^zczyzna wydaje si^e "+
        "nieustannie wodzi^c za tob^a wzrokiem, przez ca^ly czas zr^ecznie "+
        "przebiera d^lo^nmi, w kt^orych co chwile znajduje si^e inny "+
        "przedmiot. W jego ustach pob^lyskuje srebrna igie^lka, z kt^or^a "+
        "najwyra^xniej nigdy si^e nie rozstaje, a w jego prostym ubraniu, "+
        "kt^ore ma na sobie, widnieje jeszcze kilka innych, podobnych "+
        "igie^l. Mimo, i^z m^e^zczyzna ma ju^z swoje lata, to nie sprawia "+
        "mu wi^ekszej trudno^sci stworzenie jakiegokolwiek zam^owienia, "+
        "nawet od najbogatszych klient^ow. Wida^c fach pozostaje w r^ekach "+
        "przez d^lugie lata.\n");
    dodaj_przym("zgarbiony","zgarbieni");
    dodaj_przym("niski","niscy");

    set_stats (({ 50, 50, 50, 40, 30, 70 }));
    set_skill(SS_DEFENCE, 50 + random(4));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 55 + random(15));
    add_armour(RINDE_UBRANIA + "spodnie/ciemne_powycierane_Mc.c");
    add_armour(RINDE_UBRANIA + "koszule/zwykla_polatana_Mc.c");
    add_armour(RINDE_UBRANIA + "buty/czarne_znoszone_Mc.c");
    set_act_time(5);
    add_act("@@krawiec_pracuje@@");
    add_act("mlasnij cicho");
    add_act("emote szybko poprawia rek^a w^losy.");
    add_act("emote bezwiednie bawi sie srebrn^a igie^lk^a.");
    
    add_prop(NPC_I_NO_RUN_AWAY, 1);
    add_prop(CONT_I_WEIGHT, 72000);
    add_prop(CONT_I_HEIGHT, 169);

    set_skill(SS_CRAFTING_TAILOR, 80);
    config_default_craftsman();
    craftsman_set_domain(PATTERN_DOMAIN_TAILOR);
    craftsman_set_sold_item_patterns( ({
                WZORY + "spodnie_szare_lniane.c",
                WZORY + "bluzka_czarna_wiazana.c",
                WZORY + "kamizelka_czarna_skorzana.c",
                WZORY + "koszula_biala_lniana.c",
                WZORY + "spodnica_czarna_zwiewna.c",
                WZORY + "spodnica_dluga_przezroczysta.c",
                WZORY + "spodnie_zwykle_plocienne.c",
                WZORY + "sukienka_krotka_bordowa.c",
                WZORY + "buty_wiazane_skorzane.c",
                WZORY + "buty_eleganckie_zamszowe",
                WZORY + "peleryna_czarna_aksamitna",
                WZORY + "spodnie_gustowne_czarne",
                WZORY + "spodnie_satynowe_wisniowe",
                WZORY + "suknia_zielonkawa_elegancka",
                WZORY + "buty_wygodne_czarne.c",
                WZORY + "spodnie_waskie_czarne.c",
                WZORY + "buty_dlugie_czarne.c",
                WZORY + "buty_jasne_damskie.c",
                WZORY + "buty_skorzane_na_obcasie.c",
                WZORY + "buty_wysokie_eleganckie.c",
                WZORY + "buty_zadbane_zamszowe.c",
                WZORY + "cizemki_miekkie_skorzane.c",
                WZORY + "kaftan_czarny_wyszywany.c",
                WZORY + "kapelusz_czerwony_elegancki.c",
                WZORY + "koszula_biala_jedwabna.c",
                WZORY + "onuce_czyste_biale.c",
                WZORY + "pas_szeroki_ciemnobezowy.c",
                WZORY + "spodnica_pomarszczona_dluga.c",
                WZORY + "spodnie_czarne_skorzane.c"			
                }) );

    set_store_room(ZACHOD + "Krawiec/lokacje/magazyn.c");

	set_money_greed_buy(109);
	set_money_greed_sell(116);
	set_money_greed_change(126);

    add_ask(({"warsztat","pracownie"}), VBFC_ME("pyt_o_warsztat"));
    add_ask(({"ubranie","ubrania","odzienie"}), VBFC_ME("pyt_o_ubrania"));
    set_default_answer(VBFC_ME("default_answer"));
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_warsztat()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: A tak, tak. Jeste^s w^la^snie w moim warsztacie "+
            "krawieckim, w kt^orym jak nie trudno zgadn^a^c, powstaj^a nowe "+
            "ubrania.");
        set_alarm(1.5,0.0,"powiedz_gdy_jest",this_player(),
            "emote m^owi ko^ncz^ac swoj^a my^sl: Niekoniecznie dla takich "+
            "jak ty.");
        set_alarm(3.0,0.0,"powiedz_gdy_jest",this_player(),
            "emote m^owi po chwili: Ale jak chcesz to oczywi^scie mo^zesz "+
            "przejrze^c moje towary. Pieni^adz to pieni^adz. Niewa^zne od "+
            "kogo, wa^zne, ^ze jest!");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }    
    return "";
}
string
pyt_o_ubrania()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote m^owi: Chcesz jakie^s zam^owi^c?");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }    
    return "";
}
string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "rozloz rece bezradnie");
    return "";
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {
        switch(random(3))
        {
            case 0: set_alarm(1.0, 0.0, "command", "krzyknij Nie, nie! "+
                "Zniszczysz moje materia^ly!");break;
            case 1: set_alarm(1.0, 0.0, "command", "krzyknij Zaplamisz "+
                "krwi^a moje najdro^zsze kreacje!");break;
            case 2: set_alarm(1.0, 0.0, "command", "warknij z rozpacza");break;
        }
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);  
    }
    else
    {
        switch(random(3))
        {
            case 0: set_alarm(1.0, 0.0, "command", "krzyknij Nie, nie! "+
                "Zniszczysz moje materia^ly!");break;
            case 1: set_alarm(1.0, 0.0, "command", "krzyknij Zaplamisz "+
                "krwi^a moje najdro^zsze kreacje!");break;
            case 2: set_alarm(1.0, 0.0, "command", "warknij z rozpacza");
                break;
        }
    walka = 1;
    return ::attacked_by(wrog);  
    }
}
int
czy_walka()
{
    if(!query_attack())
    {

       this_object()->command("sapnij");

       walka = 0;
       return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}

void
init()
{                                                                          
    ::init();
    craftsman_init();
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_craftsman_auto_load();
}

public string
init_arg(string arg)
{   
    return init_craftsman_arg(::init_arg(arg));
}

string
krawiec_pracuje()
{
    if (craftsman_is_busy()) {
        switch (random(5)) {
            case 0: return "emote przebiera mi^edzy materia^lami.";
            case 1: return "emote szyje co^s zawzi^ecie.";
            case 2: return "emote wycina co^s zr^ecznie.";
            case 3: return "emote wyci^aga z szuflady jakie^s przybory do "+
                "szycia.";
            case 4: return "emote ociera kropelki potu z czo^la.";
        }
    }
    return "";
}
