
/* Autor: Avard
   Opis : Faeve
   Data : 7.04.07 */

inherit "/std/creature";
inherit "/std/combat/unarmed";
inherit "/std/act/attack";
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

void random_przym(string str, int n);

void
create_creature()
{
    ustaw_odmiane_rasy("ghoul");
    set_gender(G_MALE);
    random_przym("oszpecony:oszpeceni brudny:brudni "+
        "^smierdz^acy:^smierdz^acy zaniedbany:zaniedbani "+
        "zakrwawiony:zakrwawieni || "+
        "agresywny:agresywni nerwowy:nerwowi wysoki:wysocy "+
        "chudy:chudzi wychudzony:wychudzeni",2);
     
    set_long("Kreatura kt^orej si^e przygl^adasz wygl^ada na humanoida, "+
        "gdy^z budowa cia^la przypomina starca z powykrecanymi ko^nczynami. "+
        "Jego d^lugie r^ece zaopatrzone s^a w szerokie pazury s^lu^z^ace do "+
        "rozgrzebywania mogi^l i ^swie^zej ziemi. Dziwna substancja kt^or^a "+
        "ociekaja pazury potwora budzi w tobie obawy. Ostre jak brzytwa "+
        "z^eby i d^lugi j^ezyk pozwalaj^a mia^zd^zy^c ko^sci, a nast^epnie "+
        "wysysa^c z nich szpik kostny. Stw^or ten pojawia si^e na "+
        "powierzchni tylko noc^a, w dzie^n za^s przesiaduje w mrocznych "+
        "miejscach by ukry^c si^e przed ra^z^acym go s^lo^ncem. Zwykle nie "+
        "poluje, zadowalajac si^e tylko padlin^a b^ad^x cia^lami "+
        "pozostawionymi po bitwie. Lecz gdy zobaczy s^labego przeciwnika "+
        "lub gdy zostanie zaskoczony b^edzie walczy^l z pe^ln^a "+
        "zaci^eto^sci^a staj^ac si^e naprawde gro^xnym przeciwnikiem.\n");
    set_stats (({70,60,140,40,40,100}));
    add_prop(CONT_I_WEIGHT, 60000);
    add_prop(CONT_I_HEIGHT, 185);
    add_prop(LIVE_I_NEVERKNOWN, 1);
    set_aggressive(1);
    set_attack_chance(100);
    set_npc_react(1); 

   /* set_cact_time(10);
    add_cact("jeknij");
    add_cact("emote spogl^ada na ciebie wrogo.");*/

    set_attack_unarmed(0, 25, 30, W_SLASH,  50, "pazurami prawej d�oni");
    set_attack_unarmed(1, 25, 30, W_SLASH,  50, "pazurami lewej d�oni");
    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "g^low^e");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "lew^a r�ke");
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "praw^a r�ke");
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 20, "nogi");

    add_prop(LIVE_I_SEE_DARK, 1);
}

void random_przym(string str, int n)
{
        mixed* groups = ({ });
        groups = explode(str, "||");
        int groups_size = sizeof(groups);
        if(n > groups_size)
                return;
        for(int i =0; i < groups_size; i++)
                groups[i] = explode(groups[i], " ");
        for(int a = 0; a < n; a++)
        {
                int group_index = random(sizeof(groups));
                int adj_index = random(sizeof(groups[group_index]));
                string* tmp = explode(groups[group_index][adj_index], ":");
                //Jak si� przypadkiem wylosowa�o co� pustego, to zrobimy losowanie jeszcze raz.
                //i tak do evala;P
                if(sizeof(tmp) != 2)
                {
                    a--;
                    continue;
                }
                dodaj_przym(tmp[0], tmp[1]);
                groups = groups-({groups[group_index]});
        }
}
