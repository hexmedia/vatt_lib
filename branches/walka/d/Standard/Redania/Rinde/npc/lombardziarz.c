#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include "dir.h"
inherit RINDE+"std/npc.c";

void
create_rinde_npc()
{
    set_living_name("ferel");

    ustaw_odmiane_rasy("m�czyzna");
    set_gender(G_MALE);

    set_surname("Holentoler");
    set_origin("z Rinde");

    ustaw_imie(({"ferel", "ferela", "ferelowi", "ferela", "ferelem",
	"ferelu"}), PL_MESKI_OS);

    set_long("Cz�owiek rozgl�da si� znudzonym wzrokiem po pomieszczeniu, " +
	"co chwila przygl�daj�c ci si� k�tem oka, zapewne my�l�c, �e " +
	"nie dostrze�esz jego spojrzenia. Przechadza si� po pomieszczeniu " +
	"spokojnym krokiem, co jaki� czas przecieraj�c swoj� szmateczk� " +
	"jakie� szparga�y walaj�ce si� na p�kach, poprawiajac przy okazji " +
	"ich u�o�enie. Jego siwe w�osy oraz liczne zmarszczki pokrywaj�ce " +
	"twarz �wiadcz� o jego s�dziwym wieku, a przygarbione plecy wraz " +
	"z wiecznie wysuni�tymi do przodu ramionami, mog� nasun�� my�l, " +
	"�e tyle lat co prze�y� - przepracowa�. Jego m�tne oczy leniwie " +
	"przypatruj� si� ka�dej rzeczy, jakby pierwszy raz j� widzia�, " +
	"mimo �e sp�dza tu prawie ca�y czas i bez w�tpienia jest obeznany " +
	"ze wszystkim w tym pomieszczeniu.\n");

    dodaj_przym("przygarbiony", "przygarbieni");
    dodaj_przym("zgorzknia�y", "zgorzkniali");

    set_act_time(30);
    add_act("emote przechadza si� po pomieszczeniu.");
    add_act("emote przeciera szmatk� jaki� przedmiot.");
    add_act("emote poprawia u�o�enie rupieci na jednej z p�ek.");
    add_act("emote domyka jedn� z szafek.");
    add_act("emote wzdycha bezg�o�nie.");
    add_act("emote spogl�da na ciebie k�tem oka, licz�c �e nie dostrze�esz " +
	"jego spojrzenia.");
    add_act("emote ociera szmatk� pot z czo�a, ale czy to nie ta sama " +
	"szmatka kt�r� wyciera� przed chwil� kurz..?");

    add_ask(({"pomoc", "prac�", "zadanie"}), VBFC_ME("pyt_o_pomoc"));
    set_default_answer(VBFC_ME("default_answer"));

    set_stats ( ({ 65, 40, 70, 21, 30, 58 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    add_armour(RINDE_UBRANIA + "fraki/stary_nieatrakcyjny_Mc.c");
    add_armour(RINDE_UBRANIA + "spodnie/wytarte_czarne_Mc.c");
    add_armour(RINDE_UBRANIA + "buty/stare_skorzane_Mc.c");
    
    add_prop(CONT_I_WEIGHT, 65000);
    add_prop(CONT_I_HEIGHT, 170);
}

string
pyt_o_pomoc()
{
    set_alarm(2.0, 0.0, "command_present", this_player(),
	"powiedz do " + this_player()->query_name(PL_DOP) + " Bol� mnie plecy.");
    return "";
}

string
default_answer()
{
     set_alarm(2.0, 0.0, "command_present", this_player(),
             "powiedz do " + OB_NAME(this_player()) + " Nie przeszkadzaj mi.");
     return "";
}

void
return_introduce(string imie)
{
    object osoba = present(imie, environment());

    if (osoba)
    {
        command("przedstaw sie " + OB_NAME(osoba));
    }
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
	case "nadepnij":
	    if (random(2))
		set_alarm(1.5, 0.0, "command_present", wykonujacy,
		    "emote charczy starczo.");
	    else
		set_alarm(1.5, 0.0, "command_present", wykonujacy,
		    "popatrz z irytacj� na " + OB_NAME(wykonujacy));
            break;

        case "za�miej":
        case "poca�uj":
        case "przytul":
        case "pog�aszcz":
	    if (random(2))
		set_alarm(1.5, 0.0, "command_present", wykonujacy,
		    "emote chrz�ka niepewnie.");
	    else
		set_alarm(1.5, 0.0, "command_present", wykonujacy,
		    "emote odwraca wzrok, a na jego policzkach wida� " +
		    "szkar�atny rumieniec.");
            break;
    }
}

void
attacked_by(object wrog)
{
    command("emote krzyczy starczym g�osem: Pooooomocyyy! Bij�!");

    ::attacked_by(wrog);
}
