
/* Dzieciak z Rinde        *
 * Na podst. dzieciakow    *
 * Avarda - Gjoef.         *
 * Opis by Tinardan        */

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <filter_funs.h>
#include "dir.h"
inherit RINDE+"std/npc.c";

int czy_walka();
int walka = 0;

void
pocaluj(object kto);

string
prosba_o_jedzenie();

void
create_rinde_npc()
{
    set_living_name("dzieciak4");
    ustaw_odmiane_rasy("chlopiec");
    set_gender(G_MALE);
    dodaj_nazwy("dziecko");
    dodaj_przym("jasnow^losy", "jasnow^losi");
    dodaj_przym("grubiutki", "grubiutcy");

    set_long("Przypomina nieco r^o^zow^a i piegowat^a pi^leczk^e z jasnymi " +
    "w^losami i kr^otkimi, ^smiesznymi r^aczkami i n^o^zkami. Ubrany jest " +
    "w ^xle dobrane, mieszcza^nskie stroje, z kieszeni wychyla si^e " +
    "paczuszka z kanapk^a, a w r^eku trzyma dorodne jab^lko. Jego zabawn^a, " +
    "pyzat^a twarz ozdabiaj^a ^ladne, szare oczy. Zza nigdy nie domkni^etych " +
    "warg wystaj^a spore, przypominaj^ace nieco kr^olicze, z^eby.\n");

    set_stats(({10, 10, 10, 10, 10, 10}));
    add_prop(CONT_I_WEIGHT, 50000);
    add_prop(CONT_I_HEIGHT, 150);
    add_prop(CONT_I_VOLUME, 50000);
    add_prop(LIVE_I_NEVERKNOWN, 1);

    set_act_time(13);
    add_act("@@prosba_o_jedzenie@@");
	add_act("emote ogl^ada uwa^znie jab^lko, po czym zaczyna je chrupa^c "+
        "ze smakiem.");
	add_act("emote spogl^ada weso^lo na wystaj^aca mu z kieszeni kanapk^e.");
	add_act("emote wpatruje si^e t^esknie w wystaw^e.");
	add_act("westchnij cicho");
        
    set_chat_time(20);
    add_chat("Zjad^lbym sobie co^s...");
	
    add_armour("/d/Standard/Redania/Rinde/przedmioty/ubrania/" +
        "buty_dla_dzieciaka4.c");
    add_armour("/d/Standard/Redania/Rinde/przedmioty/ubrania/" +
        "koszulka_dla_dzieciaka4.c");
    add_armour("/d/Standard/Redania/Rinde/przedmioty/ubrania/" +
        "spodenki_dla_dzieciaka4.c");
    //add_object("/d/Standard/Redania/Rinde/przedmioty/kanapka.c");
    //add_object("/d/Standard/Redania/Rinde/przedmioty/jablko.c");

	set_default_answer(VBFC_ME("default_answer"));
	
	set_monster_home("/d/Standard/Redania/Rinde/wschod/ulica5.c");
	set_restrain_path("/d/Standard/Redania/Rinde/wschod/ulica5.c");
}
 
void
powiedz_gdy_jest(object player, string tekst)
{
	if (environment(this_object()) == environment(player))
	this_object()->command(tekst);
}

string
default_answer()
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
    "powiedz do "+ this_player()->query_name(PL_DOP) +
    " Ja nic nie wiem, ale ch^etnie bym co^s zjad^l.");
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}

void
return_introduce(object ob)
{
    command("powiedz :niepewnie: Dzie^n dobry.");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
            break;
            
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
            break;
            
		case "poca^luj": set_alarm(1.5, 0.0, "pocaluj", wykonujacy);
            break;

        case "przytul":
        case "pog^laszcz": set_alarm(2.5, 0.0, "command", "powiedz Fajniej " +
            "by^lo, gdybym dosta^l co^s do jedzenia.");
            break;
    }
}

void
nienajlepszy(object wykonujacy)
{
    command("emote krzyczy przera^zonym g^losem: Ale za co?!");
} 

void
attacked_by(object wrog)
{
    if(walka == 0)
    {
        this_object()->command("rozplacz sie");
        set_alarm(5.0, 0.0, "czy_walka", 1);
        walka = 1;
    
        return ::attacked_by(wrog);
    }

    else if(walka == 1)
    {
        set_alarm(1.0, 0.0, "run_away");
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;

        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {

        this_object()->command("emote rozgl^ada si^e ze ^lzami w oczach.");
        walka = 0;
        
        return 1;
    }
    else
        set_alarm(5.0, 0.0, "czy_walka", 1);
}

void
pocaluj(object kto)
{
    this_object()->command("emote nadstawia grzecznie policzek i pyta: Ma "+
        "mo^ze pan" + kto->koncowka("", "i") + " co^s do jedzenia?");
}

string
prosba_o_jedzenie()
{
    object *livingi = FILTER_LIVE(all_inventory(environment(this_object())));
    livingi = filter(livingi, query_humanoid);
    livingi -= ({ this_object() });

    if (!sizeof(livingi)) {
        return "";
    }
    
    object adresat = livingi[random(sizeof(livingi))];

    tell_room(environment(this_object()), QCIMIE(this_object(), PL_MIA) +
    " poci^aga " + QIMIE(adresat, PL_BIE) + " za r^ekaw, m^owi^ac: Ma pan" +
    adresat->koncowka("", "i") + " co^s do jedzenia?\n", ({ this_object(),
    adresat }) );
    adresat->catch_msg(QCIMIE(this_object(), PL_MIA) + " poci^aga ci^e za " +
    "r^ekaw, m^owi^ac: Ma pan" + adresat->koncowka("", "i") + " co^s do " +
    "jedzenia?\n");
    tell_object(this_object(), "Zwracasz si^e do " +
    adresat->query_name(PL_DOP) + " z pro^sb^a o jedzenie.\n");

    return "";
}

void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);
    
    if(present(from, environment()) == 0)
	return;
    set_alarm(1.0, 0.0, "command", "powiedz do " + from->query_name(PL_DOP) +
    " O, fajnie. Dzi^eki.");
    set_alarm(5.0, 0.0, "command", "zjedz " + ob->short(PL_BIE));
}
