inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <object_types.h>
#include <composite.h>
#include <filter_funs.h>

#include "dir.h"

void
create_zwierze()
{
    add_leftover("/std/leftover", "sk�ra", 1, 0, 1, 1, 5 + random(5), O_SKORY);

    ustaw_odmiane_rasy("kot");
    set_gender(G_MALE);

    set_long("Niezwykle chudy kot, na kt�rego bokach wystaj� �ebra, nie " +
	"wygl�da na zwierz�, kt�re posiada jakiegokolwiek pana. Zab��kany, " +
	"rozgl�da si� w poszukiwaniu resztek jedzenia i miauczy g�o�no " +
	"wyra�aj�c w ten spos�b swoje niezadowolenie. Jego cienka i " +
	"szorstka sier�� jest pr��kowana i ci�ko rozr�nic jej kolor. " +
	"W oczy rzucaj� si� tylko czarne i br�zowe paski, odznaczaj�ce si� " +
	"na zakurzonym futerku.\n");

    dodaj_przym("zaniedbany", "zaniedbani");
    dodaj_przym("pr��kowany", "pr��kowani");

    set_act_time(30);
    add_act("emote mruczy cicho.");
    add_act("emote rozgl�da si� w poszukiwaniu jedzenia.");
    add_act("emote �asi si� o twoj� nog�.");
    add_act("emote miauczy g�o�no.");
    add_act("emote li�e swoj� �apk�.");
    add_act("emote z zainteresowaniem �ledzi jaki� szybko poruszaj�cy " +
	"si� punkt.");

    set_stats ( ({ 65, 31, 70, 21, 30, 58 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    set_attack_unarmed(0,   5,  5 , W_SLASH, 100, "pazurkami");
   
    set_hitloc_unarmed(0, ({ 0, 0, 5 }), 20, "g�ow�");
    set_hitloc_unarmed(1, ({ 0, 0, 5 }), 80, "brzuch");

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 1000);
    add_prop(CONT_I_HEIGHT, 32);
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
	case "nadepnij":
	    set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;

        case "szturchnij":
	    set_alarm(1.0, 0.0, "taki_sobie", wykonujacy);
            break;

        case "za�miej":
        case "poca�uj":
        case "przytul":
        case "pog�aszcz":
	    set_alarm(1.0, 0.0, "dobry", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
    command("emote syczy cicho.");
    command("emote obna�a gro�nie ostre pazury.");
}

void
taki_sobie(object kto)
{
}

void
dobry(object kto)
{
    command("emote mruczy z zadowoleniem.");
    command("emote �asi si� o ciebie.");
}

void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
}
