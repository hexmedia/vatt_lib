
/* Autor: Avard
   Opis : Tinardan
   Data : 10.09.06
   Info : Straznik swiatynny */

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"
 
inherit RINDE+"std/npc.c";

string *zle = ({ });
void
create_rinde_npc()
{
    set_living_name("brudaszeswiatyni");
    ustaw_odmiane_rasy("m^e^zczyzna");
    set_gender(G_MALE);
    dodaj_nazwy("straznik");
    dodaj_nazwy("brudas");
    dodaj_przym("wysoki","wysocy");
    dodaj_przym("brudny","brudni");
     
    set_long("Istniej^a ludzie, kt^orzy ju^z na pierwszy rzut oka "+
        "wzbudzaj^a u innych niech^e^c i odraz^e. Na swoje nieszcz^e^scie "+
        "ten m^e^zczyzna w^la^snie do takich os^ob nale^zy. Wysoki, "+
        "niezgrabny, o zbyt d^lugich r^ekach i nogach, wiecznie "+
        "przygarbiony, powinien budzi^c wsp^o^lczucie, ale na jego "+
        "twarzy wypisana jest buta i pogarda dla tych, kt^orzy nie "+
        "dost^apili ^laski zbrojnej opieki nad ^swi^atyni^a. Jego "+
        "broda jest spl^atana i ma niewyra^xny odcie^n jasnego blondu, a "+
        "oczy wielkie, rybie i m^etne. Na domiar z^lego cuchnie "+
        "niemi^losiernie, a jego zbroja wygl^ada, jakby od wiek^ow nie "+
        "widzia^la szczotki i szmatki, a za to cz^esto g^esto mia^la "+
        "spotkania z resztkami posi^lk^ow dumnego jegomo^scia.\n");
    set_stats (({ 70, 60, 60, 40, 25, 100 }));
    add_prop(CONT_I_WEIGHT, 60000);
    add_prop(CONT_I_HEIGHT, 170);
    add_prop(LIVE_I_NEVERKNOWN, 1);

    set_act_time(40);
    add_act("emote spluwa na posadzk^e, tak, ^ze jego plwocina o kilka cali "+
        "mija twoj^a nog^e.");
    add_act("emote poprawia bro^n.");
    add_act("emote charczy g^lo^sno.");
    add_act("emote wyjmuje brudny patyczek z kieszeni i zaczyna sobie nim "+
        "czy^sci^c z^eby.");
    
    set_cact_time(10);
    add_cact("emote m^owi: Jatka!");
    add_cact("emote m^owi: Haaa!");

    add_armour(RINDE_ZBROJE + "kaftany/stary_cwiekowany_Lc.c");
    add_armour(RINDE_UBRANIA + "spodnie/stare_skorzane_Lc.c");
    add_armour(RINDE_UBRANIA + "pasy/przetarty_skorzany_Mc.c");
    add_object(RINDE_BRONIE + "palki/mocna_drewniana.c");

    add_ask(({"ratusz"}), VBFC_ME("pyt_o_ratusz"));
    add_ask(({"^swi^atynie"}), VBFC_ME("pyt_o_swiatynie"));
    add_ask(({"stra^znik^ow","stra^z"}), VBFC_ME("pyt_o_straz"));
    add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
    add_ask(({"burmistrza","Nevilla","nevilla"}),VBFC_ME("pyt_o_burmistrza"));
    add_ask(({"kreppa","kap^lana","Kreppa"}),VBFC_ME("pyt_o_kreppa"));
    set_default_answer(VBFC_ME("default_answer"));

    set_random_move(200);
    set_monster_home(SWIATYNIA + "lokacje/przedsionek");
    set_restrain_path( ({ SWIATYNIA + "lokacje/przedsionek", 
	    SWIATYNIA + "lokacje/nawa_glowna",
        SWIATYNIA + "lokacje/nawa_boczna_e",
        SWIATYNIA + "lokacje/nawa_boczna_w"}) );
}
void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
pyt_o_ratusz()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "emote m^owi: Jest w centrum.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"emote m^owi: Jak ju^z si^e "+
            "z tob^a policze to ta wiedza nie b^edzie ci potrzebna!");
    }
    return "";
}
string
pyt_o_swiatynie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "emote m^owi: To tutaj.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"emote m^owi: Jak ju^z si^e "+
            "z tob^a policze to ta wiedza nie b^edzie ci potrzebna!");
    }
    return "";
}
string
pyt_o_straz()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "emote m^owi: Patroluj^a miasto, albo upijaj^a si^e w "+
            "garnizonie.");
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
            "wzrusz ramionami");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"emote m^owi: Jak ju^z si^e "+
            "z tob^a policze to ta wiedza nie b^edzie ci potrzebna!");
    }
    return "";
}
string
pyt_o_garnizon()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "emote m^owi: Jest we wschodniej cz^e^sci miasta.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"emote m^owi: Jak ju^z si^e "+
            "z tob^a policze to ta wiedza nie b^edzie ci potrzebna!");
    }
    return "";
}
string
pyt_o_burmistrza()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "emote m^owi: Poszukaj go w ratuszu.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"emote m^owi: Jak ju^z si^e "+
            "z tob^a policze to ta wiedza nie b^edzie ci potrzebna!");
    }
    return "";
}
string
default_answer()
{
    if(!query_attack())
    {
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_BIE) +
            " Nic mi o tym nie wiadomo.");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"emote m^owi: Jak ju^z si^e "+
            "z tob^a policze to ta wiedza nie b^edzie ci potrzebna!");
    }
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("pokiwaj lekko");
}


void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "nadepnij" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
        {
            if(TP->query_gender() == 0)
            {
                set_alarm(0.5,0.0, "nienajlepszy", wykonujacy); break;
            }
            else
            {
                set_alarm(0.5,0.0,"command","powiedz O, dupka "+
                    "przysz^la ch^etna."); 
                set_alarm(1.5,0.0,"command",
                    "usmiechnij sie oblesnie"); break;
            }                
        }
        case "prychnij":
        {
            set_alarm(1.0,0.0, "command", "spojrzyj lekcewazaco na "+ 
                OB_NAME(wykonujacy)); break;        
        }
        case "przytul": set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                   break;
    }
}
void
nienajlepszy(object wykonujacy)
{
    if(member_array(wykonujacy->query_real_name(), zle) != -1)
    {
        command("zabij "+OB_NAME(wykonujacy));
    }
    else
    {
        switch(random(2))
        {
            case 0: command("emote ods^lania z^eby w niemal zwierz^ecym "+
                "grymasie i patrzy na ciebie gro^xnie.");
            zle += ({wykonujacy->query_real_name()});break;
            case 1: command("emote m^owi cicho: Zr^ob to jeszcze raz, a "+
                "po^za^lujesz.");
            zle += ({wykonujacy->query_real_name()});break;
        }
    }
} 
