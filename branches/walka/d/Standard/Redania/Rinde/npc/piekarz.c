#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include "dir.h"
inherit RINDE+"std/npc.c";
void
create_rinde_npc()
{
    set_living_name("bert");

    ustaw_odmiane_rasy("m�czyzna");
    set_gender(G_MALE);

    set_title(", Piekarz");

    dodaj_nazwy("piekarz");

    ustaw_imie(({"bert", "berta", "bertowi", "berta", "bertem", "bercie"}),
	PL_MESKI_OS);

    set_long("Na g�owie piekarza tkwi zabawny beret, b�d�cy symbolem jego " +
	"profesji. R�ce ma po �okcie ubielone m�k�, ale fartuch, bia�y " +
	"i wyprasowany, l�ni czysto�ci�. Co jaki� czas zagl�da do kuchni " +
	"uwa�nym spojrzeniem, kontroluj�c prac� piekarczyk�w. Jego " +
	"okr�g�a, puco�owata twarz jest ci�gle roze�miana, a oczy " +
	"dobrodusznie spogl�daj� na klient�w.\n");

    dodaj_przym("u�miechni�ty", "u�miechni�ci");
    dodaj_przym("gruby", "grubi");

    set_act_time(30);
    add_act("emote wodzi wzrokiem dooko�a.");
    add_act("zanuc");
    add_act("chrzaknij");
    add_act("zarumien sie");
    add_act("powiedz Ehh, ci�ka ta moja robota.");
    add_act("beknij cicho");
    add_act("emote rozgl�da si� po klienteli.");
    add_act("emote mruczy pod nosem: Ciekawe sk�d teraz m�k� b�d� bra�.");
    add_act("emote mruczy co� po nosem.");

//    set_cchat_time(15);
//    add_cchat();

//    set_default_answer(VBFC_ME("default_answer"));

    set_stats ( ({ 65, 31, 70, 21, 30, 58 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    
    add_armour(RINDE_UBRANIA + "spodnie_los_los_biedaka.c");
    add_armour(RINDE_UBRANIA+"fartuch_bialy_przybrudzony.c");
    
//    add_prop(CONT_I_WEIGHT, 91000);
//    add_prop(CONT_I_HEIGHT, 172);
    add_ask("m�k�",VBFC_ME("pyt_o_make"));
}

string
default_answer()
{
     set_alarm(2.0, 0.0, "command_present", this_player(),
             "powiedz do " + OB_NAME(this_player()) +
                  " Ja nic nie wiem!");
     return "";
}

string
pyt_o_make()
{
     set_alarm(2.0, 0.0, "command_present", this_player(),
             "powiedz do " + OB_NAME(this_player()) +
                  " No w�a�nie? Sk�d m�k� mam bra� teraz? Nie wiesz?");
     return "";
}

string
query_presentation()
{
    return "Gruby " + ::query_presentation();
}

void
return_introduce(string imie)
{
    object osoba = present(imie, environment());

    if (osoba)
    {
        command("przedstaw sie " + OB_NAME(osoba));
    }
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
	case "nadepnij":
	    set_alarm(2.0, 0.0, "zly", wykonujacy);
            break;

        case "szturchnij":
	    set_alarm(2.0, 0.0, "taki_sobie", wykonujacy);
            break;

        case "za�miej":
        case "poca�uj":
        case "przytul":
        case "pog�aszcz":
	    set_alarm(2.0, 0.0, "dobry", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
    switch (random(6))
    {
    case 0 : set_alarm(2.0, 0.0, "command_present", kto,                  "powiedz do "+OB_NAME(kto) + " Ehh, prosz� o rozejm."); break;
    case 1 : set_alarm(2.0, 0.0, "command_present", kto, "powiedz do "+OB_NAME(kto) +
              " Co z tob� u licha?");
        break;
    case 2 : command("kopnij " + OB_NAME(kto)); break;
    case 3 : command("emote wskazuje w kierunku wyj�cia."); break;
    case 4 : command("emote zdaje si� ignorowa� wszystkich i wszystko wok�.");
              break;
    case 5 : set_alarm(2.0, 0.0, "command_present", kto, "powiedz do "+OB_NAME(kto) + " Dosy� tego!");
         command("rozejrzyj sie panicznie");break;
    default:
      command("westchnij cicho");
    }
}

void
taki_sobie(object kto)
{
    switch (random(2))
        {
          case 0 : set_alarm(2.0, 0.0, "command_present", kto, "powiedz do " +OB_NAME(kto) + " S�ucham?"); break;
          case 1 : set_alarm(2.0, 0.0, "command_present", kto, "powiedz do " +OB_NAME(kto) + " O co chodzi?"); break;
        }
}

void
dobry(object kto)
{
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
      {
        switch (random(3))
        {
          case 0 : command("usmiechnij sie niesmialo do " +OB_NAME(kto));
                        break;
          case 1 : command("zarumien sie"); break;
          case 2 : set_alarm(2.0, 0.0, "command_present", kto, "powiedz do " +OB_NAME(kto) +
                                        " Oh, panienko...");
          break;
        }
      }
}

void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
}
int
query_karczmarz()
{
	return 1;
}
