/* Autor: Avard
   Opis : Sniegulak
   Data : 10.04.07 */

inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include "dir.h"

void random_przym(string str, int n);

void
create_zwierze()
{
    ustaw_odmiane_rasy("szczur");
    set_gender(G_MALE);

    set_long("Czerwone oczka, brudna sier^s^c i d^lugi ogon. To "+
        "przedstawiciel szczurzej populacji, kt^ora tak uwielbia "+
        "ciemne zakamarki i resztki jedzenia. Okaz ten jest "+
        "wyj^atkowo chudy i ruchliwy.\n");

    random_przym("wychudzony:wychudzeni niewielki:niewielcy drobny:drobni "+
        "ko^scisty:ko^sci^sci ma^ly:mali ||brudny:brudni "+
        "szorstkow^losy:szorstkow^losi||czerwonooki:czerwonoocy "+
        "p^lochliwy:p^lochliwi ruchliwy:ruchliwi",2);


    set_stats (({1, 40, 1, 20, 20, 1}));

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 800);
    add_prop(CONT_I_VOLUME, 800);
    add_prop(CONT_I_HEIGHT, 10);

    //set_attack_unarmed(0, 15, 15, W_SLASH,  50, "praw^a ^lapk^a");
    //set_attack_unarmed(1, 15, 15, W_SLASH,  50, "lew^a ^lapk^a");
    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "g^low^e");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "lew^a ^lapk^e");
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "praw^a ^lapk^e");
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "tyln^a, lew^a ^lapk^e");
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "tyln^a, praw^a ^lapk^e");

    set_random_move(1);
    set_restrain_path(KANALY_LOKACJE);
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
        case "szturchnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
   TO->run_away();
}

//WTF?! run_away ju� jest w std/zwierze! Vera.
/*void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
    TO->run_away();
}*/
void random_przym(string str, int n)
{
        mixed* groups = ({ });
        groups = explode(str, "||");
        int groups_size = sizeof(groups);
        if(n > groups_size)
                return;
        for(int i =0; i < groups_size; i++)
                groups[i] = explode(groups[i], " ");
        
        for(int a = 0; a < n; a++)
        {
                int group_index = random(sizeof(groups));
                int adj_index = random(sizeof(groups[group_index]));
                string* tmp = explode(groups[group_index][adj_index], ":");
                dodaj_przym(tmp[0], tmp[1]);
                groups = groups-({groups[group_index]});
        }
}
