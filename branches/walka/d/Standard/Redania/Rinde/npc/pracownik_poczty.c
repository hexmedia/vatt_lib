
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include "dir.h"
inherit RINDE+"std/npc";
inherit "/lib/sklepikarz";

create_rinde_npc()
{
    ustaw_odmiane_rasy("m�czyzna");
    dodaj_nazwy("pracownik poczty");
    dodaj_nazwy("pracownik");

    dodaj_przym("wysoki", "wysocy");
    dodaj_przym("ciemnow^losy", "ciemnow^losi");

    set_long("Wysoki, ciemnow^losy m^e^zczyzna sprawnie porusza si^e mi^edzy stosami " +
             "list^ow i paczek. Jego m^etny wzrok ci^agle biega po okolicy, przeszukuj^ac " +
             "najmniejszy nawet zakamarek. Pracownik bezustannie poprawia swoj^a " +
             "szar^a, przybrudzon^a szat^e i ocenia jej stan, kt^ory nie jest ju^z najlepszy. " +
             "Na twarzy m^e^zczyzny nigdy nie pojawia si^e u^smiech, ale jego niebieskie " +
             "oczy wydaj^a si^e by^c ci^agle roze^smiane.\n");

    ustaw_imie(({"donis", "donisa", "donisowi", "donisa", "donisem",
	"donisie"}), PL_MESKI_OS);
    set_living_name("donis");
    set_surname("Nerrisent");
    set_title(", Pracownik poczty w Rinde");

    set_gender(G_MALE); 

    add_prop(CONT_I_WEIGHT, 70000);
    add_prop(CONT_I_HEIGHT, 180);

    set_skill(SS_DEFENCE, 30);
    set_skill(SS_UNARM_COMBAT, 15);
    set_stats(({43, 40, 32, 65, 55, 50}));

    set_chat_time(52);
    add_chat("Listy, listy, ci^agle te listy...");
    add_chat("Nasza poczta jest niezawodna!");

    set_act_time(70);
    add_act("emote drapie si^e po g^lowie.");
    add_act("emote duma nad czym^s.");
    add_act("emote ziewa dyskretnie.");
    add_act("emote poprawia swoj^a szat^e.");
    add_act("emote mruczy pod nosem.");
    add_act("emote ogl^ada jedn^a z paczek.");

    set_cchat_time(18);
    add_cchat("Ja ci pok^a^z^e!");
    add_cchat("A niech ci^e!");

    set_cact_time(7);
    add_cact("emote ryczy w^sciekle.");
    add_cact("emote sapie gro^xnie.");

    add_armour("/d/Standard/items/ubrania/szaty/dluga_jasnobrazowa");

    set_default_answer(VBFC_ME("default_answer"));

    add_ask(({"zdrowie", "prac^e", "^zycie"}), "powiedz Dzi^ekuj^e, nie narzekam.", 1);
    add_ask("pomoc", "powiedz Mmm.. Nie, nie trzeba.", 1);
    add_ask("zadanie", "powiedz Zadanie? Jakie zadanie?", 1);
    add_ask(({"go^l^ebie", "go^l^ebia"}), "powiedz Mo^ze pan" +
    TP->koncowka("", "i") + " kupi^c ode mnie go^l^ebia pocztowego.", 1);
    
    config_default_sklepikarz();
    set_money_greed_buy(110);
    set_money_greed_sell(110);
    set_co_skupujemy(0);

    set_store_room("/d/Standard/Redania/Rinde/Polnoc/lokacje/magazyn_golebi.c");
    
    add_prop(NPC_M_NO_ACCEPT_GIVE, 0);
}

void
emote_hook(string emote, object wykonujacy, string przyslowek = 0)
{
    switch (emote)
    {
    case "uk^lo^n":
      command("kiwnij glowa");
      break;
    case "zata^ncz":
    case "u^sciskaj":
    case "szturchnij":
    case "pogr^o^x":  
    case "po^laskocz":  
    case "obejmij":
    case "klepnij":  
    case "gr^o^x":
    case "b^lagaj":
    case "pog^laszcz":
    case "poca^luj":
    case "przytul":
      switch (random(3))
      {
      case 1:
        command("skrzyw sie");
        break;
      case 2:
        command("fuknij");
        break;
      case 3:
        command("zacisnij piesci");
      }  
      break; 
    case "spoliczkuj":  
    case "kopnij":
    case "opluj":
      set_alarm(1.0, 0.0, "wkop_mu", wykonujacy);
      break;
    case "za^smiej":
      set_alarm(1.0, 0.0, "return_laugh", wykonujacy);
      break;
    case "przywitaj":
    case "powitaj":
      set_alarm(1.0, 0.0, "przywitaj", wykonujacy);
      break;
    }
}

void
wkop_mu(object kto)
{
    switch (random(3))
    {
    case 0:
      command("powiedz Jak ^smia^le^s to zrobi^c, chamie?!");
      break;
    case 1:
      command("powiedz Zr^ob tak jeszcze raz, a po^za^lujesz!");
      break;
    case 2:
      command("powiedz Spieprzaj!");
    }

    command("kopnij " + OB_NAME(kto));
}

void
return_laugh(object kto)
{
    switch(random(2))
    {
    case 0:
      command("powiedz ^Smiej si^e, ^smiej.");
      break;
    case 1:
      command("powiedz ostro Zamknij si^e.");
    }
}

void
return_introduce(string imie)
{
    object osoba;

    osoba = present(imie, environment());

    if (osoba)
      {
      command("powiedz Ach tak, mi^lo mi.");
      command("przedstaw sie");
      }

    else
      command("wzrusz ramionami");
}

void
przywitaj(object kto)
{
      command("powiedz Dzie^n dobry.");
}

void
shop_hook_niczego_nie_skupujemy()
{
    command("powiedz Ale� ja niczego nie skupuj�! Ja tylko go��bie "+
    	"pocztowe sprzedaj�!");
}

void
shop_hook_list_empty_store(string str)
{
    command("powiedz Obecnie nie mam ju� �adnych go��bi na sprzeda�.");
}

void
init() 
{
    ::init();
    init_sklepikarz();
}

