
#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <object_types.h>
#include <filter_funs.h>
#include "dir.h"

inherit RINDE+"std/npc.c";
inherit "/lib/sklepikarz";

int czy_walka();
int walka = 0;

int co_godzine();
void znik();

void
create_rinde_npc()
{
    ustaw_odmiane_rasy("p�elf");

    set_gender(G_MALE);

    dodaj_nazwy("straganiarz");

    set_living_name("kupieczrynkupolelf");

    set_long("Nie wiadomo czy sprawiaj^a to jego oczy - wielkie jak u "+
       "elfa, lecz jednocze^snie z wyra^xn^a przewag^a cech ludzkich - "+
       "bezczelne i m^sciwe, czy jego postawa, ale juz na pierwszy rzut "+
       "oka wida�, �e to figura do�� oryginalna i nie da sobie w kasz� "+
       "dmucha�. Ubrany jest w jak�� dziwaczn�, pow��czyst� szat� w "+
       "kolorze jasnej czerwieni i granatu, a d�ugie, br�zowe w�osy "+
       "odrzucone s� na plecy i przewi�zane rzemieniem. Przy pasie "+
       "zatkni�t^a ma d�ug^a, wysadzan^a rubinami mizerykordi^e, a tu^z "+
       "pod r�k� po�o�on� proc�.\n");

    dodaj_przym("wysoki", "wysocy");
    dodaj_przym("d�ugow�osy", "d�ugow�osi");

    set_cact_time(30);
    add_cact("emote zaciska z�by.");
    add_cact("emote wydaje z siebie st�umiony syk.");

    set_default_answer(VBFC_ME("default_answer"));

    set_stats ( ({ 70, 50, 80, 21, 30, 58 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_KNIFE, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    config_default_sklepikarz();
    set_money_greed_buy(118);
    set_money_greed_sell(111);
    set_money_greed_change(140);
    set_co_skupujemy(O_UBRANIA|O_BUTY|O_KUCHENNE|
    				O_ZBROJE|O_INNE|O_BIZUTERIA);
    //set_co_skupujemy(MAXINT);

    //set_store_room(RINDE + "centrum/magazyn_straganiarza.c");
set_store_room(RINDE + "Centrum/lokacje/magazyn_straganiarza.c");

    add_armour(RINDE_UBRANIA + "szaty/kunsztowna_powloczysta_Lp.c");
    add_object(RINDE_BRONIE + "mizerykordie/zdobiona_waska.c");
//    add_armour(RINDE_UBRANIA + "spodnie_los_los_biedaka.c");

    add_prop(LIVE_I_NEVERKNOWN, 1);
//    add_prop(CONT_I_WEIGHT, 91000);
//    add_prop(CONT_I_HEIGHT, 172);

    set_alarm_every_hour("co_godzine");
    co_godzine();

    set_alarm(5.0, 0.0, "command", "stan za pierwszym straganem");
    set_alarm(10.0, 0.0, "command", "emote k^ladzie towar na straganie.");
}

void 
shop_hook_tego_nie_skupujemy(object ob)
{
    command("powiedz Nie skupuj� takich rzeczy.");
}

string
default_answer()
{
     set_alarm(2.0, 0.0, "command_present", this_player(),
             "emote wzrusza ramionami i wskazuje na towary.");
     return "";
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
	case "nadepnij":
	    set_alarm(1.0, 0.0, "command_present", wykonujacy,
		"emote szczerzy d�ugie, bia�e z�by, dziwnie przypominaj�ce " +
		"z�by jakiego� drapie�nika, i k�adzie r�k� na sztylecie.");
            break;

        case "szturchnij":
        case "za�miej":
        case "poca�uj":
		if (wykonujacy->query_gender() == G_FEMALE)
			set_alarm(1.0, 0.0, "command_present", wykonujacy,
				"popatrz zimno na " + OB_NAME(wykonujacy));
		else
			set_alarm(1.0, 0.0, "command_present", wykonujacy,
				"emote wymownie k�adzie d�o� na r�koje�ci sztyletu.");
		break;
    }
}
void
attacked_by(object wrog)
{
    if(walka==0) 
    {

    command("krzyknij Ratunku!");
    command("dobadz broni");

    set_alarm(5.0, 0.0, "czy_walka", 1);
    walka = 1;
    return ::attacked_by(wrog); 
    }

    else if(walka == 1)
    {
      set_alarm(1.0, 0.0, "command", "krzyknij Pomocy!");
      walka = 1;

      return ::attacked_by(wrog);
    }

}

int
czy_walka()
{
    if(!query_attack() && walka)
    {

       this_object()->command("rozejrzyj sie ostroznie");
       this_object()->command("opusc bron");
       

       walka = 0;
       return 1;
    }
       else
       {
       set_alarm(5.0, 0.0, "czy_walka", 1);
       }

}
void
init()
{
    ::init();
    init_sklepikarz();
}
void
znik()
{
    set_alarm(5.0, 0.0, "command", "emote przechodzi przez bram^e.");
    remove_object();
}

void
do_domu()
{
    if(query_attack())
    {
        set_alarm(10.0,0.0,"do_domu");
        return;
    }
    else
    {
        int k = 0;
        mixed droga2=znajdz_sciezke(environment(this_object()),
            POLNOC + "lokacje/ulica3");
        command(droga2[k]);
        if(k==sizeof(droga2)-1)
        {
            znik();
            return;
        }
        else
        {
            k++;
            set_alarm(15.0,0.0,"do_domu");
            return;
        }
    }
}

int
co_godzine()
{
    if (environment(TO)->pora_dnia() >= MT_POZNY_WIECZOR)
    {
        set_alarm(4.0, 0.0, "command", "powiedz Zabieram si^e st^ad.");
        set_alarm(10.0, 0.0, "command", "emote zdejmuje towar ze straganu.");
        set_alarm(13.0, 0.0, "command", "wyjdz zza straganu");
        set_alarm(15.0, 0.0, "do_domu");
    }
}

public string
query_auto_load()
{
    return 0;
}
