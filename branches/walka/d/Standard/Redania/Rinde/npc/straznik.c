#pragma strict_types

#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"
inherit RINDE+"std/npc.c";
string 
*losowy_przym()
{
    string *lp;
    string *lm;
    int i;
    lp=({ "opalony"  ,"bialowlosy"  ,"chudy"  ,"gruby"  ,"barczysty" ,
          "szybki" ,"kulejacy"});
    lm=({ "opaleni"  ,"bialowlosi"  ,"chudzi" ,"grubi"  ,"barczysci"  ,
          "szybcy" ,"kulejacy"});
    i=random(7);

    return ({ lp[i] , lm[i] });

}

string 
*losowy_przym2()
{
    string *lp;
    string *lm;
    int i;
    lp=({ "wysoki"  ,"silny"  ,"mlody"  ,"zielonooki"  ,"zamyslony" ,
          "stary" ,"zreczny"});
    lm=({ "wysocy"  ,"silni"  ,"mlodzi" ,"zielonoocy"  ,"zamysleni"  ,
          "starzy" ,"zreczni"});
    i=random(7);

    return ({ lp[i] , lm[i] });

} 

void
create_rinde_npc()
{
    int skil;
    string *przm = losowy_przym();
    string *przm2 = losowy_przym2();
    set_living_name("straznik_w_candlekeep");
    dodaj_nazwy(PL_MEZCZYZNA);
    dodaj_przym( przm[0], przm[1] );   
    dodaj_przym( przm2[0], przm2[1] );
    set_long("^Lysy jak kolano drab z i^scie dzicz^a szczecin^a na g^ebie.\n");
    ustaw_odmiane_rasy(({ "straznik", "straznika", "straznikowi", "straznika", 
       "straznikiem", "strazniku" }), ({ "straznicy", 
       "straznikow", "straznika", "straznicy", "straznikami",
                   "straznikach" }), PL_MESKI_OS);
    set_gender(G_MALE);
    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 75000);
    add_prop(CONT_I_HEIGHT, 175);   
    add_prop(NPC_I_NO_FEAR,1);
    switch(przm2[0])
    {
    case "wysoki":
    set_stats(({ 35, 50, 50, 20, 20, 30}));
    skil=20;
    break;
    case "silny":
    set_stats(({ 90, 60, 60, 10, 20, 20}));
    skil=70;
    break;
    case "mlody":
    set_stats(({ 40, 45, 50, 20, 20, 20}));
    skil=30;
    break;
    case "zielonooki":
    set_stats(({ 60, 50, 50, 10, 15, 40}));
    skil=40;
    break;
    case "zamyslony":
    set_stats(({ 70, 40,60, 10, 15, 10}));
    skil=50;
    break;
    case "szybki":
    set_stats(({ 50, 70,60, 40, 45, 10}));
    skil=60;
    break;
    case "zreczny":
    set_stats(({ 50, 90,60, 40, 45, 10}));
    skil=55;
    break;
    }
    set_skill(SS_DEFENCE,skil);
    set_skill(SS_WEP_SWORD,20+random(skil));
    set_skill(SS_PARRY, 20+random(skil/2));
    set_skill(SS_SHIELD_PARRY,30+random(skil));
    set_skill(SS_AWARENESS, 20 + random(skil/2));

    set_chat_time(10);
    add_cchat("A masz!");
    add_cchat("Zginiesz smiercia powolna i bolesna.");
    add_cchat("Za Candlekeep.");

    set_act_time(10);
    add_act("ziewnij");
    add_act("rozejrzyj sie uwaznie");
    add_act("tupnij wesolo");

    set_aggressive("@@atak");
    set_attack_chance(100);
}

void
return_introduce(string imie)
{
    object osoba;
    
    osoba = present(imie, environment());

    if (osoba)
        command("powiedz Milo cie poznac, " + osoba->query_wolacz() + ".");
}

public void wesprzyj(object ob)
{ 
    command("wesprzyj " + OB_NAME(ob));    
    if (query_attack())
    {
        switch(random(3))
        {
            case 0: command("warknij"); break;
            case 1: command("krzyknij No to teraz masz tu w ryja!"); break;
        }
        command("wesprzyj " + OB_NAME(ob));
        return ;
    }
}

public void pomocy()
{
    (all_inventory(environment(this_object())) - ({ this_object() }))->wesprzyj(this_object());
}

void attacked_by(object wrog)
{
    command("emote krzyczy: Straz! Atakuja miasto!");
    set_alarm(1.0, 0.0, "pomocy");
    set_alarm(0.1, 0.0, &set_follow(wrog, 1.0,0));
    return ::attacked_by(wrog);
}
int
ataczek()
{
        return RINDE_PATROL->query_czy_zly(this_interactive());
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
int plec=wykonujacy->query_gender();
    switch(emote)
    {
    
        case "pocaluj":
        case "podziekuj":
        case "pogratuluj":
        case "przepros":
        case "mrugnij": 
 if (query_attack())
 break;
                 if(!plec) /* facio */
        {
               command("powiedz Ty sobie nie pozwalaj za duzo.");
               command("splun");
               }
               else if(plec==1)
               {
                command("powiedz no no..... Zaczynasz mi sie podobac..");
                command("powiedz Moze wyskoczymy "+
                  "gdzies po sluzbie.");
                command("mrugnij");
               } 
                        else
                {
                 command("powiedz kim ty wogole jestes ???? Facetem czy baba ??");
                 command("wzdrygnij sie");
                }
                break;
        case "nadepnij":
        case "szepnij":
        case "blagaj":
        case "usciskaj": 
 if (query_attack())
 break;
              set_alarm(4.0, 0.0, "command", 
                "powiedz Nie lubie jak mi sie przeszkadza!!");
break;
        case "spoliczkuj":
case "opluj":
case "kopnij":
command("powiedz I po co ci to bylo ?");
                        set_alarm(1.0, 0.0, "command", "krzyknij Zgin niczego niewarta istoto !");
                        command("zabij "+lower_case(wykonujacy->query_name(PL_BIE)));
                        RINDE_PATROL->dodaj_zlego(wykonujacy);
pomocy();
                     break;
    }
}

