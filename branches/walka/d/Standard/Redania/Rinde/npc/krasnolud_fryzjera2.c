#include <stdproperties.h>
#include <macros.h>
#include <money.h>
#include <pl.h>
#include "dir.h"
inherit RINDE+"std/npc.c";

int czy_walka();
int walka = 0;

create_rinde_npc()
{
    dodaj_przym("pot�ny","pot�ni");
    dodaj_przym("ciemnooki","ciemnoocy");
    ustaw_odmiane_rasy(PL_KRASNOLUD);
    set_gender(G_MALE);
    set_long("Ubrany w sk�rzan� kurt� i kolcze spodnie, "+
             "sprawia wra�enie niezbyt gro�nego, ale jeden "+
	     "rzut oka na przepe�nione spokojn� pewno�ci� "+
	     "siebie oczy, na spos�b trzymania broni a i "+
	     "na sam ci�ar broni, przekonuje, �e jest to osoba, "+
	     "kt�rej walki s� nieobce.\n");
    add_prop(CONT_I_WEIGHT, 115000);
    add_prop(CONT_I_HEIGHT, 162); 
    add_prop(NPC_I_NO_RUN_AWAY, 1);
    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(NPC_I_NO_FEAR, 1);

    set_stats(({ 79, 42, 79, 35, 24, 77}));
    //add_object(RINDE_BRONIE+"miecz_szeroki_poreczny.c");
    add_weapon(RINDE_BRONIE + "miecze/miecz_szeroki_poreczny.c");
    add_armour(RINDE_ZBROJE + "spodenki/szerokie_zabrudzone_Mk.c");
    add_armour(RINDE_ZBROJE + "kurtki/skorzana_mocna_Mk.c");

    set_cchat_time(1);
    add_cchat("Bracia! Do boju!");
    add_cchat("Do boju kurwaaaaa!");
    add_cchat("Waaaaa! Waaa! Waaaaaaaaaaaa!");
    add_cchat("Jest m�j!");
    add_cchat("Aaaa!");
    add_cchat("Osz ty!");
    add_cchat("Ty psie!");
    
    set_alarm(0.2, 0.0, &command("wesprzyj fryderyka"));
}    
    
    
void
attacked_by(object wrog)
{
    if(walka==0) 
    {
    this_object()->command("dobadz broni");
//    palka = add_weapon(RINDE_BRONIE + "miecz_szeroki_poreczny.c");
    set_alarm(5.0, 0.0, "czy_walka", 1);
    walka = 1;
    return ::attacked_by(wrog); 
    }

    else if(walka == 1)
    {
      set_alarm(5.0, 0.0, "czy_walka", 1);
      walka = 1;
      return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
       this_object()->command("kopnij cialo");
       this_object()->command("opusc bron");
       walka = 0;
       say(this_object()->short(PL_MIA)+" wychodzi.\n");
       this_object()->destruct();
       return 1;
    }
       else
       {
       set_alarm(5.0, 0.0, "czy_walka", 1);
       }
}