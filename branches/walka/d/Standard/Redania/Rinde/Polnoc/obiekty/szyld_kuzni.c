
/* Tabliczka, made by Avard 04.06.06 */
inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("szyld");
    dodaj_przym("drewniany","drewniani");
    dodaj_przym("niewielki","niewielcy");

    set_long("Rysunek na szyldzie przedstawia m^lot wisz^acy nad kowad^lem, "+
        "na kt^orego ^srodku pisze: \"Ku^znia\".\n");
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 35);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_I_NO_GET, "Szyld jest przybity mocnymi gwo^zdziami, nie "+
        "dasz rady go zabra^c.\n");
    ustaw_material(MATERIALY_DR_BUK);
    set_type(O_INNE);
}
