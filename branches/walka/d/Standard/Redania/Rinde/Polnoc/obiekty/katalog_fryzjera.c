/* Katalog do zak�adu fryzjerskiego, w kt�rym znajduje si�
   ca�a pomoc- informacje na temat wykonywanych us�ug przez Fryderyczka
   naszego.
                            Lil. 30.10.2005 */

inherit "/std/book";

#include "dir.h"
#include <materialy.h>
#include <stdproperties.h>
#include <macros.h>
#define TTYPY "/d/Standard/obj/tatuaze/tat_typy.txt"
#define TWZORY "/d/Standard/obj/tatuaze/tat_wzory.txt"
#define FDLUGOSCI "/d/Standard/obj/fryzury/fryz_dlugosci.txt"
//#define FTYPY "/d/Standard/obj/fryzury/fryz_typy.txt"
#define FLISTA "/d/Standard/obj/fryzury/fryz_lista.txt"
#define FKOLORY "/d/Standard/obj/fryzury/kolory.txt"


create_book()
{
    ustaw_nazwe("katalog");
    dodaj_przym("b��kitny","b��kitni");
    dodaj_przym("opas�y","opa�li");

    set_long("Ok�adk� tego grubego katalogu zdobi starannie "+
            "wygrawerowany napis: 'Zak�ad Fryzjerski w Rinde'. Jej pozaginane "+
            "rogi i pop�kany grzbiet �wiadczy o cz�stej jego "+
            "eksploatacji.\n");

    add_prop(OBJ_I_VALUE, 3);
    add_prop(OBJ_I_NO_GET, "Katalog jest przyczepiony �a�cuchem do p�ki!\n");
    add_prop(OBJ_I_WEIGHT, 2001);
    add_prop(OBJ_I_VOLUME, 999);

    setuid();
    seteuid(getuid());

    set_max_pages(12);
    set_autoload();
}

void
read_book_at_page(int strona)
{
    switch (strona)
    {
	case 1: write("\t\t\tSPIS TRE�CI:\n\n"+
	        "\t\t 1.   Ten spis.\n"+
		"\t\t 2.   Cennik.\n"+
		"\t\t 3.   Miejsca, kt�re tatuuj�.\n"+
		"\t\t 4.   Typy tatua�y, jakie wykonuj�.\n"+
		"\t\t 5.   Wzory tatua�y, kt�re znam.\n"+
		"\t\t 6.   Fryzury i ich d�ugo�ci.\n"+
		"\t\t 7.   Typy fryzur.\n"+
		"\t\t 8.   Farby do w�os�w, jakimi dysponuj�.\n"+
		"\t\t 9.   Strzy�enie w�s�w.\n"+
		"\t\t10.   Strzy�enie brody.\n"+
		"\t\t11.   Rzecz o przek�uwaniu.\n"+
		"\t\t12.   Uwagi.\n");
	        break;
		
	case 2: write("\t\tCENNIK:\n"+
	              "Wykonanie tatua�u:\t6 koron\n"+
		      "Usuni�cie tatua�u:\t180 koron\n"+
		      "Zam�wienie fryzury:\t4 korony\n"+
		      "Farbowanie w�os�w:\t18 koron\n"+
		      "Strzy�enie w�s�w:\t<wielka plama po kawie!>\n"+
		      "Strzy�enie brody:\t<wielka plama po kawie!>\n"+
		      "Przek�uwanie:\t\t2 korony\n");break;
		
	case 3: write("Na czole, prawym policzku, lewym policzku, karku, "+
		"skroni, szyi, plecach, lewej �opatce, prawej �opatce, "+
	        "lewym ramieniu, prawym ramieniu, lewym przedramieniu, "+
		"prawym przedramieniu, lewej d�oni, prawej d�oni, lewej "+
		"piersi, prawej piersi, brzuchu, lewym po�ladku, prawym po�ladku "+
		"lewym udzie, prawym udzie, lewej �ydce, prawej �ydce, lewej "+
		"stopie, prawej stopie.\n"+
		"Innych miejsc nie tatuuj�, z obawy o zdrowie klienta.\n");
	        break;
	case 4:
	    TP->more(implode(explode(read_file(TTYPY), "\n"),", ")+
		".");
	        break;
	case 5:
	    {
		mixed twzory = explode(read_file(TWZORY), "\n");
		int i;
		string s = "";

		for (i = 0; i < sizeof(twzory); i++)
		    s += (i + 1) + ". " + twzory[i] + "\n";
		TP->more(s+
		     "\n\tJestem otwarty na propozycje nowych tatua�y!\n");
	    }
	    break;
       case 6:
	    TP->more(implode(explode(read_file(FDLUGOSCI), "\n"), ", ")+
		".");
	    break;
		case 7:
		{  
			string ret = "*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*\n";
			ret +=       "*                                                                             *\n";
			ret +=       "*    Oferuj� nast�puj�ce u�o�enia w�os�w:                                     *\n";
			ret +=       "*                                                                             *\n";
			ret +=       "*                                                                             *\n";
		   
			string *fryzury = explode(read_file(FLISTA), "\n");
			string pojfryz;
		   
			for(int i = 0; i < sizeof(fryzury); i++)
			{
				ret += "*";
				pojfryz=explode(fryzury[i], ":")[0];
				ret += "          ";
			    ret += pojfryz;
				for(int ii = 0; ii < (67-strlen(pojfryz)); ii++)
					ret += " ";
				ret += "*\n";
		   }
		   
			ret += "*                                                                             *\n";
			ret += "*                                                                             *\n";
			ret += "*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*\n";
		   
			TP->more(ret);
		}
	    break;
       case 8: write("Farbuj� w�osy na:\n");
                TP->more(implode(map(explode(read_file(FKOLORY), "\n"),
		 &operator([])(,0) @ &explode(,":")), ", ")+
		".");
            break;
       case 9: write("Kto� tu wyla� kaw�! Wszystko jest rozmazane...\n"); break;
       case 10: write("Kto� tu wyla� kaw�! Wszystko jest rozmazane...\n"); break;
       case 11: write("Przek�uwam nast�puj�ce miejsca:\n"+
                "lewe ucho, prawe ucho, nos, lew� brew, praw� brew, brod�.\n"); break;
       case 12: write("Moje farby do w�os�w powinny trzyma� "+
                "oko�o miesi�ca.\n"+
		"Nie handluj� niczym, wi�c w kolczyki zaopatrze� musisz si� "+
		"samodzielnie.\n"); break;
    }
}
