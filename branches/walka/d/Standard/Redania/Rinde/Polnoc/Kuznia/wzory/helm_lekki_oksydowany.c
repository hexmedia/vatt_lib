inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(RINDE_ZBROJE + "helmy/lekki_oksydowany_M");
    //Tak naprawde to jest to kaptur, a nie helm.
    set_estimated_price(1075);
    set_time_to_complete(7000);
    enable_want_sizes();
}

