inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(RINDE_ZBROJE + "kirysy/srebrny_elegancki_Lc");
    set_estimated_price(12000);
    set_time_to_complete(12000);
    enable_want_sizes();
}

