
/* Opis By Tinardan, przerobiony i zakodowany przed Lil. 18.08.2005 
*/

inherit "/std/room";

#include <stdproperties.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include <macros.h>
#include <pattern.h>
#include "dir.h"

void
create_room() 
{
    set_short("ku�nia");
    set_long("@@dlugi@@");

    add_prop(ROOM_I_INSIDE,1);
	
    add_item("palenisko", "Wysoka, kamienna konstrukcja, jednym "+
	"bokiem wspieraj�ca si� o �cian� zabiera spor� cz�� "+
	"pomieszczenia. Miechy kowalskie, co chwil� "+
	"pobudzane do pracy przez kowala, podsycaj� "+
	"niegasn�cy �ar paleniska. Otw�r buzuj�cy ogniem "+
	"wygl�da jak rozwarta paszcza jakiego� stwora.\n");	
    add_item("stojaki","Stojaki na bro� nie rzucaj� si� od "+
	"razu w oczy i s� ustawione tak, by tylko kowal mia� do nich "+
	"dost�p. Zapewne to tam s� sk�adowane towary wykonywane "+
	"na specjalne zam�wienia.\n");
		
    add_npc(RINDE_NPC + "kowal");
		
    add_object(MEBLE + "krzeslo_drewniane_proste", 2);
    dodaj_rzecz_niewyswietlana("drewniane proste krzes�o", 2);

    add_object(POLNOC_DRZWI + "z_kuzni.c");
    dodaj_rzecz_niewyswietlana("pot^e^zne drzwi", 1);
}

public string
exits_description() 
{
    return "Wyj^scie z ku^xni prowadzi na ulice.\n";
}

string
dlugi()
{
    string str;
    int i, il, il_krzesel;
    object *inwentarz;
    object kowal = find_living("harr");
    inwentarz = all_inventory();
    il_krzesel = 0;
    il = sizeof(inwentarz);
    for (i = 0; i < il; ++i)
    {
        if (inwentarz[i]->test_kuznia_rinde()) {
            ++il_krzesel; 
        }}
        str = "Mrok i gor�c obra�y sobie zapewne to pomieszczenie "+
            "jako swoje kr�lestwo. Zakurzona b�ona rozci�gni�ta "+
            "w okiennych ramach przepuszcza jedynie niewyra�n� "+
            "po�wiat�, tak, �e z trudem mo�na ze �rodka odr�ni� "+
            "dzie� od nocy. Pod�oga jest zbudowana z ociosanego "+
            "kamienia";
        if(!this_player()->query_armour(TS_R_FOOT))
        {
            str += " i przyjemnie ch�odna. ";
        }
        else { str+= ". "; }

        str += "Na �rodku izby, niczym tron, rozpar�o si� "+
            "pot�ne, migocz�ce ogniem palenisko. "+
            "Rozta�czone p�omienie rzucaj� pos�pne "+
            "cienie na �ciany z grubo ciosanych bel";

        if(il_krzesel)
        {
            str += " i nieliczne krzes�a.";
        }
        else { str += "."; }       

        str +=" W k�cie wida� stojaki na miecze, groty i g�owice, ";

        if(!sizeof(filter(all_inventory(this_player()) + 
                        all_inventory(environment(this_player())),
                        &operator(!=)(,0) @ &->query_lit())) || 
                this_player()->query_skill(SS_AWARENESS) >= 44 ||
                !dzien_noc())
        {
            str +="ale pomieszczenie jest na tyle ciemne, �e nie mo�na "+
                "dojrze�, czy s� zape�nione czy te� zupe�nie puste.";
        }
        else { str += "na kt�rych pouk�adane s� przer�ne "+
            "kowalskie dzie�a w r�nym stopniu uko�czenia."; }


        if (objectp(kowal) && (ENV(kowal) == TO) &&
                (kowal->craftsman_query_domain() == PATTERN_DOMAIN_BLACKSMITH)) {
            if (kowal->craftsman_is_busy()) {
                str += " Nad paleniskiem widoczne s� nagie, pochylone " +
                    "plecy mistrza kowalskiego. Jego r�ka rytmicznie unosi si� i opada, " +
                    "wystukuj�c odwieczny takt rodz�cej si� broni.";
            }
            else {
                str += " W p�mroku mo�na dostrzec pot�n� sylwetk� kowala.";
            }
        }

        str += "\n";
        return str;
}
