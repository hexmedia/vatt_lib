
/* 
 * Drzwi z kuzni w Rinde
 * Wykonane przez Avarda, dnia 04.06.06
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"


void
create_door()
{
    ustaw_nazwe("drzwi"); 
    dodaj_przym("pot^e^zny","pot^e^zni");
    
    set_other_room(POLNOC_LOKACJE+"ulica6.c");
    set_door_id("DRZWI_DO_KUZNI_W_RINDE");
    set_door_desc("Pot^e^zne drzwi wykonane z mocnego d^ebu i dodatkowo "+
        "okute ^zelazem nie zniszcz^a si^e zbyt ^latwo. \n");

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({"wyj^scie","w kierunku wyj�cia","z ku�ni"}));
    //set_pass_mess("przez pot^e^zne drzwi na zewn^atrz");
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_key("KLUCZ_DRZWI_DO_KUZNI");
    set_lock_name("zamek");
}
