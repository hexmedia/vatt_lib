
/* 
 * Drzwi na poczte w Rinde
 * Wykonane przez Avarda, dnia 31.05.06
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"


void
create_door()
{
    ustaw_nazwe("drzwi");    
    dodaj_przym("drewniany", "drewniani");
   
    set_other_room(POLNOC_LOKACJE + "poczta.c");
    set_door_id("DRZWI_NA_POCZTE_RINDE");
    
    set_door_desc("Pewnikiem jest to wej^scie do urz^edu pocztowego, o czym "+
        "swiadczy^c mo^ze namalowana na nich ^z^o^lta tr^abka pocztowa.\n");

    add_cmd_item(({"wymalowan^a tr^abk^e na drzwiach","namalowan^a tr^abk^e na "+
        "drzwiach","tr^abk^e na drzwiach"}),({"ob","obejrzyj"}),
        "Ot, malunek tr^abki, nad kt^or^a napisano:\n Urz^ad Pocztowy w "+
        "Rinde.\n");
        
    set_pass_command(({"poczta","na poczt�","z zewn�trz"}));
    //set_pass_mess("do budynku poczty.");
    set_open_desc("");
    set_closed_desc("");
    set_lock_command("zamknij");
    set_unlock_command("otw�rz");

    set_key("KLUCZ_DRZWI_NA_POCZTE_RINDE");
    set_lock_name("zamek");
}
