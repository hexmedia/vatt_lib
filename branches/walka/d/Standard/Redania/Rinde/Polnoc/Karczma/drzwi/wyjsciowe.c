inherit "/std/door";

#include <pl.h>
#define KOD_DRZWI "wejsciedobaszty"
#include "dir.h"

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami", 
        "drzwiach" }), PL_NIJAKI_OS); 
        
    dodaj_przym("solidny", "solidni");
    
    set_other_room(POLNOC_LOKACJE + "ulica4.c");
                                                                                       
    set_door_id(KOD_DRZWI);
    
    set_door_desc("Wygl�daj� na solidn� robot�, acz sprawiaj� "+
               "wra�enie jakby wiecznie nie domykaj�cych si�. "+
	       "Gospoda jest czynna ca�� dob� i w �wi�ta, dlatego "+
	       "te� tak mocne drzwi s�u�� chyba w g��wnej mierze "+
	       "tylko za ozdob�.\n");   
    
    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({ "wyj�cie","na zewn�trz","z karczmy"}));
    //set_pass_mess("na zewn�trz");
}
