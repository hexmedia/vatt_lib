inherit "/std/window";
inherit "/lib/peek";

#include <pl.h>
#include <macros.h>
#define KOD_OKNA "takiesobieokienkowkarczmiebasztawmiescierindegdziegrasujawiewiorki"
#include "dir.h"
int wyskocz_przez_okno();
void
create_window()
{
    ustaw_nazwe("okno"); 
    dodaj_przym("niedu�y", "nieduzi");   
                                  
    set_other_room(KARCZMA_LOKACJE + "podworze.c");
    add_peek("przez okno", KARCZMA_LOKACJE + "podworze.c");   
    set_window_id(KOD_OKNA);
    set_open(0);
    set_open_desc("");
    set_closed_desc("");
    set_window_desc("Niedu^ze okno ozdobionio zwiewnymi niebieskimi zaslonkami. \n");
    this_player()->set_hp(this_player()->query_hp()-50-random(48));

    set_pass_command("okno");
    set_pass_mess("przez niedu^ze okno na zewn^atrz");
}
void init()
{
    ::init();
    init_peek();
    add_action("wyskocz_przez_okno","wyskocz");
}


int
wyskocz_przez_okno()
{
   
   object *okna;
   
   okna = filter(all_inventory(environment( this_player() ) ),
             &operator(!=)(0,) @ &->query_window_desc());
   okna = filter(okna, &operator(!=)(0,) @ &->query_open());
   
   notify_fail("Przez co chcesz wyskoczy�?");
   
   if (!sizeof(okna))
   {
      notify_fail("Otw^orz najpierw okno.\n");
      return 0;
   }
   
   write("Podchodzisz do okna i wyskakujesz przez nie.\n");
   say(QCIMIE(this_player(), PL_MIA) + " wyskakuje przez okno!\n");
   tell_room(KARCZMA_LOKACJE + "podworze", 
          QCIMIE(this_player(), PL_MIA) + " spad^l"+
	  this_player()->koncowka("", "a", "o") +" w^la^snie na ziemi^e!\n");
      this_player()->set_hp(this_player()->query_hp()-66-random(48));
   this_player()->move_living("M",KARCZMA_LOKACJE+"podworze",1,0);
	     
   return 1;
}
