inherit "/std/receptacle";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>

create_receptacle()
{
   ustaw_nazwe( ({"szafka","szafki","szafce","szafk^e","szafk^a","szafce"}),  
        ({"szafki","szafek","szafkom","szafki","szafkami","szafkach"}),
                                       PL_ZENSKI);
	           			       
   set_long("Prosta, drewniana szafka nocna jest niewiele wy�sza od "+
	      "��ek.\n");
								                
   add_prop(CONT_I_CLOSED, 1);
   add_prop(CONT_I_RIGID, 1);
   add_prop(CONT_I_MAX_VOLUME, 24000);
   add_prop(CONT_I_MAX_WEIGHT, 10000);
   add_prop(CONT_I_VOLUME, 16800);
   add_prop(CONT_I_WEIGHT, 5000);
   add_prop(OBJ_I_NO_GET, 1);
   add_prop(OBJ_I_DONT_GLANCE,1);
    set_owners(({DAMIR,RINDE_NPC+"pomagacz_damira"}));

}

