inherit "/std/room";
#include <macros.h>
#include <stdproperties.h>
#include "dir.h"

void
create_room()
{
 set_short("Magazyn.");
 set_long("W spizarni jak to w spizarni. Jak jest ka^zdy widzi.\n");
    add_prop(ROOM_I_INSIDE,1);
 add_exit("kuchnia", "gora", 0, 0);
    add_object(KANALY_DRZWI + "klapa_do");
}
public string
exits_description() 
{
    return "Id^ac na g^ore trafisz do kuchni.\n";
}