/*     Korytarz pietra gospody Baszta.
            Opis i wykonanie: Lil
	    15 czerwca 2005 roku pa�skiego */

inherit "/std/room";
inherit "lib/peek";
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <pl.h>
#include <options.h>
#include <formulas.h>
#include "dir.h"

#define SUBLOC_SCIANA ({"�ciana", "�ciany", "�cianie", "�cian�", "�cian�", "�cianie", "na"})

int wyjdz();
int strych();

string query_opis_klapy2();
string sprawdz_opis_okna();


void
create_room()
{
    set_short("W�ski korytarz");
    set_long("Od strony zachodniej ten d^lugi i w^aski korytarz "+
             "wie^nczy jeszcze w�sze przej�cie do pokoju "+
	     "go�cinnego, na wschodzie za^s ko^nczy si^e sporych rozmiar�w "+
	     "@@sprawdz_opis_okna@@ oknem. Tu� przed nim zaczynaj� "+
	     "si� chwiejne, krzywe schodki na "+
	     "parter gospody, kt�re skrzypi� za ka�dym razem, "+
	     "gdy kt�ry� z go�ci z nich korzysta"+
	     "@@opis_sublokacji||�ciana||||. Na jednej ze �cian, tu� obok przej�cia "+
	     "na balkonik wisi |Na jednej ze �cian, tu� obok przej�cia "+
	     "na balkonik wisz� |Na jednej ze �cian, tu� obok przej�cia na balkonik wisi @@"+
	     ". W samym k^acie korytarza "+
	     "dostrzegasz kilka malutkich, przymocowanych do �ciany prog�w. "+
	     "Po przeciwnej stronie znajduj^a si^e "+
	     "kolejne drzwi do kolejnego pomieszczenia. Widok "+
	     "podrapanego drewna i fragment^ow cegie^l ods^loni^etych "+
	     "przez odpadaj^acy tynk czyni to miejsce typowym dla ka^zdej miejskiej karczmy.\n");

   add_peek("przez okno", POLNOC_LOKACJE + "ulica6.c"); 
    
    
   add_item(({"progi","szczeble"}),"Przybite do ^sciany progi uk�adaj� si� w "+
              "kilka szczebli i pn� a� do sufitu. "+
	      "Dopiero teraz dostrzegasz tam niewielk� "+
	      "@@query_opis_klapy2@@ w suficie.\n");
   
   add_item(({"schodki","schody"}),"W wyniku d^lugotrwa^lej eksploatacji, a "+
       "mo^ze niefachowego wykonania, drewniane schodki prowadz^ace na d^o^l "+
       "prezentuj^a si^e nadzwyczaj kiepsko.\n");

   add_object(KARCZMA_DRZWI+"klapa.c");

    add_prop(ROOM_I_INSIDE,1);

   add_object(KARCZMA_DRZWI + "gora");
   add_object(KARCZMA_OBIEKTY+"okno_gora");
   
   /* dodajemy sublokacj� typu 'na', jednak dosy� specyficzn� */
   add_subloc(SUBLOC_SCIANA);
   /* nie chcemy, by mo�na by�o na �cian� odk�ada� ani k�a�� rzeczy */
   add_subloc_prop(SUBLOC_SCIANA, CONT_I_CANT_ODLOZ, 1);
   add_subloc_prop(SUBLOC_SCIANA, CONT_I_CANT_POLOZ, 1);
   /* oczywi�cie mo�na na �cianie wiesza� przedmioty */
   add_subloc_prop(SUBLOC_SCIANA, SUBLOC_I_MOZNA_POWIES, 1);
   /* ale tylko typu �ciennego */
   add_subloc_prop(SUBLOC_SCIANA, SUBLOC_I_DLA_O, O_SCIENNE);
   add_subloc_prop(SUBLOC_SCIANA, CONT_I_MAX_RZECZY, 1);
   
   add_object(RINDE+"przedmioty/obrazy/maly_amatorski.c", 1, 0, SUBLOC_SCIANA);

    add_exit("start", "zachod", 0, 0);
    add_exit("balkon", ({"poludnie", "na balkonik", "z korytarza"}), 0, 0);
    add_exit("balkon", ({"balkonik", "na balkonik", "z korytarza"}), 0, 0);
    add_exit("sala", ({ "d", "po schodach na d^o^l", "po schodach z g^ory" }), wyjdz, 0);
    add_exit("sala", ({ "schody", "po schodach na d^o^l", "po schodach z g^ory" }), wyjdz, 0);
//    add_exit("strych", ({ "gora", "po drabince na strych" }), strych, 0);
}
public string
exits_description() 
{
    return "Wej^scie na balkon jest na po^ludniu, na zachodzie znajduje "+
           "si^e jeden z pokoi karczemnych. S^a tutaj tak^ze drzwi, "+
           "id^ac za^s na d^o^l trafisz do g^l^ownej sali.\n";
}

void init()
{
    ::init();
    init_peek();
}

string
sprawdz_opis_okna()
{
    if (present("okno", TO)->query_open())
        return "otwartym";

    return "zamkni�tym";
}

int
wyjdz()
{
    write("Pod^a^zasz schodami na d^o^l.\n");
    return 0;
}
int
strych()
{
    write("Pod^a^zasz po drabince na strych.\n");
    return 0;
}

string 
query_opis_klapy2()
{
    if (present("klapa", TO)->query_open())
        return "dziur�";

    return "klap�";
}
