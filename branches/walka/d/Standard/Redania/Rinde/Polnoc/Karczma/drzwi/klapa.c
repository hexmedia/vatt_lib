inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    set_open(0);
    ustaw_nazwe( ({"klapa", "klapy","klapie","klap^e","klap^a","klapie"}),
            ({"klapy","klap","klapom","klapy","klapami","klapach"}), PL_ZENSKI);

    dodaj_przym("drewniany", "drewniani");
    set_other_room(KARCZMA_LOKACJE + "strych.c");
    set_door_id("klapa_na_strych");
    set_door_desc("Jest to @@query_opis_klapy:"+file_name(this_object())+"@@ drewniana klapa w suficie.\n");
    set_open_desc("");
    set_closed_desc("");
    set_pass_command( ({"gora","na g�r� po szczeblach w �cianie",
                "z do�u przez otwart� klap�"}));
    //set_pass_mess( ({"na g�r� po szczeblach w �cianie",
    //                   "Pod��asz na g�r� po szczeblach w �cianie.\n"}) );
    set_fail_pass("Drewniana klapa w suficie jest zamkni^eta.\n");
    set_open_command("otw^orz");
    set_close_command("zamknij");
    set_open_mess( ({"otwiera klap^e.\n", "Kto^s otwiera klap^e.\n",
                "Otwierasz klap^e.\n" }) );
    set_fail_open( ({"Klapa jest juz otwarta.\n", "Klapa jest zamkni^eta od drugiej strony.\n",}) );
    set_close_mess( ({"zamyka klap^e.\n", "Kto^s zamyka klap^e.\n",
                "Zamykasz klap^e.\n" }) );
    //    set_unlock_mess(({"a\n", "b\n"}));
    set_fail_close("Klapa jest ju^z zamkni^eta.\n");
}

string 
query_opis_klapy()
{
    if (query_open())
        return "otwarta";

    return "zamkni^eta";
}
