/*
 * Jest to kominek do karczmy Baszta w Rinde.
 * Jest bardzo prowizoryczny. I mo�e obs�ugiwa� go tylko karczmarz
 * gospody.
 * S� tu tylko eventy i alarmy na nie ;)
 *                                               Vera, 06.06.06
 */
inherit "/std/object";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include "dir.h"

int ile_pali=0,pali_sie=0;

nomask void
create_object()
{
    ustaw_nazwe("kominek");
    dodaj_przym("niewielki","niewielcy");
    set_long("Kominek w zasadzie nie jest traktowany jako g��wne �r�d�o ciep�a " +
            "w pomieszczeniu, poniewa� jest zbyt ma�y. S�u�y przede wszystkim "+
	    "ozdobie - kominek daje " + 
            "dodatkowe �wiat�o o ciep�ym widmie barwnym (dzi�ki niemu wi�c, podobnie " +
            "jak i dzi�ki wiecom - kobiety zyskuj� na atrakcyjno�ci). Kominek " +
            "poprzez gr� p�omieni, weso�y trzask wysuszonych drewienek oraz zapach " +
            "�ywiczny rozchodz�cy si� z p�on�cych szczap wprowadza nowy, atrakcyjny " +
            "element zmienny w dekoracji wn�trza. @@czy_plone_w_longu@@\n");
	    
    setuid();
    seteuid(getuid());
//    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(OBJ_I_VOLUME, 16800);
    add_prop(OBJ_I_NO_GET, "Nie da si� go podnie��.\n");
    add_prop(OBJ_I_WEIGHT, 10000);
//    add_prop(CONT_I_MAX_WEIGHT, 20000);
    /* dodajemy sublokacje */
//    add_subloc("do");
    /* okre�lamy limit obj�to�ci w sublokacji 'pod' */
//    add_subloc_prop("do", CONT_I_MAX_VOLUME, 50000);
    /* okre�lamy limit wagi w sublokacji 'na' */
    //add_subloc_prop("na", CONT_I_MAX_WEIGHT, 500);
    /* na meblu tylko sztylety i topory mo�na k�a�� */
    //add_subloc_prop("na", SUBLOC_I_DLA_O, O_BRON_SZTYLETY | O_BRON_TOPORY);

    /* sami opisujemy kontener */
//    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    set_type(O_MEBLE);
}

string
czy_plone_w_longu()
{
    string str;
    if(ile_pali>15)
        str="W tej chwili pali si� do�� du�ym ogniem.";
    else if(ile_pali>10)
        str="W tej chwili pali si� w nim ogie�.";
    else if(ile_pali>5)
        str="W tej chwili pali si� w nim ma�y ogie�.";
    else if(ile_pali>0)
        str="W tej chwili pali si� w nim skromny ogie�.";
    else
        str="W tej chwili jest nieczynny.";
        
    return str;
}

void
init()
{
    ::init();
    add_action("dokladamy","do��");
    add_action("dokladamy","pod��");
}

int
dokladamy(string str)
{
    mixed kominek_ob;
    object *co;
    //co=filter(co,all_inventory(TP));
    
    notify_fail(capitalize(query_verb())+" co do czego?\n");
    
    if(!stringp(query_verb()))
        return 0;
    if(!strlen(str))
        return 0;
        
   co=INV_ACCESS(co);
   if(!parse_command(str, environment(TP),
        "%i:"+PL_BIE+" 'do' %i:"+PL_DOP, co, kominek_ob))
        return 0;     
        
        
    //FIXME to nie jest najlepszy pomys�...Jaki� inny living mo�e te�
    //      mie� tak na imi� -_- ... No, ale lepszego pomys�u nie mam
    //      p�ki co.    
    if(find_living("damir"))
    saybb(QCIMIE(this_player(), PL_MIA) + " pr�buje wrzuci� co� do kominka, ale "+
          "karczmarz "+TP->koncowka("go","j�")+" powstrzymuje.");
    find_living("damir")->command("powiedz Prosz� nie wrzuca� niczego "+
                                  "do kominka!");
    return 1;
}


jestem_komineczkiem_w_karczemce()
{
    return 1;
}

void eventy()
{
    string str;
    switch(random(7))
    {
        case 0: str="P�omie� w kominku grzeje przyjemnie."; break;
        case 1: str="Ogniste j�zyki w kominku skacz� figlarnie."; break;
        case 2: str="Drewno w kominku trzaska pod wp�ywem ciep�a."; break;
        case 3: str="Z kominka dochodz� twych uszu odg�osy trzaskaj�cych "+
                    "ga��zek."; break;
        case 4: str="Czujesz nagrzane przez kominek powietrze."; break;
        case 5: str="Ciep�o bij�ce z kominka grzeje ci� przyjemnie."; break;
        case 6: str="Powoli robi si� coraz cieplej."; break;
    }
    str+="\n";
    
    if(pali_sie)
        tell_room(environment(TO),str);
    
    float f;
    switch(random(10))
    {
        case 0:f=80.0;break;
        case 1:f=120.0;break;
        case 2:f=150.0;break;
        case 3:f=200.0;break;
        case 4:f=240.0;break;
        case 5:f=300.0;break;
        case 6:f=360.0;break;
        case 7:f=400.0;break;
        case 8:f=430.0;break;
        case 9:f=660.0;break;
    }    
    ile_pali++;
    
    if(ile_pali>20)
    {
        tell_room(environment(TO),"Ogie� w kominku powoli przygasa.\n");
        pali_sie=0;
        ile_pali=0;
    }    
    else
        set_alarm(f,0.0,"eventy");
}
    

void
palimy()
{
    if(!ile_pali) 
        ile_pali=1;
    pali_sie=1;
    set_alarm(2.0,0.0,"eventy");
}

int
query_ile_pali()
{
    return ile_pali;
}
int
query_pali_sie()
{
    return pali_sie;
}
