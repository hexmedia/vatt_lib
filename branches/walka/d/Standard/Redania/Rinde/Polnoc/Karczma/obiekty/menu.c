/* Menu karczmy Baszta.
   Lil 06.07.2005               */

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
int przeczytaj_menu();

create_object()
{
    ustaw_nazwe("tabliczka");
    dodaj_nazwy("tablica");
    dodaj_nazwy("menu");

    set_long("     _________________________________________\n    / o\t\t       MENU\t\t    o \\\n   |"+
                      "\t\tDania g^l^owne:\t\t      |\n"+
                      "   |\tJajecznica z serem\t3 grosze      |\n"+
                      "   |\tRy^z\t\t\t6 groszy      |\n"+
		      "   |\tBigos\t\t\t3 groszy      |\n"+
		      "   |\tSa^latka owocowa\t\t5 groszy      |\n"+
		      "   |\tZupa pomidorowa\t\t9 groszy      |\n   |\t\t\t\t\t      |\n"+
		      
		      "   |\t\tZak^aski:\t\t      |\n"+
		      "   |\tKie^lbasa\t\t5 groszy      |\n"+
		      "   |\tChleb\t\t\t6 groszy      |\n   |\t\t\t\t\t      |\n"+
		      
		      
		      "   |\t\tNapoje:\t\t\t      |\n"+
		      "   |\tMleko\t\t\t3 grosze      |\n"+
		      "   |\tSok wi�niowy\t\t4 grosze      |\n"+
		      "   |\tWoda\t\t\t1 grosze      |\n"+
		      "   |\tJasne piwo\t\t10 groszy     |\n"+
		      "   |\t w beczu^lce 10l\t        180 groszy    |\n"+
		      "   |\tReda^nskie piwo\t\t12 groszy     |\n"+
		      "   |\tCzerwone wino\t\t6 groszy      |\n"+
		      "   |\t w buk^laku 1,5l\t        37 groszy     |\n"+
		      "   |\tJab^lecznik\t\t6 grosze      |\n"+
		      "   |\t w buk^laku 2l\t        44 groszy     |\n"+
		      "   |\tW^odka\t\t\t1 groszy      |\n"+
		      "   | o\t w buk^laku 1l\t        30 groszy   o |\n"+
		      "   \\_________________________________________/\n"
		      
		      
		      
		      
		      
		      
		      );

    add_prop(OBJ_I_WEIGHT, 485);
    add_prop(OBJ_I_NO_GET, "Tabliczka zosta^la mocno przybita do kontuaru.\n");
    add_prop(OBJ_I_DONT_GLANCE,1);

    add_cmd_item(({"menu","tabliczk^e"}), "przeczytaj", przeczytaj_menu,
                   "Przeczytaj co?\n"); 
}

int
przeczytaj_menu()
{
   return long();
}
