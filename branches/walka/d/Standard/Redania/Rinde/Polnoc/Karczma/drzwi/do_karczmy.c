inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe( ({"drzwiczki", "drzwiczek","drzwiczkom",
                "drzwiczki","drzwiczkami","drzwiczkach"}), PL_NIJAKI_OS);

    dodaj_przym("ma�y", "mali");
    dodaj_przym("niski", "niscy");
    set_other_room(KARCZMA_LOKACJE + "sala.c");
    set_door_id("drzwiczki_na_podworze");
    set_door_desc("Drewniane drzwiczki, do^s^c niskie i ma^le.\n");
    set_open_desc("");
    set_closed_desc("");
    set_pass_command(({"drzwiczki", "do karczmy", "z podw�rza"}));
    //set_pass_mess("przez drzwiczki do karczmy");
    set_fail_pass("Drzwiczki s^a zamkni^ete.\n");
    set_open_command("otworz");
    set_close_command("zamknij");
    set_open_mess( ({"otwiera drzwiczki do karczmy.\n", 
                "Kto^s otwiera drzwiczki z drugiej strony.\n",
                "Otwierasz niskie ma^le drzwiczki.\n" }) );
    set_fail_open("Drzwiczki s^a ju^z otwarte.\n");
    set_close_mess( ({"Zamyka drzwiczki prowadz^ace do karczmy.\n",
                "Kto^s zamyka drzwiczki z drugiej strony.\n",
                "Zamykasz drzwiczki.\n" }) );
    set_fail_close("Drzwiczki s^a ju^z zamkni^ete.\n");
}
