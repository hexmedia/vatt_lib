inherit "/std/door";

#include <pl.h>
#define KOD_DRZWI "gorakarczmy"
#include "dir.h"

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami", 
        "drzwiach" }), PL_NIJAKI_OS); 
        
    dodaj_przym("drewniany", "drewniani");
    
    set_other_room(KARCZMA_LOKACJE + "pokoj.c");
                                                                                       
    set_door_id(KOD_DRZWI);
    
    set_door_desc("Masz przed soba drewnine drzwi. Niezbyt dobrze zbita "+
       "kupa desek i nic wiecej. Nie spostrzegasz niczego, co sluzyloby "+
       "za zamek. Te drzwi pewnie sa zamykane tylko od jednej strony - "+
       "tej przeciwnej.\n");
    
    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({"drzwi","przez drzwi do pokoju","z zewn�trz"}));
    //set_pass_mess("przez drewniane drzwi na p�noc");
    
    //FIXME
    set_locked(1);
    set_open(0);
    set_pick(88);
}
