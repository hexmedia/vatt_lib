//opis: rina. wywalilem bledy i nie zostalo prawie nic.

inherit "/std/container.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>
#include <mudtime.h>
#include <composite.h>
#include <sit.h>

#include "dir.h"

void
create_container()
{
    ustaw_nazwe("biurko");
    dodaj_przym("du^zy", "duzi");
    dodaj_przym("mahoniowy", "mahoniowi");
    
    set_long("@@dlugi_opis@@");

    make_me_sitable("na", "na du^zym mahoniowym biurku",
        "z du^zego mahoniowego biurka", 2, PL_MIE, "na");

    ustaw_material(MATERIALY_DR_MAHON);
    
    add_prop(OBJ_I_WEIGHT, 80000);
    add_prop(OBJ_I_VOLUME, 13000);
    add_prop(CONT_I_MAX_VOLUME, 30000);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_MAX_WEIGHT, 100000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    add_subloc("na");
    
    add_object(KARCZMA_OBIEKTY + "swiecznik_pokoj", 1, 0, "na");
    set_owners(({DAMIR,RINDE_NPC+"pomagacz_damira"}));
}

public int
query_type()
{
    return O_MEBLE;
}

string
dlugi_opis()
{
    string str = "Ciemnobr^azowy, do b^lysku wypolerowany blat z mahoniu " +
    "jest w doskona^lym stanie. Lekko zaokr^aglonym kraw^edziom biurka uksz" +
    "ta^ltowano w delikatnie faluj�ce linie. ";

    switch (sizeof(all_inventory(this_object())))
    {
        case 0:
            str += "\n";
            break;
            
        case 1:
            str += "Le^zy na nim " +
            this_object()->subloc_items("na")[..-1] + ".\n";
            break;
            
        default:
            str += "Le^z^a na nim " +
            this_object()->subloc_items("na")[..-1] + ".\n";
            break;
    }
    
    return str;
}
