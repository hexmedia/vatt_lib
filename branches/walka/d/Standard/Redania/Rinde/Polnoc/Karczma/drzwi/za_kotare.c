inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    set_open(0);
    ustaw_nazwe( ({"kotara", "kotary","kotarze","kotare","kotara","kotarze"}),
            ({"kotary","kotar","kotarom","kotary","kotarami","kotarach"}), PL_ZENSKI);

    dodaj_przym("ci�ki", "ci�cy");
    set_other_room(KARCZMA_LOKACJE + "kotara.c");
    set_door_id("kotara_w_sali");
    set_door_desc("Masz przed sob� ci�k� kotar�.\n");
    set_open_desc("");
    set_closed_desc("");
    set_pass_command(({ "kotara", "za kotar�",
                "z sali g��wnej"}));
    set_pass_mess("za kotar�");
    set_fail_pass("Odsu� najpierw kotar�.\n");
    set_open_command("odsu�");
    set_close_command("zasu�");
    set_open_mess( ({"odsuwa kotar�.\n", "Kto� odsuwa kotar� z drugiej strony.\n",
                "Odsuwasz kotar�.\n" }) );
    set_fail_open("Kotara jest ju� odsuni�ta.\n");
    set_close_mess( ({"zasuwa kotare.\n", "Kto� zasuwa kotar� z drugiej strony.\n",
                "Zasuwasz kotar�.\n" }) );
    set_fail_close("Kotara jest ju� zasuni�ta.\n");
}
