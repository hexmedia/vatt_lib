inherit "/std/door";

#include <pl.h>
#define KOD_DRZWI "gorakarczmy"
#include "dir.h"

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami", 
        "drzwiach" }), PL_NIJAKI_OS); 
        
    dodaj_przym("drewniany", "drewniani");
    
    set_other_room(KARCZMA_LOKACJE + "gora.c");
                                                                                       
    set_door_id(KOD_DRZWI);
    
    set_door_desc("Masz przed soba drewnine drzwi. Niezbyt dobrze zbita "+
       "kupa desek i nic wiecej. Zuwazasz zasuwke, ktora mozesz "+
       "zamknac drzwi.\n");
    
    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({"drzwi","przez drzwi na zewn�trz","z pokoju"}));
    //set_pass_mess("przez drewniane drzwi na po�udnie");
    
    set_lock_command("zamknij");                                                       
    set_unlock_command("otw�rz");

    set_lock_mess( ({ "zasuwa zasuwke w drewnianych drzwiach.\n", 
        "Slyszysz jakis szczek po drugiej stronie drzwi... jakby ktos " +
        "przesuwal zasuwke?\n", "Zasuwasz zasuwke w drewnianych drzwiach.\n"
}));

    set_unlock_mess( ({ "odsuwa zasuwke w drewnianych drzwiach.\n", 
         "Slyszysz jakis szczek po drugiej stronie drzwi... jakby ktos " +
         "przesuwal zasuwke?\n", "Odsuwasz zasuwke w drewnianych drzwiach.\n"
}));
        
/* Nazwa zamka w bierniku, bo sluzy wylacznie do komendy obejrzyj.
 */
    set_lock_name("zasowke"); 
    set_lock_desc("Ohh, Zwykla zasuwka... taka jak w kazdym przykladzie. "+
       "Z pewnoscia nie jest w stanie za duzo wytrzymac.\n");                          
       
    //set_locked(1);
    //set_pick(88);
}
