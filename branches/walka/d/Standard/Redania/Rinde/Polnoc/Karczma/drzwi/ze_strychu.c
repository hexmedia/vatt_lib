inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    set_open(0);
    ustaw_nazwe( ({"klap^e", "klapy","klapie","klap^e","klap^a","klapie"}),
            ({"klapy","klap","klapom","klapy","klapami","klapach"}), PL_ZENSKI);

    dodaj_przym("drewniany", "drewniani");
    set_other_room(KARCZMA_LOKACJE + "gora.c");
    set_door_id("klapa_na_strych");
    set_door_desc("Drewniana klapa w pod�odze.\n");
    set_open_desc("");
    set_closed_desc("");
    set_pass_command( ({"d","na d� przez otwart� klap�",
                "z g�ry"}));
    //set_pass_mess("na d� przez otwart� klap�");
    set_fail_pass("Otw^orz najpierw klap^e.\n");
    set_open_command("otw�rz");
    set_close_command("zamknij");
    set_open_mess( ({"otwiera klap^e.\n", "Klapa w suficie otwiera si^e\n",
                "Otwierasz klape.\n" }) );
    set_fail_open("Klapa jest ju� otwarta.\n");
    set_close_mess( ({"zamyka klap^e.\n", "Z g�ry s�ycha� jakie� ha�asy.\n", "Zamykasz klap^e.\n" }) );
    set_fail_close("Klapa jest ju^z zamkni^eta.\n");

    set_lock_command("zamknij");
    set_unlock_command("otw�rz");
    set_lock_mess( ({ "zamyka klap� na haczyk.\n", 
                "", "Zamykasz klap� na haczyk.\n"}));
    set_unlock_mess( ({ "wyci�ga haczyk i odblokowuje klap�.\n", 
                "", "Wyci�gasz haczyk i odblokowujesz klap�.\n"}));
    set_lock_name("haczyk");
    set_lock_desc("Joseph Schelling.\n");
}
