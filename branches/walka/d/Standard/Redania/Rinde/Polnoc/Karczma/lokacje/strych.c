/* Super tajny strych:)
            Lil, Gregh        */
	    
	    
	    
//EVENT: Gdzie� z k�ta dochodzi czasem chrobotanie...myszy?

inherit "/std/room";
inherit "lib/peek";

#include "dir.h"
#include <filter_funs.h>
#include <ss_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <pl.h>
#include <options.h>
#include <formulas.h>

int wyjdz();

string query_opis_klapy();
int query_open_klapy();

string sprawdz_opis_okienka();

void
create_room()
{
   set_short("Strych");
   set_long("Stare graty poupychane w rogach, jak i niski, spadzisty " +
                "dach czyni� to miejsce znacznie mniejszym od tych "+
		"znajduj�cych si� na parterze gospody. "+
			
		"@@sprawdz_opis_okienka@@ "+
		
		"jedynego na strychu doj�cia �wie�szego powietrza"+
		"@@query_opis_klapy@@ "+
		
	"Wyczuwalna jest tutaj charakterystyczna wo� starego drewna. "+
	"Wszystko dooko�a pokrywa warstwa kurzu: od starego "+
	"dywanu na pod^lodze po zwisaj^ace ze stropu liczne paj�czyny.\n");

   add_peek("przez okienko", POLNOC_LOKACJE + "ulica4.c");
   add_sit("na dywanie", "na starym, zakurzonym dywanie",
               "ze starego dywanu", 0);
   add_object(KARCZMA_DRZWI+"ze_strychu.c");
   add_prop(ROOM_I_INSIDE,1);
   add_object(KARCZMA_OBIEKTY+"okienko_ze_strychu");
}

public string
exits_description() 
{
    return "Klapa w pod^lodze prowadzi na d^o^l.\n";
}
int
wyjdz()
{
    write("Pod^a^zasz schodami na d^o^l.\n");
    return 0;
}

string 
query_opis_klapy()
{
    if (present("klapa", TO)->query_open())
        return ", ni� to pochodz�ce z otwartej klapy.";

    return ".";
}

int
query_open_klapy()
{
    if (present("klapa", TO)->query_open())
        return 0;

    return 1;
}

void init()
{
    ::init();
    init_peek();
}

string
sprawdz_opis_okienka()
{
    if (present("okienko", TO)->query_open())
        return "Cichy gwizd co pewien czas rozlega si� "+
	          "po pomieszczeniu wraz z delikatnym powiewem wiatru, "+
		  "gdy ten wedrze si� tutaj przez otwarte okienko," ;

    return "Wiatr co pewien czas stuka szyb^a zamkni�tego okienka,";
}
