/* U�ywany:
 * -(m.in?) w startowej lokacji karczmy Baszta, Rinde.
 */

inherit "/std/container";

#include "dir.h"
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_container()
{
    ustaw_nazwe("st�");
    dodaj_przym("spory", "sporzy");
    dodaj_przym("d�bowy", "d�bowi");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Sporej wielko�ci solidny st�, poznaczony jest wieloma " +
            "bliznami - �ladami po ci�ciach, uderzeniach, zadrapaniach. " +
            "Najwidoczniej kto� u�ywa� go do pracy, lub do karczemnych bijatyk, "+
            "co wydaje si� do�� prawdopodobne z uwagi na stan mebla.\n"+
            "Dostrzegasz na nim star� ksi�g�"+
            "@@opis_sublokacji| oraz |na|||" + PL_BIE+ "@@"+
            ".\n");

    add_prop(CONT_I_WEIGHT, 81543);
    add_prop(CONT_I_VOLUME, 112531);
    add_prop(OBJ_I_VALUE, 60);
    add_prop(OBJ_M_NO_GET,"St� o dziwo ani drgnie! Zdaje si�, �e "+
            "jest na sta�e przybity do pod�ogi!\n");

    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 115000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 215000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    set_owners(({DAMIR,RINDE_NPC+"pomagacz_damira"}));
}
