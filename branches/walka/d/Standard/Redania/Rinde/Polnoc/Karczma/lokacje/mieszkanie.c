inherit "/std/room";
#include "dir.h"

#include <macros.h>
#include <stdproperties.h>

void
create_room()
{
 set_short("Sypialnia gospodarza.");
 set_long("W sypialni gospodarza jak to w sypialni gospodarza. Jak jest ka^zdy widzi.\n");
    add_prop(ROOM_I_INSIDE,1);
 add_exit("kuchnia", "wschod", 0, 0);
}
public string
exits_description() 
{
    return "Na wschodzie znajduje si^e wej^scie do kuchni.\n";
}

