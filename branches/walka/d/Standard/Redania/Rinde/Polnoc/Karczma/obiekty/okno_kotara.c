/* LIL< NIEDOKONOCZZZZZZZNEEE
FIXME FIXME FIXME
TODO TODO TODO

inherit "/std/window";
inherit "/lib/peek";

#include <pl.h>
#include <macros.h>
#define KOD_OKNA "takiesobieokienkowkarczmiebasztawmiescierindegdziejestfajnie"
#include "dir.h"
int wyskocz_przez_okno();
void
create_window()
{
    ustaw_nazwe( ({ "okno", "okna", "oknu", "okno", "oknem", 
        "oknie" }), ({ "okna", "okien", "oknom", "okna", "oknami", "oknach" }),
	PL_NIJAKI_OS); 
        
    dodaj_przym("niedu�y", "nieduzi");   
  //  add_cmd_item(({"przez okno"}), "wyskocz", wyskocz_przez_okno,
   //                            "Przez co chcesz wyskoczy^c?\n");
    add_peek("przez okno", RINDE + "polnoc/karczma/podworze.c");                                     
    set_window_id(KOD_OKNA);
    set_open(0);
    set_open_desc("");
    set_closed_desc("");
    set_window_desc("Niedu�e okno.\n");
    set_other_room(RINDE+"polnoc/ulica3");
    set_door_id("okno_za_kotara");
    set_door_desc("Jest do niedu�e, zwyk�e okno.\n");
    set_open_desc("");
    set_closed_desc("");
    set_pass_command( ({"wyjd� przez okno","wyjd� przez niedu�e okno" }));
    set_pass_mess("przez okno.\n");
    set_fail_pass("Otw�rz wpierw okno.\n");
    set_open_command("otw�rz");
    set_close_command("zamknij");
    set_open_mess( ({"otwiera okno.\n", "Kto^s otwiera karczemne okno.\n",
           "Otwierasz okno.\n" }) );
    set_fail_open("Okno jest ju� otwarte.\n");
    set_close_mess( ({"zamyka okno.\n", "Kto^s zamyka karczemne okno.\n",
         "Zamykasz okno.\n" }) );
    set_fail_close("Okno jest ju� zamkni�te.\n");
}



void init()
{
    ::init();
    init_peek();
    add_action("wyskocz_przez_okno","wyskocz");
}


int
wyskocz_przez_okno()
{
   
   object *okna;
   
   okna = filter(all_inventory(environment( this_player() ) ),
             &operator(!=)(0,) @ &->query_window_desc());
   okna = filter(okna, &operator(!=)(0,) @ &->query_open());
   
   notify_fail("Przez co chcesz wyskoczy�?");
   
   if (!sizeof(okna))
   {
      notify_fail("Otw^orz najpierw okno.\n");
      return 0;
   }
   
   write("Podchodzisz do okna i wyskakujesz przez nie.\n");
   say(QCIMIE(this_player(), PL_MIA) + " wyskakuje przez okno!\n");
   tell_room(RINDE + "polnoc/karczma/podworze", 
          QCIMIE(this_player(), PL_MIA) + " spad^l"+
	  this_player()->koncowka("", "a", "o") +" w^la^snie na ziemi^e!\n");
      this_player()->set_hp(this_player()->query_hp()-66-random(48));
   this_player()->move_living("M",RINDE+"polnoc/karczma/podworze",1,0);
	     
   return 1;
}