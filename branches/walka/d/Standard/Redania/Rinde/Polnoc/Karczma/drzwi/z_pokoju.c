inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami", 
                "drzwiach" }), PL_NIJAKI_OS);
    dodaj_przym("drewniany", "drewniani");
    set_other_room(POLNOC + "karczma/lokacje/gora.c");
    set_door_id("drzwi_do_pokoju");
    set_door_desc("Masz przed soba drewnine drzwi. Niezbyt dobrze zbita "+
            "kupa desek i nic wiecej. Zuwazasz zasuwke, ktora mozesz "+
            "zamknac drzwi.\n");
    set_open_desc("Na po�udniu s� otwarte drzwi.\n");
    set_closed_desc("Na po�udniu s� zamkni�te drzwi.\n");
    set_pass_command(({"s","na po�udnie przez otwarte drzwi",
                "z p�nocy przez otwarte drzwi"}));
    //set_pass_mess("przez drewniane drzwi na po�udnie");
    set_lock_command("zamknij");
    set_unlock_command("otw�rz");
    set_lock_mess( ({ "zasuwa zasuwke w drewnianych drzwiach.\n", 
                "Slyszysz jakis szczek po drugiej stronie drzwi... jakby ktos " +
                "przesuwal zasuwke?\n", "Zasuwasz zasuwke w drewnianych drzwiach.\n"}));
    set_unlock_mess( ({ "odsuwa zasuwke w drewnianych drzwiach.\n", 
                "Slyszysz jakis szczek po drugiej stronie drzwi... jakby ktos " +
                "przesuwal zasuwke?\n", "Odsuwasz zasuwke w drewnianych drzwiach.\n"}));
    set_lock_name("zasowke");
    set_lock_desc("Ohh, Zwykla zasuwka... \n");
}
