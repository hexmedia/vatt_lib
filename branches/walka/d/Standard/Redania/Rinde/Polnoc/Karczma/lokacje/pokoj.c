//opis: Rina

inherit "/std/room";
inherit "lib/peek";
#include <macros.h>
#include <stdproperties.h>
#include "dir.h"

object drzwi;
int otworz_okno();

void
create_room()
{
   set_short("Zaciszny pok^oj");
   set_long("@@dlugi_opis@@");
   add_prop(ROOM_I_INSIDE,1);
   add_object(KARCZMA_DRZWI + "pokoj"); 
   add_peek("przez okno", KARCZMA_LOKACJE + "podworze.c");   
   add_cmd_item("okno", "otw^orz", otworz_okno);
   add_cmd_item(({"lamp^e", "oliwn^a lamp^e"}), "we^x", "Lampa jest zawieszona zbyt wysoko.\n");
   add_item("okno",
            "Okno z widokiem na szare pi^etrowe kamienice, z nad kt^orych "+
	    "po lewej stronie wychyla si^e wie^za ratuszowa. Nie dostrzegasz "+
	    "^zadnego uchwytu, ani sposobu, na kt^ory mo^znaby je otworzy^c.\n");

    add_item(({"lamp^e", "oliwn^a lamp^e"}),
    "Wykonany z br^azu sze^sciok^atny pojemnik na oliw�, gdzieniegdzie " +
    "pokryty zielonkawymi plamami patyny, podtrzymuje owalny, nieco " +
    "okopcony szklany klosz. Przez przymocowane do podstawy trzy ko^la " +
    "przeplata si^e pordzewia^ly ^la^ncuch, spi^ety u g^ory wygi^etym w " +
    "podkow^e uchwytem.\n");
    
    add_object(KARCZMA_OBIEKTY + "biurko");
    add_object(KARCZMA_OBIEKTY + "toaletka");
    add_object(KARCZMA_OBIEKTY + "kufer");
    add_object(KARCZMA_OBIEKTY + "krzeslo");
    add_object(KARCZMA_OBIEKTY + "lozko_wielkie");
    
    dodaj_rzecz_niewyswietlana("du^ze mahoniowe biurko");
    dodaj_rzecz_niewyswietlana("otwarty d^ebowy kufer");
    dodaj_rzecz_niewyswietlana("zamkni^ety d^ebowy kufer");
    dodaj_rzecz_niewyswietlana("smuk^le eleganckie krzes^lo");
    dodaj_rzecz_niewyswietlana("wielkie d^ebowe ^lo^ze");
    dodaj_rzecz_niewyswietlana("jesionowa toaletka");
}

public string
exits_description() 
{
    return "Wyj^scie z tego pokoju prowadzi na korytarz.\n";
}

int
otworz_okno()
{
   saybb(QCIMIE(this_player(), PL_MIA) + " podchodzi na chwile do okna "+
            "i spogl^ada na nie z zainteresowaniem, jakby pr^obuj^ac "+
            "je otworzy^c.\n");
   write("Niestety, okno nie posiada ^zadnego uchwytu, za kt^ory "+
         "m^og^l" + this_player()->koncowka("by^s","aby^s") + " poci^agn^a^c. "+
	 "W dodatku po chwili dostrzegasz metalowe ^lebki gwo^xdzi wbitych "+
	 "do futryny.\n");
   return 1;
}

string
dlugi_opis()
{
    string str = "Panuj^acy w pokoju p^o^lmrok spot^egowany jest " +
    "ciemnobr^azowymi kolorami wyposa^zenia. ";

    if (!dzien_noc())
        str += "Zaledwie kilka ostrych jak brzytwy promyk^ow s^lo^nca " +
        "wdziera si^e poprzez niedok^ladnie zas^loni^ete kotary. W ich ";
    else
        str += "Zawieszona tu^z nad drzwiami oliwna lampa daje wystarczaj^aco "+
        "du^zo ^swiat^la, by zepchn^a^c migocz^ace cienie w zakamarki " +
        "pomieszczenia. W jej ";

    str += "blasku leniwie ko^lysz^a si^e drobiny kurzu, obudzone lekkim " +
    "powiewem wiatru. Na pobielonych ^scianach i suficie ta^ncz^a weso�o " +
    "kolorowe refleksy ^swiat^la. Opr^ocz kurzu daje si^e odczu^c ulotny " +
    "zapach mydlin. ";

    if (jest_rzecz_w_sublokacji(0, "jesionowa toaletka"))
    {
        str += "Tu^z przy drzwiach, po lewej stronie stoi toaletka";
        if (jest_rzecz_w_sublokacji(0, "otwarty d^ebowy kufer") ||
            jest_rzecz_w_sublokacji(0, "zamkni^ety d^ebowy kufer"))
        {
            if (jest_rzecz_w_sublokacji(0, "wielkie d^ebowe ^lo^ze"))
                str += ", obok niej kufer, a tu^z za nimi wielkie ^lo^ze. ";
            else
                str += ", a tu^z obok niej niewielki kufer. ";
        }
        else
        {
            if (jest_rzecz_w_sublokacji(0, "wielkie d^ebowe ^lo^ze"))
                str += ", a tu^z za ni^a wielkie ^lo^ze. ";
        }
    }
    else
    {
        if (jest_rzecz_w_sublokacji(0, "otwarty d^ebowy kufer") ||
            jest_rzecz_w_sublokacji(0, "zamkni^ety d^ebowy kufer"))
        {
            if (jest_rzecz_w_sublokacji(0, "wielkie d^ebowe ^lo^ze"))
                str += "Tu^z przy drzwiach, po lewej stronie stoi " +
                "niewielki kufer, a tu^z za nim wielkie ^lo^ze. ";
            else
                str += "Tu^z przy drzwiach, po lewej stronie stoi " +
                "niewielki kufer. ";
        }
        else
        {
            if (jest_rzecz_w_sublokacji(0, "wielkie d^ebowe ^lo^ze"))
                str += "Tu^z przy drzwiach, po lewej stronie stoi " +
                "wielkie ^lo^ze. ";
        }
    }
    
    if (jest_rzecz_w_sublokacji(0, "du^ze mahoniowe biurko"))
    {
        if (jest_rzecz_w_sublokacji(0, "smuk^le eleganckie krzes^lo"))
            str += "Pod praw^a ^scian^a swoje miejsce znalaz^lo jedynie " +
            "poka^xnych rozmiar�w mahoniowe biurko i dostawione " +
            "do^n krzes�o. ";
        else
            str += "Pod praw^a ^scian^a swoje miejsce znalaz^lo jedynie " +
            "poka^xnych rozmiar�w mahoniowe biurko. ";
    }
    else
    {
        if (jest_rzecz_w_sublokacji(0, "smuk^le eleganckie krzes^lo"))
            str += "Pod praw^a ^scian^a swoje miejsce znalaz^lo jedynie " +
            "krzes^lo. ";
    }

    str += "\n";
    
    return str;
}

void init()
{
    ::init();
    init_peek();
}
   