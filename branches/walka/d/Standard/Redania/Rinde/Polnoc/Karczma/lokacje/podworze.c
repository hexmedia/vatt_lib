inherit "/std/room";
#include "dir.h"

#include <macros.h>
#include <stdproperties.h>

string sprawdz_opis_drzwiczek();

void
create_room()
{
    set_short("Podw^orze.");
    set_long("Nie znajdujesz tu niczego godnego uwagi. Z jednej strony "+
            "widnieje obdrapana �ciana karczmy z @@sprawdz_opis_drzwiczek@@ "+
            "ma�ymi drzwiczkami, natomiast z drugiej, kilkana�cie st�p "+
            "zaledwie od budynku stoi w�tpliwej budowy stodo�a, w kt�rej "+
            "dziura z wyrwanych kilku desek s�u�y za przej�cie do wewn�trz.\n");

    add_exit("stajnia", ({"stajnia", "do stajni","z podw�rza"}));

    add_object(KARCZMA_DRZWI+"do_karczmy.c");
}
public string
exits_description() 
{
    return "Znajduje si^e tutaj wej^scie do stajni.\n";
}

string 
sprawdz_opis_drzwiczek()
{
    if (present("drzwiczki", TO)->query_open())
        return "otwartymi";

    return "zamkni^etymi";
}

