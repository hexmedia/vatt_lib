/* Ot stol. Pierwszy. Z glownej sali.
                                lil   */

#pragma strict_types

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <materialy.h>
#include <cmdparse.h>

string czy_cos_jest_na_stole();

nomask void
create_container()
{
	make_me_sitable("przy","wygodnie przy du�ym stole",
					"od du�ego sto�u",6, PL_MIE, "przy");
    ustaw_nazwe("st�");

    dodaj_przym("du�y","duzi");

    set_long("Drewniany st�, ten wi�kszy.\n" + "@@czy_cos_jest_na_stole@@");
    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(CONT_I_VOLUME, 16800);

					//OBJ_I_NO_GET PONIEWA� JEST MAKE_ME_SITABLE
					//A NIE MA DO NIEGO KRZESE�..nIESTETY
	add_prop(OBJ_I_NO_GET, "Nie da si� go podnie��.\n");
    add_prop(CONT_I_CANT_WLOZ_DO, 1); /* By nie mo�na by�o nic do niego w�o�y� komend� 'wloz do' */

    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_MAX_WEIGHT, 20000);
    //add_prop(OBJ_I_DONT_GLANCE,1);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    /* dodajemy sublokacj� 'na', by mo�na by�o co� na st� k�a�� */
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 77700);
    set_owners(({DAMIR,RINDE_NPC+"pomagacz_damira"}));
	ustaw_material(MATERIALY_DR_DAB);
}

string
czy_cos_jest_na_stole()
{
   if (sizeof(all_inventory(this_object())) > 0)
       return "Na stole le�y: " + this_object()->subloc_items("na")[..-1] + ".\n";
   return "";
}

public int
jestem_stolem_karczmy()
{
        return 1;
}