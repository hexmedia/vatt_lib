#include <stdproperties.h>

inherit "/std/room";

void create_room() 
{
    set_short("Magazyn go^l^ebi");
    set_long("Magazyn go^l^ebi pocztowych.\n");

    add_prop(ROOM_I_INSIDE,1);

    add_object("/std/golab.c", 100);
}
