/* LIL */
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include <pogoda.h>
#include "dir.h"

inherit RINDE_STD;

string
koleiny()
{
    string str = "Koleist� ju� ulic� wci�� orz� wozy przeje�d�aj�ce t�dy, "+
	   "tworz�c pod�u�ne bruzdy";

    if (CZY_JEST_WODA(TO))
        str += "wype�nione w tej chwili ka�u�ami";
    str += ".\n";
    return str;
}

void create_rinde_street() 
{
	set_short("Ulica przed gospod�");
	add_exit("ulica3.c","p^o^lnocny-zach^od");
	add_exit("ulica5.c","po^ludnie");
	add_exit("ulica6.c","po^ludniowy-wsch^od");
//	add_exit("karczma/sala.c","karczma");
    add_object(POLNOC_DRZWI + "karczma");
	add_prop(ROOM_I_INSIDE,0);
    add_sit("na ziemi","na ziemi","z ziemi",0);
    add_sit("na stopniu","na stopniu do gospody","ze stopnia",2);
	
    add_item(({"ziemi�","utwardzon� ziemi�","ulic�","utwardzon� ulic�"}),
           "Wy��obiona licznymi koleinami ulica ci�gnie prosto jak strza�a "+
	   "od bramy p�nocnej, w kierunku bramy wschodniej, "+
	   "przecinaj�c w ten spos�b obrze�a miasta i znacznie odci��aj�c "+
	   "tym samym centrum miasta.\n");
    add_item(({"koleiny","pot�ne koleiny","koleiny na drodze","koleiny na ulicy",
           "pot�ne koleiny na drodze","pot�ne koleiny na ulicy"}),
           "@@koleiny@@");
    add_item("kamienice",
           "Ci�gn�ce si� wzd�u� ulicy kamienice zapewne "+
	   "pami�taja lepsze czasy. S� szare i maj� "+
	   "obdrapane �ciany.\n");
    add_item(({"gospod�","karczm�","najwi�ksz� budowl�"}),
           "Ta poka�na budowla ma dwa pi�tra. Parter ci�gni� si� przez "+
	   "ca�� d�ugo�� ulicy, a na po�udniowym wschodzie ko�czy si� "+
	   "dobudowan� stajni�. Do karczmy prowadz� dwa stopnie "+
	   "wyrastaj�ce niemal�e z samej ulicy. Skromny szyld ko�ysz�cy si� "+
	   "nad drzwiami zdradza nazw� \"Baszta\", a nad nim "+
	   "dostrzegasz balustrad�, zapewne karczemnego tarasiku dla go�ci.\n");
    add_item(({"dr�k�","w�sk� dr�k�"}), "Wiedzie ona na po�udnie, do centrum.\n");
    add_item(({"stopie�","stopnie","kr�tki stopie�","kr�tkie stopnie"}),
           "Dwa kr�tkie drewniane stopnie wystaj� tu� z ulicy i prowadz� do "+
	   "karczemnych drzwi.\n");
 
    add_object(KARCZMA_OBIEKTY+"szyld");

    add_object(RINDE+"przedmioty/lampa_uliczna");

    set_alarm_every_hour("co_godzine");
		
}
public string
exits_description() 
{
    return "Ulica prowadzi na p^o^lnocny-zach^od, po^ludnie oraz "+
           "po^ludniowy-wsch^od. Jest tutaj tak^ze wej^scie do karczmy.\n";
}


string dlugi_opis()
{
    string str;
    str="Poka�na pi�trowa budowla na obrze�ach miasta "+
        "stoi tu� przy ulicy. Mocarne z metalowymi "+
	"wyko�czeniami i zawiasami drzwi dziel� od "+
	"samej drogi jedynie dwa, kr�tkie stopnie. "+
	"Obok, po obu stronach maj� swoje miejsca dwa, "+
	"�rednich rozmiar�w okna, przez kt�re ";
    if(!dzien_noc())
      str+="mo�na dojrze� nieco karczemnego gwaru";
    else
      str+="s�czy si� �wiat�o, o�wietlaj�c do�� mocno ulic�";
      
    str+=". Na pi�trze, nad ko�ysz�cym si� szyldem, "+
         "ledwie widoczna st�d balustrada zdradza "+
	 "istnienie tarasu gospody. Utwardzona, "+
	 "koleista ulica ci�gnie si� prosto jak "+
	 "strza�a od p�nocnej bramy w stron� bramy "+
	 "wschodniej, odci��aj�c tym samym centrum "+
	 "miasta od przeje�d�aj�cych tu co rusz woz�w. "+
	 "Jest tu jeszcze jedna, bardzo w�ska dr�ka "+
	 "wiod�ca w stron� centralnego placu. Jak okiem "+
	 "si�gn��, wzd�u� ulicy znajduj� si� kamienice, "+
	 "a po drugiej stronie s�awna gospoda, z "+
	 "przybudowan� do� stajni� na po�udniowym "+
	 "wschodzie.";




    str+="\n";
    return str;
}

void
odnow_sonniego()
{
    LOAD_ERR(RINDE_NPC + "sonni.c");

    if(!present("sonni", TO) && (sizeof(object_clones(find_object(RINDE_NPC+"sonni.c"))) == 0))
    {
        object sonni;
        sonni =  clone_object(RINDE_NPC + "sonni.c");
        tell_roombb(TO, QCIMIE(sonni, PL_MIA)+" wychodzi "+
            "z jednej z kamienic.\n");
        sonni->init_arg(0);
        sonni->start_me();
        sonni->move(TO);
    }
}

void
co_godzine()
{
    if (MT_GODZINA == 16)
    {
        set_alarm(3000.0,0.0, "odnow_sonniego");
    }
}

/*
OBIEKTY:
mocarne drzwi
Wygl�daj� na solidn� robot�, acz sprawiaj� wra�enie jakby wiecznie nie domykaj�cych si�. Gospoda jest czynna ca�� dob� i w �wi�ta, dlatego te� tak mocne drzwi s�u�� chyba w g��wnej mierze jako ozdoba.

skromny szyld
Unosz�cy si� nad drzwiamy skromny, lekki szyld porusza si� co jaki� czas przy nawet najl�ejszym podmuchu wiatru. Na szcz�cie nie skrzypi, jak to maj� w zwyczaju wszystkie takowe szyldy, cho� jest uczepiony belki �a�cuchami. Na jego powierzchni, bez �adnych ozd�b napisano najzwyklejsz� w �wiecie czcionk�, du�ymi literami nazw� karczmy: BASZTA.

dwa okna
*/
