/* Lil & Gregh
   02.2005     */

inherit "/std/room";
inherit "lib/peek";

#include <macros.h>
#include <stdproperties.h>
#include "dir.h"



void
create_room()
{
 set_short("Sypialnia");
 set_long("Do^s^c sporych rozmiar^ow pomieszczenie urz^adzone "+
          "zosta^lo bardzo skromnie. Nie ma si^e co dziwi^c, skoro "+
	  "zamys^lem w^la^sciciela by^lo pewno to, by s^lu^zy^lo "+
	  "strudzonym w^edrowcom jedynie do odpoczynku. Tu^z pod "+
	  "^scianami ustawiono  w rz^edzie wygodne ^l^o^zka, a pod "+
	  "oknem przy zas^lonach - ma^l^a szafk^e nocn^a. W ^scianie "+
	  "przeciwnej natomiast znajduje si^e wyj^scie z pomieszczenia. "+
	  "Na ^srodku drewnianej posadzki ma swoje miejsce d^ebowy "+
	  "st^o^l, na kt^orym spoczywa jaka^s ksi^ega, obok kt^orej "+
	  "jest tak^ze misa Z WODA LUB NIE.\n");
   

   add_item(({"^lo^ze","^l^o^zko","^lo^za","^l^o^zka"}),
             "Wygodne lo^za umieszczono w rz^edzie przy szarych "+
	     "^scianach pomieszczenia. Wszystkie s^a za^scielone.\n");
   add_item("po^sciel",
            "Czysta, bia^la po^sciel.\n");
   add_prop(ROOM_I_INSIDE,1);
   add_peek("przez okno", KARCZMA_LOKACJE + "podworze.c");

   add_exit("gora", "wschod", 0, 0);
   add_sit("na ^l^o^zku", "na wygodnym ^l^o^zku", "z ^l^o^zka", 5);
   add_object(KARCZMA_OBIEKTY+"szafka_nocna");
}



void init()
{
    ::init();
    init_peek();
}
   
