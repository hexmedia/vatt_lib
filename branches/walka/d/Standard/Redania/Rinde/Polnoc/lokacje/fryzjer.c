/* Opis Tinardan. 
  Reszta Lil.  */

inherit "/std/room";

#include <stdproperties.h>
#include "dir.h"
#include <sit.h>
#include <object_types.h>
#include <pl.h>

void create_room() 
{
    set_short("elegancki salon fryzjerski");
    set_long("@@long_lok@@");
    add_exit("ulica5.c",({"wyj^scie","w kierunku wyj�cia",
                          "z zak�adu fryzjerskiego"}));


    add_prop(ROOM_I_INSIDE,1);

    
    
    add_item(({"obrazy","liczne obrazy","obrazy na �cianie",
               "liczne obrazy na �cianie"}),
	       "W�a�ciciel zak�adu trzyma te obrazy zawieszone "+
	       "tu� pod samym stropem sufitu, przez co "+
	       "nie mo�na przyjrze� si� im dok�adniej, a "+
	       "tym bardziej ich zdj��. Przedstawiaj� "+
	       "najprzer�niejsze sceny i pozycje fryzjer�w, "+
	       "pazi�w, artyst�w i krawc�w. Wszystkie istoty "+
	       "wyst�puj�ce na obrazach s� zadbanymi i elegancko "+
	       "przystrzy�onymi m�czyznami.\n");
    add_item("p�k�","Skromna drewniana p�ka trzyma si� na dw�ch "+
             "w�skich �a�cuszkach wystaj�cych ze �ciany"+
	     "@@opis_sublokacji|. Dostrzegasz na niej |na p�ce|||"+
	     PL_BIE+"@@"+".\n");
        
	       
    add_npc(RINDE_NPC + "fryzjer");

    add_object(RINDE+"przedmioty/lustro_fryzjer.c", 1, 0, "na �cianie");
    add_object(POLNOC_OBIEKTY+"katalog_fryzjera.c", 1, 0, "na p�ce");
//    dodaj_rzecz_niewyswietlana("ogromne wspania�e lustro",1);
    /* dodajemy sublokacj� typu 'na', jednak dosy� specyficzn� */
    add_object(RINDE+"przedmioty/meble/fotel_fryzjerski.c");
    dodaj_rzecz_niewyswietlana("fryzjerski sk�rzany fotel", 1);
    add_subloc("na �cianie", 0, "ze �ciany");
    add_subloc_prop("na �cianie", CONT_I_CANT_ODLOZ, 1);
    add_subloc_prop("na �cianie", CONT_I_CANT_POLOZ, 1);
    add_subloc_prop("na �cianie", SUBLOC_I_MOZNA_POWIES, 1);
    add_subloc_prop("na �cianie", SUBLOC_I_DLA_O, O_SCIENNE);
    add_subloc_prop("na �cianie", CONT_I_MAX_RZECZY, 1);
    
    add_subloc("na p�ce", 0, "z p�ki");
    add_subloc_prop("na p�ce", CONT_I_MAX_WEIGHT, 10500);
    //add_subloc_prop("na p�ce", CONT_I_CANT_ODLOZ, 1);
    //add_subloc_prop("na p�ce", CONT_I_CANT_POLOZ, 1);
    //add_subloc_prop("na p�ce", SUBLOC_I_MOZNA_POWIES, 1);
    //add_subloc_prop("na p�ce", CONT_I_MAX_RZECZY, 1);

}
public string
exits_description() 
{
    return "Wyj^scie z zak^ladu prowadzi na ulice.\n";
}



string long_lok()
{
   string str;
   
   int i, il, il_foteli;
   object *inwentarz;

   inwentarz = all_inventory();
   il_foteli = 0;
   il = sizeof(inwentarz);

   for (i = 0; i < il; ++i) {
         if (inwentarz[i]->jestem_fotelem_fryzjera()) {
                        ++il_foteli; }}

			
 /*  str = "nieeeeeeeeeeeeeeeeeeeeeeeeeeeeeePorz�dek panuj�cy w tym pomieszczeniu �wiadczy niew�tpliwie o "+
         "zaradno�ci jego w�a�ciciela.";
    
    if(dzien_noc()){
    str += "Du�e okna wpuszczaj� mn�stwo �wiat�a, za� na suficie przywieszono "+
           "kilka dodatkowych lamp. ";}
    else{
    str += "Poprzez du�e okna przedostaje si� odrobin� �wiat�a z zewn�trz. ";}
    
    //if(du�o przedmiot�w le�y na lokacji
    str += "Wsz�dzie panuje �ad i czysto��.";
    
    str += "Dosy� przestronny pok�j gustownie urz�dzono w odcieniach b��kitu. "+
           "B��kitne s� �ciany, b��kitne meble. ";
  
   if(this_player()->query_prop(SIT_SIEDZACY)){
   str += "Pod�og� wy�ciela bogato zdobiony, kunsztowny b��kitny dywanik. ";}
   else {str += "St�pasz po bogato zdobionym, kunsztownym b��kitnyn dywaniku. ";}
  
  
  if (il_krzesel == 0) {
  str += "Przy d�ugiej �cianie stoi solidna, b��kitna lada. ";}
  if (il_krzesel == 1) {
  str += "Przy d�ugiej �cianie stoi solidna, b��kitna lada wraz z jednym b��kitnym krzes�em. ";}
  if (il_krzesel == 2) {
  str += "Przy d�ugiej �cianie stoi solidna, b��kitna lada wraz z dwoma b��kitnymi krzes�ami. ";}
  if (il_krzesel >= 3) {
  str += "Przy d�ugiej �cianie stoi solidna, b��kitna lada wraz z trzema b��kitnymi krzes�ami. ";}
  
  str +="Polki scienne uginaja sie od nadmiaru przeroznych grzebieni, plynow, "+
  "kremow, masci, kosmetykow i flakonikow. Obok pokaznych luster wbito niewielka tabliczke.(???)";*/


  str="To pomieszczenie zosta�o urz�dzone z najwy�szym smakiem. "+
      "Poczynaj�c od bogatych rze�bie� belkowania, "+
      "przez liczne obrazy na idealnie g�adkiej �cianie, po "+
      "pod�og�, wy�o�on� r�wniutkimi p�ytkami wszystko l�ni "+
      "czysto�ci� i promieniuje mo�e tylko lekko przes�odzon� "+
      "elegancj�. ";
      
    if(subloc_items("na p�ce")=="")
      str+="Tu� obok drzwi wej�ciowych wisi na �cianie niewielka p�ka. ";
    else  
      str+="Tu� obok drzwi wej�ciowych wisi na �cianie p�ka, na kt�rej "+
       "dostrzegasz "+subloc_items("na p�ce")+". ";    
          
    if(subloc_items("na �cianie") == "ogromne wspania�e lustro")
      str+="Z lewej strony na �rodku �ciany znajduje si� "+
      "prawdziwe arcydzie�o - lustro o tak rewelacyjnie "+
      "wyg�adzonej tafli, �e nie mo�na dopatrzy� si� cho�by "+
      "jednego za�amania.";
       
    if(subloc_items("na �cianie") == "")
      str+="Z lewej strony na �rodku �ciany widoczny jest ja�niejszy "+
      "kontur sporych rozmiar�w, tak jakby w tym miejscu wisia�o "+
      "kiedy� co� du�ego. Na pod�odze pod �cian� walaj� si� "+
      "od�amki szk�a.";
    else if(subloc_items("na �cianie") != "" &&
          subloc_items("na �cianie") != "ogromne wspania�e lustro")
      str+="Z lewej strony na �rodku �ciany widoczny jest ja�niejszy "+
      "kontur sporych rozmiar�w, tak jakby w tym miejscu wisia�o "+
      "kiedy� co� du�ego. Teraz wisi na nim "+subloc_items("na �cianie")+
      ".";
      
    
    str+=" ";
        
    if(jest_rzecz_w_sublokacji(0, "fryzjerski sk�rzany fotel")) 
    {
        object fotel = filter(all_inventory(environment(this_player())),
         &->jestem_fotelem_fryzjera())[0];
            
       str+="Tu� obok stoi fryzjerski sk�rzany fotel, ";
       if(fotel->query_prop(OBJ_I_DONT_GLANCE) && fotel->query_prop(OBJ_M_NO_GET))
       {
           if(this_player()->query_prop(SIT_SIEDZACY) == fotel)
               str+="na kt�rym w�a�nie siedzisz.";
           else
               str+="w obecnej chwili zajmowany w�a�nie przez jakiego� klienta.";
       }
       else
         str+="w obecnej chwili wolny.";
    }  
    
    
    
 /*     "Fryzjer stoi z boku czyszcz�c starannie no�yce i grzebienie "+
      "najr�niejszej barwy i rozmiaru.";
       
       "Fryzjer biega dooko�a klienta, a jego r�ce i narz�dzie, "+
       "kt�re w nich trzyma zamieniaj� si� w chmur� srebrnych "+
       "b�ysk�w wok� g�owy klienta.";
       
       
       
  */
  


    str += "\n";
    return str;
}
/*
void
init()
{
    ::init();
    add_action(read_scroll, "przeczytaj");
}*/
