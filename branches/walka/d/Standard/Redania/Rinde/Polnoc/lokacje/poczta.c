/*   Alcyone & maruda Lil. 3.05 */

inherit "/std/room";
#include "dir.h"

#include <stdproperties.h>
#include <mail.h>
#include <macros.h>
#include <filter_funs.h> 

#define MAILCHECKER "/secure/mail_checker"
#define MAILREADER "/secure/mail_reader"
#define KRZESELKA RINDE+"przedmioty/meble/krzeslo_poczta"

string czy_duzo_istot();

void create_room() 
{
   set_short("Urz�d Pocztowy w Rinde");
   set_long("Intensywny zapach starego papieru wype�nia "+
	    "@@czy_duzo_istot@@ Drewnian� posadzk� dzieli "+
	    "d�uga, przykryta licznymi skrzynkami i kwitami "+
	    "lada, za kt^or^a krz^ata si^e pracownik poczty. "+
	    "@@reszta@@");
	    
   add_item("skrzynki",
            "Br�zowe, sporych rozmiar�w skrzynki ustawiono "+
	    "na ladzie w ten spos�b, �e zajmuj� niema�� "+
	    "powierzchni�.\n");
   /*add_item("stolik",
            "Przy �cianie stoi ma�y, kwadratowy stolik, przy "+
	    "kt�rym mo�esz usi��� i napisa� list.\n");*/
   /*add_item(({"krzes�a","krzes�o"}),
            "Trzy pary krzese� wykonanych z ciemnego drewna "+
	    "stoj� przy stoliku.\n");*/
   add_item(({"posadzk�","pod�og�"}),
            "Stara, drewniana posadzka skrzypi w niekt�rych "+
	    "miejscach pod naciskiem twych st�p.\n");
   add_item("lad�",
            "Si�gaj�ca niemal�e od �ciany do �ciany lada "+
	    "ugina si� lekko pod ci�arem skrzynek na� "+
	    "u�o�onych, zza kt�rych co jaki� czas wygl�da "+
	    "pracownik poczty.\n");
   add_item("tablic�",
            "Na niewielkiej tablicy wisi jedynie stary, "+
	    "przybrudzony pergamin.\n");

    add_cmd_item(({"pergamin", "stary pergamin", "pergamin na tablicy"}),
({"obejrzyj", "ob", "przeczytaj"}), "\"Go^l^ebie pocztowe do nabycia u pracownika poczty\".\n");
	    
    add_object(POLNOC_DRZWI+"z_poczty.c");
    dodaj_rzecz_niewyswietlana("drewniane drzwi", 1);
    add_object(KRZESELKA);
    add_object(KRZESELKA);
    add_object(KRZESELKA);
    add_object(KRZESELKA);
    add_object(KRZESELKA);
    add_object(KRZESELKA);
    add_object(RINDE+"przedmioty/meble/stolik_poczta");
    dodaj_rzecz_niewyswietlana("drewniane proste krzes�o", 6);
    dodaj_rzecz_niewyswietlana("ma�y kwadratowy stolik", 1);

    add_npc(RINDE_NPC + "pracownik_poczty");

   add_prop(ROOM_I_INSIDE,1);
 /*  add_sit("przy stoliku","przy stoliku odsuwaj�c wolne krzes�o",
           "od stolika",6);*/
}
public string
exits_description() 
{
    return "Wyj^scie z poczty prowadzi na ulic^e.\n";
}

	
init() 
{
  object obj;
  ::init();

  if (present(READER_ID, this_player()))
    return 1;
  if ((obj = clone_object(MAILREADER)) != 0)
    {
	obj->move(this_player());
    }
}

leave_inv(object who, object where) 
{
  object obj;

  if (who && 
      where && 
      where != this_object() &&
      (obj = present(READER_ID, who)) != 0)
    obj->remove_object();
  ::leave_inv(who, where);
}

int
jestem_poczta_w_rinde()
{
    return 1;
}

string
czy_duzo_istot()
{
  //if(sizeof(FILTER_LIVE(all_inventory(environment(this_player()))))>=5)
   if(sizeof(FILTER_LIVE(all_inventory()))>=5)
     return "pomieszczenie, w kt�rym panuj�cy t�ok jest "+
            "tak k�opotliwy, i� trudno jest ci znale�� "+
	    "jakie� wolne i ustronne miejsce.";
     
  return "to opustosza�e pomieszczenie.";
}


string 
reszta()
{
    string str;
    
    if(jest_rzecz_w_sublokacji(0,"ma�y kwadratowy stolik"))
    {
        str="Jedn� ze �cian podpiera ma�y drewniany "+
	    "stolik";
	if(jest_rzecz_w_sublokacji(0,"drewniane proste krzes�o"))
	{
	    str+=" z przysuni�tymi do� krzes�ami, a nad nim, "+
	    "przybity do tablicy wisi niewielki, "+
	    "przybrudzony pergamin.";
	}
	else
	    str+=", a nad nim, przybity do tablicy wisi niewielki "+
	         "przybrudzony pergamin.";
    }
    else
    {
        if(jest_rzecz_w_sublokacji(0,"drewniane proste krzes�o"))
        {
            str="Pod �cian� stoj� krzes�a, a nad nimi "+
                "przybity do tablicy wisi niewielki, "+
                "przybrudzony pergamin.";
        }
        else
            str="Na jednej ze �cian przybity do tablicy wisi "+
                "niewielki, przybrudzony pergamin.";
    }
    
    str+="\n";
    return str;    
}
	    
	    
