 
/* Lil. 1.05 */
#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;

void
create_rinde_street()
{
    set_short("Przed urz^edem pocztowym");
    set_long("Ulica prowadzi od p^o^lnocnej bramy na po^ludnie, w "+
        "kierunku g^l^ownego placu, z kt^orego wyrasta widoczna st^ad ju^z "+
        "smuk^la wie^za miejskiego ratusza. Wzd^lu^z ulicy ci^agn^a si^e "+
        "pi^etrowe kamieniczki, a w jednej z nich mie^sci si^e lokalny "+
        "urz^ad pocztowy, na kt^orego drzwiach wymalowano niestarannie "+
        "^z^o^lt^a tr^abk^e.\n");


    add_item(({"kamieniczki","kamienice"}),
        "S^a to pi^etrowe szare kamienice, kt^orych ^sciany, mimo kilku "+
        "bazgro^l^ow, wygl^adaj^a na do^s^c czyste.\n");
    add_item("^sciany",
        "^Sciany kamienic s^a oznakowane kilkoma krzywymi napisami.\n");
    add_item(({"napisy na ^scianie","krzywe napisy"}),
        "Niewyra^xne, cz^e^sciowo zamazane ju^z napisy, z kt^orych nie "+
        "potrafisz wyci^agn^a^c ^zadnego sensownego s^lowa.\n");
    add_item("ulic^e",
        "Brukowana ulica na kt^or^a spogl^adasz ma sw^oj pocz^atek u "+
        "p^o^lnocnej bramy i ci^agnie si^e przez centrum, a^z do po^ludniowej"+
        " cz^e^sci miasta.\n");
    add_item(({"poczt^e","urz^ad pocztowy"}),
        "Urz^ad ten mie^sci si^e w jednej z pi^etrowych kamieniczek "+
        "wybudowanych po obu stronach ulicy.\n");
    add_item(({"wie^z^e","smuk^l^a wie^z^e"}),
        "Smuk^la wie^za na po^ludniu wyrasta dumnie spo^sr^od kamienic, "+
        "niczym palec, wskazuj^acy centrum tego miasta.\n");

    add_exit("ulica2.c","p^o^lnoc");
    add_exit(CENTRUM_LOKACJE + "placnw.c","po^ludnie");
    
    add_prop(ROOM_I_INSIDE,0);
    add_sit("pod ^scian^a","pod ^scian^a","spod ^sciany",0);

    add_object(POLNOC_DRZWI+"na_poczte.c");

    add_object(RINDE+"przedmioty/lampa_uliczna");
}
public string
exits_description() 
{
    return "Ulica prowadzi na p^o^lnoc, za^s na po^ludniu znajduje si^e plac. "+
           "Jest tutaj tak^ze wej^scie na poczt^e.\n";
}

