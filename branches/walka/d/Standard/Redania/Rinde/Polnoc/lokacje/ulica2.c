/* made by Delvert & Lil & Wish 22.07.2005 */
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit RINDE_STD;

void create_rinde_street() 
{
	set_short("Ulica");
	set_long("@@dluuugi_opis@@");

	add_exit("braman.c","p^o^lnoc");
	add_exit("ulica1.c","po^ludnie");	

	add_item(({"kamienice","szare kamienice","zniszczone kamienice",
		"szare zniszczone kamienice"}),"S� to pi�trowe szare i ponure kamienice.\n");
	add_item(({"bram�","p�nocn� bram�"}),
		"Na p�nocy ulic� ko�czy brama. Musisz podej�� bli�ej, je�li chcesz j� dok�adniej obejrze�.\n");
	add_item("ulic�", "@@item1@@");

	add_prop(ROOM_I_INSIDE, 0);

	if (pora_roku() == MT_ZIMA) add_sit("na ziemi","na ziemi","z ziemi",0);
		else add_sit("na b�otnistej ziemi","na b�otnistej ziemi","z b�otnistej ziemi",0);

    add_object(RINDE+"przedmioty/lampa_uliczna");

    add_object(RINDE_NPC+"dzieciak2");
    add_object(RINDE_NPC+"dzieciak3");
}
public string
exits_description() 
{
    return "Ulica prowadzi na po^ludnie oraz na p^o^lnoc w kierunku bramy "+
           "miasta.\n";
}


string
dluuugi_opis()
{
    string str = "";

    if (dzien_noc()) str += "Ciemna"; else str += "Pe^lna zgie^lku i szumu";
    str += " ulica prowadzi do p�nocnej bramy miasta. Jest tak szeroka, �e " +
	"spokojnie mog� si� na niej mija� dwa wozy. To tutaj ko�czy si� bruk, " +
	"a zaczyna ";
    if (pora_roku() == MT_ZIMA) str += "zmarzni�ta"; else str += "zab�ocona";
    str += " ulica. Wzd�u� niej ci�gn� si� szare i zniszczone kamienice, kt�re ";
    if (dzien_noc()) str += "o tej porze wygl�daj� bardzo ponuro";
	else str+= "zlewaj� si� z codzienno�ci� tego miasta";
    str += ". Sta�ymi elementami tutaj s� brud i pomyje na r�wni z wsz�dobylskim " +
	"smrodem towarzysz�cym ekskrementom. Zdecydowanie jest to typowy " +
	"krajobraz ma�omiasteczkowych zaniedbanych ulic, do kt�rych zdaje si� " +
	"nie docieraj� ju� tury�ci, bo i po co? Jedyn� atrakcj� tutaj jest " +
	"konkurencja \"na kogo mieszka�cy kamienicy wylej� pomyje tym razem\".\n";
    return str;
}

string
item1()
{
	if (!dzien_noc() && pora_roku() == MT_ZIMA) 
			return "Szerok� ulic� wype�niaj� przechodnie.\n";
    if (!dzien_noc()) 
			return "Szerok� ulic� wype�niaj� przechodnie bezustannie wpadaj�c w co wi�ksze b�otniste ka�u�e.\n";

	return "Jest to szeroka b�otnista ulica spowita p�mrokiem.\n";
}
