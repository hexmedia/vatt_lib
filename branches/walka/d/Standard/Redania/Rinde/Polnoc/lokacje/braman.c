
/* Autor nieznany, poprawki by Avard 25.05.06 */
#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;

void create_rinde_street() 
{
    set_short("Przy p^o^lnocnej bramie");

    add_exit(TRAKT_RINDE_LOKACJE+"trakt37.c","p^o^lnoc");
    add_exit("ulica2.c","po^ludnie");       
    add_exit("ulica3.c","po^ludniowy-wsch^od");

    add_item(({"mury","mur","fortyfikacje"}), "Wysokie mury miasta maj^ace "+
        "chroni^c mieszka^nc^ow przed potencjalnymi naje^xdzcami. Ogromne "+
        "kamienne ^sciany pokryte s^a mchem i licznymi p^ekni^eciami "+
        "wskazuj^acymi na wiekowo^s^c konstrukcji jednak mimo tego na pewno "+
        "przetrwaj^a jeszcze wiele lat. \n");

    add_prop(ROOM_I_INSIDE,0);

    add_object(RINDE+"przedmioty/lampa_uliczna");
    add_object(POLNOC_DRZWI+"braman_out.c");
}
public string
exits_description() 
{
    return "Ulica prowadzi na po^ludnie i po^ludniowy-wsch^od, na p^o^lnocy "+
           "znajduje si^e brama miasta.\n";
}


string
dlugi_opis()

{
    string str;

    str = "Szeroka ulica jest niew^atpliwie jedn^a z wi^ekszych, je^sli nie "+
        "najwi^eksz^a arteri^a komunikacyjn^a miasta. Na p^o^lnocy "+
        "dostrzegasz szary pas miejskich mur^ow, z kt^orych wybija si^e "+
        "pot^e^zna brama obsadzona wartownikami. Ulica biegnie na po^ludnie,"+
        " jak z bicza strzeli^l, a wzd^lu^z niej rz^edem ci^agn^a si^e "+
        "ciemnoszare, przykro wygl^adaj^ace kamienice. W oddali majacz^a "+
        "zarysy ratusza g^oruj^acego nad miejskim rynkiem. Jeszcze dalej "+
        "rysuje si^e w^aska, ledwie st^ad dostrzegalna wst^ega mur^ow, "+
        "okalaj^acych przeciwleg^l^a cz^e^s^c miasta. W tym miejscu od "+
        "g^l^ownej arterii miasta odbiega nieco mniejsza uliczka "+
        "prowadz^aca do jakiego^s niewielkiego skrzy^zowania na "+
        "po^ludniowym wschodzie.\n";

    return str;
}
