/* Lil & Alcyone & Avard */
#include <stdproperties.h>
#include <macros.h>
#include <pogoda.h>
#include "dir.h"

inherit RINDE_STD;
inherit "/lib/peek";

string
koleiny()
{
    string str = "Koleist� ju� ulic� wci�� orz� wozy przeje�d�aj�ce t�dy, "+
	   "tworz�c pod�u�ne bruzdy";

    if (CZY_JEST_WODA(TO))
        str += "wype�nione w tej chwili ka�u�ami";
    str += ".\n";
    return str;
}

void create_rinde_street() 
{
    set_short("Ulica");
    add_exit("ulica4.c","p^o^lnocny-zach^od");
    add_exit(WSCHOD_LOKACJE + "ulica1.c","po^ludniowy-wsch^od");
    add_exit(KARCZMA_LOKACJE + "stajnia", ({"stajnia",
                "do stajni", "z zewn�trz"}));
    //    add_peek("przez szpar�", RINDE + "polnoc/karczma/stajnia");
    add_peek("przez szpar� w stajni", KARCZMA_LOKACJE + "stajnia");
    add_prop(ROOM_I_INSIDE,0);
    add_item(({"ziemi�","utwardzon� ziemi�","ulic�","utwardzon� ulic�"}),
            "Wy��obiona licznymi koleinami ulica ci�gnie prosto jak strza�a "+
            "od bramy p�nocnej, w kierunku bramy wschodniej, "+
            "przecinaj�c w ten spos�b obrze�a miasta i znacznie odci��aj�c "+
            "tym samym centrum miasta.\n");
    add_item(({"koleiny","pot�ne koleiny","koleiny na drodze","koleiny na ulicy",
           "pot�ne koleiny na drodze","pot�ne koleiny na ulicy"}),
           "@@koleiny@@");
    add_item("kamienice",
            "Ci�gn�ce si� wzd�u� ulicy kamienice zapewne "+
            "pami�taja lepsze czasy. S� szare i maj� "+
            "obdrapane �ciany.\n");
    add_item(({"stajni�","du�� stajni�"}),
            "Du�a stajnia sprawia wra�enie, jakby mia�a zaraz run��. "+
            "Do�� zauwa�alnie opiera si� o �cian� gospody. Liczne szpary "+
            "mi�dzy jej deskami zdradzaj� niesolidn� konstrukcj�.\n");
    add_item("ku�ni�",
            "Na wschodzie wznosi si^e niewielki budynek, z kt^orego "+
            "dochodz^a ciag^le odg^losy stukania, a przez ma^le okienko "+
            "mo^zna dostrzec wymykaj^ace si^e stamt^ad iskry. Nad pot^e^znymi "+
            "drzwiami, powiewa drewniany, co rusz skrzypi^acy, szyld, na kt^orym "+
            "namalowany jest jaki^s kolorowy napis. \n");

    add_object(POLNOC_DRZWI + "do_kuzni.c");
    dodaj_rzecz_niewyswietlana("pot^e^zne drzwi", 1);

}
public string
exits_description() 
{
    return "Ulica prowadzi na p^o^lnocny-zach^od oraz po^ludniowy-wsch^od, "+
           "s� tutaj tak^ze wej^scia do ku�ni i stajni.\n";
}


string dlugi_opis()
{
    string str;
    
    str="Ca�� ulic� ci�gn� si� kamienne i drewniane zabudowania. "+
        "Du�a stajnia, dobudowana do gospody zas�ania niemal�e "+
	"ca�y widok na p�noc";
    
    if(dzien_noc())
    str+=", a przez liczne szpary przedostaje si� nieco �wiat�a z wewn�trz";
    else
    str+="";
    
    str+=". Niedaleko niej, z wschodniej strony ma swoje miejsce kowal i jego "+
         "ku�nia"; 

    //if pracuje kowal
    //str+=", z kt�rej dobiegaj� ci� cz�ste charakterystyczne odg�osy uderzania "+
         //"metalu o metal, a podczas przerwy jakie� g�o�ne wi�zanki przekle�stw.";
    //else
    //str+="";
    
    
    str+=". Ze strony przeciwnej rozci�ga si� rz�d szarych, smutnych kamienic - "+
         "licz�cych sobie zapewne znacznie wi�cej wiosen ni� rozklekotana stajnia "+
	 "i gospoda. Przeje�d�aj�ce t�dy wozy wy��obi�y d�ugie koleiny na drodze "+
	 "wiod�cej prosto od p�nocnej bramy w kierunku wschodnim, nieco dalej "+
	 "zakr�caj�c lekko na po�udnie.";




    str+="\n";
    return str;
}

void init()
{
    ::init();
    init_peek();
}
