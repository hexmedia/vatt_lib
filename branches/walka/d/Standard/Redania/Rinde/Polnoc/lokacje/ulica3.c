/* LIL */
// 4.6.06 - zrobic zaleznosci - gj.
#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;

void create_rinde_street() 
{
    set_short("Ulica");
    add_exit("braman.c","p^o^lnocny-zach^od");
    add_exit("ulica4.c","po^ludniowy-wsch^od");
    add_sit("na ziemi","na ziemi","z ziemi",0);
    add_prop(ROOM_I_INSIDE,0);
	
    add_item(({"ziemi�","utwardzon� ziemi�","ulic�","utwardzon� ulic�"}),
           "Wy��obiona licznymi koleinami ulica ci�gnie prosto jak strza�a "+
	   "od pobliskiej bramy p�nocnej, w kierunku bramy wschodniej, "+
	   "przecinaj�c w ten spos�b obrze�a miasta i znacznie odci��aj�c "+
	   "tym samym centrum miasta.\n");
    add_item(({"koleiny","pot�ne koleiny","koleiny na drodze","koleiny na ulicy",
           "pot�ne koleiny na drodze","pot�ne koleiny na ulicy"}),
	   "Koleist� ju� ulic� wci�� orz� wozy przeje�d�aj�ce t�dy, "+
	   "tworz�c pod�u�ne bruzdy.\n");
    add_item(({"domostwa","domy","domki","kamienice"}),
           "Ci�gn�ce si� wzd�u� ulicy liczne zabudowania s� do�� "+
	   "r�norodne. Od starych, pami�taj�cych pewnie lepsze czasy "+
	   "kamienic po po�udniowej strony, po drewniane chatki, bli�sze "+
	   "murom miasta.\n");
    add_item(({"gospod�","karczm�","najwi�ksz� budowl�"}),
           "Umiejscowiona na wschodzie budowla - gospoda \"Baszta\" "+
	   "jest dwupi�trowa i posiada do�� spore podw�rze na "+
	   "zapleczu.\n");


}
public string
exits_description() 
{
    return "Ulica prowadzi na po^ludniowy-wsch^od oraz na p^o^lnocny-zach^od "+
           "w kierunku bramy miasta.\n";
}


string dlugi_opis()
{
    string str;
    str="Utwardzon� ziemi� ci�gnie si� ta ulica prosto jak strza�a. "+
        "Przez lata przeje�d�aj�ce t�dy wozy wy��obi�y pot�ne "+
	"koleiny, kt�re teraz wype�niaj� liczne ka�u�e. Jest to "+
	"droga sko�nie przecinaj�ca obrze�a miasta, przez co "+
	"umo�liwia znacznie �atwiejsze i efektywniejsze przedarcie "+
	"si� przez miasto z p�nocnej bramy do bramy wschodniej "+
	"skrz�tnie omijaj�c t�umy centrum wydzieraj�ce si� z "+
	"ka�dych stron. Wzd�u� panuj�cego tu ruchu ci�gn� si� "+
	"rz�dy domostw, domk�w i kilka kamienic po po�udniowej "+
	"stronie. Zdecydowanie najwi�ksza budowla to bez w^atpienia "+
	" gospoda \"Baszta\", znana w ca�ym mie�cie ze "+
	"swych niskich cen i poczciwo�ci w�a�ciciela.";

    if((KARCZMA_LOKACJE+"kotara")->query_prop(ROOM_I_LIGHT) >= 0 &&
       dzien_noc())
       str+=" Z niewielkiego okna jej zachodniej "+
	  "�ciany dobiega niemrawe �wiat�o.";
    else
      str+=" Jej zachodnia �ciana z niewielkim "+
	     "oknem przylega rogiem do ulicy.";
	  
	  
   




    str+="\n";
    return str;
}
