inherit "/std/room";

#include <stdproperties.h>
#include <macros.h>

void create_room() 
{
    set_short("Tajny magazyn");
    set_long("Jeste� w tajnym magazynie. Nie ma st�d �adnego wyj�cia!\n");
    add_prop(ROOM_I_INSIDE,1);
}

void
usun_ziolo(object ob)
{
    ob->remove_object();
}

void
enter_inv(object ob, object old)
{
    object* tab;
    if (ob->query_herb()) 
    {
        ob->stop_decay();
        if (sizeof((tab = filter(all_inventory(TO), &operator(==)(ob->short()) @ &->short()))) > 5) 
        {
            set_alarm(1.0, 0.0, &usun_ziolo(tab[1]));
        }
    }
    ::enter_inv(ob, old);
}

void
leave_inv(object ob, object to)
{
    if(ob->query_herb())
        ob->start_decay();
    ::leave_inv(ob, to);
}
