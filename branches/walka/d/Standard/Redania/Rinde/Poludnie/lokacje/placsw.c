// do przejrzenia
/*
 * BO, zwierzaczek
 *
 * przeczytaj napis/szyld/napis na szyldzie
 * Lombard.
 * 
 * ?usiadz
 * Mozna tu usiasc na ziemi.
 *
 * <--------->
 *  Co to jest? Czemu do cholery ktos (czyli zapewne Delvert) sie nie podpisuje?
 *   Ma byc obiektowosc, wiec szyld tez ma byc jako obiekt. Poza tym, nie bylo
 *   mozna go przeczytac, wiec poprawiam.
 *                 Saturday 08 of April 2006      Lil.
 * 
 */

#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;

void create_rinde_street() 
{
    set_short("Placyk");

    //TO MA BYC OBIEKTOWOSC? :P Lil.
    /*add_item("szyld",
      "Sporych rozmiar�w szyld z jakim� napisem. Mo�e warto " +
      "by by�o go przeczyta�?\n");*/
    add_object(POLUDNIE_OBIEKTY+"szyld_lombardu.c");

    add_event("Grupka ludzi wesz�a w�asnie do budynku na po�udniu.\n");
    add_event("Weso�e dzieci przebieg�y szybko niedaleko ciebie, " +
            "najwidoczniej bawi�c si� w co�.\n");
    add_event("Mieszkaniec przechodz�c tu� obok ciebie, nieumy�lnie " +
            "ci� potr�ci�.\n");
    add_event("Zabiegany kupiec pop�dzi� w stron� jakiego� klienta.\n");
    add_event("Dochodzi ci� g�o�na rozmowa jakich� kobiet.\n");

    add_exit("placnw.c","p^o^lnoc");
    add_exit("placne.c","p^o^lnocny-wsch^od");
    add_exit("bramas.c","zach^od");
    add_exit("placse.c","wsch^od");
    add_object(POLUDNIE_DRZWI + "do_lombardu.c");

    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Dalsza cz^e^s^c placu widoczna jest na p^olnocy, "+
           "p^olnocnym-wschodzie i wschodzie, za^s na zachodzie znajduje "+
           "si^e ulica prowadz^aca do bramy miasta.\n";
}

string
dlugi_opis()
{
	string str;

	str = "Niewielkich rozmiar�w placyk tu w�a�nie ma swoje " +
		"po�udniowo-zachodnie kra�ce. Wystarczy lekko si� " +
		"rozejrze� i bez problemu mo�na dostrzec reszt� " +
		"tego placu. Kilka jard^ow na p^o^lnoc st^ad dostrzegasz " +
		"sporych rozmiar�w skalniaczek, za� naprzeciw niego " +
		"jaki� pos�g. Ziemi� wy�o�ono tutaj r�wnym kamieniem, " +
		"po kt�rym przyjemnie si� st�pa. Na zachodzie widzisz " +
		"wyj�cie z placu na jedn� z ulic, a na po^ludniu ci^agnie " +
                "si^e rz^ad kamienic. Do jednej z nich " +
		"prowadz� jakie� drzwi";
        if(jest_rzecz_w_sublokacji(0, "ma�y szyld"))
        str+=", nad kt�rymi dostrzegasz szyld.\n";
        else
        str+=".\n";

	return str;
}

