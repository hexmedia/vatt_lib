/* Jak zwykle niedokonczona lokacja.
   Tym razem ze wzgledu na brak std pogody.
   
   
   Opis by Khaer, przerobiony by Lil          */

#include <stdproperties.h>
#include "dir.h"
#include <mudtime.h>
#include <macros.h>
#include <pogoda.h>

inherit RINDE_STD;

string dlugi_opis();

void create_rinde_street() 
{
	set_short("Ulica");
//	set_long("@@dlugi_opis@@\n");

	add_exit("ulica7.c","p^o^lnocny-zach^od");	
	add_exit("bramas.c","po^ludniowy-wsch^od");	

	add_prop(ROOM_I_INSIDE,0);

	
	add_item(({"chat�","chaty"}), "Wszystkie te chatki zbudowane sa "+
	           "pod�ug jednego schematu. �ciany z u�o�onych poziomo, "+
		   "d�bowych bali, niewielkie okienka z okiennicami i "+
		   "spadzisty dach, z kt�rego sterczy prosty komin to "+
		   "niemal�e ich jedyne elementy.\n");
        
/*sopel, sople (tylko zima) - Niektore malenkie jak igly, inne dlugie i ostre jak groty wloczni. Natomiast wszystkie jednakowo pieknie mienia sie szeroka paleta barw, gdy padna na nie promienie Slonca.*/

        add_item("kmieci", "Wielu kr�ci si� wok�. Wszyscy ubrani s� w "+
	         "robocze ubrania, niekt�rzy z kosami na ramionach "+
		 "ruszaj� do pracy lub wracaj� z niej.");
}
public string
exits_description() 
{
    return "Ulica prowadzi na p^o^lnocny-zach^od oraz na "+
           "po^ludniowy-wsch^od w stron^e bramy miasta.\n";
}

string
dlugi_opis()
{
    string str;
    
    if(!dzien_noc())
    {
      str ="Po obu stronach tej uliczki na obrze�ach miasta "+
         "stoj� drewniane chaty, niekt�re ze �cianami pobielonymi "+
	 "wapnem. ";
      if(pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
        str+="Czasem z okna jednej z chat wychyli si� kto� "+
	 "ciekawski omiataj�c uliczk� wzrokiem. Kilku kmieci "+
	 "siedzi na progach swych domostw zajmuj�c si� rozmaitymi "+
	 "rzeczami, inni stoj� na �rodku uliczki dyskutuj�c o czym� "+
	 "zawzi�cie, jest tak�e jeden, kt�ry sierpem �cina swie�� "+
	 "traw� pod fasad� swojego domu mrucz�c co� ponuro pod nosem.";
      if(pora_roku() == MT_JESIEN)
        str+="Czasem z okna jednej z chat wychyli si� kto� "+
	 "ciekawski omiataj�c uliczk� wzrokiem. Kilku kmieci "+
	 "siedzi na progach swych domostw zajmuj�c si� rozmaitymi "+
	 "rzeczami, inni stoj� na �rodku uliczki dyskutuj�c o czym� "+
	 "zawzi�cie. Nag^le, ch�odne podmuchy wiatru igraj� ze stertami "+
	 "opad�ych li�ci rozrzucaj�c je po ca�ej ulicy";   
      if(pora_roku() == MT_ZIMA)
        {
	str+="Uliczka sprawia wra�enie niemal ca�kowicie wymar�ej, "+
	     "a wszystkie drzwi i okiennice s� zatrza�ni�te na g�ucho, "+
	     "aby nie wpuszczac ch�odu do chat. Ze spadzistych dach�w "+
	     "zwisaj� jasne kryszta�y sopli, za� z komin�w chat unosz� "+
	     "si� jasne smugi dymu.";
	if(CZY_JEST_SNIEG(TO))
	str +=" �wie�y �nieg skrzypi g�o�no pod twoimi nogami, a mr�z "+
	      "szczypie w uszy i czerwieni lica.";
        }      
       /* #2 - zaleznie od pogody
Slonecznie i sucho (poza zima) - Ziemia, po ktorej stapasz od dawna nie widziala deszczu, wobec czego kazdy twoj krok wzbija tuman kurzu.
Deszcz - Z chlupotem brodzisz w blotnistej mazi, w jaka zamienila sie niewybrukowana sciezka pod wplywem deszczu. 
Slonecznie (zima) - Slonce wiszace nisko nad horyzontem rozprasza sie na soplach lodu tworzac wielobarwne refleksy na wszystkim dookola.
Snieg - Mala zadymka sniezna zmusza cie do ciaglego mruzenia oczu.       */
     }
     
     else
     {
  /*   if(jest_ksiezyc())
       {
       str="Okr�g�a tarcza ksi�yca wisi wysoko na niebie, a miliony "+
           "gwiazd b�yskaj� do ciebie z g�ry. Wyt�aj�c wzrok dostrzegasz "+
	   "po bokach obrysy drewnianych chat; wszystkie zamkniete s� "+
	   "na g�ucho w obawie przed mrokiem nocy.";
       }
       else
       {*/
       str="Wyt�aj�c wzrok dostrzegasz po bokach obrysy drewnianych chat; "+
           "wszystkie zamkni�te s� na g�ucho w obawie przed mrokiem nocy.";
//       }
     if(pora_roku() == MT_WIOSNA)
       str+=" Tw�j wzrok wy�awia r�wnie� z mroku mnogo�� rozmaitych ro�lin "+
           "rosn�cych po bokach �cie�ki. Przez gr� cieni ro�liny te wydaj� "+
	   "si� dziwnie zdeformowane i w jaki� spos�b budz� niepok�j.";
     if(pora_roku() == MT_LATO)
       str+=" Pomimo zaj�cia s�o�ca powietrze wci�� jest nagrzane i "+
            "temperatura jest ca�kiem zno�na.";
     if(pora_roku() == MT_JESIEN)
       str+=" Gwa�towne podmuchy wiatru przejmuj� ci� nag�ym dreszczem. "+
            "Zesch�e li�cie szeleszcz� sucho pod twoimi stopami, a "+
	    "pogr��one w mroku drzewa zdaj� si� obserwowa� ci� w "+
	    "milczeniu i powoli wyci�ga� ku tobie swe obesch�e ga��zie.";
     if(pora_roku() == MT_ZIMA)
       {
    /*   if(jes_snieg())
       str+=" Przejmuj�cy mr�z i skrzypienie �niegu pod twoimi stopami "+
            "przypominaj� ci�gle o panuj�cej porze roku.";
       else*/
       str+=" Przejmuj�cy mr�z przypomina ci�gle o panuj�cej porze roku.";
       }
       
       
     }
     
   /*  if(jest_deszcz())Z chlupotem brodzisz w blotnistej mazi, w jaka zamienila sie niewybrukowana sciezka pod wplywem deszczu. 
   if(jest_snieg()) Mala zadymka sniezna zmusza cie do ciaglego mruzenia oczu.*/
     
     str +="\n";
     
     return str;
}
       