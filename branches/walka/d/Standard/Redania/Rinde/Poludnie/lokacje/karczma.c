/*
 * ?usiadz
 * Mozna tu usiasc przy pierwszym stole, przy drugim stole, przy trzecim stole, przy czwartym stole, przy piatym stole oraz przy ladzie.
 */
inherit "/std/room";
inherit "/lib/pub_new";

#include <stdproperties.h>
#include "dir.h"

void
create_room() 
{
    set_short("Karczma \"Pod Weso�ym D�inem\"");

    set_long("@@dlugi_opis@@");

    add_item("lad�",
	"Du�a lada o porysowanym blacie, zapewne od cz�stego przesuwania " +
	"kufli pe�nych piwa. Na jej przedniej cz�ci jaki� miejscowy " +
	"artysta namalowa� d�ina z szerokim i przyjaznym u�miechem, " +
	"trzymaj�cego w r�ce zw�j, za� na zwoju wypisany �adnie " +
	"kaligrafowanymi literami jad�ospis.\n");
    add_item(({"st�", "sto�y"}),	"Dla go�ci rozstawiono sto�y, a przy nich �awy i krzes�a.\n");
    add_item("drzwi",	"Sporych rozmiar�w dwuskrzyd�owe drzwi prowadz�ce na ulic�.\n");

    add_event("Jeden z go�ci wlaz� na st� i wymachuj�c pe�nym " +
	"kieliszkiem wzni�s� toast za koleg�, co pozostali " +
	"obecni w karczmie skwitowali g�o�nymi oklaskami.\n");
    add_event("Jaki� zupe�nie pijany klient osun�� si� z krzes�a na pod�og�.\n");
    add_event("Kelnerki z niebywa�� zr�czno�ci� uwijaj� si� mi�dzy go��mi.\n");
    add_event("Grupka m�czyzn siedz�ca przy jednym ze sto��w wybuch�a " +
	"g�o�nym �miechem.\n");
    add_event("Kelnerka niechc�cy wpad�a na ciebie i przepraszaj�c " +
	"czmychn�a do kuchni.\n");
    add_event("Dochodzi ci� ci�g�y gwar karczemnych rozm�w.\n");
    add_event("Kilku klient�w wysz�o z karczmy w weso�ych humorach.\n");

    add_sit("przy pierwszym stole", "przy pierwszym stole", "od sto�u", 4);
    add_sit("przy drugim stole", "przy drugim stole", "od sto�u", 4);
    add_sit("przy trzecim stole", "przy trzecim stole", "od sto�u", 4);
    add_sit("przy czwartym stole", "przy czwartym stole", "od sto�u", 4);
    add_sit("przy pi�tym stole", "przy pi�tym stole", "od sto�u", 4);
    add_sit("przy ladzie", "przy ladzie", "od lady", 5);

    add_npc(RINDE_NPC + "karczmarz_wyspiarskiej");
    add_npc(RINDE_NPC + "trubadur");
    add_npc(RINDE_NPC + "mezczyzna_w_karczmie");

    add_drink("piwo", ({ "jasny", "ja�ni"}), "jasno-bursztynowego, " +
            "wspaniale pieni�cego si� piwa", 700, 4, 15, "kufel", "piwa",
            "Jest to kufel wykonany z krystalicznego szk�a, kt�rego uchwyt " +
            "wykonano z dobrego d�bowego drewna.",10000);
    add_drink("piwo", ({ "reda�ski", "reda�scy" }), "jasno-bursztynowego, " +
            "wspaniale pieni�cego si� piwa", 750, 5, 16, "kufel", "piwa",
            "Jest to kufel wykonany z krystalicznego szk�a, kt�rego uchwyt " +
            "wykonano z dobrego d�bowego drewna.");

    add_wynos_food("golab", ({ ({"pieczony"}), ({"pieczeni"}) }),
                    350, 15, "Upieczony go��b.\n", 43200, ({ "sple�nia�y", "sple�niali" }));
    add_wynos_food("golabek", ({ ({"duszony"}), ({"duszeni"}) }), 310, 13, "Duszony go��bek.\n",
                    33200, ({ "sple�nia�y", "sple�niali" }));

    add_object(POLUDNIE_OBIEKTY + "menu");

    add_exit("ulica2.c", ({"wyj�cie", "na zewn�trz", "z karczmy"}) );

    add_prop(ROOM_I_INSIDE, 1);
}
public string
exits_description() 
{
    return "Wyj^scie prowadzi na ulice.\n";
}

void
init()
{
    ::init();
    init_pub();
}

string
dlugi_opis()
{
    string str;

    str = "Du�a sala karczmy wype�niona jest najr�niejszymi go��mi. " +
	"T�ok sk�adaj�cy si� ze sta�ych bywalc�w, a tak�e z przypadkowych " +
	"go�ci utrudnia rozejrzenie si� po pomieszczeniu. O�wietlenie " +
	"daj� pochodnie umieszczone na �cianach oraz du�e okna z lekkimi " +
	"firankami nieco po��k�ymi od dymu unosz�cego si� w powietrzu. " +
	"W karczmie ustawiono kilka prostok�tnych sto��w, a dodatkowo, maj�c " +
	"na uwadze samotnych bywalc�w, przygotowano kilka miejsc niedaleko " +
	"karczmarza. Wysoka, wykonana z mahoniowego drewna lada o nieco " +
	"porysowanym ju� blacie, przydaje temu miejscu niejakiego uroku jak i " +
	"charakteru typowej wyspiarskiej karczmy. Nad wej�ciem kto� " +
	"umiej�tnie wymalowa� szeroko u�miechni�tego d�ina, " +
	"trzymaj�cego zw�j z jad�ospisem gospody. Do wyj�cia prowadz� " +
	"dwuskrzyd�owe drzwi, za� w rogu dostrzegasz niewielkie przej�cie, " +
	"zapewne do kuchni.\n";

    return str;
}
