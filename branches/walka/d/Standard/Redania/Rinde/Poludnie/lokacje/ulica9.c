#include <mudtime.h>
#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;

void create_rinde_street() 
{
	set_short("Boczna uliczka");
       set_long("Ma�o ucz�szczana, w�ska uliczka prowadzi od "
        + "niewielkiego placu na zachodzie na p�noc, do samego "
        + "centrum Rinde.\n");

	add_exit(WSCHOD_LOKACJE + "ulica7.c","p�noc");
	add_exit("placse.c","zach�d");

	add_prop(ROOM_I_INSIDE,0);
	
	set_alarm_every_hour("czy_postawic_kupca");

}

int czy_postawic_kupca()
{
	if(MT_GODZINA > 21 && present("hared", this_object()) == 0)
		add_object(RINDE+"npc/hared.c");
		
}
public string
exits_description() 
{
    return "Ulica prowadzi na p^o^lnoc i zach^od.\n";
}
