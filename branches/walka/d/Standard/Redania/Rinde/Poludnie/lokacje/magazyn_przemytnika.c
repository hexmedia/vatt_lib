
inherit "/std/room";
#include <stdproperties.h>
#include <macros.h>
#include "dir.h"

#define BRONIER (RINDE + "przedmioty/bronie/")
#define BRONIEO "/d/Standard/items/bronie/"
#define ZBROJEO "/d/Standard/items/zbroje/"
#define LAMPA "/d/Standard/items/narzedzia/lampa_niewielka_poreczna"


void create_room() 
{
    set_short("Tajny magazyn");
    set_long("Jeste� w tajnym magazynie. Nie ma st�d �adnego wyj�cia!\n");
    add_prop(ROOM_I_INSIDE,1);
    /* Domy�lne rzeczy, kt�rymi handluje sklepikarz... */
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Lc");
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Lc");
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Lc");
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Lk");
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Lk");
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Mc");
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Mc");
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Mk");
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Mk");
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Sk");
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Sk");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/krzemien");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/torch");
    add_object("/std/olej");   
    add_object("/std/olej");   
    add_object("/std/olej");   
    add_object("/std/olej");   
    add_object("/std/olej");
    add_object("/std/olej");   
    add_object("/std/olej");   
    add_object("/std/olej");   
    add_object("/std/olej");   
    add_object("/std/olej");   
    add_object("/std/olej");   
    add_object("/std/olej");
    add_object(LAMPA);
    add_object(LAMPA);
    add_object(LAMPA);
    add_object(LAMPA);
    add_object(LAMPA);
    add_object(LAMPA);
    add_object(LAMPA);
    add_object(LAMPA);
    add_object(LAMPA);
    add_object(LAMPA);
    add_object(LAMPA);
    add_object(BRONIEO+"noze/maly_poreczny");
    add_object(BRONIEO+"noze/maly_poreczny");
    add_object(BRONIEO+"noze/maly_poreczny");
    add_object(BRONIEO+"noze/maly_poreczny");
    add_object(BRONIER+"topory/krotki_zdobiony");
    add_object(BRONIER+"topory/krotki_zdobiony");
    add_object(BRONIER+"topory/krotki_zdobiony");
    add_object(BRONIER+"topory/prosty_jednoreczny.c");
    add_object(BRONIER+"topory/prosty_jednoreczny.c");
    add_object(BRONIER+"topory/prosty_jednoreczny.c");
    add_object(BRONIER+"topory/prosty_jednoreczny.c");
    add_object(BRONIER+"topory/prosty_jednoreczny.c");
    add_object(BRONIER+"mloty/ciezki_zelazny.c");
    add_object(BRONIER+"mloty/ciezki_zelazny.c");
    add_object(BRONIER+"mloty/ciezki_zelazny.c");
    add_object(BRONIER+"mloty/poreczny_zelazny_nadziak.c");
    add_object(BRONIER+"mloty/poreczny_zelazny_nadziak.c");
    add_object(BRONIER+"mloty/poreczny_zelazny_nadziak.c");
}