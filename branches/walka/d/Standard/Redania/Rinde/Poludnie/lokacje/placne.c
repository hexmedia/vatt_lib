/*
 * BO, zwierzaczek
 *
 * przeczytaj napis
 * Taka strza�a ka�dego mo�e trafi�, uwa�aj na swoje po�ladki!
 * 
 */
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"


inherit RINDE_STD;

void create_rinde_street() 
{
        set_short("Placyk");

        add_item(({"pos^ag", "pomnik"}),
                "Wysoki pomnik wykonany z jakiego^s ciemnego kruszcu. " +
                "Przedstawia m^lodego ch^lopczyka owini^etego w pasie " +
                "jak^a^s chust^a, kt^ora lu^xno zwisa mu a� do ud. Dobywa on " +
                "^luku, jedn^a z r^ak naci^agaj^ac mocno ci^eciw^e. Przez plecy " +
                "za^s ma przewieszony ko^lczan, ca^ly nape^lniony strza^lami. " +
                "U do^lu pos^agu dostrzegasz niewielk^a tabliczk^e.\n");

        add_item("tabliczk^e",
                "Niewielka tabliczka u do^lu pos^agu z podpisem autora. " +
                "Jacy^s z^lo^sliwcy dopisali pod spodem jaki^s napis.\n");
        add_item("napis na tabliczce",
                "Taka strza^la ka^zdego mo^ze trafi^c, uwa^zaj na swoje "+
                "po^sladki!\n");
        add_cmd_item("napis na tabliczce","przeczytaj",
                "Taka strza^la ka^zdego mo^ze trafi^c, uwa^zaj na swoje "+
                "po^sladki!\n");
        add_event("Lekki wiatr muska ci^e delikatnie.\n");
        add_event("Jeden z mieszka^nc^ow przeszed^l tu� obok ciebie.\n");
        add_event("Jaki^s turysta na chwil^e zatrzyma^l si^e przed pos^agiem, " +
                "po czym ruszy^l dalej.\n");
        add_event("Grupka m^lodzie�y przesz^la placykiem g^lo^sno rozmawiaj^ac.\n");
        add_event("Ciemny ptaszek przelecia^l nad tob^a i na chwil^e cupn^a^l " +
                "na pos^agu.\n");

        add_exit("ulica3.c","p^o^lnoc");
        add_exit("placse.c","po^ludnie");       
        add_exit("placsw.c","po^ludniowy-zach^od");
        add_exit("placnw.c","zach^od");

        add_prop(ROOM_I_INSIDE,0);

        add_sit("pod pos^agiem","pod pos^agiem","spod pos^agu",6);

}
public string
exits_description() 
{
    return "Dalsza cz^e^s^c placu widoczna jest na zachodzie, "+
           "po^ludniu i po^ludniowym-zachodzie, za^s na p^o^lnocy znajduje "+
           "si^e ulica.\n";
}

string
dlugi_opis()
{
        string str;

        str = "Znajdujesz si^e w p^o^lnocno-wschodniej cz^e^sci tego " +
                "niezbyt du�ego placyku. Bez wi^ekszych trudno^sci mo�esz " +
                "dostrzec pozosta^le jego fragmenty. Tutaj jedynym " +
                "wyr^o�niaj^acym si^e obiektem jest wysoki pomnik wykonany " +
                "z jakiego^s ciemnego kamienia. Przedstawia stoj^acego " +
                "na palcach prawej stopy ch^lopczyka, ubranego jedynie " +
                "w szmatk^e owini^et^a wok^o^l bioder i lu^xno zwisaj^ac^a " +
                "na udach. Ch^lopiec trzyma w r^ekach ^luk, a przez plecy " +
                "ma przewieszony ko^lczan z zapasowymi strza^lami. " +
                "Odwracaj^ac wzrok od pos^agu mo�na si^e przyjrze^c " +
                "kamieniom, z kt^orych sk^lada si^e r^owne pod^lo�e, " +
                "ozdobionych jakimi^s niezrozumia^lymi wzorami, zapewne " +
                "w zamierzeniu autora maj^acych wygl^ada^c niezwykle " +
                "ambitnie, a w rzeczywisto^sci po prostu kiczowatych. ";

        return str;
}
int
co_godzine()
{
    if (MT_GODZINA == 16)
    {
        set_alarm(3000.0,0.0, "odnow_darle");
    }
}
void
odnow_darle()
{
    LOAD_ERR(RINDE_NPC + "darla.c");

    if(!present("darla", TO) && (sizeof(object_clones(find_object(RINDE_NPC+"darla.c"))) == 0))
    {
        object darla;
        darla = clone_object(RINDE_NPC + "darla.c");
        tell_roombb(TO, QCIMIE(darla, PL_MIA)+" wychodzi "+
            "z jednej z kamienic.\n");
        darla->init_arg(0);
        darla->start_me();
        darla->move(TO);
    }
}

