#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit RINDE_STD;

void create_rinde_street() 
{
    set_short("Ulica");
//	set_long("Jeste^s na uliczce krzywo biegn^acej od ^swiatyni"
//	    + "do po^ludniowej bramy. Na rogu stoi ma^ly domek - apteka.\n");

    add_item(({"wie��","strzelist� wie��","�wi�tynn� wie��","bia�� wie��",
               "strzelist� bia�� wie��","bia�� strzelist� wie��",
               "�wi�tynn� bia�� wie��","�wi�tynn� strzelist� wie��"}),
               "�wi�tynna w�ska i bia�a wie�a wybija si� ponad dachy "+
               "innych budynk�w i wygl�da bez trudu przez fortyfikacj� miasta.\n");
    add_item(({"wie^z^e","smuk^l^a wie^z^e","wie^z^e ratusza",
               "smuk^la wie^z^e ratusza"}),
               "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic, "+
               "niczym palec, wskazuj^acy centrum tego miasta.\n");
    add_item(({"domy", "domostwa", "chaty", "cha^lupy"}),
	    "Drewniane zaniedbane cha^lupy po obu stronach ulicy tworz^a " +
	    "niezbyt przyjemny widok.\n");
    add_item(({"dom", "budynek", "apteke"}),
	    "Du^zy i solidny budynek z ociosanych bali drewnianych wyra^xnie" +
	    "kontrastuje z otaczaj^acymi go cha^lupami. Szyld tu^z nad " +
	    "wej^sciem g^losi: Apteka.\n");

    add_exit("ulica8", "p^o^lnoc");	
    add_exit("ulica6", "po^ludniowy-wsch^od");	
//    add_exit("apteka", ({"apteka", "do apteki", "z zewn�trz"}));

    add_prop(ROOM_I_INSIDE,0);
    add_sit("na ziemi","na ziemi","z ziemi",0);

    add_object(POLUDNIE_OBIEKTY+"szyld_apteki.c");
    add_object(POLUDNIE_DRZWI+"drzwi_do_apteki.c");
    add_object(KANALY_DRZWI + "krata_do.c");   
}
public string
exits_description() 
{
    return "Ulica prowadzi na p^o^lnoc i po^ludniowy-wsch^od, znajduje si^e "+
           "tu tak^ze wej^scie do apteki.\n";
}

string
dlugi_opis()
{
    string str;

    str = "W^aska uliczka w tej cz^e^sci miasta prowadzi od bramy na "+
        "po^ludniowym wschodzie w kierunku p^o^lnocnym, gdzie dostrzegasz "+
        "bia^l^a smuk^l^a wie^z^e miejskiej ^swi^atyni. Drewniane domostwa "+
        "po obu stronach ulicy nie wygl^adaj^a zbyt solidnie i zdaj^a "+
        "si^e potwierdza�, ^ze znajdujesz si^e w jednej z ubo^zszych "+
        "dzielnic miasta. Obrazu n^edzy dope^lniaj^a porozrzucane "+
        "gdzieniegdzie przy domach bli^zej niezidentyfikowane ^smieci, a "+
        "w paru miejscach widniej^a ciemne plamy na ziemi, zapewne ka^lu^ze "+
        "jakich^s nieczystosci. Jeden z budynk^ow wyra^xnie r^o^zni si^e "+
        "od otaczaj^acych go cha^lup, jest wi^ekszy i bardziej zadbany, "+
        "pokryte br^azow^a farb^a zdobione okiennice i kryty czerwon^a "+
        "dach^owk^a dach ^swiadcz^a o wy^zszej pozycji materialnej "+
        "wla^sciciela. Naprzeciw niego, na ^srodku drogi znajduje si^e "+
        "stalowa krata. ";

    if (jest_rzecz_w_sublokacji(0, "sosnowy szyld"))
        str += "Na szyldzie tu^z nad wej^sciem widnieje napis "+
        "m^owi^acy, ^ze mie^sci si^e tu apteka. ";
    switch (pora_roku())
    {
	case MT_ZIMA:
	    str += "Z niekt^orych komin^ow w g^or^e unosz^a si^e cienkie "+
        "smu^zki dymu. Drog^e pokrywa ciemnoszara warstwa ^sniegu, "+
        "poprzecinana koleinami i miejscami wydeptana.\n";
	    break;
	case MT_JESIEN:
	    str += "Z niekt^orych komin^ow w g^or^e unosz^a si^e cienkie smu^zki "+
        "dymu. Zab^locon^a drog^e pokrywaj^a liczne g^l^ebokie koleiny.\n";
	    break;
	case MT_LATO:
	    str += "Dobrze ubita droga pokryta jest w wielu miejscach wysuszon^a "+
        "przez s^lo^nce traw^a.\n";
	    break;
	case MT_WIOSNA:
	    str += "M^loda, jasnozielona trawa porasta w wielu miejscach w^ask^a "+
        "drog^e.\n";
	    break;
    }
    return str;
}
