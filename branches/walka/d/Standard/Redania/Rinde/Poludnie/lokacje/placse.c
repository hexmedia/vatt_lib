// do przejrzenia
// opisy zenujace, do sth
// ktora to juz fontanna w rinde??
/*
 * BO, zwierzaczek
 */
#include <stdproperties.h>
#include "dir.h"
#include <mudtime.h>
#include <macros.h>

inherit RINDE_STD;
inherit "/lib/drink_water";

void create_rinde_street() 
{
	set_short("Placyk");

	add_item("fontann�",
		"Niewielkich rozmiar�w fontanna ma wystarczaj�c� ilo�� " +
		"wody, by ka�dy przechodzie� m�g� si� z niej napi�. " +
		"Na jej �rodku wyrze�biono niewielki pos��ek " +
		"przedstawiaj�cy smuk�� rybk�, z kt�rej tryska " +
		"swie�a woda.\n");
	
	add_event("Woda w fontannie tryska weso�o zach�caj�c do " +
		"napicia si� jej.\n");
	add_event("Niewielki ptaszek przylecia�, by napi� si� " +
		"troszk� wody.\n");
	add_event("Jeden z mieszka�c�w podszed� do fontanny i nabra� " +
		"troch� wody do wiaderka, po czym odszed�.\n");
	add_event("Grupka m�odzie�y na chwil� przystan�a niedaleko " +
		"ciebie, g�o�no �miej�c si� i rozmawiaj�c.\n");
	add_event("Kupiec z dostaw� towar�w przebieg� obok, ci�ko " +
		"przy tym dysz�c.\n");	
       set_drink_places("z fontanny");

	add_exit("placne.c","p^o^lnoc");
	add_exit("placnw.c","p^o^lnocny-zach^od");
	add_exit("placsw.c","zach^od");
	add_exit("ulica9.c","wsch^od");

	add_prop(ROOM_I_INSIDE,0);

}
public string
exits_description() 
{
    return "Dalsza cz^e^s^c placu widoczna jest na zachodzie, "+
           "p^o^lnocy i p^o^lnocnym-zachodzie, za^s na wschodzie znajduje "+
           "si^e ulica.\n";
}
string
dlugi_opis()
{
	string str;

	str = "Bez trudu z tego miejsca mo�esz " +
		"ogarn�� wzrokiem reszt� placu. Dostrzegasz " +
		"na p�nocy ciemny pos�g oraz mniej wi�cej naprzeciw " +
		"niego ca�kiem poka�ny skalniaczek z ro^slinami. Po stronie " +
		"wschodniej za�, placyk przeistacza si� w miejsk� uliczk� " +
		"prowadz�c^a w g��b miasta. Niedaleko dostrzegasz niewielk� " +
		"fontann�, tryskaj�c� �wie�� wod�. Od czasu do czasu jaki� " +
		"zb��kany ptaszek siada na jej brzegu i pije troch�, " +
                "odlatuj�c po chwili rado�nie. Zapewne nie tylko on gasi " +
                "przy niej swoje pragnienie.\n";

	return str;
}

void
init()
{
    ::init();

    init_drink_water(); 
}
void
drink_effect(string str)
{
    write("Pijesz nieco wody z fontanny. \n");
    saybb(QCIMIE(this_player(), PL_MIA) + " pije nieco wody z fontanny. \n");
}
