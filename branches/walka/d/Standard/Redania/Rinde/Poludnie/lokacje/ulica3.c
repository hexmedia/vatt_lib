#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;

void create_rinde_street() 
{
	set_short("Ulica");
	set_long("Trzy schodki w d^o^l na po^ludnie jest mniejszy placyk, "
	    + "uliczka prowadzi z zachodu na wsch^od, na p^o^lnocy "
	    + "plac ratuszowy. \n");
	add_exit(CENTRUM_LOKACJE + "placse.c","p^o^lnoc");
	add_exit("placne.c","po^ludnie");	
	add_exit("ulica2.c","zach^od");


	add_prop(ROOM_I_INSIDE,0);

}
public string
exits_description() 
{
    return "Dalsza cz^e^s^c placu widoczna jest na po^ludniu i p^o^lnocy, "+
           "za^s ulica prowadzi na zach^od.\n";
}
