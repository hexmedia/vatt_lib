#include <stdproperties.h>
#include "dir.h"

inherit RINDE_STD;

void create_rinde_street() 
{
    set_short("Dziwne miejsce");
    add_exit("bramas.c","p�noc");
    add_object(POLUDNIE_DRZWI+"bramas_out.c"); 
}

string
dlugi_opis()
{
    string str;
    str = "W tym bardzo dziwnym miejscu czujesz, �e nie powin"+
    this_player()->koncowka("iene�","na�")+" przebywa�. W zasadzie to..."+
    " jak tu si� dosta�"+this_player()->koncowka("e�","a�")+"?\n";
    return str;
}
