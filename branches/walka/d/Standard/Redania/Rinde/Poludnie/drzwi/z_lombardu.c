
/* 
 * Drzwi prowadzace do lombardu
 * Wykonane przez Avarda, dnia 08.06.06
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
    dodaj_przym("drewniany", "drewniani");
    
    set_other_room(POLUDNIE_LOKACJE +"placsw.c");
    set_door_id("DRZWI_DO_LOMBARDU_RINDE");
    set_door_desc("Nie wyr^ozniaj^ace si^e niczym nadzwyczajnym drzwi "+
        "pomalowane na ciemny br^az.\n");
 
    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({"wyj^scie","przez drewniane drzwi na zewn�trz",
                      "z lombardu"}));
    //set_pass_mess("przez drewniane drzwi na zewn^atrz");
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_key("KLUCZ_DRZWI_DO_LOMBARDU_RINDE");
    set_lock_name("zamek");
}
