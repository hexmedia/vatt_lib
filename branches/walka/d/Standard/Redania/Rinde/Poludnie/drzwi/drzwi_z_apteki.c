
/*Drzwi z apteki by Sed 06.03.07*/

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
    dodaj_przym("drewniany", "drewniani");
    
    set_other_room(POLUDNIE_LOKACJE+"ulica7.c");
    
    set_door_id("DRZWI_Z_APTEKI_RINDE");
    set_door_desc("Masz przed sob^a solidne drewniane drzwi.\n");
    
    set_open_desc("Na zachodniej scianie daja sie zauwazyc otwarte drewniane "+
        "drzwi.\n");
    set_closed_desc("Na zachodniej scianie daja sie zauwazyc zamkniete "+
        "drewniane drzwi.\n");
        
    set_pass_command(({({"zachod", "w", "wyj^scie" }),
        "przed drewniane drzwi na zach^od","z apteki"}));
}

