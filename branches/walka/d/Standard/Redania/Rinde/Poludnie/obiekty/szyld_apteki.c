// szyld rindowskiej apteki, na podst. innego szyldu autorstwa Lil
// g.

inherit "/std/object.c";
#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>

int przeczytaj_szyld();

create_object()
{
    ustaw_nazwe("szyld");
    dodaj_przym("sosnowy","sosnowi");
    set_long("Du^zy, sosnowy szyld wisz^acy nad drzwiami jednego z " +
        "budynk^ow. Starannie namalowany napis wskazuje, i^z jest to apteka.\n");
    ustaw_material(MATERIALY_DR_SOSNA);
    add_prop(OBJ_I_WEIGHT, 1985);
    add_prop(OBJ_I_VOLUME, 451);
    add_prop(OBJ_I_VALUE, 2);
    add_prop(OBJ_I_NO_GET, "Szyld dynda sobie wysoko "+
             "nad twoj� g�ow�. Raczej go nie si�gniesz.\n");
    add_prop(OBJ_I_DONT_GLANCE,1);
    add_cmd_item(({"szyld","szyld apteki","szyld nad aptek^a"}),
                "przeczytaj", przeczytaj_szyld,
                  "Przeczytaj co?\n");
}

int
przeczytaj_szyld()
{
    write("Apteka u Wawrzynoska - zio^la i medykamenty.\n");
    return 1;
}