/*
 * opis Brz^ozkowy
 * faevcia wrzuci^la
 * 22.06.07
 *
 */

inherit "/std/herb";

#include <pl.h>
#include <herb.h>
#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>


void
create_herb()
{
    ustaw_nazwe_glowna_ziola("ziele");
    ustaw_nazwe_ziola("krwawnik");	

    ustaw_nazwe("li^s^c");

    dodaj_przym("du^zy", "duzi");
    dodaj_przym("z^lo^zony", "z^lo^zeni");

    dodaj_id_nazwy_calosci("krwawnik");
    ustaw_id_short_calosci("ziele krwawnika");

    ustaw_unid_nazwe_calosci("ro^slina");
    dodaj_unid_przym_calosci("niewysoki", "niewysocy");
    dodaj_unid_przym_calosci("jasnozielony", "jasnozieloni");

    set_id_long("Sztywne, lekko omsza^le ^lodygi seledynowej barwy otulone "+
		"przez pierzaste listeczki d^xwigaj^a na swych szczytach skupione w "+
		"baldachy drobniutkie bia^le lub r^o^zowe kwiatki krwawnika "+
		"wydzielaj^ace charakterystyczny lekko gorzkawy aromat. Wiejskie "+
		"kobiety wczesn^a wiosn^a wyciskaj^a sok z m^lodych li^sci wiedz^ac o"+
		"jego wzmacniaj^acym dzia^laniu. Nawet nieuczone znachorki stosuj^a "+
		"^swie^ze li^scie do sporz^adzania ok^lad^ow na rany, owrzodzenia i "+
		"st^luczenia wiedz^ac o ich dzia^laniu przeciwkrwotocznym i "+
		"przeciwzapalnym. Napar z krwawnika stosowany jest przez uczonych "+
		"medyk^ow w leczeniu wrzod^ow ^zo^l^adka, gdy^z dzia^la "+
		"przeciwkrwotocznie a tak^ze rozkurczowo i przeciwzapalnie. "+
		"Natomiast k^apiele z dodatkiem tego ziela ^lagodz^a b^ole "+
		"reumatyczne i koj^a stargane nerwy. \n");

    set_unid_long("Sztywne, lekko omsza^le ^lodygi seledynowej barwy otulone "+
		"przez pierzaste listeczki d^xwigaj^a na swych szczytach skupione w "+
		"baldachy drobniutkie bia^le lub r^o^zowe kwiatki wydzielaj^ace "+
		"charakterystyczny lekko gorzkawy aromat. \n");

    ustaw_czas_psucia(350, 400); // Czas psucia i czas niszczenia si^e
    ustaw_trudnosc_identyfikacji(10); // Od tej warto^sci SS_HERBALISM gracz
//b^edzie m^og^l zio^lo zidentyfikowa^c
    ustaw_czestosc_wystepowania(85); 

    ustaw_ilosc_w_calosci(5);

    ustaw_trudnosc_znalezienia(10); //Du^zy krzak, ^latwo znale^x^c

    set_amount(7); //wartosc odzycza

    ustaw_przym_zepsutego("suchy", "suche");

    add_prop(OBJ_I_VALUE, 12); //warto^s^c zio^la
    add_prop(OBJ_I_VOLUME, 150); 
    add_prop(OBJ_I_WEIGHT, 70);   

    ustaw_material(MATERIALY_IN_ROSLINA);

    ustaw_pore_roku( MT_WIOSNA | MT_LATO );
}