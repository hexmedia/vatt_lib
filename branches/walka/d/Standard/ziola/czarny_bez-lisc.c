/**
 * by Brz�zek
 * 
 * Poprawki - Krun 2007
 */

inherit "/std/herb";

#include <pl.h>
#include <herb.h>
//#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe_glowna_ziola("li��");
    dodaj_przym_ziola("czarny", "czarni");
    ustaw_nazwe_ziola("bez");	

    ustaw_nazwe("li��");

    dodaj_przym("du�y", "duzi");
    dodaj_przym("z�o�ony", "z�o�eni");

    dodaj_id_nazwy_calosci("bez");
    ustaw_id_short_calosci(({"wysoki drzewiasty krzew czarnego bzu",
        "wysokiego drzewiastego krzewu czarnego bzu", "wysokiemu drzewiastemu krzewowi czarnego bzu",
        "wysoki drzewiasty krzew czarnego bzu", "wysokim drzewiastym krzewem czarnego bzu",
        "wysokim drzewiastym krzewie czarnego bzu"}),
       ({"wysokie drzewiaste krzewy czarnego bzu", "wysokich drzewiastych krzew�w czarnego bzu",
        "wysokim drzewiastym krzewom czarnego bzu", "wysokie drzewiaste krzewy czarnego bzu",
        "wysokimi drzewiastymi krzewami czarnego bzu", "wysokich drzewiastych krzewach czarnego bzu"}),
        PL_MESKI_NOS_NZYW);

    dodaj_id_nazwy_calosci("krzew");
    dodaj_id_przym_calosci("wysoki", "wysocy");
    dodaj_id_przym_calosci("czarny", "czarni");
    dodaj_id_przym_calosci("drzewiasty", "drzewiasci");

    ustaw_unid_nazwe_calosci("krzew");
    dodaj_unid_przym_calosci("wysoki", "wysocy");
    dodaj_unid_przym_calosci("drzewiasty", "drzewiasci");

    set_id_long("Zielone, eliptyczne listki o nier�wnopi�kowanych brzegach s� "+
        "do�� ostro zako�czone i osadzone nieparzysto-pierzastosiecznie na kruchej "+
        "�ody�ce, tworz�c z�o�ony li�� o specyficzny zapachu. A jednak ok�ad ze �wie�ych, " +
        "roztartych li�ci czarnego bzu zastosowany na obrz�ki, oparzenia "+
        "i obola�e stawy przynosi ulg�. Niekt�rzy medycy podaj� te� napar z kwiat�w, "+
        "li�ci i owoc�w czarnego bzu cukrzykom.\n");

    set_unid_long("Zielone, eliptyczne listki o ostropi�kowanych brzegach "+
        "osadzone s� naprzeciwlegle na kruchej �ody�ce tworz�c du�y, " + 
        "z�o�ony li��, pachn�cy niezbyt �adnie po roztarciu.\n");

    ustaw_czas_psucia(350, 400); // Czas psucia i czas niszczenia si�
    ustaw_trudnosc_identyfikacji(17); // Od tej warto�ci SS_HERBALISM gracz b�dzie m�g� zio�o zidentyfikowa�
    ustaw_czestosc_wystepowania(70); 
    ustaw_sposob_wystepowania(HERBS_WYST_POJEDYNCZO);

    ustaw_ilosc_w_calosci(5);

    ustaw_trudnosc_znalezienia(10); //Du�y krzak, �atwo znale��

    set_amount(2); //wartosc odzycza

    ustaw_przym_zepsutego("suchy", "suche");

    add_prop(OBJ_I_VALUE, 15); //warto�� zio�a
    add_prop(OBJ_I_VOLUME, 10); 
    add_prop(OBJ_I_WEIGHT, 10);   

    ustaw_material(MATERIALY_IN_ROSLINA);

    //ustaw_pore_roku(); FIXME
}
