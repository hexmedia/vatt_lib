/**
 * 
 * 
 *
 */


inherit "/std/herb";

#include <pl.h>
#include <herb.h>
//#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe("li^s^c");
    dodaj_przym("pod^lu^zny", "pod^lu^znini");
    dodaj_przym("lancetowaty", "lancetowaci");    

    ustaw_nazwe_ziola("babka lancetowata");
    dodaj_nazwy_ziola("babka");
    dodaj_nazwy_ziola("li��");
    ustaw_nazwe_glowna_ziola("li��");

    ustaw_id_nazwe_calosci("babka lancetowata");
    dodaj_id_nazwy_calosci("babka");

    set_id_long("Pod^lu^zne li^s^cie o lancetowatym kszta^lcie i ciep^lej, "+
        "zielonej barwie, skupione w przyziemne rozetki, pokryte s^a "+
        "charakterystycznym g^l^ebokim biegnacym wzd^lu^z li^scia unerwieniem. "+
        "^Swie^ze li^scie maj^a dzia^lanie odka^zaj^ace i przeciwzapalne, nadaj^a "+
        "si^e wi^ec na ok^lady na rany, ropne owrzodzenia sk^ory, czyraki i "+
        "oparzenia. Odwary z wysuszonych li^sci stosujemy jako ^lagodny ^srodek "+
        "przeciwbiegunkowy i ^lagodz^acy objawy wrzod^ow ^zo^l^adka. Po zmieszaniu "+
        "kilku suszonych li^sci z podobn^a ilo^sci^a koszyczka rumianku, kory "+
        "d^ebu, owocu bor^owki, rdestu ptasiego, k^l^acza pi^eciornika i p^lucnicy "+
        "undrikskiej otrzymamy doskona^l^a mieszank^e przeciwbiegunkow^a "+
        "niezb^edn^a w czasie podr^o^zy. Ziele babki zastosowane w postaci "+
        "parowej nasiad^owki przyniesie ulg^e cierpi^acym na hemoroidy "+
        "szczeg^olnie uci^a^zliwe w czasie d^lu^zszych podr^o^zy. Sok wyci^sni^ety ze "+
        "zmia^zd^zonych ^swie^zych li^sci rozmieszany z cukrem to wyborny syrop o "+
        "dzia^laniu wykrztu^snym stosowany przy suchym kaszlu u doros^lych i "+
        "dzieci.\n");

    set_unid_long("Pod^lu^zne li^s^cie o lancetowatym kszta^lcie i ciep^lej, "+
        "zielonej barwie, skupione w przyziemne rozetki, pokryte s^a "+
        "charakterystycznym g^l^ebokim biegnacym wzd^lu^z li^scia unerwieniem. "+
        "Ok^lady ze ^swie^zych li^sci babki zwyczajnej doskonale nadaj^a si^e do "+
        "opatrunk^ow na niedu^ze rany. \n");

    ustaw_czas_psucia(350, 400); // schniecie niszczenie
    ustaw_trudnosc_identyfikacji(17);
    ustaw_czestosc_wystepowania(80);

    ustaw_trudnosc_znalezienia(30); //Mimo, �e zio�o pospolite, cz�ciej je mo�na spotka�,
                    //to jednak jest to zio�o na tyle ma�e, �e trzeba jak�� spostrzegawczo��
                    //i uma mie� coby je znale��.

    ustaw_przym_zepsutego("ususzony", "ususzeni");

    set_amount(5); //wartosc od^zywcza

    add_prop(OBJ_I_VOLUME, 3); //FIXME
    add_prop(OBJ_I_WEIGHT, 3); //FIXME
    add_prop(OBJ_I_VALUE, 10); //FIXME

    ustaw_material(MATERIALY_IN_ROSLINA); 

    //ustaw_pore_roku();//FIXME: Chyba ca�y rok wi�c nie trzeba ustawia�, ale g�owy nie dam sprawd� kto�.
}
