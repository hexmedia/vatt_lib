/*
 *
 *  alles - faevka, 26.03.2007
 *  Poprawki: Krun, 14.04.2007 
 */


inherit "/std/herb";

#include <pl.h>
#include <herb.h>
//#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe_ziola("ziele pio^lunu");
    dodaj_przym_zident("srebrnoszary", "srebrnoszarzy");
    dodaj_nazwy_ziola("pio^lun"); 
    dodaj_nazwy_ziola("bylica");

    ustaw_nazwe("ziele");
    dodaj_przym("srebrnoszary", "srebrnoszarzy");

    ustaw_id_nazwe_calosci("bylica pio^lun");
    dodaj_id_nazwy_calosci("pio^lun");
    ustaw_id_short_calosci("ziele pio^lunu");
    dodaj_id_przym_calosci("srebrzystoszary", "srebszystoszarzy");

    ustaw_unid_nazwe_calosci("ziele");
    dodaj_unid_przym_calosci("srebrzystoszary", "srebrzystoszarzy");

    set_id_long("Z lekko zdrewnia^lej srebrnoszarej ^lodygi wyrastaj^a "+
        "mniejsze - lekko zielonkawe i delikatne, od kt^orych z kolei "+
        "odchodz^a, rozmieszczone w r^ownych odst^epach niedu^ze, "+
        "pierzastodzielne listki. Prawie przy ka^zdym z nich zwisa tworz^acy "+
        "groniasty wiech drobny koszyczek drobnych ^z^o^ltych kwiatk^ow. "+
        "Bylica pio^lun porawia przemian^e materii oraz ma lekkie dzia^lanie "+
        "dezynfekuj^ace, ale nie mo^zna u^zywa^c go zbyt d^lugo i w du^zych "+
        "dawkach, gdy^z mo^ze prowadzi^c do silnych zatru^c, a w gorszych "+
        "przypadkach - do zaburze^n czynno^sci m^ozgu. \n");

    set_unid_long("Z lekko zdrewnia^lej srebrnoszarej ^lodygi wyrastaj^a "+
        "mniejsze - lekko zielonkawe i delikatne, od kt^orych z kolei "+
        "odchodz^a, rozmieszczone w r^ownych odst^epach niedu^ze, "+
        "pierzastodzielne listki. Prawie przy ka^zdym z nich zwisa tworz^acy "+
        "groniasty wiech drobny koszyczek drobnych ^z^o^ltych kwiatk^ow. \n");

    ustaw_czas_psucia(350, 400); //schniecie, niszczenie
    ustaw_trudnosc_identyfikacji(45); // Trza miec uma powyzej 45 coby to zidentyfikować
    ustaw_czestosc_wystepowania(80); // Bedzie wystepowac na 80% lokacji
    ustaw_sposob_wystepowania(HERBS_WYST_POJEDYNCZO); //Rosnie pojedynczo

    set_amount(1); //pozywnosc

    ustaw_trudnosc_znalezienia(40); // FIXME: Strzelałem nie wiem jak to wygląda

    ustaw_przym_zepsutego("zgni^ly", "zgnili");

    add_prop(OBJ_I_VALUE, 15); //wartosc
    add_prop(OBJ_I_VOLUME, 70); //FIXME
    add_prop(OBJ_I_WEIGHT, 70); //FIXME

    ustaw_material(MATERIALY_IN_ROSLINA);

    //ustaw_pore_roku(); FIXME
}
