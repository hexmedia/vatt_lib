/*
 * Autor: Feave 
 * Opis: ??
 * Poprawki: Krun, 14.04.2007
 */

inherit "/std/herb";

#include <pl.h>
#include <herb.h>
#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe("korze^n");
    dodaj_przym("czerwonobrunatny", "czerwonobrunatni");
    dodaj_przym("pachn^acy", "pachn^acy");

    ustaw_nazwe_glowna_ziola("korze^n");
    ustaw_nazwe_ziola("arcydzi^egiel");

    ustaw_id_nazwe_calosci("arcydzi�giel");

    ustaw_unid_nazwe_calosci("ro�lina");
    dodaj_unid_przym_calosci("wysoki", "wysocy");
    dodaj_unid_przym_calosci("pachn^acy", "pachn^acy");

    set_id_long("Cienki, lecz d^lugi korze^n arcydzi^egla, pokryty jest pod^luznymi "+
        "bruzdami. Wydziela przyjemny, korzenny zapach. Arcydzi^egiel zawiera "+
        "spore ilo�ci olejk^ow eterycznych, dzi^eki czemu dzia^la przeciwskurczowo, "+
        "wzmacniaj�co i uspokajaj�co. \n");

    set_unid_long("Cienki, lecz d^lugi korze^n, pokryty pod^luznymi bruzdami. "+
        "Wydziela przyjemny, korzenny zapach.\n");

    dodaj_komende_zbierania("wyrwij", "wyrywasz", "wyrywa", "wyrwa�");
    dodaj_komende_zbierania("zerwij", "zrywasz", "zrywa", "zerwa�", 10);
        //Jak pewnie zauwa�yli�cie(Ci co mieli ju� styczno�� z zio�ami)
        //doszed� nowy argument.. jest to szansa na udane zebranie,
        //domy�lnie jest 100, ale w przypadku gdy zrywamy takie zio�o
        //mamy szanse przypadkiem je wyrwa�, ale niewielk�, dlatego ju� w tym
        //przypadku szansa dostania korzenia == 1/10
        //Troche zagmatwa�em jak to ja zwykle, ale og�lna idea chyba jasna:P

    ustaw_czas_psucia(350, 400); // schniecie niszczenie
    ustaw_trudnosc_identyfikacji(27); 
    ustaw_czestosc_wystepowania(80);

    ustaw_trudnosc_znalezienia(20); //Zio�o du�e wi�c i trudno�� nie mo�e by� du�a, 
                                    //a �e u�ywa si� korze�.. C� trzeba wpa�� na wyrwij:P

    ustaw_przym_zepsutego("ususzony", "ususzeni");

    set_amount(15); //wartosc od^zywcza

    add_prop(OBJ_I_VOLUME, 45); 
    add_prop(OBJ_I_WEIGHT, 50); 
    add_prop(OBJ_I_VALUE, 34); 

    ustaw_material(MATERIALY_IN_ROSLINA);

    ustaw_pore_roku(MT_JESIEN);
}
