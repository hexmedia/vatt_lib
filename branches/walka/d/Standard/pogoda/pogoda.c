inherit "/std/room";

#pragma strict_types
#pragma no_clone
#pragma no_inherit
#pragma binary_save

#include <pl.h>
#include <std.h>
#include <filepath.h>
#include <stdproperties.h>
#include <macros.h>
#include <log.h>
#include <files.h>
#include <pogoda.h>

public string   dlugi();
private int     qadmin(object ob);

public void
create_room()
{
    set_short("W ogromnej maszynie - Pokoju Pogodowym Vatt'gherna");
    set_long(&dlugi());

    add_prop(ROOM_I_LIGHT, 1);
    add_prop(ROOM_I_NO_CLEANUP, 1);
    add_prop(ROOM_I_INSIDE, 1);
    add_exit("/d/Standard/wiz/wizroom", ({"wyj�cie", "z powrotem do Wielkiego Kompleksu pomieszcze� Czarodziei", "z pokoju pogodowego"}) );
    setuid();
    seteuid(getuid());
}

public string
dlugi()
{
  string ret;

  ret = "Znajdujesz si� wewn�trz maszyny Pokoju " +
    "Pogodowego Vatt'gherna.\n";

  ret += "Dost�pne komendy:\n" +
    "status\t\t\t- Wypisanie statusu obiektu pogodowego\n" +
    "lokacje\t\t\t- Wypisanie zarejestrowanych lokacji\n" +
    "pogoda <x> <y>\t\t- Wypisanie informacji o pogodzie dla obszaru (x, y)\n" +
    "warunki\t\t\t- Wypisanie prawdziwych warunk�w\n";

  if (qadmin(this_player()))
  {
    ret += "plan <x> <y> <str>\t- Za�adowanie nowego planu dla obszaru (x, y)\n";
  }

  return ret + "\n";
}

public void
init()
{
    ::init();
    add_action("get_status", "status");
    add_action("lokacje", "lokacje");
    add_action("pogoda", "pogoda");
    add_action("set_pplan", "plan");
    add_action("get_warunki", "warunki");
}

private int
qadmin(object ob)
{
	if (SECURITY->query_wiz_rank(ob->query_real_name()) >= WIZ_MAGE)
		return 1;
	return 0;
}

int
get_status(string str)
{
	if (strlen(str))
	{
		notify_fail("Ta komenda nie przyjmuje �adnych argument�w.\n");
		return 0;
	}
	POGODA_OBJECT->get_status();
	return 1;
}

int
lokacje(string str)
{
	if (strlen(str))
	{
		notify_fail("Ta komenda nie przyjmuje �adnych argument�w.\n");
		return 0;
	}
	POGODA_OBJECT->print_rooms();
	return 1;
}

int
set_pplan(string str)
{
	int x, y;
	string pl;

	if (!qadmin(this_player()))
	{
		notify_fail("Nie masz odpowiednich uprawnie� do tej operacji.\n");
		return 0;
	}
	if (!stringp(str) || sscanf(str, "%d %d %s", x, y, pl) != 3)
	{
		notify_fail("Spos�b u�ycia: plan <x> <y> <plan>\n");
		return 0;
	}
	write("Za�adowano by plan: " + pl  + " (" + x + ", " + y + ")\n");
	return 1;
}

int
pogoda(string str)
{
	int x, y;
  object plan_pogody;

	if (!stringp(str) || sscanf(str, "%d %d", x, y) != 2)
	{
		notify_fail("Spos�b u�ycia: pogoda <x> <y>\n");
		return 0;
	}
  plan_pogody = POGODA_OBJECT->get_plan(x, y);
  if (!objectp(plan_pogody)) {
    notify_fail("Nie istnieje plan pogodowy dla obszaru (" + x + ", " + y + ")\n");
    return 0;
  }
  write("Pogoda na obszarze (" + x + ", " + y + "):\n");
  write("  Zachmurzenie: ");
  switch(plan_pogody->get_zachmurzenie()) {
    case POG_ZACH_ZEROWE: write("zerowe\n"); break;
    case POG_ZACH_LEKKIE: write("lekkie\n"); break;
    case POG_ZACH_SREDNIE: write("�rednie\n"); break;
    case POG_ZACH_DUZE: write("du�e\n"); break;
    case POG_ZACH_CALKOWITE: write("ca�kowite\n"); break;
  }
  write("  Temperatura: " + plan_pogody->get_temperatura() + "\n");
  write("  Opady: ");
  switch(plan_pogody->get_opady()) {
    case POG_OP_ZEROWE: write("brak\n"); break;
    case POG_OP_D_L: write("lekki deszcz\n"); break;
    case POG_OP_D_S: write("�redni deszcz\n"); break;
    case POG_OP_D_C: write("ci�ki deszcz\n"); break;
    case POG_OP_D_O: write("oberwanie chmury\n"); break;
    case POG_OP_S_L: write("lekki �nieg\n"); break;
    case POG_OP_S_S: write("�redni �nieg\n"); break;
    case POG_OP_S_C: write("ci�ki �nieg\n"); break;
    case POG_OP_G_L: write("lekki grad\n"); break;
    case POG_OP_G_S: write("�redni grad\n"); break;
  }
  write("  Wiatry: ");
  switch(plan_pogody->get_wiatry()) {
    case POG_WI_ZEROWE: write("brak\n"); break;
    case POG_WI_LEKKIE: write("lekkie\n"); break;
    case POG_WI_SREDNIE: write("�rednie\n"); break;
    case POG_WI_DUZE: write("du�e\n"); break;
    case POG_WI_BDUZE: write("bardzo du�e\n"); break;
  }
  return 1;
}

int
get_warunki(string str)
{
	if (strlen(str))
	{
		notify_fail("Ta komenda nie przyjmuje �adnych argument�w.\n");
		return 0;
	}
    object warunki = POGODA_OBJECT->get_warunki();
    write("Zachmurzenie: " + warunki->get_orig_zachmurzenie() + "\n");
    write("Wiatr: " + warunki->get_orig_wiatr() + "\n");
    write("Temperatura: " + warunki->get_temperatura() + "\n");
	return 1;
}
