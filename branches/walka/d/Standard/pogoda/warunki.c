#pragma save_binary
#pragma strict_types

#include <pogoda.h>

string zachmurzenie;
string wiatr;
int temperatura = 21;

private static int cur_zachmurzenie = POG_ZACH_LEKKIE;
private static int cur_opady = POG_OP_ZEROWE;
private static int cur_wiatr = POG_WI_SREDNIE;

public nomask void
create()
{
    setuid();
    seteuid(getuid());
    restore_object("/d/Standard/pogoda/warunki");
    switch (zachmurzenie) {
        case "Pochmurno, okresami przeja�nienia.":
            cur_zachmurzenie = POG_ZACH_SREDNIE;
            break;
        case "Pochmurno, opady deszczu przelotnego.":
            cur_zachmurzenie = POG_ZACH_DUZE;
            cur_opady = POG_OP_D_L;
            break;
        case "Pochmurno, intensywne opady deszczu.":
            cur_zachmurzenie = POG_ZACH_DUZE;
            cur_opady = POG_OP_D_C;
            break;
        case "Pochmurno z przeja�nieniami, okresami deszcz przelotny.":
            cur_zachmurzenie = POG_ZACH_SREDNIE;
            cur_opady = POG_OP_D_L;
            break;
        case "Zachmurzenie ca�kowite.":
            cur_zachmurzenie = POG_ZACH_CALKOWITE;
            break;
        case "Pogodnie.":
            cur_zachmurzenie = POG_ZACH_ZEROWE;
            break;
        case "Pogodnie, okresami wzrost zachmurzenia do umiarkowanego.":
            cur_zachmurzenie = POG_ZACH_LEKKIE;
            break;
        case "Pochmurno, snieg przelotny.":
            cur_zachmurzenie = POG_ZACH_DUZE;
            cur_opady = POG_OP_S_L;
            break;
        case "Pochmurno, intensywne opady �niegu.":
            cur_zachmurzenie = POG_ZACH_DUZE;
            cur_opady = POG_OP_S_C;
            break;
    }
    switch (wiatr) {
        case "Cisza.":
            cur_wiatr = POG_WI_ZEROWE;
            break;
        case "Powiew.":
        case "Wiatr s�aby.":
            cur_wiatr = POG_WI_LEKKIE;
            break;
        case "Wiatr �agodny.":
            cur_wiatr = POG_WI_SREDNIE;
            break;
    }
}

// Funkcje testowe.

string
get_orig_zachmurzenie()
{
    return zachmurzenie;
}

string
get_orig_wiatr()
{
    return wiatr;
}

// Funkcje do odczytu warunk�w.

int
get_temperatura()
{
    return temperatura;
}

int
get_zachmurzenie()
{
    return cur_zachmurzenie;
}

int
get_opady()
{
    return cur_opady;
}

int
get_wiatr()
{
    return cur_wiatr;
}
