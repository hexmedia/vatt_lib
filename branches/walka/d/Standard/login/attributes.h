#define L_M_HUMAN		1
#define L_F_HUMAN		2
#define L_M_ELF			4
#define L_F_ELF			8
#define L_M_DWARF		16
#define L_F_DWARF		32
#define L_M_HALFLING		64
#define L_F_HALFLING		128
#define L_M_GNOME		256
#define L_F_GNOME		512
#define L_M_HALFELF		1024
#define L_F_HALFELF		2048

#define L_ALL			4095		/* 2^12 - 1 */

#define L_HUMAN			(L_M_HUMAN | L_F_HUMAN)
#define L_ELF			(L_M_ELF | L_F_ELF)
#define L_DWARF			(L_M_DWARF | L_F_DWARF)
#define L_HALFLING		(L_M_HALFLING | L_F_HALFLING)
#define L_GNOME			(L_M_GNOME | L_F_GNOME)
#define L_HALFELF		(L_M_HALFELF | L_F_HALFELF)

#define L_MALE			(L_ALL / 3)
#define L_FEMALE		(2 * L_MALE)

#define L_BEARDED		(L_MALE & ~L_M_ELF | L_F_DWARF)

#define L_RACE2L_MAP		(["cz�owiek":	L_HUMAN,	\
                                  "elf":	L_ELF,		\
                                  "krasnolud":	L_DWARF,	\
                                  "nizio�ek":	L_HALFLING,	\
                                  "gnom":	L_GNOME,	\
                                  "p�elf":	L_HALFELF])

#define L_RACE(pl)		(L_RACE2L_MAP[(pl)->query_race()])
#define L_G_RACE(pl)		((pl)->query_gender() == G_FEMALE ? \
                                 2 * L_RACE(pl) / 3 : L_RACE(pl) / 3)

#define L_A_BRODA ({ 							\
        ({"bia^lobrody", "bia^lobrodzi", L_BEARDED}),			\
        ({"b^l^ekitnobrody", "b^l^ekitnobrodzi", L_DWARF}),			\
        ({"brodaty", "brodaci", L_BEARDED}),				\
        ({"ciemnobrody", "ciemnobrodzi", L_BEARDED}),			\
        ({"czarnobrody", "czarnobrodzi", L_BEARDED}),			\
        ({"czerwonobrody", "czerwonobrodzi", L_BEARDED}),		\
        ({"d^lugobrody", "d^lugobrodzi", L_BEARDED}),			\
        ({"jasnobrody", "jasnobrodzi", L_BEARDED}),			\
        ({"kr^otkobrody", "kr^otkobrodzi", L_BEARDED}),			\
        ({"ognistobrody", "ognistobrodzi", L_BEARDED}),	\
        ({"ogolony", "ogoleni", L_BEARDED & ~L_DWARF}),	\
        ({"p^lomiennobrody", "p^lomiennobrodzi", L_DWARF}),		\
        ({"pomara^nczowobrody", "pomara^nczowobrodzi", L_DWARF}),		\
        ({"rudobrody", "rudobrodzi", L_BEARDED}),			\
        ({"sinobrody", "sinobrodzi", L_BEARDED}),			\
        ({"siwobrody", "siwobrodzi", L_BEARDED}),			\
        ({"zaro^sni^ety", "zaro^sni^eci", L_BEARDED & ~L_F_DWARF}),		\
        ({"zielonobrody", "zielonobrodzi", L_DWARF}),			\
        ({"^z^o^ltobrody", "^z^o^ltobrodzi", L_BEARDED}),			\
})

#define L_A_BUDOWA ({ 							\
        ({"barczysty", "barczysci", L_M_HUMAN | L_DWARF | L_M_HALFELF}),		\
        ({"bary^lkowaty", "bary^lkowaci", ~L_ELF}),			\
        ({"beczkowaty", "beczkowaci", ~L_ELF}),				\
        ({"brzuchaty", "brzuchaci", L_MALE & ~L_M_ELF}),		\
        ({"chudy", "chudzi", ~L_DWARF & ~L_HALFLING}),			\
        ({"ci^e^zki", "ci^e^zcy", L_HUMAN | L_DWARF}),		\
        ({"d^lugonogi", "d^lugonodzy", L_HUMAN | L_HALFELF | L_ELF}),			\
        ({"d^lugor^eki", "d^lugor^ecy", L_HUMAN | L_HALFELF}),			\
        ({"garbaty", "garbaci", ~L_ELF & ~L_HALFLING}),			\
        ({"grubonogi", "grubonodzy", L_HUMAN | L_HALFELF}),		\
        ({"gruby", "grubi", ~L_ELF & ~L_GNOME}),			\
        ({"kab^l^akonogi", "kab^l^akonodzy", ~L_ELF & ~L_HALFLING}),	\
        ({"ko^scisty", "ko^scisci", ~L_HALFLING}),			\
        ({"ko^slawy", "ko^slawi", ~L_ELF & ~L_HALFLING & ~L_HALFELF}),			\
        ({"kr^epy", "kr^epi", L_MALE & ~L_M_ELF | L_F_DWARF}),		\
        ({"kr^otkonogi", "kr^otkonodzy", ~L_ELF}),			\
        ({"krzywonogi", "krzywonodzy", ~L_ELF & ~L_HALFLING}),		\
        ({"kulawy", "kulawi", ~L_ELF & ~L_HALFLING}),			\
        ({"lekki", "lekcy", ~L_DWARF}),			\
        ({"ma^ly", "mali", L_GNOME | L_HALFLING}),				\
        ({"masywny", "masywni", L_M_HUMAN | L_DWARF | L_M_HALFELF}),		\
        ({"muskularny", "muskularni", L_M_HUMAN | L_DWARF | L_M_HALFELF}),	\
        ({"niepozorny", "niepozorni", ~L_DWARF & ~L_ELF}),	\
        ({"niewysoki", "niewysocy", L_ALL}),				\
        ({"niski", "niscy",  ~L_ELF}),			\
        ({"p^ekaty", "p^ekaci", ~L_ELF & ~L_GNOME}),			\
        ({"piersiasty", "piersia^sci", L_F_HUMAN, L_F_HALFELF, L_F_DWARF}),	\
        ({"przygarbiony", "przygarbieni", ~L_ELF}),			\
        ({"pulchny", "pulchni", ~L_ELF & ~L_M_DWARF}),	\
        ({"smuk^ly", "smukli", L_HUMAN | L_ELF | L_HALFELF}),			\
        ({"szczup^ly", "szczupli", L_ALL}),				\
        ({"szpotawy", "szpotawi", ~L_ELF & ~L_HALFLING}),		\
        ({"t^lu^sciutki", "t^lu^sciutcy", L_ALL}),			\
        ({"t^lusty", "t^lu^sci", L_HUMAN | L_HALFLING}),			\
        ({"umi^e^sniony", "umi^e^snieni", L_M_HUMAN | L_DWARF | L_M_HALFELF}),	\
        ({"w^at^ly", "w^atli", ~L_DWARF}),					\
        ({"wielki", "wielcy", L_HUMAN | L_DWARF}),			\
        ({"wysoki", "wysocy", L_HUMAN | L_ELF | L_HALFELF}),		\
        ({"zwalisty", "zwali^sci", L_HUMAN | L_HALFELF}),			\
})

#define L_A_OCZY ({ 							\
        ({"b^l^ekitnooki", "b^l^ekitnoocy", L_ALL}),			\
        ({"br^azowooki", "br^azowoocy", L_ALL}),				\
        ({"ciemnooki", "ciemnoocy", L_ALL}),				\
        ({"czarnooki", "czarnoocy", L_ALL}),				\
        ({"czerwonooki", "czerwonoocy", ~L_ELF & ~L_HALFLING & ~L_HALFELF}),		\
        ({"fio^lkowooki", "fio^lkowoocy", L_F_HUMAN | L_ELF | L_F_HALFLING | L_F_HALFELF}),\
        ({"jasnooki", "jasnoocy", L_ALL}),				\
        ({"jednooki", "jednoocy", L_ALL}),				\
        ({"kociooki", "kocioocy", L_F_HUMAN | L_ELF | L_HALFELF | L_F_HALFLING}),	\
        ({"migda^lowooki", "migda^lowoocy", L_F_HUMAN | L_F_ELF | L_F_HALFELF | L_F_HALFLING}),\
        ({"modrooki", "modroocy", L_HUMAN | L_ELF | L_HALFELF | L_HALFLING}),	\
        ({"niebieskooki", "niebieskoocy", L_ALL}),			\
        ({"piwnooki", "piwnoocy", L_HUMAN | L_HALFELF | L_DWARF | L_HALFLING}),	\
        ({"p^lomiennooki", "p^lomiennoocy", L_HUMAN | L_HALFELF | L_ELF}),		\
        ({"promiennooki", "promiennoocy", L_F_HUMAN | L_F_HALFELF | L_ELF | L_F_HALFLING}),\
        ({"rybiooki", "rybioocy", ~L_ELF & ~L_HALFLING}),		\
        ({"sko^snooki", "sko^snoocy", ~L_DWARF & ~L_HALFLING}),		\
        ({"srebrnooki", "srebrnoocy", L_ELF}),				\
        ({"szarooki", "szaroocy", L_ALL}),				\
        ({"wy^lupiastooki", "wy^lupiastoocy", ~L_ELF & ~L_HALFLING}),	\
        ({"zezowaty", "zezowaci", ~L_ELF & ~L_HALFELF & ~L_HALFLING}),		\
        ({"zielonooki", "zielonoocy", L_ALL}),			\
        ({"z^lotooki", "z^lotoocy", L_HUMAN | L_ELF | L_HALFELF | L_HALFLING}),	\
        ({"^z^o^ltooki", "^z^o^ltoocy", ~L_ELF & ~L_HALFLING}),		\
})

#define L_A_OGOLNE ({							\
        ({"brudny", "brudni", ~L_ELF & ~L_HALFLING}),			\
        ({"dumny", "dumni", ~L_HALFLING}),					\
        ({"energiczny", "energiczni", L_ALL}),				\
        ({"hardy", "hardzi", ~L_HALFLING}),				\
        ({"ma^lom^owny", "ma^lom^owni", L_ALL}),				\
        ({"melancholijny", "melancholijni", L_ALL}),			\
        ({"nerwowy", "nerwowi", L_ALL}),				\
        ({"nie^smia^ly", "nie^smiali", ~L_DWARF}),		\
        ({"opanowany", "opanowani", L_ALL}),				\
        ({"przyjacielski", "przyjacielscy", L_ALL}),			\
        ({"rozmowny", "rozmowni", L_ALL}),				\
        ({"^smierdz^acy", "^smierdz^acy", ~L_ELF & ~L_HALFLING & ~L_HALFELF}),		\
        ({"spokojny", "spokojni", L_ALL}),				\
        ({"t^epy", "t^epi", ~L_ELF & ~L_HALFELF}),					\
        ({"weso^ly", "weseli", L_ALL}),					\
        ({"wynios^ly", "wynio^sli", ~L_HALFLING}),			\
})

#define L_A_SKORA ({ 							\
        ({"bladosk^ory", "bladosk^orzy", L_ALL}),				\
        ({"ciemnosk^ory", "ciemnosk^orzy", ~L_HALFLING}),			\
        ({"czarnosk^ory", "czarnosk^orzy", L_HUMAN}),			\
        ({"czerwonosk^ory", "czerwonosk^orzy", L_HUMAN}),			\
        ({"jasnosk^ory", "jasnosk^orzy", ~L_ELF}),			\
        ({"kosmaty", "kosmaci", L_M_HALFLING}),		\
        ({"oliwkowy", "oliwkowi", L_HUMAN | L_GNOME}),			\
        ({"opalony", "opaleni", L_ALL}),				\
        ({"r^o^zowiutki", "r^o^zowiutcy", L_HUMAN | L_HALFLING}),		\
        ({"smag^ly", "smagli", ~L_DWARF & ~L_GNOME}),		\
        ({"^sniady", "^sniadzi", ~L_ELF}),				\
        ({"w^lochaty", "w^lochaci", L_M_HUMAN, L_M_HALFLING}),	\
        ({"wylenia^ly", "wyleniali", L_M_GNOME}),		\
        ({"^z^o^lty", "^z^o^lci", L_HUMAN | L_HALFELF}),					\
})

#define L_A_TWARZ ({ 							\
        ({"bezz^ebny", "bezz^ebni", ~L_ELF & L_HALFELF}),				\
        ({"blady", "bladzi", L_ALL}),					\
        ({"czerwononosy", "czerwononosi", ~L_ELF & ~L_HALFELF}),	\
        ({"czerwony", "czerwoni", ~L_ELF & ~L_GNOME & ~L_HALFELF}),			\
        ({"d^lugonosy", "d^lugonosi", L_ALL}),				\
        ({"d^lugouchy", "d^lugousi", L_ALL}),				\
        ({"dziobaty", "dziobaci", L_ALL}),				\
        ({"k^lapouchy", "k^lapousi", L_GNOME}),			\
        ({"kostropaty", "kostropaci", ~L_ELF & ~L_HALFELF & ~L_HALFLING}),		\
        ({"krostowaty", "krostowaci", ~L_ELF & ~L_HALFLING}),		\
        ({"kr^otkonosy", "kr^otkonosi", L_ALL}),				\
        ({"kr^otkouchy", "kr^otkousi", ~L_ELF & ~L_HALFELF & ~L_HALFLING}),		\
        ({"krzywonosy", "krzywonosi", L_ALL}),				\
        ({"ogorza^ly", "ogorzali", L_ALL}),				\
        ({"ospowaty", "ospowaci", L_ALL}),				\
        ({"ostronosy", "ostronosi", L_ALL}),				\
        ({"ostrouchy", "ostrousi", ~L_HUMAN & ~L_DWARF}),	\
        ({"piegowaty", "piegowaci", L_ALL}),				\
        ({"p^laskonosy", "p^laskonosi", ~L_ELF}),				\
        ({"pomarszczony", "pomarszczeni", ~L_ELF}),			\
        ({"pryszczaty", "pryszczaci", L_ALL}),				\
        ({"pucu^lowaty", "pucu^lowaci", ~L_ELF}),				\
        ({"pyzaty", "pyzaci", ~L_ELF & ~L_DWARF}),			\
        ({"rumiany", "rumiani", ~L_ELF}),				\
        ({"spiczastouchy", "spiczastousi", ~L_HUMAN & ~L_DWARF}),\
        ({"szczerbaty", "szczerbaci", L_ALL}),				\
        ({"w^asaty", "w^asaci", L_BEARDED}),				\
        ({"wielkonosy", "wielkonosi", L_ALL}),				\
        ({"wielkouchy", "wielkousi", L_ALL}),				\
})

#define L_A_WIEK ({ 							\
        ({"bardzo m^lody", "bardzo m^lodzi", L_ALL}),			\
        ({"bardzo stary", "bardzo starzy", L_ALL}),			\
        ({"dojrza^ly", "dojrza^li", L_ALL}),				\
        ({"leciwy", "leciwi", ~L_ELF}),			\
        ({"m^lody", "m^lodzi", L_ALL}),					\
        ({"podstarza^ly", "podstarzali", ~L_ELF}),			\
        ({"stary", "starzy", L_ALL}),					\
        ({"wiekowy", "wiekowi", L_ALL}),				\
})

#define L_A_WLOSY ({							\
        ({"bia^low^losy", "bia^low^losi", L_ALL}),			\
        ({"czarnow^losy", "czarnow^losi", L_ALL}),			\
        ({"ciemnow^losy", "ciemnow^losi", L_ALL}),			\
        ({"czerwonow^losy", "czerwonow^losi", L_ALL}),			\
        ({"d^lugow^losy", "d^lugow^losi", L_ALL}),				\
        ({"jasnow^losy", "jasnow^losi", L_ALL}),			\
        ({"kasztanowow^losy", "kasztanowow^losi", L_F_HUMAN | L_ELF | L_HALFLING}),\
        ({"k^edzierzawy",  "k^edzierzawi", L_ALL}),			\
        ({"kr^otkow^losy", "kr^otkow^losi", L_ALL}),			\
        ({"kruczow^losy", "kruczow^losi", L_HUMAN, L_ELF}),		\
        ({"kud^laty", "kud^laci", L_ALL}),				\
        ({"^lysiej^acy", "^lysiej^acy", ~L_ELF}),				\
        ({"^lysy", "^lysi", ~L_ELF}),					\
        ({"ognistow^losy", "ognistow^losi", L_ALL}),			\
        ({"pomara^nczowow^losy", "pomara^nczowow^losi", L_DWARF}),		\
        ({"rudow^losy", "rudow^losi", L_ALL}),				\
        ({"rudy", "rudzi", L_ALL}),					\
        ({"ry^zy", "ry^zy", ~L_ELF & ~L_HALFLING}),			\
        ({"siwy", "siwi", ~L_ELF}),					\
        ({"srebrnow^losy", "srebrnow^losi", L_ELF | L_HALFELF}),			\
        ({"szpakowaty", "szpakowaci", ~L_ELF}),				\
        ({"zielonow^losy", "zielonow^losi", L_ELF | L_HALFELF}),			\
        ({"z^lotow^losy", "z^lotow^losi", ~L_GNOME & ~L_DWARF}),	\
})

#define L_ATTRIBUTES ([ 	\
        "broda"	: L_A_BRODA,	\
        "budowa": L_A_BUDOWA,	\
        "ogolne": L_A_OGOLNE,	\
        "oczy"	: L_A_OCZY,	\
        "skora"	: L_A_SKORA,	\
        "twarz"	: L_A_TWARZ,	\
        "wiek"	: L_A_WIEK,	\
        "wlosy"	: L_A_WLOSY,	\
])
