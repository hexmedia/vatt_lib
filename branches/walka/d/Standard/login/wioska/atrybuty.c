/*
 * system z obliczaniem dostepnych do wyboru atrybutow:
 * -kolor oczu, kolor wlosow, dlugosc wlosow
 * -broda, wasy
 * -zaleznie od zadeklarowanej wczesniej wagi i wzrostu wybieramy ceche ciala
 * -cecha charakterystyczna (np. garb, karnacja skory) Opcjonalnie do wyboru
 * -pochodzenie
 * -itd.
 *              Lil.
 *
 *
 * W mappingach na ogol trzecia wartosc jest niepotrzebna, bo mozna
 * ja odmienic z pierwszej wartosci, ale o tym zorientowalam sie dopiero pozniej
 * wiec w sumie...to trudno sie mowi. W niczym to nie przeszkadza i tak.
 */

//inherit "/d/Standard/login/wioska/std.c";
#include <composite.h>
#include <ss_types.h>

int wzrost, waga;
mixed kolor_oczu,kolor_wlosow;
string lista_kolorow_oczu;
int tmp_wiek;
string *tmp_kolor_oczu, tmp_kolor_wlosow, *tmp_przym_wlosy;
float tmp_dlugosc_wlosow;
float tmp_dlugosc_brody, tmp_dlugosc_wasow;
string tmp_budowa_ciala, tmp_pochodzenie;
string *tmp_cecha_ciala, *tmp_cecha_szczegolna/*, *dostepne_cechy_ciala*/;
string modyfikator_od_ciala;
int faza=0, faza_pomocy=0;



//dostepne kolory oczu dla kobiety:
mapping oczy_kobiety = ([
              "b喚kitne":({"b喚kitnooki","b喚kitnoocy","b喚kitnych"}),
              "br您owe": ({"br您owooki","br您owoocy","br您owych"}),
              "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
              "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
              "czerwone":({"czerwonooki","czerwonoocy","czerwonych"}),
              "fio趾owe":({"fio趾owooki","fio趾owoocy","fio趾owych"}),
              "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
              "migda這we":({"migda這wooki","migda這woocy","migda這wych"}),
              "modre":   ({"modrooki","modroocy","modrych"}),
              "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
              "piwne":   ({"piwnooki","piwnoocy","piwnych"}),
              "p這mienne":({"p這miennooki","p這miennoocy","p這miennych"}),
              "promienne":({"promiennooki","promiennoocy","promiennych"}),
              "rybie":    ({"rybiooki","rybioocy","rybich"}),
              "sko郾e":   ({"sko郾ooki","sko郾oocy","sko郾ych"}),
              "szare":    ({"szarooki","szaroocy","szarych"}),
              "鄴速e":    ({"鄴速ooki","鄴速oocy","鄴速ych"}),
              "z這te":    ({"z這tooki","z這toocy","z這tych"}),
              "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
              "matowe":    ({"matowooki","matowoocy","matowych"}),
			  "szmaragdowe":({"szmaragdowooki","szmaragdowoocy","szmaragdowych"}),
              "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla mezczyzny:
mapping oczy_mezczyzny = ([
              "b喚kitne":({"b喚kitnooki","b喚kitnoocy","b喚kitnych"}),
              "br您owe": ({"br您owooki","br您owoocy","br您owych"}),
              "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
              "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
              "czerwone":({"czerwonooki","czerwonoocy","czerwonych"}),
              "modre":   ({"modrooki","modroocy","modrych"}),
              "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
              "piwne":   ({"piwnooki","piwnoocy","piwnych"}),
              "p這mienne":({"p這miennooki","p這miennoocy","p這miennych"}),
              "rybie":    ({"rybiooki","rybioocy","rybich"}),
              "sko郾e":   ({"sko郾ooki","sko郾oocy","sko郾ych"}),
              "szare":    ({"szarooki","szaroocy","szarych"}),
              "鄴速e":    ({"鄴速ooki","鄴速oocy","鄴速ych"}),
              "z這te":    ({"z這tooki","z這toocy","z這tych"}),
              "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
              "matowe":    ({"matowooki","matowoocy","matowych"}),
              "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla elfki:
mapping oczy_elfki = ([
              "b喚kitne":({"b喚kitnooki","b喚kitnoocy","b喚kitnych"}),
              "br您owe": ({"br您owooki","br您owoocy","br您owych"}),
              "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
              "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
              "fio趾owe":({"fio趾owooki","fio趾owoocy","fio趾owych"}),
              "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
              "migda這we":({"migda這wooki","migda這woocy","migda這wych"}),
              "modre":   ({"modrooki","modroocy","modrych"}),
              "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
              "p這mienne":({"p這miennooki","p這miennoocy","p這miennych"}),
              "promienne":({"promiennooki","promiennoocy","promiennych"}),
              "srebrne":  ({"srebrnooki","srebrnoocy","srebrnych"}),
              "szare":    ({"szarooki","szaroocy","szarych"}),
              "z這te":    ({"z這tooki","z這toocy","z這tych"}),
              "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
			"szmaragdowe":({"szmaragdowooki","szmaragdowoocy","szmaragdowych"}),
			"orzechowe":	({"orzechowooki","orzechowoocy","orzechowych"}),
              "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla elfa:
mapping oczy_elfa = ([
              "b喚kitne":({"b喚kitnooki","b喚kitnoocy","b喚kitnych"}),
              "br您owe": ({"br您owooki","br您owoocy","br您owych"}),
              "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
              "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
              "fio趾owe":({"fio趾owooki","fio趾owoocy","fio趾owych"}),
              "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
              "modre":   ({"modrooki","modroocy","modrych"}),
              "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
              "p這mienne":({"p這miennooki","p這miennoocy","p這miennych"}),
              "promienne":({"promiennooki","promiennoocy","promiennych"}),
              "srebrne":  ({"srebrnooki","srebrnoocy","srebrnych"}),
              "szare":    ({"szarooki","szaroocy","szarych"}),
              "z這te":    ({"z這tooki","z這toocy","z這tych"}),
              "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
			"szmaragdowe":({"szmaragdowooki","szmaragdowoocy","szmaragdowych"}),
			"orzechowe":	({"orzechowooki","orzechowoocy","orzechowych"}),
              "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla nizio趾i:
mapping oczy_niziolki = ([
              "b喚kitne":({"b喚kitnooki","b喚kitnoocy","b喚kitnych"}),
              "br您owe": ({"br您owooki","br您owoocy","br您owych"}),
              "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
              "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
              "fio趾owe":({"fio趾owooki","fio趾owoocy","fio趾owych"}),
              "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
              "migda這we":({"migda這wooki","migda這woocy","migda這wych"}),
              "modre":   ({"modrooki","modroocy","modrych"}),
              "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
              "piwne":   ({"piwnooki","piwnoocy","piwnych"}),
              "promienne":({"promiennooki","promiennoocy","promiennych"}),
              "szare":    ({"szarooki","szaroocy","szarych"}),
              "z這te":    ({"z這tooki","z這toocy","z這tych"}),
              "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
              "matowe":    ({"matowooki","matowoocy","matowych"}),
			"szmaragdowe":({"szmaragdowooki","szmaragdowoocy","szmaragdowych"}),
			"orzechowe":	({"orzechowooki","orzechowoocy","orzechowych"}),
              "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla nizio趾a:
mapping oczy_niziolka = ([
              "b喚kitne":({"b喚kitnooki","b喚kitnoocy","b喚kitnych"}),
              "br您owe": ({"br您owooki","br您owoocy","br您owych"}),
              "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
              "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
              "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
              "modre":   ({"modrooki","modroocy","modrych"}),
              "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
              "piwne":   ({"piwnooki","piwnoocy","piwnych"}),
              "promienne":({"promiennooki","promiennoocy","promiennych"}),
              "szare":    ({"szarooki","szaroocy","szarych"}),
              "z這te":    ({"z這tooki","z這toocy","z這tych"}),
              "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
              "matowe":    ({"matowooki","matowoocy","matowych"}),
			"szmaragdowe":({"szmaragdowooki","szmaragdowoocy","szmaragdowych"}),
			"orzechowe":	({"orzechowooki","orzechowoocy","orzechowych"}),
              "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla krasnoludow (obie plcie maja takie same):
mapping oczy_krasnoludow = ([
              "b喚kitne":({"b喚kitnooki","b喚kitnoocy","b喚kitnych"}),
              "br您owe": ({"br您owooki","br您owoocy","br您owych"}),
              "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
              "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
              "czerwone":({"czerwonooki","czerwonoocy","czerwonych"}),
              "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
              "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
              "piwne":   ({"piwnooki","piwnoocy","piwnych"}),
              "rybie":    ({"rybiooki","rybioocy","rybich"}),
              "sko郾e":   ({"sko郾ooki","sko郾oocy","sko郾ych"}),
              "szare":    ({"szarooki","szaroocy","szarych"}),
              "鄴速e":    ({"鄴速ooki","鄴速oocy","鄴速ych"}),
              "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
              "matowe":    ({"matowooki","matowoocy","matowych"}),
              "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla p馧elfki:
mapping oczy_polelfki = ([
              "b喚kitne":({"b喚kitnooki","b喚kitnoocy","b喚kitnych"}),
              "br您owe": ({"br您owooki","br您owoocy","br您owych"}),
              "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
              "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
              "fio趾owe":({"fio趾owooki","fio趾owoocy","fio趾owych"}),
              "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
              "migda這we":({"migda這wooki","migda這woocy","migda這wych"}),
              "modre":   ({"modrooki","modroocy","modrych"}),
              "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
              "piwne":   ({"piwnooki","piwnoocy","piwnych"}),
              "p這mienne":({"p這miennooki","p這miennoocy","p這miennych"}),
              "promienne":({"promiennooki","promiennoocy","promiennych"}),
              "rybie":    ({"rybiooki","rybioocy","rybich"}),
              "sko郾e":   ({"sko郾ooki","sko郾oocy","sko郾ych"}),
              "szare":    ({"szarooki","szaroocy","szarych"}),
              "鄴速e":    ({"鄴速ooki","鄴速oocy","鄴速ych"}),
              "z這te":    ({"z這tooki","z這toocy","z這tych"}),
              "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
              "matowe":    ({"matowooki","matowoocy","matowych"}),
			"orzechowe":	({"orzechowooki","orzechowoocy","orzechowych"}),
              "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla p馧elfa:
mapping oczy_polelfa = ([
              "b喚kitne":({"b喚kitnooki","b喚kitnoocy","b喚kitnych"}),
              "br您owe": ({"br您owooki","br您owoocy","br您owych"}),
              "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
              "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
              "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
              "modre":   ({"modrooki","modroocy","modrych"}),
              "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
              "piwne":   ({"piwnooki","piwnoocy","piwnych"}),
              "p這mienne":({"p這miennooki","p這miennoocy","p這miennych"}),
              "rybie":    ({"rybiooki","rybioocy","rybich"}),
              "sko郾e":   ({"sko郾ooki","sko郾oocy","sko郾ych"}),
              "szare":    ({"szarooki","szaroocy","szarych"}),
              "鄴速e":    ({"鄴速ooki","鄴速oocy","鄴速ych"}),
              "z這te":    ({"z這tooki","z這toocy","z這tych"}),
              "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
              "matowe":    ({"matowooki","matowoocy","matowych"}),
              "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla gnomow (obie plcie maja te same):
mapping oczy_gnomow = ([
              "b喚kitne":({"b喚kitnooki","b喚kitnoocy","b喚kitnych"}),
              "br您owe": ({"br您owooki","br您owoocy","br您owych"}),
              "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
              "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
              "czerwone":({"czerwonooki","czerwonoocy","czerwonych"}),
              "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
              "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
              "rybie":    ({"rybiooki","rybioocy","rybich"}),
              "sko郾e":   ({"sko郾ooki","sko郾oocy","sko郾ych"}),
              "szare":    ({"szarooki","szaroocy","szarych"}),
              "鄴速e":    ({"鄴速ooki","鄴速oocy","鄴速ych"}),
              "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
              "matowe":    ({"matowooki","matowoocy","matowych"}),
              "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);

//dostepne naturalne kolory wlosow dla kobiety:
mapping wlosy_kobiety = ([
              "bia貫":   ({"bia這w這sy","bia這w這si"}),
              "czarne":  ({"czarnow這sy","czarnow這si"}),
              "ciemne":  ({"ciemnow這sy","ciemnow這si"}),
              "czerwone":({"czerwonow這sy","czerwonow這si"}),
              "jasne":   ({"jasnow這sy","jasnow這si"}),
              "kasztanowe":({"kasztanowow這sy","kasztanowow這si"}),
              "kruczoczarne":({"kruczow這sy","kruczow這si"}),
              "ogniste":  ({"ognistow這sy","ognistow這si"}),
              "rude":    ({"rudow這sy","rudow這si"}),
              "z這te":   ({"z這tow這sy","z這tow這si"}),
              "z這ciste":   ({"z這cistow這sy","z這cistow這si"}),
              "miedziane":   ({"miedzianow這sy","miedzianow這si"}),
              "siwe":    ({"siwow這sy","siwow這si"}),
              "popielate":({"popielatow這sy","popielatow這si"})
              ]);
//dostepne naturalne kolory wlosow dla mezczyzny:
mapping wlosy_mezczyzny = ([
              "bia貫":   ({"bia這w這sy","bia這w這si"}),
              "czarne":  ({"czarnow這sy","czarnow這si"}),
              "ciemne":  ({"ciemnow這sy","ciemnow這si"}),
              "czerwone":({"czerwonow這sy","czerwonow這si"}),
              "jasne":   ({"jasnow這sy","jasnow這si"}),
              "kruczoczarne":({"kruczow這sy","kruczow這si"}),
              "ogniste":  ({"ognistow這sy","ognistow這si"}),
              "rude":    ({"rudow這sy","rudow這si"}),
              "z這te":   ({"z這tow這sy","z這tow這si"}),
              "z這ciste":   ({"z這cistow這sy","z這cistow這si"}),
              "miedziane":   ({"miedzianow這sy","miedzianow這si"}),
              "siwe":    ({"siwow這sy","siwow這si"}),
              "popielate":({"popielatow這sy","popielatow這si"})
              ]);
//dostepne naturalne kolory wlosow dla elfow (obie plcie tak samo):
mapping wlosy_elfow = ([
              "bia貫":   ({"bia這w這sy","bia這w這si"}),
              "czarne":  ({"czarnow這sy","czarnow這si"}),
              "ciemne":  ({"ciemnow這sy","ciemnow這si"}),
              "czerwone":({"czerwonow這sy","czerwonow這si"}),
              "jasne":   ({"jasnow這sy","jasnow這si"}),
              "kasztanowe":({"kasztanowow這sy","kasztanowow這si"}),
              "kruczoczarne":({"kruczow這sy","kruczow這si"}),
              "ogniste":  ({"ognistow這sy","ognistow這si"}),
              "rude":    ({"rudow這sy","rudow這si"}),
              "z這te":   ({"z這tow這sy","z這tow這si"}),
              "z這ciste":   ({"z這cistow這sy","z這cistow這si"}),
              "miedziane":   ({"miedzianow這sy","miedzianow這si"}),
              "srebrne": ({"srebrnow這sy","srebrnow這si"}),
              "zielone": ({"zielonow這sy","zielonow這si"})
              ]);
//dostepne naturalne kolory wlosow dla nizio趾闚 (bez podzia逝 na p貫�):
mapping wlosy_niziolkow = ([
              "bia貫":   ({"bia這w這sy","bia這w這si"}),
              "czarne":  ({"czarnow這sy","czarnow這si"}),
              "ciemne":  ({"ciemnow這sy","ciemnow這si"}),
              "jasne":   ({"jasnow這sy","jasnow這si"}),
              "kasztanowe":({"kasztanowow這sy","kasztanowow這si"}),
              "ogniste":  ({"ognistow這sy","ognistow這si"}),
              "rude":    ({"rudow這sy","rudow這si"}),
              "z這te":   ({"z這tow這sy","z這tow這si"}),
              "z這ciste":   ({"z這cistow這sy","z這cistow這si"}),
              "miedziane":   ({"miedzianow這sy","miedzianow這si"}),
              "siwe":    ({"siwow這sy","siwow這si"}),
              "popielate":({"popielatow這sy","popielatow這si"})
              ]);
//dostepne naturalne kolory wlosow dla krasnoludow (bez podzia逝 na p貫�):
mapping wlosy_krasnoludow = ([
              "bia貫":   ({"bia這w這sy","bia這w這si"}),
              "czarne":  ({"czarnow這sy","czarnow這si"}),
              "ciemne":  ({"ciemnow這sy","ciemnow這si"}),
              "czerwone":({"czerwonow這sy","czerwonow這si"}),
              "jasne":   ({"jasnow這sy","jasnow這si"}),
              "pomara鎍zowe":({"pomara鎍zowow這sy","pomara鎍zowow這si"}),
              "ogniste":  ({"ognistow這sy","ognistow這si"}),
              "rude":    ({"rudow這sy","rudow這si"}),
              "miedziane":   ({"miedzianow這sy","miedzianow這si"}),
              "siwe":    ({"siwow這sy","siwow這si"})
              ]);
//dostepne naturalne kolory wlosow dla p馧elfow (obie plcie tak samo):
mapping wlosy_polelfow = ([
              "bia貫":   ({"bia這w這sy","bia這w這si"}),
              "czarne":  ({"czarnow這sy","czarnow這si"}),
              "ciemne":  ({"ciemnow這sy","ciemnow這si"}),
              "czerwone":({"czerwonow這sy","czerwonow這si"}),
              "jasne":   ({"jasnow這sy","jasnow這si"}),
              "kruczoczarne":({"kruczow這sy","kruczow這si"}),
              "ogniste":  ({"ognistow這sy","ognistow這si"}),
              "rude":    ({"rudow這sy","rudow這si"}),
              "z這te":   ({"z這tow這sy","z這tow這si"}),
              "z這ciste":   ({"z這cistow這sy","z這cistow這si"}),
              "miedziane":   ({"miedzianow這sy","miedzianow這si"}),
              "srebrne": ({"srebrnow這sy","srebrnow這si"}),
              ]);
//dostepne naturalne kolory wlosow dla gnom闚 (bez podzia逝 na p貫�):
mapping wlosy_gnomow = ([
              "bia貫":   ({"bia這w這sy","bia這w這si"}),
              "czarne":  ({"czarnow這sy","czarnow這si"}),
              "ciemne":  ({"ciemnow這sy","ciemnow這si"}),
              "czerwone":({"czerwonow這sy","czerwonow這si"}),
              "jasne":   ({"jasnow這sy","jasnow這si"}),
              "ogniste":  ({"ognistow這sy","ognistow這si"}),
              "rude":    ({"rudow這sy","rudow這si"}),
              "miedziane":   ({"miedzianow這sy","miedzianow這si"}),
              "siwe":    ({"siwow這sy","siwow這si"})
              ]);

//dostepne dlugosci wlosow
mapping dlugosc_wlosow = ([
              "brak":               0.0,
              "bardzo kr鏒kie":     3.0,
	      "kr鏒kie":            6.0,
	      "鈔edniej d逝go軼i":  10.0,
	      "si璕aj帷e ramion":   30.0,
	      "d逝gie":             40.0,
	      "bardzo d逝gie":      50.0,
    	      "si璕aj帷e pasa":     65.0,
	      "si璕aj帷e po這wy ud":85.0,
	      "si璕aj帷e kolan":    110.0,
	      "si璕aj帷e ziemi":    160.0
              ]);

/*
 * Cyfry na koncu tych mappingow oznaczaja na jakie cechy dany przymiotnik ma wplyw.
 * Np, gdy jest s to znaczy, ze postac bedzie troche silna po wybraniu tego.
 *
 * s-sila,
 * z-zrecznosc,
 * w-wytrzymalosc,
 * b-brak
 */

//dostepne szczegolne cechy dotyczace budowy ciala (w zastepstwie opisu wagi) dla kobiety:
mapping cialo_kobiety = ([
    "szerokie barki":   ({"barczysty","barczy軼i","barczyst�","s","osiem"}),
    "bary趾owato嗆": ({"bary趾owaty","bary趾owaci","bary趾owat�","b","osiem"}),
    "beczkowato嗆":  ({"beczkowaty","beczkowaci","beczkowat�","b","siedem"}),
    "poka幡y brzuch": ({"brzuchaty","brzuchaci","brzuchat�","b","dziewiec"}),
    "szczup豉 budowa cia豉": ({"szczup造","szczupli","szczup陰","z","trzy"}),
    "chuda budowa cia豉": ({"chudy","chudzi","chud�","z","cztery"}),
    "ci篹ka postura":  ({"ci篹ki","ci篹cy","ci篹k�","w","osiem"}),
    "ko軼isto嗆":  ({"ko軼isty","ko軼i軼i","ko軼ist�","b","siedem"}),
    "lekka budowa cia豉": ({"lekki","lekcy","lekk�","z","cztery"}),
    "gruba budowa cia豉": ({"gruby","grubi","grub�","w","szesc"}),
    "oty這嗆": ({"oty造","otyli","oty陰","w","siedem"}),
    "niepozorna budowa cia豉":({"niepozorny","niepozorni","niepozorn�","b","piec"}),
    "pulchna budowa cia豉": ({"pulchny","pulchni","pulchn�","w","piec"}),
    "smuk豉 budowa cia豉":  ({"smuk造","smukli","smuk陰","z","piec"}),
    "w徠豉 budowa cia豉": ({"w徠造","w徠li","w徠陰","b","szesc"}),
    "wielka postura":   ({"wielki","wielcy","wielk�","s","dziewiec"}),
    "t逝ste cia這":     ({"t逝sty","t逝軼i","t逝st�","w","osiem"}),
    "kr瘼a budowa cia豉": ({"kr瘼y","kr瘼i","kr瘼�","s","osiem"}),
    "puszysto嗆": ({"puszysty","puszy軼i","puszyst�","w","szesc"}),
    "drobna budowa cia豉": ({"drobny","drobni","drobn�","b","piec"}),
    "filigranowa budowa cia豉": ({"filigranowy","filigranowi","filigranow�","b","szesc"}),
    "篡lasta budowa cia豉": ({"篡lasty","篡la軼i","篡last�","b","siedem"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "proporcjonalna budowa cia豉": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonaln�","b","zero"}),
    "przeci皻na waga":  ({"przeci皻nej wagi","przeci皻nych wag","przeci皻nej wagi","b",
                          "zero"})
              ]);
//dostepne szczegolne cechy dostyczace budowy ciala (w zastepstwie opisu wagi) dla mezczyzny:
mapping cialo_mezczyzny = ([
    "masywna postura":  ({"masywny","masywni","masywnym","w","szesc"}),
    "poka幡e musku造":  ({"muskularny","muskularni","muskularnym","s","siedem"}),
    "szerokie barki":   ({"barczysty","barczy軼i","barczystym","s","szesc"}),
    "bary趾owato嗆": ({"bary趾owaty","bary趾owaci","bary趾owatym","b","siedem"}),
    "beczkowato嗆":  ({"beczkowaty","beczkowaci","beczkowatym","b","siedem"}),
    "poka幡y brzuch": ({"brzuchaty","brzuchaci","brzuchatym","b","piec"}),
    "szczup豉 budowa cia豉": ({"szczup造","szczupli","szczup造m","z","trzy"}),
    "chuda budowa cia豉": ({"chudy","chudzi","chudym","z","cztery"}),
    "ci篹ka postura":  ({"ci篹ki","ci篹cy","ci篹kim","w","osiem"}),
    "ko軼isto嗆":  ({"ko軼isty","ko軼i軼i","ko軼istym","b","osiem"}),
    "lekka budowa cia豉": ({"lekki","lekcy","lekkim","z","piec"}),
    "gruba budowa cia豉": ({"gruby","grubi","grubym","w","szesc"}),
    "oty這嗆": ({"oty造","otyli","oty造m","w","szesc"}),
    "niepozorna budowa cia豉": ({"niepozorny","niepozorni","niepozornym","b","szesc"}),
    "pulchna budowa cia豉": ({"pulchny","pulchni","pulchnym","w","piec"}),
    "smuk豉 budowa cia豉":  ({"smuk造","smukli","smuk造m","z","piec"}),
    "w徠豉 budowa cia豉": ({"w徠造","w徠li","w徠造m","b","szesc"}),
    "wielka postura":   ({"wielki","wielcy","wielkim","s","osiem"}),
    "zwalista postura": ({"zwalisty","zwali軼i","zwalistym","w","dziewiec"}),
    "t逝ste cia這":     ({"t逝sty","t逝軼i","t逝stym","w","osiem"}),
    "poka幡e mi窷nie":  ({"umi窷niony","umi窷nieni","umi窷nionym","s","szesc"}),
    "kr瘼a budowa cia豉": ({"kr瘼y","kr瘼i","kr瘼ym","s","siedem"}),
    "puszysto嗆": ({"puszysty","puszy軼i","puszystym","w","szesc"}),
    "drobna budowa cia豉": ({"drobny","drobni","drobnym","b","szesc"}),
    "篡lasta budowa cia豉": ({"篡lasty","篡la軼i","篡lastym","b","cztery"}),
    "proporcjonalna budowa cia豉": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonalnym","b","zero"}),
    "przeci皻na waga":  ({"przeci皻nej wagi","przeci皻nych wag","przeci皻nej wagi","b",
                          "zero"})
              ]);

//dostepne szczegolne cechy dotyczace budowy ciala
// (w zastepstwie opisu wagi) dla polelfki:
mapping cialo_polelfki = ([
    "beczkowato嗆":  ({"beczkowaty","beczkowaci","beczkowat�","b","osiem"}),
    "poka幡y brzuch": ({"brzuchaty","brzuchaci","brzuchat�","b","dziewiec"}),
    "szczup豉 budowa cia豉": ({"szczup造","szczupli","szczup陰","z","piec"}),
    "chuda budowa cia豉": ({"chudy","chudzi","chud�","z","szesc"}),
    "ko軼isto嗆":  ({"ko軼isty","ko軼i軼i","ko軼ist�","b","siedem"}),
    "lekka budowa cia豉": ({"lekki","lekcy","lekk�","z","piec"}),
    "oty這嗆": ({"oty造","otyli","oty陰","w","osiem"}),
    "niepozorna budowa cia豉":({"niepozorny","niepozorni","niepozorn�","b","szesc"}),
    "pulchna budowa cia豉": ({"pulchny","pulchni","pulchn�","w","siedem"}),
    "smuk豉 budowa cia豉":  ({"smuk造","smukli","smuk陰","z","szesc"}),
    "w徠豉 budowa cia豉": ({"w徠造","w徠li","w徠陰","b","szesc"}),
    "puszysto嗆": ({"puszysty","puszy軼i","puszyst�","w","osiem"}),
    "drobna budowa cia豉": ({"drobny","drobni","drobn�","b","piec"}),
    "filigranowa budowa cia豉": ({"filigranowy","filigranowi","filigranow�","b","siedem"}),
    "篡lasta budowa cia豉": ({"篡lasty","篡la軼i","篡last�","b","siedem"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "proporcjonalna budowa cia豉": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonaln�","b","zero"}),
    "przeci皻na waga":  ({"przeci皻nej wagi","przeci皻nej wagi","przeci皻nej wagi","b",
                          "zero"})
              ]);
//dostepne szczegolne cechy dostyczace budowy ciala 
//(w zastepstwie opisu wagi) dla polelfa:
mapping cialo_polelfa= ([
    "poka幡e musku造":  ({"muskularny","muskularni","muskularnym","s","siedem"}),
    "szerokie barki":   ({"barczysty","barczy軼i","barczystym","s","siedem"}),
    "beczkowato嗆":  ({"beczkowaty","beczkowaci","beczkowatym","b","osiem"}),
    "poka幡y brzuch": ({"brzuchaty","brzuchaci","brzuchatym","b","osiem"}),
    "szczup豉 budowa cia豉": ({"szczup造","szczupli","szczup造m","z","trzy"}),
    "chuda budowa cia豉": ({"chudy","chudzi","chudym","z","cztery"}),
    "ko軼isto嗆":  ({"ko軼isty","ko軼i軼i","ko軼istym","b","piec"}),
    "lekka budowa cia豉": ({"lekki","lekcy","lekkim","z","piec"}),
    "oty這嗆": ({"oty造","otyli","oty造m","w","siedem"}),
    "niepozorna budowa cia豉": ({"niepozorny","niepozorni","niepozornym","b","szesc"}),
    "pulchna budowa cia豉": ({"pulchny","pulchni","pulchnym","w","siedem"}),
    "smuk豉 budowa cia豉":  ({"smuk造","smukli","smuk造m","z","piec"}),
    "w徠豉 budowa cia豉": ({"w徠造","w徠li","w徠造m","b","piec"}),
    "poka幡e mi窷nie":  ({"umi窷niony","umi窷nieni","umi窷nionym","s","siedem"}),
    "kr瘼a budowa cia豉": ({"kr瘼y","kr瘼i","kr瘼ym","s","osiem"}),
    "puszysto嗆": ({"puszysty","puszy軼i","puszystym","w","osiem"}),
    "drobna budowa cia豉": ({"drobny","drobni","drobnym","b","szesc"}),
    "篡lasta budowa cia豉": ({"篡lasty","篡la軼i","篡lastym","b","szesc"}),
    "proporcjonalna budowa cia豉": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonalnym","b","zero"}),
    "przeci皻na waga":  ({"przeci皻nej wagi","przeci皻nej wagi","przeci皻nej wagi","b",
                          "zero"})
              ]);

//dostepne szczegolne cechy dotyczace budowy ciala
// (w zastepstwie opisu wagi) dla elfow:
mapping cialo_elfki = ([
    "szczup豉 budowa cia豉": ({"szczup造","szczupli","szczup陰","z","dwa"}),
    "chuda budowa cia豉": ({"chudy","chudzi","chud�","z","trzy"}),
    "ko軼isto嗆":  ({"ko軼isty","ko軼i軼i","ko軼ist�","b","cztery"}),
    "lekka budowa cia豉": ({"lekki","lekcy","lekk�","z","trzy"}),
    "niepozorna budowa cia豉":({"niepozorny","niepozorni","niepozorn�","b","szesc"}),
    "pulchna budowa cia豉": ({"pulchny","pulchni","pulchn�","w","osiem"}),
    "smuk豉 budowa cia豉":  ({"smuk造","smukli","smuk陰","z","trzy"}),
    "w徠豉 budowa cia豉": ({"w徠造","w徠li","w徠陰","b","cztery"}),
    "drobna budowa cia豉": ({"drobny","drobni","drobn�","b","piec"}),
    "filigranowa budowa cia豉": ({"filigranowy","filigranowi","filigranow�","b","piec"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "proporcjonalna budowa cia豉": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonaln�","b","brak"}),
    "przeci皻na waga":  ({"przeci皻nej wagi","przeci皻nej wagi","przeci皻nej wagi","b",
                          "brak"})
              ]);
mapping cialo_elfa = ([
    "szczup豉 budowa cia豉": ({"szczup造","szczupli","szczup造m","z","dwa"}),
    "chuda budowa cia豉": ({"chudy","chudzi","chudym","z","trzy"}),
    "ko軼isto嗆":  ({"ko軼isty","ko軼i軼i","ko軼istym","b","cztery"}),
    "lekka budowa cia豉": ({"lekki","lekcy","lekkim","z","trzy"}),
    "niepozorna budowa cia豉":({"niepozorny","niepozorni","niepozornym","b","szesc"}),
    "pulchna budowa cia豉": ({"pulchny","pulchni","pulchnym","w","osiem"}),
    "smuk豉 budowa cia豉":  ({"smuk造","smukli","smuk造m","z","trzy"}),
    "w徠豉 budowa cia豉": ({"w徠造","w徠li","w徠造m","b","cztery"}),
    "drobna budowa cia豉": ({"drobny","drobni","drobnym","b","piec"}),
    "filigranowa budowa cia豉": ({"filigranowy","filigranowi","filigranowym","b","piec"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "proporcjonalna budowa cia豉": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonalnym","b","brak"}),
    "przeci皻na waga":  ({"przeci皻nej wagi","przeci皻nej wagi","przeci皻nej wagi","b",
                          "brak"})
              ]);

//dostepne szczegolne cechy dotyczace budowy ciala 
//(w zastepstwie opisu wagi) dla niziolkow:
mapping cialo_niziolki = ([
    "bary趾owato嗆": ({"bary趾owaty","bary趾owaci","bary趾owat�","b","szesc"}),
    "beczkowato嗆":  ({"beczkowaty","beczkowaci","beczkowat�","b","szesc"}),
    "poka幡y brzuch": ({"brzuchaty","brzuchaci","brzuchat�","b","piec"}),
    "szczup豉 budowa cia豉": ({"szczup造","szczupli","szczup陰","z","szesc"}),
    "chuda budowa cia豉": ({"chudy","chudzi","chud�","z","siedem"}),
    "lekka budowa cia豉": ({"lekki","lekcy","lekk�","z","szesc"}),
    "gruba budowa cia豉": ({"gruby","grubi","grub�","w","siedem"}),
    "oty這嗆": ({"oty造","otyli","oty陰","w","osiem"}),
    "niepozorna budowa cia豉":({"niepozorny","niepozorni","niepozorn�","b","piec"}),
    "pulchna budowa cia豉": ({"pulchny","pulchni","pulchn�","w","piec"}),
    "w徠豉 budowa cia豉": ({"w徠造","w徠li","w徠陰","b","szesc"}),
    "t逝ste cia這":     ({"t逝sty","t逝軼i","t逝st�","w","osiem"}),
    "puszysto嗆": ({"puszysty","puszy軼i","puszyst�","w","szesc"}),
    "drobna budowa cia豉": ({"drobny","drobni","drobn�","b","szesc"}),
    "filigranowa budowa cia豉": ({"filigranowy","filigranowi","filigranow�","b","osiem"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "proporcjonalna budowa cia豉": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonaln�","b","zero"}),
    "przeci皻na waga":  ({"przeci皻nej wagi","przeci皻nej wagi","przeci皻nej wagi","b",
                          "zero"})
              ]);
mapping cialo_niziolka = ([
    "bary趾owato嗆": ({"bary趾owaty","bary趾owaci","bary趾owatym","b","szesc"}),
    "beczkowato嗆":  ({"beczkowaty","beczkowaci","beczkowatym","b","szesc"}),
    "poka幡y brzuch": ({"brzuchaty","brzuchaci","brzuchatym","b","piec"}),
    "szczup豉 budowa cia豉": ({"szczup造","szczupli","szczup造m","z","szesc"}),
    "chuda budowa cia豉": ({"chudy","chudzi","chudym","z","siedem"}),
    "lekka budowa cia豉": ({"lekki","lekcy","lekkim","z","szesc"}),
    "gruba budowa cia豉": ({"gruby","grubi","grubym","w","siedem"}),
    "oty這嗆": ({"oty造","otyli","oty造m","w","osiem"}),
    "niepozorna budowa cia豉":({"niepozorny","niepozorni","niepozornym","b","piec"}),
    "pulchna budowa cia豉": ({"pulchny","pulchni","pulchnym","w","piec"}),
    "w徠豉 budowa cia豉": ({"w徠造","w徠li","w徠造m","b","szesc"}),
    "t逝ste cia這":     ({"t逝sty","t逝軼i","t逝stym","w","osiem"}),
    "puszysto嗆": ({"puszysty","puszy軼i","puszystym","w","szesc"}),
    "drobna budowa cia豉": ({"drobny","drobni","drobnym","b","szesc"}),
    "filigranowa budowa cia豉": ({"filigranowy","filigranowi","filigranowym","b","osiem"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "proporcjonalna budowa cia豉": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonalnym","b","zero"}),
    "przeci皻na waga":  ({"przeci皻nej wagi","przeci皻nej wagi","przeci皻nej wagi","b",
                          "zero"})
              ]);



//dostepne szczegolne cechy dotyczace budowy ciala 
// (w zastepstwie opisu wagi) dla krasnoludki:
mapping cialo_krasnoludki = ([
    "szerokie barki":   ({"barczysty","barczy軼i","barczyst�","s","osiem"}),
    "bary趾owato嗆": ({"bary趾owaty","bary趾owaci","bary趾owat�","b","osiem"}),
    "beczkowato嗆":  ({"beczkowaty","beczkowaci","beczkowat�","b","siedem"}),
    "poka幡y brzuch": ({"brzuchaty","brzuchaci","brzuchat�","b","siedem"}),
    "szczup豉 budowa cia豉": ({"szczup造","szczupli","szczup陰","z","szesc"}),
    "chuda budowa cia豉": ({"chudy","chudzi","chud�","z","siedem"}),
    "lekka budowa cia豉": ({"lekki","lekcy","lekk�","z","szesc"}),
    "gruba budowa cia豉": ({"gruby","grubi","grub�","w","siedem"}),
    "oty這嗆": ({"oty造","otyli","oty陰","w","siedem"}),
    "niepozorna budowa cia豉":({"niepozorny","niepozorni","niepozorn�","b","osiem"}),
    "pulchna budowa cia豉": ({"pulchny","pulchni","pulchn�","w","piec"}),
    "t逝ste cia這":     ({"t逝sty","t逝軼i","t逝st�","w","siedem"}),
    "kr瘼a budowa cia豉": ({"kr瘼y","kr瘼i","kr瘼�","s","piec"}),
    "puszysto嗆": ({"puszysty","puszy軼i","puszyst�","w","szesc"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "przeci皻na waga":  ({"przeci皻nej wagi","przeci皻nych wag","przeci皻nej wagi","b",
                          "zero"})
              ]);
//dostepne szczegolne cechy dostyczace budowy ciala 
//(w zastepstwie opisu wagi) dla krasnoluda:
mapping cialo_krasnoluda = ([
    "masywna postura":  ({"masywny","masywni","masywnym","w","piec"}),
    "poka幡e musku造":  ({"muskularny","muskularni","muskularnym","s","szesc"}),
    "szerokie barki":   ({"barczysty","barczy軼i","barczystym","s","cztery"}),
    "bary趾owato嗆": ({"bary趾owaty","bary趾owaci","bary趾owatym","b","szesc"}),
    "beczkowato嗆":  ({"beczkowaty","beczkowaci","beczkowatym","b","szesc"}),
    "poka幡y brzuch": ({"brzuchaty","brzuchaci","brzuchatym","b","piec"}),
    "chuda budowa cia豉": ({"chudy","chudzi","chudym","z","siedem"}),
    "ci篹ka postura":  ({"ci篹ki","ci篹cy","ci篹kim","w","szesc"}),
    "lekka budowa cia豉": ({"lekki","lekcy","lekkim","z","siedem"}),
    "gruba budowa cia豉": ({"gruby","grubi","grubym","w","szesc"}),
    "oty這嗆": ({"oty造","otyli","oty造m","w","siedem"}),
    "niepozorna budowa cia豉": ({"niepozorny","niepozorni","niepozornym","b","siedem"}),
    "pulchna budowa cia豉": ({"pulchny","pulchni","pulchnym","w","piec"}),
    "zwalista postura": ({"zwalisty","zwali軼i","zwalistym","w","siedem"}),
    "t逝ste cia這":     ({"t逝sty","t逝軼i","t逝stym","w","siedem"}),
    "poka幡e mi窷nie":  ({"umi窷niony","umi窷nieni","umi窷nionym","s","piec"}),
    "kr瘼a budowa cia豉": ({"kr瘼y","kr瘼i","kr瘼ym","s","cztery"}),
    "puszysto嗆": ({"puszysty","puszy軼i","puszystym","w","szesc"}),
    "przeci皻na waga":  ({"przeci皻nej wagi","przeci皻nej wagi","przeci皻nej wagi","b",
                          "zero"})
              ]);

//dostepne szczegolne cechy dotyczace budowy ciala 
// (w zastepstwie opisu wagi) dla gnomki:
mapping cialo_gnomki = ([
    "poka幡y brzuch": ({"brzuchaty","brzuchaci","brzuchat�","b","siedem"}),
    "szczup豉 budowa cia豉": ({"szczup造","szczupli","szczup陰","z","piec"}),
    "chuda budowa cia豉": ({"chudy","chudzi","chud�","z","szesc"}),
    "lekka budowa cia豉": ({"lekki","lekcy","lekk�","z","piec"}),
    "oty這嗆": ({"oty造","otyli","oty陰","w","osiem"}),
    "w徠豉 budowa cia豉": ({"w徠造","w徠li","w徠造m","b","szesc"}),
    "niepozorna budowa cia豉":({"niepozorny","niepozorni","niepozorn�","b","szesc"}),
    "pulchna budowa cia豉": ({"pulchny","pulchni","pulchn�","w","siedem"}),
    "kr瘼a budowa cia豉": ({"kr瘼y","kr瘼i","kr瘼�","s","siedem"}),
    "puszysto嗆": ({"puszysty","puszy軼i","puszyst�","w","siedem"}),
    "drobna budowa cia豉": ({"drobny","drobni","drobn�","b","piec"}),
    "filigranowa budowa cia豉": ({"filigranowy","filigranowi","filigranow�","b","szesc"}),
    "篡lasta budowa cia豉": ({"篡lasty","篡la軼i","篡last�","b","szesc"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "proporcjonalna budowa cia豉": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonaln�","b","zero"}),
    "przeci皻na waga":  ({"przeci皻nej wagi","przeci皻nej wagi","przeci皻nej wagi","b",
                          "zero"})
              ]);
//dostepne szczegolne cechy dostyczace budowy ciala 
//(w zastepstwie opisu wagi) dla gnoma:
mapping cialo_gnoma = ([
    "masywna postura":  ({"masywny","masywni","masywnym","w","osiem"}),
    "poka幡e musku造":  ({"muskularny","muskularni","muskularnym","s","siedem"}),
    "szerokie barki":   ({"barczysty","barczy軼i","barczystym","s","siedem"}),
    "bary趾owato嗆": ({"bary趾owaty","bary趾owaci","bary趾owatym","b","szesc"}),
    "beczkowato嗆":  ({"beczkowaty","beczkowaci","beczkowatym","b","siedem"}),
    "poka幡y brzuch": ({"brzuchaty","brzuchaci","brzuchatym","b","siedem"}),
    "chuda budowa cia豉": ({"chudy","chudzi","chudym","z","szesc"}),
    "lekka budowa cia豉": ({"lekki","lekcy","lekkim","z","szesc"}),
    "gruba budowa cia豉": ({"gruby","grubi","grubym","w","siedem"}),
    "oty這嗆": ({"oty造","otyli","oty造m","w","siedem"}),
    "niepozorna budowa cia豉": ({"niepozorny","niepozorni","niepozornym","b","szesc"}),
    "pulchna budowa cia豉": ({"pulchny","pulchni","pulchnym","w","szesc"}),
    "w徠豉 budowa cia豉": ({"w徠造","w徠li","w徠造m","b","szesc"}),
    "zwalista postura": ({"zwalisty","zwali軼i","zwalistym","w","osiem"}),
    "t逝ste cia這":     ({"t逝sty","t逝軼i","t逝stym","w","osiem"}),
    "poka幡e mi窷nie":  ({"umi窷niony","umi窷nieni","umi窷nionym","s","siedem"}),
    "kr瘼a budowa cia豉": ({"kr瘼y","kr瘼i","kr瘼ym","s","siedem"}),
    "puszysto嗆": ({"puszysty","puszy軼i","puszystym","w","siedem"}),
    "篡lasta budowa cia豉": ({"篡lasty","篡la軼i","篡lastym","b","szesc"}),
    "proporcjonalna budowa cia豉": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonalnym","b","zero"}),
    "przeci皻na waga":  ({"przeci皻nej wagi","przeci皻nej wagi","przeci皻nej wagi","b",
                          "zero"})
              ]);





//cechy 'szczegolne' dla kobiet (kazdej rasy):
mapping cechy_szczegolne_kobiet= ([
      "garb":        ({"garbaty","garbaci","garbat�","noelf"}),
      //KULAWY?
      "poka幡e piersi": ({"piersiasty","piersia軼i","piersiast�","noelf"}),
      "lekki garb": ({"przygarbiony","przygarbieni","przygarbion�","noelf"}),
      "ko郵awe nogi": ({"ko郵awy","ko郵awi","ko郵aw�","noelf"}),
      "kr鏒kie nogi": ({"kr鏒konogi","kr鏒konodzy","kr鏒konog�","noelf"}),
      "krzywe nogi":  ({"krzywonogi","krzywonodzy","krzywonog�","noelf"}),
      "grube nogi":  ({"grubonogi","grubonodzy","grubonog�","noelf"}),
      "d逝gie nogi": ({"d逝gonogi","d逝gonodzy","d逝gonog�"}),
      "d逝gie r璚e": ({"d逝gor瘯i","d逝gor璚y","d逝gor瘯�","noelf"}),
      "blada sk鏎a":  ({"bladosk鏎y","bladosk鏎zy","bladosk鏎�"}),
      "ciemna sk鏎a": ({"ciemnosk鏎y","ciemnosk鏎zy","ciemnosk鏎�","noelf"}),
      "czarna sk鏎a": ({"czarnosk鏎y","czarnosk鏎zy","czarnosk鏎�","noelf"}),
      "czerwona sk鏎a": ({"czerwonosk鏎y","czerwonosk鏎zy","czerwonosk鏎�","noelf"}),
      //TE CHYBA NIE SA 'szczegolne' :)
      //"jasna sk鏎a":  ({"jasnosk鏎y","jasnosk鏎zy","jasnosk鏎�"}),
      //"smag豉 sk鏎a":  ({"smag造","smagli","smag陰"}),
      //"郾iada sk鏎a":  ({"郾iady","郾iadzi","郾iad�"}),
      "鄴速a sk鏎a":   ({"鄴速osk鏎y","鄴速osk鏎zy","鄴速osk鏎�","noelf"}),
      //TWARZ:
      "brak uz瑿ienia": ({"bezz瑿ny","bezz瑿ni","bezzebn�","noelf"}),
      "czerwony nos":   ({"czerwononosy","czerwononosi","czerwononos�","noelf"}),
      "d逝gi nos":     ({"d逝gonosy","d逝gonosi","d逝gonos�","noelf"}),
      "d逝gie uszy":   ({"d逝gouchy","d逝gousi","d逝gouch�"}),
      "kostropato嗆":  ({"kostropaty","kostropaci","kostropat�","noelf"}),
      "krzywy nos":    ({"krzywonosy","krzywonosi","krzywonos�","noelf"}),
      "ogorza豉 twarz": ({"ogorza造","ogorzali","ogorza陰","noelf"}),
      "ostry nos":     ({"ostronosy","ostronosi","ostronos�"}),
      "ostre uszy":    ({"ostrouchy","ostrousi","ostrouch�"}),
      "liczne piegi":  ({"piegowaty","piegowaci","piegowat�"}),
      "pyzata twarz":  ({"pyzaty","pyzaci","pyzat�","noelf"}),
      "rumiana twarz": ({"rumiany","rumiani","rumian�","noelf"}),
      "wielki nos":    ({"wielkonosy","wielkonosi","wielkonos�","noelf"}),
      "wielkie uszy":  ({"wielkouchy","wielkousi","wielkouch�"})
              ]);

//cechy 'szczegolne' dla mezczyzn (kazdej rasy):
mapping cechy_szczegolne_mezczyzn= ([
      "garb":        ({"garbaty","garbaci","garbatym","noelf"}),
      "lekki garb": ({"przygarbiony","przygarbieni","przygarbionym","noelf"}),
      "ko郵awe nogi": ({"ko郵awy","ko郵awi","ko郵awym","noelf"}),
      "kr鏒kie nogi": ({"kr鏒konogi","kr鏒konodzy","kr鏒konogim","noelf"}),
      "krzywe nogi":  ({"krzywonogi","krzywonodzy","krzywonogim","noelf"}),
      "grube nogi":  ({"grubonogi","grubonodzy","grubonogim","noelf"}),
      "d逝gie nogi": ({"d逝gonogi","d逝gonodzy","d逝gonogim"}),
      "d逝gie r璚e": ({"d逝gor瘯i","d逝gor璚y","d逝gor瘯im"}),
      "blada sk鏎a":  ({"bladosk鏎y","bladosk鏎zy","bladosk鏎ym"}),
      "ciemna sk鏎a": ({"ciemnosk鏎y","ciemnosk鏎zy","ciemnosk鏎ym","noelf"}),
      "czarna sk鏎a": ({"czarnosk鏎y","czarnosk鏎zy","czarnosk鏎ym","noelf"}),
      "czerwona sk鏎a": ({"czerwonosk鏎y","czerwonosk鏎zy","czerwonosk鏎ym","noelf"}),
      //TE CHYBA NIE SA 'szczegolne' :)
      //"jasna sk鏎a":  ({"jasnosk鏎y","jasnosk鏎zy","jasnosk鏎ym"}),
      //"smag豉 sk鏎a":  ({"smag造","smagli","smag造m"}),
      //"郾iada sk鏎a":  ({"郾iady","郾iadzi","郾iadym"}),
      "鄴速a sk鏎a":   ({"鄴速osk鏎y","鄴速osk鏎zy","鄴速osk鏎ym","noelf"}),
      //TWARZ:
      "brak uz瑿ienia": ({"bezz瑿ny","bezz瑿ni","bezz瑿nym","noelf"}),
      "czerwony nos":   ({"czerwononosy","czerwononosi","czerwononosym","noelf"}),
      "d逝gi nos":     ({"d逝gonosy","d逝gonosi","d逝gonosym"}),
      "d逝gie uszy":   ({"d逝gouchy","d逝gousi","d逝gouchym"}),
      "kostropato嗆":  ({"kostropaty","kostropaci","kostropatym","noelf"}),
      "krzywy nos":    ({"krzywonosy","krzywonosi","krzywonosym","noelf"}),
      "ogorza豉 twarz": ({"ogorza造","ogorzali","ogorza造m","noelf"}),
      "ostry nos":     ({"ostronosy","ostronosi","ostronosym"}),
      "ostre uszy":    ({"ostrouchy","ostrousi","ostrouchym"}),
      "liczne piegi":  ({"piegowaty","piegowaci","piegowatym"}),
      "pyzata twarz":  ({"pyzaty","pyzaci","pyzatym","noelf"}),
      "rumiana twarz": ({"rumiany","rumiani","rumianym","noelf"}),
      "wielki nos":    ({"wielkonosy","wielkonosi","wielkonosym","noelf"}),
      "wielkie uszy":  ({"wielkouchy","wielkousi","wielkouchym"})
              ]);

//tablica pochodzenia jest wspolna dla kazdej rasy.
//UWAGA, jak kto� tu b璠zie chcia� co� dopisa�, to prosz� te�
//zerkn望 na funkcje powiedz_pochodzenie() w std!!!
/*UWAGA2!!! Zakomentowa貫m miejsca inne ni� wsie i miasta dost瘼ne
 ka盥ym. Ju� nie b璠zie mo積a wybra� np. G鏎 Sinych, albo Mahakamu -
 takie pochodzenie 鈍iadczy o jakim� ju� statucie, nie mo瞠 by� tak
 瞠 pierwszy lepszy elf, czy krasnolud mo瞠 pochodzi� z takich miejsc
 (analogicznie - Tir Tochair dla gnom闚). Je郵i chce, niech z這篡
 podanie. Oczywi軼ie o ile nie ma tego miejsca, gdzie mo積a zdoby�
 te pochodzenie!! :)
 Vera. PROSZ� NIE EDYTOWA� BEZ UZGODNIENIA TEGO ZE MN�
 */
string *pochodzenie=({
               "z Rinde",               //"z Dol Blathanna",
               "z Lan Exeter",          "z Pont Vanis",
               "z Blaviken",            "z Creyden",
               "z Ho這pola",            "z Tridam",
               "z Aedd Gynvael",        "z Rakvelerin",
               "z Gors Velen",          "z Hirundum",
               "z Yspaden",             "z Mirt",
               "z Ard Carraigh",        "z Daevon",
               "z Hengfors",            "z Crinfrid",
               "z Murivel",             "z Tretogoru",
               "z Oxenfurtu",           "z Novigradu",
               "z Grabowej Buchty",     "z Cidaris",
               "z Vole",                "z Wyzimy",
               "z Dorian",              "z Anchor",
               "z Piany",               "z Bia貫go Mostu",
               "z Burdorff",            "z Ellander",
               "z Zavady",              "z Mariboru",
               "z Carreras",            "z Hagge",
               "z Ban Glean",           "z Gulety",
               "z Aldersbergu",         "z Vengerbergu",
               "z Ban Ard",             //"z Mahakamu",
               "z Rivii",               "z Lyrii",
               "z Brugge",              "z Dillingen",
               "z Nastrog",             "z Bremervoord",
               "z Ard Skellige",        "z Cintry",
               "z Tigg",                "z Kagen",
               "z Klucza",              "z Riedbrune",
               "z Beauclair",           "z Belhaven",
               "z Neunreuth",           "z Metinny",
               "z Maecht",              "z Amarillo",
               "z Claremont",           "z Thurn",
               "z Nilfgaardu",          //"z G鏎 Sinych",
               "z Gelibolu"//,			"z Tir Tochair"
              });
/*oto lista umiejetnosci dostepnych dla wszystkich do wyboru.
 *jest umyslnie tak okrojona, by byly to tylko te umiejetnosci, ktore
 *potencjalnie moga dac od razu graczowi zawod. Tak wiec nie ma tu
 *zadnej wymyslnej listy ;)
 */
mapping lista_skilli = ([
               "miecze":                  SS_WEP_SWORD,
               "bronie drzewcowe":        SS_WEP_POLEARM,
               "topory":                  SS_WEP_AXE,
               "sztylety":                SS_WEP_KNIFE,
               "maczugi":                 SS_WEP_CLUB,
               "m這ty":                   SS_WEP_WARHAMMER,
               "bronie strzeleckie":      SS_WEP_MISSILE,
               "walka bez broni":         SS_UNARM_COMBAT,
               "parowanie":               SS_PARRY,
               "zielarstwo":              SS_HERBALISM,
              //jesli zielarstwo, to i alchemia chyba nie? -_-
               "ocena przeciwnika":       SS_APPR_MON,
               "ocena obiektu":           SS_APPR_OBJ,
               "szacowanie":              SS_APPR_VAL,
//               "kieszonkostwo":           51,
               "znajomo嗆 j瞛yk闚":       SS_LANGUAGE,
               "opieka nad zwierz皻ami":  SS_ANI_HANDL,
                "這wiectwo":               SS_HUNTING,
               "jazda konna":             SS_RIDING,
               "tropienie":               SS_TRACKING,
               "targowanie si�":          SS_TRADING
//               "muzykalno嗆":             SS_MUSIC
               ]);

//kolory zarostu sa do wyboru tylko wtedy, gdy ktos zechce
//byc lysy permanentnie. W przeciwnym wypadku, kolor zarostu
//jest podobny do koloru wlosow.
string *kolory_zarostu = ({
              "bia貫",
              "czarne",
              "ciemne",
              "czerwone",
              "jasne",
              "kruczoczarne",
              "rude",
              "siwe",
              "popielate"
              });


mapping dlugosc_wasow = ([
               "brak":0.1,              "kr鏒kie":0.3,
              "鈔edniej d逝go軼i":0.5,  "d逝gie":1.0
               ]);
mapping dlugosc_brody = ([
               "brak":0.1,       "kr鏒k�":3.1,
               "si璕aj帷� szyi":8.1, "si璕aj帷� piersi":19.1,
               "si璕aj帷� pasa":40.1
               ]);

//ograniczenia w wyborze wieku.
mapping DOLNA_GRANICA_WIEKU = ([
			"cz^lowiek" :	14,
			"elf"	    :	20,
			"p^o^lelf"  :	14,
			"krasnolud" :	19,
			"nizio^lek" :   14,
                        "gnom"      :   18
                        ]);
mapping GORNA_GRANICA_WIEKU = ([
			"cz^lowiek" :	70,
			"elf"	    :	300,
			"p^o^lelf"  :	70,
			"krasnolud" :	90,
			"nizio^lek" :   80,
                        "gnom"      :   85
                        ]);

/* A tu jest taka sprytna mala funkcja, ktora w wiosce zastepuje funkcje 'koncowka'.
 * Dlatego, ze z koncowka dla bytow astralnych byly same klopoty :/
 */
string koncoweczka(string m, string k)
{
    if(this_player()->query_gender()==1)
      return k;
    else
      return m;
}

/*
void
//filtrowane_atrybuty(string str)
filtrowane_atrybuty()
{
    string *arr;
    object kto=this_player(); //???

    arr = m_indices(this_player()->query_gender()==1?cialo_kobiety:cialo_mezczyzny);
    arr = filter(arr, &call_other("/d/Standard/login/wioska/atrybuty", ,kto) @ &operator(+)("p",) @ &implode(,"_") @ &explode(," ") @ plain_string);

    dostepne_cechy_ciala=arr;
   // write(COMPOSITE_WORDS(arr));
    

  //str=filter(m_indices(this_player()->query_gender()==1?cialo_kobiety:cialo_mezczyzny),
    //         &call_other("/d/Standard/login/wioska/atrybuty.c", ,kto) @ &operator(+)("p",) @ &implode(,"_") @ &explode(," ") @ plain_string);
}
*/





/* Funkcje obliczajace czy dany przymiotnik jest mozliwy
 * do wyboru (dzieki zadeklarowanej wczesniej wadze i wzroscie)
 *
 *POZIOMY TO:     WAGA            WZROST
 *    0         bardzo chud        niespotykanie nisk
 *    1         chud               bardzo nisk
 *    2         szczupl            nisk
 *    3         przecietnej w.     przecietnego wzrostu
 *    4         lekko otyl         wysok
 *    5         otyl               bardzo wysok
 *    6         bardzo otyl        niespotykanie wysok
 */

int
pszerokie_barki(object kto, int waga, int wzrost)
{
   if(waga>=3)
    return 1;
   else
    return 0;

}
int
pbarylkowatosc(object kto, int waga, int wzrost)
{
   if(waga>=4)
    return 1;
   else
    return 0;

}
pbeczkowatosc(object kto, int waga, int wzrost)
{
   if(waga>=4)
    return 1;
   else
    return 0;

}
int
ppokazny_brzuch(object kto, int waga, int wzrost)
{
   if(waga>=2)
    return 1;
   else
    return 0;

}
int
pszczupla_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga==1 || waga==2)
    return 1;
   else
    return 0;

}
int
pchuda_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga<=2)
    return 1;
   else
    return 0;

}
int
pciezka_postura(object kto, int waga, int wzrost)
{
   if(waga>=4)
    return 1;
   else
    return 0;

}
int
pkoscistosc(object kto, int waga, int wzrost)
{
   if(waga<=2)
    return 1;
   else
    return 0;

}
int
plekka_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga<=2)
    return 1;
   else
    return 0;

}
int
pgruba_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga>=4)
    return 1;
   else
    return 0;

}
int
potylosc(object kto, int waga, int wzrost)
{
   if(waga>=5)
    return 1;
   else
    return 0;

}
int
pniepozorna_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga<=3)
    return 1;
   else
    return 0;

}
int
ppulchna_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga>=4)
    return 1;
   else
    return 0;

}
int
psmukla_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga<=3)
    return 1;
   else
    return 0;

}
int
pwatla_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga<=2)
    return 1;
   else
    return 0;

}
int
pwielka_postura(object kto, int waga, int wzrost)
{
   if(waga>=3 && wzrost>=4)
    return 1;
   else
    return 0;

}
int
pzwalista_postura(object kto, int waga, int wzrost)
{
   if(waga>=4 && wzrost>=3)
    return 1;
   else
    return 0;

}
int
ptluste_cialo(object kto, int waga, int wzrost)
{
   if(waga>=4)
    return 1;
   else
    return 0;

}
int
pkrepa_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga>=3)
    return 1;
   else
    return 0;

}
int
pprzecietna_waga(object kto, int waga, int wzrost)
{
   if(waga==3)
    return 1;
   else
    return 0;

}
int
pmasywna_postura(object kto, int waga, int wzrost)
{
   if(waga>=4 && wzrost>=2)
    return 1;
   else
    return 0;
}
int
ppokazne_muskuly(object kto, int waga, int wzrost)
{
   if(waga>=2)
    return 1;
   else
    return 0;
}
int
ppokazne_miesnie(object kto, int waga, int wzrost)
{
   if(waga>=3)
    return 1;
   else
    return 0;
}
int
ppuszystosc(object kto,int waga, int wzrost)
{
   if(waga>=3)
    return 1;
   else
    return 0;
}
int
pdrobna_budowa_ciala(object kto,int waga, int wzrost)
{
   if(waga<=2)
    return 1;
   else
    return 0;
}
int
pfiligranowa_budowa_ciala(object kto,int waga, int wzrost)
{
   if(waga<=2)
    return 1;
   else
    return 0;
}
int
pzylasta_budowa_ciala(object kto,int waga, int wzrost)
{
   if(waga<=2)
    return 1;
   else
    return 0;
}
int
pproporcjonalna_budowa_ciala(object kto,int waga, int wzrost)
{
   if(waga==3)
    return 1;
   else
    return 0;
}
