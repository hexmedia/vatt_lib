/*
 * Gliniany ma�y garnek do kuchni w wiosce z etapu tworzenia postaci.
 *             Lil. Thursday 25 of March 2006
 */

inherit "/std/holdable_object";
#include <object_types.h>
#include <materialy.h>
#include <macros.h>
#include <pl.h>
#include <stdproperties.h>
#define OWNER "/d/Standard/login/wioska/npc/niziolek"

create_holdable_object()
{
    ustaw_nazwe("garnek");
    dodaj_przym("ma�y","mali");
    dodaj_przym("gliniany","gliniani");
    set_long("Ma�y garnek o kr�tkich i grubych uszach wykonany jest w ca�o�ci "+
             "z gliny.\n");
    set_hit(2);
    set_pen(3);
    set_dt(W_BLUDGEON);
    set_wt(W_CLUB);
    set_hands(W_ANYH);
    set_likely_dull(MAXINT);
    set_likely_break(MAXINT);
    set_weapon_hits(9991999);
    ustaw_material(MATERIALY_GLINA, 100);
    set_type(O_KUCHENNE);
    add_prop(OBJ_I_WEIGHT, 952);
    add_prop(OBJ_I_VOLUME, 1040);
    add_prop(OBJ_I_VALUE, 2);
    set_owners(({ OWNER }));
}
