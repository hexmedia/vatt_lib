inherit "/std/object.c";

#include <pl.h>
#include <macros.h>
#include <object_types.h>
#include <stdproperties.h>

void
create_object()
{
  ustaw_nazwe("zawini�tko");
  dodaj_przym("tajemniczy","tajemniczy");

  set_long("Owini�te p��tnem tajemnicze, do�� spore zawini�tko jest "+
      "pod sta�� opiek� postaci siedz�cej przed nim.\n");
  add_prop(OBJ_I_VOLUME, 5555);
  add_prop(OBJ_I_WEIGHT, 10000);
  add_prop(OBJ_M_NO_GET, "Pr�bujesz wzi�� tajemnicze zawini�tko.\n"+
      "Tajemniczy spokojny p�elf m�wi stanowczo: "+
      "Jeszcze nie pora.\n");
  add_prop(OBJ_I_DONT_GLANCE,1);
}
