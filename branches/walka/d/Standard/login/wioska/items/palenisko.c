/*
 * Autor: Rantaur
 * Data 23.04.2006
 *  Palenisko do kuzni w wiosce tworzenia postaci
 *  */
inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>
#define OWNER "/d/Standard/login/wioska/npc/krasnolud"

int rozpalone = 0;
float delay = 0.0;

int podsyc(string co);
string zwroc_stan();

void create_object()
{
    ustaw_nazwe("palenisko");
    dodaj_przym("niski", "niscy");
    dodaj_przym("ceglany", "ceglani");
    set_long("Okr�g�e palenisko jest dosy� niskie, jednak nie powinno to"
	    +" przeszkadza� w swobodnym operowaniu przy nim. Jego konstrukcja"
	    +" jest ca�kiem prosta - na zbudowanej z ceg�y �ciance umieszczono"
	    +" solidn� krat� tak by rozgrzewany przedmiot nie wpad� pomi�dzy"
            +" pal�ce si� w�gle. Rol� komina pe�ni osadzony na czterech"
	    +" kolumienkach, stokowaty dach, kt�ry stopniowo przechodzi w"
	    +" odprowadzaj�ca dymy rur�. Jak ka�dy porz�dny piec r�wnie� i ten"
	    +" wyposa�ony jest w poka�ny, sk�rzany miech s�u��cy do podsycania" 
	    +"ognia.\n"+"@@zwroc_stan@@");
    add_item("miech", "Miech wykonany jest z grubej sk�ry, a jego boki"
		     +" dodatkowo wzmacniaj� drewniane listewki. Z prawej strony"
		     +" zamocowane s� trzy wygi�te pr�ty pod kt�rymi mo�na umie�ci�"
		     +" stop� podczas roztwierania miecha.\n");

    add_prop(OBJ_I_WEIGHT, 530000);
    add_prop(OBJ_I_VOLUME, 30000);
    add_prop(OBJ_I_VALUE, 2520);
    add_prop(OBJ_M_NO_GET, "Palenisko jest mocno przytwierdzone do pod�o�a -"
		          +" chyba nie mo�esz go tak poprostu zabra�.\n");
    set_owners(({OWNER}));
    set_type(O_INNE);
    ustaw_material(MATERIALY_GLINA);
}

init()
{
    ::init();
    add_action(podsyc, "podsy�");
}

void przygas()
{
    rozpalone -= 1;
    delay -= 45.0;
    tell_room(environment(this_object()), "Ogie� w palenisku nieznacznie przygasa.\n");
}
	
int podsyc(string co)
{
    if(co ~= "ogie� w palenisku") 
    {
        if(rozpalone < 2)
        {
	    rozpalone += 1;
	    delay += 45.0;
	    write("Podchodzisz do paleniska i za pomoc� miecha wt�aczasz"
		  +" do niego nieco powietrza.\n\n"
		  +"W�gle rozjarzaj� si� jaskrawo-pomara�czowym �wiat�em wyzwalaj�c"
		  +" z siebie dziesi�tki nowych, coraz to bardziej rozta�czonych" 
		  +" p�omieni.\n");
	    say(QCIMIE(TP, PL_MIA)+" podchodzi do paleniska i za pomoc� miecha"
		  +" wt�acza do niego nieco powietrza.\n\n"
		  +"W�gle rozjarzaj� si� jaskrawo-pomara�czowym �wiat�em wyzwalaj�c"
		  +" z siebie dziesi�tki nowych, coraz to bardziej rozta�czonych" 
		  +" p�omieni.\n");
	    set_alarm(delay, 0.0, &przygas());
	}
	else
        {
	    write("Przy pomocy miecha wt�aczasz powietrze do paleniska.\n\n" 
		  +"P�omienie hucz� i miotaj� si� dziko, by po chwili powr�ci� do"
		  +" spokojniejszego, lecz r�wnie nieprzewidywalnego jak poprzednio"
		  +" ta�ca.\n");
	    saybb(QCIMIE(TP, PL_MIA)+" przy pomocy miecha wt�acza powietrze do"
		      +" paleniska.\n\n"
		      +"P�omienie hucz� i miotaj� si� dziko, by po chwili powr�ci� do"
		      +" spokojniejszego, lecz r�wnie nieprzewidywalnego jak poprzednio"
		      +" ta�ca.\n");       
        }
    }
    else write("Co i gdzie chcesz podsyci�?\n");    
    return 1;
}

string zwroc_stan() //Zwraca czy w piecu pali sie, czy tylko zarzy etc.
{
    string stan;
    switch(rozpalone)
    {
        case 0:
	    stan = "W�gle w palenisku migocz� czerwono-pomara�czowym �wiat�em.\n";
	    return stan;
	    break;
	case 1:
	    stan = "W palenisku bez�adnie ta�cz� jaskrawe p�omienie. Niekt�re z"
		  +" nich co jaki� czas znikaj�, by za chwil� zab�ysn�� w innym"
		  +" miejscu.\n";
	    return stan;
	    break;
	case 2:
	    if(this_player()->query_race()!="byt astralny")
              {stan = "Ogie� w palenisku p�onie tak mocno, �e nawet w wi�kszej" 
	            +" odleg�o�ci jeste� w stanie poczu� bij�ce od niego ciep�o.\n";}
            else
              {stan = "Ogie� w palenisku p�onie tak mocno, �e trzeba uwa�a�, by "+
                    "si� nie sparzy�.\n";}

	    return stan;
	    break;
    }
}
