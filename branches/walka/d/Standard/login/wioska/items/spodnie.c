
/* Zwyk^le szare spodnie 
   Wykonany przez Avarda, dnia 10.05.06
   Opis by Alcyone */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
	ustaw_nazwe("spodnie");
	dodaj_przym("zwyk^ly","zwykli");
	dodaj_przym("br^azowy","br^azowi");
	set_long("Materia^l, z kt^orego zosta^ly uszyte te spodnie, jest do�^c "+
             "szorstki w dotyku. Farbowana na ziemisty kolor we^lna, nie "+
             "jest pierwszej jako�ci, a w wielu miejscach wida^c spore "+
             "przetarcia. Spodnie s^a ma^le i z pewno�ci^a nie nadaj^a si^e dla "+
             "wysokiej osoby. Ich kr^oj dostosowany jest raczej dla nizio^lk^ow"+
             ", a w^askie nogawki bed^a pasowa^c na nich jak ula^l. Ubi^or ten, "+
             "nie posiada �adnych zdobie�. Jedynie materia^l przeszyty jest "+
             "czarn^a nici^a, kt^ora nie rzuca si^e nawet w oczy.\n");

    add_prop(ARMOUR_S_DLA_RASY, 5);
    add_prop(ARMOUR_I_DLA_PLCI, 0);
	set_slots(A_LEGS);
	add_prop(OBJ_I_VOLUME, 400);
	add_prop(OBJ_I_WEIGHT, 500);
	ustaw_material(MATERIALY_WELNA, 100);
	set_type(O_UBRANIA);
	add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 60);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_S_DLA_RASY, "nizio�ek");
    add_prop(OBJ_I_VALUE, 11);
}
