/*
 *  Sklep gnoma wioski z etapu tworzenia postaci.
 *        By Lil & Aneta (long) 
 *                          Wed Mar 22 2006
 */

inherit "/d/Standard/login/wioska/std";

#include <std.h>
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <language.h>
#include <composite.h>
#include <filter_funs.h>
#include <materialy.h>
#include "login.h"
#define UBRANIA "/d/Standard/items/ubrania/"

object gift1, gift2, gift3, gift4; //prezenty :P
object npc;
int faza_reakcji=0; //jesli jest 1, to npc nie reaguje na wybory np. mowiac "Wspaniale."
                    //dotyczy to przypadkow kiedy gracz np. stwierdzi, ze nie chce byc
                    //danej rasy. Wtedy to by brzmialo bez sensu.
object byt=this_player();

public void
create_wioska_room()
{
    set_short("W sklepie gnoma");

    add_exit("lok1",  ({"wyj�cie","do wyj�cia","ze sklepu"}));
    add_sit("na ziemi","na ziemi","z ziemi",0);
    add_prop(ROOM_I_INSIDE,1);
    npc=clone_object(PATH+"wioska/npc/gnom.c");
    npc->init_arg(0);
    npc->move(this_object());

}

exits_description() 
{
	return "Wyj�cie st�d prowadzi na plac.\n";
}

string
dlugi_opis()
{
    string str;

    str = "Ciasny sklepik mieni si� tysi�cem r�nych barw, "+
          "niczym fontanna t�czowych kolor�w zalewa ca�e "+
          "pomieszczenie i przykuwa wzrok. Drewniana lada "+
          "przecinaj�ca pomieszczenie przystrojona zosta�a "+
          "finezyjn� serwetk�, a gnom wychylaj�cy si� zza "+
          "niej kr�ci si� i poprawia towary na p�kach. "+
          "Pono� gnomy ceni� sobie perfekcj� i porz�dek, tak wi�c "+
          "wszystkie wyroby s� najlepszej jako�ci, u�o�one "+
          "r�wno obok siebie, bez zb�dnego nie�adu. "+
          "W powietrzu unosz� si� smugi �nie�nobia�ego "+
          "piasku, przypominaj�c p�atki �niegu wiruj� "+
          "ponad g�owami, wzbijaj�c si� pod sam sufit i "+
          "ta�cz�c w pomieszczeniu migotliwym blaskiem.";

    return str;
}

void jaka_rasa()
{
    if(plec==1)
      {
        rasa="gnomka";
      }
    else
      {
        rasa="gnom";
      }
  race="gnom";
}

/* Generalnie staramy si� tworzy� jak najmniej akcji npca
 * w jednej fazie. �eby na zafloodowa�o nagle ekranu...
 */

void faza0start()
{
    faza_reakcji=1;
    npc->command("emote m�wi szybko nerwowo gestykuluj�c d�o�mi: "+
                "Witam, witam!");
    npc->command("popatrz z usmiechem na byt astralny");
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
               " Zdaje si�, �e chcia�"+koncoweczka("by�","aby�")+
               " dosta� cia�o?");
}


void fazaniebyt()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Widz�, �e ju� podj"+koncoweczka("��e�","�a�")+
	     " decyzj�, nic tu po tobie!");
}
void faza0tak()
{
    faza_reakcji=1;
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Nadawa�"+koncoweczka("by�","aby�")+" si� na gnom"+
             koncoweczka("a","k�")+"! Chcesz zosta� gnomem?");
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Wierz�, �e si� zgodzisz, podejmuj�c w�a�ciw� "+
             "decyzj�.");
}
void faza0nie()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Co? Na pewno? Dobrze, zatem poczekam na ciebie. "+
             "Wr�� jak ju� si� zdecydujesz!");
}
void faza0inne()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Nie rozumiem! Czy to oznacza, �e tak?");
    npc->command("usmiechnij sie");
}
void faza1tak()

{
    faza_reakcji=1;
    npc->command("podskocz");
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Wiedzia�em, �e mnie nie zawiedziesz! "+
             "Nim przejdziemy do sedna sprawy, chcia�bym ci "+
             "co� opowiedzie�. Czy mog�?");
    npc->command("usmiechnij sie niepewnie");
}
void faza1nie()
{
    faza_reakcji=1;
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Niemo�liwe! Niemo�liwe! Ty musisz by� "+
             "gnom"+koncoweczka("em","k�")+", ja to wiem! "+
             "Poczekam tu na ciebie, a ty do mnie wr�� "+
             "jak ju� dotrze do ciebie to, co przed chwil� "+
             "powiedzia�"+koncoweczka("e�","a�")+"!"); 
    npc->command("usiadz na ziemi");
    npc->command("popatrz smutno");
}
void faza1inne()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Tak? Powiedz tak!");
}

/* Dobrze by by�o, gdyby ka�dy enpec opowiada� histori� z w�asnego
 * punktu widzenia, nie?
 * Historia wi�c ma by� subiektywna. Im bardziej, tym lepiej ;)
 *
 *
 * HISTORIA ROZBITA NA FAZY. PIERWSZA FAZA JEST NA SAMYM DOLE: faza2tak()
 * A OSTATNIA NA SAMEJ GORZE (faza2tak4()), wiec od konca czytamy ;)
 */
void faza2tak4()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
         " Wi�c jak? Chcesz by� jedn"+koncoweczka("ym","�")+
         " z nas?");
    npc->command("zatrzepocz");
}
void faza2tak3()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
         " Ale nie jest tak �le! Jako� si� trzymamy i nie dajemy sobie "+
         "w kasze dmucha�!");
    set_alarm(6.5,0.0,&faza2tak4());
}
void faza2tak2()
{
    npc->command("otrzasnij sie");
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
         " Wracaj�c do sedna. Niestety, ale ci�kie czasy sobie wybra�" +koncoweczka("e�","a�")+
         "... Ju� od dawna przyjaci� mamy najcz�ciej tylko w�r�d "+
         "krasnolud�w i to z nimi dzielimy Mahakam - krain�, w kt�rej "+
         "�yje znaczna wi�kszo�� z nas. Lecz s� "+
         "i gnomy �yj�ce i pracuj�ce wraz z lud�mi w ich "+
         "miastach i dla nich pracuj�. C� ... Takie �ycie.");
    npc->command("westchnij cicho");
    set_alarm(10.5,0.0,&faza2tak3());
}
void faza2tak1()
{
    npc->command("odetchnij");
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
         " Dlatego te� jeste�my najlepsi na �wiecie w fachu, "+
         "kt�ry obrali sobie nasi praprapraprapraprapraprapradziadowie! "+
         "A jest to obr�bka metali i kamieni. Nikt si� nie zna na tym tak jak gnom! "+
         "S�ysza�"+koncoweczka("e�","a�")+
         " kiedy� o gwyhyrach? Kr��� o nich legendy!");
    npc->command("popatrz z usmiechem");
    set_alarm(10.0,0.0,&faza2tak2());
}
void faza2tak() //Opowiada historie + ostateczne pytanie o rase
{
    faza_reakcji=1;
    npc->command("emote ociera pot z czo�a.");
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
         " Ot�! Masz zaszczyt w�a�nie ogl�da� przedstawiciela najstarszej "+
         "rasy na tym kontynencie! Tak, tak! To w�a�nie my, "+
         "gnomy byli�my tu od praczas�w, a po nas dopiero "+
         "przedstawiciele innych ras tu przyp�yn�li. Nale�y "+
         "o tym pami�ta�!");
    set_alarm(9.5,0.0,&faza2tak1());
}
//koniec fazy opowiadania historii

void faza2nie()
{
    faza_reakcji=1;
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Dobrze zatem. Mo�e nast�pna osoba si� skusi... "+
	     "A zatem, czy jeste� pew"+koncoweczka("ien","na")+
             " swego wyboru?");
}
void faza2inne()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Aj aj aj! Nie rozumiem! Tak czy nie?");
}
void faza3tak() //Ostateczne pytanie o rase. Nastepnie pytanie o wzrost
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
                 " Ha! Nie zawiod�"+koncoweczka("e�","a�")+
                 " mnie! Przejd�my teraz do konkret�w.");
    npc->command("mrugnij porozumiewawczo");
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Jakiego chcesz by� wzrostu?");
}
void faza3nie()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Niemo�liwe! Niemo�liwe! Ty musisz by� "+
             "gnom"+koncoweczka("em","k�")+", ja to wiem! "+
             "Poczekam tu na ciebie, a ty do mnie wr�� "+
             "jak ju� dotrze do ciebie to, co przed chwil� "+
             "powiedzia�"+koncoweczka("e�","a�")+"!"); 
    npc->command("usiadz na ziemi");
    npc->command("popatrz smutno");
}
void faza3inne()

{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Aj aj aj! Nie rozumiem.");
}
void faza4zle() //odpowiedz na wzrost, pytanie o wage
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Wystarczy, �e spytasz mnie o wzrost, a dowiesz "+
             "si�, jaki mo�esz wybra�.");
}
void faza4dobrze()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Mhm, a teraz powiedz mi jakiej chcesz by� wagi.");
    npc->command("wstan");
}
void faza5zle() //waga
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " M"+koncoweczka("�g�by�","og�aby�")+
		"powt�rzy�, bo chyba nie zrozumia�em?");
}
void faza5dobrze()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Dobrze, a ile chcesz mie� lat?");
}
void faza6nierozumiem() //wiek
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Aj aj! Nie rozumiem, ile?");
}
void faza6zamlody()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " A� tak"+koncoweczka("i","a")+
             " m�od"+koncoweczka("y","a")+
             " to nie mo�esz by�! Musisz wybra� conajmniej "+
		"osiemna�cie.");
}
void faza6zastary()

{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " To zdecydowanie za du�o, w takim wieku nie "+
		"m"+koncoweczka("�g�by�","og�aby�")+
		"pozwoli� sobie na wiele rzeczy. Wybierz mniej ni� "+
		"osiemdziesi�t pi��, a wi�cej ni� osiemna�cie.");
}
void faza6dobrze()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " B�dziesz mia�"+koncoweczka("","a")+
             " zatem "+LANG_SNUM(wiek,0,1)+" "+slowo_lat+
             "! Wspania�y wyb�r! A oczy? Jakiego chcesz mie� koloru?");
}
void faza7zle() //kolor oczu
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Aj aj! Nie znam takiego koloru. "+
             "Znam tylko: "+
    implode(m_indices(oczy_gnomow), ", ")+".");
}

/* Po prostu idziemy dalej, czyli
 * pytamy o to, czy �yczy sobie posiada� w�osy W OG�LE,
 * czyli, czy gracz chce by� �ysy NA STA�E.
 */
void faza7dobrze8()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Mhmm, "+
             "a co z w�osami? Chcesz je w og�le posiada� "+
		"przysz�"+koncoweczka("y","a")+
                " gnom"+koncoweczka("ie","ko")+
		"?");
}
/* A tutaj omijamy pytanie o to, czy gracz chce w og�le
 * posiada� ow�osienie, poniewa� jest elfem lub p�elfem, a
 * oni nie mog� by� 'na sta�e' �ysi.
 * Oczywi�cie nikt im nie zabroni si� zgoli�, ale w�osy
 * b�d� ros�y im i tak... :)
 * Wi�c pytamy od razu o kolor w�os�w.
 */
void faza7dobrze9()
{
    npc->command("spanikuj"); //w przypadku elfa i polelfa tu 
                              //ma byc pytanie od razu o kolor w�os�w.
    npc->command("powiedz Aj aj aj! Co� posz�o nie po mojej my�li! "+
                 "Zg�o� b��d! b��d! b��d!");
    npc->command("krzyknij");
}

/* Je�li gracz chce posiada� w�osy w og�le, to jedziemy z pytaniem
 * o kolor w�os�w.
 */
void faza8tak()
{
   npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Zatem jakiego koloru chcesz mie� w�osy?");
}

/* Kobietom nie zadajemy pyta� o zarost, wi�c omijamy kilka faz
 * i pytamy odrazu o budow� cia�a t� �ys� kobietk� <szok> ....
 */
void faza8nie1()
{

   npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
                " Mmm, ciekawe...�ysa gnomka, hihihihihi. "+
                "Twoj wyb�r!");
   faza11dobrze1();
}

/* A m�czy�ni i tak musz� poda� kolor...zarostu. �eby by�o wiadomo
 * jaki kolor brody i w�s�w im da� (gdyby posiadali w�osy, to wyliczaloby
 * si� to z koloru w�os�w, ale �e wybrali, �e s� permanentnie �ysi...)
 */
void faza8nie2()
{
   npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Zatem b�dziesz �ysy, musisz jednak wybra� kolor "+
	   "swego zarostu, jaki to b�dzie kolor?");
}
/* Tak czy nie? Inna odpowied�...
 */
void faza8inne()
{
   npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Odpowiedz pozytywnie, je�li chcesz posiada� w�osy.");
}
/* kolor wlosow, pytanie o dlugosc
 */
void faza9zle() 
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Nie nie nie! Taki kolor nie mo�e by�! Wybierz kt�ry� z tych: "+
           //w przypadku innych ras jest s� to inne indeksy mapping�w
    implode(m_indices(wlosy_gnomow), ", ")+".");
}
void faza9dobrze()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Hihi, wspaniale! "+
             "A teraz wybierz d�ugo�� w�os�w!");
}
/* Ta faza jest tylko wtedy, je�li si� wybra�o permanentnie �ysego.
 * Bo i tak trzeba poda� kolor zarostu. Wi�c ta faza jest tylko dla m�czyzn
 */
void faza10zle()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Nie znam takiego koloru zarostu.");
}
void faza10dobrze()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " A jakiej d�ugo�ci w�sy �yczysz sobie posiada�?");
}
/* W tej fazie gracz wybiera d�ugo�� w�os�w i dostaje pytanie
 * O w�sy, je�li jest m�czyzn�. Je�li jest elfem lub kobiet�,
 * to omijamy te pytania i skaczemy odrazu do pyt. o budowe cia�a
 */
void faza11zle()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Nie znam takiej d�ugo�ci w�os�w.");
}


/* jesli jest: albo elfem, albo kobiet�, albo poni�ej 19 roku �ycia:
 */
void faza11dobrze1()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Teraz zajmiemy si� twoim przysz�ym cia�em! "+
             "Jak� cech� budowy "+
		"cia�a chcesz posiadac?");
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Mo�esz wybra� spo�r�d: "+   //m["indeks"][2]
             COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
                             //lista filtrowana przez wybrana wage i wzros
}
/* w przeciwnym wypadku:
 */
void faza11dobrze2()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " A co z w�sami? Jak d�ugie maj� by�? Je�li "+
		"chcesz mo�esz ich nie posiada� teraz, ale z "+
                "czasem i tak odrosn�.");
}
/* d�ugo�� w�s�w:
 */
void faza12zle()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Nie znam takiej d�ugo�ci! Je�li masz "+
		"z czym� jaki� problem wystarczy mnie spyta� o to!");
}
void faza12dobrze()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " A co z brod�? Jakiej d�ugo�ci ma by�?");
}
/* d�ugo�� brody
 * i pytanie o ceche budowy cia�a 
 */
void faza13zle()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Aj, aj! Nie znam takiej d�ugo�ci! "+
             "Je�li chcesz zapozna� si� z d�ugo�ciami brody, zapytaj "+
             "mnie o to.");
}
void faza13dobrze()

{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " A teraz najciekawsze!");
    npc->command("zatrzyj rece");
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Wybierz cech� charakterystyczn� "+
             "dla budowy twojego przysz�ego cia�a.");
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Masz do dyspozycji: "+   //m["indeks"][2]
             COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
                             //lista filtrowana przez wybrana wage i wzrost
}


void faza14zle() //budowa ciala. Pytanie o ceche szczegolna
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Aj aj! Nie znam takiej cechy. Masz do dyspozycji: "+   
               COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
}
void faza14dobrze()

{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
         " Teraz jeszcze ciekawsze!");
    npc->command("usmiechnij sie rozbrajajaco");
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
         " Chcesz mie� jak�� ceche szczeg�ln�? "+
         "Mo�esz wybra� w�r�d nast�puj�cych cech: "+
         COMPOSITE_WORDS2(m_indexes(plec==1?cechy_szczegolne_kobiet:
         cechy_szczegolne_mezczyzn)," lub ")+". "+
             "Oczywi�cie wcale nie musisz posiada� "+
             "�adnej charakterystycznej cechy....W�a�nie, wi�c "+
             "jak b�dzie? Chcesz posiada� jak�� cech� charakterystyczn�?"+
             " Odpowiedz tak lub nie.");
}
void faza15tak() //odpowiedz czy chce sie miec ceche szczegolna <opcjonalnie>
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Ah! Wspaniale! Oby by�o wi�cej tak odwa�nych istot, "+
             "jak ty! Wi�c jaka to b�dzie cecha?");
}
void faza15nie()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Szkoda, mia�em nadzieje, �e b�dziesz wyj�tkow"+
             koncoweczka("y","a")+"...Ale trudno!");
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Teraz musimy wybra� twoje pochodzenie! Czy chcesz "+
             "posiada� jakie�? "+
             "Pami�taj"+
             " p�niej tak�e b�dziesz "+
             "m"+koncoweczka("�g�","og�a")+" si� na nie "+
             "zdecydowa�, lecz do wyboru b�dziesz mia�"+
             koncoweczka("","a")+" jedynie miejsce, "+
             "w kt�rym si� b�dziesz aktualnie znajdowa�"+
             koncoweczka("","a")+
             ". Pami�taj, �e raz wybrane miejsce ju� nigdy si� "+
             "nie zmieni.");
}
void faza15inne()
{
   npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Hmmm...Tak czy nie?");
}
void faza16zle() //wybieranie cechy szczegolnej (jesli sie zgodzilo wczesniej)
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Nie znam takiej cechy szczeg�lnej! Musisz wybra� w�r�d "+
             "tych, kt�re ci wcze�niej wymienia�em!");
}
void faza16dobrze()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Ha! Wida� w tobie prawdziwie gnomi� krew!");
      npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Teraz musimy wybra� twoje pochodzenie! Czy chcesz "+
             "posiada� jakie�? "+
             "Jednak pami�taj! P�niej tak�e b�dziesz "+
             "m"+koncoweczka("�g�","og�a")+" si� na nie "+
             "zdecydowa�, lecz do wyboru b�dziesz mia�"+
             koncoweczka("","a")+" jedynie miejsce, "+
             "w kt�rym si� b�dziesz aktualnie znajdowa�"+
             koncoweczka("","a")+".");
}
void faza17tak() //odpowiedz czy chce sie posiadac pochodzenie.
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " A zatem sk�d chcesz pochodzi�?");
}
void faza17nie()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Jeszcze o co� chcia�bym zapyta�... "+
             "Jak� cech� fizyczn� lub mentaln� chcesz, by "+
             "wyr�nia�a si� w�r�d innych? Do "+
             "wyboru masz si��, zr�czno��, wytrzyma�o��, inteligencj�, "+
             "m�dro�� oraz odwag�.");
} 
void faza17inne()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Aj! aj! Nie rozumiem. Tak czy nie?");
} 
void faza18zle() //wybor pochodzenia jesli sie zgodzilo wczesniej.
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Oh, przykro mi. Nie s�ysza�em o takim miejscu, musisz wybra� inne.");
}
void faza18dobrze()
{
    string pocho = powiedz_pochodzenie();

    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Hmm "+pocho[2..]+
             ", ciekawe! No ale wr��my do pyta�... "+
             "Jak� cech� fizyczn� lub mentaln� chcesz, by "+
             "wyr�nia�a si� w�r�d innych? Do wyboru "+
             "masz si��, zr�czno��, wytrzyma�o��, inteligencj�, m�dro�� "+
             "oraz odwag�.");
}
void faza19zle() //cechy
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Nie ma takiej cechy! Musisz wybra� kt�r�� z tych, "+
             "kt�re wymieni�em.");
}
void faza19dobrze() //cechy
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Przejd�my do twoich umiej�tno�ci, musisz wybra� sobie "+
             "dwie. Wyb�r jest do�� skromny, ale jestem pewien, �e "+
             "znajdziesz co� dla siebie! Je�li chcesz pozna� list�, zapytaj "+
             "mnie o to. Jaka zatem b�dzie pierwsza twoja wybrana umiej�tno��?");
}
void faza20zle() //umiejetnosci - wybor pierwszej
{

    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Przykro mi! Nie znam takiej umiej�tno�ci.");
}
void faza20dobrze()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Rozumiem! A jaka b�dzie druga umiej�tno��?");
}

void faza21zle() //umiejetnosci - wybor drugiej
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Nie! Takiej nie znam! Wybierz inn�.");
}
/* nie mo�na wybra� dw�ch umiej�tno�ci takich samych pod rz�d.
 */
void faza21zle2()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Ha! Nie mo�esz wybra� dw�ch takich samych umiej�tno�ci.");

}
void faza21dobrze()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Hihi, chyba ju� wszystko za�atwili�my!"+
             " Czy jeste� got"+koncoweczka("�w","owa")+"?");
}
void faza22tak() //podsumowanie + prezenty?:) No wlasnie...Jakie?
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Ach! By�bym zapomnia�! Nie mo�esz wej�� do �wiata "+
             "w tym stanie! Jako przysz�"+koncoweczka("y","a")+
             " gnom"+koncoweczka("","ka")+ " mam dla ciebie kilka "+
             "przydatnych podarunk�w! Oto one:");
    npc->command("daj monety, koszule, spodnie i noz "+OB_NAME(byt));
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Mam nadzieje, �e ubrania b�d� pasowa�.");
}
void faza22nie()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Teraz mi to m�wisz?! Teraz nie?");
    npc->command("zalam sie");
}
void faza22inne()
{
    npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
             " Aj aj! Nie rozumiem, tak czy nie?");
}



/* A tutaj mamy list� reakcji na odpowiedzi :P
 * Pokazuje sie to za ka�dym razem (natychmiastowo!) po
 * pozytywnej lub negatywnej odpowiedzi.
 * Odpowiedzi b��dne (czyli wywo�uj�ce fazaXinne() nie s� 
 * brane pod uwag�).
 */
void reakcje_na_odpowiedzi()
{
    switch(random(17))//im wiecej tym lepiej. 12-15 jest optymalne.
    {
      case 0:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("emote m�wi szybko nerwowo gestykuluj�c:"+
                        " �wietnie!"); break;
             }
      case 1:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("powiedz Mhmm."); break;
             }
      case 2:
             npc->command("mrugnij nerwowo"); break;
      case 3:
             npc->command("mrugnij nerwowo"); break;
      case 4:
             npc->command("pokiwaj energicznie"); break;
      case 5:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("usmiechnij sie mimochodem"); break;
             }
      case 6:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("usmiechnij sie do siebie"); break;
             }
      case 7:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("powiedz Tak, tak!");
                                                            break;
             }
      case 8:
             npc->command("pokiwaj szybko"); break;
      case 9:
             npc->command("przewroc oczami"); break;
      case 10:
             npc->command("mrugnij"); break;
      case 11..13:
             npc->command("zachichocz cicho"); break;
      case 14:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("powiedz Mhm, �wietnie!"); break;
             }
      case 15:
             npc->command("rozejrzyj sie szybko"); break;
      case 16:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("powiedz Nie�le.");break;
             }

      default:

             npc->command("wzdrygnij sie"); break;
     }
}

void
przydziel_prezenty(int x)
{
    gift1=clone_object("/d/Standard/items/bronie/noze/maly_poreczny.c");

    if(x>128) //byczek
    {
        gift2=clone_object(UBRANIA+"spodnie/zwykle_plocienne_Mk.c");
        gift3=clone_object(UBRANIA+"koszule/przybrudzona_lniana_Mk.c");
    }
    else if(x<115) //maluch
    {
        gift2=clone_object(UBRANIA+"spodnie/krotkie_luzne_Sc.c");
        gift3=clone_object(UBRANIA+"koszule/przybrudzona_lniana_Sc.c");
    }
    else //w miar� norma
    {
        gift2=clone_object("/d/Standard/login/wioska/items/czarne_luzne_Lc.c");
        gift3=clone_object(UBRANIA+"koszule/stara_plocienna_gnom.c");
    }

//    gift1->init_arg(0);
    gift1->move(npc);
//    gift2->init_arg(0);
    gift2->move(npc);
//    gift3->init_arg(0);
    gift3->move(npc);

}
