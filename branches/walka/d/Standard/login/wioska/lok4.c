/*
 *  Po�udniowa cz�� placu wioski z etapu tworzenia postaci.
 *        By Lil.
 *                          Wed Mar 22 2006
 */

inherit "/d/Standard/login/wioska/std";

#include <std.h>
#include <stdproperties.h>
#include <macros.h>
#include <language.h>
#include <ss_types.h>
#include "login.h"
#define UBRANIA "/d/Standard/items/ubrania/"
#define SCIEZKA_UBR "/d/Standard/Redania/Rinde/przedmioty/ubrania/"
#define WIOSKA_ITEMS "/d/Standard/login/wioska/items/"

object gift1, gift2, gift3, gift4; //prezenty :P
object npc;
int faza_reakcji=0; //jesli jest 1, to npc nie reaguje na wybory np. mowiac "Wspaniale."
                    //dotyczy to przypadkow kiedy gracz np. stwierdzi, ze nie chce byc
                    //danej rasy. Wtedy to by brzmialo bez sensu.
object byt=this_player();

public void
create_wioska_room()
{
    set_short("Po�udniowa cz�� placu");
    add_exit("lok1",  "n");
    add_exit("lok6","s");
    npc=clone_object(PATH+"wioska/npc/polelf.c");
    npc->init_arg(0);
    npc->move(this_object());
    clone_object(PATH+"wioska/items/zawiniatko.c")->move(this_object());
    add_sit("na ziemi","na ziemi","z ziemi",0);
    npc->command("usiadz");
}

exits_description() 
{
	return "Dalsza cz�� placu ci�gnie si� na p�nocy, natomiast na po�udniu "+
			"dostrzegasz niewielk� le�n� polank�.\n";
}

string
dlugi_opis()
{
    string str;
//FIXME?
    str = "Na suchej, mocno sp�kanej ziemi siedzi odziany w czarne szaty "+
          "p�elf, przed kt�rym le�y jakie� zawini�tko. Silny wiatr co chwila "+
          "wzbija w powietrze kolejne tumany piachu i targa na wszystkie "+
          "strony d�ugimi w�osami postaci. "+
          "";

    return str;
}


void jaka_rasa()
{
    if(plec==1)
      {
        rasa="p�elfka";
      }
    else
      {
        rasa="p�elf";
      }
  race="p�elf";
}


/* Generalnie staramy si� tworzy� jak najmniej akcji npca
 * w jednej fazie. �eby na zafloodowa�o nagle ekranu...
 */

void faza0start()
{
    faza_reakcji=1;
    npc->command("emote m�wi spokojnie:"+
               " Chcesz cia�o miesza�ca?");
}

void fazaniebyt()
{
    npc->command("emote m�wi spokojnie:"+
             " Ju� podj"+koncoweczka("��e�","�a�")+
	     " decyzj�, nic tu po tobie.");
}
void faza0tak()
{
    faza_reakcji=1;
    npc->command("emote m�wi spokojnie:"+
             " Na miesza�ca ka�dy si� nadaje. Chcesz zosta� jedn"+koncoweczka("ym","�")+
             " z nas?");
}
void faza0nie()
{
    npc->command("emote m�wi spokojnie:"+
             " S�usznie. Udaj si� na p�noc, a mo�e dostaniesz inne, lepsze cia�o.");
}
void faza0inne()
{
    npc->command("emote m�wi spokojnie:"+
             " Nie zrozumieli�my si�. Tak czy nie?");
}
void faza1tak()
{
    faza_reakcji=1;
    npc->command("emote m�wi spokojnie:"+
             " Niech i tak si� stanie. Zostaniesz "+
             "jedn"+koncoweczka("ym","�")+" z nas.");
    npc->command("skrzyw sie nieznacznie");
    npc->command("emote m�wi spokojnie:"+
             " Nie wiesz na co w�a�nie zamierzasz podpisa� sw�j wyrok... "+
             "M�g�bym ci co� opowiedzie�, chcesz?");
}
void faza1nie()
{
    faza_reakcji=1;
    npc->command("emote m�wi spokojnie:"+
             " S�usznie. Udaj si� na p�noc, a mo�e dostaniesz inne, lepsze cia�o.");
}
void faza1inne()
{
    npc->command("emote m�wi spokojnie:"+
             " Nie zrozumieli�my si�. Tak czy nie?");
}

/* Dobrze by by�o, gdyby ka�dy enpec opowiada� histori� z w�asnego
 * punktu widzenia, nie?
 * Historia wi�c ma by� subiektywna. Im bardziej, tym lepiej ;)
 *
 *
 *
 * HISTORIA ROZBITA NA FAZY. PIERWSZA FAZA JEST NA SAMYM DOLE: faza2tak()
 * A OSTATNIA NA SAMEJ GORZE (faza2tak4()), wiec od konca czytamy ;)
 */
void faza2tak4()
{
    npc->command("emote m�wi spokojnie:"+
         " Eh, co z tob�? Chcesz pochodzi� z ojca i matki odmiennych ras?");
}
void faza2tak3()
{
    npc->command("emote m�wi spokojnie:"+
         " Ja ju� jestem sko�czony. Zostan� tu i zaczekam na �mier�.");
    set_alarm(4.0,0.0,&faza2tak4());
}
void faza2tak2()
{
    npc->command("emote m�wi spokojnie:"+
         " Ty masz wyb�r...Na to wygl�da. Je�li czujesz, �e taki tw�j los "+
         "i wierzysz w to, nie mog� ci zabroni�.");
    //npc->command("westchnij cicho");
    set_alarm(6.0,0.0,&faza2tak3());
}
void faza2tak1()
{
    npc->command("emote m�wi spokojnie:"+
         " Nie wybiera�em sobie swej rasy. Nikt mnie o to nie pyta�, zreszt� tak "+
         "jak i ojciec matki nie pyta� czy chce... Tak wysz�o. S�ysza�"+
         koncoweczka("e�","a�")+" zapewne "+
         "wiele opowie�ci o mi�o�ci mi�dzy cz�owiekiem a elfem, co? Powiem ci "+
         "jedno: �ajno prawda, niech sobie wsadz� te opowie�ci w rzy� "+
         "i tak ka�dy z nas jest niechcianym b�kartem.");
    npc->command("usmiechnij sie chlodno");
    set_alarm(18.0,0.0,&faza2tak2());
}
void faza2tak() //Opowiada historie + ostateczne pytanie o rase
{
    faza_reakcji=1;
    npc->command("emote m�wi spokojnie:"+
         " My, p� elfy, a p� ludzie. Wiesz, wielu przedstawicieli ras "+
         "z kt�rych pochodzimy nawet w tej po�owie nie traktuje nas tak, jak oni "+
         "siebie wzajemnie. Jeste�my odmie�cami i z tym si� licz.");
    set_alarm(10.0,0.0,&faza2tak1());
}
//koniec fazy opowiadania historii

void faza2nie()
{
    faza_reakcji=1;
    npc->command("emote m�wi spokojnie:"+
	     " A zatem, czy jeste� pew"+koncoweczka("ien","na")+
             " swego wyboru?");
}
void faza2inne()
{
    npc->command("emote m�wi spokojnie:"+
             " Nie zrozumieli�my si�. Tak czy nie?");
}
void faza3tak() //Ostateczne pytanie o rase. Nastepnie pytanie o wzrost
{
    npc->command("emote m�wi spokojnie:"+
                 " Dobrze zatem przejd�my do konkret�w.");
    npc->command("popatrz");
    npc->command("emote m�wi spokojnie:"+
             " Jakiego chcesz by� wzrostu?");
}
void faza3nie()
{
    npc->command("emote m�wi spokojnie:"+
             " Tak przypuszcza�em."); 
    npc->command("wzrusz ramionami");
}
void faza3inne()

{
    npc->command("emote m�wi spokojnie:"+
             " Nie zrozumieli�my si�. Tak czy nie?");
}
void faza4zle() //odpowiedz na wzrost, pytanie o wage
{
    npc->command("emote m�wi spokojnie:"+
             " Nie rozumiem. Zapytaj mnie, je�li masz problem.");
}
void faza4dobrze()
{
    npc->command("emote m�wi spokojnie:"+
             " Mhm, a teraz powiedz mi jakiej chcesz by� wagi.");
}
void faza5zle() //waga
{
    npc->command("emote m�wi spokojnie:"+
             " Nie zrozumieli�my si�. Zapytaj mnie, je�li masz problem z tym.");
}
void faza5dobrze()
{
    npc->command("emote m�wi spokojnie:"+
             " Ile chcesz mie� lat?");
}
void faza6nierozumiem() //wiek
{
    npc->command("emote m�wi spokojnie:"+
             " Nie rozumiem, ile?");
}
void faza6zamlody()
{
    npc->command("emote m�wi spokojnie:"+
             " A� tak"+koncoweczka("i","a")+
             " m�od"+koncoweczka("y","a")+
             " nie mo�esz by�. Musisz wybra� conajmniej "+
		"czterna�cie.");
}
void faza6zastary()

{
    npc->command("emote m�wi spokojnie:"+
             " To za du�o. Wybierz mniej ni� "+
		"siedemdziesi�t, a wi�cej ni� czterna�cie.");
}
void faza6dobrze()
{
    npc->command("emote m�wi spokojnie:"+
             " Dobrze. B�dziesz mia�"+koncoweczka("","a")+
             " zatem "+LANG_SNUM(wiek,0,1)+" "+slowo_lat+
             ". A oczy...Tak...Jakiego koloru chcesz mie� oczy?");
}
void faza7zle() //kolor oczu
{
    npc->command("emote m�wi spokojnie:"+
             " Nie. Wybierz: "+
    implode(m_indices(plec==1? oczy_polelfki:oczy_polelfa), ", ")+".");
}

/* Po prostu idziemy dalej, czyli
 * pytamy o to, czy �yczy sobie posiada� w�osy W OG�LE,
 * czyli, czy gracz chce by� �ysy NA STA�E.
 */
void faza7dobrze8()
{
    npc->command("emote m�wi spokojnie:"+
             " W�osy...Tak, co z w�osami? Chcesz je w og�le posiada� "+
		"przysz�"+koncoweczka("y","a")+
                " p�elf"+koncoweczka("ie","ko")+
		"?");
}
/* A tutaj omijamy pytanie o to, czy gracz chce w og�le
 * posiada� ow�osienie, poniewa� jest elfem lub p�elfem, a
 * oni nie mog� by� 'na sta�e' �ysi.
 * Oczywi�cie nikt im nie zabroni si� zgoli�, ale w�osy
 * b�d� ros�y im i tak... :)
 * Wi�c pytamy od razu o kolor w�os�w.
 */
void faza7dobrze9()
{
    faza8tak();
}

/* Je�li gracz chce posiada� w�osy w og�le, to jedziemy z pytaniem
 * o kolor w�os�w.
 */
void faza8tak()
{
   npc->command("emote m�wi spokojnie:" +
             " W�osy...Jaki chcesz mie� ich kolor?");
}

/* Kobietom nie zadajemy pyta� o zarost, wi�c omijamy kilka faz
 * i pytamy odrazu o budow� cia�a t� �ys� kobietk� <szok> ....
 */
void faza8nie1()
{
    npc->command("emote m�wi spokojnie:" +
                " To tw�j wyb�r.");
   faza11dobrze1();
}

/* A m�czy�ni i tak musz� poda� kolor...zarostu. �eby by�o wiadomo
 * jaki kolor brody i w�s�w im da� (gdyby posiadali w�osy, to wyliczaloby
 * si� to z koloru w�os�w, ale �e wybrali, �e s� permanentnie �ysi...)
 */
void faza8nie2()
{
    npc->command("emote m�wi spokojnie:" +
             " Musisz jednak wybra� kolor "+
	   "swojego zarostu, jaki to b�dzie kolor?");
}
/* Tak czy nie? Inna odpowied�...
 */
void faza8inne()
{
    npc->command("emote m�wi spokojnie:" +
             " Odpowiedz pozytywnie, je�li chcesz posiada� w�osy.");
}
/* kolor wlosow, pytanie o dlugosc
 */
void faza9zle() 
{
    npc->command("emote m�wi spokojnie:" +
             " C� to za kolor? Nie znam go. Znam natomiast "+
           //w przypadku innych ras jest s� to inne indeksy mapping�w
    implode(m_indices(wlosy_polelfow), ", ")+".");
}
void faza9dobrze()
{
    npc->command("emote m�wi spokojnie:" +
             " Wybierz jeszcze ich d�ugo��.");
}
/* Ta faza jest tylko wtedy, je�li si� wybra�o permanentnie �ysego.
 * Bo i tak trzeba poda� kolor zarostu. Wi�c ta faza jest tylko dla m�czyzn
 */
void faza10zle()
{
    npc->command("emote m�wi spokojnie:" +
             " Nie znam takiego koloru zarostu.");
}
void faza10dobrze()
{
    npc->command("emote m�wi spokojnie:" +
             " W�sy..Jak d�ugie chcesz je posiada�?");
}
/* W tej fazie gracz wybiera d�ugo�� w�os�w i dostaje pytanie
 * O w�sy, je�li jest m�czyzn�. Je�li jest elfem lub kobiet�,
 * to omijamy te pytania i skaczemy odrazu do pyt. o budowe cia�a
 */
void faza11zle()
{
    npc->command("emote m�wi spokojnie:" +
             " Nie znam takiej d�ugo�ci w�os�w.");
}
/* jesli jest: albo elfem, albo kobiet�, albo poni�ej 19 roku �ycia:
 */
void faza11dobrze1()
{
    faza13dobrze();
}
/* w przeciwnym wypadku:
 */
void faza11dobrze2()
{
    faza10dobrze();
}
/* d�ugo�� w�s�w:
 */
void faza12zle()
{
    npc->command("emote m�wi spokojnie:" +
             " Nie znam takiej d�ugo�ci w�s�w. Je�li masz "+
		"z czym� jaki� problem wystarczy mnie zapyta�.");
}
void faza12dobrze()
{
    npc->command("emote m�wi spokojnie:" +
             " A broda? Jakiej d�ugo�ci ma by�?");
}
/* d�ugo�� brody
 * i pytanie o ceche budowy cia�a 
 */
void faza13zle()
{
    npc->command("emote m�wi spokojnie:" +
             " Nie znam takiej d�ugo�ci.");
}
void faza13dobrze() // tu mnie zaczela bolec dynia
{
    npc->command("emote m�wi spokojnie:" +
             " A�eby doko^nczy� dzie�a, wybierz jedn� " +
             "cech� budowy przysz�ego cia�a.");
    npc->command("emote m�wi spokojnie:" +
             " Oto one: "+   //m["indeks"][2]
             COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
                             //lista filtrowana przez wybrana wage i wzrost
}


void faza14zle() //budowa ciala. Pytanie o ceche szczegolna
{
    npc->command("emote m�wi spokojnie:" +
             " Nie rozumiem... Wybierz: " +
               COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
}
void faza14dobrze()
{
    npc->command("emote m�wi spokojnie:" +
         " I jeszcze jedno. Pr�cz tego mo�esz posiada� " +
         "charakterystyczn� cech�: " +
         COMPOSITE_WORDS2(m_indexes(plec==1?cechy_szczegolne_kobiet:
         cechy_szczegolne_mezczyzn)," lub ")+". Czy chcesz?");
}
void faza15tak() //odpowiedz czy chce sie miec ceche szczegolna <opcjonalnie>
{
    npc->command("emote m�wi spokojnie:" +
             " Zatem jaka to cecha?");
}
void faza15nie()
{
    npc->command("emote m�wi spokojnie:" +
             " Jeszcze tylko kilka pyta�.");
    npc->command("emote m�wi spokojnie:" +
             " Mo�esz wybra� jakie� pochodzenie. Czy chcesz " +
             "je posiada�? "+
             "Oczywi�cie je�li nie wybierzesz teraz p�niej b�dziesz " +
             koncoweczka("m�g�", "mog�a") + " zmieni� sw�j " +
             "wyb�r, jednak�e ograniczy si� on tylko do miejsca, w kt�rym"+
             " b�dziesz si� znajdowa�"+koncoweczka("", "a")+".");
}
void faza15inne()
{
   npc->command("emote m�wi spokojnie:" +
             " Chcesz czy nie chcesz?");
}
void faza16zle() //wybieranie cechy szczegolnej (jesli sie zgodzilo wczesniej)
{
    npc->command("emote m�wi spokojnie:" +
             " Nie znam takiej cechy. Wybierz kt�r�� z tych, kt�re wymieni�em.");
}
void faza16dobrze()
{
    npc->command("emote m�wi spokojnie:" +
             " Najwa�niejsze, �eby� ty by�"+koncoweczka("","a")+
             " zadowolon"+koncoweczka("y","a")+".");
    npc->command("emote m�wi spokojnie:" +
             " Mo�esz wybra� jakie� pochodzenie. Czy " +
             "chcesz je posiada�? "+
             "Oczywi�cie je�li nie wybierzesz teraz p�niej b�dziesz " +
             koncoweczka("m�g�", "mog�a") + " zmieni� sw�j " +
             "wyb�r, jednak�e ograniczy si� on tylko do miejsca, w kt�rym"+
             " b�dziesz si� znajdowa�"+koncoweczka("", "a")+".");
}
void faza17tak() //odpowiedz czy chce sie posiadac pochodzenie.
{
    npc->command("emote m�wi spokojnie:" +
             " Sk�d wi�c chcesz pochodzi�?");
}
void faza17nie()
{
    npc->command("emote m�wi spokojnie:" +
             " Niech tak b�dzie... A teraz wybierz jedn� z sze�ciu cech, "+
             "kt�ra b�dzie twoj� siln� stron�. Do wyboru " +
             "masz si��, zr�czno��, wytrzyma�o��, inteligencj�, " +
             "m�dro�� b�d� odwag�.");
} 
void faza17inne()
{
    npc->command("emote m�wi spokojnie:" +
             " Nie zrozumieli�my si�. Tak czy nie?");
} 
void faza18zle() //wybor pochodzenia jesli sie zgodzilo wczesniej.
{
    npc->command("emote m�wi spokojnie:" +
             " �le, bo musisz wybra� jedno z tych miejsc, kt�re znam.");
}
void faza18dobrze()
{
    npc->command("emote m�wi spokojnie:" +
             " Tak jest. A teraz wybierz jedn� z sze�ciu cech, "+
             "kt�ra b�dzie twoj� siln� stron�. Do wyboru " +
             "masz si��, zr�czno��, wytrzyma�o��, inteligencj�, " +
             "m�dro�� b�d� odwag�.");
}
void faza19zle() //cechy
{
    npc->command("emote m�wi spokojnie:" +
             " Wybierz jedn� z tych sze�ciu.");
}
void faza19dobrze() //cechy
{
    npc->command("emote m�wi spokojnie:" +
             " Przejd�my teraz do umiej�tno�ci. Mianowicie mo�esz " +
             "wybra� dwie, w kt�rych b�dziesz si� specjalizowa�"+
             koncoweczka("","a")+". " +
             "Jaki jest tw�j pierwszy wyb�r?");
}
void faza20zle() //umiejetnosci - wybor pierwszej
{
    npc->command("emote m�wi spokojnie:" +
             " �le, bo nie znam takiej umiej�tno�ci, wi�c jej nie dostaniesz.");
}
void faza20dobrze()
{
    npc->command("emote m�wi spokojnie:" +
             " Aha, a druga umiej�tno��?");
}
void faza21zle() //umiejetnosci - wybor drugiej
{
    faza20zle();
}
/* nie mo�na wybra� dw�ch umiej�tno�ci takich samych pod rz�d.
 */
void faza21zle2()
{
    npc->command("emote m�wi spokojnie:" +
             " Przecie� wybra�"+koncoweczka("e�","a�")+" j� przed chwil�.");
}
void faza21dobrze()
{
    npc->command("pokiwaj nieznacznie");
    npc->command("emote m�wi spokojnie:" +
             " To wszystko. Jeste� gotow" +koncoweczka("y", "a")+"?");
}
void faza22tak() //podsumowanie + prezenty?:) No wlasnie...Jakie?
{
    npc->command("emote m�wi spokojnie:" +
             " Ess'tedd, esse creasa.");
    npc->command("emote odwija zawini�tko.");
    npc->command("daj monety, koszule, spodnie i noz "+OB_NAME(byt));
    npc->command("emote m�wi spokojnie:" +
             " �egnaj!");
}
void faza22nie()
{
    npc->command("emote m�wi spokojnie:" +
    " M�drze. Id� do innych ras, �yj�c ich �yciem, b�dzie ci �atwiej.");
}
void faza22inne()
{
    npc->command("emote m�wi spokojnie:" +
    " To znaczy tak, czy nie?");
}

/* A tutaj mamy list� reakcji na odpowiedzi :P
 * Pokazuje sie to za ka�dym razem (natychmiastowo!) po
 * pozytywnej lub negatywnej odpowiedzi.
 * Odpowiedzi b��dne (czyli wywo�uj�ce fazaXinne() nie s� 
 * brane pod uwag�).
 */
void reakcje_na_odpowiedzi()
{
    switch(random(9))//im wiecej tym lepiej.
    {
      case 0:
             faza_reakcji==1?:npc->command("emote m�wi spokojnie:" +
             " Dobrze."); break;
      case 1:
             npc->command("potrzyj policzek"); break;
      case 2:
             npc->command("powiedz Tak s�dzi�em."); break;
      case 3:
             npc->command("emote spogl�da przez chwil� "+
                   "nieobecnym wzrokiem w dal.");
                                                            break;
      case 4:
             npc->command("pokiwaj nieznacznie"); break;
      case 5:
             npc->command("emote zamyka na chwil� oczy."); break;
      case 6:
      case 7:
      case 8:
              break;

      default:
             npc->command("emote u�miecha si� niepewnie."); break;
     }
}

void
przydziel_prezenty(int x)
{
    gift1=clone_object("/d/Standard/items/bronie/noze/maly_poreczny.c");

    if(x>179) //byczek
    {
        gift2=clone_object(UBRANIA+"spodnie/krotkie_luzne_XLc.c");
        gift3=clone_object(UBRANIA+"koszule/plocienna_skromna_XXL.c");
    }
    else if(x<165) //maluch
    {
        gift2=clone_object(UBRANIA+"spodnie/krotkie_luzne_Sc.c");
        gift3=clone_object(UBRANIA+"koszule/przybrudzona_lniana_Sc.c");
    }
    else //w miar� norma
    {
        gift2=clone_object(SCIEZKA_UBR+"koszula_brazowa_lniana.c");
        gift3=clone_object(WIOSKA_ITEMS+"przybrudzone_lniane_M.c");
    }

//    gift1->init_arg(0);
    gift1->move(npc);
//    gift2->init_arg(0);
    gift2->move(npc);
//    gift3->init_arg(0);
    gift3->move(npc);

}
