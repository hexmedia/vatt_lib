/* Lil
 */

inherit "/std/monster";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";
inherit "/d/Standard/login/wioska/atrybuty.c";

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <language.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <login.h>
#define SCIEZKA_UBR "/d/Standard/Redania/Rinde/przedmioty/ubrania/"

mixed wzrost_i_waga;
int plec;
string race, rasa;


void
create_monster()
{
    ustaw_odmiane_rasy("p�elf");
    set_gender(G_MALE);
    set_long("Tajemnicza posta� o d�ugich, kruczoczarnych w�osach i "+
             "smuk�ej, tr�jk�tnej twarzy wygl�da niczym pos�g. Jedynie "+
             "wiecznie rozwiewane przez wiatr w�osy i szaty ka�� my�le�, "+
             "�e skrywaj�ca si� pod nimi istota jest z krwi i ko�ci. "+
             "Posta� ani razu nie uraczy�a ci� swym wzrokiem, poch�oni�ta "+
             "wpatrywaniem si� w le��ce przed ni� zawini�tko zdaje si� w "+
             "og�le nie zwraca� na ciebie uwagi.\n");

    dodaj_przym("tajemniczy", "tajemniczy");
    dodaj_przym("spokojny", "spokojni");

    set_act_time(38);
    add_act("emote wpatruje si� w le��ce przed nim zawini�tko.");
    add_act("potrzyj policzek");
    add_act("zamysl sie");
    add_act("zmruz oczy");
    add_act("zagryz wargi");
    add_act("emote zastyga w bezruchu.");

    set_default_answer(VBFC_ME("default_answer"));
    set_stats ( ({ 45, 61, 70, 31, 30, 68 }) );
    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));
    add_armour(SCIEZKA_UBR+ "plaszcz_obszerny_krwistoczerwony.c");

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 91000);
    add_prop(CONT_I_HEIGHT, 172);


    add_ask(({"wzrost","poziomy wzrostu","dost�pne poziomy wzrostu"}),
            VBFC_ME("pyt_o_wzrost"));
    add_ask(({"wag�","poziomy wagi","dost�pne poziomy wagi","oty�o��",
             "poziomy oty�o�ci","dost�pne poziomy oty�o�ci"}),
            VBFC_ME("pyt_o_wage"));
    add_ask("wiek",VBFC_ME("pyt_o_wiek"));
    add_ask(({"kolory oczu","kolor oczu","oczy"}), VBFC_ME("pyt_o_oczy"));
    add_ask(({"kolor w�os�w","kolory w�os�w"}), VBFC_ME("pyt_o_kol_wlosow"));
    add_ask(({"d�ugo�� w�os�w","d�ugo�ci w�os�w"}), VBFC_ME("pyt_o_dl_wlosow"));
    add_ask(({"pochodzenie","dost�pne miejsca pochodzenia","dost�pne pochodzenie"}),
             VBFC_ME("pyt_o_pochodzenie"));
    add_ask(({"cechy szczeg�lne","cechy dodatkowe","cechy wyj�tkowe"}),
             VBFC_ME("pyt_o_cechy_szczegolne"));
    add_ask(({"cechy budowy cia�a","cechy dotycz�ce budowy cia�a",
              "budow� cia�a"}),
             VBFC_ME("pyt_o_cechy_budowy"));
    add_ask("cechy",VBFC_ME("pyt_o_cechy"));
    add_ask(({"d�ugo�� w�os�w","dost�pne d�ugo�ci w�os�w","poziomy d�ugo�ci w�os�w"}),
              VBFC_ME("pyt_o_dlugosc_wlosow"));
    add_ask(({"umiej�tno�ci","list� umiej�tno�ci"}),
              VBFC_ME("pyt_o_umy"));
    add_ask(({"w�sy","d�ugo�� w�s�w","d�ugo�ci w�s�w","dost�pne d�ugo�ci w�s�w"}),
              VBFC_ME("pyt_o_wasy"));
    add_ask(({"brod�","brody","d�ugo�� brody","d�ugo�ci brody","dost�pne d�ugo�ci brody"}),
              VBFC_ME("pyt_o_brode"));
    add_ask(({"kolory zarostu","kolor zarostu","dost�pne kolory zarostu","kolor brody",
              "kolory brody"}),
              VBFC_ME("pyt_o_kolory_zarostu"));
    add_ask(({"histori�","histori� rasy","opowie��"}),
              VBFC_ME("pyt_o_historie"));
    add_ask("w�osy", VBFC_ME("pyt_o_wlosy"));
	add_ask(({"kolor","kolory"}), VBFC_ME("pyt_o_kolor"));

    MONEY_MAKE_K(2)->move(this_object());
}

string
default_answer()
{
     set_alarm(2.0, 0.0, "command_present", this_player(),
             "emote m�wi: Nie wiem.");
     return "";
}

void
return_introduce(string imie)
{
    command("'Witaj.");
}

void
add_introduced(string imie, string biernik)
{
    set_alarm(1.0, 0.0, "return_introduce", imie);
}
void jaka_rasa()
{
    if(plec==1)
      {
        rasa="p�elfka";
      }
    else
      {
        rasa="p�elf";
      }
  race="p�elf";
}

string
pyt_o_kolor()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Nie rozumiem. Masz na my�li kolor oczu, w�os�w? "+
		"A mo�e kolor zarostu?");

    return "";
}
string
pyt_o_wzrost()
{
    wzrost_i_waga=({HEIGHTDESC(koncoweczka("i","a")), 
                      WIDTHDESC(koncoweczka("y","a")) });

    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Mo�esz by� " +
                implode(wzrost_i_waga[0][0..-2], ", ") + " lub " + 
                wzrost_i_waga[0][sizeof(wzrost_i_waga[0]) - 1] + ".");

    return "";
}
string
pyt_o_wlosy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Nie rozumiem. Masz na my�li "+
	"d�ugo�� w�os�w, czy kolor w�os�w?");
    return "";
}
string
pyt_o_wage()
{
    wzrost_i_waga=({HEIGHTDESC(koncoweczka("i","a")), 
                      WIDTHDESC(koncoweczka("y","a")) });
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Mo�esz by� " +
                implode(wzrost_i_waga[1][0..-2], ", ") + " lub " + 
                wzrost_i_waga[1][sizeof(wzrost_i_waga[1]) - 1] + ".");

    return "";
}

string
pyt_o_wiek()
{
     jaka_rasa();
     set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Nie mo�esz wybra� mniej "+
        "ni� "+LANG_SNUM(DOLNA_GRANICA_WIEKU[race],0,1) +
        " lat, ani "+
        "wi�cej ni� "+LANG_SNUM(GORNA_GRANICA_WIEKU[race],0,1)+" lat.");
     return "";
}
string
pyt_o_oczy()
{
     plec=this_player()->query_gender();
     set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Masz do wyboru: "+
        COMPOSITE_WORDS2(m_indices(plec==1?oczy_polelfki:oczy_polelfa)," lub ")+".");
     return "";
}
string
pyt_o_kol_wlosow()
{
    plec=this_player()->query_gender();
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Masz do wyboru: "+
         COMPOSITE_WORDS2(m_indices(wlosy_polelfow), " lub ")+
         ".");
}
string
pyt_o_dl_wlosow()
{
     set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Masz do wyboru: "+
         COMPOSITE_WORDS2(m_indices(dlugosc_wlosow), " lub ")+
         ".");
}
string
pyt_o_pochodzenie()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Mo�esz wybra� w�r�d: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote bierze g��boki wdech.");
    set_alarm(2.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: "+COMPOSITE_WORDS2(pochodzenie," lub ")+".");
    return "";
}

string
pyt_o_cechy_szczegolne()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Mo�esz wybra� w�r�d: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: "+
    COMPOSITE_WORDS2(m_indices(this_player()->query_gender()==1?cechy_szczegolne_kobiet:
         cechy_szczegolne_mezczyzn)," lub ")+".");
    return "";
}

string
pyt_o_cechy_budowy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Ju� powiedzia�em.");
    return "";
}

string
pyt_o_cechy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Jest si�a, zr�czno��, wytrzyma�o��, inteligencja, "+
         "m�dro�� i odwaga.");
    return "";
}

string
pyt_o_dlugosc_wlosow()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Wybierz jedn� z takich d�ugo�ci w�os�w: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: "+
    COMPOSITE_WORDS2(m_indices(dlugosc_wlosow)," lub ")+".");
    return "";
}

string
pyt_o_umy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Do wyboru masz nast�puj�ce umiej�tno�ci: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: "+
    COMPOSITE_WORDS(m_indices(lista_skilli))+".");
    return "";
}
string
pyt_o_wasy()
{
    if(this_player()->query_gender()==1)
       set_alarm(1.5, 0.0, "command_present", this_player(),
             "powiedz do " + OB_NAME(this_player()) +
                  " A tobie po co o tym wiedzie�?");
    else {
       set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Wybierz w�r�d takich d�ugosci w�s�w: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: "+
    COMPOSITE_WORDS2(m_indices(dlugosc_wasow)," lub ")+".");
         }
    return "";
}
string
pyt_o_brode()
{
    if(this_player()->query_gender()==1)
       set_alarm(1.5, 0.0, "command_present", this_player(),
             "powiedz do " + OB_NAME(this_player()) +
                  " A tobie po co o tym wiedzie�?");
    else {
       set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Wybierz w�r�d takich d�ugo�ci brody: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: "+
    COMPOSITE_WORDS2(m_indices(dlugosc_brody)," lub ")+".");
         }
    return "";
}
string
pyt_o_kolory_zarostu()
{
    if(this_player()->query_gender()==1)
       set_alarm(1.5, 0.0, "command_present", this_player(),
             "powiedz do " + OB_NAME(this_player()) +
                  " A tobie po co o tym wiedzie�?");
    else {
       set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: Wybierz w�r�d takich kolor�w zarostu: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote m�wi spokojnie: "+
    COMPOSITE_WORDS2(kolory_zarostu," lub ")+".");
         }
    return "";
}
string
pyt_o_historie()
{
   set_alarm(1.0, 0.0, "command_present",
      this_player(),call_other(environment(this_player()),"faza2tak"));
}
