/* Lil
 */

inherit "/std/monster";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";
inherit "/d/Standard/login/wioska/atrybuty.c";

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <language.h>
#include <filter_funs.h>
#include <login.h>

mixed wzrost_i_waga;
int plec;
string race, rasa;


void
create_monster()
{
    ustaw_odmiane_rasy("gnom");
    set_gender(G_MALE);
    set_long("Ten nieco przysadzisty gnom w kwiecie wieku mo�e "+
             "poszczyci� si� wspania��, bujn�, smolistoczarn� "+
             "czupryn�, kt�ra si�ga mu prawie do pasa oraz "+
             "d�ugim, ostrym nosem, jak na gnom�w przysta�o. "+
             "Br�zowe, weso�o iskrz�ce oczy spogl�daj� na �wiat. "+
             "Dzi�ki nim ten poczciwy gnom wygl�da na bardzo "+
             "sympatyczn� i przyjazn� posta�. Jest prawdopodobnie "+
             "w�a�cicielem tego sklepiku i wydaje si� by� bardzo "+
             "zadowolony ze swego stanowiska. Krz�taj�c si� za lad�, "+
             "z u�miechem go�ci kazd� istot�, kt�ra tu zawita.\n");
    dodaj_przym("czarnow�osy", "czarnow�osi");
    dodaj_przym("ruchliwy", "ruchliwi");
    set_act_time(30);
    add_act("emote spogl�da na ciebie badawczo.");
    add_act("potrzasnij energicznie");
    add_act("czas");
    add_act("emote przegl�da zawarto�� swych p�ek.");
    add_act("emote przeciera d�oni� lad�.");
    add_act("emote poprawia towary na p�kach.");
    add_act("emote przygl�da ci si� ciekawie.");
    set_default_answer(VBFC_ME("default_answer"));
    set_stats ( ({ 65, 51, 60, 41, 40, 58 }) );
    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 61000);
    add_prop(CONT_I_HEIGHT, 152);

    add_ask(({"wzrost","poziomy wzrostu","dost�pne poziomy wzrostu"}),
            VBFC_ME("pyt_o_wzrost"));
    add_ask(({"wag�","poziomy wagi","dost�pne poziomy wagi","oty�o��",
             "poziomy oty�o�ci","dost�pne poziomy oty�o�ci"}),
            VBFC_ME("pyt_o_wage"));
    add_ask("wiek",VBFC_ME("pyt_o_wiek"));
    add_ask(({"kolory oczu","kolor oczu","oczy"}), VBFC_ME("pyt_o_oczy"));
    add_ask(({"kolor w�os�w","kolory w�os�w"}), VBFC_ME("pyt_o_kol_wlosow"));
    add_ask(({"d�ugo�� w�os�w","d�ugo�ci w�os�w"}), VBFC_ME("pyt_o_dl_wlosow"));
    add_ask(({"pochodzenie","dost�pne miejsca pochodzenia","dost�pne pochodzenie"}),
             VBFC_ME("pyt_o_pochodzenie"));
    add_ask(({"cechy szczeg�lne","cechy dodatkowe","cechy wyj�tkowe"}),
             VBFC_ME("pyt_o_cechy_szczegolne"));
    add_ask(({"cechy budowy cia�a","cechy dotycz�ce budowy cia�a",
              "budow� cia�a"}),
             VBFC_ME("pyt_o_cechy_budowy"));
    add_ask("cechy",VBFC_ME("pyt_o_cechy"));
    add_ask(({"d�ugo�� w�os�w","dost�pne d�ugo�ci w�os�w","poziomy d�ugo�ci w�os�w"}),
              VBFC_ME("pyt_o_dlugosc_wlosow"));
    add_ask(({"umiej�tno�ci","list� umiej�tno�ci"}),
              VBFC_ME("pyt_o_umy"));
    add_ask(({"w�sy","d�ugo�� w�s�w","d�ugo�ci w�s�w","dost�pne d�ugo�ci w�s�w"}),
              VBFC_ME("pyt_o_wasy"));
    add_ask(({"brod�","brody","d�ugo�� brody","d�ugo�ci brody","dost�pne d�ugo�ci brody"}),
              VBFC_ME("pyt_o_brode"));
    add_ask(({"kolory zarostu","kolor zarostu","dost�pne kolory zarostu","kolor brody",
              "kolory brody"}),
              VBFC_ME("pyt_o_kolory_zarostu"));
    add_ask(({"histori�","histori� rasy","opowie��"}),
              VBFC_ME("pyt_o_historie"));
    add_ask("w�osy", VBFC_ME("pyt_o_wlosy"));
	add_ask(({"kolor","kolory"}), VBFC_ME("pyt_o_kolor"));

    add_armour("/d/Standard/login/wioska/items/czarne_luzne_Lc.c");
    add_armour("/d/Standard/items/ubrania/koszule/stara_plocienna_gnom.c");

    MONEY_MAKE_K(2)->move(this_object());
}

void jaka_rasa()
{
    if(plec==1)
      {
        rasa="gnomka";
      }
    else
      {
        rasa="gnom";
      }
  race="gnom";
}

string
pyt_o_kolor()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi szybko nerwowo gestykuluj�c: Masz na my�li kolor oczu, w�os�w? "+
		"A mo�e kolor zarostu?");

    return "";
}
string
pyt_o_wzrost()
{
    wzrost_i_waga=({HEIGHTDESC(koncoweczka("i","a")), 
                      WIDTHDESC(koncoweczka("y","a")) });

    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi szybko nerwowo gestykuluj�c: Mo�esz by� " +
                implode(wzrost_i_waga[0][0..-2], ", ") + " lub " + 
                wzrost_i_waga[0][sizeof(wzrost_i_waga[0]) - 1] + ".");

    return "";
}
string
pyt_o_wage()
{
    wzrost_i_waga=({HEIGHTDESC(koncoweczka("i","a")), 
                      WIDTHDESC(koncoweczka("y","a")) });
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi szybko nerwowo gestykuluj�c:  Mo�esz by� " +
                implode(wzrost_i_waga[1][0..-2], ", ") + " lub " + 
                wzrost_i_waga[1][sizeof(wzrost_i_waga[1]) - 1] + ".");

    return "";
}
string
pyt_o_wlosy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi szybko nerwowo gestykuluj�c: Aj! Nie "+
	"rozumiem! Masz na my�li "+
	"d�ugo�� w�os�w, czy kolor w�os�w?");
    return "";
}
string
pyt_o_wiek()
{
     jaka_rasa();
     set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi szybko nerwowo gestykuluj�c: "+
        "By by� ju� samodzieln� osob� nie mo�esz mie� mniej "+
        "ni� "+LANG_SNUM(DOLNA_GRANICA_WIEKU[race],0,1) +
        " lat. Lecz by by� JESZCZE samodzieln� istot� nie mo�esz wybra� sobie "+
        "wi�cej ni� "+LANG_SNUM(GORNA_GRANICA_WIEKU[race],0,1)+" lat.");
     return "";
}
string
pyt_o_oczy()
{
     plec=this_player()->query_gender();
     set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi szybko nerwowo gestykuluj�c: Masz do wyboru: "+
        COMPOSITE_WORDS2(m_indices(oczy_gnomow)," lub ")+".");
     return "";
}
string
pyt_o_kol_wlosow()
{
    plec=this_player()->query_gender();
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: Masz do wyboru: "+
         COMPOSITE_WORDS2(m_indices(wlosy_gnomow), " lub ")+
         ".");
}
string
pyt_o_dl_wlosow()
{
     set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: Masz do wyboru: "+
         COMPOSITE_WORDS2(m_indices(dlugosc_wlosow), " lub ")+
         ".");
}
string
pyt_o_pochodzenie()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: Mo�esz wybra� w�r�d: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote bierze g��boki wdech.");
    set_alarm(2.0, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: "+
        COMPOSITE_WORDS2(pochodzenie," lub ")+".");
    return "";
}

string
pyt_o_cechy_szczegolne()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: Mo�esz wybra� w�r�d: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: "+
    COMPOSITE_WORDS2(m_indices(this_player()->query_gender()==1?cechy_szczegolne_kobiet:
         cechy_szczegolne_mezczyzn)," lub ")+".");
    return "";
}

string
pyt_o_cechy_budowy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: Aj aj aj! To� "+
        "powiedzia�em! Trzeba by�o s�ucha�!");
    return "";
}

string
pyt_o_cechy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: Jest si�a, zr�czno��, "+
         "wytrzyma�o��, inteligencja, "+
         "m�dro�� i odwaga.");
    return "";
}

string
pyt_o_dlugosc_wlosow()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: "+
        "Mo�esz wybra� jedn� z nast�puj�cych d�ugo�ci w�os�w: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote szepcze: "+
    COMPOSITE_WORDS2(m_indices(dlugosc_wlosow)," lub ")+".");
    return "";
}

string
pyt_o_umy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: "+
        "Tak, tak! Do wyboru masz nast�puj�ce umiej�tno�ci: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote szepcze: "+
    COMPOSITE_WORDS(m_indices(lista_skilli))+".");
    return "";
}
string
pyt_o_wasy()
{
    if(this_player()->query_gender()==1)
       set_alarm(1.5, 0.0, "command_present", this_player(),
             "emote m�wi  szybko nerwowo gestykuluj�c: "+
                  "Ej�e! Gnomki nie musz� o tym wiedzie�!");
    else {
       set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: "+
        "Do wyboru masz nast�puj�ce d�ugosci w�s�w: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: "+
    COMPOSITE_WORDS2(m_indices(dlugosc_wasow)," lub ")+".");
         }
    return "";
}
string
pyt_o_brode()
{
    if(this_player()->query_gender()==1)
       set_alarm(1.5, 0.0, "command_present", this_player(),
             "emote m�wi  szybko nerwowo gestykuluj�c: "+
                  "Ej�e! Gnomki nie musz� o tym wiedzie�!");
    else {
       set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: "+
        "Do wyboru masz nast�puj�ce d�ugo�ci brody: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: "+
    COMPOSITE_WORDS2(m_indices(dlugosc_brody)," lub ")+".");
         }
    return "";
}
string
pyt_o_kolory_zarostu()
{
    if(this_player()->query_gender()==1)
       set_alarm(1.5, 0.0, "command_present", this_player(),
             "emote m�wi  szybko nerwowo gestykuluj�c: "+
                  "Ej�e! Gnomki nie musz� o tym wiedzie�!");
    else {
       set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: "+
        "Do wyboru masz nast�puj�ce kolory zarostu: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote m�wi  szybko nerwowo gestykuluj�c: "+
    COMPOSITE_WORDS2(kolory_zarostu," lub ")+".");
         }
    return "";
}
string
pyt_o_historie()
{
   set_alarm(1.0, 0.0, "command_present",
      this_player(),call_other(environment(this_player()),"faza2tak"));
}


string
default_answer()
{
     set_alarm(2.0, 0.0, "command_present", this_player(),
             "emote m�wi szybko nerwowo gestykuluj�c d�o�mi: "+
                  " Przykro mi, ale nie wiem o czym m�wisz!");
     return "";
}

void
return_introduce(string imie)
{
    object osoba = present(imie, environment());

    if (osoba)
    {
        command("emote m�wi szybko nerwowo gestykuluj�c d�o�mi: "+
                "Bardzo mi mi�o!");
    }
}
void
add_introduced(string imie, string biernik)
{
    set_alarm(1.0, 0.0, "return_introduce", imie);
}

