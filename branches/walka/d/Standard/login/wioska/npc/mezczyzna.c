/* Lil
 */

inherit "/std/monster";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";
inherit "/d/Standard/login/wioska/atrybuty.c";

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <language.h>
#include <filter_funs.h>
#include <login.h>

mixed wzrost_i_waga;
int plec;
string race, rasa;

void
create_monster()
{
    ustaw_odmiane_rasy("m�zczyzna");
    set_gender(G_MALE);
    set_long("Jest to bardzo stary wysoki m�czyzna o d�ugich siwych "+
             "w�osach. Na jego pooranej licznymi zmarszczkami twarzy "+
             "wyr�niaj� si� p�omienne oczy, kt�re wygl�daj� na wci�� m�ode, "+
             "pe�ne wigoru i �ycia.\n");

    dodaj_przym("stary", "starzy");
    dodaj_przym("p�omiennooki", "p�omiennoocy");

    set_act_time(30);
    add_act("emote spogl�da na ciebie badawczo.");
    add_act("napij sie ze studni");
    add_act("czas");
    add_act("westchnij starczo");
    add_act("zamysl sie");
    add_act("rozejrzyj sie dyskretnie");
    add_act("emote przygl�da ci si� ciekawie.");

    set_default_answer(VBFC_ME("default_answer"));

    set_stats ( ({ 65, 31, 70, 21, 30, 58 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_HEIGHT, 176);


    add_ask(({"wzrost","poziomy wzrostu","dost�pne poziomy wzrostu"}),
            VBFC_ME("pyt_o_wzrost"));
    add_ask(({"wag�","poziomy wagi","dost�pne poziomy wagi","oty�o��",
             "poziomy oty�o�ci","dost�pne poziomy oty�o�ci"}),
            VBFC_ME("pyt_o_wage"));
    add_ask("wiek",VBFC_ME("pyt_o_wiek"));
    add_ask(({"kolory oczu","kolor oczu","oczy"}), VBFC_ME("pyt_o_oczy"));
    add_ask(({"kolor w�os�w","kolory w�os�w"}), VBFC_ME("pyt_o_kol_wlosow"));
    add_ask(({"d�ugo�� w�os�w","d�ugo�ci w�os�w"}), VBFC_ME("pyt_o_dl_wlosow"));
    add_ask(({"pochodzenie","dost�pne miejsca pochodzenia","dost�pne pochodzenie"}),
             VBFC_ME("pyt_o_pochodzenie"));
    add_ask(({"cechy szczeg�lne","cechy dodatkowe","cechy wyj�tkowe"}),
             VBFC_ME("pyt_o_cechy_szczegolne"));
    add_ask(({"cechy budowy cia�a","cechy dotycz�ce budowy cia�a",
              "budow� cia�a","cechy budowy","cech� budowy"}),
             VBFC_ME("pyt_o_cechy_budowy"));
    add_ask("cechy",VBFC_ME("pyt_o_cechy"));
    add_ask(({"d�ugo�� w�os�w","dost�pne d�ugo�ci w�os�w","poziomy d�ugo�ci w�os�w"}),
              VBFC_ME("pyt_o_dlugosc_wlosow"));
    add_ask(({"umiej�tno�ci","list� umiej�tno�ci"}),
              VBFC_ME("pyt_o_umy"));
    add_ask(({"w�sy","d�ugo�� w�s�w","d�ugo�ci w�s�w","dost�pne d�ugo�ci w�s�w"}),
              VBFC_ME("pyt_o_wasy"));
    add_ask(({"brod�","brody","d�ugo�� brody","d�ugo�ci brody","dost�pne d�ugo�ci brody"}),
              VBFC_ME("pyt_o_brode"));
    add_ask(({"kolory zarostu","kolor zarostu","dost�pne kolory zarostu","kolor brody",
              "kolory brody"}),
              VBFC_ME("pyt_o_kolory_zarostu"));
    add_ask(({"histori�","histori� rasy","opowie��"}),
              VBFC_ME("pyt_o_historie"));
    add_ask("w�osy", VBFC_ME("pyt_o_wlosy"));
	add_ask(({"kolor","kolory"}), VBFC_ME("pyt_o_kolor"));

    add_armour("/d/Standard/login/wioska/items/czarne_luzne_Lc.c");
    add_armour("/d/Standard/login/wioska/items/pozolkla_lniana_M.c");

    MONEY_MAKE_K(2)->move(this_object());
}

void jaka_rasa()
{
    if(plec==1)
      {
        rasa="kobieta";
      }
    else
      {
        rasa="m�czyzna";
      }
  race="cz�owiek";
}

string
pyt_o_kolor()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Nie rozumiem. Masz na my�li kolor oczu, w�os�w? "+
		"A mo�e kolor zarostu?");

    return "";
}
string
pyt_o_wzrost()
{
/*    set_alarm(1.0, 0.0, "command_present", this_player(),
	"szepnij " + this_player()->query_name(PL_CEL) + 
    " asdf"); //CZEMU SZEPTANIE NIE DZIALA?
*/
    wzrost_i_waga=({HEIGHTDESC(koncoweczka("i","a")), 
                      WIDTHDESC(koncoweczka("y","a")) });

    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Mo�esz by� " +
                implode(wzrost_i_waga[0][0..-2], ", ") + " lub " + 
                wzrost_i_waga[0][sizeof(wzrost_i_waga[0]) - 1] + ".");

    return "";
}
string
pyt_o_wage()
{
    wzrost_i_waga=({HEIGHTDESC(koncoweczka("i","a")), 
                      WIDTHDESC(koncoweczka("y","a")) });
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Mo�esz by� " +
                implode(wzrost_i_waga[1][0..-2], ", ") + " lub " + 
                wzrost_i_waga[1][sizeof(wzrost_i_waga[1]) - 1] + ".");

    return "";
}

string
pyt_o_wiek()
{
     jaka_rasa();
     set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: By by� ju� samodzieln� osob� nie mo�esz mie� mniej "+
        "ni� "+LANG_SNUM(DOLNA_GRANICA_WIEKU[race],0,1) +
        " lat. Lecz by by� jeszcze samodzieln� istot� nie mo�esz wybra� sobie "+
        "wi�cej ni� "+LANG_SNUM(GORNA_GRANICA_WIEKU[race],0,1)+" lat.");
     return "";
}
string
pyt_o_oczy()
{
     plec=this_player()->query_gender();
     set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Masz do wyboru: "+
        COMPOSITE_WORDS2(m_indices(plec==1?oczy_kobiety:oczy_mezczyzny)," lub ")+".");
     return "";
}
string
pyt_o_kol_wlosow()
{
    plec=this_player()->query_gender();
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Masz do wyboru: "+
         COMPOSITE_WORDS2(m_indices(plec==1? wlosy_kobiety:wlosy_mezczyzny), " lub ")+
         ".");
}
string
pyt_o_dl_wlosow()
{
     set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Masz do wyboru: "+
         COMPOSITE_WORDS2(m_indices(dlugosc_wlosow), " lub ")+
         ".");
}
string
pyt_o_pochodzenie()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Mo�esz wybra� w�r�d: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote bierze g��boki wdech.");
    set_alarm(2.0, 0.0, "command_present", this_player(),
	"emote szepcze: "+COMPOSITE_WORDS2(pochodzenie," lub ")+".");
    return "";
}

string
pyt_o_cechy_szczegolne()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Mo�esz wybra� w�r�d: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote szepcze: "+
    COMPOSITE_WORDS2(m_indices(this_player()->query_gender()==1?cechy_szczegolne_kobiet:
         cechy_szczegolne_mezczyzn)," lub ")+".");
    return "";
}

string
pyt_o_cechy_budowy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Ju� m�wi�em! Nie b�d� si� powtarza�.");
    return "";
}

string
pyt_o_cechy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Jest si�a, zr�czno��, wytrzyma�o��, inteligencja, "+
         "m�dro�� i odwaga.");
    return "";
}

string
pyt_o_dlugosc_wlosow()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Mo�esz wybra� jedn� z nast�puj�cych d�ugo�ci w�os�w: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote szepcze: "+
    COMPOSITE_WORDS2(m_indices(dlugosc_wlosow)," lub ")+".");
    return "";
}
string
pyt_o_wlosy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Nie rozumiem. Masz na my�li "+
	"d�ugo�� w�os�w, czy kolor w�os�w?");
    return "";
}

string
pyt_o_umy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Do wyboru masz nast�puj�ce umiej�tno�ci: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote szepcze: "+
    COMPOSITE_WORDS(m_indices(lista_skilli))+".");
    return "";
}
string
pyt_o_wasy()
{
    if(this_player()->query_gender()==1)
       set_alarm(1.5, 0.0, "command_present", this_player(),
             "powiedz do " + OB_NAME(this_player()) +
                  " Odczep si�! Baba jeste� czy ch�op?");
    else {
       set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Do wyboru masz nast�puj�ce d�ugosci w�s�w: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote szepcze: "+
    COMPOSITE_WORDS2(m_indices(dlugosc_wasow)," lub ")+".");
         }
    return "";
}
string
pyt_o_brode()
{
    if(this_player()->query_gender()==1)
       set_alarm(1.5, 0.0, "command_present", this_player(),
             "powiedz do " + OB_NAME(this_player()) +
                  " Odczep si�! Baba jeste� czy ch�op?");
    else {
       set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Do wyboru masz nast�puj�ce d�ugo�ci brody: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote szepcze: "+
    COMPOSITE_WORDS2(m_indices(dlugosc_brody)," lub ")+".");
         }
    return "";
}
string
pyt_o_kolory_zarostu()
{
    if(this_player()->query_gender()==1)
       set_alarm(1.5, 0.0, "command_present", this_player(),
             "powiedz do " + OB_NAME(this_player()) +
                  " Odczep si�! Baba jeste� czy ch�op?");
    else {
       set_alarm(1.0, 0.0, "command_present", this_player(),
	"emote szepcze: Do wyboru masz nast�puj�ce kolory zarostu: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
	"emote szepcze: "+
    COMPOSITE_WORDS2(kolory_zarostu," lub ")+".");
         }
    return "";
}
string
pyt_o_historie()
{
   set_alarm(1.0, 0.0, "command_present",
      this_player(),call_other(environment(this_player()),"faza2tak"));
}


string
default_answer()
{
     set_alarm(1.5, 0.0, "command_present", this_player(),
             "powiedz do " + OB_NAME(this_player()) +
                  " Nie wiem o czym m�wisz.");
     return "";
}

void
return_introduce(string imie)
{
    object osoba = present(imie, environment());

    if (osoba)
    {
        command("powiedz do " + OB_NAME(osoba) + " Mi�o mi ci� pozna�.");
    }
}
void
add_introduced(string imie, string biernik)
{
    set_alarm(1.0, 0.0, "return_introduce", imie);
}

