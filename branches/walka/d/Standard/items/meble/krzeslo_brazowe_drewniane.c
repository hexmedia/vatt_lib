//Krzeslo zwykle. Lil
inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("krzes�o");
                 
    dodaj_przym("br�zowy","br�zowi");
    dodaj_przym("drewniany","drewniani");
    
   make_me_sitable("na","na drewnianym b��kitnym krze�le","z krzes�a",1);
                                                                               
    set_long("Proste, drewniane krzes�o zbite z kilku desek "+
             "mieni si� r�nymi kolorami w odcieniach br�zu.\n");
    add_prop(OBJ_I_WEIGHT, 3250);
    add_prop(OBJ_I_VOLUME, 12500);
    add_prop(OBJ_I_VALUE, 2);
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_DR_DAB);
}
