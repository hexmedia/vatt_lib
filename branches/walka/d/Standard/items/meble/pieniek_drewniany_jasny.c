inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("pieniek");
    dodaj_przym("drewniany", "drewniani");
    dodaj_przym("jasny", "ja�ni");
    ustaw_material(MATERIALY_DR_BUK);
    set_type(O_MEBLE);
    set_long("Kawa� sporego pie�ka, odci�tego zapewnie z jakiego� wiekowego " +
             "drzewa. Pieniek jest czysty, mo�na na nim swobodnie usi���.\n");
	     
    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 12000);
    add_prop(OBJ_I_VALUE, 1);

    make_me_sitable("na","na pie�ku","z pie�ka",1);
}
