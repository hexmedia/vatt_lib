inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("taboret");
    dodaj_przym("wysoki", "wysocy");
    dodaj_przym("drewniany", "drewniani");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Wysoki taboret, zbity z kilku niezbyt dobrze dobranych desek, " +
             "przez co ka�da z czterech jego n�g jest nier�wna i taboret " +
             "ko�ysze si� co chwil�, gdy kto� na nim siedzi.\n");
	     
    add_prop(OBJ_I_WEIGHT, 3000);
    add_prop(OBJ_I_VOLUME, 8000);
    add_prop(OBJ_I_VALUE, 1);

    make_me_sitable("na","na rozklekotanym taborecie","z taboretu",1);
}
