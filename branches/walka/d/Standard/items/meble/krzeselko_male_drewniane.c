inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("krzese�ko");
    dodaj_przym("ma�y", "mali");
    dodaj_przym("drewniany", "drewniani");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);

    set_long("Niewielkie krzese�ko przeznaczone zapewne dla dziecka, lub " +
             "dla kogo� o niewielkim wzro cie.\n");
	     
    add_prop(OBJ_I_WEIGHT, 3000);
    add_prop(OBJ_I_VOLUME, 8000);
    add_prop(OBJ_I_VALUE, 1);

    make_me_sitable("na","na ma�ym krzese�ku","z krzese�ka",1);
}

