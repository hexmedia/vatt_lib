
/* Lawa z klecznikiem, made by Avard 08.06.06  */
/* Uzyta w:
(/d/Standard/Redania/Rinde/poludnie/placnw.c)
*/
inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("^lawka");
    dodaj_przym("drewniany","drewniani");
    dodaj_przym("zdobiony","zdobieni");

    set_long("Wida^c, ^ze ta drewniana ^lawka przeznaczona jest do "+
        "postawienia na zewn^atrz budynku, mocne d^ebowe drewno na pewno "+
        "nie podda si^e ^latwo z^lej pogodzie. Na jej oparciu mo^zna "+
        "zauwa^zy^c misternie wykonane zdobienia przedstawiaj^ace "+
        "roz^lo^zysty d^ab. \n");
    make_me_sitable("na","na ^lawce","z ^lawki",3);
    add_prop(OBJ_I_WEIGHT, 5000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 260);
    add_prop(OBJ_M_NO_SELL, 1);
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
}

