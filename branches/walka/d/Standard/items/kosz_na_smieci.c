/* Autor: Avard
   Opis : Samaia
   Data : 08.07.07*/

inherit "/std/receptacle";

#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "/sys/formulas.h"

void
create_receptacle()
{
    ustaw_nazwe("kosz");
    dodaj_nazwy("^smietnik");
    dodaj_przym("bia^ly", "biali");
    dodaj_przym("drewniany","drewniani");

    set_long("Niewielki, zniszczony pojemnik, s^lu^z^acy jako kosz na "+
        "^smieci zbity zosta^l z kilku desek, pomalowanych na bia^lo. "+
        "By^c mo^ze dawniej wygl^ada^lo to estetycznie, lecz obecnie "+
        "farba star^la si^e w wielu miejscach ods^laniaj^ac prawdziwy - "+
        "jasnobr^azowy - kolor drewna. Wewn^etrz pojemnik zosta^l obity "+
        "p^l^otnem, by co mniejsze ^smiecie nie wypada^ly na zewn^atrz.\n");
         
    add_prop(OBJ_I_VALUE, 0);
    add_prop(CONT_I_RIGID, 1);
    add_prop(CONT_I_MAX_VOLUME, 500000);
    add_prop(CONT_I_MAX_WEIGHT, 500000);
    add_prop(CONT_I_VOLUME, 3000);
    add_prop(CONT_I_WEIGHT, 3000);
    set_type(O_INNE);
    add_prop(CONT_I_NO_OPEN_DESC, 1);
    ustaw_material(MATERIALY_DR_DAB);

    set_alarm_every_hour("smieci");
}

void
smieci()
{
    deep_inventory()->remove_object();
    //Chyba wypadaloby dac jakis opis oprozniania kosza, 
    //ale niestety nie mam teraz zadnego pomyslu. ;)
}