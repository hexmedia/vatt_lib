/* Zwyk�a ko�� sze�cio�cienna. Mo�na ni� rzuca� (normalnie)
 * lub rzuci� na jaki� st�
 *                                Vera
 */
 
/* Ko�� ta jest sprzedawana u p�elfa na placu w Rinde
 */

inherit "/std/object";


#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>
#include <cmdparse.h>
private int rzut (string str);
//int rzuc ();
//int pomoc ();

string *losowy_przym()
{
  string *lp, *lp2;
   string *lm, *lm2;
   int i;
   lp=({ "zwyk�y", "drewniany", "br�zowy"});
   lp2=({"starty", "sze^scio^scienny",
          "kolorowy"});
   lm=({"zwykli", "drewniani", "br�zowi"});
   lm2=({"starci", "sze^scio^scienni", 
          "kolorowi"});
   i=random(3);
   return ({ lp[i] , lm[i], lp2[i], lm2[i] });
}

void
create_object()
{
  string *przm = losowy_przym();
  ustaw_nazwe("kostka");
  dodaj_nazwy("kosc");
  dodaj_przym(przm[0], przm[1]);
  dodaj_przym(przm[2], przm[3]);
  
  set_long("Zwyk�a, drewniana ko�� sze�cio�cienna.\n");
  add_prop(OBJ_I_VOLUME, 19);
  add_prop(OBJ_I_WEIGHT,4);
  add_prop(OBJ_I_VALUE, 6);
}

void
init ()
{
  ::init ();
  add_action("rzut", "rzu^c");
  add_action("pomoc", "?", 2);
}

//jesli dziala tak jak jest to ponizsza fun juz niepotrzebna ;)
/*
int
filterek_kostek(object ob)
{
    if (environment(ob) == this_player()) return 1;
    return 0;
}
*/

private int
rzut(string str)
{
    object *obs;
   object *gdzie;
    int ret;
    string jak;

    notify_fail("Rzu^c czym [w kogo / gdzie]?\n");

    if (!strlen(str))
        return 0;

/*    if (!parse_command(str, all_inventory(this_player()),
        "%i:" + PL_NAR, obs))
        return 0;

    obs = NORMAL_ACCESS(obs, 0, 0);

    if (!sizeof(obs))
        return 0;

    ret = obs[0]->rzuc();
    if (ret)
        this_player()->set_obiekty_zaimkow(obs);
    return 1;*/
    if (parse_command(str, all_inventory(this_player()),
        "%i:" + PL_NAR, obs))
        {
             obs = NORMAL_ACCESS(obs, 0, 0);

             if (sizeof(obs) != 1)
	     {
            return 0;
	     }
	    
              if (obs[0]->move(environment(this_player())))
	      {
		      return 0;
	      }
              ret = obs[0]->rzuc_normalnie();
            if (ret)
            this_player()->set_obiekty_zaimkow(obs);
           return 1;
        }
     if (parse_command(str, environment(this_player()),
        "%i:"+PL_NAR+" 'na' %i:"+PL_BIE, obs, gdzie))
        {
//	   obs = NORMAL_ACCESS(obs, "filterek_kostek", this_object());

	   obs = INV_ACCESS(obs); // oto jak sie uzywa INV_ACCESSa ! tadam!
	   gdzie = NORMAL_ACCESS(gdzie, 0, 0);

	   if (sizeof(obs) != 1)
	   {
		   return 0;
	   }
	
	   if (sizeof(gdzie) != 1)
	   {
		   return 0;
	   }
	   if(member_array("na", gdzie[0]->query_sublocs()) == -1)
	   {
	      notify_fail("Nie mo�esz rzuci� "+obs[0]->short(PL_NAR)+" na "+
	                  gdzie[0]->short(PL_BIE)+".\n");
	      return 0;
	   }

	   if (obs[0]->move(gdzie[0], "na"))
	   {
		   return 0;
	   }
	   ret = obs[0]->rzuc_gdzies(gdzie[0]);
	   if (ret)
            this_player()->set_obiekty_zaimkow(obs);
           return 1;
	}
}

int
rzuc_normalnie()
{
  string liczba;
  int liczbap = random(6) + 1;
  
  switch(liczbap)
    {
    case 1:
    liczba = "jeden";break;
    case 2:
    liczba = "dwa";break;
    case 3:
    liczba = "trzy";break;
    case 4:
    liczba = "cztery";break;
    case 5:
    liczba = "pi^e^c";break;
    case 6:
    liczba = "sze^s^c";break;
    }
  
  
  write("Rzucasz " + short(PL_NAR) + ". Wypada cyfra " + liczba + ".\n");
  saybb(QCIMIE(this_player(), PL_MIA) + " rzuca " + short(PL_NAR) + 
        ". Wypada cyfra " + liczba + ".\n");
  return 1;
}

public int
pomoc (string str)
{
    object kosteczka;
    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, kosteczka))
        return 0;

    if (kosteczka != this_object())
        return 0;


  write("Mo�esz j� rzuci�.\n");
  return 1;
}

int
rzuc_gdzies(object ob)
{
  string liczba;
  int liczbap = random(6) + 1;
  
  switch(liczbap)
    {
    case 1:
    liczba = "jeden";break;
    case 2:
    liczba = "dwa";break;
    case 3:
    liczba = "trzy";break;
    case 4:
    liczba = "cztery";break;
    case 5:
    liczba = "pi^e^c";break;
    case 6:
    liczba = "sze^s^c";break;
    }
  
  
  write("Rzucasz " + short(PL_NAR) + " na "+ob->short(PL_BIE)+
        ". Wypada cyfra " + liczba + ".\n");
  saybb(QCIMIE(this_player(), PL_MIA) + " rzuca " + short(PL_NAR) + 
        " na "+ ob->short(PL_BIE)+". Wypada cyfra " + liczba + ".\n");
  return 1;
}
