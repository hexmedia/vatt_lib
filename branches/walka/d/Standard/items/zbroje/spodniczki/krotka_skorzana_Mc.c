
/* Autor: Avard
   Opis : Tinardan
   Data : 19.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
    ustaw_nazwe("spodniczka");
    dodaj_przym("kr^otki","kr^otcy");
    dodaj_przym("sk^orzany","sk^orzani");

    set_long("Sp^odniczka wygl^ada raczej na cz^e^s^c zbroi ni^z ubranie. "+
        "Uszyta jest z szerokich na trzy palce, osobnych pask^ow sk^ory, "+
        "kt^ore na siebie nachodz^a. Ka^zdy pasek nabijany jest ^cwiekami, "+
        "a przy ko�cu okuty ^zelazem. Sp^odniczka zapina si^e w talii na "+
        "mocn^a, prost^a klamr^e. Sk^ora, z kt^orej zosta^la zrobiona jest "+
        "twarda i specjalnie wzmacniana przez garbarza. Paski pozszywane "+
        "s^a z kilku jej warstw tak, by gruby materia^l chroni^l przed "+
        "ciosami. \n");

    set_slots(A_THIGHS, A_HIPS);
    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 2160);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    set_size("M");
    ustaw_material(MATERIALY_SK_SWINIA, 70);
    ustaw_material(MATERIALY_ZELAZO, 30);
    set_type(O_ZBROJE);
    set_ac(A_THIGHS,2,3,8, A_HIPS,2,3,8);
}