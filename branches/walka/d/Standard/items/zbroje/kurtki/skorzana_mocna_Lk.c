
/* Autor: Avard
   Opis : Erhest
   Data : 20.06.05 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("kurtka");
    dodaj_przym("sk^orzany","sk^orzani");
    dodaj_przym("mocny","mocni");

    set_long("Gruba warstwa sk^ory, odpowiednio okrojona i wygarbowana "+
		"tworzy teraz jak^ze przydatn^a w zimne dni cz^e^s^c ubrania. Po "+
		"dok^ladnym przyjrzeniu si^e jej, stwierdzasz, ^ze by^la to niegdy^s "+
		"wierzchnia cz^e^s^c cia^la jakiego^s nied^zwiedzia. Obecnie jednak, "+
		"przerobiona na kurtke, dobrze sprawuje si^e w nowej roli. Ca^lo^s^c "+
		"wykonania psuj^a jednak twarde szwy przy rekawach i mankietach "+
		"kt^ore wbijaj^a si^e w cia^lo, przez co noszenie jej nie jest "+
		"najwygodniejsze.\n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    set_ac(A_TORSO, 3,3,8, A_FOREARMS, 3,3,8, A_ARMS, 3,3,8);
    add_prop(OBJ_I_WEIGHT, 2500);
    add_prop(OBJ_I_VOLUME, 1100);
    add_prop(OBJ_I_VALUE, 120);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_SK_NIEDZWIEDZ, 100);
    add_prop(ARMOUR_S_DLA_RASY, "krasnolud");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
}