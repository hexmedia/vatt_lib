inherit "/std/receptacle.c";

#include <stdproperties.h>
#include <pl.h>
#include <materialy.h>

create_container()
{
    set_long("S�oik wykonany jest z grubego, ciemnego szk�a,"
			+" kt�re po�yskuje delikatnie odbijaj�c padaj�ce"
			+" na� �wiat�o. Dodatkowo zaopatrzono go w"
			+" solidn� pokrywk� dzi�ki kt�rej zawarto��"
			+" s�oika nie ma szans przez przypadek wypa��.\n");

    ustaw_nazwe( ({ "s�oik", "s�oika", "s�oikowi", "s�oik",
        "s�oikiem", "s�oiku" }), ({ "s�oiki", "s�oik�w",
        "s�oikom", "s�oiki", "s�oikami", "s�oikach" }) ,
        PL_MESKI_NOS_NZYW);


	dodaj_przym("ciemny", "ciemni");
    dodaj_przym("szklany", "szklani" );

    add_prop(CONT_I_MAX_WEIGHT, 1000);
    add_prop(CONT_I_MAX_VOLUME, 1000);
    add_prop(CONT_I_RIGID,1);
    add_prop(CONT_I_TRANSP,1);
	add_prop(OBJ_I_VALUE,183);
	//s�oik sam z siebie wa�y:
	add_prop(OBJ_I_WEIGHT,333);

	ustaw_material(MATERIALY_RZ_SZKLO,100);
}


/* Ziola w sloiku nie wiedna... */
void
enter_inv(object ob, object from)
{
  ::enter_inv(ob, from);
  ob->stop_decay();
}

/* ...ale poza nim tak. */
void
leave_inv(object ob, object to)
{
  ::leave_inv(ob, to);
  ob->start_decay();
}
