/*
 * Jest to ucho Iluendila, elfa.
 * Podczas walki z Janisem je straci�, oprawca za� zabra� je sobie
 * jako skalp, natomiast ofiara pozostanie ju� po kres swych dni
 * jednoucha.
 * Vera. Fri May 4 16:21:46 2007
 */
#include <stdproperties.h>
#include <pl.h>
inherit "/std/food";

create_food() 
{

    set_decay_time(1);
	set_long("Ucho zosta�o najprawdopodbniej brutalnie wyci�te "+
	"ofierze, o czym �wiadcz� plamy zaschni�tej krwi. Du�y "+
	"rozmiar i spiczasto zako�czona ko�c�wka �wiadcz� o tym, �e "+
	"zapewne pochodzi�o ono od doros�ego elfa.\n");
	ustaw_nazwe("ucho");
	dodaj_przym("spiczasty","spicza�ci");

	set_amount(10);
	stop_decay();

	jak_pachnie="@@zapach@@";
    //jak_wacha_jak_sie_zachowuje="@@reakcja@@";

}

string
zapach(string str)
{
  str="Ucho �mierdzi st�chlizn�, lecz znacznie mocniej odczuwalny "+
		"jest zapach jakiego� mocno chemicznego eliksiru.";
  return str;
}
/*
string
reakcja(string str)
{
  str=" w^acha kup^e starych, ^smierdz^acych ubra^n";
  return str;
}*/
