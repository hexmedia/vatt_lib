
/* Autor: Avard
   Opis : Samaia
   Data : 22.08.07 */

inherit "/std/kolczyk.c";

#include <pl.h>
#include "/sys/stdproperties.h"
#include <materialy.h>
#include <object_types.h>

void
create_kolczyk()
{
    dodaj_przym("filigranowy","filigranowi");
    dodaj_przym("srebrny","srebrni");

    set_long("Ten urokliwy, a jednocze^snie skromny kolczyk w postaci "+
        "srebrnej kulki oraz zatopionej w niej drobinki b^lyszcz^acego, "+
        "czarnego kamyczka, zapewne onyksu, zosta^l umieszczony na "+
        "kr^otkim, posrebrzanym druciku, co umo^zliwia umieszczenie go w "+
        "brodzie, brwi, tudzie^z innej cz^e^sci cia^la. Malutka, owalna "+
        "blaszka pomaga kolczykowi mocno przylega^c do cia^la i nie "+
        "zmienia^c swojego po^lo^zenia.\n");
               
    add_prop(OBJ_I_VOLUME, 100);
    add_prop(OBJ_I_WEIGHT, 10);
    add_prop(OBJ_I_VALUE, 200);
    ustaw_material(MATERIALY_SREBRO, 100);
    set_type(O_BIZUTERIA);
}