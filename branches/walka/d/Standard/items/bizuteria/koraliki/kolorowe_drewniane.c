
/* kolorowe drewniane koraliki
Wykonano dnia 10.06.06 przez Avarda. Opis napisany przez Tinardan 
Sprzedawane w:
Straganiarz w Rinde(RINDE centrum/magazyn_straganiarza)
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("koraliki");
	  
    dodaj_przym("kolorowy","kolorowi");
    dodaj_przym("drewniany","drewniani");

    set_long("Kilka kawa^lk^ow pospiesznie ukszta^ltowanego i wyg^ladzonego "+
        "drewna zosta^lo pomalowane na r^o^zne kolory i po^l^aczone w jeden "+
        "sznur korali. S^a dosy^c krzykliwe w barwach, czerwone, zielone, "+
        "b^l^ekitne i ^z^o^lte przeplataj^a sie na nitce tworz^ac "+
        "najr^o^zniejsze wzory. Koraliki pobrz^ekuj^a cicho ocieraj^ac si^e "+
        "o siebie i wygl^adaj^a na tyle ^ladnie, ^ze ka^zda mieszczanka "+
        "chcia^laby je mie^c w swojej kolekcji. \n");

    set_slots(A_NECK);
    set_type(O_BIZUTERIA);
    add_prop(OBJ_I_WEIGHT, 50);
    add_prop(OBJ_I_VOLUME, 50);
    add_prop(OBJ_I_VALUE, 25);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
    ustaw_material(MATERIALY_DR_LIPA, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_NECKLACE, 1);
    set_size("M");

	  }
string
query_auto_load()
{
   return ::query_auto_load();
}
