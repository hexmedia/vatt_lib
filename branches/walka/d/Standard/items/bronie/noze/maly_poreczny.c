/* Taki sobie zwykly noz. Bo zadnego nie bylo :P
 * Lil. Monday 08 of May 2006
 */

/*
 *U�ywany w sklepie z broniami w Rinde
 */
inherit "/std/weapon";
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>

void
create_weapon()
{
    ustaw_nazwe("n�");
    dodaj_przym("ma�y","mali");
    dodaj_przym("por�czny","por�czni");
    set_long("Niewielki n� nie jest ani zbyt mocny do kuchennych woja�y z "+
             "mi�sem, a tym bardziej nie nadaje si� do walki. Jest natomiast "+
             "niezast�piony, je�li chodzi o te drobniejsze czynno�ci, jak "+
             "otwieranie koperty, czy te� krojenie chleba.\n");

    add_prop(OBJ_I_WEIGHT, 300);
    add_prop(OBJ_I_VOLUME, 300);
    add_prop(OBJ_I_VALUE, 270);

    set_hit(1);
    set_pen(1);
    set_wt(W_KNIFE);
    set_dt(W_IMPALE | W_SLASH);
    ustaw_material(MATERIALY_STAL, 90);
    ustaw_material(MATERIALY_DR_SOSNA, 10);
    set_type(O_BRON_SZTYLETY);
    set_hands(A_ANY_HAND);
}

string
query_auto_load()
{
    return ::query_auto_load();
}
