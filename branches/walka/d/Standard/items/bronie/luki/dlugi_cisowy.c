#include <stdproperties.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>

inherit "/std/strzelecka";

void
create_strzelecka()
{                    
    dodaj_przym("d^lugi", "d^ludzy");
    dodaj_przym("cisowy", "cisowi");
    ustaw_nazwe("^luk");
                    
    set_long("Ju^z na pierwszy rzut oka wida^c, ^ze wyszed^l on spod r^eki "+
              "rzemie^slnika niew^atpliwie znaj^acego si^e na rzeczy. "+
              "Opleciony rzemieniem majdan zapewnia wygod^e uchwytu, a "+
              "gruba, konopna ci^eciwa nie porani palc^ow strzelca. Samo "+
              "^luczysko za^s, d^lugie na cztery ^lokcie i umiej^etnie "+
              "wyprofilowane, pozwala przy odrobinie wprawy posy^la^c "+
              "strza^l^e nawet na setki jard^ow.\n");

    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VOLUME, 1200); //?
    add_prop(OBJ_I_VALUE, 3600);
    set_min_sila(25); //...bo wiecej sie nie da
    set_celnosc(70);
    set_szybkosc(7);
    set_sila(85);
    set_zasieg(1);
    set_pocisk("strza^la");
}
