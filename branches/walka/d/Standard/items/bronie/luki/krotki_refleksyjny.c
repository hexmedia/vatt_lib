
/* Autor: Avard
   Opis : vortak
   Data : 21.01.07 */

inherit "/std/luk.c";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>

void
create_luk()
{
    ustaw_nazwe("�uk");
    dodaj_przym("jesionowy","jesionowi");
    dodaj_przym("refleksyjny","refleksyjni");

    set_long("�uk ten jest zbudowany z kilku warstw po��czonych ze sob� tak "+
    "�ci�le, �e ci�ko wyr�ni� granic� mi�dzy nimi. Dzi�ki zastosowaniu "+
    "�ci�gien zwierz�cych bro� jest niespotykanie spr�ysta, za� warstwa "+
    "rogowa, kt�r� pokryto brzusiec ��czyska zapewnia niezwyk�� si�� "+
    "ra�enia. Rdze� stanowi jednolity fragment jesionu. Do naci�gni�cia "+
    "takiej konstrukcji wymagana jest niema�a si�a. Ca�y drzewiec zosta� "+
    "specjalnie zmatowiony dzi�ki czemu nie odbija nawet najmniejszych "+
    "refleks�w �wietlnych. Podczas walki d�o� mo�na wygodnie oprze� na "+
    "wyprofilowanym korkowym majdanie, za� na ko�cach obydwu ramion "+
    "umieszczono miedziane, finezyjnie rze�bione gryfy. Jedwabna ci�ciwa, "+
    "zako�czona wzmocnionymi owijk� p�tlami zaczepiona jest na specjalnie "+
    "w tym celu wykonanych zag��biebiach. Mniej wi�cej w po�owie jej "+
    "d�ugo�ci podobna owijka tworzy siode�ko - miejsce, w kt�rym zak�ada "+
    "si� strza��.\n");

    set_min_sila(10);
    set_szybkosc(5);
    set_celnosc(90);
    set_zasieg(1);
    set_pocisk("strza�a");
    add_prop(OBJ_I_WEIGHT, 1000);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 4500);
}