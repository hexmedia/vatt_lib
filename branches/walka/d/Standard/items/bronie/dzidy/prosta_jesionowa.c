
/*opis by Tu^lacz
zakodowane by Sedzik 5.04.2007
*/

inherit "/std/weapon";

#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <wa_types.h>

void create_weapon()
{
        ustaw_nazwe("dzida");
        dodaj_przym("prosty", "pro^sci");
        dodaj_przym("jesionowy", "jesionowi");
        set_long("Jest to obok pa^lki jedna z najprostszych broni jakie zna "
		+"^swiat, aby j^a wykona^c wystarczy znale^x^c prosty kij i zaostrzy^c go "
		+"na jednym ko^ncu. Ten egzemplarz jest wykonany z jesionowego drewna, "
		+"kt^ore mimo, i^z nosi ^slady wyg^ladzania ci^agle jest wystarczaj^aco "
		+"szorstkie by nie wy^slizgiwac si^e z r^eki podczas walki. \n");
        
        set_hit(14);
        set_pen(12);

        add_prop(OBJ_I_WEIGHT, 4200);
        add_prop(OBJ_I_VOLUME, 3130);
	add_prop(OBJ_I_VALUE, 1549);
        set_wt(W_POLEARM);
        set_dt(W_IMPALE);
        
        set_hands(W_BOTH);
        ustaw_material(MATERIALY_DR_JESION, 100);
        set_type(O_BRON_DRZEWCOWE_K);
}

