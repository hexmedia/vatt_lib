/*
 *  Athelas - aromatyczny podluzny lisc
 *  Wlasciwosci - jak dotad nie znane
 */
#pragma save_binary
// #pragma strict_types

inherit "/std/herb";

#include <stdproperties.h>
#include <pl.h>

create_herb()
{
    ustaw_nazwe( ({"lisc", "liscia", "lisciowi", "lisc", "lisciem",
	"lisciu"}), ({"liscie", "lisci", "lisciom", "liscie", "lisciami",
	"lisciach"}), PL_MESKI_NOS_NZYW);
    dodaj_przym("aromatyczny", "aromatyczni");
    dodaj_przym("podluzny", "podluzni");
    ustaw_nazwe_ziola( ({"athelas", "athelasu", "athelasowi", "athelas",
	"athelasem", "athelasie"}), PL_MESKI_NOS_NZYW);

    set_amount(3);
    set_decay_time(720);

    set_id_diff(20);
    set_find_diff(9);

    set_ingest_verb( ({"powachaj", "wachasz", "wacha", "powachac"}) );
    
    add_prop(OBJ_I_WEIGHT, 20);
    add_prop(OBJ_I_VOLUME, 6);
    add_prop(OBJ_I_VALUE, 650);
}
