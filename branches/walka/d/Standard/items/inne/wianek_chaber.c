/* Rantauryczny wianek - moze byc dziurawy :P */

inherit "/std/armour";

#include <wa_types.h>
#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("wianek");

    dodaj_przym("b��kitnokwiecisty", "b��kitnokwieci�ci");
    dodaj_przym("drobny", "drobni");

    set_long("Delikatny, b��kitnej barwy wianek upleciony zosta�"
            +" ze �wie�ych chabr�w, tworz�c swoiste nakrycie g�owy,"
            +" kt�re przywodzi na my�l wiosenn� ��k�. Pomi�dzy"
            +" barwnymi kwiatami tkwi� soczysto zielone li�cie,"
            +" dope�niaj�c kompozycj�, a ca�o�� roztacza wok�"
            +" przyjemny zapach, dodaj�c tym samym uroku nosz�cej"
            +" j� osobie.\n");
            
    set_slots(A_HEAD);
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VOLUME, 70);
    add_prop(OBJ_I_VALUE, 2);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_IN_ROSLINA, 100);

    // Za��my, ze wianek pasuje na kazda glowe...
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 1000000.0);
}
