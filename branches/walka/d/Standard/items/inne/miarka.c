/**
 * \file miarka.c
 *
 * Plik z definicja miarki krawieckiej.
 * Autor: Jeremian, poprawki:  Vera.
 */

inherit "/std/object";

#include <pl.h>
#include <macros.h>

/**
 * Tablica z czesciami ciala do mierzenia i opisami czynnosci
 */
mixed* cele = ({
    ({({"obw�d g�owy", "g�ow�"}), "Obwijasz", PL_BIE, "wok� g�owy","obwija"}),
    ({({"obw�d szyi", "szyj�"}), "Obwijasz", PL_BIE, "wok� szyi","obwija"}),
    ({({"obw�d bark�w", "barki"}),"Obwijasz",PL_BIE,"wok� bark�w", "obwija"}),
    ({({"d�ugo�� ramienia"}), "Przyk�adasz", PL_BIE, "wzd�u� ramienia",
        "przyk�ada"}),
    ({({"obw�d ramienia"}), "Obwijasz", PL_BIE, "wok� ramienia","obwija"}),
    ({({"d�ugo�� przedramienia"}), "Przyk�adasz",PL_BIE,"wzd�u� przedramienia",
        "przyk�ada"}),
    ({({"obw�d przedramienia"}), "Obwijasz", PL_BIE, "wok� przedramienia",
        "obwija"}),
    ({({"obw�d nadgarstka", "nadgarstek"}), "Obwijasz", PL_BIE,
        "wok� nadgarstka", "obwija"}),
    ({({"d�ugo�� d�oni"}), "Przyk�adasz", PL_BIE, "wzd�u� d�oni","przyk�ada"}),
    ({({"szeroko�� d�oni"}), "Przyk�adasz", PL_BIE, "wszerz d�oni",
        "przyk�ada"}),
    ({({"d�ugo�� tu�owia", "tu��w"}), "Przyk�adasz", PL_BIE, "wzd�u� tu�owia",
        "przyk�ada"}),
    ({({"obw�d w klatce piersiowej", "klatk�", "klatk� piersiow�","klat�"}),
        "Obwijasz si�", PL_NAR, "w klatce piersiowej", "obwija si�"}),
    ({({"obw�d w pasie", "pas"}), "Obwijasz si�", PL_NAR, "w pasie",
        "obwija si�"}),
    ({({"obw�d w biodrach", "biodra"}), "Obwijasz si�", PL_NAR, 
        "w biodrach", "obwija si�"}),
    ({({"d�ugo�� uda"}), "Przyk�adasz", PL_BIE, "wzd�u� uda","przyk�ada"}),
    ({({"obw�d uda"}), "Obwijasz", PL_BIE, "wok� uda", "obwija"}),
    ({({"d�ugo�� goleni"}), "Przyk�adasz", PL_BIE, "wzd�u� goleni",
        "przyk�ada"}),
    ({({"obw�d goleni"}), "Obwijasz", PL_BIE, "wok� goleni",
        "obwija"}),
    ({({"obw�d kostki", "kostk�"}), "Obwijasz", PL_BIE, "wok� kostki",
         "obwija"}),
    ({({"d�ugo�� stopy"}), "Przyk�adasz", PL_BIE, "wzd�u� stopy",
        "przyk�ada"}),
    ({({"szeroko�� stopy"}), "Przyk�adasz", PL_BIE, "wszerz stopy",
        "przyk�ada"})   });

/**
 * Funkcja tworzaca obiekt.
 */

create_object()
{
    ustaw_nazwe("miarka");
    set_long("D�ugi kawa�ek ta�my z naniesionymi w r�wnych odst�pach "+
             "kreskami. Co kilka kresek wykaligrafowane widniej� "+
             "liczby oznaczaj�ce odleg�o�� od pocz�tku miarki.\n");
    dodaj_przym("krawiecki", "krawieccy");
}

public int
pomoc(string str)
{
    object ob;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, ob))
        return 0;

    if (!ob) return 0;

    if (ob != this_object())
        return 0;

    write("Jest to miarka, kt�r� mo�esz sobie zmierzy�: "+
          "obw�d g�owy, obw�d szyi, obw�d bark�w, d�ugo�� ramienia, "+
          "obw�d ramienia, d�ugo�� przedramienia, obw�d "+
          "przedramienia, obw�d nadgarstka, d�ugo�� d�oni, "+
          "szeroko�� d�oni, d�ugo�� tu�owia, obw�d w klatce "+
          "piersiowej, obw�d w pasie, obw�d w biodrach, d�ugo�� uda "+
          "obw�d uda, d�ugo�� goleni, obw�d goleni, obw�d kostki "+
          "d�ugo�� stopy oraz szeroko�� stopy.\n");

    return 1;
}




/**
 * Funkcja inicjalizujaca obiekt.
 */

void
init()
{
    add_action("zmierz", "zmierz");
    add_action("pomoc", "?", 2);
    ::init();
}

/**
 * Funkcja odpowiadajaca za mierzenie.
 */

int
zmierz(string tekst)
{
    int przerwac = 0;
    int ind = 0;

    foreach(mixed line : cele)
    {
        foreach(string cel : line[0]) 
        {
            if (tekst ~= cel)
            {
                przerwac = 1;
            }
        }
        if (przerwac)
            break;
        ++ind;
    }

    if (przerwac)
    {
         write(cele[ind][1] + " " + short(this_player(), cele[ind][2]) +
              " " +cele[ind][3] + ". Na miarce wyszlo " +
              sprintf("%.2f",this_player()->oblicz_rozmiary_osoby()[ind]) +
              " cm.\n");
         saybb(QCIMIE(this_player(), PL_MIA) +" "+ cele[ind][4] + " " +
               QSHORT(this_object(),cele[ind][2]) + " " + cele[ind][3]+".\n");
         return 1;
    }

    notify_fail("Zmierz co ?\n");
}

/**
 * Funkcja oznaczajaca auto-odtwarzanie przedmiotu.
 */
string
query_auto_load()
{
    return ::query_auto_load();
}
