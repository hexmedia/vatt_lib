/*
 * Meski skorzany pas
 *
 * Autor: Lil, opis: Dinrahia(Aneta)
 */

inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <object_types.h>
#include "/sys/materialy.h"

void
create_armour()
{

        ustaw_nazwe("pas");
        dodaj_przym("m�ski","m�scy");
        dodaj_przym("sk�rzany", "sk�rzani");
        set_long("Wykonany z dobrze wygarbowanej sk�ry ten pas b�dzie "+
                 "d�ugo s�u�y� swojemu w�a�cicielowi. Mo�e nie sprawia "+
                 "wra�enia nowego, ale na pewno jest mocny. Cechuje go "+
                 "br�zowoczerwona barwa i prostota wykonania, jednak z "+
                 "braku uszczerbk�w na swej matowej powierzchni nie mo�e �le "+
                 "�wiadczy� o u�ytecznosci przedmiotu.\n");
        set_slots(A_HIPS);
        add_prop(OBJ_I_WEIGHT, 770);
        set_type(O_UBRANIA);
        add_prop(OBJ_I_VOLUME, 373);
        add_prop(OBJ_I_VALUE, 15);
        add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 90);
        /* Ustawiamy materia�y, z kt�rych jest wykonany pas */
        ustaw_material(MATERIALY_SK_SWINIA);
}

string
query_auto_load()
{
   return ::query_auto_load();
}
