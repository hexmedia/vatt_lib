
/* Ublocone drewniane chodaki
Wykonano dnia 23.07.2005 przez Avarda. Opis zaczerpniety z banku opisow,
napisany przez Tinardan w dziale "ubrania i inne przedmioty" 
dnia 18.07.2005*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("chodak");
    ustaw_nazwe_glowna("para");
    dodaj_przym("ublocony","ubloceni");
    dodaj_przym("drewniany","drewniani");

    set_long("Na pierwszy rzut oka wida^c, ^ze te buty nie stanowi^a "+
        "marzenia ka^zdego podr^o^znika. Ci^e^zkie, topornie ociosane dwa "+
        "drewniane kloce z otworami na stopy. Ich jedyna ozdob^a s^a "+ 
        "zabawnie wyrze^xbione czuby na tym, co w zwyk^lych butach mo^znaby "+
        "nazwa^c noskiem. Ich powierzchnia jest pokryta warstw^a "+
        "zaskorupia^lego b^lota.\n");

    set_slots(A_FEET);
    add_prop(OBJ_I_WEIGHT, 2000);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 75);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_DR_BUK, 100);
    add_prop(ARMOUR_S_DLA_RASY, "niziolek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}

string
query_auto_load()
{
   return ::query_auto_load();
}