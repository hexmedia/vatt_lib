
/* Przybrudzona lniana koszula 
Wykonano dnia 23.07.2005 przez Avarda. Opis napisany przez Tinardan 
Poprawki by Avard, dnia 20.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("koszula");
    dodaj_przym("przybrudzony","przybrudzeni");
    dodaj_przym("lniany","lniani");
    set_long("Plamy i ^laty wyra^xnie odznaczaj^a si^e na jasnym materiale "+
        "koszuli. Mankiety d^lugich r^ekaw^ow i ko^lnierz strz^epi^l si^e "+
        "na ko^ncach i gubi^l dlugie szare nitki. Koszula prawdopodobnie"+
        "by^la kiedy^s bia^la, ale teraz kolorem przypomina raczej py^l "+
        "unosz^acy si^e nad brukowanymi ulicami miast. Niemniej jednak"+
        "sprawia wra^zenie ciep^lej i do^s^c wygodnej, a materia^l jest "+
        "mi^ekki i przyjemny w dotyku. \n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    add_prop(OBJ_I_WEIGHT, 300);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 15);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_LEN, 100);
    add_prop(ARMOUR_S_DLA_RASY, "niziolek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
}
string
query_auto_load()
{
   return ::query_auto_load();
}
