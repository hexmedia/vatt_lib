
/* lekki skorzane pantofelki
Wykonano by Avard 07.06.06
Opis by Faeve */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("pantofelki");
    ustaw_nazwe_glowna("para");
    dodaj_przym("lekki","lekcy");
    dodaj_przym("sk^orzany","sk^orzany");

    set_long("Buciki te, niebywale lekkie, wykonane z cieniutkiej sk^orki, "+
        "z pewno^sci^a nie nadaj^a si^e na dalekie woja^ze i deszczow^a "+
        "pogode, jednak wydaj^a si^e by^c w sam raz na kr^otkie wypady do "+
        "miasta. Niski obcasik i lekko zaokr^aglone czubki powoduj^a, ^ze "+
        "stopa w tym pantofelku wygl^ada na niewielk^a i zgrabn^a.\n");

    set_slots(A_FEET);
    add_prop(OBJ_I_WEIGHT, 400);
    add_prop(OBJ_I_VOLUME, 600);
    add_prop(OBJ_I_VALUE, 75);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_SK_CIELE, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    set_size("M");
}

string
query_auto_load()
{
   return ::query_auto_load();
}