// mam dopisac, ze opis jest Tinardan.
// T� szat� nosi pracownik poczty w Rinde.

inherit "/std/armour.c";
#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("szata");
    dodaj_przym("d^lugi", "d^ludzy");
    dodaj_przym("jasnobr^azowy", "jasnobr^azowi");
    set_long("Materia^l jest w dotyku niczym jedwab, mi^ekko za^lamuje " +
             "si^e przy karczku i r^ekawach i sprawia wra^zenie bardzo " +
             "delikatnego. Jednak po grubo^sci w^l^okien od razu mo^zna " +
             "pozna^c, ^ze tkacz, kt^ory go zrobi^l, przyk^lada^l si^e nie " +
             "tylko do jego wygl^adu, ale te^z wytrzyma^lo^sci i odporno^sci " +
             "na ka^zde warunki pogodowe." +
             " Jasnobr^azowy odcie^n szaty sprawia, i^z osoba nosz^aca " +
             "to ubranie nie rzuca si^e w oczy i przy odrobinie dobrej woli " +
             "mo^ze si^e z ^latwo^sci^a ukry^c. D^lugie r^ekawy zwi^azywane " +
             "s^a na ko^ncu rzemieniem, tak by dopasowa�y si^e szeroko^sci^a "+
             "do nadgarstka, a d^o^l szaty dla wygody rozkrojono po bokach " +
             "a^z do wysoko^sci bioder." +
             " Przy wyci^eciu na szyj^e i u do^lu szaty widniej^a zielone "+
             "i srebrne hafty, tworz^ace jaki^s zawi^ly wz^or, przeplataj^ace"+
             " si^e i przenikaj^ace nawzajem niczym dwa l^sni^ace w^e^ze.\n");
    ustaw_material(MATERIALY_JEDWAB);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 100); // zeby sie nie srac...
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 100);
    set_slots(A_ROBE);
    add_prop(OBJ_I_VOLUME, 1800);
    add_prop(OBJ_I_VALUE, 30);
    add_prop(OBJ_I_WEIGHT, 886);
}


string
query_auto_load()
{
    return ::query_auto_load();
}
