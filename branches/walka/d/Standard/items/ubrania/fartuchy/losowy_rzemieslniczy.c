/* Zwykly brudny fartuszek,
    dla kowali, karczmarzy itepe
                    Lil 01.05                */

inherit "/std/armour.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>

string *losowy_przym()
 {
   string *lp;
   string *lm;
   int i;
   lp=({ "brudny", "poplamiony", "przybrudzony"});
   lm=({"brudni", "poplamieni", "przybrudzeni"});
   i=random(3);
   return ({ lp[i] , lm[i] });
 }
 string *losowy_przym2()
 {
   string *lp;
   string *lm;
   int i;
   lp=({ "szary", "d�ugi", "stary"});
   lm=({"szarzy", "d�udzy", "starzy"});
   i=random(3);
   return ({ lp[i] , lm[i] });
 }
 
void
create_armour()
{
   string *przm = losowy_przym();
   string *przm2 = losowy_przym2();
   ustaw_nazwe("fartuch");
   dodaj_przym( przm[0], przm[1] );
   dodaj_przym( przm2[0], przm2[1] );
   set_long("D�ugi prostok�t mocnego materia�u wi�zany jest grubymi "+
                 "pasmami na szyi oraz z ty�u na wysoko�ci l�d�wi. W czasach "+
		 "swej �wietno�ci m�g� by� bia�y, lecz teraz przybiera "+
		 "jedynie odcienie ��ci i szaro�ci z ciemniejszymi plamami "+
		 "w kilku miejscach. Jest ju� nad�arty z�bem czasu, acz "+
		 "nadal wygl�da solidnie.\n");
   
   add_prop(OBJ_I_WEIGHT, 2481);
   add_prop(OBJ_I_VOLUME, 2000);
   add_prop(OBJ_I_VALUE, 44);
   set_type(O_UBRANIA);
   set_ac(A_THIGHS | A_HIPS | A_STOMACH, 2, 2, 2);
   ustaw_material(MATERIALY_SK_SWINIA);
   add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 100); //przeciez nie da sie go 'nie zalozyc'!
   add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 100); //w koncu to fartuch!
   
   
}


string
query_auto_load()
{
    return ::query_auto_load();
}
