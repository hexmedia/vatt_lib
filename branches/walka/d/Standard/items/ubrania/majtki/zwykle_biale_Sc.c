
/* 
 * Zwykle biale majtki
 * Wykonane przez Avarda dnia 18.03.06
 * Poprawki by Avard dnia 20.05.06
 */

 inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
	ustaw_nazwe("majtki");
    ustaw_nazwe_glowna("para");
	dodaj_przym("zwyk^ly","zwykli");
	dodaj_przym("bia^ly","biali");
	
	set_long("Nie wyr^o^zniaj^ace si^e niczym nadzwyczajnym majtki s^a wykonane "+
		"z dobrej jako�ci bawe^lny. \n");

	set_slots(A_HIPS);
	add_prop(OBJ_I_WEIGHT, 20);
    add_prop(OBJ_I_VOLUME, 150);
    add_prop(OBJ_I_VALUE, 20);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("S");
	ustaw_material(MATERIALY_BAWELNA, 100);
	set_type(O_UBRANIA);
}
string
query_auto_load()
{
   return ::query_auto_load();
}
