
/* Grube welniane skarpety
Wykonano dnia 21.06.2005 przez Avarda. Opis zaczerpniety z banku opisow,
napisany przez Ghalleriana w dziale "ubrania i inne przedmioty" 
Poprawki by Avard dnia 20.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <pl.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("skarpety");
    ustaw_nazwe_glowna("para");

    dodaj_przym("gruby","grubi");
    dodaj_przym("we^lniany","we^lniani");

    set_long("Skarpety na kt^ore spogl^adasz nie charakteryzuj^a si^e "+
        "niczym wyj^atkowym. Uszyte zosta^ly z we^lny wysokiej jako^sci, "+
        "zapewne owiec ^zyj^acych wysoko w g^orach. Dobrze i mocno "+
        "wykonane b^ed^a zapewne d^lugo s^lu^zy^ly w dalekich podr^o^zach. "+
        "Ze wzgl^edu na swoj^a grubo^s� dobrze b^ed^a chroni� przed zimnem "+
        "jak i przetarciami.\n");

    set_slots(A_FEET);
    ustaw_material(MATERIALY_WELNA, 100);
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VOLUME, 90);
    add_prop(OBJ_I_VALUE, 18);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("XS");

}
string
query_auto_load()
{
   return ::query_auto_load();
}