
/* Delikatna elficka tunika
   Wykonana przez Avarda, dnia 19.05.06 
   Opis by Tinardan */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("tunika");
    dodaj_przym("delikatny","delikatni");
    dodaj_przym("elfi","elfi");
    set_long("Materia^l zdaje si^e by^c najcie^nszym jedwabiem, utkanym "+
        "przez mistrz^ow najwy^zszej klasy. Tunika barwiona jest na ciemn^a "+
        "ziele^n, jej d^lugie rozszerzane r^ekawy zdobi misterny haft. U "+
        "do^lu, przy mankietach i ko^lnierzu obszyta jest kremowym "+
        "aksamitem, a z ty^lu widnieje ledwo dostrzegalny rysunek lec^acego "+
        "or^la.\n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS, A_HIPS);
    add_prop(OBJ_I_VOLUME, 1550);
    add_prop(OBJ_I_WEIGHT, 400);
    add_prop(OBJ_I_VALUE, 400);
    add_prop(ARMOUR_S_DLA_RASY, "elf");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("S");
    ustaw_material(MATERIALY_JEDWAB, 100);
    set_type(O_UBRANIA);

}

string
query_auto_load()
{
   return ::query_auto_load();
}