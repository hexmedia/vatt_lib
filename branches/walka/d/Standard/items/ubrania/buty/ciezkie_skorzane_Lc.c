/* Autor: Avard
   Opis : Faeve
   Data : 16.06.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");
    dodaj_przym("ci^e^zki","ci^e^zcy");
    dodaj_przym("sk^orzany","sk^orzani");

    set_long("Z grubej, dobrej gatunkowo sk^ory, o pi^eknie wyprofilowanym "+
        "nosku i wysokiej cholewie musz^a by^c niebywale wygodne, cho^c "+
        "zapewne do^s^c ci^e^zkie. Mocna, gumowa podeszwa poznaczona jest "+
        "bruzdami, by u^latwi^c w^edr^owk^e po nier^ownym terenie. Buty "+
        "wi^azane s^a dwoma d^lugimi sznurowad^lami przewlekanymi przez "+
        "starannie wyci^ete w sk^orze dziurki. Wy^zej, w okolicy ^lydek, "+
        "przymocowano, z obu stron ka^zdego buta, po trzy niedu^ze haczyki, "+
        "na kt^orych zaczepia si^e sznur^owki na krzy^z - dzi^eki temu but "+
        "^swietnie trzyma nog^e i prawdopodobie^nstwo wyst^apienia "+
        "jakiego^s urazu jest doprawdy niewielkie.\n");
        
    set_slots(A_FEET);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VALUE, 80);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
    ustaw_material(MATERIALY_SK_CIELE, 100);
    set_type(O_UBRANIA);
}