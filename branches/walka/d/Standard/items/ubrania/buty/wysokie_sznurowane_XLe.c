
/* Wysokie sznurowane buty
Wykonano dnia 19.05.06 przez Avarda. 
Opis by Tinardan */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");

    dodaj_przym("wysoki","wysocy");
    dodaj_przym("sznurowany","sznurowani");

    set_long("Wykonane z mi^ekkiej, ale mocnej ciel^ecej sk^ory przez "+
        "jakiego^s elfiego szewca. Si^egaj^ace kolan, wysoko sznurowane o "+
        "do^s^c grubej podeszwie i fantazyjnie wywini^etej cholewie "+
        "sprawiaj^a wra^zenie wygodnych i wytrzyma^lych. Sk^orzane "+
        "sznurowad^lo owija si^e wok^o^l cholewy, a jego wi^azanie "+
        "ukryte jest pod srebrzon^a klamr^a.\n");
		
	set_slots(A_FEET);
	add_prop(OBJ_I_VOLUME, 1000);
	add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VALUE, 75);
    add_prop(ARMOUR_S_DLA_RASY, "elf");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("XL");
	ustaw_material(MATERIALY_SK_CIELE, 90);
    ustaw_material(MATERIALY_SREBRO, 10);
	set_type(O_UBRANIA);
}
string
query_auto_load()
{
   return ::query_auto_load();
}