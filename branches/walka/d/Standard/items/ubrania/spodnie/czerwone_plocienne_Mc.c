
/*  czerwone p��cienne spodnie
Wykonano przez Avarda dnia 07.06.06
Opis by Tinardan */
/* Uzyte w:
Trubadur uliczny z Rinde (/d/Standard/Redania/Rinde/npc/trubadur_uliczny.c);
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");

    dodaj_przym("czerwony","czerwoni");
    dodaj_przym("p^l^ocienny","p^l^ocienni");

    set_long("Spodnie s^a na tyle w^askie, ^ze przylegaj^a g^ladko do "+
        "cia^la, znakomicie ukazuj^ac zar^owno jego zalety jak i "+
        "mankamenty. Do^s^c popularne w^sr^od mieszczan, cho^c "+
        "przys^luguj^a bardziej trubadurom i w^edrownym aktorom ni^z "+
        "szanuj^acym si^e, powa^znym m^e^zczyznom, ale wielu m^lodych "+
        "ludzi zdaje si^e na to nie zwa^za^c.\n");

    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 837);
    add_prop(OBJ_I_VALUE, 75);
    ustaw_material(MATERIALY_LEN, 100); /* Do zmiany, bo za cholere nie
	wiem jaka tu moze byc skora :P */
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}
string
query_auto_load()
{
   return ::query_auto_load();
}