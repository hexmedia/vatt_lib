/*
 * Ciep�e bawe�niane spodnie Rozmiar: L
 * Posiadaja dwie kieszenie, ale nie zakodowane!
 * Autor: Lil, Opis: Dinrahia (tudzie�: Aneta)
 */

inherit "/std/armour";
inherit "/std/container";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
     ustaw_nazwe("spodnie");
     ustaw_nazwe_glowna("para");
     dodaj_przym("ciep�y","ciepli");
     dodaj_przym("bawe�niany","bawe�niani");
     set_long("Ciemnozielona barwa tych spodni jest mi�a dla oka. Wykonane "+
              "zosta�y z mocnego lnu, przez co idealnie nadaj� si� dla "+
              "pracowitych os�b. Prostota ich wykonania z pewno�ci� nie "+
              "przypadnie do gustu elegantom, jednak wielu powinno doceni� "+
              "ich u�yteczno��.\n");
//Maj� dwie funkcjonalne kieszenie na wysoko�ci "+
//              "kolan.\n");
     set_slots(A_THIGHS | A_HIPS | A_STOMACH);
     add_prop(CONT_I_WEIGHT, 700);
     add_prop(CONT_I_VOLUME, 1850);
     add_prop(OBJ_I_VALUE, 33);
     ustaw_material(MATERIALY_BAWELNA);
     set_type(O_UBRANIA);
}
string
query_auto_load()
{
   return ::query_auto_load();
}

