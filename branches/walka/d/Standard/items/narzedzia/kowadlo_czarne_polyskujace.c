/*
 * Autor: Rantaur
 * Data: 23.04.2006
 *  Mlot dla krasnoluda w kuzni etapu tworzenia postaci
 *  */
inherit "/std/object";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void create_object()
{
    ustaw_nazwe("kowadlo");
    dodaj_przym("czarny", "czarni");
    dodaj_przym("po�yskuj�cy", "po�yskuj�cy");
    set_long("Kowad�o wygl�da na bardzo zadbane, za ka�dym razem gdy na�"
	    +" spojrzysz zdaje ci si�, �e odpowiada ono tobie delikatnym"
	    +" b�yskiem. Przygl�daj�c mu si� bli�ej, na pokrytej drobnym"
	    +" py�em g�adzi zauwa�asz dwie dziurki s�u��ce zapewne do"
	    +" przebijania otwor�w w wykonywanych przedmiotach. Jedyn� rzecza,"
	    +" kt�ra pogarsza wizerunek narz�dzia jest obt�uczony r�g - by�"
	    +" mo�e kowad�o jest ju� do�� stare i nie wytrzyma�o niefortunnego"
	    +" uderzenia.\n");
	add_prop(OBJ_I_VALUE, 4632); 
	add_prop(OBJ_I_WEIGHT, 70000);
	add_prop(OBJ_I_VOLUME, 11200);
	set_type(O_NARZEDZIA);
	ustaw_material(MATERIALY_STAL);
}
