inherit "/std/object";
inherit "/lib/material";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_object()
{
    ustaw_nazwe("jedwab");
    set_long("T^a niezwykle przyjemn^a i delikatn^a w dotyku tkanin^a jest "+
        "powszechnie znany jedwab. Jest do^s^c drogi przez co tylko co "+
        "bogatsi mog^a pozwoli^c sobie na kreacj^e z tego materia^lu. Jasny "+
        "kolor bez problemu mo^zna przefarbowa^c.\n");

    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 3000);

    ustaw_material(MATERIALY_JEDWAB);
    set_material_capacity(MATERIALY_JEDWAB, 10000);
}

public void
notify_change_material_amount(string material, int oldValue, int newValue)
{
    float factor = itof(newValue)/itof(query_material_capacity(material));
    if (newValue == 0) {
        TO->remove_object();
        return;
    }
    add_prop(OBJ_I_WEIGHT, ftoi(factor * 10000.0));
    add_prop(OBJ_I_VOLUME, ftoi(factor * 5000.0));
    add_prop(OBJ_I_VALUE, ftoi(factor * 1000.0));
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_material_auto_load();
}

public string
init_arg(string arg)
{
    return init_material_arg(::init_arg(arg));
}
