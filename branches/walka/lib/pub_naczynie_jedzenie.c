/* Troche przerobilam...Wlasciwie calkiem sporo :)
     m.in. komunikaty, super wypasna funkcja query_naczynie, stuknij etc.
     jak i zmienilam komende 'kesnij' na 'napij sie'
     i kilka innych mniejszych lub ciut wiekszych zmian
     typu: trzeba chwycic, aby uzywac ^_^
     Te naczynia z zalozenia sluzyc maja jedynie jako
     naczynia karczemne.
                                                 Lil  03.07.2005 */

/* Obiekt naczynia specjalnie for Dragfor by Simon wiekszosc kodu tutaj
 * pochodzi z /std/beczulka ja dodalem pare funkcji i to wszystko :D. Ale
 * generalnie opiera sie to na /std/beczulka i zamiast napij sie jest kesnij
 * oraz mozliwosc     wychylenia do dna.
 * Simon pazdziernik 2003
 */
#pragma strict_types

inherit "/std/holdable_object";
inherit "/cmd/std/command_driver.c";

#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>

int niszczenie = 0;

private	int	pojemnosc_naczynia,
		ilosc_jedzenia,
		procent_alco;
private	string	opis_jedzenia,
		nazwa_dop,
		nazwa_bie,
		dlugi_opis;
private string  nazwa_jedzenia;		
void
create_naczynie()
{

}



private void    ustaw_nazwe_jedzenia(string str);
private	int	sprobuj(string str);
private	int	wylej(string str);
private int     zjedz(string str);
private int rozbij (string str);
private int pomoc (string str);
//private void rozsyp_sie ();
void oblicz_sh_po_wypiciu();
int zle (string str);
/*
 * Nazwa funkcji : create_object
 * Opis          : Konstruktor obiektu. Ustawiona jako nomask, wywoluje
 *		   create_beczulka().
 */
public nomask void

create_holdable_object()
{
    ustaw_nazwe("naczynie");
    pojemnosc_naczynia = 300;
    ilosc_jedzenia = 0;
//    procent_alco = 0;
    opis_jedzenia = "";
    nazwa_dop = "";
    nazwa_bie = "";
    add_prop(OBJ_I_WEIGHT, "@@query_waga");
    add_prop(OBJ_I_VOLUME, "@@query_objetosc");
    add_prop(OBJ_I_VALUE, 1);
    add_prop(OBJ_M_NO_SELL, "@@query_mozna_sprzedac");
    set_long("@@long");
//    set_czy_bron(1);
     set_hit(1);
     set_pen(1);
     set_wt(W_CLUB);
     set_dt(W_BLUDGEON);
   set_likely_dull(MAXINT);
   set_likely_break(MAXINT);
   set_weapon_hits(9991000);
    create_naczynie();
    
}

 
/*
 * Nazwa funkcji : query_naczynie
 * Opis          : Zwraca 1 jesli obiekt jest naczyniem.
 * Funkcja zwraca: int 1
 */
 public int
 query_naczynie()
 {
    return 1;
 }

/*
 * Nazwa funkcji : query_waga
 * Opis          : Zwraca wage beczulki, w zaleznosci od jej pojemnosci i
 *		   ilosci plynu w srodku.
 * Funkcja zwraca: int - patrz wyzej.
 */
public int
query_waga()
{
    return ilosc_jedzenia + (pojemnosc_naczynia / 4);
}

/*
 * Nazwa funkcji : query_objetosc
 * Opis          : Zwraca objetosc beczulki, w zaleznosci od jej pojemnosci.
 * Funkcja zwraca: int - patrz wyzej.
 */
public int
query_objetosc()
{
    return 6 * pojemnosc_naczynia / 5;
}

/*
 * Nazwa funkcji : query_mozna_sprzedac
 * Opis          : Zwraca czy mozna sprzedac - w zaleznosci od tego, czy jest
 *		   pusta czy nie.
 * Funkcja zwraca: 	0 - mozna sprzedac,
 *		   string - nie mozna sprzedac, wyjasnienie dlaczego.
 */
public mixed
query_mozna_sprzedac()
{
    return "Nie mo^zesz tego sprzeda^c.";
}

public void
init()
{     
     add_action(sprobuj, "spr^obuj");
     add_action(sprobuj, "posmakuj");
     add_action(sprobuj, "skosztuj");
     add_action(wylej, "wylej");
     add_action(zjedz, "zjedz");
     add_action(rozbij, "rozbij");
//     add_action(zle, "dob^ad^x");
     add_action(pomoc, "?", 2);

//     add_action("zwrot", "zwr^o^c");  TA KOMENDA BEDZIE W PUBIE!
    ::init();
}
/*
void
enter_env(object dest, object old)
{
	if(function_exists("create_room", dest))
		niszczenie = set_alarm(300.0, 0.0, "rozsyp_sie");
	if(living(dest) && (niszczenie != 0))
		remove_alarm(niszczenie);
	::leave_env(dest, old);
}*/

/*
 * Nazwa funkcji : napij
 * Opis          : Wywolywana, gdy gracz wpisze komende 'napij'. Sprawdza
 *		   z czego gracz chce sie napic i wywoluje drink_from_it()
 *		   w odpowiedniej beczulce. Zwraca to co zwroci 
 *		   drink_from_it().
 * Argumenty     : string - argument do komendy.
 * Funkcja zwraca: W zaleznosci od rezultatu funkcji ob->drink_from_it(),
 *			1 - komenda przyjeta, lub
 *			0 - nie przyjeta, szukaj w innym obiekcie.
 */
private int
sprobuj(string str)
{
    object *obs;
    string co;
    string dd;
    int ret;
    string nazwa;

//    notify_fail("Napij si^e sk^ad?\n");
    notify_fail(capitalize(query_verb()) + " co?\n");
    if (!strlen(str))
        return 0;
   
    if (str ~= nazwa_dop)
     {
	write("Pr^obujesz "+nazwa_dop+". ");
	say(QCIMIE(TP, PL_MIA)+" pr^obuje "+nazwa_dop+".\n");
	switch(random(6, atoi(OB_NUM(TP)) + atoi(OB_NUM(this_object()))))
	{
	   case 0 : write("Smakuje ca^lkiem nie^xle.\n");break;
	   case 1 : write("Smakuje nienajgorzej.\n");break;
	   case 2 : write("Smakuje wy^smienicie.\n");break;
	   case 3 : write("Smakuje przepysznie.\n");break;
	   case 4 : write("Nie smakuje najlepiej...\n");break;
	   case 5 : write("^Srednio ci smakuje.\n");break;
	}
	   
	return 1;
     }
    return 0;

// ponizsze na razie jest niewazne
/*
    if (!parse_command(str, all_inventory(TP), 
        " %i:" + PL_DOP, obs))
        return 0;
    
    obs = NORMAL_ACCESS(obs, 0, 0);
    
    if (!sizeof(obs))
        return 0;
        
    if (sizeof(obs) > 1)
    {
        notify_fail("Mo^zesz si^e napi^c tylko z jednego naczynia naraz.\n");
        return 0;
    }

    if(!obs[0]->query_wielded())
    {
        notify_fail("Musisz wpierw chwyci^c " + koncowka("ten", "ta", "to") +
        " " + query_nazwa(PL_BIE) + ".\n");
        return 0;
    }*/
//    if(obs[0]->query_nazwa_jedzenia_dop() == nazwa_dop)
//    {
//	write("probujesz\n");
//	return 1;
//    }

  
//    if(co!=nazwa_jedzenia) return 0;
//    nazwa=obs[0]->query_nazwa(PL_DOP);

/*    if(wildmatch("s[bcdfghjklmnprstwxz]*",nazwa))
    {
        if(dd!="ze") return 0;
    }
    else
    {
        if(dd!="z") return 0;
    }*/

//    ret = obs[0]->drink_from_it(TP);
//    if (ret)
//	TP->set_obiekty_zaimkow(obs);
//    return ret;
}

/*
 * Nazwa funkcji : wylej
 * Opis          : Wywolywana, gdy gracz wpisze komende 'wylej'. Sprawdza
 *		   z czego gracz chce wylac zawartosc i wywoluje
 *		   wylej_from_it() w odpowiedniej beczulce. Zwraca to co
 *		   zwroci wylej_from_it().
 * Argumenty     : string - argument do komendy.
 * Funkcja zwraca: W zaleznosci od rezultatu funkcji ob->wylej_from_it(),
 *			1 - komenda przyjeta, lub
 *			0 - nie przyjeta, szukaj w innym obiekcie.
 */
public int
wylej(string str)
{
    object *obs;
    int ret;
    
    notify_fail("Wylej co?\n");
    
    if (!strlen(str))
        return 0;
        
    if (!parse_command(str, all_inventory(TP), 
        "'zawarto^s^c' %i:" + PL_DOP, obs))
        return 0;
    
    obs = NORMAL_ACCESS(obs, 0, 0);
    
    if (!sizeof(obs))
        return 0;
        
    if (sizeof(obs) > 1)
    {
        notify_fail("Mo^zesz wyla^c zawarto^s^c tylko jednego naczynia naraz.\n");
        return 0;
    }

    ret = obs[0]->wylej_from_it();
    if (ret)
	TP->set_obiekty_zaimkow(obs);

    return ret;
}


/*
 * Nazwa funkcji : drink_from_it
 * Opis          : Gracz pije konkretnie z tej beczulki. Pije albo nie,
 *		   wypisuje odpowiednie teksty.
 * Funkcja zwraca: 1 - komenda przyjeta.
 */
public int
drink_from_it(object player)
{
    int kes;
    string str;

    if (!ilosc_jedzenia)
    {
        write(capitalize(short()) + " jest zupe^lnie pust" + 
            koncowka("y", "a", "e") + ".\n");
        return 1;
    }
    
    kes = MIN(player->eat_max() / 20, player->eat_max() - 
    		player->query_soaked());
    if (!kes)
    {
        write("Nie wmusisz w siebie ani k^esa wi^ecej.\n");
        return 1;
    }

 //   kes = pojemnosc_naczynia / 10;
    /*if (!player->drink_alco((kes * procent_alco) / 100, 0))
    {
        write("Trunek w " + query_nazwa(PL_MIE) + " jest tak mocny, ^ze chyba " +
            "nie dasz rady go teraz wypi^c.\n");
        return 1;
    }*/
    
 /*   if (!player->drink_soft(kes, 0))
    {
         write("Nie wmusisz w siebie ani kropelki wi^ecej.\n");
         return 1;
    }
    
    ilosc_jedzenia -= kes;
    
    if (ilosc_jedzenia<=0)
    {
        str = ", opr^o^zniaj^ac " + koncowka("go", "j^a", "je") + " zupe^lnie.\n";
	ilosc_jedzenia=0;
    }
    else
        str = ".\n";*/
        
    write("Pijesz kes " + nazwa_dop + " "+ z_ze(query_nazwa(PL_DOP) + str));
    say(QCIMIE(player, PL_MIA) + " pije kes " + nazwa_dop + 
        " " + z_ze(query_nazwa(PL_DOP) + str));
        
  /*  if (ilosc_jedzenia<=0)
    {
	ilosc_jedzenia = 0;
        procent_alco = 0;
        opis_jedzenia = "";
        nazwa_dop = "";
	oblicz_sh_po_wypiciu();
    }
 */       
    return 1;
}

/*
 * Nazwa funkcji : wylej_from_it
 * Opis          : Gracz chce wylac konkretnie z tej beczulki. Wylewa albo
 *		   nie, wypisuje odpowiednie teksty.
 * Funkcja zwraca: 1 - komenda przyjeta.
 */
/*public int
wylej_from_it()
{
    if (!ilosc_jedzenia)
    {
        write(capitalize(query_nazwa(PL_MIA)) + " jest zupe^lnie pust" + 
            koncowka("y", "a", "e") + ".\n");
        return 1;
    }
    
    write("Wylewasz ca^l^a zawarto^s^c " + query_nazwa(PL_DOP) + ", w postaci " + 
        opis_jedzenia + " na ziemi^e.\n");
    say(QCIMIE(TP, PL_MIA) + " wylewa ca^l^a zawarto^s^c " +
        query_nazwa(PL_DOP) + ", w postaci " + opis_jedzenia + 
        " na ziemi^e.\n");
        
    ilosc_jedzenia = 0;
    procent_alco = 0;
    opis_jedzenia = "";
    nazwa_dop = "";
    oblicz_sh_po_wypiciu();
    return 1;
}*/

public string
long()
{
    string str;
    
    str = (dlugi_opis ? dlugi_opis : "Jest to " + query_nazwa(PL_MIA) + " slu^z^ac" + 
	koncowka("y", "a", "e") + " do przechowywania r^o^znych potraw. ") + 
	" W tej chwili ";
        
    switch(100 * ilosc_jedzenia / pojemnosc_naczynia)
    {
        case 0: str += "jest zupe^lnie pust" + koncowka("y", "a", "e"); break;
        case 1..20: str += "zawiera ma^l^a ilo^s^c " + opis_jedzenia; break;
        case 21..50: str += "zawiera troch^e " + opis_jedzenia; break;
        case 51..70: str += "zawiera sporo " + opis_jedzenia; break;
        case 71..90: str += "jest prawie pe^ln" + koncowka("y", "a", "e") +
            " " + opis_jedzenia; break;
        default: str += "jest pe^ln" + koncowka("y", "a", "e") + " " +
            opis_jedzenia; break;
    }
    
    str += ".\n";
    
    return str;
}

/*
 * Nazwa funkcji : set_dlugi_opis
 * Opis          : Ustawia dlugi opis beczulki. Do opisu ustawionego
 *		   przez te funkcje dodawany jest opisik o zawartosci
 *		   pojemnika.
 * Argumenty     : string opis - opis.
 */
public void
set_dlugi_opis(string opis)
{
    dlugi_opis = opis;
}

/*
 * Nazwa funkcji : query_dlugi_opis
 * Opis          : Zwraca dlugi opis beczulki ustawiony przez wiza.
 * Funkcja zwraca: string - dlugi opis.
 */
public string
query_dlugi_opis()
{
    return dlugi_opis;
}

/*
 * Nazwa funkcji : set_pojemnosc
 * Opis          : Sluzy do ustawiania pojemnosci beczulki w mililitrach.
 * Argumenty     : int - pojemnosc w mililitrach.
 */
public void
set_pojemnosc(int pojemnosc)
{
    pojemnosc_naczynia = pojemnosc;
}

/*
 * Nazwa funkcji : query_pojemnosc
 * Opis          : Zwraca pojemnosc beczulki w mililitrach.
 * Funkcja zwraca: int - pojemnosc w mililitrach.
 */
public int
query_pojemnosc()
{
    return pojemnosc_naczynia;
}

/*
 * Nazwa funkcji : set_ilosc_jedzenia
 * Opis          : Sluzy do ustawienia ile ml plynu znajduje sie akt w
 *		   pojemniku.
 * Argumenty     : int - liczba mililitrow plynu.
 */
public void
set_ilosc_jedzenia(int ilosc)
{
    if (ilosc > pojemnosc_naczynia)
        ilosc = pojemnosc_naczynia;
        
    ilosc_jedzenia = ilosc;
}

/*
 * Nazwa funkcji : query_ilosc_jedzenia
 * Opis          : Zwraca ile aktualnie ml. plynu znajduje sie w pojemniku.
 * Funkcja zwraca: int - patrz wyzej.
 */
public int
query_ilosc_jedzenia()
{
    return ilosc_jedzenia;
}

/*
 * Nazwa funkcji : set_nazwa_jedzenia_dop
 * Opis          : Ustawia nazwe plynu w dopelniaczu. Nazwa jest
 *		   wykorzystywana w komunikatach wysylanych graczowi,
 *		   np. przy piciu.
 * Argumenty     : string - nazwa plynu w dopelniaczu.
 */
public void
set_nazwa_jedzenia_dop(string str)
{
    nazwa_dop = str;
}

/*
 * Nazwa funkcji : query_nazwa_jedzenia_dop
 * Opis          : Zwraca nazwe plynu, ktory znajduje sie akt. w beczulce
 *		   w dopelniaczu.
 * Funkcja zwraca: string - patrz wyzej.
 */
public string
query_nazwa_jedzenia_dop()
{
    return nazwa_dop;
}

/*
 * Nazwa funkcji : set_nazwa_jedzenia_bie
 * Opis          : Ustawia nazwe plynu w bierniku. Nazwa jest
 *		   wykorzystywana w komunikatach wysylanych graczowi,
 *		   np. przy piciu.
 * Argumenty     : string - nazwa plynu w dopelniaczu.
 */
public void
set_nazwa_jedzenia_bie(string str)
{
    nazwa_bie = str;
}

/*
 * Nazwa funkcji : query_nazwa_jedzenia_bie
 * Opis          : Zwraca nazwe plynu, ktory znajduje sie akt. w beczulce
 *		   w bierniku.
 * Funkcja zwraca: string - patrz wyzej.
 */
public string
query_nazwa_jedzenia_bie()
{
    return nazwa_bie;
}

/*
 * Nazwa funkcji : set_opis_jedzenia
 * Opis          : Sluzy do ustawienia dlugiego opisu plynu znajdujacego
 *		   sie akt. w beczulce. Powinien on byc w dopelniaczu.
 *		   Wykorzystywany jest np. w dlugim opisie beczulki.
 * Argumenty     : string - dlugi opis w dopelaniczu.
 */
public void
set_opis_jedzenia(string opis)
{
    opis_jedzenia = opis;
}

/*
 * Nazwa funkcji : query_opis_jedzenia
 * Opis          : Zwraca dlugi opis plynu znajdujacego sie akt. w beczulce,
 *		   w dopelniaczu.
 * Funkcja zwraca: string - patrz wyzej.
 */
public string
query_opis_jedzenia()
{
    return opis_jedzenia;
}

/*
 * Nazwa funkcji : set_vol
 * Opis          : Ustawia ile procent alkoholu jest w cieczy, ktora
 *		   jest w beczulce.
 * Argumenty     : int - procent alkoholu
 */
public void
set_vol(int vol)
{
    procent_alco = vol;
}

/*
 * Nazwa funkcji : query_vol
 * Opis          : Zwraca jak bardzo procentowy jak napoj w beczulce.
 * Funkcja zwraca: int - ile procent alkoholu jest w plynie w beczulce.
 */
public int
query_vol()
{
    return procent_alco;
}
/*
 * Nazwa funkcji : ustaw_nazwe_jedzenia
 * Opis          : Ustawia nazwe plynu ktora nalezy podac przy kesaniu,np.
 *		   kesnij wodki z kieliszka(w dopelniaczu nalezy to podac)
 * Argumenty     : string - nazwa w dopelniaczu.
 */
public void    
ustaw_nazwe_jedzenia(string str)
{
nazwa_jedzenia=str;
}

private int     
zjedz(string str)
{
    object *obs;
    int ret;
    
    notify_fail("Zjedz co?\n");
    
    if (!strlen(str))
        return 0;
    
    if (str ~= nazwa_bie) {
	return this_object()->zjedz_to(TP);
    }
    return 0;

// ponizsze niewazne :P
/*
    if (!parse_command(str, all_inventory(TP), 
        "%i:" +PL_BIE,obs))
        return 0;
    obs = NORMAL_ACCESS(obs, 0, 0);
    
    if (!sizeof(obs))
        return 0;
        
    if (sizeof(obs) > 1)
    {
        notify_fail("Mo^zesz je^s^c tylko z jednego naczynia jednocze^snie.\n");
        return 0;
    }
    
    if(!obs[0]->query_wielded())
    {
        notify_fail("Musisz wpierw chwyci^c " + koncowka("ten", "ta", "to") +
        " " + query_nazwa(PL_BIE) + ".\n");
        return 0;
    }

    ret = obs[0]->zjedz_to(TP);
    if (ret)
	TP->set_obiekty_zaimkow(obs);
    return ret;*/
}

public int
zjedz_to()
{

    string str;
    int kes;
    if (!ilosc_jedzenia)
    {
        write(capitalize(query_nazwa(PL_MIA)) + " jest zupe^lnie pust" + 
            koncowka("y", "a", "e") + ".\n");
        return 1;
    }
    
    kes = MIN(TP->drink_max() / 20, TP->drink_max() - 
    		TP->query_soaked());
    if (!kes)
    {
        write("Nie wmusisz w siebie ani odrobiny wi^ecej.\n");
        return 1;
    }

    //kes = pojemnosc_naczynia; WTF? A NIE ILOSC zarcia?
    kes=ilosc_jedzenia;
/*    if (!TP->drink_alco((kes * procent_alco) / 100, 0))
    {
        write("Trunek w " + query_nazwa(PL_MIE) + " jest taki mocny, ^ze chyba " +
            "nie dasz rady go teraz wypi^c.\n");
        return 1;
    }*/
    
    if (!TP->eat_food(kes, 1))//jesli nie daje rady w calosci zjesc
    {   //Vera
         if(TP->query_stuffed()>=(TP->query_prop(LIVE_I_MAX_EAT)-100))
         {
             write("Nie wmusisz w siebie ani odrobiny wi^ecej.\n");
             return 1;
         }
         else
         /* if((TP->query_stuffed()+ilosc_jedzenia)>
                 TP->query_prop(LIVE_I_MAX_EAT))*/
         {
             string str_ile;
             int roznica;
             roznica=(TP->query_prop(LIVE_I_MAX_EAT))-(TP->query_stuffed());
             ilosc_jedzenia-=roznica;
             
             /*if((TP->query_stuffed()+ilosc_jedzenia)<
                 TP->query_prop(LIVE_I_MAX_EAT))*/
                 
             switch(100 * ilosc_jedzenia / pojemnosc_naczynia)
             {
                 case 0: /*str_ile="prawie wszystko"; 
                         write("Jejku jej! Zg�o� b��d w jedzeniu!\n"); break;*/
                             str_ile="wi�kszo��"; break;
                 case 1..20: str_ile="wi�kszo��"; break;
                 case 21..50: str_ile="sporo"; break;
                 case 51..70: str_ile="troch�"; break;
                 case 71..90: str_ile="ma�� ilo��"; break;
                 default: str_ile="pewn� ilo��"; break;
                         /*str_ile="prawie nic"; 
                         write("Jejku jej! Zg�o� b��d w jedzeniu!\n");
                         break;*/
             }
             //TP->set_stuffed(TP->query_prop(LIVE_I_MAX_EAT));
             TP->set_stuffed(TP->query_stuffed()+roznica);
             write("Zjadasz "+str_ile+" "+nazwa_dop+ ", pozostawiaj�c "+
                   "reszt�.\n");
             saybb(QCIMIE(TP, PL_MIA) + " zjada "+str_ile+" "+ 
             short(PL_DOP) + ", pozostawiaj�c reszt�.\n");
             if(ilosc_jedzenia<=0) // just in case ;)
             {
                 ilosc_jedzenia=0;
                 opis_jedzenia = "";
                 nazwa_dop = "";
	         nazwa_bie = "";
	         oblicz_sh_po_wypiciu();
             }
             
             return 1;
         }
         //else
         //{
             /*write("Jejku, jej! Wyst�pi� niespodziewany b��d w jedzeniu! "+
                   "Zg�o� to natychmiast opisuj�c wszystkie "+
                   "okoliczno�ci w jakich do tego dosz�o!\n");*/
             write("Nie wmusisz w siebie ani odrobiny wi^ecej.\n");
             return 1;
         //}
    }
    
    ilosc_jedzenia -= kes;
    
    if (ilosc_jedzenia<=0)
    {
        str = ", opr^o^zniaj^ac " + koncowka("go", "j^a", "je") + " zupe^lnie.\n";
	ilosc_jedzenia=0;
    }
   
    TP->eat_food(kes, 0);
        
    write("Zjadasz " +short(PL_BIE)+ str);
    saybb(QCIMIE(TP, PL_MIA) + " zjada " + 
    short(PL_BIE) + str);
        
    if (ilosc_jedzenia<=0)
    {
	ilosc_jedzenia = 0;
//        procent_alco = 0;
        opis_jedzenia = "";
        nazwa_dop = "";
	nazwa_bie = "";
	oblicz_sh_po_wypiciu();
    }
        
    return 1;
}

void
oblicz_sh_po_wypiciu()
{
string *kon=({});
string *kon2=({});
string *lp=({});
string *lm=({});
int i;
kon+=({koncowka("y","a","e")});
kon+=({koncowka("ego","ej","ego")});
kon+=({koncowka("emu","ej","emu")});
kon+=({koncowka("y","a","e")});
kon+=({koncowka("ym","a","ym")});
kon+=({koncowka("ym","ej","ym")});

kon2+=({koncowka("e","e","e")});
kon2+=({koncowka("ych","ych","ych")});
kon2+=({koncowka("ym","ym","ym")});
kon2+=({koncowka("e","e","e")});
kon2+=({koncowka("ymi","ymi","ymi")});
kon2+=({koncowka("ymi","ymi","ymi")});
for(i=0;i<6;i++)
{
    lp+=({"pust"+kon[i]+" "+query_nazwa(i)});
    dodaj_przym("pusty","pu�ci"); //chyba dobrze, co?
}
for(i=0;i<6;i++)
{
    lm+=({"pust"+kon2[i]+" "+query_pnazwa(i)});
    dodaj_przym("pusty","pu�ci"); //chyba dobrze, co?
}
ustaw_shorty(lp,lm);
}

int
rozbij (string str)
{
	notify_fail("Rozbij co?\n");
	if(!(str ~= query_nazwa(PL_BIE)))
		return 0;
	  
	unwield_me();
	if (!ilosc_jedzenia)
	{
	write("Ciskasz " + query_nazwa(PL_NAR) +
	      " o ziemi^e rozbijaj^ac " + koncowka("go", "j^a", "je") + " na kawa^lki.\n");
	say(QCIMIE(TP, PL_MIA) + " ciska " +
            query_nazwa(PL_NAR) + " o ziemi^e rozbijaj^ac " + koncowka("go", "j^a", "je") +
	     " na kawa^lki.\n");
	}
	else
	{
	write("Ciskasz " + query_nazwa(PL_NAR) + " o ziemi^e rozbijaj^ac " +
	      koncowka("go", "j^a", "je") + " na kawa^lki, a przy okazji widowiskowo ^swini^ac "+
	       "pod^log^e resztkami "+nazwa_dop+".\n");
	say(QCIMIE(TP, PL_MIA) + " ciska " + query_nazwa(PL_NAR) +
	    " o ziemi^e rozbijaj^ac " + koncowka("go", "j^a", "je") + " na kawa^lki, a przy "+
	    "okazji widowiskowo ^swini^ac pod^log^e resztkami "+nazwa_dop+".\n");
	}
	remove_object();
	
	return 1;
}

int
zle (string str)
{
    write("Najpierw " + koncowka("go", "j^a", "je") + " chwy^c!\n");
    return 1;
}


public int
pomoc(string str)
{
    object nacz;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) return 0;

    if (!parse_command(str, TP, "%o:" + PL_MIA, nacz))
	return 0;

    if (nacz != this_object())
	return 0;

    write("Dla "+query_nazwa(PL_DOP)+" dost�pne s� komendy:\n"+
            "rozbij, spr^obuj, zjedz.\n");
	    return 1;
} 

public int
is_naczynie_jedzenie()
{
    return 1;
}

