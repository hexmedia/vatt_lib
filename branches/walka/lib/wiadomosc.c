inherit "/std/object";

#include <pl.h>
#include <macros.h>
#include <stdproperties.h>

string message;

void
create_object()
{
    ustaw_nazwe( ({"karteczka","karteczki","karteczce","karteczk^e",
	"karteczk^a","karteczce"}), ({"karteczki","karteczek","karteczkom",
	"karteczki","karteczkami","karteczkach"}), PL_ZENSKI);

    dodaj_nazwy( ({"wiadomo^s^c","wiadomo^sci","wiadomo^sci","wiadomo^s^c",
	"wiadomo^sci^a","wiadomo^sci"}), ({"wiadomo^sci","wiadomo^sci",
	"wiadomo^sciom","wiadomo^sci","wiadomo^sciami","wiadomo^sciach"}),
	PL_MESKI_NZYW);

    set_long("Niewielka karteczka. Jest na niej co^s napisane.\n");

    add_prop(OBJ_I_WEIGHT, 1);
    add_prop(OBJ_I_VOLUME, 10);
}

void
init()
{
    ::init();
    add_action("przeczytaj", "przeczytaj");
    add_action("zniszcz", "zniszcz");
}

int
przeczytaj(string str)
{
    object kartka;

    notify_fail("Przeczytaj co?\n");

    if (!str || !parse_command(str, this_player(), "%o:" + PL_BIE, kartka) || kartka != this_object())
	return 0;

    this_player()->more("Czytasz zawarto^s^c karteczki:\n" + message);
    return 1;
}

int
zniszcz(string str)
{
    object kartka;

    notify_fail("Zniszcz co?\n");

    if (!str || !parse_command(str, this_player(), "%o:" + PL_BIE, kartka) || kartka != this_object())
	return 0;

    write("Rwiesz wiadomo�� na strz�py.\n");
    saybb(QCIMIE(TP,PL_MIA)+" rwie wiadomo�� na strz�py.\n");
    destruct();
    return 1;
}

public void
set_message(string m)
{
    message = m;
}

public string
query_message()
{
    return message;
}

string
query_wiad_auto_load()
{
    return "#/LIB/WIADOMOSC#" + query_message() + "#/LIB/WIADOMOSC#";
}

public void
init_wiad_arg(string arg)
{
    string reszt;
    string wiad;
    
    sscanf(arg, "%s#/LIB/WIADOMOSC#%s#/LIB/WIADOMOSC#%s", reszt, wiad, reszt);
    
    set_message(wiad);
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_wiad_auto_load();
}

public void
init_arg(string arg)
{
    ::init_arg(arg);
    init_wiad_arg(arg);
}
