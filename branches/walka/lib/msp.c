/**
 * \file /lib/msp.c
 *
 * Plik z funkcjami wspomagaj�cymi protok� msp
 * 
 */

/**
 * Funkcja wysy�a dzwi�k do danego gracza.
 * 
 * @param ob - gracz
 * @param name - nazwa pliku
 * 
 * @return 1/0 - wys�ano/nie wys�ano (msp jest wy��czone)
 */

int
send_dzwiek(object ob, string name)
{
  if (!objectp(ob) || !interactive(ob) || !(efun::query_msp(ob)))
  {
    return 0;
  }
  tell_object(ob, "\n!!SOUND(" + name + ")\n");
  return 1;
}

/**
 * Funkcja wysy�a muzyk� do danego gracza.
 * 
 * @param ob - gracz
 * @param name - nazwa pliku
 * 
 * @return 1/0 - wys�ano/nie wys�ano (msp jest wy��czone)
 */

int
send_muzyka(object ob, string name)
{
  if (!objectp(ob) || !interactive(ob) || !(efun::query_msp(ob)))
  {
    return 0;
  }
  tell_object(ob, "\n!!MUSIC(" + name + "\n");
  return 1;
}
