/*
 *
 * Biblioteka zawierajaca funkcje siedzenia.
 *
 * Dzielo Cerdina 2003
 *
 * Kod powinien byc prosty i zrozumialy. Przynajmniej
 * mi sie tak wydaje :) 
 */

#include <composite.h>
#include <filter_funs.h>
#include <macros.h>
#include <composite.h>
#include "/sys/siedzenie.h"
#include "/sys/stdproperties.h"

/* Zmienne */
static mixed *siedzenia = ({({""}),({""}),({""}),({0})});
static int ktore;

int
czy_siedza(object kto)
{
    if ((kto->query_prop(SIT_SIEDZE)) == ktore)
    return 1;

    return 0;
}


string
sit_nf()
{
    int i, sz;
    string s;

    s = "Gdzie chcesz usiasc? Moze ";
    sz = sizeof(siedzenia[0]);

    for(i = 1; i < sz; i++)
    {
        if(i == 1)
                s += siedzenia[0][i];
        else if(i = sz-1)
                s += " albo " + siedzenia[0][i];
        else
                s += ", " + siedzenia[0][i];
    }
    return  s + "?\n";
}

string
sit_juz_siedzi(int gdzie)
{
int gdzie_siedzi;

gdzie_siedzi = this_player()->query_prop(SIT_SIEDZE);
if (gdzie == gdzie_siedzi)
    {
    return "Przeciez juz siedzisz "+siedzenia[1][gdzie]+"!\n";
    }
return "Alez ty juz siedzisz "+siedzenia[1][gdzie_siedzi]+" i "+
       "sadzisz, ze rozdwojenie sie by usiasc "+siedzenia[1][gdzie]+
       " mogloby byc odrobine klopotliwe w tym przypadku...\n";
}

string
sit_brak_miejsc(int gdzie)
{
    return "Chyba juz sie nie zmiescisz "+siedzenia[1][gdzie]+"...\n";
}

int
siadam(string str)
{
int sit_nr;

    if (sizeof(siedzenia[0]) == 1) 
    {
	notify_fail("Nie ma tu gdzie usiasc.\n");
	return 0;
    }

    if (!str || (sit_nr = member_array(str,siedzenia[0])) == -1)
    {
        notify_fail(sit_nf());
        return 0;
    }

    if (this_player()->query_prop(SIT_SIEDZE))
    {
        notify_fail(sit_juz_siedzi(sit_nr));
        return 0;
    }
    
    ktore = sit_nr;
    if (siedzenia[3][sit_nr] != 0 &&
    sizeof(filter(FILTER_LIVE(all_inventory(this_object())),czy_siedza))
    >= siedzenia[3][sit_nr])
    {
        notify_fail(sit_brak_miejsc(sit_nr));
        return 0;
    }

write("Siadasz "+siedzenia[1][sit_nr]+".\n");
saybb(QCIMIE(this_player(),PL_MIA) + " siada " + siedzenia[1][sit_nr] + ".\n");
this_player()->add_prop(SIT_SIEDZE,sit_nr);
this_player()->add_prop(OBJ_I_DONT_GLANCE,1);
return 1;
}

string
sit_nie_siedze()
{
return "Przeciez nigdzie nie siedzisz!\n";
}

int
wstaje(string str)
{
int sit_nr;
mixed nwst;

    if (sizeof(siedzenia[0]) == 1) 
        {
        notify_fail(sit_nie_siedze());
        return 0;
        }


    if (!(sit_nr = this_player()->query_prop(SIT_SIEDZE)))
        {
        notify_fail(sit_nie_siedze());
        return 0;
        }

if (nwst = this_player()->query_prop(SIT_NIE_WSTANE))
    {
    if (stringp(nwst))
	write(nwst);
    else
	write("Nie mozesz wstac w tej chwili.\n");
    return 1;
    }

write("Wstajesz "+siedzenia[2][sit_nr]+".\n");
saybb(QCIMIE(this_player(),PL_MIA) + " wstaje "+siedzenia[2][sit_nr]+".\n");
this_player()->remove_prop(SIT_SIEDZE);
this_player()->remove_prop(OBJ_I_DONT_GLANCE);
return 1;
}

#define ZABRONIONE ({\
			"podskocz",\
			"zatancz",\
			"tupnij",\
		   })

int
sit_blok(string str)
{
int size, index;
mixed wyjscia;
string *wyj = ({});

if (!this_player()->query_prop(SIT_SIEDZE)) return 0;

wyjscia = this_object()->query_exit();
index = -2;
size = sizeof(wyjscia);

    while ((index += 3) < size)
    {
        if (pointerp(wyjscia[index]))
            wyj += ({ wyjscia[index][0] });
        else
            wyj += ({ wyjscia[index] });
    }

wyj -= ({});

if (this_player()->query_prop(SIT_SIEDZE))
    {
if (member_array(query_verb(),wyj) > -1)
    {
    write("Nie mozesz sie tam udac gdyz siedzisz.\n");
    return 1;
    }
if (member_array(query_verb(),ZABRONIONE) > -1)
    {
    write("Nie mozesz tego uczynic gdyz siedzisz.\n");
    return 1;
    }
    }
}

void
init_sit()
{
    add_action(siadam, "usiadz");
    add_action(wstaje,"wstan");
    add_action(sit_blok,"",1);
}

void
set_sit(string sit_gdzie, string sit_gdzie_l, string sit_skad, int ile_miejsc = 0)
{
siedzenia[0] += ({ sit_gdzie });
siedzenia[1] += ({ sit_gdzie_l });
siedzenia[2] += ({ sit_skad });
siedzenia[3] += ({ ile_miejsc });
}

int
czy_tu_sie_da_siedziec()
{
    return 1;
}

string
ilu_ich(object *kto)
{
if (sizeof(kto) == 2) return " siedzi ";
return " siedza ";
}

void
sit_wyswietl(object *kto, int gdzie)
{

if (!kto || !sizeof(kto)) return;

if (sizeof(kto) == 1)
    {
    if (this_player()->query_prop(SIT_SIEDZE) == gdzie)
        {
        write("Siedzisz sam "+siedzenia[1][gdzie]+".\n");
        }
    else
        {
        write(capitalize(siedzenia[1][gdzie])+" siedzi samotnie "+
        COMPOSITE_FILE->desc_live(kto, PL_MIA, 1)+".\n");
        }
    }
else
    {
    if (this_player()->query_prop(SIT_SIEDZE) == gdzie)
        {
        write("Oprocz ciebie "+siedzenia[1][gdzie]+ilu_ich(kto)+
        COMPOSITE_FILE->desc_live(kto - ({ this_player() }), PL_MIA, 1)+".\n");
        }
    else
        {
        write(capitalize(siedzenia[1][gdzie])+" siedza "+
        COMPOSITE_FILE->desc_live(kto, PL_MIA, 1)+".\n");
        }
    }
}

void
pokaz_dodatkowo()
{
string show;
int sit_nr, sit_a,sit_b;
object *sit_o, *lv;
string item;

lv = FILTER_LIVE(all_inventory(this_object()));
sit_a = sizeof(siedzenia[0]);

while (sit_a > 1)
    {
    ktore = sit_a-1;
    sit_o = filter(lv,czy_siedza);
    sit_wyswietl(sit_o,ktore);
    lv -= sit_o;
    sit_a--;
    }
}

void
leave_inv_sit(object ob)
{
    if (ob->query_prop(SIT_SIEDZE))
	{
        ob->remove_prop(SIT_SIEDZE);
        ob->remove_prop(OBJ_I_DONT_GLANCE);
	}
}

void 
remove_sit(string sit_gdzie)
{
object *sit_o, *lv;
string item;
int sit_a,i;
lv = FILTER_LIVE(all_inventory(this_object()));
sit_a = member_array(sit_gdzie,siedzenia[0]);
if(sit_a != -1)
{
    ktore = sit_a;
    sit_o = filter(lv,czy_siedza);
    for(i=0;i<sizeof(sit_o);i++)
    {
    set_this_player(sit_o[i]);
    sit_o[i]->command("$wstan");
    }
    for(i=0;i<4;i++)
      siedzenia[i] = exclude_array(siedzenia[i],ktore,ktore);
}
}
