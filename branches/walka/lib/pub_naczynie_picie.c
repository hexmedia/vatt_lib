/* Troche przerobilam...Wlasciwie calkiem sporo :)
     m.in. komunikaty, super wypasna funkcja query_naczynie, stuknij etc.
     jak i zmienilam komende 'lyknij' na 'napij sie'
     i kilka innych mniejszych lub ciut wiekszych zmian
     typu: trzeba chwycic, aby uzywac ^_^
     Te naczynia z zalozenia sluzyc maja jedynie jako
     naczynia karczemne.
                                                 Lil  03.07.2005 */

/* Obiekt naczynia specjalnie for Dragfor by Simon wiekszosc kodu tutaj
 * pochodzi z /std/beczulka ja dodalem pare funkcji i to wszystko :D. Ale
 * generalnie opiera sie to na /std/beczulka i zamiast napij sie jest lyknij
 * oraz mozliwosc wychylenia do dna.
 * Simon pazdziernik 2003
 */
#pragma strict_types
inherit "/std/holdable_object";
inherit "/cmd/std/command_driver.c";

#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>

int niszczenie = 0;

private	int	pojemnosc_naczynia,
		ilosc_plynu,
		procent_alco;
private	string	opis_plynu,
		nazwa_dop,
		dlugi_opis;
private string  nazwa_plynu;		
void
create_naczynie()
{

}



private void    ustaw_nazwe_plynu(string str);
private	int	napij(string str);
private	int	wylej(string str);
private int     wychyl(string str);
private int rozbij (string str);
private int stuknij (string str);
private int pomoc (string str);
//private void rozsyp_sie ();
void oblicz_sh_po_wypiciu();
int zle (string str);
/*
 * Nazwa funkcji : create_object
 * Opis          : Konstruktor obiektu. Ustawiona jako nomask, wywoluje
 *		   create_beczulka().
 */
public nomask void

create_holdable_object()
{
    ustaw_nazwe("naczynie");
    pojemnosc_naczynia = 200;
    ilosc_plynu = 0;
    procent_alco = 0;
    opis_plynu = "";
    nazwa_dop = "";
    add_prop(OBJ_I_WEIGHT, "@@query_waga");
    add_prop(OBJ_I_VOLUME, "@@query_objetosc");
    add_prop(OBJ_I_VALUE, 1);
    add_prop(OBJ_M_NO_SELL, "@@query_mozna_sprzedac");
    set_long("@@long");
//    set_czy_bron(1);
     set_hit(1);
     set_pen(1);
     set_wt(W_CLUB);
     set_dt(W_BLUDGEON);
   set_likely_dull(MAXINT);
   set_likely_break(MAXINT);
   set_weapon_hits(9991000);
    create_naczynie();
    
}

 
/*
 * Nazwa funkcji : query_naczynie
 * Opis          : Zwraca 1 jesli obiekt jest naczyniem.
 * Funkcja zwraca: int 1
 */
 public int
 query_naczynie()
 {
    return 1;
 }

/*
 * Nazwa funkcji : query_waga
 * Opis          : Zwraca wage beczulki, w zaleznosci od jej pojemnosci i
 *		   ilosci plynu w srodku.
 * Funkcja zwraca: int - patrz wyzej.
 */
public int
query_waga()
{
    return ilosc_plynu + (pojemnosc_naczynia / 4);
}

/*
 * Nazwa funkcji : query_objetosc
 * Opis          : Zwraca objetosc beczulki, w zaleznosci od jej pojemnosci.
 * Funkcja zwraca: int - patrz wyzej.
 */
public int
query_objetosc()
{
    return 6 * pojemnosc_naczynia / 5;
}

/*
 * Nazwa funkcji : query_mozna_sprzedac
 * Opis          : Zwraca czy mozna sprzedac - w zaleznosci od tego, czy jest
 *		   pusta czy nie.
 * Funkcja zwraca: 	0 - mozna sprzedac,
 *		   string - nie mozna sprzedac, wyjasnienie dlaczego.
 */
public mixed
query_mozna_sprzedac()
{
    return "Nie mo^zesz tego sprzeda^c.\n";
}

public void
init()
{     
     add_action(napij, "napij");
     add_action(wylej, "wylej");
     add_action(wychyl, "wychyl");
     add_action(rozbij, "rozbij");
//     add_action(zle, "dob^ad^x");
     add_action("toast", "wznie^s");
     add_action(stuknij, "stuknij");
     add_action(pomoc, "?", 2);

//     add_action("zwrot", "zwr^o^c");
    ::init();
}
/*
void
enter_env(object dest, object old)
{
	if(function_exists("create_room", dest))
		niszczenie = set_alarm(300.0, 0.0, "rozsyp_sie");
	if(living(dest) && (niszczenie != 0))
		remove_alarm(niszczenie);
	::leave_env(dest, old);
}*/

/*
 * Nazwa funkcji : napij
 * Opis          : Wywolywana, gdy gracz wpisze komende 'napij'. Sprawdza
 *		   z czego gracz chce sie napic i wywoluje drink_from_it()
 *		   w odpowiedniej beczulce. Zwraca to co zwroci 
 *		   drink_from_it().
 * Argumenty     : string - argument do komendy.
 * Funkcja zwraca: W zaleznosci od rezultatu funkcji ob->drink_from_it(),
 *			1 - komenda przyjeta, lub
 *			0 - nie przyjeta, szukaj w innym obiekcie.
 */
private int
napij(string str)
{
    object *obs;
    string co;
    string dd;
    int ret;
    string nazwa;

    notify_fail("Napij si^e sk^ad?\n");
    
    if (!strlen(str))
        return 0;
   

    if (!parse_command(str, all_inventory(this_player()), 
        " 'si^e' 'z' / 'ze' %i:" + PL_DOP, obs))
        return 0;
    
    obs = NORMAL_ACCESS(obs, 0, 0);
    obs = filter(obs, &->is_naczynie_picie());
    
    if (!sizeof(obs))
        return 0;
        
    if (sizeof(obs) > 1)
    {
        notify_fail("Mo^zesz si^e napi^c tylko z jednego naczynia naraz.\n");
        return 0;
    }

    if(obs[0]->query_prop(OBJ_I_BROKEN))
    {
        notify_fail("Nie bardzo wiesz jak to zrobi^c...\n");
        return 0;
    }

    if(!obs[0]->query_wielded())
    {
        notify_fail("Musisz wpierw chwyci^c " + koncowka("ten", "t^a", "to") +
        " " + query_nazwa(PL_BIE) + ".\n");
        return 0;
    }
  


  
//    if(co!=nazwa_plynu) return 0;
    nazwa=obs[0]->query_nazwa(PL_DOP);

/*    if(wildmatch("s[bcdfghjklmnprstwxz]*",nazwa))
    {
        if(dd!="ze") return 0;
    }
    else
    {
        if(dd!="z") return 0;
    }*/

    ret = obs[0]->drink_from_it(this_player());
    if (ret)
	this_player()->set_obiekty_zaimkow(obs);
    return ret;
}

/*
 * Nazwa funkcji : wylej
 * Opis          : Wywolywana, gdy gracz wpisze komende 'wylej'. Sprawdza
 *		   z czego gracz chce wylac zawartosc i wywoluje
 *		   wylej_from_it() w odpowiedniej beczulce. Zwraca to co
 *		   zwroci wylej_from_it().
 * Argumenty     : string - argument do komendy.
 * Funkcja zwraca: W zaleznosci od rezultatu funkcji ob->wylej_from_it(),
 *			1 - komenda przyjeta, lub
 *			0 - nie przyjeta, szukaj w innym obiekcie.
 */
public int
wylej(string str)
{
    object *obs;
    int ret;
    
    notify_fail("Wylej zawarto^s^c czego?\n");
    
    if (!strlen(str))
        return 0;
        
    if (!parse_command(str, all_inventory(this_player()), 
        "'zawarto^s^c' %i:" + PL_DOP, obs))
        return 0;
    
    obs = NORMAL_ACCESS(obs, 0, 0);
    
    if (!sizeof(obs))
        return 0;
        
    if (sizeof(obs) > 1)
    {
        notify_fail("Mo^zesz wyla^c zawarto^s^c tylko jednego naczynia naraz.\n");
        return 0;
    }

    if(obs[0]->query_prop(OBJ_I_BROKEN))
    {
        notify_fail("Nie bardzo wiesz jak to zrobi^c...\n");
        return 0;
    }

    ret = obs[0]->wylej_from_it();
    if (ret)
	this_player()->set_obiekty_zaimkow(obs);

    return ret;
}


/*
 * Nazwa funkcji : drink_from_it
 * Opis          : Gracz pije konkretnie z tej beczulki. Pije albo nie,
 *		   wypisuje odpowiednie teksty.
 * Funkcja zwraca: 1 - komenda przyjeta.
 */
public int
drink_from_it(object player)
{
    int lyk;
    string str;

    if (!ilosc_plynu)
    {
        write(capitalize(short()) + " jest zupe^lnie pust" + 
            koncowka("y", "a", "e") + ".\n");
        return 1;
    }
    
    lyk = MIN(player->drink_max() / 20, player->drink_max() - 
    		player->query_soaked());
    if (!lyk)
    {
        write("Nie wmusisz w siebie ani ^lyka wi^ecej.\n");
        return 1;
    }

    lyk = pojemnosc_naczynia / 10;

    if (!player->drink_soft(lyk, 1))
    {
         write("Nie wmusisz w siebie ani ^lyka wi^ecej.\n");
         return 1;
    }

    if (!player->drink_alco((lyk * procent_alco) / 100, 0))
    {
        write("Trunek w " + query_nazwa(PL_MIE) + " jest tak mocny, ^ze chyba " +
            "nie dasz rady go teraz wypi^c.\n");
        return 1;
    }
    
	player->drink_soft(lyk, 0);

    ilosc_plynu -= lyk;
    
    if (ilosc_plynu<=0)
    {
        str = ", opr^o^zniaj^ac " + koncowka("go", "j^a", "je") + " zupe^lnie.\n";
	ilosc_plynu=0;
    }
    else
        str = ".\n";
        
    write("Pijesz ^lyk " + nazwa_dop + " "+ z_ze(query_nazwa(PL_DOP) + str));
    say(QCIMIE(player, PL_MIA) + " pije ^lyk " + nazwa_dop + 
        " " + z_ze(query_nazwa(PL_DOP) + str));
        
    if (ilosc_plynu<=0)
    {
	ilosc_plynu = 0;
        procent_alco = 0;
        opis_plynu = "";
        nazwa_dop = "";
	oblicz_sh_po_wypiciu();
    }
        
    return 1;
}

/*
 * Nazwa funkcji : wylej_from_it
 * Opis          : Gracz chce wylac konkretnie z tej beczulki. Wylewa albo
 *		   nie, wypisuje odpowiednie teksty.
 * Funkcja zwraca: 1 - komenda przyjeta.
 */
public int
wylej_from_it()
{
    if (!ilosc_plynu)
    {
        write(capitalize(query_nazwa(PL_MIA)) + " jest zupe^lnie pust" + 
            koncowka("y", "a", "e") + ".\n");
        return 1;
    }
    
    write("Wylewasz ca^l^a zawarto^s^c " + query_nazwa(PL_DOP) + ", w postaci " + 
        opis_plynu + " na ziemi^e.\n");
    say(QCIMIE(this_player(), PL_MIA) + " wylewa ca^l^a zawarto^s^c " +
        query_nazwa(PL_DOP) + ", w postaci " + opis_plynu + 
        " na ziemi^e.\n");
        
    ilosc_plynu = 0;
    procent_alco = 0;
    opis_plynu = "";
    nazwa_dop = "";
    oblicz_sh_po_wypiciu();
    return 1;
}

public string
long()
{
    string str;
    
    str = (dlugi_opis ? dlugi_opis : "Jest to " + query_nazwa(PL_MIA) + " s�u��c" + 
	koncowka("y", "a", "e") + " do przechowywania w " + 
	koncowka("nim", "niej") + " r^o^znych trunk^ow. ") + 
	" W tej chwili ";
        
    switch(100 * ilosc_plynu / pojemnosc_naczynia)
    {
        case 0: str += "jest zupe^lnie pust" + koncowka("y", "a", "e"); break;
        case 1..20: str += "zawiera ma^l^a ilo^s^c " + opis_plynu; break;
        case 21..50: str += "zawiera troch^e " + opis_plynu; break;
        case 51..70: str += "zawiera sporo " + opis_plynu; break;
        case 71..90: str += "jest prawie pe^ln" + koncowka("y", "a", "e") +
            " " + opis_plynu; break;
        default: str += "jest pe^ln" + koncowka("y", "a", "e") + " " +
            opis_plynu; break;
    }
    
    str += ".\n";
    
    return str;
}

/*
 * Nazwa funkcji : set_dlugi_opis
 * Opis          : Ustawia dlugi opis beczulki. Do opisu ustawionego
 *		   przez te funkcje dodawany jest opisik o zawartosci
 *		   pojemnika.
 * Argumenty     : string opis - opis.
 */
public void
set_dlugi_opis(string opis)
{
    dlugi_opis = opis;
}

/*
 * Nazwa funkcji : query_dlugi_opis
 * Opis          : Zwraca dlugi opis beczulki ustawiony przez wiza.
 * Funkcja zwraca: string - dlugi opis.
 */
public string
query_dlugi_opis()
{
    return dlugi_opis;
}

/*
 * Nazwa funkcji : set_pojemnosc
 * Opis          : Sluzy do ustawiania pojemnosci beczulki w mililitrach.
 * Argumenty     : int - pojemnosc w mililitrach.
 */
public void
set_pojemnosc(int pojemnosc)
{
    pojemnosc_naczynia = pojemnosc;
}

/*
 * Nazwa funkcji : query_pojemnosc
 * Opis          : Zwraca pojemnosc beczulki w mililitrach.
 * Funkcja zwraca: int - pojemnosc w mililitrach.
 */
public int
query_pojemnosc()
{
    return pojemnosc_naczynia;
}

/*
 * Nazwa funkcji : set_ilosc_plynu
 * Opis          : Sluzy do ustawienia ile ml plynu znajduje sie akt w
 *		   pojemniku.
 * Argumenty     : int - liczba mililitrow plynu.
 */
public void
set_ilosc_plynu(int ilosc)
{
    if (ilosc > pojemnosc_naczynia)
        ilosc = pojemnosc_naczynia;
        
    ilosc_plynu = ilosc;
}

/*
 * Nazwa funkcji : query_ilosc_plynu
 * Opis          : Zwraca ile aktualnie ml. plynu znajduje sie w pojemniku.
 * Funkcja zwraca: int - patrz wyzej.
 */
public int
query_ilosc_plynu()
{
    return ilosc_plynu;
}

/*
 * Nazwa funkcji : set_nazwa_plynu_dop
 * Opis          : Ustawia nazwe plynu w dopelniaczu. Nazwa jest
 *		   wykorzystywana w komunikatach wysylanych graczowi,
 *		   np. przy piciu.
 * Argumenty     : string - nazwa plynu w dopelniaczu.
 */
public void
set_nazwa_plynu_dop(string str)
{
    nazwa_dop = str;
}

/*
 * Nazwa funkcji : query_nazwa_plynu_dop
 * Opis          : Zwraca nazwe plynu, ktory znajduje sie akt. w beczulce
 *		   w dopelniaczu.
 * Funkcja zwraca: string - patrz wyzej.
 */
public string
query_nazwa_plynu_dop()
{
    return nazwa_dop;
}

/*
 * Nazwa funkcji : set_opis_plynu
 * Opis          : Sluzy do ustawienia dlugiego opisu plynu znajdujacego
 *		   sie akt. w beczulce. Powinien on byc w dopelniaczu.
 *		   Wykorzystywany jest np. w dlugim opisie beczulki.
 * Argumenty     : string - dlugi opis w dopelaniczu.
 */
public void
set_opis_plynu(string opis)
{
    opis_plynu = opis;
}

/*
 * Nazwa funkcji : query_opis_plynu
 * Opis          : Zwraca dlugi opis plynu znajdujacego sie akt. w beczulce,
 *		   w dopelniaczu.
 * Funkcja zwraca: string - patrz wyzej.
 */
public string
query_opis_plynu()
{
    return opis_plynu;
}

/*
 * Nazwa funkcji : set_vol
 * Opis          : Ustawia ile procent alkoholu jest w cieczy, ktora
 *		   jest w beczulce.
 * Argumenty     : int - procent alkoholu
 */
public void
set_vol(int vol)
{
    procent_alco = vol;
}

/*
 * Nazwa funkcji : query_vol
 * Opis          : Zwraca jak bardzo procentowy jak napoj w beczulce.
 * Funkcja zwraca: int - ile procent alkoholu jest w plynie w beczulce.
 */
public int
query_vol()
{
    return procent_alco;
}
/*
 * Nazwa funkcji : ustaw_nazwe_plynu
 * Opis          : Ustawia nazwe plynu ktora nalezy podac przy lykaniu,np.
 *		   lyknij wodki z kieliszka(w dopelniaczu nalezy to podac)
 * Argumenty     : string - nazwa w dopelniaczu.
 */
public void    
ustaw_nazwe_plynu(string str)
{
nazwa_plynu=str;
}

private int     
wychyl(string str)
{
    object *obs;
    int ret;
    
    notify_fail("Wychyl co [do dna]?\n");
    
    if (!strlen(str))
        return 0;
    
    if (!parse_command(str, this_player(), " %i:"+PL_BIE, obs) &&
        !parse_command(str, this_player(), " %i:"+PL_BIE+" 'do' 'dna' ", obs))
            return 0;

    obs = NORMAL_ACCESS(obs, 0, 0);
    
    if (!sizeof(obs))
        return 0;
        
    if (sizeof(obs) > 1)
    {
        notify_fail("Mo^zesz si^e napi^c tylko z jednego naczynia naraz.\n");
        return 0;
    }
    
    if(obs[0]->query_prop(OBJ_I_BROKEN))
    {
        notify_fail("Nie bardzo wiesz jak to zrobi^c...\n");
        return 0;
    }

    if(!obs[0]->query_wielded())
    {
        notify_fail("Musisz wpierw chwyci^c " + koncowka("ten", "t^a", "to") +
        " " + query_nazwa(PL_BIE) + ".\n");
        return 0;
    }

    ret = obs[0]->wychyl_to(this_player());

    if (ret)
	this_player()->set_obiekty_zaimkow(obs);

    return ret;
}

public int
wychyl_to()
{

    string str;
    int lyk;
    if (!ilosc_plynu)
    {
        write(capitalize(query_nazwa(PL_MIA)) + " jest zupe^lnie pust" + 
            koncowka("y", "a", "e") + ".\n");
        return 1;
    }
    
    lyk = MIN(this_player()->drink_max() / 20, this_player()->drink_max() - 
    		this_player()->query_soaked());
    if (!lyk)
    {
        write("Nie wmusisz w siebie ani ^lyka wi^ecej.\n");
        return 1;
    }

    lyk = pojemnosc_naczynia;

    
    if (!this_player()->drink_soft(lyk, 1))
    {
         write("Nie wmusisz w siebie ani ^lyka wi^ecej.\n");
         return 1;
    }

    if (!this_player()->drink_alco((lyk * procent_alco) / 100, 0))
    {	
        write("Trunek w " + query_nazwa(PL_MIE) + " jest taki mocny, ^ze chyba " +
            "nie dasz rady go teraz wypi^c.\n");
        return 1;
    }
    
	this_player()->drink_soft(lyk, 0);
    ilosc_plynu -= lyk;
    
    if (ilosc_plynu<=0)
    {
        str = ", opr^o^zniaj^ac " + koncowka("go", "j^a", "je") + " zupe^lnie.\n";
	ilosc_plynu=0;
    }

        
    write("Wychylasz " +query_nazwa(PL_BIE)+ str);
    say(QCIMIE(this_player(), PL_MIA) + " wychyla " + 
    query_nazwa(PL_BIE) + str);
        
    if (ilosc_plynu<=0)
    {
	ilosc_plynu = 0;
        procent_alco = 0;
        opis_plynu = "";
        nazwa_dop = "";
	oblicz_sh_po_wypiciu();
    }
        
    return 1;
}
void
oblicz_sh_po_wypiciu()
{
string *kon=({});
string *kon2=({});
string *lp=({});
string *lm=({});
int i;
kon+=({koncowka("y","a","e")});
kon+=({koncowka("ego","ej","ego")});
kon+=({koncowka("emu","ej","emu")});
kon+=({koncowka("y","a","e")});
kon+=({koncowka("ym","a","ym")});
kon+=({koncowka("ym","ej","ym")});

kon2+=({koncowka("e","e","e")});
kon2+=({koncowka("ych","ych","ych")});
kon2+=({koncowka("ym","ym","ym")});
kon2+=({koncowka("e","e","e")});
kon2+=({koncowka("ymi","ymi","ymi")});
kon2+=({koncowka("ymi","ymi","ymi")});
for(i=0;i<6;i++)
{
    lp+=({"pust"+kon[i]+" "+query_nazwa(i)});
    dodaj_przym("pusty","pu�ci"); //chyba dobrze, co?
}
for(i=0;i<6;i++)
{
    lm+=({"pust"+kon2[i]+" "+query_pnazwa(i)});
    dodaj_przym("pusty","pu�ci"); //chyba dobrze, co?
}
ustaw_shorty(lp,lm);
}

private int
rozbij (string str)
{
	notify_fail("Rozbij co?\n");
	if(!(str ~= query_nazwa(PL_BIE)))
		return 0;

	unwield_me();
	if (!ilosc_plynu)
	{
	write("Ciskasz pust" + koncowka("ym ","^a ","ym ") + query_nazwa(PL_NAR) +
	      " o ziemi^e rozbijaj^ac " + koncowka("go", "j^a", "je") + " na kawa^lki.\n");
	say(QCIMIE(this_player(), PL_MIA) + " ciska pust" + koncowka("ym ","^a ","ym ") +
            query_nazwa(PL_NAR) + " o ziemi^e rozbijaj^ac " + koncowka("go", "j^a", "je") +
	     " na kawa^lki.\n");
	}
	else
	{
	write("Ciskasz " + query_nazwa(PL_NAR) + " o ziemi^e rozbijaj^ac " +
	      koncowka("go", "j^a", "je") + " na kawa^lki.\n");
	say(QCIMIE(this_player(), PL_MIA) + " ciska " + query_nazwa(PL_NAR) +
	    " o ziemi^e rozbijaj^ac " + koncowka("go", "j^a", "je") + " na kawa^lki.\n");
	}
	remove_object();
	
	return 1;
}
/*
private void
rozsyp_sie ()
{
	saybb(capitalize(query_nazwa(PL_MIA)) + " rozsypuje sie w pyl.\n");
	write(capitalize(query_nazwa(PL_MIA)) + " rozsypuje sie w pyl.\n");
	remove_object();
}*/

int
zle (string str)
{
    write("Mo^ze lepiej by^loby " + koncowka("go", "j^a", "je") + " <chwyci^c>?\n");
    return 1;
}
/*
int try_hit
(object target)
{
    return 0;
}
*/
int
toast (string str)
{
    string za;
    object* obs;
    object* czym;

    notify_fail("Wznie^s czym toast [za co / za kogo]?\n");
    if(!str)
        return 0;
        
    if(!parse_command(str, this_player(),"%i:" + PL_NAR + " 'toast' %s", czym, za))
        return 0;
        
    czym = NORMAL_ACCESS(czym, 0, 0);
    
    if(sizeof(czym) != 1)
        return 0;

    if(!czym[0]->query_wielded())
    {
        notify_fail("Musisz wpierw chwyci^c " + koncowka("ten", "t^a", "to") +
        " " + query_nazwa(PL_BIE) + ".\n");
        return 0;
    }
      
    
    if(!(czym[0]->query_ilosc_plynu()))
    {
        notify_fail("Ale^z "+ czym[0]->koncowka("tw^oj","twoja","twoje") + " " + 
                    czym[0]->query_nazwa(PL_MIA) + " jest zupe^lnie pust" +
                    czym[0]->koncowka("y","a","e") +"!\n");
        return 0;
    }
            
    if(za == "")
    {
        write("Unosz^ac wysoko ponad g^low^e sw^oj " + czym[0]->query_nazwa(PL_BIE) + " " +
	      czym[0]->query_nazwa_plynu_dop() + " wznosisz toast.\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " unosz^ac wysoko ponad g^low^e sw^oj " +
	      czym[0]->query_nazwa(PL_BIE) + " " +
	      czym[0]->query_nazwa_plynu_dop() + " wznosi toast.\n");
        return 1;
    }
    
    if(parse_command(za, environment(this_player()), "'za' %l:" + PL_BIE, obs))
    {
    
        obs = NORMAL_ACCESS(obs, 0, 0);
        actor("Unosz^ac wysoko ponad g^low^e sw^oj " + czym[0]->query_nazwa(PL_BIE) + " " + czym[0]->query_nazwa_plynu_dop() +
                " wznosisz toast za", obs, PL_BIE);
        target("unosz^ac wysoko ponad g^low^e sw^oj " + czym[0]->query_nazwa(PL_BIE) + 
                " " + czym[0]->query_nazwa_plynu_dop() + " wznosi toast za ciebie.", obs);
        all2actbb("unosz^ac wysoko ponad g^low^e sw^oj " + czym[0]->query_nazwa(PL_BIE) + 
                " " + czym[0]->query_nazwa_plynu_dop() + " wznosi toast za", obs, PL_BIE, ".\n");
        return 1;            
    }
    if(parse_command(za, environment(this_player()), "'za' %s", za))
    {
        write("Unosz^ac wysoko ponad g^low^e sw^oj " + czym[0]->query_nazwa(PL_BIE) + " " + czym[0]->query_nazwa_plynu_dop() +
                " wznoisz toast za " + za + ".\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " unosz^ac wysoko ponad g^low^e sw^oj " + czym[0]->query_nazwa(PL_BIE) + 
                " " + czym[0]->query_nazwa_plynu_dop() + " wznosi toast za " + za + ".\n");
        return 1;    
    }
    return 0;
}

private int
stuknij (string str)
{
    string str2;
    object* obs;
    object* czym;
    object* kim;

    notify_fail("Stuknij si^e czym z kim?\n");
    if(!str)
        return 0;
        
    if(!parse_command(str, all_inventory(this_player()), " 'si^e' %i:" + PL_NAR + " 'z' %s", czym, str2))
        return 0;
        
    if(!parse_command(str2, all_inventory(environment(this_player())), "%l:" + PL_NAR, kim))
        return 0;

    czym = NORMAL_ACCESS(czym, 0, 0);
    kim = NORMAL_ACCESS(kim, 0, 0);
	
    if(sizeof(czym) != 1)
        return 0;

    if(sizeof(kim) != 1)
        return 0;

    if(!czym[0]->query_wielded())
    {
        notify_fail("Musisz wpierw chwyci^c " + koncowka("ten", "t^a", "to") +
        " " + query_nazwa(PL_BIE) + ".\n");
        return 0;
    }
      
    
    if(!(czym[0]->query_ilosc_plynu()))
    {
        notify_fail("Ale^z "+ czym[0]->koncowka("tw^oj","twoja","twoje") + " " + 
                    czym[0]->query_nazwa(PL_MIA) + " jest zupe^lnie pust" +
                    czym[0]->koncowka("y","a","e") +"!\n");
        return 0;
    }
            
    if ((sizeof(filter(filter(all_inventory(kim[0]), &->query_naczynie()), &operator(!=)(0,) @ &->query_wielded())) +
        sizeof(filter(filter(all_inventory(kim[0]), &->query_beczulka()), &operator(!=)(0,) @ &->query_wielded()))) == 0)   
    {
	notify_fail(kim[0]->query_imie(this_player(), PL_MIA) + " nie trzyma w swych d^loniach ^zadnego naczynia "+
	               "godnego stukni^ecia si^e z nim.\n");
	return 0;
    }

    
        write("Stukasz si^e z "+kim[0]->query_imie(this_player(), PL_NAR)+" "+ czym[0]->query_nazwa(PL_NAR) + " " +
	      czym[0]->query_nazwa_plynu_dop() + ".\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " stuka si^e z "+kim[0]->query_imie(this_player(), PL_NAR)+" "+
	      czym[0]->query_nazwa(PL_NAR) + " " +
	      czym[0]->query_nazwa_plynu_dop() + ".\n",
	      ({ this_player(), kim[0] }));
	tell_object(kim[0], this_player()->query_Imie(kim[0], PL_MIA) + " stuka si^e z tob^a " +
	      czym[0]->query_nazwa(PL_NAR) + " " +
	      czym[0]->query_nazwa_plynu_dop() + ".\n");
        return 1;

}

public int
pomoc(string str)
{
    object nacz;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, nacz))
	return 0;

    if (nacz != this_object())
	return 0;

        write("Dla "+query_nazwa(PL_DOP)+" dost�pne s� komendy:\n"+
            "napij si^e, rozbij, stuknij si^e, wznie^s, wychyl, wylej.\n");
	    return 1;
} 

public int
is_naczynie_picie()
{
    return 1;
}

