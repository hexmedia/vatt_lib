/**
 * \file /lib/colors.c
 *
 * Plik z funkcjami wspomagaj�cymi kolorki
 * 
 */

/**
 * Funkcja zmienia kolor wy�wietlania dla gracza.
 * 
 * @param ob - gracz
 * @param color - kolor
 * 
 * @return 1/0 - wys�ano/nie wys�ano (kolorki s� wy��czone)
 */

int
set_color(object ob, int color)
{
  if (!objectp(ob) || !interactive(ob))
  {
    return 0;
  }
  tell_object(ob, "[1;" + color + "m");
  return 1;
}

/**
 * Funkcja czy�ci kolor wy�wietlania dla gracza.
 * 
 * @param ob - gracz
 * 
 * @return 1/0 - wys�ano/nie wys�ano (kolorki s� wy��czone)
 */

int
clear_color(object ob)
{
  if (!objectp(ob) || !interactive(ob))
  {
    return 0;
  }
  tell_object(ob, "[0m");
  return 1;
}
