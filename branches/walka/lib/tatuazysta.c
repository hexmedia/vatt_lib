// delvert, 18.10.2005

#include <ss_types.h>
#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <money.h>
#include <files.h>
#include <pl.h>


mixed Typy_tatuazy, Wzory_tatuazy;

// Miejsca, w ktorych mozna sobie robic tatuaze
mixed Miejsca = ([
	"na czole": ({ "z czo^la", 0 }),
	"na lewym policzku": ({ "z lewego policzka", 0 }),
	"na prawym policzku": ({ "z prawego policzka", 0 }),
	"na skroni": ({ "ze skroni", TS_HEAD }),
	"na karku": ({ "z karku", TS_NECK }), // ?
	"na szyi": ({ "z szyi", TS_NECK }),
	"na plecach": ({ "z plec^ow", 0 }), // ?
	"na lewej ^lopatce": ({ "z lewej ^lopatki", 0 }), // ?
	"na prawej ^lopatce": ({ "z prawej ^lopatki", 0 }), // ?
	"na lewym ramieniu": ({ "z lewego ramienia", TS_L_ARM }),
	"na prawym ramieniu": ({ "z prawego ramienia", TS_R_ARM }),
	"na lewym przedramieniu": ({ "z lewego przedramienia", TS_L_FOREARM }),
	"na prawym przedramieniu": ({ "z prawego przedramienia", TS_R_FOREARM }),
	"na lewej d^loni": ({ "z lewej d^loni", TS_L_HAND }),
	"na prawej d^loni": ({ "z prawej d^loni", TS_R_HAND }),
	"na lewej piersi": ({ "z lewej piersi", 0 }), // ?
	"na prawej piersi": ({ "z prawej piersi", 0 }), // ?
	"na brzuchu": ({ "z brzucha", TS_STOMACH }),
	"na lewym po^sladku": ({ "z lewego po^sladka", 0 }), // ?
	"na prawym po^sladku": ({ "z prawego po^sladka", 0 }), // ?
	"na lewym udzie": ({ "z lewego uda", TS_L_THIGH }),
	"na prawym udzie": ({ "z prawego uda", TS_R_THIGH }),
	"na lewej ^lydce": ({ "z lewej ^lydki", TS_L_SHIN }),
	"na prawej ^lydce": ({ "z prawej ^lydki", TS_R_SHIN }),
	"na lewej stopie": ({ "z lewej stopy", TS_L_FOOT }),
	"na prawej stopie": ({ "z prawej stopy", TS_R_FOOT })
    ]);

int cena_tatuazu;
int cena_usuniecia_tatuazu;

void
create_tatuazysta()
{
    setuid();
    seteuid(getuid());
    Typy_tatuazy = explode(read_file("/d/Standard/obj/tatuaze/tat_typy.txt"), "\n");
    Wzory_tatuazy = explode(read_file("/d/Standard/obj/tatuaze/tat_wzory.txt"), "\n");
}

void
init_tatuazysta()
{
    add_action("zamow_tatuaz", "zam^ow");
    add_action("usun_tatuaz", "usu^n");
}

void
hook_nie_ma_takiego_tatuazu()
{
    write("Nie ma takiego tatua^zu.\n");
}

void
hook_nie_ma_takiego_miejsca()
{
    write("Nie ma takiego miejsca na tatua^z.\n");
}

void
hook_masz_cos_na_sobie()
{
    write("Mo^ze najpierw zdejmij z siebie par^e rzeczy.\n");
}

void
hook_trzymasz_cos()
{
    write("Mo^ze najpierw opu^s^c, to co trzymasz.\n");
}

void
hook_juz_masz_tam_tatuaz()
{
    write("Ju^z masz tam tatua^z.\n");
}

void
hook_nie_masz_tam_tatuazu()
{
    write("Nie masz tam tatua^zu.\n");
}


void
hook_tworzenie_tatuazu()
{
    write("No i masz nowiutki tatua^z.\n");
}

void
hook_usuwanie_tatuazu()
{
    write("Usuwasz sw^oj tatua^z.\n");
}

void
hook_nie_starczy_na_tatuaz()
{
    write("Nie starczy ci kasy.\n");
}

void
hook_zamow_co()
{
    notify_fail("Zam^ow <typ> tatua^z <numer> <gdzie> ?\n");
}

/*
 * Nazwa funkcji: hook_inne_sprawdzenia_przed_tatuazem
 * Opis         : Je�eli przed wykonaniem tatuazu maj� by� dokonane jeszcze
 *                jakie� niestandardowe sprawdzenia, to wrzu� je tutaj.
 * Zwraca       : Je�li funkcja zwroci 1 - tatuaz zostanie zrobiony,
 *                je�li 0 - tatuaz nie zostanie zrobiony.
 */
int
hook_inne_sprawdzenia_przed_tatuazem()
{
    return 1;
}

int
zamow_tatuaz(string str)
{
    int i, numer;
    string typ, gdzie;

    if (!str || sscanf(str, "%s tatua^z %d %s", typ, numer, gdzie) != 3)
    {
	hook_zamow_co();
	return 0;
    }

    if ((i = member_array(typ, Typy_tatuazy)) != -1)
	typ = Typy_tatuazy[i];
    else
    {
	hook_nie_ma_takiego_tatuazu();
        return 1;
    }

    numer--;
    if (numer < 0 || numer >= sizeof(Wzory_tatuazy))
    {
	hook_nie_ma_takiego_tatuazu();
	return 1;
    }

    if ((i = member_array(gdzie, m_indices(Miejsca))) != -1)
	gdzie = m_indices(Miejsca)[i];
    else
    {
	hook_nie_ma_takiego_miejsca();
	return 1;
    }
    
    if (sizeof(this_player()->query_armour(Miejsca[gdzie][1])))
    {
	hook_masz_cos_na_sobie();
	return 1;
    }

    if (this_player()->query_weapon(Miejsca[gdzie][1]))
    {
	hook_trzymasz_cos();
	return 1;
    }

    if (this_player()->query_tatuaz(gdzie))
    {
	hook_juz_masz_tam_tatuaz();
	return 1;
    }

    if(!hook_inne_sprawdzenia_przed_tatuazem())
        return 1;
	
    
    if (!MONEY_ADD(this_player(), -cena_tatuazu))
    {
	hook_nie_starczy_na_tatuaz();
	return 1;
    }

    this_player()->dodaj_tatuaz(gdzie, typ, Wzory_tatuazy[numer]);

    hook_tworzenie_tatuazu();
    return 1;
}

int
usun_tatuaz(string str)
{
    string gdzie;
    int i;

    notify_fail("Usu^n tatua^z <sk^ad> ?\n");

    if (!str)
	return 0;

    if (sscanf(str, "tatua^z %s", gdzie) != 1)
	return 0;

    // tu jest troche mieszania ;)
    i = member_array(gdzie, map(m_values(Miejsca), &operator([])(,0)));
    if (i == -1)
    {
	hook_nie_ma_takiego_miejsca();
	return 1;
    }
    gdzie = m_indexes(Miejsca)[i];

    if (sizeof(this_player()->query_armour(Miejsca[gdzie][1])))
    {
	hook_masz_cos_na_sobie();
	return 1;
    }

    if (this_player()->query_weapon(Miejsca[gdzie][1]))
    {
	hook_trzymasz_cos();
	return 1;
    }

    if (!this_player()->query_tatuaz(gdzie))
    {
	hook_nie_masz_tam_tatuazu();
	return 1;
    }

    if (!MONEY_ADD(this_player(), -cena_usuniecia_tatuazu))
    {
	hook_nie_starczy_na_tatuaz();
	return 1;
    }

    this_player()->usun_tatuaz(gdzie);

    hook_usuwanie_tatuazu();

    return 1;
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

void
add_introduced(string imie)
{
    set_alarm(2.0, 0.0, "return_introduce", imie);
}

void
return_introduce(string imie)
{
    object osoba;
    osoba = present(imie, environment());

    if (osoba->query_gender())
    {
        command("przedstaw sie " + osoba->query_nazwa(PL_CEL));
	command("powitaj " + osoba->query_nazwa(PL_DOP));
	return;
    }
    else
    {
	command("przedstaw sie " + osoba->query_nazwa(PL_CEL));
              return;
    }
}

void
set_cena_tatuazu(int i)
{
    cena_tatuazu = i;
}

int
query_cena_tatuazu()
{
    return cena_tatuazu;
}

void
set_cena_usuniecia_tatuazu(int i)
{
    cena_usuniecia_tatuazu = i;
}

int
query_cena_usuniecia_tatuazu()
{
    return cena_usuniecia_tatuazu;
}
