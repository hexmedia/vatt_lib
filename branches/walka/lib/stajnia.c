/*
    Standardowy lib stajni. Wystaczy zainheritowac
    Garagoth.
    // Zetar - przykro mi, ale to sie nie nadawalo do niczego... //
    // Avber - dzieki za ortografie, ten kawalek wykorzystalem. //
*/



inherit "/lib/trade";

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <composite.h>
#include <cmdparse.h>
#include <subloc.h>

public int zostawianie(string str);
public int przejrzyj(string str);
public int odbieranie(string str);

static object stajenny = 0;
static string stajenny_file = 0;
static int cena = 100;
static string nazwa_stajni = 0;

mapping w_stajni = ([]); // imie: ({ konie })

/*
 * Function : ustaw_cene
 * Description :ustawia cene oplaty za przechwanie wierzchowca w stajni
 * Arguments: int i - wartosc ceny w miedziakach
 * Returns:
 */
void
ustaw_cene(int i)
{
        cena = i;
}
int
query_cena()
{
    return cena;
}

void
ustaw_nazwe_stajni(string s)
{
    nazwa_stajni = s;
}
string
query_nazwa_stajni()
{
    return nazwa_stajni;
}

/*
 * Function : ustaw_stajennego
 * Description : ustawia ewentualnego stajennego w stajni
 * Arguments: string str - sciezka do pliku stajennego
 * Returns:
 */
void
ustaw_stajennego(string str)
{
        stajenny_file = str;
}

void
reset_stajnia()
{
    if (!stajenny && stajenny_file)
    {
            stajenny = clone_object(stajenny_file);
            stajenny->move(this_object(), 1);
    }
}

nomask void
create_stajnia()
{
    this_object()->add_cmd_item(({ "tabliczke" }), ({ "czytaj", "przeczytaj"
}),
        "@@tabliczka_stajenna");

    config_default_trade();

    this_object()->add_prop(ROOM_I_INSIDE, 1);
    this_object()->add_prop(ROOM_I_NO_HORSE_RIDE, 0);
    this_object()->add_prop(ROOM_I_NO_HORSE_LEAD, 0);

    setuid();
    seteuid(getuid());

    restore_object(MASTER + "_save");
    if(stajenny_file && strlen(stajenny_file))
    {
        stajenny = clone_object(stajenny_file);
        stajenny->move(this_object(), 1);
    }


}

string
tabliczka_stajenna()
{
        return "Witaj przybyszu! W tej stajni mozesz zostawic swego "+
               "wierzchowca, zeby odpoczal i zostal nalezycie
oporzadzony.\n\n"+
               "zostaw <konia>          - zostawienie wierzchowca w stajni\n"+
           "przejrzyj <konie>       - przejrzenie zostawionych koni\n"+
               "odbierz <konia>         - odebranie zostawionego
wierzchowca\n\n"+
               "Cena uslugi wynosi :    - " + MONEY_TEXT_SPLIT(cena, PL_BIE) +
            ".\n";
}

void
init_stajnia()
{
    add_action(zostawianie, "zostaw");
    add_action(przejrzyj, "przejrzyj");
    add_action(odbieranie, "odbierz");
}

void
save_stajnia()
{
    save_object(MASTER + "_save");
}

public string *
kon2str(object * k)
{
    string * ks;
    int i;
    ks = allocate(sizeof(k));
    for(i = 0; i < sizeof(ks); i++)
        ks[i] = k[i]->query_auto_load();
    return ks;
}

public object *
str2kon(string * s)
{
    object * w;
    string m, param, err;
    int a;

    if(!pointerp(s))
        return ({});
    w = allocate(sizeof(s));
    for(a = 0; a < sizeof(s); a++)
    {
        if(!stringp(s[a]))
            continue;
        if(sscanf(s[a], "%s:%s", m, param) == 2)
        {
            w[a] = clone_object(m);
            catch(w[a]->init_arg(param));
        }
        else
            w[a] = 0;
    }
    return w - ({ 0 });
}

private mixed
zostaw_konie(object * cel, object w)
{
    string * s;
    if (!can_pay(cena * sizeof(cel), w))
    {
        notify_fail("Nie stac cie na oplacenie pobytu " +
            FO_COMPOSITE_LIVE(cel, this_player(), PL_DOP, 0) +
            " w stajni.\n");
        return 0;
    }
    s = w_stajni[w->query_real_name(PL_MIA)];
    if(!s)
        s = ({});
    s += kon2str(cel);
    w_stajni[w->query_real_name(PL_MIA)] = s;
    return pay(cena, w);
}

private int
sprawdz_wlasciciela(object h, object w)
{
    if(h->query_owner() != w->query_real_name())
    {
        w->catch_msg("Alez " + h->short(w, PL_MIA) +
            " nie nalezy do ciebie!\n");
        return 0;
    }
    else
        return 1;
}

int
czy_ktos_na_nim_siedzi(object k)
{
    if(k->query_has_riders())
    {
        notify_fail("Na " + k->short(this_player(), PL_MIA) +
            " ktos siedzi, nie mozesz " + k->koncowka("go", "jej", "go") +
            " zostawic w stajni.\n");
        return 0;
    }
    return 1;
}

int
zostawianie(string str)
{
    object * cel;
    int * reszta;
    int jest_stajenny = 0;

    if(!str)
    {
        notify_fail("Zostaw co? Moze jakiegos wierzchowca?\n");
        return 0;
    }
    cel = PARSE_THIS(str, "%l:"+PL_BIE, SUBL_ACS_MANIP);
    if (sizeof(cel))
    {
        if (objectp(stajenny))
        {
            if(!present(stajenny, this_object()))
            {
                notify_fail("Stajenny sobie gdzies poszedl.\n");
                return 0;
            }
            if (!CAN_SEE(stajenny, this_player()))
            {
                stajenny->command("emote rozglada sie z wyrazem " +
                    "zagubienia na twarzy");
                return 1;
            }
            jest_stajenny = 1;
        }
        else if(stringp(stajenny_file))
        {
            notify_fail("Nie widzisz nigdzie stajennego.\n");
            return 0;
        }
        cel = filter(cel, &->query_horse());
        cel = filter(cel, &sprawdz_wlasciciela(, this_player()));
        cel = filter(cel, &czy_ktos_na_nim_siedzi());
        if(sizeof(cel) < 1)
        {
            notify_fail("", 1);
            return 0;
        }
        if(pointerp(reszta = zostaw_konie(cel, this_player())))
        {
            write("Zostawiasz " +
                FO_COMPOSITE_LIVE(cel, this_player(), PL_DOP, 0) +
                " w stajni, placac " +
                (jest_stajenny ? stajenny->short(this_player(), PL_CEL) :
                 "stajennemu") + " " + MONEY_TEXT(reszta[0..3], PL_BIE) +
                (stringp(MONEY_TEXT(reszta[4..8], PL_MIA)) ?
                 " i otrzymujesz " + MONEY_TEXT(reszta[4..8], PL_DOP) +
                 " reszty" : "") + ".\n");
            saybb(QCIMIE(this_player(), PL_MIA) + " zostawia " +
                QCOMPDEAD(PL_BIE, 0) + " w stajni.\n");

            cel->remove_object();
            save_stajnia();
            return 1;
        }
        else
            return 0;
    }
    notify_fail("Zostaw co? Nie widze tu " + str + "...\n");
    return 0;
}

int
przejrzyj(string str)
{
    string i;
    int a, jest_stajenny;
    object * w, * k;
    if(!str)
    {
        notify_fail("Przejrzyj co?\n");
        return 0;
    }
    i = this_player()->query_real_name(PL_MIA);
    if(!pointerp(w_stajni[i]) || !sizeof(w_stajni[i]))
    {
        if(objectp(stajenny) && present(stajenny, this_object()) &&
                CAN_SEE(stajenny, this_player()))
            stajenny->command("powiedz do " + OB_NAME(this_player()) +
                " Nie zostawil" + this_player()->koncowka("es", "as", "os") +
                " w tej stajni zadnego wierzchowca.");
        else
            write("Nie zostawil" + this_player()->koncowka("es", "as", "os") +
                " w tej stajni zadnego wierzchowca.\n");
        return 1;
    }
    if(objectp(stajenny))
    {
        if(present(stajenny, this_object()))
            jest_stajenny = 1;
        else
        {
            notify_fail("Stajenny gdzies sobie poszedl.\n");
            return 0;
        }
    }
    else if(stajenny_file)
    {
        notify_fail("Nie zauwazasz nigdzie stajennego.\n");
        return 0;
    }
    else
        jest_stajenny = 0;

    w = str2kon(w_stajni[i]);
    parse_command(str, w, "%l:"+PL_BIE, k);
    k = SIMPLE_ACCESS(k, 0, 0);

    if(jest_stajenny)
    {
        if (!CAN_SEE(stajenny, this_player()))
        {
            stajenny->command("emote rozglada sie z wyrazem " +
                "zagubienia na twarzy");
            return 1;
        }
        stajenny->command("powiedz do " + OB_NAME(this_player()) +
            " " + (sizeof(k) ? "Zostawil" +
                this_player()->koncowka("es", "as", "os") + " u mnie " +
                "nastepujace wierzchowce:" : "Nie zostawil" +
                this_player()->koncowka("es", "as", "os") + " u mnie " +
                "zadnych takich wierzchowcow."));
    }
    else
        write((sizeof(k) ? "Zostawil" +
            this_player()->koncowka("es", "as", "os") + " tutaj " +
            "nastepujace wierzchowce:" : "Nie zostawil" +
            this_player()->koncowka("es", "as", "os") + " tutaj " +
            "zadnych takich wierzchowcow.") + "\n");
    for(a = 0; a < sizeof(k); a++)
    {
        if(jest_stajenny)
            stajenny->command("powiedz do " + OB_NAME(this_player()) +
                 " " + capitalize(k[a]->query_short(PL_MIA)));
        else
            write(capitalize(k[a]->query_short(PL_MIA)) + "\n");
    }
    w->remove_object();
    return 1;
}

int
odbieranie(string str)
{
    string i;
    int a, jest_stajenny;
    object * w, * k;
    if(!str)
    {
        notify_fail("Przejrzyj co?\n");
        return 0;
    }
    i = this_player()->query_real_name(PL_MIA);
    if(!pointerp(w_stajni[i]) || !sizeof(w_stajni[i]))
    {
        if(objectp(stajenny) && present(stajenny, this_object()) &&
                CAN_SEE(stajenny, this_player()))
            stajenny->command("powiedz do " + OB_NAME(this_player()) +
                " Nie zostawil" + this_player()->koncowka("es", "as", "os") +
                "w tej stajni zadnego wierzchowca.\n");
        else
            write("Nie zostawil" + this_player()->koncowka("es", "as", "os") +
                "w tej stajni zadnego wierzchowca.\n");
        return 1;
    }
    if(objectp(stajenny))
    {
        if(present(stajenny, this_object()))
            jest_stajenny = 1;
        else
        {
            notify_fail("Stajenny gdzies sobie poszedl.\n");
            return 0;
        }
    }
    else if(stajenny_file)
    {
        notify_fail("Nie zauwazasz nigdzie stajennego.\n");
        return 0;
    }
    else
        jest_stajenny = 0;

    w = str2kon(w_stajni[i]);
    k = FIND_STR_IN_ARR(str, w, PL_BIE);

    if (jest_stajenny && !CAN_SEE(stajenny, this_player()))
    {
        stajenny->command("emote rozglada sie z wyrazem " +
            "zagubienia na twarzy");
        return 1;
    }

    (w - k)->remove_object();
    w_stajni[i] -= kon2str(k);
    k->move(this_object(), 1);
    save_stajnia();

    if(jest_stajenny)
    {
        //Stajenny daje konie
        write(stajenny->query_Imie(this_player(), PL_MIA) +
            " przyprowadza ci " + FO_COMPOSITE_LIVE(k, this_player(),
                PL_BIE, 0) + ".\n");
        say(QCIMIE(stajenny, PL_MIA) + " przyprowadza " +
            QCOMPLIVE(PL_MIA, 0) + " i daje " + (sizeof(k) ? "je" : "go") +
            " " + QIMIE(this_player(), PL_CEL) + ".\n");
    }
    else
    {
       //Fake-stajenny daje konie
        write("Stajenny przyprowadza ci " + FO_COMPOSITE_LIVE(k, this_player(),
                PL_BIE, 0) + ".\n");
        say("Stajenny przyprowadza " +
            QCOMPLIVE(PL_MIA, 0) + " i daje " + (sizeof(k) ? "je" : "go") +
            " " + QIMIE(this_player(), PL_CEL) + ".\n");
    }
    return 1;
}
