
#include <ss_types.h>
#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <money.h>
#include <files.h>
#include <pl.h>

/*
* Fryzjer by Jeremian
* 2002
*/

// Bardzo mocno przerobiony
// wlasciwie to z pierwotnej wersji nic nie zostalo ;)

mixed Dlugosci_fryzur, Opisy_fryzur, Kolory_wlosow;

int cena_fryzury;
int cena_farbowania;

void
create_fryzjer()
{
    mixed tmp;
    string str, str2;
    int min, max;
    int i;

    setuid();
    seteuid(getuid());


    Dlugosci_fryzur = explode(read_file("/d/Standard/obj/fryzury/fryz_dlugosci.txt"), "\n");

    Kolory_wlosow = ([ ]);
    tmp = explode(read_file("/d/Standard/obj/fryzury/kolory.txt"), "\n");
    for (i = 0; i < sizeof(tmp); i++)
    {
	if (sscanf(tmp[i], "%s:%s", str, str2) == 2)
	    Kolory_wlosow[str] = str2;
    }

    Opisy_fryzur = ([ ]);
    tmp = explode(read_file("/d/Standard/obj/fryzury/fryz_lista.txt"), "\n");
    for (i = 0; i < sizeof(tmp); i++)
    {
	if (sscanf(tmp[i], "%s:%d-%d", str, min, max) == 3)
	    Opisy_fryzur[str] = ({ min, max });
	else
	    Opisy_fryzur[tmp[i]] = ({ 1, sizeof(Dlugosci_fryzur) });
    }
}

void
init_fryzjer()
{
    add_action("zamow_fryzure", "zam^ow");
    add_action("pofarbuj_wlosy", "pofarbuj");
    add_action("pofarbuj_wlosy", "zafarbuj");
}

void
hook_nie_ma_takiej_fryzury()
{
    write("Nie ma takiej fryzury.\n");
}

void
hook_nie_pasuje_dlugosc()
{
    write("Ta d^lugo^s^c w^los^ow nie pasuje do tej fryzury.\n");
}

void
hook_za_krotkie_wlosy()
{
    write("Masz za kr^otkie w^losy, ^zeby zam^owi^c tak^a fryzur^e.\n");
}

hook_za_dlugie_wlosy()
{
    write("D^lugie w^losy masz, komu je dasz?\n");
}

void
hook_nie_starczy_na_fryzure()
{
    write("Nie starczy ci monet.\n");
}

void
hook_nie_starczy_na_farbowanie()
{
    write("Nie starczy ci monet.\n");
}

/*
void
hook_nie_pasuje_dlugosc()
{
    write("Ta d^lugo^s^c w^los^ow nie pasuje do tej fryzury.\n");
}
*/

void
hook_tworzenie_fryzury()
{
    write("Gratulacje! Masz nowa fryzur^e\n");
}

void
hook_usuwanie_fryzury()
{
    write("Usuwasz swoj^a fryzur^e.\n");
}

void
hook_nie_ma_takiego_koloru()
{
    write("Nie ma takiego koloru.\n");
}

void
hook_farbujesz_wlosy()
{
    write("Farbujesz sobie w^losy.\n");
}

void
hook_zamow_co()
{
    notify_fail("Zam^ow <d^lugo^s^c> w^losy <opis> ?\n");
}

/*
 * Nazwa funkcji: hook_inne_sprawdzenia_przed_fryzura
 * Opis		: Jeeli przed wykonaniem fryzury maj by?dokonane jeszcze
 * 		  jakie niestandardowe sprawdzenia, to wrzu?je tutaj.
 * Zwraca	: Jeli funkcja zwroci 1 - fryzura zostanie zrobiona,
 * 		  jeli 0 - fryzura nie zostanie zrobiona.
 */
int
hook_inne_sprawdzenia_przed_fryzura()
{
	return 1;
}

int
zamow_fryzure(string str)
{
    string s1, s2, s3;
    float dlugosc;
    int min, max;
    int dl;
    int i;


    if (!str || sscanf(str, "%s w^losy %s", s1, s3) != 2)
    {
	hook_zamow_co();
	return 0;
    }

    if ((i = member_array(s1, Dlugosci_fryzur)) != -1)
	s1 = Dlugosci_fryzur[i];
    else
    {
	hook_nie_ma_takiej_fryzury();
	return 1;
    }

/*
    if ((i = member_array(s2, Typy_fryzur)) != -1)
	s2 = Typy_fryzur[i];
    else
    {
	hook_nie_ma_takiej_fryzury();
        return 1;
    }
*/

    if ((i = member_array(s3, m_indices(Opisy_fryzur))) != -1)
	s3 = m_indices(Opisy_fryzur)[i];
    else
    {
	hook_nie_ma_takiej_fryzury();
	return 1;
    }

    switch (s1)
    {
	case "bardzo kr^otkie": dlugosc = 3.0; dl = 1; break;
	case "kr^otkie": dlugosc = 6.0; dl = 2; break;
	case "^sredniej d^lugo^sci": dlugosc = 10.0; dl = 3; break;
	case "si^egaj^ace ramion": dlugosc = 30.0; dl = 4; break;
	case "d^lugie": dlugosc = 40.0; dl = 5; break;
	case "bardzo d^lugie": dlugosc = 50.0; dl = 6; break;
	case "si^egaj^ace pasa": dlugosc = 65.0; dl = 7; break;
	case "si^egaj^ace po^lowy ud": dlugosc = 85.0; dl = 8; break;
	case "si^egaj^ace kolan": dlugosc = 110.0; dl = 9; break;
	case "si^egaj^ace ziemi": dlugosc = 160.0; dl = 10; break;
	default: dlugosc = 0.0; dl = 0; break;
    }

    if (dlugosc > this_player()->query_dlugosc_wlosow())
    {
	hook_za_krotkie_wlosy();
	return 1;
    }

    min = Opisy_fryzur[s3][0];
    max = Opisy_fryzur[s3][1];
    if (dl < min)
    {
	hook_nie_pasuje_dlugosc();
	return 1;
    }
    if (dl > max)
    {
	dl = (min + max + 1) / 2;
	switch (dl)
        {
	    case 1: s1 = "bardzo kr^otkie"; dlugosc = 3.0; break;
	    case 2: s1 = "kr^otkie"; dlugosc = 6.0; break;
	    case 3: s1 = "^sredniej d^lugo^sci"; dlugosc = 10.0; break;
	    case 4: s1 = "si^egaj^ace ramion"; dlugosc = 30.0; break;
	    case 5: s1 = "d^lugie"; dlugosc = 40.0; break;
	    case 6: s1 = "bardzo d^lugie"; dlugosc = 50.0; break;
    	    case 7: s1 = "si^egaj^ace pasa"; dlugosc = 65.0; break;
	    case 8: s1 = "si^egaj^ace po^lowy ud"; dlugosc = 85.0; break;
	    case 9: s1 = "si^egaj^ace kolan"; dlugosc = 110.0; break;
	    case 10: s1 = "si^egaj^ace ziemi"; dlugosc = 160.0; break;
//	    default: dlugosc = 0.0; dl = 0; break;
	}
	hook_za_dlugie_wlosy();
    }

    if(!hook_inne_sprawdzenia_przed_fryzura())
	    return 1;

    if (!MONEY_ADD(this_player(), -cena_fryzury))
    {
	hook_nie_starczy_na_fryzure();
	return 1;
    }

    this_player()->set_opis_fryzury(s3);
    this_player()->set_dlugosc_wlosow(dlugosc);

    write("Zamawiasz wybran� fryzur^e.\n");
    say(QCIMIE(this_player(),PL_MIA) + " zamawia sobie jak^a^s fryzur^e.\n");
    set_alarm(5.0,0.0,"hook_tworzenie_fryzury");
    return 1;
}

int
pofarbuj_wlosy(string str)
{
    notify_fail("Pofarbuj w^losy na <kolor> ?\n");

    if (!str || sscanf(str, "w^losy na %s", str) != 1)
	return 0;

    if (!Kolory_wlosow[str])
    {
	hook_nie_ma_takiego_koloru();
	return 1;
    }

    if (!MONEY_ADD(this_player(), -cena_farbowania))
    {
	hook_nie_starczy_na_farbowanie();
	return 1;
    }
    
    //Dodajemy przym do listy. Vera
    string lpoj,lmn;
    int prior;
    switch(str)
    {
        case "czarno":lpoj="czarnow^losy";lmn="czarnow^losi";prior=4;break;
        case "kruczoczarno":lpoj="krucz^lowosy";lmn="krucz^lowosi";prior=4;break;
        case "siwo":lpoj="siwow^losy";lmn="siwow^losi";prior=6;break;
        case "^snie^znobia^lo":lpoj="bia^low^losy";lmn="bia^low^losi";prior=8;break;
        case "srebrzy^scie":lpoj="srebrzystow^losy";lmn="srebrzystow^losi";prior=7;break;
        case "z^locisto":lpoj="zocistow^losy";lmn="zocistow^losi";prior=6;break;
        case "kasztanowo":lpoj="kasztanowow^losy";lmn="kasztanowow^losi";prior=4;break;
        case "orzechowo":lpoj="orzechowow^losy";lmn="orzechowow^losi";prior=5;break;
        case "popielato":lpoj="popielatow^losy";lmn="popielatow^losi";prior=5;break;
        case "b^l^ekitno":
        case "niebiesko":lpoj="niebieskow^losy";lmn="niebieskow^losi";prior=8;break;
        case "rudo":lpoj="rudow^losy";lmn="rudow^losi";prior=7;break;
        case "jaskrawoczerwono":
        case "czerwono":lpoj="czerwonow^losy";lmn="czerwonow^losi";prior=7;break;
        case "jaskrawozielono":
        case "jasnozielono":
        case "zielono":
        case "ciemnozielono":lpoj="zielonow^losy";lmn="zielonow^losi";prior=7;break;
        case "fioletowo":lpoj="fioletowow^losy";lmn="fioletowow^losi";prior=8;break;
        case "blond":lpoj="jasnow^losy";lmn="jasnow^losi";prior=4;break;
        case "mied^x":lpoj="miedzianow^losy";lmn="miedzianow^losi";prior=5;break;
        case "platyn^e":lpoj="platynowow^losy";lmn="platynowow^losi";prior=6;break;
		case "mi�d":lpoj="miodowow�osy";lmn="miodowow�osi";prior=5;break;
        default: lpoj="matowow^losy";lmn="matowow^losi";prior=3;break;
    }
    TP->usun_przym_z_listy(TP->query_priorytet_naturalnego(),
                           TP->query_naturalny_kolor_wlosow()[1],
                           TP->query_naturalny_kolor_wlosow()[2]);
    TP->usun_przym_z_listy(TP->query_priorytet_farby(),
                           TP->query_kolor_wlosow()[1],
                           TP->query_kolor_wlosow()[2]);
    TP->dodaj_przym_do_listy(prior,lpoj,lmn);
    TP->set_priorytet_farby(prior);
    TP->set_kolor_wlosow(Kolory_wlosow[str],lpoj,lmn);

    hook_farbujesz_wlosy();
    
    return 1;
}


void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

void
add_introduced(string imie)
{
    set_alarm(2.0, 0.0, "return_introduce", imie);
}

void
return_introduce(string imie)
{
    object osoba;
    osoba = present(imie, environment());

    if (osoba->query_gender())
    {
        command("przedstaw sie " + osoba->query_nazwa(PL_CEL));
	command("powitaj " + osoba->query_nazwa(PL_DOP));
	return;
    }
    else
    {
	command("przedstaw sie " + osoba->query_nazwa(PL_CEL));
              return;
    }
}


void
set_cena_fryzury(int i)
{
    cena_fryzury = i;
}

int
query_cena_fryzury()
{
    return cena_fryzury;
}

void
set_cena_farbowania(int i)
{
    cena_farbowania = i;
}

int
query_cena_farbowania()
{
    return cena_farbowania;
}
